
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.claro_dslam_interface.PortStatus;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getStatusPortResult" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}PortStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getStatusPortResult"
})
@XmlRootElement(name = "getStatusPortResponse")
public class GetStatusPortResponse {

    @XmlElementRef(name = "getStatusPortResult", namespace = "http://tempuri.org/", type = JAXBElement.class)
    protected JAXBElement<PortStatus> getStatusPortResult;

    /**
     * Gets the value of the getStatusPortResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PortStatus }{@code >}
     *     
     */
    public JAXBElement<PortStatus> getGetStatusPortResult() {
        return getStatusPortResult;
    }

    /**
     * Sets the value of the getStatusPortResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PortStatus }{@code >}
     *     
     */
    public void setGetStatusPortResult(JAXBElement<PortStatus> value) {
        this.getStatusPortResult = ((JAXBElement<PortStatus> ) value);
    }

}
