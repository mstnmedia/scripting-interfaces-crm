
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.datacontract.schemas._2004._07.claro_dslam_interface.PortStatus;
import org.datacontract.schemas._2004._07.claro_m6_apiinterface.ArrayOfDSLAMFACILITY;
import org.datacontract.schemas._2004._07.claro_sacs.CompositeType;
import org.datacontract.schemas._2004._07.claro_sacs_webservices.DiagnosticResult;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetStatusPortNodeAddress_QNAME = new QName("http://tempuri.org/", "node_address");
    private final static QName _GetStatusPortOrderNumber_QNAME = new QName("http://tempuri.org/", "order_number");
    private final static QName _GetStatusPortVendorName_QNAME = new QName("http://tempuri.org/", "vendor_name");
    private final static QName _GetStatusPortNodeName_QNAME = new QName("http://tempuri.org/", "node_name");
    private final static QName _GetDiagnosticoTelefono_QNAME = new QName("http://tempuri.org/", "telefono");
    private final static QName _GetDataUsingDataContractResponseGetDataUsingDataContractResult_QNAME = new QName("http://tempuri.org/", "GetDataUsingDataContractResult");
    private final static QName _InsertSCATTestTipo_QNAME = new QName("http://tempuri.org/", "tipo");
    private final static QName _InsertSCATTestAni_QNAME = new QName("http://tempuri.org/", "ani");
    private final static QName _InsertSCATTestTarjeta_QNAME = new QName("http://tempuri.org/", "tarjeta");
    private final static QName _GetDataResponseGetDataResult_QNAME = new QName("http://tempuri.org/", "GetDataResult");
    private final static QName _GetFacilitysResponseGetFacilitysResult_QNAME = new QName("http://tempuri.org/", "GetFacilitysResult");
    private final static QName _GetDiagnosticoResponseGetDiagnosticoResult_QNAME = new QName("http://tempuri.org/", "getDiagnosticoResult");
    private final static QName _GetStatusPortResponseGetStatusPortResult_QNAME = new QName("http://tempuri.org/", "getStatusPortResult");
    private final static QName _GetDataUsingDataContractComposite_QNAME = new QName("http://tempuri.org/", "composite");
    private final static QName _ConsultarNumeroPendienteResponseConsultarNumeroPendienteResult_QNAME = new QName("http://tempuri.org/", "ConsultarNumeroPendienteResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetDiagnostico }
     * 
     */
    public GetDiagnostico createGetDiagnostico() {
        return new GetDiagnostico();
    }

    /**
     * Create an instance of {@link GetDataUsingDataContractResponse }
     * 
     */
    public GetDataUsingDataContractResponse createGetDataUsingDataContractResponse() {
        return new GetDataUsingDataContractResponse();
    }

    /**
     * Create an instance of {@link GetFacilitys }
     * 
     */
    public GetFacilitys createGetFacilitys() {
        return new GetFacilitys();
    }

    /**
     * Create an instance of {@link ActualizarRespuesta }
     * 
     */
    public ActualizarRespuesta createActualizarRespuesta() {
        return new ActualizarRespuesta();
    }

    /**
     * Create an instance of {@link GetDataUsingDataContract }
     * 
     */
    public GetDataUsingDataContract createGetDataUsingDataContract() {
        return new GetDataUsingDataContract();
    }

    /**
     * Create an instance of {@link GetStatusPort }
     * 
     */
    public GetStatusPort createGetStatusPort() {
        return new GetStatusPort();
    }

    /**
     * Create an instance of {@link ActualizarRespuestaResponse }
     * 
     */
    public ActualizarRespuestaResponse createActualizarRespuestaResponse() {
        return new ActualizarRespuestaResponse();
    }

    /**
     * Create an instance of {@link InsertSCATTest }
     * 
     */
    public InsertSCATTest createInsertSCATTest() {
        return new InsertSCATTest();
    }

    /**
     * Create an instance of {@link GetDataResponse }
     * 
     */
    public GetDataResponse createGetDataResponse() {
        return new GetDataResponse();
    }

    /**
     * Create an instance of {@link ConsultarNumeroPendiente }
     * 
     */
    public ConsultarNumeroPendiente createConsultarNumeroPendiente() {
        return new ConsultarNumeroPendiente();
    }

    /**
     * Create an instance of {@link GetFacilitysResponse }
     * 
     */
    public GetFacilitysResponse createGetFacilitysResponse() {
        return new GetFacilitysResponse();
    }

    /**
     * Create an instance of {@link GetDiagnosticoResponse }
     * 
     */
    public GetDiagnosticoResponse createGetDiagnosticoResponse() {
        return new GetDiagnosticoResponse();
    }

    /**
     * Create an instance of {@link GetStatusPortResponse }
     * 
     */
    public GetStatusPortResponse createGetStatusPortResponse() {
        return new GetStatusPortResponse();
    }

    /**
     * Create an instance of {@link ConsultarNumeroPendienteResponse }
     * 
     */
    public ConsultarNumeroPendienteResponse createConsultarNumeroPendienteResponse() {
        return new ConsultarNumeroPendienteResponse();
    }

    /**
     * Create an instance of {@link GetData }
     * 
     */
    public GetData createGetData() {
        return new GetData();
    }

    /**
     * Create an instance of {@link InsertSCATTestResponse }
     * 
     */
    public InsertSCATTestResponse createInsertSCATTestResponse() {
        return new InsertSCATTestResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "node_address", scope = GetStatusPort.class)
    public JAXBElement<String> createGetStatusPortNodeAddress(String value) {
        return new JAXBElement<String>(_GetStatusPortNodeAddress_QNAME, String.class, GetStatusPort.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "order_number", scope = GetStatusPort.class)
    public JAXBElement<String> createGetStatusPortOrderNumber(String value) {
        return new JAXBElement<String>(_GetStatusPortOrderNumber_QNAME, String.class, GetStatusPort.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "vendor_name", scope = GetStatusPort.class)
    public JAXBElement<String> createGetStatusPortVendorName(String value) {
        return new JAXBElement<String>(_GetStatusPortVendorName_QNAME, String.class, GetStatusPort.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "node_name", scope = GetStatusPort.class)
    public JAXBElement<String> createGetStatusPortNodeName(String value) {
        return new JAXBElement<String>(_GetStatusPortNodeName_QNAME, String.class, GetStatusPort.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "telefono", scope = GetDiagnostico.class)
    public JAXBElement<String> createGetDiagnosticoTelefono(String value) {
        return new JAXBElement<String>(_GetDiagnosticoTelefono_QNAME, String.class, GetDiagnostico.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompositeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetDataUsingDataContractResult", scope = GetDataUsingDataContractResponse.class)
    public JAXBElement<CompositeType> createGetDataUsingDataContractResponseGetDataUsingDataContractResult(CompositeType value) {
        return new JAXBElement<CompositeType>(_GetDataUsingDataContractResponseGetDataUsingDataContractResult_QNAME, CompositeType.class, GetDataUsingDataContractResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "tipo", scope = InsertSCATTest.class)
    public JAXBElement<String> createInsertSCATTestTipo(String value) {
        return new JAXBElement<String>(_InsertSCATTestTipo_QNAME, String.class, InsertSCATTest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ani", scope = InsertSCATTest.class)
    public JAXBElement<String> createInsertSCATTestAni(String value) {
        return new JAXBElement<String>(_InsertSCATTestAni_QNAME, String.class, InsertSCATTest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "tarjeta", scope = InsertSCATTest.class)
    public JAXBElement<String> createInsertSCATTestTarjeta(String value) {
        return new JAXBElement<String>(_InsertSCATTestTarjeta_QNAME, String.class, InsertSCATTest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetDataResult", scope = GetDataResponse.class)
    public JAXBElement<String> createGetDataResponseGetDataResult(String value) {
        return new JAXBElement<String>(_GetDataResponseGetDataResult_QNAME, String.class, GetDataResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "telefono", scope = GetFacilitys.class)
    public JAXBElement<String> createGetFacilitysTelefono(String value) {
        return new JAXBElement<String>(_GetDiagnosticoTelefono_QNAME, String.class, GetFacilitys.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDSLAMFACILITY }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetFacilitysResult", scope = GetFacilitysResponse.class)
    public JAXBElement<ArrayOfDSLAMFACILITY> createGetFacilitysResponseGetFacilitysResult(ArrayOfDSLAMFACILITY value) {
        return new JAXBElement<ArrayOfDSLAMFACILITY>(_GetFacilitysResponseGetFacilitysResult_QNAME, ArrayOfDSLAMFACILITY.class, GetFacilitysResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DiagnosticResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getDiagnosticoResult", scope = GetDiagnosticoResponse.class)
    public JAXBElement<DiagnosticResult> createGetDiagnosticoResponseGetDiagnosticoResult(DiagnosticResult value) {
        return new JAXBElement<DiagnosticResult>(_GetDiagnosticoResponseGetDiagnosticoResult_QNAME, DiagnosticResult.class, GetDiagnosticoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getStatusPortResult", scope = GetStatusPortResponse.class)
    public JAXBElement<PortStatus> createGetStatusPortResponseGetStatusPortResult(PortStatus value) {
        return new JAXBElement<PortStatus>(_GetStatusPortResponseGetStatusPortResult_QNAME, PortStatus.class, GetStatusPortResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompositeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "composite", scope = GetDataUsingDataContract.class)
    public JAXBElement<CompositeType> createGetDataUsingDataContractComposite(CompositeType value) {
        return new JAXBElement<CompositeType>(_GetDataUsingDataContractComposite_QNAME, CompositeType.class, GetDataUsingDataContract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ConsultarNumeroPendienteResult", scope = ConsultarNumeroPendienteResponse.class)
    public JAXBElement<String> createConsultarNumeroPendienteResponseConsultarNumeroPendienteResult(String value) {
        return new JAXBElement<String>(_ConsultarNumeroPendienteResponseConsultarNumeroPendienteResult_QNAME, String.class, ConsultarNumeroPendienteResponse.class, value);
    }

}
