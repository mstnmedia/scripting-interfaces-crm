
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.claro_sacs_webservices.DiagnosticResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getDiagnosticoResult" type="{http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model}DiagnosticResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDiagnosticoResult"
})
@XmlRootElement(name = "getDiagnosticoResponse")
public class GetDiagnosticoResponse {

    @XmlElementRef(name = "getDiagnosticoResult", namespace = "http://tempuri.org/", type = JAXBElement.class)
    protected JAXBElement<DiagnosticResult> getDiagnosticoResult;

    /**
     * Gets the value of the getDiagnosticoResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DiagnosticResult }{@code >}
     *     
     */
    public JAXBElement<DiagnosticResult> getGetDiagnosticoResult() {
        return getDiagnosticoResult;
    }

    /**
     * Sets the value of the getDiagnosticoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DiagnosticResult }{@code >}
     *     
     */
    public void setGetDiagnosticoResult(JAXBElement<DiagnosticResult> value) {
        this.getDiagnosticoResult = ((JAXBElement<DiagnosticResult> ) value);
    }

}
