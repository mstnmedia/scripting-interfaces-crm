
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.claro_m6_apiinterface.ArrayOfDSLAMFACILITY;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFacilitysResult" type="{http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel}ArrayOfDSLAMFACILITY" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFacilitysResult"
})
@XmlRootElement(name = "GetFacilitysResponse")
public class GetFacilitysResponse {

    @XmlElementRef(name = "GetFacilitysResult", namespace = "http://tempuri.org/", type = JAXBElement.class)
    protected JAXBElement<ArrayOfDSLAMFACILITY> getFacilitysResult;

    /**
     * Gets the value of the getFacilitysResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfDSLAMFACILITY }{@code >}
     *     
     */
    public JAXBElement<ArrayOfDSLAMFACILITY> getGetFacilitysResult() {
        return getFacilitysResult;
    }

    /**
     * Sets the value of the getFacilitysResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfDSLAMFACILITY }{@code >}
     *     
     */
    public void setGetFacilitysResult(JAXBElement<ArrayOfDSLAMFACILITY> value) {
        this.getFacilitysResult = ((JAXBElement<ArrayOfDSLAMFACILITY> ) value);
    }

}
