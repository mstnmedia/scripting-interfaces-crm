package org.tempuri;

import com.mstn.scripting.core.models.InterfaceConfigs;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.3-b02- Generated
 * source version: 2.1
 *
 */
@WebServiceClient(name = "DWServices", targetNamespace = "http://tempuri.org/", wsdlLocation = "http://nttappsweb0010.corp.codetel.com.do/SACSWS/DWServices.svc?wsdl")
public class DWServices extends Service {

	private final static URL DWSERVICES_WSDL_LOCATION;
	private final static Logger LOGGER = Logger.getLogger(org.tempuri.DWServices.class.getName());

	static public final String CONFIG_KEY = "diagnostico_web";

	static public IDWServices getInstance() {
		if (!InterfaceConfigs.wsClients.containsKey(CONFIG_KEY)) {
			IDWServices port = new DWServices().getBasicHttpBindingIDWServices();
			InterfaceConfigs.wsClients.put(CONFIG_KEY, port);
		}
		return (IDWServices) InterfaceConfigs.wsClients.get(CONFIG_KEY);
	}
	static {
		URL url = null;
		String configURL = InterfaceConfigs.get(CONFIG_KEY);
		try {
			URL baseUrl;
			baseUrl = org.tempuri.DWServices.class.getResource(".");
			url = new URL(baseUrl, configURL);
		} catch (MalformedURLException e) {
			LOGGER.log(Level.WARNING, "Failed to create URL for the wsdl Location: '{0}', retrying as a local file", configURL);
			LOGGER.warning(e.getMessage());
		}
		DWSERVICES_WSDL_LOCATION = url;
	}

	public DWServices(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public DWServices() {
		super(DWSERVICES_WSDL_LOCATION, new QName("http://tempuri.org/", "DWServices"));
	}

	/**
	 *
	 * @return returns IDWServices
	 */
	@WebEndpoint(name = "BasicHttpBinding_IDWServices")
	public IDWServices getBasicHttpBindingIDWServices() {
		return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IDWServices"), IDWServices.class);
	}

	/**
	 *
	 * @param features A list of {@link javax.xml.ws.WebServiceFeature} to
	 * configure on the proxy. Supported features not in the
	 * <code>features</code> parameter will have their default values.
	 * @return returns IDWServices
	 */
	@WebEndpoint(name = "BasicHttpBinding_IDWServices")
	public IDWServices getBasicHttpBindingIDWServices(WebServiceFeature... features) {
		return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IDWServices"), IDWServices.class, features);
	}

}
