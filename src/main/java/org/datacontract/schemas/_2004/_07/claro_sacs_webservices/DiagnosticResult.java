
package org.datacontract.schemas._2004._07.claro_sacs_webservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.claro_dslam_interface.DIAGNOSTICO;
import org.datacontract.schemas._2004._07.claro_dslam_interface.DIAGNOSTICOGPON;
import org.datacontract.schemas._2004._07.claro_dslam_interface.DSLAMINFO;
import org.datacontract.schemas._2004._07.claro_dslam_interface.FACILITY;
import org.datacontract.schemas._2004._07.claro_dslam_interface.GPONINFO;


/**
 * <p>Java class for DiagnosticResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DiagnosticResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoPrueba" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="diagnostico" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}DIAGNOSTICO" minOccurs="0"/>
 *         &lt;element name="diagnosticoGpon" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}DIAGNOSTICO_GPON" minOccurs="0"/>
 *         &lt;element name="dslamInfo" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}DSLAM_INFO" minOccurs="0"/>
 *         &lt;element name="facilidades" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}FACILITY" minOccurs="0"/>
 *         &lt;element name="gponInfo" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}GPON_INFO" minOccurs="0"/>
 *         &lt;element name="messege" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="testResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiagnosticResult", propOrder = {
    "codigoPrueba",
    "diagnostico",
    "diagnosticoGpon",
    "dslamInfo",
    "facilidades",
    "gponInfo",
    "messege",
    "testResult"
})
public class DiagnosticResult {

    protected Integer codigoPrueba;
    @XmlElementRef(name = "diagnostico", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", type = JAXBElement.class)
    protected JAXBElement<DIAGNOSTICO> diagnostico;
    @XmlElementRef(name = "diagnosticoGpon", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", type = JAXBElement.class)
    protected JAXBElement<DIAGNOSTICOGPON> diagnosticoGpon;
    @XmlElementRef(name = "dslamInfo", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", type = JAXBElement.class)
    protected JAXBElement<DSLAMINFO> dslamInfo;
    @XmlElementRef(name = "facilidades", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", type = JAXBElement.class)
    protected JAXBElement<FACILITY> facilidades;
    @XmlElementRef(name = "gponInfo", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", type = JAXBElement.class)
    protected JAXBElement<GPONINFO> gponInfo;
    @XmlElementRef(name = "messege", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", type = JAXBElement.class)
    protected JAXBElement<String> messege;
    @XmlElementRef(name = "testResult", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", type = JAXBElement.class)
    protected JAXBElement<String> testResult;

    /**
     * Gets the value of the codigoPrueba property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoPrueba() {
        return codigoPrueba;
    }

    /**
     * Sets the value of the codigoPrueba property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoPrueba(Integer value) {
        this.codigoPrueba = value;
    }

    /**
     * Gets the value of the diagnostico property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DIAGNOSTICO }{@code >}
     *     
     */
    public JAXBElement<DIAGNOSTICO> getDiagnostico() {
        return diagnostico;
    }

    /**
     * Sets the value of the diagnostico property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DIAGNOSTICO }{@code >}
     *     
     */
    public void setDiagnostico(JAXBElement<DIAGNOSTICO> value) {
        this.diagnostico = ((JAXBElement<DIAGNOSTICO> ) value);
    }

    /**
     * Gets the value of the diagnosticoGpon property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DIAGNOSTICOGPON }{@code >}
     *     
     */
    public JAXBElement<DIAGNOSTICOGPON> getDiagnosticoGpon() {
        return diagnosticoGpon;
    }

    /**
     * Sets the value of the diagnosticoGpon property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DIAGNOSTICOGPON }{@code >}
     *     
     */
    public void setDiagnosticoGpon(JAXBElement<DIAGNOSTICOGPON> value) {
        this.diagnosticoGpon = ((JAXBElement<DIAGNOSTICOGPON> ) value);
    }

    /**
     * Gets the value of the dslamInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DSLAMINFO }{@code >}
     *     
     */
    public JAXBElement<DSLAMINFO> getDslamInfo() {
        return dslamInfo;
    }

    /**
     * Sets the value of the dslamInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DSLAMINFO }{@code >}
     *     
     */
    public void setDslamInfo(JAXBElement<DSLAMINFO> value) {
        this.dslamInfo = ((JAXBElement<DSLAMINFO> ) value);
    }

    /**
     * Gets the value of the facilidades property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FACILITY }{@code >}
     *     
     */
    public JAXBElement<FACILITY> getFacilidades() {
        return facilidades;
    }

    /**
     * Sets the value of the facilidades property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FACILITY }{@code >}
     *     
     */
    public void setFacilidades(JAXBElement<FACILITY> value) {
        this.facilidades = ((JAXBElement<FACILITY> ) value);
    }

    /**
     * Gets the value of the gponInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GPONINFO }{@code >}
     *     
     */
    public JAXBElement<GPONINFO> getGponInfo() {
        return gponInfo;
    }

    /**
     * Sets the value of the gponInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GPONINFO }{@code >}
     *     
     */
    public void setGponInfo(JAXBElement<GPONINFO> value) {
        this.gponInfo = ((JAXBElement<GPONINFO> ) value);
    }

    /**
     * Gets the value of the messege property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMessege() {
        return messege;
    }

    /**
     * Sets the value of the messege property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMessege(JAXBElement<String> value) {
        this.messege = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the testResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTestResult() {
        return testResult;
    }

    /**
     * Sets the value of the testResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTestResult(JAXBElement<String> value) {
        this.testResult = ((JAXBElement<String> ) value);
    }

}
