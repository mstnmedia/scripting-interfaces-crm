
package org.datacontract.schemas._2004._07.claro_dslam_interface;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DSLAM_INFO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DSLAM_INFO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Actual_Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Availability" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cabina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Calidad_Servicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Condicion_Administrativa" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Condicion_Operativa" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CustomerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiagnosticoCobre" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}DIAGNOSTICO" minOccurs="0"/>
 *         &lt;element name="Distacia_SVDG" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Error_Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Facilida" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}FACILITY" minOccurs="0"/>
 *         &lt;element name="Feeder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ID_FACILIDADES" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Linea_Defectuosa" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Loop_Atenuacion_Bajada" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Loop_Atenuacion_Subida" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MAC_Address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Maxima_Velocidad_Bajada" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Maxima_Velocidad_Subida" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Nombre_Profile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Numero_Profile" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PVC_33" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PVC_35" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Restrt_Cnt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rinit_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ruido_Bajada" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Ruido_Subida" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Senal_Atenuacion_Bajada" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Senal_Atenuacion_Subida" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Serial_Modem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Session_Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Session_Id_IPTV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Slot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tipo_Tarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ultima_Bajada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ultimo_Sincronismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="User" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VLAN_IPTV" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VLAN_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Velocidad_ADSL" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Velocidad_Actual_DN" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Velocidad_Actual_UP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Velocidad_Configurada_DN" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Velocidad_Configurada_UP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Velocidad_Contratada_DN" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Velocidad_Contratada_UP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Velocidad_Puerto_DN" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Velocidad_Puerto_UP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="distancia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dividirPorMillar" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DSLAM_INFO", propOrder = {
    "actualType",
    "availability",
    "cabina",
    "calidadServicio",
    "condicionAdministrativa",
    "condicionOperativa",
    "customerID",
    "diagnosticoCobre",
    "distaciaSVDG",
    "enabled",
    "errorStatus",
    "facilida",
    "feeder",
    "id",
    "idfacilidades",
    "lineaDefectuosa",
    "loopAtenuacionBajada",
    "loopAtenuacionSubida",
    "macAddress",
    "maximaVelocidadBajada",
    "maximaVelocidadSubida",
    "nombreProfile",
    "numeroProfile",
    "pvc33",
    "pvc35",
    "restrtCnt",
    "rinitID",
    "ruidoBajada",
    "ruidoSubida",
    "senalAtenuacionBajada",
    "senalAtenuacionSubida",
    "serialModem",
    "sessionId",
    "sessionIdIPTV",
    "slot",
    "tipoTarjeta",
    "ultimaBajada",
    "ultimoSincronismo",
    "user",
    "vlaniptv",
    "vlanId",
    "velocidadADSL",
    "velocidadActualDN",
    "velocidadActualUP",
    "velocidadConfiguradaDN",
    "velocidadConfiguradaUP",
    "velocidadContratadaDN",
    "velocidadContratadaUP",
    "velocidadPuertoDN",
    "velocidadPuertoUP",
    "distancia",
    "dividirPorMillar"
})
public class DSLAMINFO {

    @XmlElementRef(name = "Actual_Type", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> actualType;
    @XmlElementRef(name = "Availability", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> availability;
    @XmlElementRef(name = "Cabina", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> cabina;
    @XmlElementRef(name = "Calidad_Servicio", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> calidadServicio;
    @XmlElement(name = "Condicion_Administrativa")
    protected Boolean condicionAdministrativa;
    @XmlElement(name = "Condicion_Operativa")
    protected Boolean condicionOperativa;
    @XmlElementRef(name = "CustomerID", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> customerID;
    @XmlElementRef(name = "DiagnosticoCobre", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<DIAGNOSTICO> diagnosticoCobre;
    @XmlElement(name = "Distacia_SVDG")
    protected Integer distaciaSVDG;
    @XmlElement(name = "Enabled")
    protected Boolean enabled;
    @XmlElementRef(name = "Error_Status", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> errorStatus;
    @XmlElementRef(name = "Facilida", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<FACILITY> facilida;
    @XmlElementRef(name = "Feeder", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> feeder;
    @XmlElement(name = "ID")
    protected Integer id;
    @XmlElement(name = "ID_FACILIDADES")
    protected Integer idfacilidades;
    @XmlElement(name = "Linea_Defectuosa")
    protected Boolean lineaDefectuosa;
    @XmlElement(name = "Loop_Atenuacion_Bajada")
    protected Integer loopAtenuacionBajada;
    @XmlElement(name = "Loop_Atenuacion_Subida")
    protected Integer loopAtenuacionSubida;
    @XmlElementRef(name = "MAC_Address", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> macAddress;
    @XmlElement(name = "Maxima_Velocidad_Bajada")
    protected Integer maximaVelocidadBajada;
    @XmlElement(name = "Maxima_Velocidad_Subida")
    protected Integer maximaVelocidadSubida;
    @XmlElementRef(name = "Nombre_Profile", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> nombreProfile;
    @XmlElement(name = "Numero_Profile")
    protected Integer numeroProfile;
    @XmlElement(name = "PVC_33")
    protected Boolean pvc33;
    @XmlElement(name = "PVC_35")
    protected Boolean pvc35;
    @XmlElementRef(name = "Restrt_Cnt", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> restrtCnt;
    @XmlElementRef(name = "Rinit_ID", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> rinitID;
    @XmlElement(name = "Ruido_Bajada")
    protected Integer ruidoBajada;
    @XmlElement(name = "Ruido_Subida")
    protected Integer ruidoSubida;
    @XmlElement(name = "Senal_Atenuacion_Bajada")
    protected Integer senalAtenuacionBajada;
    @XmlElement(name = "Senal_Atenuacion_Subida")
    protected Integer senalAtenuacionSubida;
    @XmlElementRef(name = "Serial_Modem", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> serialModem;
    @XmlElementRef(name = "Session_Id", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> sessionId;
    @XmlElementRef(name = "Session_Id_IPTV", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> sessionIdIPTV;
    @XmlElementRef(name = "Slot", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> slot;
    @XmlElementRef(name = "Tipo_Tarjeta", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> tipoTarjeta;
    @XmlElementRef(name = "Ultima_Bajada", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ultimaBajada;
    @XmlElementRef(name = "Ultimo_Sincronismo", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ultimoSincronismo;
    @XmlElementRef(name = "User", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> user;
    @XmlElement(name = "VLAN_IPTV")
    protected Boolean vlaniptv;
    @XmlElementRef(name = "VLAN_id", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> vlanId;
    @XmlElement(name = "Velocidad_ADSL")
    protected Integer velocidadADSL;
    @XmlElement(name = "Velocidad_Actual_DN")
    protected Integer velocidadActualDN;
    @XmlElement(name = "Velocidad_Actual_UP")
    protected Integer velocidadActualUP;
    @XmlElement(name = "Velocidad_Configurada_DN")
    protected Integer velocidadConfiguradaDN;
    @XmlElement(name = "Velocidad_Configurada_UP")
    protected Integer velocidadConfiguradaUP;
    @XmlElement(name = "Velocidad_Contratada_DN")
    protected Integer velocidadContratadaDN;
    @XmlElement(name = "Velocidad_Contratada_UP")
    protected Integer velocidadContratadaUP;
    @XmlElement(name = "Velocidad_Puerto_DN")
    protected Integer velocidadPuertoDN;
    @XmlElement(name = "Velocidad_Puerto_UP")
    protected Integer velocidadPuertoUP;
    @XmlElementRef(name = "distancia", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> distancia;
    protected Boolean dividirPorMillar;

    /**
     * Gets the value of the actualType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActualType() {
        return actualType;
    }

    /**
     * Sets the value of the actualType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActualType(JAXBElement<String> value) {
        this.actualType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the availability property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAvailability() {
        return availability;
    }

    /**
     * Sets the value of the availability property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAvailability(JAXBElement<String> value) {
        this.availability = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the cabina property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCabina() {
        return cabina;
    }

    /**
     * Sets the value of the cabina property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCabina(JAXBElement<String> value) {
        this.cabina = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the calidadServicio property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCalidadServicio() {
        return calidadServicio;
    }

    /**
     * Sets the value of the calidadServicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCalidadServicio(JAXBElement<String> value) {
        this.calidadServicio = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the condicionAdministrativa property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCondicionAdministrativa() {
        return condicionAdministrativa;
    }

    /**
     * Sets the value of the condicionAdministrativa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCondicionAdministrativa(Boolean value) {
        this.condicionAdministrativa = value;
    }

    /**
     * Gets the value of the condicionOperativa property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCondicionOperativa() {
        return condicionOperativa;
    }

    /**
     * Sets the value of the condicionOperativa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCondicionOperativa(Boolean value) {
        this.condicionOperativa = value;
    }

    /**
     * Gets the value of the customerID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerID() {
        return customerID;
    }

    /**
     * Sets the value of the customerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerID(JAXBElement<String> value) {
        this.customerID = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the diagnosticoCobre property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DIAGNOSTICO }{@code >}
     *     
     */
    public JAXBElement<DIAGNOSTICO> getDiagnosticoCobre() {
        return diagnosticoCobre;
    }

    /**
     * Sets the value of the diagnosticoCobre property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DIAGNOSTICO }{@code >}
     *     
     */
    public void setDiagnosticoCobre(JAXBElement<DIAGNOSTICO> value) {
        this.diagnosticoCobre = ((JAXBElement<DIAGNOSTICO> ) value);
    }

    /**
     * Gets the value of the distaciaSVDG property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDistaciaSVDG() {
        return distaciaSVDG;
    }

    /**
     * Sets the value of the distaciaSVDG property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDistaciaSVDG(Integer value) {
        this.distaciaSVDG = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the errorStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getErrorStatus() {
        return errorStatus;
    }

    /**
     * Sets the value of the errorStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setErrorStatus(JAXBElement<String> value) {
        this.errorStatus = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the facilida property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FACILITY }{@code >}
     *     
     */
    public JAXBElement<FACILITY> getFacilida() {
        return facilida;
    }

    /**
     * Sets the value of the facilida property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FACILITY }{@code >}
     *     
     */
    public void setFacilida(JAXBElement<FACILITY> value) {
        this.facilida = ((JAXBElement<FACILITY> ) value);
    }

    /**
     * Gets the value of the feeder property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFeeder() {
        return feeder;
    }

    /**
     * Sets the value of the feeder property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFeeder(JAXBElement<String> value) {
        this.feeder = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setID(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the idfacilidades property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIDFACILIDADES() {
        return idfacilidades;
    }

    /**
     * Sets the value of the idfacilidades property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIDFACILIDADES(Integer value) {
        this.idfacilidades = value;
    }

    /**
     * Gets the value of the lineaDefectuosa property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLineaDefectuosa() {
        return lineaDefectuosa;
    }

    /**
     * Sets the value of the lineaDefectuosa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLineaDefectuosa(Boolean value) {
        this.lineaDefectuosa = value;
    }

    /**
     * Gets the value of the loopAtenuacionBajada property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLoopAtenuacionBajada() {
        return loopAtenuacionBajada;
    }

    /**
     * Sets the value of the loopAtenuacionBajada property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLoopAtenuacionBajada(Integer value) {
        this.loopAtenuacionBajada = value;
    }

    /**
     * Gets the value of the loopAtenuacionSubida property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLoopAtenuacionSubida() {
        return loopAtenuacionSubida;
    }

    /**
     * Sets the value of the loopAtenuacionSubida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLoopAtenuacionSubida(Integer value) {
        this.loopAtenuacionSubida = value;
    }

    /**
     * Gets the value of the macAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMACAddress() {
        return macAddress;
    }

    /**
     * Sets the value of the macAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMACAddress(JAXBElement<String> value) {
        this.macAddress = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the maximaVelocidadBajada property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximaVelocidadBajada() {
        return maximaVelocidadBajada;
    }

    /**
     * Sets the value of the maximaVelocidadBajada property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximaVelocidadBajada(Integer value) {
        this.maximaVelocidadBajada = value;
    }

    /**
     * Gets the value of the maximaVelocidadSubida property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximaVelocidadSubida() {
        return maximaVelocidadSubida;
    }

    /**
     * Sets the value of the maximaVelocidadSubida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximaVelocidadSubida(Integer value) {
        this.maximaVelocidadSubida = value;
    }

    /**
     * Gets the value of the nombreProfile property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNombreProfile() {
        return nombreProfile;
    }

    /**
     * Sets the value of the nombreProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNombreProfile(JAXBElement<String> value) {
        this.nombreProfile = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the numeroProfile property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroProfile() {
        return numeroProfile;
    }

    /**
     * Sets the value of the numeroProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroProfile(Integer value) {
        this.numeroProfile = value;
    }

    /**
     * Gets the value of the pvc33 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPVC33() {
        return pvc33;
    }

    /**
     * Sets the value of the pvc33 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPVC33(Boolean value) {
        this.pvc33 = value;
    }

    /**
     * Gets the value of the pvc35 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPVC35() {
        return pvc35;
    }

    /**
     * Sets the value of the pvc35 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPVC35(Boolean value) {
        this.pvc35 = value;
    }

    /**
     * Gets the value of the restrtCnt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRestrtCnt() {
        return restrtCnt;
    }

    /**
     * Sets the value of the restrtCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRestrtCnt(JAXBElement<String> value) {
        this.restrtCnt = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the rinitID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRinitID() {
        return rinitID;
    }

    /**
     * Sets the value of the rinitID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRinitID(JAXBElement<String> value) {
        this.rinitID = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ruidoBajada property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRuidoBajada() {
        return ruidoBajada;
    }

    /**
     * Sets the value of the ruidoBajada property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRuidoBajada(Integer value) {
        this.ruidoBajada = value;
    }

    /**
     * Gets the value of the ruidoSubida property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRuidoSubida() {
        return ruidoSubida;
    }

    /**
     * Sets the value of the ruidoSubida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRuidoSubida(Integer value) {
        this.ruidoSubida = value;
    }

    /**
     * Gets the value of the senalAtenuacionBajada property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSenalAtenuacionBajada() {
        return senalAtenuacionBajada;
    }

    /**
     * Sets the value of the senalAtenuacionBajada property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSenalAtenuacionBajada(Integer value) {
        this.senalAtenuacionBajada = value;
    }

    /**
     * Gets the value of the senalAtenuacionSubida property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSenalAtenuacionSubida() {
        return senalAtenuacionSubida;
    }

    /**
     * Sets the value of the senalAtenuacionSubida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSenalAtenuacionSubida(Integer value) {
        this.senalAtenuacionSubida = value;
    }

    /**
     * Gets the value of the serialModem property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSerialModem() {
        return serialModem;
    }

    /**
     * Sets the value of the serialModem property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSerialModem(JAXBElement<String> value) {
        this.serialModem = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the sessionId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSessionId() {
        return sessionId;
    }

    /**
     * Sets the value of the sessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSessionId(JAXBElement<String> value) {
        this.sessionId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the sessionIdIPTV property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSessionIdIPTV() {
        return sessionIdIPTV;
    }

    /**
     * Sets the value of the sessionIdIPTV property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSessionIdIPTV(JAXBElement<String> value) {
        this.sessionIdIPTV = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the slot property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSlot() {
        return slot;
    }

    /**
     * Sets the value of the slot property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSlot(JAXBElement<String> value) {
        this.slot = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the tipoTarjeta property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipoTarjeta() {
        return tipoTarjeta;
    }

    /**
     * Sets the value of the tipoTarjeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipoTarjeta(JAXBElement<String> value) {
        this.tipoTarjeta = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ultimaBajada property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUltimaBajada() {
        return ultimaBajada;
    }

    /**
     * Sets the value of the ultimaBajada property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUltimaBajada(JAXBElement<String> value) {
        this.ultimaBajada = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ultimoSincronismo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUltimoSincronismo() {
        return ultimoSincronismo;
    }

    /**
     * Sets the value of the ultimoSincronismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUltimoSincronismo(JAXBElement<String> value) {
        this.ultimoSincronismo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUser(JAXBElement<String> value) {
        this.user = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the vlaniptv property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVLANIPTV() {
        return vlaniptv;
    }

    /**
     * Sets the value of the vlaniptv property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVLANIPTV(Boolean value) {
        this.vlaniptv = value;
    }

    /**
     * Gets the value of the vlanId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVLANId() {
        return vlanId;
    }

    /**
     * Sets the value of the vlanId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVLANId(JAXBElement<String> value) {
        this.vlanId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the velocidadADSL property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadADSL() {
        return velocidadADSL;
    }

    /**
     * Sets the value of the velocidadADSL property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadADSL(Integer value) {
        this.velocidadADSL = value;
    }

    /**
     * Gets the value of the velocidadActualDN property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadActualDN() {
        return velocidadActualDN;
    }

    /**
     * Sets the value of the velocidadActualDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadActualDN(Integer value) {
        this.velocidadActualDN = value;
    }

    /**
     * Gets the value of the velocidadActualUP property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadActualUP() {
        return velocidadActualUP;
    }

    /**
     * Sets the value of the velocidadActualUP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadActualUP(Integer value) {
        this.velocidadActualUP = value;
    }

    /**
     * Gets the value of the velocidadConfiguradaDN property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadConfiguradaDN() {
        return velocidadConfiguradaDN;
    }

    /**
     * Sets the value of the velocidadConfiguradaDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadConfiguradaDN(Integer value) {
        this.velocidadConfiguradaDN = value;
    }

    /**
     * Gets the value of the velocidadConfiguradaUP property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadConfiguradaUP() {
        return velocidadConfiguradaUP;
    }

    /**
     * Sets the value of the velocidadConfiguradaUP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadConfiguradaUP(Integer value) {
        this.velocidadConfiguradaUP = value;
    }

    /**
     * Gets the value of the velocidadContratadaDN property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadContratadaDN() {
        return velocidadContratadaDN;
    }

    /**
     * Sets the value of the velocidadContratadaDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadContratadaDN(Integer value) {
        this.velocidadContratadaDN = value;
    }

    /**
     * Gets the value of the velocidadContratadaUP property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadContratadaUP() {
        return velocidadContratadaUP;
    }

    /**
     * Sets the value of the velocidadContratadaUP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadContratadaUP(Integer value) {
        this.velocidadContratadaUP = value;
    }

    /**
     * Gets the value of the velocidadPuertoDN property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadPuertoDN() {
        return velocidadPuertoDN;
    }

    /**
     * Sets the value of the velocidadPuertoDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadPuertoDN(Integer value) {
        this.velocidadPuertoDN = value;
    }

    /**
     * Gets the value of the velocidadPuertoUP property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadPuertoUP() {
        return velocidadPuertoUP;
    }

    /**
     * Sets the value of the velocidadPuertoUP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadPuertoUP(Integer value) {
        this.velocidadPuertoUP = value;
    }

    /**
     * Gets the value of the distancia property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDistancia() {
        return distancia;
    }

    /**
     * Sets the value of the distancia property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDistancia(JAXBElement<String> value) {
        this.distancia = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the dividirPorMillar property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDividirPorMillar() {
        return dividirPorMillar;
    }

    /**
     * Sets the value of the dividirPorMillar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDividirPorMillar(Boolean value) {
        this.dividirPorMillar = value;
    }

}
