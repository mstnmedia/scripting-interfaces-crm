
package org.datacontract.schemas._2004._07.claro_dslam_interface;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FACILITY complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FACILITY">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CIRCUIT_DESIGN_ID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="CIRCUIT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CLLI_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EQUIPMENT_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FACILITY_COUNT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IP_ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NODE_ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NODE_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PUERTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REFERENCIA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TELEFONO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TIPO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VENDOR_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isCobre" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FACILITY", propOrder = {
    "circuitdesignid",
    "circuitid",
    "cllicode",
    "equipmentname",
    "facilitycount",
    "id",
    "ipaddress",
    "nodeaddress",
    "nodename",
    "puerto",
    "referencia",
    "status",
    "telefono",
    "tipo",
    "type",
    "vendorname",
    "isCobre"
})
public class FACILITY {

    @XmlElement(name = "CIRCUIT_DESIGN_ID")
    protected Long circuitdesignid;
    @XmlElementRef(name = "CIRCUIT_ID", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> circuitid;
    @XmlElementRef(name = "CLLI_CODE", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> cllicode;
    @XmlElementRef(name = "EQUIPMENT_NAME", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> equipmentname;
    @XmlElement(name = "FACILITY_COUNT")
    protected Integer facilitycount;
    @XmlElement(name = "ID")
    protected Integer id;
    @XmlElementRef(name = "IP_ADDRESS", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ipaddress;
    @XmlElementRef(name = "NODE_ADDRESS", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> nodeaddress;
    @XmlElementRef(name = "NODE_NAME", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> nodename;
    @XmlElementRef(name = "PUERTO", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> puerto;
    @XmlElementRef(name = "REFERENCIA", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> referencia;
    @XmlElementRef(name = "STATUS", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> status;
    @XmlElementRef(name = "TELEFONO", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> telefono;
    @XmlElementRef(name = "TIPO", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> tipo;
    @XmlElementRef(name = "TYPE", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> type;
    @XmlElementRef(name = "VENDOR_NAME", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> vendorname;
    protected Boolean isCobre;

    /**
     * Gets the value of the circuitdesignid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCIRCUITDESIGNID() {
        return circuitdesignid;
    }

    /**
     * Sets the value of the circuitdesignid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCIRCUITDESIGNID(Long value) {
        this.circuitdesignid = value;
    }

    /**
     * Gets the value of the circuitid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCIRCUITID() {
        return circuitid;
    }

    /**
     * Sets the value of the circuitid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCIRCUITID(JAXBElement<String> value) {
        this.circuitid = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the cllicode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCLLICODE() {
        return cllicode;
    }

    /**
     * Sets the value of the cllicode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCLLICODE(JAXBElement<String> value) {
        this.cllicode = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the equipmentname property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEQUIPMENTNAME() {
        return equipmentname;
    }

    /**
     * Sets the value of the equipmentname property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEQUIPMENTNAME(JAXBElement<String> value) {
        this.equipmentname = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the facilitycount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFACILITYCOUNT() {
        return facilitycount;
    }

    /**
     * Sets the value of the facilitycount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFACILITYCOUNT(Integer value) {
        this.facilitycount = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setID(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the ipaddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIPADDRESS() {
        return ipaddress;
    }

    /**
     * Sets the value of the ipaddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIPADDRESS(JAXBElement<String> value) {
        this.ipaddress = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the nodeaddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNODEADDRESS() {
        return nodeaddress;
    }

    /**
     * Sets the value of the nodeaddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNODEADDRESS(JAXBElement<String> value) {
        this.nodeaddress = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the nodename property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNODENAME() {
        return nodename;
    }

    /**
     * Sets the value of the nodename property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNODENAME(JAXBElement<String> value) {
        this.nodename = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the puerto property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPUERTO() {
        return puerto;
    }

    /**
     * Sets the value of the puerto property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPUERTO(JAXBElement<String> value) {
        this.puerto = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the referencia property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getREFERENCIA() {
        return referencia;
    }

    /**
     * Sets the value of the referencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setREFERENCIA(JAXBElement<String> value) {
        this.referencia = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSTATUS() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSTATUS(JAXBElement<String> value) {
        this.status = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the telefono property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTELEFONO() {
        return telefono;
    }

    /**
     * Sets the value of the telefono property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTELEFONO(JAXBElement<String> value) {
        this.telefono = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the tipo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTIPO() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTIPO(JAXBElement<String> value) {
        this.tipo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTYPE() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTYPE(JAXBElement<String> value) {
        this.type = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the vendorname property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVENDORNAME() {
        return vendorname;
    }

    /**
     * Sets the value of the vendorname property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVENDORNAME(JAXBElement<String> value) {
        this.vendorname = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the isCobre property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCobre() {
        return isCobre;
    }

    /**
     * Sets the value of the isCobre property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCobre(Boolean value) {
        this.isCobre = value;
    }

}
