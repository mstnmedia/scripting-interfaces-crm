
package org.datacontract.schemas._2004._07.claro_dslam_interface;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.claro_sacs_interface.Facilidades;


/**
 * <p>Java class for GPON_INFO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GPON_INFO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrentThroughput" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataUsageDS" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DataUsageUS" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DiagnosticoGpon" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}DIAGNOSTICO_GPON" minOccurs="0"/>
 *         &lt;element name="Facilida" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}FACILITY" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ID_FACILIDADES" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OLTAdmState" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OLTFirmware" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OLTOperState" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OLTPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OLTPortRxPower" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="OLTPortTxPower" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="OLTRxONTOpticalPower" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="OLTUptime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTAdmState" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ONTBatteryStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTBiasCurrent" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ONTCPUUsage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTDistancia" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ONTLastDown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTLastDownCause" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTLastDownTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTLineProfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTMemUsage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTOnlineDuration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTOperState" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ONTRxPower" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="ONTSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTSoftwareVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTTemp" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ONTTxPower" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="ONTUpTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTVendor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTVoltage" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="OntProductDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Service_Port" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}ArrayOfSERVICE_PORT" minOccurs="0"/>
 *         &lt;element name="Velocidad_Configurada_DN" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="Velocidad_Configurada_UP" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="Velocidad_Contratada_DN" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Velocidad_Contratada_UP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="facilidades" type="{http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model}Facilidades" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GPON_INFO", propOrder = {
    "currentThroughput",
    "dataUsageDS",
    "dataUsageUS",
    "diagnosticoGpon",
    "facilida",
    "id",
    "idfacilidades",
    "oltAdmState",
    "oltFirmware",
    "oltOperState",
    "oltPassword",
    "oltPortRxPower",
    "oltPortTxPower",
    "oltRxONTOpticalPower",
    "oltUptime",
    "ontAdmState",
    "ontBatteryStatus",
    "ontBiasCurrent",
    "ontcpuUsage",
    "ontDescription",
    "ontDistancia",
    "ontLastDown",
    "ontLastDownCause",
    "ontLastDownTime",
    "ontLineProfile",
    "ontMemUsage",
    "ontOnlineDuration",
    "ontOperState",
    "ontRxPower",
    "ontSerialNumber",
    "ontSoftwareVersion",
    "ontTemp",
    "ontTxPower",
    "ontUpTime",
    "ontVendor",
    "ontVersion",
    "ontVoltage",
    "ontProductDescription",
    "servicePort",
    "velocidadConfiguradaDN",
    "velocidadConfiguradaUP",
    "velocidadContratadaDN",
    "velocidadContratadaUP",
    "facilidades"
})
public class GPONINFO {

    @XmlElementRef(name = "CurrentThroughput", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> currentThroughput;
    @XmlElement(name = "DataUsageDS")
    protected Integer dataUsageDS;
    @XmlElement(name = "DataUsageUS")
    protected Integer dataUsageUS;
    @XmlElementRef(name = "DiagnosticoGpon", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<DIAGNOSTICOGPON> diagnosticoGpon;
    @XmlElementRef(name = "Facilida", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<FACILITY> facilida;
    @XmlElement(name = "ID")
    protected Integer id;
    @XmlElement(name = "ID_FACILIDADES")
    protected Integer idfacilidades;
    @XmlElement(name = "OLTAdmState")
    protected Boolean oltAdmState;
    @XmlElementRef(name = "OLTFirmware", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> oltFirmware;
    @XmlElement(name = "OLTOperState")
    protected Boolean oltOperState;
    @XmlElementRef(name = "OLTPassword", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> oltPassword;
    @XmlElement(name = "OLTPortRxPower")
    protected Double oltPortRxPower;
    @XmlElement(name = "OLTPortTxPower")
    protected Double oltPortTxPower;
    @XmlElement(name = "OLTRxONTOpticalPower")
    protected Double oltRxONTOpticalPower;
    @XmlElementRef(name = "OLTUptime", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> oltUptime;
    @XmlElement(name = "ONTAdmState")
    protected Boolean ontAdmState;
    @XmlElementRef(name = "ONTBatteryStatus", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontBatteryStatus;
    @XmlElement(name = "ONTBiasCurrent")
    protected Integer ontBiasCurrent;
    @XmlElementRef(name = "ONTCPUUsage", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontcpuUsage;
    @XmlElementRef(name = "ONTDescription", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontDescription;
    @XmlElement(name = "ONTDistancia")
    protected Integer ontDistancia;
    @XmlElementRef(name = "ONTLastDown", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontLastDown;
    @XmlElementRef(name = "ONTLastDownCause", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontLastDownCause;
    @XmlElementRef(name = "ONTLastDownTime", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontLastDownTime;
    @XmlElementRef(name = "ONTLineProfile", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontLineProfile;
    @XmlElementRef(name = "ONTMemUsage", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontMemUsage;
    @XmlElementRef(name = "ONTOnlineDuration", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontOnlineDuration;
    @XmlElement(name = "ONTOperState")
    protected Boolean ontOperState;
    @XmlElement(name = "ONTRxPower")
    protected Double ontRxPower;
    @XmlElementRef(name = "ONTSerialNumber", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontSerialNumber;
    @XmlElementRef(name = "ONTSoftwareVersion", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontSoftwareVersion;
    @XmlElement(name = "ONTTemp")
    protected Integer ontTemp;
    @XmlElement(name = "ONTTxPower")
    protected Double ontTxPower;
    @XmlElementRef(name = "ONTUpTime", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontUpTime;
    @XmlElementRef(name = "ONTVendor", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontVendor;
    @XmlElementRef(name = "ONTVersion", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontVersion;
    @XmlElement(name = "ONTVoltage")
    protected Double ontVoltage;
    @XmlElementRef(name = "OntProductDescription", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontProductDescription;
    @XmlElementRef(name = "Service_Port", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<ArrayOfSERVICEPORT> servicePort;
    @XmlElement(name = "Velocidad_Configurada_DN")
    protected Double velocidadConfiguradaDN;
    @XmlElement(name = "Velocidad_Configurada_UP")
    protected Double velocidadConfiguradaUP;
    @XmlElement(name = "Velocidad_Contratada_DN")
    protected Integer velocidadContratadaDN;
    @XmlElement(name = "Velocidad_Contratada_UP")
    protected Integer velocidadContratadaUP;
    @XmlElementRef(name = "facilidades", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<Facilidades> facilidades;

    /**
     * Gets the value of the currentThroughput property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrentThroughput() {
        return currentThroughput;
    }

    /**
     * Sets the value of the currentThroughput property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrentThroughput(JAXBElement<String> value) {
        this.currentThroughput = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the dataUsageDS property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDataUsageDS() {
        return dataUsageDS;
    }

    /**
     * Sets the value of the dataUsageDS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDataUsageDS(Integer value) {
        this.dataUsageDS = value;
    }

    /**
     * Gets the value of the dataUsageUS property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDataUsageUS() {
        return dataUsageUS;
    }

    /**
     * Sets the value of the dataUsageUS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDataUsageUS(Integer value) {
        this.dataUsageUS = value;
    }

    /**
     * Gets the value of the diagnosticoGpon property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DIAGNOSTICOGPON }{@code >}
     *     
     */
    public JAXBElement<DIAGNOSTICOGPON> getDiagnosticoGpon() {
        return diagnosticoGpon;
    }

    /**
     * Sets the value of the diagnosticoGpon property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DIAGNOSTICOGPON }{@code >}
     *     
     */
    public void setDiagnosticoGpon(JAXBElement<DIAGNOSTICOGPON> value) {
        this.diagnosticoGpon = ((JAXBElement<DIAGNOSTICOGPON> ) value);
    }

    /**
     * Gets the value of the facilida property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FACILITY }{@code >}
     *     
     */
    public JAXBElement<FACILITY> getFacilida() {
        return facilida;
    }

    /**
     * Sets the value of the facilida property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FACILITY }{@code >}
     *     
     */
    public void setFacilida(JAXBElement<FACILITY> value) {
        this.facilida = ((JAXBElement<FACILITY> ) value);
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setID(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the idfacilidades property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIDFACILIDADES() {
        return idfacilidades;
    }

    /**
     * Sets the value of the idfacilidades property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIDFACILIDADES(Integer value) {
        this.idfacilidades = value;
    }

    /**
     * Gets the value of the oltAdmState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOLTAdmState() {
        return oltAdmState;
    }

    /**
     * Sets the value of the oltAdmState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOLTAdmState(Boolean value) {
        this.oltAdmState = value;
    }

    /**
     * Gets the value of the oltFirmware property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOLTFirmware() {
        return oltFirmware;
    }

    /**
     * Sets the value of the oltFirmware property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOLTFirmware(JAXBElement<String> value) {
        this.oltFirmware = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the oltOperState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOLTOperState() {
        return oltOperState;
    }

    /**
     * Sets the value of the oltOperState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOLTOperState(Boolean value) {
        this.oltOperState = value;
    }

    /**
     * Gets the value of the oltPassword property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOLTPassword() {
        return oltPassword;
    }

    /**
     * Sets the value of the oltPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOLTPassword(JAXBElement<String> value) {
        this.oltPassword = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the oltPortRxPower property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getOLTPortRxPower() {
        return oltPortRxPower;
    }

    /**
     * Sets the value of the oltPortRxPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setOLTPortRxPower(Double value) {
        this.oltPortRxPower = value;
    }

    /**
     * Gets the value of the oltPortTxPower property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getOLTPortTxPower() {
        return oltPortTxPower;
    }

    /**
     * Sets the value of the oltPortTxPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setOLTPortTxPower(Double value) {
        this.oltPortTxPower = value;
    }

    /**
     * Gets the value of the oltRxONTOpticalPower property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getOLTRxONTOpticalPower() {
        return oltRxONTOpticalPower;
    }

    /**
     * Sets the value of the oltRxONTOpticalPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setOLTRxONTOpticalPower(Double value) {
        this.oltRxONTOpticalPower = value;
    }

    /**
     * Gets the value of the oltUptime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOLTUptime() {
        return oltUptime;
    }

    /**
     * Sets the value of the oltUptime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOLTUptime(JAXBElement<String> value) {
        this.oltUptime = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontAdmState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isONTAdmState() {
        return ontAdmState;
    }

    /**
     * Sets the value of the ontAdmState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setONTAdmState(Boolean value) {
        this.ontAdmState = value;
    }

    /**
     * Gets the value of the ontBatteryStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTBatteryStatus() {
        return ontBatteryStatus;
    }

    /**
     * Sets the value of the ontBatteryStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTBatteryStatus(JAXBElement<String> value) {
        this.ontBatteryStatus = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontBiasCurrent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getONTBiasCurrent() {
        return ontBiasCurrent;
    }

    /**
     * Sets the value of the ontBiasCurrent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setONTBiasCurrent(Integer value) {
        this.ontBiasCurrent = value;
    }

    /**
     * Gets the value of the ontcpuUsage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTCPUUsage() {
        return ontcpuUsage;
    }

    /**
     * Sets the value of the ontcpuUsage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTCPUUsage(JAXBElement<String> value) {
        this.ontcpuUsage = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTDescription() {
        return ontDescription;
    }

    /**
     * Sets the value of the ontDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTDescription(JAXBElement<String> value) {
        this.ontDescription = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontDistancia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getONTDistancia() {
        return ontDistancia;
    }

    /**
     * Sets the value of the ontDistancia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setONTDistancia(Integer value) {
        this.ontDistancia = value;
    }

    /**
     * Gets the value of the ontLastDown property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTLastDown() {
        return ontLastDown;
    }

    /**
     * Sets the value of the ontLastDown property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTLastDown(JAXBElement<String> value) {
        this.ontLastDown = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontLastDownCause property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTLastDownCause() {
        return ontLastDownCause;
    }

    /**
     * Sets the value of the ontLastDownCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTLastDownCause(JAXBElement<String> value) {
        this.ontLastDownCause = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontLastDownTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTLastDownTime() {
        return ontLastDownTime;
    }

    /**
     * Sets the value of the ontLastDownTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTLastDownTime(JAXBElement<String> value) {
        this.ontLastDownTime = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontLineProfile property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTLineProfile() {
        return ontLineProfile;
    }

    /**
     * Sets the value of the ontLineProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTLineProfile(JAXBElement<String> value) {
        this.ontLineProfile = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontMemUsage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTMemUsage() {
        return ontMemUsage;
    }

    /**
     * Sets the value of the ontMemUsage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTMemUsage(JAXBElement<String> value) {
        this.ontMemUsage = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontOnlineDuration property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTOnlineDuration() {
        return ontOnlineDuration;
    }

    /**
     * Sets the value of the ontOnlineDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTOnlineDuration(JAXBElement<String> value) {
        this.ontOnlineDuration = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontOperState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isONTOperState() {
        return ontOperState;
    }

    /**
     * Sets the value of the ontOperState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setONTOperState(Boolean value) {
        this.ontOperState = value;
    }

    /**
     * Gets the value of the ontRxPower property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getONTRxPower() {
        return ontRxPower;
    }

    /**
     * Sets the value of the ontRxPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setONTRxPower(Double value) {
        this.ontRxPower = value;
    }

    /**
     * Gets the value of the ontSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTSerialNumber() {
        return ontSerialNumber;
    }

    /**
     * Sets the value of the ontSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTSerialNumber(JAXBElement<String> value) {
        this.ontSerialNumber = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontSoftwareVersion property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTSoftwareVersion() {
        return ontSoftwareVersion;
    }

    /**
     * Sets the value of the ontSoftwareVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTSoftwareVersion(JAXBElement<String> value) {
        this.ontSoftwareVersion = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontTemp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getONTTemp() {
        return ontTemp;
    }

    /**
     * Sets the value of the ontTemp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setONTTemp(Integer value) {
        this.ontTemp = value;
    }

    /**
     * Gets the value of the ontTxPower property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getONTTxPower() {
        return ontTxPower;
    }

    /**
     * Sets the value of the ontTxPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setONTTxPower(Double value) {
        this.ontTxPower = value;
    }

    /**
     * Gets the value of the ontUpTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTUpTime() {
        return ontUpTime;
    }

    /**
     * Sets the value of the ontUpTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTUpTime(JAXBElement<String> value) {
        this.ontUpTime = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontVendor property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTVendor() {
        return ontVendor;
    }

    /**
     * Sets the value of the ontVendor property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTVendor(JAXBElement<String> value) {
        this.ontVendor = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontVersion property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTVersion() {
        return ontVersion;
    }

    /**
     * Sets the value of the ontVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTVersion(JAXBElement<String> value) {
        this.ontVersion = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontVoltage property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getONTVoltage() {
        return ontVoltage;
    }

    /**
     * Sets the value of the ontVoltage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setONTVoltage(Double value) {
        this.ontVoltage = value;
    }

    /**
     * Gets the value of the ontProductDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOntProductDescription() {
        return ontProductDescription;
    }

    /**
     * Sets the value of the ontProductDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOntProductDescription(JAXBElement<String> value) {
        this.ontProductDescription = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the servicePort property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSERVICEPORT }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSERVICEPORT> getServicePort() {
        return servicePort;
    }

    /**
     * Sets the value of the servicePort property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSERVICEPORT }{@code >}
     *     
     */
    public void setServicePort(JAXBElement<ArrayOfSERVICEPORT> value) {
        this.servicePort = ((JAXBElement<ArrayOfSERVICEPORT> ) value);
    }

    /**
     * Gets the value of the velocidadConfiguradaDN property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getVelocidadConfiguradaDN() {
        return velocidadConfiguradaDN;
    }

    /**
     * Sets the value of the velocidadConfiguradaDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setVelocidadConfiguradaDN(Double value) {
        this.velocidadConfiguradaDN = value;
    }

    /**
     * Gets the value of the velocidadConfiguradaUP property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getVelocidadConfiguradaUP() {
        return velocidadConfiguradaUP;
    }

    /**
     * Sets the value of the velocidadConfiguradaUP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setVelocidadConfiguradaUP(Double value) {
        this.velocidadConfiguradaUP = value;
    }

    /**
     * Gets the value of the velocidadContratadaDN property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadContratadaDN() {
        return velocidadContratadaDN;
    }

    /**
     * Sets the value of the velocidadContratadaDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadContratadaDN(Integer value) {
        this.velocidadContratadaDN = value;
    }

    /**
     * Gets the value of the velocidadContratadaUP property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVelocidadContratadaUP() {
        return velocidadContratadaUP;
    }

    /**
     * Sets the value of the velocidadContratadaUP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVelocidadContratadaUP(Integer value) {
        this.velocidadContratadaUP = value;
    }

    /**
     * Gets the value of the facilidades property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Facilidades }{@code >}
     *     
     */
    public JAXBElement<Facilidades> getFacilidades() {
        return facilidades;
    }

    /**
     * Sets the value of the facilidades property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Facilidades }{@code >}
     *     
     */
    public void setFacilidades(JAXBElement<Facilidades> value) {
        this.facilidades = ((JAXBElement<Facilidades> ) value);
    }

}
