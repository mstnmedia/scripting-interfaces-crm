
package org.datacontract.schemas._2004._07.claro_sacs_interface;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.claro_sacs_interface package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Facilidades_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "Facilidades");
    private final static QName _FacilidadesType_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "Type");
    private final static QName _FacilidadesTipo_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "Tipo");
    private final static QName _FacilidadesCircuitDesignId_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "CircuitDesignId");
    private final static QName _FacilidadesClliCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "ClliCode");
    private final static QName _FacilidadesTelefono_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "Telefono");
    private final static QName _FacilidadesFecha_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "Fecha");
    private final static QName _FacilidadesNodeAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "NodeAddress");
    private final static QName _FacilidadesEquipmentName_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "EquipmentName");
    private final static QName _FacilidadesCircuitId_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "CircuitId");
    private final static QName _FacilidadesTipoReferencia_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "TipoReferencia");
    private final static QName _FacilidadesVendorName_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "VendorName");
    private final static QName _FacilidadesUsuario_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "Usuario");
    private final static QName _FacilidadesStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "Status");
    private final static QName _FacilidadesNodeName_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "NodeName");
    private final static QName _FacilidadesReferencia_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", "Referencia");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.claro_sacs_interface
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Facilidades }
     * 
     */
    public Facilidades createFacilidades() {
        return new Facilidades();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facilidades }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "Facilidades")
    public JAXBElement<Facilidades> createFacilidades(Facilidades value) {
        return new JAXBElement<Facilidades>(_Facilidades_QNAME, Facilidades.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "Type", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesType(String value) {
        return new JAXBElement<String>(_FacilidadesType_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "Tipo", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesTipo(String value) {
        return new JAXBElement<String>(_FacilidadesTipo_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "CircuitDesignId", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesCircuitDesignId(String value) {
        return new JAXBElement<String>(_FacilidadesCircuitDesignId_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "ClliCode", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesClliCode(String value) {
        return new JAXBElement<String>(_FacilidadesClliCode_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "Telefono", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesTelefono(String value) {
        return new JAXBElement<String>(_FacilidadesTelefono_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "Fecha", scope = Facilidades.class)
    public JAXBElement<XMLGregorianCalendar> createFacilidadesFecha(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FacilidadesFecha_QNAME, XMLGregorianCalendar.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "NodeAddress", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesNodeAddress(String value) {
        return new JAXBElement<String>(_FacilidadesNodeAddress_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "EquipmentName", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesEquipmentName(String value) {
        return new JAXBElement<String>(_FacilidadesEquipmentName_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "CircuitId", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesCircuitId(String value) {
        return new JAXBElement<String>(_FacilidadesCircuitId_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "TipoReferencia", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesTipoReferencia(String value) {
        return new JAXBElement<String>(_FacilidadesTipoReferencia_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "VendorName", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesVendorName(String value) {
        return new JAXBElement<String>(_FacilidadesVendorName_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "Usuario", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesUsuario(String value) {
        return new JAXBElement<String>(_FacilidadesUsuario_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "Status", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesStatus(String value) {
        return new JAXBElement<String>(_FacilidadesStatus_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "NodeName", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesNodeName(String value) {
        return new JAXBElement<String>(_FacilidadesNodeName_QNAME, String.class, Facilidades.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", name = "Referencia", scope = Facilidades.class)
    public JAXBElement<String> createFacilidadesReferencia(String value) {
        return new JAXBElement<String>(_FacilidadesReferencia_QNAME, String.class, Facilidades.class, value);
    }

}
