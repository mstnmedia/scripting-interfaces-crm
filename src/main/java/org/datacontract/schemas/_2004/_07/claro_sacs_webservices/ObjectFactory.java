
package org.datacontract.schemas._2004._07.claro_sacs_webservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.datacontract.schemas._2004._07.claro_dslam_interface.DIAGNOSTICO;
import org.datacontract.schemas._2004._07.claro_dslam_interface.DIAGNOSTICOGPON;
import org.datacontract.schemas._2004._07.claro_dslam_interface.DSLAMINFO;
import org.datacontract.schemas._2004._07.claro_dslam_interface.FACILITY;
import org.datacontract.schemas._2004._07.claro_dslam_interface.GPONINFO;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.claro_sacs_webservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DiagnosticResult_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", "DiagnosticResult");
    private final static QName _DiagnosticResultMessege_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", "messege");
    private final static QName _DiagnosticResultDiagnostico_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", "diagnostico");
    private final static QName _DiagnosticResultFacilidades_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", "facilidades");
    private final static QName _DiagnosticResultDiagnosticoGpon_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", "diagnosticoGpon");
    private final static QName _DiagnosticResultDslamInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", "dslamInfo");
    private final static QName _DiagnosticResultGponInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", "gponInfo");
    private final static QName _DiagnosticResultTestResult_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", "testResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.claro_sacs_webservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DiagnosticResult }
     * 
     */
    public DiagnosticResult createDiagnosticResult() {
        return new DiagnosticResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DiagnosticResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", name = "DiagnosticResult")
    public JAXBElement<DiagnosticResult> createDiagnosticResult(DiagnosticResult value) {
        return new JAXBElement<DiagnosticResult>(_DiagnosticResult_QNAME, DiagnosticResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", name = "messege", scope = DiagnosticResult.class)
    public JAXBElement<String> createDiagnosticResultMessege(String value) {
        return new JAXBElement<String>(_DiagnosticResultMessege_QNAME, String.class, DiagnosticResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DIAGNOSTICO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", name = "diagnostico", scope = DiagnosticResult.class)
    public JAXBElement<DIAGNOSTICO> createDiagnosticResultDiagnostico(DIAGNOSTICO value) {
        return new JAXBElement<DIAGNOSTICO>(_DiagnosticResultDiagnostico_QNAME, DIAGNOSTICO.class, DiagnosticResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FACILITY }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", name = "facilidades", scope = DiagnosticResult.class)
    public JAXBElement<FACILITY> createDiagnosticResultFacilidades(FACILITY value) {
        return new JAXBElement<FACILITY>(_DiagnosticResultFacilidades_QNAME, FACILITY.class, DiagnosticResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DIAGNOSTICOGPON }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", name = "diagnosticoGpon", scope = DiagnosticResult.class)
    public JAXBElement<DIAGNOSTICOGPON> createDiagnosticResultDiagnosticoGpon(DIAGNOSTICOGPON value) {
        return new JAXBElement<DIAGNOSTICOGPON>(_DiagnosticResultDiagnosticoGpon_QNAME, DIAGNOSTICOGPON.class, DiagnosticResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DSLAMINFO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", name = "dslamInfo", scope = DiagnosticResult.class)
    public JAXBElement<DSLAMINFO> createDiagnosticResultDslamInfo(DSLAMINFO value) {
        return new JAXBElement<DSLAMINFO>(_DiagnosticResultDslamInfo_QNAME, DSLAMINFO.class, DiagnosticResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GPONINFO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", name = "gponInfo", scope = DiagnosticResult.class)
    public JAXBElement<GPONINFO> createDiagnosticResultGponInfo(GPONINFO value) {
        return new JAXBElement<GPONINFO>(_DiagnosticResultGponInfo_QNAME, GPONINFO.class, DiagnosticResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.WEBSERVICES.Model", name = "testResult", scope = DiagnosticResult.class)
    public JAXBElement<String> createDiagnosticResultTestResult(String value) {
        return new JAXBElement<String>(_DiagnosticResultTestResult_QNAME, String.class, DiagnosticResult.class, value);
    }

}
