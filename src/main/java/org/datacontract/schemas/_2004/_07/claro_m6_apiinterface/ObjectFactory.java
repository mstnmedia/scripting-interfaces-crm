
package org.datacontract.schemas._2004._07.claro_m6_apiinterface;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.claro_m6_apiinterface package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DSLAMFACILITY_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "DSLAMFACILITY");
    private final static QName _ArrayOfDSLAMFACILITY_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "ArrayOfDSLAMFACILITY");
    private final static QName _DSLAMFACILITYCIRCUITID_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "CIRCUIT_ID");
    private final static QName _DSLAMFACILITYSTATUS_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "STATUS");
    private final static QName _DSLAMFACILITYNODENAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "NODE_NAME");
    private final static QName _DSLAMFACILITYVENDORNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "VENDOR_NAME");
    private final static QName _DSLAMFACILITYCLLICODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "CLLI_CODE");
    private final static QName _DSLAMFACILITYNODEADDRESS_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "NODE_ADDRESS");
    private final static QName _DSLAMFACILITYTYPE_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "TYPE");
    private final static QName _DSLAMFACILITYIPADDRESS_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "IP_ADDRESS");
    private final static QName _DSLAMFACILITYTELEFONO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "TELEFONO");
    private final static QName _DSLAMFACILITYEQUIPMENTNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", "EQUIPMENT_NAME");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.claro_m6_apiinterface
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DSLAMFACILITY }
     * 
     */
    public DSLAMFACILITY createDSLAMFACILITY() {
        return new DSLAMFACILITY();
    }

    /**
     * Create an instance of {@link ArrayOfDSLAMFACILITY }
     * 
     */
    public ArrayOfDSLAMFACILITY createArrayOfDSLAMFACILITY() {
        return new ArrayOfDSLAMFACILITY();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DSLAMFACILITY }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "DSLAMFACILITY")
    public JAXBElement<DSLAMFACILITY> createDSLAMFACILITY(DSLAMFACILITY value) {
        return new JAXBElement<DSLAMFACILITY>(_DSLAMFACILITY_QNAME, DSLAMFACILITY.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDSLAMFACILITY }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "ArrayOfDSLAMFACILITY")
    public JAXBElement<ArrayOfDSLAMFACILITY> createArrayOfDSLAMFACILITY(ArrayOfDSLAMFACILITY value) {
        return new JAXBElement<ArrayOfDSLAMFACILITY>(_ArrayOfDSLAMFACILITY_QNAME, ArrayOfDSLAMFACILITY.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "CIRCUIT_ID", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYCIRCUITID(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYCIRCUITID_QNAME, String.class, DSLAMFACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "STATUS", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYSTATUS(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYSTATUS_QNAME, String.class, DSLAMFACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "NODE_NAME", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYNODENAME(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYNODENAME_QNAME, String.class, DSLAMFACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "VENDOR_NAME", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYVENDORNAME(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYVENDORNAME_QNAME, String.class, DSLAMFACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "CLLI_CODE", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYCLLICODE(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYCLLICODE_QNAME, String.class, DSLAMFACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "NODE_ADDRESS", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYNODEADDRESS(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYNODEADDRESS_QNAME, String.class, DSLAMFACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "TYPE", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYTYPE(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYTYPE_QNAME, String.class, DSLAMFACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "IP_ADDRESS", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYIPADDRESS(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYIPADDRESS_QNAME, String.class, DSLAMFACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "TELEFONO", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYTELEFONO(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYTELEFONO_QNAME, String.class, DSLAMFACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel", name = "EQUIPMENT_NAME", scope = DSLAMFACILITY.class)
    public JAXBElement<String> createDSLAMFACILITYEQUIPMENTNAME(String value) {
        return new JAXBElement<String>(_DSLAMFACILITYEQUIPMENTNAME_QNAME, String.class, DSLAMFACILITY.class, value);
    }

}
