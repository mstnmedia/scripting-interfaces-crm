
package org.datacontract.schemas._2004._07.claro_dslam_interface;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SERVICE_PORT complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SERVICE_PORT">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Admin_Status" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Connection_Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Connection_Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Default_Gateway" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flow_Para" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ID_GPON_INFO" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IP_Access_Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Inbound_Table_Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Index" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Manage_Priority" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Manage_VLAN" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONTMACAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Outbound_Table_Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RX" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Service_Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Subnet_Mask" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TX" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="VLAN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Vlan_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SERVICE_PORT", propOrder = {
    "adminStatus",
    "connectionStatus",
    "connectionType",
    "defaultGateway",
    "description",
    "flowPara",
    "id",
    "idgponinfo",
    "ip",
    "ipAccessType",
    "inboundTableName",
    "index",
    "managePriority",
    "manageVLAN",
    "name",
    "ontmacAddress",
    "outboundTableName",
    "rx",
    "serviceType",
    "state",
    "subnetMask",
    "tx",
    "vlan",
    "vlanId"
})
public class SERVICEPORT {

    @XmlElement(name = "Admin_Status")
    protected Boolean adminStatus;
    @XmlElementRef(name = "Connection_Status", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> connectionStatus;
    @XmlElementRef(name = "Connection_Type", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> connectionType;
    @XmlElementRef(name = "Default_Gateway", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> defaultGateway;
    @XmlElementRef(name = "Description", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> description;
    @XmlElement(name = "Flow_Para")
    protected Integer flowPara;
    @XmlElement(name = "ID")
    protected Integer id;
    @XmlElement(name = "ID_GPON_INFO")
    protected Integer idgponinfo;
    @XmlElementRef(name = "IP", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ip;
    @XmlElementRef(name = "IP_Access_Type", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ipAccessType;
    @XmlElementRef(name = "Inbound_Table_Name", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> inboundTableName;
    @XmlElement(name = "Index")
    protected Integer index;
    @XmlElement(name = "Manage_Priority")
    protected Integer managePriority;
    @XmlElement(name = "Manage_VLAN")
    protected Integer manageVLAN;
    @XmlElementRef(name = "Name", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "ONTMACAddress", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> ontmacAddress;
    @XmlElementRef(name = "Outbound_Table_Name", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> outboundTableName;
    @XmlElement(name = "RX")
    protected Integer rx;
    @XmlElementRef(name = "Service_Type", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> serviceType;
    @XmlElement(name = "State")
    protected Boolean state;
    @XmlElementRef(name = "Subnet_Mask", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> subnetMask;
    @XmlElement(name = "TX")
    protected Integer tx;
    @XmlElementRef(name = "VLAN", namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> vlan;
    @XmlElement(name = "Vlan_Id")
    protected Integer vlanId;

    /**
     * Gets the value of the adminStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdminStatus() {
        return adminStatus;
    }

    /**
     * Sets the value of the adminStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdminStatus(Boolean value) {
        this.adminStatus = value;
    }

    /**
     * Gets the value of the connectionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConnectionStatus() {
        return connectionStatus;
    }

    /**
     * Sets the value of the connectionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConnectionStatus(JAXBElement<String> value) {
        this.connectionStatus = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the connectionType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConnectionType() {
        return connectionType;
    }

    /**
     * Sets the value of the connectionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConnectionType(JAXBElement<String> value) {
        this.connectionType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the defaultGateway property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultGateway() {
        return defaultGateway;
    }

    /**
     * Sets the value of the defaultGateway property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultGateway(JAXBElement<String> value) {
        this.defaultGateway = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the flowPara property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFlowPara() {
        return flowPara;
    }

    /**
     * Sets the value of the flowPara property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFlowPara(Integer value) {
        this.flowPara = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setID(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the idgponinfo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIDGPONINFO() {
        return idgponinfo;
    }

    /**
     * Sets the value of the idgponinfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIDGPONINFO(Integer value) {
        this.idgponinfo = value;
    }

    /**
     * Gets the value of the ip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIP() {
        return ip;
    }

    /**
     * Sets the value of the ip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIP(JAXBElement<String> value) {
        this.ip = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ipAccessType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIPAccessType() {
        return ipAccessType;
    }

    /**
     * Sets the value of the ipAccessType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIPAccessType(JAXBElement<String> value) {
        this.ipAccessType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the inboundTableName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInboundTableName() {
        return inboundTableName;
    }

    /**
     * Sets the value of the inboundTableName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInboundTableName(JAXBElement<String> value) {
        this.inboundTableName = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the index property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIndex() {
        return index;
    }

    /**
     * Sets the value of the index property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIndex(Integer value) {
        this.index = value;
    }

    /**
     * Gets the value of the managePriority property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getManagePriority() {
        return managePriority;
    }

    /**
     * Sets the value of the managePriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setManagePriority(Integer value) {
        this.managePriority = value;
    }

    /**
     * Gets the value of the manageVLAN property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getManageVLAN() {
        return manageVLAN;
    }

    /**
     * Sets the value of the manageVLAN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setManageVLAN(Integer value) {
        this.manageVLAN = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ontmacAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getONTMACAddress() {
        return ontmacAddress;
    }

    /**
     * Sets the value of the ontmacAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setONTMACAddress(JAXBElement<String> value) {
        this.ontmacAddress = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the outboundTableName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutboundTableName() {
        return outboundTableName;
    }

    /**
     * Sets the value of the outboundTableName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutboundTableName(JAXBElement<String> value) {
        this.outboundTableName = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the rx property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRX() {
        return rx;
    }

    /**
     * Sets the value of the rx property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRX(Integer value) {
        this.rx = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceType(JAXBElement<String> value) {
        this.serviceType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setState(Boolean value) {
        this.state = value;
    }

    /**
     * Gets the value of the subnetMask property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubnetMask() {
        return subnetMask;
    }

    /**
     * Sets the value of the subnetMask property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubnetMask(JAXBElement<String> value) {
        this.subnetMask = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the tx property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTX() {
        return tx;
    }

    /**
     * Sets the value of the tx property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTX(Integer value) {
        this.tx = value;
    }

    /**
     * Gets the value of the vlan property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVLAN() {
        return vlan;
    }

    /**
     * Sets the value of the vlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVLAN(JAXBElement<String> value) {
        this.vlan = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the vlanId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVlanId() {
        return vlanId;
    }

    /**
     * Sets the value of the vlanId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVlanId(Integer value) {
        this.vlanId = value;
    }

}
