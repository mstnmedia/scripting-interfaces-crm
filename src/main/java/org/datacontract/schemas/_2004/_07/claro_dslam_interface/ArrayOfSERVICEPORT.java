
package org.datacontract.schemas._2004._07.claro_dslam_interface;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSERVICE_PORT complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSERVICE_PORT">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SERVICE_PORT" type="{http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model}SERVICE_PORT" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSERVICE_PORT", propOrder = {
    "serviceport"
})
public class ArrayOfSERVICEPORT {

    @XmlElement(name = "SERVICE_PORT", nillable = true)
    protected List<SERVICEPORT> serviceport;

    /**
     * Gets the value of the serviceport property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceport property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSERVICEPORT().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SERVICEPORT }
     * 
     * 
     */
    public List<SERVICEPORT> getSERVICEPORT() {
        if (serviceport == null) {
            serviceport = new ArrayList<SERVICEPORT>();
        }
        return this.serviceport;
    }

}
