
package org.datacontract.schemas._2004._07.claro_dslam_interface;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DIAGNOSTICO_GPON complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DIAGNOSTICO_GPON">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ADSL" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ADSL_ServicePort_Status" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ID_FACILIDADES" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IPTV" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IPTV_ServicePort_Status" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IPTV_Tx_Rx" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IP_ADSL" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IP_IPTV" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IP_VOIP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OLTAdmState" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OLTOperState" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OLTRxONTOpticalPower" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ONTAdmState" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ONTBiasCurrent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ONTOperState" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ONTRxPower" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ONTTemp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ONTTxPower" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ONTVoltage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TestResult" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VOIP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VOIP_ServicePort_Status" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VelConfigurada" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VelConfiguradaDN" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VelConfiguradaUP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DIAGNOSTICO_GPON", propOrder = {
    "adsl",
    "adslServicePortStatus",
    "id",
    "idfacilidades",
    "iptv",
    "iptvServicePortStatus",
    "iptvTxRx",
    "ipadsl",
    "ipiptv",
    "ipvoip",
    "oltAdmState",
    "oltOperState",
    "oltRxONTOpticalPower",
    "ontAdmState",
    "ontBiasCurrent",
    "ontOperState",
    "ontRxPower",
    "ontTemp",
    "ontTxPower",
    "ontVoltage",
    "password",
    "testResult",
    "voip",
    "voipServicePortStatus",
    "velConfigurada",
    "velConfiguradaDN",
    "velConfiguradaUP"
})
public class DIAGNOSTICOGPON {

    @XmlElement(name = "ADSL")
    protected Boolean adsl;
    @XmlElement(name = "ADSL_ServicePort_Status")
    protected Boolean adslServicePortStatus;
    @XmlElement(name = "ID")
    protected Integer id;
    @XmlElement(name = "ID_FACILIDADES")
    protected Integer idfacilidades;
    @XmlElement(name = "IPTV")
    protected Boolean iptv;
    @XmlElement(name = "IPTV_ServicePort_Status")
    protected Boolean iptvServicePortStatus;
    @XmlElement(name = "IPTV_Tx_Rx")
    protected Boolean iptvTxRx;
    @XmlElement(name = "IP_ADSL")
    protected Boolean ipadsl;
    @XmlElement(name = "IP_IPTV")
    protected Boolean ipiptv;
    @XmlElement(name = "IP_VOIP")
    protected Boolean ipvoip;
    @XmlElement(name = "OLTAdmState")
    protected Boolean oltAdmState;
    @XmlElement(name = "OLTOperState")
    protected Boolean oltOperState;
    @XmlElement(name = "OLTRxONTOpticalPower")
    protected Boolean oltRxONTOpticalPower;
    @XmlElement(name = "ONTAdmState")
    protected Boolean ontAdmState;
    @XmlElement(name = "ONTBiasCurrent")
    protected Boolean ontBiasCurrent;
    @XmlElement(name = "ONTOperState")
    protected Boolean ontOperState;
    @XmlElement(name = "ONTRxPower")
    protected Boolean ontRxPower;
    @XmlElement(name = "ONTTemp")
    protected Boolean ontTemp;
    @XmlElement(name = "ONTTxPower")
    protected Boolean ontTxPower;
    @XmlElement(name = "ONTVoltage")
    protected Boolean ontVoltage;
    @XmlElement(name = "Password")
    protected Boolean password;
    @XmlElement(name = "TestResult")
    protected Boolean testResult;
    @XmlElement(name = "VOIP")
    protected Boolean voip;
    @XmlElement(name = "VOIP_ServicePort_Status")
    protected Boolean voipServicePortStatus;
    @XmlElement(name = "VelConfigurada")
    protected Boolean velConfigurada;
    @XmlElement(name = "VelConfiguradaDN")
    protected Boolean velConfiguradaDN;
    @XmlElement(name = "VelConfiguradaUP")
    protected Boolean velConfiguradaUP;

    /**
     * Gets the value of the adsl property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isADSL() {
        return adsl;
    }

    /**
     * Sets the value of the adsl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setADSL(Boolean value) {
        this.adsl = value;
    }

    /**
     * Gets the value of the adslServicePortStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isADSLServicePortStatus() {
        return adslServicePortStatus;
    }

    /**
     * Sets the value of the adslServicePortStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setADSLServicePortStatus(Boolean value) {
        this.adslServicePortStatus = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setID(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the idfacilidades property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIDFACILIDADES() {
        return idfacilidades;
    }

    /**
     * Sets the value of the idfacilidades property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIDFACILIDADES(Integer value) {
        this.idfacilidades = value;
    }

    /**
     * Gets the value of the iptv property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIPTV() {
        return iptv;
    }

    /**
     * Sets the value of the iptv property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIPTV(Boolean value) {
        this.iptv = value;
    }

    /**
     * Gets the value of the iptvServicePortStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIPTVServicePortStatus() {
        return iptvServicePortStatus;
    }

    /**
     * Sets the value of the iptvServicePortStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIPTVServicePortStatus(Boolean value) {
        this.iptvServicePortStatus = value;
    }

    /**
     * Gets the value of the iptvTxRx property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIPTVTxRx() {
        return iptvTxRx;
    }

    /**
     * Sets the value of the iptvTxRx property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIPTVTxRx(Boolean value) {
        this.iptvTxRx = value;
    }

    /**
     * Gets the value of the ipadsl property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIPADSL() {
        return ipadsl;
    }

    /**
     * Sets the value of the ipadsl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIPADSL(Boolean value) {
        this.ipadsl = value;
    }

    /**
     * Gets the value of the ipiptv property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIPIPTV() {
        return ipiptv;
    }

    /**
     * Sets the value of the ipiptv property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIPIPTV(Boolean value) {
        this.ipiptv = value;
    }

    /**
     * Gets the value of the ipvoip property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIPVOIP() {
        return ipvoip;
    }

    /**
     * Sets the value of the ipvoip property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIPVOIP(Boolean value) {
        this.ipvoip = value;
    }

    /**
     * Gets the value of the oltAdmState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOLTAdmState() {
        return oltAdmState;
    }

    /**
     * Sets the value of the oltAdmState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOLTAdmState(Boolean value) {
        this.oltAdmState = value;
    }

    /**
     * Gets the value of the oltOperState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOLTOperState() {
        return oltOperState;
    }

    /**
     * Sets the value of the oltOperState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOLTOperState(Boolean value) {
        this.oltOperState = value;
    }

    /**
     * Gets the value of the oltRxONTOpticalPower property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOLTRxONTOpticalPower() {
        return oltRxONTOpticalPower;
    }

    /**
     * Sets the value of the oltRxONTOpticalPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOLTRxONTOpticalPower(Boolean value) {
        this.oltRxONTOpticalPower = value;
    }

    /**
     * Gets the value of the ontAdmState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isONTAdmState() {
        return ontAdmState;
    }

    /**
     * Sets the value of the ontAdmState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setONTAdmState(Boolean value) {
        this.ontAdmState = value;
    }

    /**
     * Gets the value of the ontBiasCurrent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isONTBiasCurrent() {
        return ontBiasCurrent;
    }

    /**
     * Sets the value of the ontBiasCurrent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setONTBiasCurrent(Boolean value) {
        this.ontBiasCurrent = value;
    }

    /**
     * Gets the value of the ontOperState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isONTOperState() {
        return ontOperState;
    }

    /**
     * Sets the value of the ontOperState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setONTOperState(Boolean value) {
        this.ontOperState = value;
    }

    /**
     * Gets the value of the ontRxPower property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isONTRxPower() {
        return ontRxPower;
    }

    /**
     * Sets the value of the ontRxPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setONTRxPower(Boolean value) {
        this.ontRxPower = value;
    }

    /**
     * Gets the value of the ontTemp property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isONTTemp() {
        return ontTemp;
    }

    /**
     * Sets the value of the ontTemp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setONTTemp(Boolean value) {
        this.ontTemp = value;
    }

    /**
     * Gets the value of the ontTxPower property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isONTTxPower() {
        return ontTxPower;
    }

    /**
     * Sets the value of the ontTxPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setONTTxPower(Boolean value) {
        this.ontTxPower = value;
    }

    /**
     * Gets the value of the ontVoltage property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isONTVoltage() {
        return ontVoltage;
    }

    /**
     * Sets the value of the ontVoltage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setONTVoltage(Boolean value) {
        this.ontVoltage = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPassword(Boolean value) {
        this.password = value;
    }

    /**
     * Gets the value of the testResult property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTestResult() {
        return testResult;
    }

    /**
     * Sets the value of the testResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTestResult(Boolean value) {
        this.testResult = value;
    }

    /**
     * Gets the value of the voip property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVOIP() {
        return voip;
    }

    /**
     * Sets the value of the voip property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVOIP(Boolean value) {
        this.voip = value;
    }

    /**
     * Gets the value of the voipServicePortStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVOIPServicePortStatus() {
        return voipServicePortStatus;
    }

    /**
     * Sets the value of the voipServicePortStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVOIPServicePortStatus(Boolean value) {
        this.voipServicePortStatus = value;
    }

    /**
     * Gets the value of the velConfigurada property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVelConfigurada() {
        return velConfigurada;
    }

    /**
     * Sets the value of the velConfigurada property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVelConfigurada(Boolean value) {
        this.velConfigurada = value;
    }

    /**
     * Gets the value of the velConfiguradaDN property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVelConfiguradaDN() {
        return velConfiguradaDN;
    }

    /**
     * Sets the value of the velConfiguradaDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVelConfiguradaDN(Boolean value) {
        this.velConfiguradaDN = value;
    }

    /**
     * Gets the value of the velConfiguradaUP property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVelConfiguradaUP() {
        return velConfiguradaUP;
    }

    /**
     * Sets the value of the velConfiguradaUP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVelConfiguradaUP(Boolean value) {
        this.velConfiguradaUP = value;
    }

}
