
package org.datacontract.schemas._2004._07.claro_dslam_interface;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DIAGNOSTICO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DIAGNOSTICO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Atenuacion" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Atenuacion_Bajada" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Atenuacion_Bajada_Loop" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Atenuacion_Subida" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Atenuacion_Subida_Loop" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AutenticadorIPTV" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CondAdministrativa" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CondOperativa" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Configuracion" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Desbalance" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ID_FACILIDADES" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IP_VIDEO" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MargenEstabilidad" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PVC33" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PVC35" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Ruido" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Ruido_Bajada" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Ruido_Subida" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Service_Profile" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TestResult" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VLAN" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VLAN_IPTV" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Velocidad" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DIAGNOSTICO", propOrder = {
    "atenuacion",
    "atenuacionBajada",
    "atenuacionBajadaLoop",
    "atenuacionSubida",
    "atenuacionSubidaLoop",
    "autenticadorIPTV",
    "condAdministrativa",
    "condOperativa",
    "configuracion",
    "desbalance",
    "id",
    "idfacilidades",
    "ip",
    "ipvideo",
    "margenEstabilidad",
    "pvc33",
    "pvc35",
    "ruido",
    "ruidoBajada",
    "ruidoSubida",
    "serviceProfile",
    "testResult",
    "vlan",
    "vlaniptv",
    "velocidad"
})
public class DIAGNOSTICO {

    @XmlElement(name = "Atenuacion")
    protected Boolean atenuacion;
    @XmlElement(name = "Atenuacion_Bajada")
    protected Boolean atenuacionBajada;
    @XmlElement(name = "Atenuacion_Bajada_Loop")
    protected Boolean atenuacionBajadaLoop;
    @XmlElement(name = "Atenuacion_Subida")
    protected Boolean atenuacionSubida;
    @XmlElement(name = "Atenuacion_Subida_Loop")
    protected Boolean atenuacionSubidaLoop;
    @XmlElement(name = "AutenticadorIPTV")
    protected Boolean autenticadorIPTV;
    @XmlElement(name = "CondAdministrativa")
    protected Boolean condAdministrativa;
    @XmlElement(name = "CondOperativa")
    protected Boolean condOperativa;
    @XmlElement(name = "Configuracion")
    protected Boolean configuracion;
    @XmlElement(name = "Desbalance")
    protected Boolean desbalance;
    @XmlElement(name = "ID")
    protected Integer id;
    @XmlElement(name = "ID_FACILIDADES")
    protected Integer idfacilidades;
    @XmlElement(name = "IP")
    protected Boolean ip;
    @XmlElement(name = "IP_VIDEO")
    protected Boolean ipvideo;
    @XmlElement(name = "MargenEstabilidad")
    protected Boolean margenEstabilidad;
    @XmlElement(name = "PVC33")
    protected Boolean pvc33;
    @XmlElement(name = "PVC35")
    protected Boolean pvc35;
    @XmlElement(name = "Ruido")
    protected Boolean ruido;
    @XmlElement(name = "Ruido_Bajada")
    protected Boolean ruidoBajada;
    @XmlElement(name = "Ruido_Subida")
    protected Boolean ruidoSubida;
    @XmlElement(name = "Service_Profile")
    protected Boolean serviceProfile;
    @XmlElement(name = "TestResult")
    protected Boolean testResult;
    @XmlElement(name = "VLAN")
    protected Boolean vlan;
    @XmlElement(name = "VLAN_IPTV")
    protected Boolean vlaniptv;
    @XmlElement(name = "Velocidad")
    protected Boolean velocidad;

    /**
     * Gets the value of the atenuacion property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAtenuacion() {
        return atenuacion;
    }

    /**
     * Sets the value of the atenuacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAtenuacion(Boolean value) {
        this.atenuacion = value;
    }

    /**
     * Gets the value of the atenuacionBajada property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAtenuacionBajada() {
        return atenuacionBajada;
    }

    /**
     * Sets the value of the atenuacionBajada property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAtenuacionBajada(Boolean value) {
        this.atenuacionBajada = value;
    }

    /**
     * Gets the value of the atenuacionBajadaLoop property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAtenuacionBajadaLoop() {
        return atenuacionBajadaLoop;
    }

    /**
     * Sets the value of the atenuacionBajadaLoop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAtenuacionBajadaLoop(Boolean value) {
        this.atenuacionBajadaLoop = value;
    }

    /**
     * Gets the value of the atenuacionSubida property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAtenuacionSubida() {
        return atenuacionSubida;
    }

    /**
     * Sets the value of the atenuacionSubida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAtenuacionSubida(Boolean value) {
        this.atenuacionSubida = value;
    }

    /**
     * Gets the value of the atenuacionSubidaLoop property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAtenuacionSubidaLoop() {
        return atenuacionSubidaLoop;
    }

    /**
     * Sets the value of the atenuacionSubidaLoop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAtenuacionSubidaLoop(Boolean value) {
        this.atenuacionSubidaLoop = value;
    }

    /**
     * Gets the value of the autenticadorIPTV property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutenticadorIPTV() {
        return autenticadorIPTV;
    }

    /**
     * Sets the value of the autenticadorIPTV property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutenticadorIPTV(Boolean value) {
        this.autenticadorIPTV = value;
    }

    /**
     * Gets the value of the condAdministrativa property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCondAdministrativa() {
        return condAdministrativa;
    }

    /**
     * Sets the value of the condAdministrativa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCondAdministrativa(Boolean value) {
        this.condAdministrativa = value;
    }

    /**
     * Gets the value of the condOperativa property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCondOperativa() {
        return condOperativa;
    }

    /**
     * Sets the value of the condOperativa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCondOperativa(Boolean value) {
        this.condOperativa = value;
    }

    /**
     * Gets the value of the configuracion property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConfiguracion() {
        return configuracion;
    }

    /**
     * Sets the value of the configuracion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConfiguracion(Boolean value) {
        this.configuracion = value;
    }

    /**
     * Gets the value of the desbalance property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDesbalance() {
        return desbalance;
    }

    /**
     * Sets the value of the desbalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDesbalance(Boolean value) {
        this.desbalance = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setID(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the idfacilidades property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIDFACILIDADES() {
        return idfacilidades;
    }

    /**
     * Sets the value of the idfacilidades property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIDFACILIDADES(Integer value) {
        this.idfacilidades = value;
    }

    /**
     * Gets the value of the ip property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIP() {
        return ip;
    }

    /**
     * Sets the value of the ip property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIP(Boolean value) {
        this.ip = value;
    }

    /**
     * Gets the value of the ipvideo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIPVIDEO() {
        return ipvideo;
    }

    /**
     * Sets the value of the ipvideo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIPVIDEO(Boolean value) {
        this.ipvideo = value;
    }

    /**
     * Gets the value of the margenEstabilidad property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMargenEstabilidad() {
        return margenEstabilidad;
    }

    /**
     * Sets the value of the margenEstabilidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMargenEstabilidad(Boolean value) {
        this.margenEstabilidad = value;
    }

    /**
     * Gets the value of the pvc33 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPVC33() {
        return pvc33;
    }

    /**
     * Sets the value of the pvc33 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPVC33(Boolean value) {
        this.pvc33 = value;
    }

    /**
     * Gets the value of the pvc35 property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPVC35() {
        return pvc35;
    }

    /**
     * Sets the value of the pvc35 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPVC35(Boolean value) {
        this.pvc35 = value;
    }

    /**
     * Gets the value of the ruido property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRuido() {
        return ruido;
    }

    /**
     * Sets the value of the ruido property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRuido(Boolean value) {
        this.ruido = value;
    }

    /**
     * Gets the value of the ruidoBajada property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRuidoBajada() {
        return ruidoBajada;
    }

    /**
     * Sets the value of the ruidoBajada property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRuidoBajada(Boolean value) {
        this.ruidoBajada = value;
    }

    /**
     * Gets the value of the ruidoSubida property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRuidoSubida() {
        return ruidoSubida;
    }

    /**
     * Sets the value of the ruidoSubida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRuidoSubida(Boolean value) {
        this.ruidoSubida = value;
    }

    /**
     * Gets the value of the serviceProfile property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isServiceProfile() {
        return serviceProfile;
    }

    /**
     * Sets the value of the serviceProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServiceProfile(Boolean value) {
        this.serviceProfile = value;
    }

    /**
     * Gets the value of the testResult property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTestResult() {
        return testResult;
    }

    /**
     * Sets the value of the testResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTestResult(Boolean value) {
        this.testResult = value;
    }

    /**
     * Gets the value of the vlan property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVLAN() {
        return vlan;
    }

    /**
     * Sets the value of the vlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVLAN(Boolean value) {
        this.vlan = value;
    }

    /**
     * Gets the value of the vlaniptv property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVLANIPTV() {
        return vlaniptv;
    }

    /**
     * Sets the value of the vlaniptv property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVLANIPTV(Boolean value) {
        this.vlaniptv = value;
    }

    /**
     * Gets the value of the velocidad property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVelocidad() {
        return velocidad;
    }

    /**
     * Sets the value of the velocidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVelocidad(Boolean value) {
        this.velocidad = value;
    }

}
