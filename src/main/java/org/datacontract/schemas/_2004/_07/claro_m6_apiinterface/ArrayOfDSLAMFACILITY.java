
package org.datacontract.schemas._2004._07.claro_m6_apiinterface;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDSLAMFACILITY complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDSLAMFACILITY">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DSLAMFACILITY" type="{http://schemas.datacontract.org/2004/07/CLARO.M6.APIINTERFACE.InterModel}DSLAMFACILITY" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDSLAMFACILITY", propOrder = {
    "dslamfacility"
})
public class ArrayOfDSLAMFACILITY {

    @XmlElement(name = "DSLAMFACILITY", nillable = true)
    protected List<DSLAMFACILITY> dslamfacility;

    /**
     * Gets the value of the dslamfacility property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dslamfacility property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDSLAMFACILITY().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DSLAMFACILITY }
     * 
     * 
     */
    public List<DSLAMFACILITY> getDSLAMFACILITY() {
        if (dslamfacility == null) {
            dslamfacility = new ArrayList<DSLAMFACILITY>();
        }
        return this.dslamfacility;
    }

}
