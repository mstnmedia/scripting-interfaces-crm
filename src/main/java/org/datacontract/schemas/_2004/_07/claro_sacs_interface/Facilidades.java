
package org.datacontract.schemas._2004._07.claro_sacs_interface;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Facilidades complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Facilidades">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CircuitDesignId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CircuitId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClliCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NodeAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NodeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Referencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoReferencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Facilidades", propOrder = {
    "circuitDesignId",
    "circuitId",
    "clliCode",
    "equipmentName",
    "fecha",
    "id",
    "nodeAddress",
    "nodeName",
    "referencia",
    "status",
    "telefono",
    "tipo",
    "tipoReferencia",
    "type",
    "usuario",
    "vendorName"
})
public class Facilidades {

    @XmlElementRef(name = "CircuitDesignId", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> circuitDesignId;
    @XmlElementRef(name = "CircuitId", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> circuitId;
    @XmlElementRef(name = "ClliCode", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> clliCode;
    @XmlElementRef(name = "EquipmentName", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> equipmentName;
    @XmlElementRef(name = "Fecha", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> fecha;
    @XmlElement(name = "Id")
    protected Integer id;
    @XmlElementRef(name = "NodeAddress", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> nodeAddress;
    @XmlElementRef(name = "NodeName", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> nodeName;
    @XmlElementRef(name = "Referencia", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> referencia;
    @XmlElementRef(name = "Status", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> status;
    @XmlElementRef(name = "Telefono", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> telefono;
    @XmlElementRef(name = "Tipo", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> tipo;
    @XmlElementRef(name = "TipoReferencia", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> tipoReferencia;
    @XmlElementRef(name = "Type", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> type;
    @XmlElementRef(name = "Usuario", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> usuario;
    @XmlElementRef(name = "VendorName", namespace = "http://schemas.datacontract.org/2004/07/CLARO.SACS.INTERFACE.Model", type = JAXBElement.class)
    protected JAXBElement<String> vendorName;

    /**
     * Gets the value of the circuitDesignId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCircuitDesignId() {
        return circuitDesignId;
    }

    /**
     * Sets the value of the circuitDesignId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCircuitDesignId(JAXBElement<String> value) {
        this.circuitDesignId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the circuitId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCircuitId() {
        return circuitId;
    }

    /**
     * Sets the value of the circuitId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCircuitId(JAXBElement<String> value) {
        this.circuitId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the clliCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClliCode() {
        return clliCode;
    }

    /**
     * Sets the value of the clliCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClliCode(JAXBElement<String> value) {
        this.clliCode = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the equipmentName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEquipmentName() {
        return equipmentName;
    }

    /**
     * Sets the value of the equipmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEquipmentName(JAXBElement<String> value) {
        this.equipmentName = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the fecha property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getFecha() {
        return fecha;
    }

    /**
     * Sets the value of the fecha property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setFecha(JAXBElement<XMLGregorianCalendar> value) {
        this.fecha = ((JAXBElement<XMLGregorianCalendar> ) value);
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the nodeAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNodeAddress() {
        return nodeAddress;
    }

    /**
     * Sets the value of the nodeAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNodeAddress(JAXBElement<String> value) {
        this.nodeAddress = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the nodeName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNodeName() {
        return nodeName;
    }

    /**
     * Sets the value of the nodeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNodeName(JAXBElement<String> value) {
        this.nodeName = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the referencia property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReferencia() {
        return referencia;
    }

    /**
     * Sets the value of the referencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReferencia(JAXBElement<String> value) {
        this.referencia = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStatus(JAXBElement<String> value) {
        this.status = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the telefono property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelefono() {
        return telefono;
    }

    /**
     * Sets the value of the telefono property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelefono(JAXBElement<String> value) {
        this.telefono = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the tipo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipo() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipo(JAXBElement<String> value) {
        this.tipo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the tipoReferencia property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipoReferencia() {
        return tipoReferencia;
    }

    /**
     * Sets the value of the tipoReferencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipoReferencia(JAXBElement<String> value) {
        this.tipoReferencia = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setType(JAXBElement<String> value) {
        this.type = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the usuario property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUsuario() {
        return usuario;
    }

    /**
     * Sets the value of the usuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUsuario(JAXBElement<String> value) {
        this.usuario = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the vendorName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorName() {
        return vendorName;
    }

    /**
     * Sets the value of the vendorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorName(JAXBElement<String> value) {
        this.vendorName = ((JAXBElement<String> ) value);
    }

}
