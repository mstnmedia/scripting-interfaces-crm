
package org.datacontract.schemas._2004._07.claro_dslam_interface;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.datacontract.schemas._2004._07.claro_sacs_interface.Facilidades;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.claro_dslam_interface package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GPONINFO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "GPON_INFO");
    private final static QName _PortStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "PortStatus");
    private final static QName _ArrayOfSERVICEPORT_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ArrayOfSERVICE_PORT");
    private final static QName _DIAGNOSTICOGPON_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "DIAGNOSTICO_GPON");
    private final static QName _DIAGNOSTICO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "DIAGNOSTICO");
    private final static QName _SERVICEPORT_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "SERVICE_PORT");
    private final static QName _FACILITY_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "FACILITY");
    private final static QName _DSLAMINFO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "DSLAM_INFO");
    private final static QName _FACILITYIPADDRESS_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "IP_ADDRESS");
    private final static QName _FACILITYTELEFONO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "TELEFONO");
    private final static QName _FACILITYEQUIPMENTNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "EQUIPMENT_NAME");
    private final static QName _FACILITYPUERTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "PUERTO");
    private final static QName _FACILITYTYPE_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "TYPE");
    private final static QName _FACILITYNODENAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "NODE_NAME");
    private final static QName _FACILITYTIPO_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "TIPO");
    private final static QName _FACILITYSTATUS_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "STATUS");
    private final static QName _FACILITYCLLICODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "CLLI_CODE");
    private final static QName _FACILITYNODEADDRESS_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "NODE_ADDRESS");
    private final static QName _FACILITYVENDORNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "VENDOR_NAME");
    private final static QName _FACILITYCIRCUITID_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "CIRCUIT_ID");
    private final static QName _FACILITYREFERENCIA_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "REFERENCIA");
    private final static QName _SERVICEPORTConnectionType_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Connection_Type");
    private final static QName _SERVICEPORTOutboundTableName_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Outbound_Table_Name");
    private final static QName _SERVICEPORTSubnetMask_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Subnet_Mask");
    private final static QName _SERVICEPORTIPAccessType_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "IP_Access_Type");
    private final static QName _SERVICEPORTInboundTableName_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Inbound_Table_Name");
    private final static QName _SERVICEPORTServiceType_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Service_Type");
    private final static QName _SERVICEPORTDefaultGateway_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Default_Gateway");
    private final static QName _SERVICEPORTVLAN_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "VLAN");
    private final static QName _SERVICEPORTONTMACAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTMACAddress");
    private final static QName _SERVICEPORTName_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Name");
    private final static QName _SERVICEPORTConnectionStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Connection_Status");
    private final static QName _SERVICEPORTDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Description");
    private final static QName _SERVICEPORTIP_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "IP");
    private final static QName _GPONINFOONTMemUsage_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTMemUsage");
    private final static QName _GPONINFOONTOnlineDuration_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTOnlineDuration");
    private final static QName _GPONINFOServicePort_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Service_Port");
    private final static QName _GPONINFOONTLastDownCause_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTLastDownCause");
    private final static QName _GPONINFOONTSoftwareVersion_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTSoftwareVersion");
    private final static QName _GPONINFOONTUpTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTUpTime");
    private final static QName _GPONINFOONTLastDown_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTLastDown");
    private final static QName _GPONINFOONTLineProfile_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTLineProfile");
    private final static QName _GPONINFOOLTUptime_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "OLTUptime");
    private final static QName _GPONINFOONTVersion_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTVersion");
    private final static QName _GPONINFOONTCPUUsage_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTCPUUsage");
    private final static QName _GPONINFOONTVendor_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTVendor");
    private final static QName _GPONINFOFacilidades_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "facilidades");
    private final static QName _GPONINFOOLTPassword_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "OLTPassword");
    private final static QName _GPONINFOOntProductDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "OntProductDescription");
    private final static QName _GPONINFODiagnosticoGpon_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "DiagnosticoGpon");
    private final static QName _GPONINFOONTLastDownTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTLastDownTime");
    private final static QName _GPONINFOFacilida_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Facilida");
    private final static QName _GPONINFOONTSerialNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTSerialNumber");
    private final static QName _GPONINFOONTDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTDescription");
    private final static QName _GPONINFOCurrentThroughput_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "CurrentThroughput");
    private final static QName _GPONINFOOLTFirmware_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "OLTFirmware");
    private final static QName _GPONINFOONTBatteryStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "ONTBatteryStatus");
    private final static QName _DSLAMINFOSessionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Session_Id");
    private final static QName _DSLAMINFOFeeder_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Feeder");
    private final static QName _DSLAMINFOCalidadServicio_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Calidad_Servicio");
    private final static QName _DSLAMINFORinitID_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Rinit_ID");
    private final static QName _DSLAMINFOSlot_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Slot");
    private final static QName _DSLAMINFOMACAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "MAC_Address");
    private final static QName _DSLAMINFOTipoTarjeta_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Tipo_Tarjeta");
    private final static QName _DSLAMINFOUltimaBajada_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Ultima_Bajada");
    private final static QName _DSLAMINFORestrtCnt_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Restrt_Cnt");
    private final static QName _DSLAMINFODistancia_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "distancia");
    private final static QName _DSLAMINFOSessionIdIPTV_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Session_Id_IPTV");
    private final static QName _DSLAMINFOVLANId_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "VLAN_id");
    private final static QName _DSLAMINFOCustomerID_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "CustomerID");
    private final static QName _DSLAMINFOErrorStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Error_Status");
    private final static QName _DSLAMINFODiagnosticoCobre_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "DiagnosticoCobre");
    private final static QName _DSLAMINFONombreProfile_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Nombre_Profile");
    private final static QName _DSLAMINFOUltimoSincronismo_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Ultimo_Sincronismo");
    private final static QName _DSLAMINFOUser_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "User");
    private final static QName _DSLAMINFOCabina_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Cabina");
    private final static QName _DSLAMINFOAvailability_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Availability");
    private final static QName _DSLAMINFOSerialModem_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Serial_Modem");
    private final static QName _DSLAMINFOActualType_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "Actual_Type");
    private final static QName _PortStatusTestResult_QNAME = new QName("http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", "TestResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.claro_dslam_interface
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FACILITY }
     * 
     */
    public FACILITY createFACILITY() {
        return new FACILITY();
    }

    /**
     * Create an instance of {@link ArrayOfSERVICEPORT }
     * 
     */
    public ArrayOfSERVICEPORT createArrayOfSERVICEPORT() {
        return new ArrayOfSERVICEPORT();
    }

    /**
     * Create an instance of {@link GPONINFO }
     * 
     */
    public GPONINFO createGPONINFO() {
        return new GPONINFO();
    }

    /**
     * Create an instance of {@link SERVICEPORT }
     * 
     */
    public SERVICEPORT createSERVICEPORT() {
        return new SERVICEPORT();
    }

    /**
     * Create an instance of {@link DIAGNOSTICO }
     * 
     */
    public DIAGNOSTICO createDIAGNOSTICO() {
        return new DIAGNOSTICO();
    }

    /**
     * Create an instance of {@link DSLAMINFO }
     * 
     */
    public DSLAMINFO createDSLAMINFO() {
        return new DSLAMINFO();
    }

    /**
     * Create an instance of {@link DIAGNOSTICOGPON }
     * 
     */
    public DIAGNOSTICOGPON createDIAGNOSTICOGPON() {
        return new DIAGNOSTICOGPON();
    }

    /**
     * Create an instance of {@link PortStatus }
     * 
     */
    public PortStatus createPortStatus() {
        return new PortStatus();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GPONINFO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "GPON_INFO")
    public JAXBElement<GPONINFO> createGPONINFO(GPONINFO value) {
        return new JAXBElement<GPONINFO>(_GPONINFO_QNAME, GPONINFO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "PortStatus")
    public JAXBElement<PortStatus> createPortStatus(PortStatus value) {
        return new JAXBElement<PortStatus>(_PortStatus_QNAME, PortStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSERVICEPORT }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ArrayOfSERVICE_PORT")
    public JAXBElement<ArrayOfSERVICEPORT> createArrayOfSERVICEPORT(ArrayOfSERVICEPORT value) {
        return new JAXBElement<ArrayOfSERVICEPORT>(_ArrayOfSERVICEPORT_QNAME, ArrayOfSERVICEPORT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DIAGNOSTICOGPON }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "DIAGNOSTICO_GPON")
    public JAXBElement<DIAGNOSTICOGPON> createDIAGNOSTICOGPON(DIAGNOSTICOGPON value) {
        return new JAXBElement<DIAGNOSTICOGPON>(_DIAGNOSTICOGPON_QNAME, DIAGNOSTICOGPON.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DIAGNOSTICO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "DIAGNOSTICO")
    public JAXBElement<DIAGNOSTICO> createDIAGNOSTICO(DIAGNOSTICO value) {
        return new JAXBElement<DIAGNOSTICO>(_DIAGNOSTICO_QNAME, DIAGNOSTICO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SERVICEPORT }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "SERVICE_PORT")
    public JAXBElement<SERVICEPORT> createSERVICEPORT(SERVICEPORT value) {
        return new JAXBElement<SERVICEPORT>(_SERVICEPORT_QNAME, SERVICEPORT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FACILITY }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "FACILITY")
    public JAXBElement<FACILITY> createFACILITY(FACILITY value) {
        return new JAXBElement<FACILITY>(_FACILITY_QNAME, FACILITY.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DSLAMINFO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "DSLAM_INFO")
    public JAXBElement<DSLAMINFO> createDSLAMINFO(DSLAMINFO value) {
        return new JAXBElement<DSLAMINFO>(_DSLAMINFO_QNAME, DSLAMINFO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "IP_ADDRESS", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYIPADDRESS(String value) {
        return new JAXBElement<String>(_FACILITYIPADDRESS_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "TELEFONO", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYTELEFONO(String value) {
        return new JAXBElement<String>(_FACILITYTELEFONO_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "EQUIPMENT_NAME", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYEQUIPMENTNAME(String value) {
        return new JAXBElement<String>(_FACILITYEQUIPMENTNAME_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "PUERTO", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYPUERTO(String value) {
        return new JAXBElement<String>(_FACILITYPUERTO_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "TYPE", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYTYPE(String value) {
        return new JAXBElement<String>(_FACILITYTYPE_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "NODE_NAME", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYNODENAME(String value) {
        return new JAXBElement<String>(_FACILITYNODENAME_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "TIPO", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYTIPO(String value) {
        return new JAXBElement<String>(_FACILITYTIPO_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "STATUS", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYSTATUS(String value) {
        return new JAXBElement<String>(_FACILITYSTATUS_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "CLLI_CODE", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYCLLICODE(String value) {
        return new JAXBElement<String>(_FACILITYCLLICODE_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "NODE_ADDRESS", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYNODEADDRESS(String value) {
        return new JAXBElement<String>(_FACILITYNODEADDRESS_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "VENDOR_NAME", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYVENDORNAME(String value) {
        return new JAXBElement<String>(_FACILITYVENDORNAME_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "CIRCUIT_ID", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYCIRCUITID(String value) {
        return new JAXBElement<String>(_FACILITYCIRCUITID_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "REFERENCIA", scope = FACILITY.class)
    public JAXBElement<String> createFACILITYREFERENCIA(String value) {
        return new JAXBElement<String>(_FACILITYREFERENCIA_QNAME, String.class, FACILITY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Connection_Type", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTConnectionType(String value) {
        return new JAXBElement<String>(_SERVICEPORTConnectionType_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Outbound_Table_Name", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTOutboundTableName(String value) {
        return new JAXBElement<String>(_SERVICEPORTOutboundTableName_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Subnet_Mask", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTSubnetMask(String value) {
        return new JAXBElement<String>(_SERVICEPORTSubnetMask_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "IP_Access_Type", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTIPAccessType(String value) {
        return new JAXBElement<String>(_SERVICEPORTIPAccessType_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Inbound_Table_Name", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTInboundTableName(String value) {
        return new JAXBElement<String>(_SERVICEPORTInboundTableName_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Service_Type", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTServiceType(String value) {
        return new JAXBElement<String>(_SERVICEPORTServiceType_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Default_Gateway", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTDefaultGateway(String value) {
        return new JAXBElement<String>(_SERVICEPORTDefaultGateway_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "VLAN", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTVLAN(String value) {
        return new JAXBElement<String>(_SERVICEPORTVLAN_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTMACAddress", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTONTMACAddress(String value) {
        return new JAXBElement<String>(_SERVICEPORTONTMACAddress_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Name", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTName(String value) {
        return new JAXBElement<String>(_SERVICEPORTName_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Connection_Status", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTConnectionStatus(String value) {
        return new JAXBElement<String>(_SERVICEPORTConnectionStatus_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Description", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTDescription(String value) {
        return new JAXBElement<String>(_SERVICEPORTDescription_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "IP", scope = SERVICEPORT.class)
    public JAXBElement<String> createSERVICEPORTIP(String value) {
        return new JAXBElement<String>(_SERVICEPORTIP_QNAME, String.class, SERVICEPORT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTMemUsage", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTMemUsage(String value) {
        return new JAXBElement<String>(_GPONINFOONTMemUsage_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTOnlineDuration", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTOnlineDuration(String value) {
        return new JAXBElement<String>(_GPONINFOONTOnlineDuration_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSERVICEPORT }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Service_Port", scope = GPONINFO.class)
    public JAXBElement<ArrayOfSERVICEPORT> createGPONINFOServicePort(ArrayOfSERVICEPORT value) {
        return new JAXBElement<ArrayOfSERVICEPORT>(_GPONINFOServicePort_QNAME, ArrayOfSERVICEPORT.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTLastDownCause", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTLastDownCause(String value) {
        return new JAXBElement<String>(_GPONINFOONTLastDownCause_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTSoftwareVersion", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTSoftwareVersion(String value) {
        return new JAXBElement<String>(_GPONINFOONTSoftwareVersion_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTUpTime", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTUpTime(String value) {
        return new JAXBElement<String>(_GPONINFOONTUpTime_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTLastDown", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTLastDown(String value) {
        return new JAXBElement<String>(_GPONINFOONTLastDown_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTLineProfile", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTLineProfile(String value) {
        return new JAXBElement<String>(_GPONINFOONTLineProfile_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "OLTUptime", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOOLTUptime(String value) {
        return new JAXBElement<String>(_GPONINFOOLTUptime_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTVersion", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTVersion(String value) {
        return new JAXBElement<String>(_GPONINFOONTVersion_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTCPUUsage", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTCPUUsage(String value) {
        return new JAXBElement<String>(_GPONINFOONTCPUUsage_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTVendor", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTVendor(String value) {
        return new JAXBElement<String>(_GPONINFOONTVendor_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facilidades }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "facilidades", scope = GPONINFO.class)
    public JAXBElement<Facilidades> createGPONINFOFacilidades(Facilidades value) {
        return new JAXBElement<Facilidades>(_GPONINFOFacilidades_QNAME, Facilidades.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "OLTPassword", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOOLTPassword(String value) {
        return new JAXBElement<String>(_GPONINFOOLTPassword_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "OntProductDescription", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOOntProductDescription(String value) {
        return new JAXBElement<String>(_GPONINFOOntProductDescription_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DIAGNOSTICOGPON }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "DiagnosticoGpon", scope = GPONINFO.class)
    public JAXBElement<DIAGNOSTICOGPON> createGPONINFODiagnosticoGpon(DIAGNOSTICOGPON value) {
        return new JAXBElement<DIAGNOSTICOGPON>(_GPONINFODiagnosticoGpon_QNAME, DIAGNOSTICOGPON.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTLastDownTime", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTLastDownTime(String value) {
        return new JAXBElement<String>(_GPONINFOONTLastDownTime_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FACILITY }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Facilida", scope = GPONINFO.class)
    public JAXBElement<FACILITY> createGPONINFOFacilida(FACILITY value) {
        return new JAXBElement<FACILITY>(_GPONINFOFacilida_QNAME, FACILITY.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTSerialNumber", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTSerialNumber(String value) {
        return new JAXBElement<String>(_GPONINFOONTSerialNumber_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTDescription", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTDescription(String value) {
        return new JAXBElement<String>(_GPONINFOONTDescription_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "CurrentThroughput", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOCurrentThroughput(String value) {
        return new JAXBElement<String>(_GPONINFOCurrentThroughput_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "OLTFirmware", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOOLTFirmware(String value) {
        return new JAXBElement<String>(_GPONINFOOLTFirmware_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "ONTBatteryStatus", scope = GPONINFO.class)
    public JAXBElement<String> createGPONINFOONTBatteryStatus(String value) {
        return new JAXBElement<String>(_GPONINFOONTBatteryStatus_QNAME, String.class, GPONINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Session_Id", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOSessionId(String value) {
        return new JAXBElement<String>(_DSLAMINFOSessionId_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Feeder", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOFeeder(String value) {
        return new JAXBElement<String>(_DSLAMINFOFeeder_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Calidad_Servicio", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOCalidadServicio(String value) {
        return new JAXBElement<String>(_DSLAMINFOCalidadServicio_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Rinit_ID", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFORinitID(String value) {
        return new JAXBElement<String>(_DSLAMINFORinitID_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Slot", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOSlot(String value) {
        return new JAXBElement<String>(_DSLAMINFOSlot_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "MAC_Address", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOMACAddress(String value) {
        return new JAXBElement<String>(_DSLAMINFOMACAddress_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Tipo_Tarjeta", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOTipoTarjeta(String value) {
        return new JAXBElement<String>(_DSLAMINFOTipoTarjeta_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Ultima_Bajada", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOUltimaBajada(String value) {
        return new JAXBElement<String>(_DSLAMINFOUltimaBajada_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Restrt_Cnt", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFORestrtCnt(String value) {
        return new JAXBElement<String>(_DSLAMINFORestrtCnt_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "distancia", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFODistancia(String value) {
        return new JAXBElement<String>(_DSLAMINFODistancia_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Session_Id_IPTV", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOSessionIdIPTV(String value) {
        return new JAXBElement<String>(_DSLAMINFOSessionIdIPTV_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "VLAN_id", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOVLANId(String value) {
        return new JAXBElement<String>(_DSLAMINFOVLANId_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "CustomerID", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOCustomerID(String value) {
        return new JAXBElement<String>(_DSLAMINFOCustomerID_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Error_Status", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOErrorStatus(String value) {
        return new JAXBElement<String>(_DSLAMINFOErrorStatus_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DIAGNOSTICO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "DiagnosticoCobre", scope = DSLAMINFO.class)
    public JAXBElement<DIAGNOSTICO> createDSLAMINFODiagnosticoCobre(DIAGNOSTICO value) {
        return new JAXBElement<DIAGNOSTICO>(_DSLAMINFODiagnosticoCobre_QNAME, DIAGNOSTICO.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Nombre_Profile", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFONombreProfile(String value) {
        return new JAXBElement<String>(_DSLAMINFONombreProfile_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Ultimo_Sincronismo", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOUltimoSincronismo(String value) {
        return new JAXBElement<String>(_DSLAMINFOUltimoSincronismo_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FACILITY }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Facilida", scope = DSLAMINFO.class)
    public JAXBElement<FACILITY> createDSLAMINFOFacilida(FACILITY value) {
        return new JAXBElement<FACILITY>(_GPONINFOFacilida_QNAME, FACILITY.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "User", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOUser(String value) {
        return new JAXBElement<String>(_DSLAMINFOUser_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Cabina", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOCabina(String value) {
        return new JAXBElement<String>(_DSLAMINFOCabina_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Availability", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOAvailability(String value) {
        return new JAXBElement<String>(_DSLAMINFOAvailability_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Serial_Modem", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOSerialModem(String value) {
        return new JAXBElement<String>(_DSLAMINFOSerialModem_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "Actual_Type", scope = DSLAMINFO.class)
    public JAXBElement<String> createDSLAMINFOActualType(String value) {
        return new JAXBElement<String>(_DSLAMINFOActualType_QNAME, String.class, DSLAMINFO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/CLARO.DSLAM.INTERFACE.Model", name = "TestResult", scope = PortStatus.class)
    public JAXBElement<String> createPortStatusTestResult(String value) {
        return new JAXBElement<String>(_PortStatusTestResult_QNAME, String.class, PortStatus.class, value);
    }

}
