package satsservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the satsservices package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

	private final static QName _ArrayOfClientSubscription_QNAME = new QName("SATSServices", "ArrayOfClientSubscription");
	private final static QName _OperationHistory_QNAME = new QName("SATSServices", "OperationHistory");
	private final static QName _Job_QNAME = new QName("SATSServices", "Job");
	private final static QName _STBInfo_QNAME = new QName("SATSServices", "STBInfo");
	private final static QName _CommandHistory_QNAME = new QName("SATSServices", "CommandHistory");
	private final static QName _ClientSubscription_QNAME = new QName("SATSServices", "ClientSubscription");
	private final static QName _ClientProfile_QNAME = new QName("SATSServices", "ClientProfile");
	private final static QName _String_QNAME = new QName("SATSServices", "string");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: satsservices
	 *
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link AddPackage }
	 *
	 */
	public AddPackage createAddPackage() {
		return new AddPackage();
	}

	/**
	 * Create an instance of {@link CreateCustomerResponse }
	 *
	 */
	public CreateCustomerResponse createCreateCustomerResponse() {
		return new CreateCustomerResponse();
	}

	/**
	 * Create an instance of {@link GetClientProfileBySTB }
	 *
	 */
	public GetClientProfileBySTB createGetClientProfileBySTB() {
		return new GetClientProfileBySTB();
	}

	/**
	 * Create an instance of {@link ArrayOfDTHCommand }
	 *
	 */
	public ArrayOfDTHCommand createArrayOfDTHCommand() {
		return new ArrayOfDTHCommand();
	}

	/**
	 * Create an instance of {@link RemovePackage }
	 *
	 */
	public RemovePackage createRemovePackage() {
		return new RemovePackage();
	}

	/**
	 * Create an instance of {@link GetOperationsHistory }
	 *
	 */
	public GetOperationsHistory createGetOperationsHistory() {
		return new GetOperationsHistory();
	}

	/**
	 * Create an instance of {@link UnlockSmartcard }
	 *
	 */
	public UnlockSmartcard createUnlockSmartcard() {
		return new UnlockSmartcard();
	}

	/**
	 * Create an instance of {@link RefreshPackage }
	 *
	 */
	public RefreshPackage createRefreshPackage() {
		return new RefreshPackage();
	}

	/**
	 * Create an instance of {@link SendToBlacklistResponse }
	 *
	 */
	public SendToBlacklistResponse createSendToBlacklistResponse() {
		return new SendToBlacklistResponse();
	}

	/**
	 * Create an instance of {@link ClearMessagesResponse }
	 *
	 */
	public ClearMessagesResponse createClearMessagesResponse() {
		return new ClearMessagesResponse();
	}

	/**
	 * Create an instance of {@link GetBlacklistStatusResponse }
	 *
	 */
	public GetBlacklistStatusResponse createGetBlacklistStatusResponse() {
		return new GetBlacklistStatusResponse();
	}

	/**
	 * Create an instance of {@link SendMessagesResponse }
	 *
	 */
	public SendMessagesResponse createSendMessagesResponse() {
		return new SendMessagesResponse();
	}

	/**
	 * Create an instance of {@link ResetResponse }
	 *
	 */
	public ResetResponse createResetResponse() {
		return new ResetResponse();
	}

	/**
	 * Create an instance of {@link ArrayOfDTHEquipment }
	 *
	 */
	public ArrayOfDTHEquipment createArrayOfDTHEquipment() {
		return new ArrayOfDTHEquipment();
	}

	/**
	 * Create an instance of {@link UnlockSmartcardResponse }
	 *
	 */
	public UnlockSmartcardResponse createUnlockSmartcardResponse() {
		return new UnlockSmartcardResponse();
	}

	/**
	 * Create an instance of {@link DTHCommand }
	 *
	 */
	public DTHCommand createDTHCommand() {
		return new DTHCommand();
	}

	/**
	 * Create an instance of {@link AddPackageResponse }
	 *
	 */
	public AddPackageResponse createAddPackageResponse() {
		return new AddPackageResponse();
	}

	/**
	 * Create an instance of {@link ChangeCronjob }
	 *
	 */
	public ChangeCronjob createChangeCronjob() {
		return new ChangeCronjob();
	}

	/**
	 * Create an instance of {@link STBInfo }
	 *
	 */
	public STBInfo createSTBInfo() {
		return new STBInfo();
	}

	/**
	 * Create an instance of {@link ArrayOfDTHSpace }
	 *
	 */
	public ArrayOfDTHSpace createArrayOfDTHSpace() {
		return new ArrayOfDTHSpace();
	}

	/**
	 * Create an instance of {@link RefreshResponse }
	 *
	 */
	public RefreshResponse createRefreshResponse() {
		return new RefreshResponse();
	}

	/**
	 * Create an instance of {@link DTHEquipment }
	 *
	 */
	public DTHEquipment createDTHEquipment() {
		return new DTHEquipment();
	}

	/**
	 * Create an instance of {@link ActivateSTBResponse }
	 *
	 */
	public ActivateSTBResponse createActivateSTBResponse() {
		return new ActivateSTBResponse();
	}

	/**
	 * Create an instance of {@link UnPairSmartcard }
	 *
	 */
	public UnPairSmartcard createUnPairSmartcard() {
		return new UnPairSmartcard();
	}

	/**
	 * Create an instance of {@link Job }
	 *
	 */
	public Job createJob() {
		return new Job();
	}

	/**
	 * Create an instance of {@link Note }
	 *
	 */
	public Note createNote() {
		return new Note();
	}

	/**
	 * Create an instance of {@link GetSpaceCountResponse }
	 *
	 */
	public GetSpaceCountResponse createGetSpaceCountResponse() {
		return new GetSpaceCountResponse();
	}

	/**
	 * Create an instance of {@link Refresh }
	 *
	 */
	public Refresh createRefresh() {
		return new Refresh();
	}

	/**
	 * Create an instance of {@link DTHSubscription }
	 *
	 */
	public DTHSubscription createDTHSubscription() {
		return new DTHSubscription();
	}

	/**
	 * Create an instance of {@link UnPairSmartcardResponse }
	 *
	 */
	public UnPairSmartcardResponse createUnPairSmartcardResponse() {
		return new UnPairSmartcardResponse();
	}

	/**
	 * Create an instance of {@link GetSTBInfo }
	 *
	 */
	public GetSTBInfo createGetSTBInfo() {
		return new GetSTBInfo();
	}

	/**
	 * Create an instance of {@link RemoveClientProfile }
	 *
	 */
	public RemoveClientProfile createRemoveClientProfile() {
		return new RemoveClientProfile();
	}

	/**
	 * Create an instance of {@link GetClientProfileResponse }
	 *
	 */
	public GetClientProfileResponse createGetClientProfileResponse() {
		return new GetClientProfileResponse();
	}

	/**
	 * Create an instance of {@link ChangePlan }
	 *
	 */
	public ChangePlan createChangePlan() {
		return new ChangePlan();
	}

	/**
	 * Create an instance of {@link GetJobInformation }
	 *
	 */
	public GetJobInformation createGetJobInformation() {
		return new GetJobInformation();
	}

	/**
	 * Create an instance of {@link CreateSpace }
	 *
	 */
	public CreateSpace createCreateSpace() {
		return new CreateSpace();
	}

	/**
	 * Create an instance of {@link ClientSubscription }
	 *
	 */
	public ClientSubscription createClientSubscription() {
		return new ClientSubscription();
	}

	/**
	 * Create an instance of {@link GetSpaceCount }
	 *
	 */
	public GetSpaceCount createGetSpaceCount() {
		return new GetSpaceCount();
	}

	/**
	 * Create an instance of {@link GetClientProfileById }
	 *
	 */
	public GetClientProfileById createGetClientProfileById() {
		return new GetClientProfileById();
	}

	/**
	 * Create an instance of {@link GetJobInformationResponse }
	 *
	 */
	public GetJobInformationResponse createGetJobInformationResponse() {
		return new GetJobInformationResponse();
	}

	/**
	 * Create an instance of {@link ClientProfile }
	 *
	 */
	public ClientProfile createClientProfile() {
		return new ClientProfile();
	}

	/**
	 * Create an instance of {@link PairSmartcardSTBResponse }
	 *
	 */
	public PairSmartcardSTBResponse createPairSmartcardSTBResponse() {
		return new PairSmartcardSTBResponse();
	}

	/**
	 * Create an instance of {@link ArrayOfClientSubscription }
	 *
	 */
	public ArrayOfClientSubscription createArrayOfClientSubscription() {
		return new ArrayOfClientSubscription();
	}

	/**
	 * Create an instance of {@link SendToBlacklist }
	 *
	 */
	public SendToBlacklist createSendToBlacklist() {
		return new SendToBlacklist();
	}

	/**
	 * Create an instance of {@link GetCommandsHistoryResponse }
	 *
	 */
	public GetCommandsHistoryResponse createGetCommandsHistoryResponse() {
		return new GetCommandsHistoryResponse();
	}

	/**
	 * Create an instance of {@link RemoveSpaceResponse }
	 *
	 */
	public RemoveSpaceResponse createRemoveSpaceResponse() {
		return new RemoveSpaceResponse();
	}

	/**
	 * Create an instance of {@link RefreshPackageResponse }
	 *
	 */
	public RefreshPackageResponse createRefreshPackageResponse() {
		return new RefreshPackageResponse();
	}

	/**
	 * Create an instance of {@link GetClientProfile }
	 *
	 */
	public GetClientProfile createGetClientProfile() {
		return new GetClientProfile();
	}

	/**
	 * Create an instance of {@link GetCommandsHistory }
	 *
	 */
	public GetCommandsHistory createGetCommandsHistory() {
		return new GetCommandsHistory();
	}

	/**
	 * Create an instance of {@link OperationHistory }
	 *
	 */
	public OperationHistory createOperationHistory() {
		return new OperationHistory();
	}

	/**
	 * Create an instance of {@link PairSmartcardSTB }
	 *
	 */
	public PairSmartcardSTB createPairSmartcardSTB() {
		return new PairSmartcardSTB();
	}

	/**
	 * Create an instance of {@link RemoveSpace }
	 *
	 */
	public RemoveSpace createRemoveSpace() {
		return new RemoveSpace();
	}

	/**
	 * Create an instance of {@link ChangePlanResponse }
	 *
	 */
	public ChangePlanResponse createChangePlanResponse() {
		return new ChangePlanResponse();
	}

	/**
	 * Create an instance of {@link RemovePackageResponse }
	 *
	 */
	public RemovePackageResponse createRemovePackageResponse() {
		return new RemovePackageResponse();
	}

	/**
	 * Create an instance of {@link ChangeStatesEquipment }
	 *
	 */
	public ChangeStatesEquipment createChangeStatesEquipment() {
		return new ChangeStatesEquipment();
	}

	/**
	 * Create an instance of {@link DTHPlan }
	 *
	 */
	public DTHPlan createDTHPlan() {
		return new DTHPlan();
	}

	/**
	 * Create an instance of {@link ArrayOfTask }
	 *
	 */
	public ArrayOfTask createArrayOfTask() {
		return new ArrayOfTask();
	}

	/**
	 * Create an instance of {@link GetClientProfileBySTBResponse }
	 *
	 */
	public GetClientProfileBySTBResponse createGetClientProfileBySTBResponse() {
		return new GetClientProfileBySTBResponse();
	}

	/**
	 * Create an instance of {@link Reset }
	 *
	 */
	public Reset createReset() {
		return new Reset();
	}

	/**
	 * Create an instance of {@link SendMessages }
	 *
	 */
	public SendMessages createSendMessages() {
		return new SendMessages();
	}

	/**
	 * Create an instance of {@link ChangeResponse }
	 *
	 */
	public ChangeResponse createChangeResponse() {
		return new ChangeResponse();
	}

	/**
	 * Create an instance of {@link ChangeStatesEquipmentResponse }
	 *
	 */
	public ChangeStatesEquipmentResponse createChangeStatesEquipmentResponse() {
		return new ChangeStatesEquipmentResponse();
	}

	/**
	 * Create an instance of {@link DTHPackage }
	 *
	 */
	public DTHPackage createDTHPackage() {
		return new DTHPackage();
	}

	/**
	 * Create an instance of {@link ReactivateResponse }
	 *
	 */
	public ReactivateResponse createReactivateResponse() {
		return new ReactivateResponse();
	}

	/**
	 * Create an instance of {@link SuspendResponse }
	 *
	 */
	public SuspendResponse createSuspendResponse() {
		return new SuspendResponse();
	}

	/**
	 * Create an instance of {@link ActivateSTB }
	 *
	 */
	public ActivateSTB createActivateSTB() {
		return new ActivateSTB();
	}

	/**
	 * Create an instance of {@link GetClientProfileByIdResponse }
	 *
	 */
	public GetClientProfileByIdResponse createGetClientProfileByIdResponse() {
		return new GetClientProfileByIdResponse();
	}

	/**
	 * Create an instance of {@link DTHOperation }
	 *
	 */
	public DTHOperation createDTHOperation() {
		return new DTHOperation();
	}

	/**
	 * Create an instance of {@link Reactivate }
	 *
	 */
	public Reactivate createReactivate() {
		return new Reactivate();
	}

	/**
	 * Create an instance of {@link CreateSpaceResponse }
	 *
	 */
	public CreateSpaceResponse createCreateSpaceResponse() {
		return new CreateSpaceResponse();
	}

	/**
	 * Create an instance of {@link GetSTBInfoResponse }
	 *
	 */
	public GetSTBInfoResponse createGetSTBInfoResponse() {
		return new GetSTBInfoResponse();
	}

	/**
	 * Create an instance of {@link CreateCustomer }
	 *
	 */
	public CreateCustomer createCreateCustomer() {
		return new CreateCustomer();
	}

	/**
	 * Create an instance of {@link CommandHistory }
	 *
	 */
	public CommandHistory createCommandHistory() {
		return new CommandHistory();
	}

	/**
	 * Create an instance of {@link RemoveClientProfileResponse }
	 *
	 */
	public RemoveClientProfileResponse createRemoveClientProfileResponse() {
		return new RemoveClientProfileResponse();
	}

	/**
	 * Create an instance of {@link Client }
	 *
	 */
	public Client createClient() {
		return new Client();
	}

	/**
	 * Create an instance of {@link ArrayOfNote }
	 *
	 */
	public ArrayOfNote createArrayOfNote() {
		return new ArrayOfNote();
	}

	/**
	 * Create an instance of {@link Task }
	 *
	 */
	public Task createTask() {
		return new Task();
	}

	/**
	 * Create an instance of {@link GetOperationsHistoryResponse }
	 *
	 */
	public GetOperationsHistoryResponse createGetOperationsHistoryResponse() {
		return new GetOperationsHistoryResponse();
	}

	/**
	 * Create an instance of {@link ArrayOfDTHPackage }
	 *
	 */
	public ArrayOfDTHPackage createArrayOfDTHPackage() {
		return new ArrayOfDTHPackage();
	}

	/**
	 * Create an instance of {@link DTHJob }
	 *
	 */
	public DTHJob createDTHJob() {
		return new DTHJob();
	}

	/**
	 * Create an instance of {@link Order }
	 *
	 */
	public Order createOrder() {
		return new Order();
	}

	/**
	 * Create an instance of {@link ClearMessages }
	 *
	 */
	public ClearMessages createClearMessages() {
		return new ClearMessages();
	}

	/**
	 * Create an instance of {@link Suspend }
	 *
	 */
	public Suspend createSuspend() {
		return new Suspend();
	}

	/**
	 * Create an instance of {@link GetBlacklistStatus }
	 *
	 */
	public GetBlacklistStatus createGetBlacklistStatus() {
		return new GetBlacklistStatus();
	}

	/**
	 * Create an instance of {@link DTHSpace }
	 *
	 */
	public DTHSpace createDTHSpace() {
		return new DTHSpace();
	}

	/**
	 * Create an instance of {@link Subscription }
	 *
	 */
	public Subscription createSubscription() {
		return new Subscription();
	}

	/**
	 * Create an instance of {@link ChangeCronjobResponse }
	 *
	 */
	public ChangeCronjobResponse createChangeCronjobResponse() {
		return new ChangeCronjobResponse();
	}

	/**
	 * Create an instance of {@link Change }
	 *
	 */
	public Change createChange() {
		return new Change();
	}

	/**
	 * Create an instance of {@link ArrayOfOrder }
	 *
	 */
	public ArrayOfOrder createArrayOfOrder() {
		return new ArrayOfOrder();
	}

	/**
	 * Create an instance of {@link ArrayOfDTHOperation }
	 *
	 */
	public ArrayOfDTHOperation createArrayOfDTHOperation() {
		return new ArrayOfDTHOperation();
	}

	/**
	 * Create an instance of
	 * {@link JAXBElement }{@code <}{@link ArrayOfClientSubscription }{@code >}}
	 *
	 */
	@XmlElementDecl(namespace = "SATSServices", name = "ArrayOfClientSubscription")
	public JAXBElement<ArrayOfClientSubscription> createArrayOfClientSubscription(ArrayOfClientSubscription value) {
		return new JAXBElement<ArrayOfClientSubscription>(_ArrayOfClientSubscription_QNAME, ArrayOfClientSubscription.class, null, value);
	}

	/**
	 * Create an instance of
	 * {@link JAXBElement }{@code <}{@link OperationHistory }{@code >}}
	 *
	 */
	@XmlElementDecl(namespace = "SATSServices", name = "OperationHistory")
	public JAXBElement<OperationHistory> createOperationHistory(OperationHistory value) {
		return new JAXBElement<OperationHistory>(_OperationHistory_QNAME, OperationHistory.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Job }{@code >}}
	 *
	 */
	@XmlElementDecl(namespace = "SATSServices", name = "Job")
	public JAXBElement<Job> createJob(Job value) {
		return new JAXBElement<Job>(_Job_QNAME, Job.class, null, value);
	}

	/**
	 * Create an instance of
	 * {@link JAXBElement }{@code <}{@link STBInfo }{@code >}}
	 *
	 */
	@XmlElementDecl(namespace = "SATSServices", name = "STBInfo")
	public JAXBElement<STBInfo> createSTBInfo(STBInfo value) {
		return new JAXBElement<STBInfo>(_STBInfo_QNAME, STBInfo.class, null, value);
	}

	/**
	 * Create an instance of
	 * {@link JAXBElement }{@code <}{@link CommandHistory }{@code >}}
	 *
	 */
	@XmlElementDecl(namespace = "SATSServices", name = "CommandHistory")
	public JAXBElement<CommandHistory> createCommandHistory(CommandHistory value) {
		return new JAXBElement<CommandHistory>(_CommandHistory_QNAME, CommandHistory.class, null, value);
	}

	/**
	 * Create an instance of
	 * {@link JAXBElement }{@code <}{@link ClientSubscription }{@code >}}
	 *
	 */
	@XmlElementDecl(namespace = "SATSServices", name = "ClientSubscription")
	public JAXBElement<ClientSubscription> createClientSubscription(ClientSubscription value) {
		return new JAXBElement<ClientSubscription>(_ClientSubscription_QNAME, ClientSubscription.class, null, value);
	}

	/**
	 * Create an instance of
	 * {@link JAXBElement }{@code <}{@link ClientProfile }{@code >}}
	 *
	 */
	@XmlElementDecl(namespace = "SATSServices", name = "ClientProfile")
	public JAXBElement<ClientProfile> createClientProfile(ClientProfile value) {
		return new JAXBElement<ClientProfile>(_ClientProfile_QNAME, ClientProfile.class, null, value);
	}

	/**
	 * Create an instance of
	 * {@link JAXBElement }{@code <}{@link String }{@code >}}
	 *
	 */
	@XmlElementDecl(namespace = "SATSServices", name = "string")
	public JAXBElement<String> createString(String value) {
		return new JAXBElement<String>(_String_QNAME, String.class, null, value);
	}

}
