
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Smartcard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Grilla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "smartcard",
    "stb",
    "grilla"
})
@XmlRootElement(name = "PairSmartcardSTB")
public class PairSmartcardSTB {

    @XmlElement(name = "Smartcard")
    protected String smartcard;
    @XmlElement(name = "STB")
    protected String stb;
    @XmlElement(name = "Grilla")
    protected String grilla;

    /**
     * Gets the value of the smartcard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmartcard() {
        return smartcard;
    }

    /**
     * Sets the value of the smartcard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmartcard(String value) {
        this.smartcard = value;
    }

    /**
     * Gets the value of the stb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTB() {
        return stb;
    }

    /**
     * Sets the value of the stb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTB(String value) {
        this.stb = value;
    }

    /**
     * Gets the value of the grilla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrilla() {
        return grilla;
    }

    /**
     * Sets the value of the grilla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrilla(String value) {
        this.grilla = value;
    }

}
