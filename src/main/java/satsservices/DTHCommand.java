
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DTHCommand complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DTHCommand">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="User" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Ficticio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Espacio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Set_Top_Box" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Smartcard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Command" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DTHCommand", propOrder = {
    "user",
    "ficticio",
    "espacio",
    "setTopBox",
    "smartcard",
    "command",
    "date"
})
public class DTHCommand {

    @XmlElement(name = "User")
    protected String user;
    @XmlElement(name = "Ficticio")
    protected String ficticio;
    @XmlElement(name = "Espacio")
    protected String espacio;
    @XmlElement(name = "Set_Top_Box")
    protected String setTopBox;
    @XmlElement(name = "Smartcard")
    protected String smartcard;
    @XmlElement(name = "Command")
    protected String command;
    @XmlElement(name = "Date")
    protected String date;

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

    /**
     * Gets the value of the ficticio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFicticio() {
        return ficticio;
    }

    /**
     * Sets the value of the ficticio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFicticio(String value) {
        this.ficticio = value;
    }

    /**
     * Gets the value of the espacio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEspacio() {
        return espacio;
    }

    /**
     * Sets the value of the espacio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEspacio(String value) {
        this.espacio = value;
    }

    /**
     * Gets the value of the setTopBox property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetTopBox() {
        return setTopBox;
    }

    /**
     * Sets the value of the setTopBox property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetTopBox(String value) {
        this.setTopBox = value;
    }

    /**
     * Gets the value of the smartcard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmartcard() {
        return smartcard;
    }

    /**
     * Sets the value of the smartcard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmartcard(String value) {
        this.smartcard = value;
    }

    /**
     * Gets the value of the command property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommand() {
        return command;
    }

    /**
     * Sets the value of the command property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommand(String value) {
        this.command = value;
    }

    /**
     * Gets the value of the date property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDate(String value) {
        this.date = value;
    }

}
