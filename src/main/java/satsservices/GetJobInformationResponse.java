
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetJobInformationResult" type="{SATSServices}Job" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getJobInformationResult"
})
@XmlRootElement(name = "GetJobInformationResponse")
public class GetJobInformationResponse {

    @XmlElement(name = "GetJobInformationResult")
    protected Job getJobInformationResult;

    /**
     * Gets the value of the getJobInformationResult property.
     * 
     * @return
     *     possible object is
     *     {@link Job }
     *     
     */
    public Job getGetJobInformationResult() {
        return getJobInformationResult;
    }

    /**
     * Sets the value of the getJobInformationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Job }
     *     
     */
    public void setGetJobInformationResult(Job value) {
        this.getJobInformationResult = value;
    }

}
