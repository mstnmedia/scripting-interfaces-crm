
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DTHPackage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DTHPackage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServItemId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="Plancode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DTHPackage", propOrder = {
    "servItemId",
    "plancode",
    "description"
})
public class DTHPackage {

    @XmlElement(name = "ServItemId")
    protected long servItemId;
    @XmlElement(name = "Plancode")
    protected String plancode;
    @XmlElement(name = "Description")
    protected String description;

    /**
     * Gets the value of the servItemId property.
     * 
     */
    public long getServItemId() {
        return servItemId;
    }

    /**
     * Sets the value of the servItemId property.
     * 
     */
    public void setServItemId(long value) {
        this.servItemId = value;
    }

    /**
     * Gets the value of the plancode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlancode() {
        return plancode;
    }

    /**
     * Sets the value of the plancode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlancode(String value) {
        this.plancode = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
