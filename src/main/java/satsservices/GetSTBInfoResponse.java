
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getSTBInfoResult" type="{SATSServices}STBInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSTBInfoResult"
})
@XmlRootElement(name = "getSTBInfoResponse")
public class GetSTBInfoResponse {

    protected STBInfo getSTBInfoResult;

    /**
     * Gets the value of the getSTBInfoResult property.
     * 
     * @return
     *     possible object is
     *     {@link STBInfo }
     *     
     */
    public STBInfo getGetSTBInfoResult() {
        return getSTBInfoResult;
    }

    /**
     * Sets the value of the getSTBInfoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link STBInfo }
     *     
     */
    public void setGetSTBInfoResult(STBInfo value) {
        this.getSTBInfoResult = value;
    }

}
