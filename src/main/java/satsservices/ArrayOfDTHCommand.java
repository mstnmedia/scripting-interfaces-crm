
package satsservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDTHCommand complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDTHCommand">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DTHCommand" type="{SATSServices}DTHCommand" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDTHCommand", propOrder = {
    "dthCommand"
})
public class ArrayOfDTHCommand {

    @XmlElement(name = "DTHCommand", nillable = true)
    protected List<DTHCommand> dthCommand;

    /**
     * Gets the value of the dthCommand property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dthCommand property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDTHCommand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DTHCommand }
     * 
     * 
     */
    public List<DTHCommand> getDTHCommand() {
        if (dthCommand == null) {
            dthCommand = new ArrayList<DTHCommand>();
        }
        return this.dthCommand;
    }

}
