
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UnlockSmartcardResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "unlockSmartcardResult"
})
@XmlRootElement(name = "UnlockSmartcardResponse")
public class UnlockSmartcardResponse {

    @XmlElement(name = "UnlockSmartcardResult")
    protected String unlockSmartcardResult;

    /**
     * Gets the value of the unlockSmartcardResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnlockSmartcardResult() {
        return unlockSmartcardResult;
    }

    /**
     * Sets the value of the unlockSmartcardResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnlockSmartcardResult(String value) {
        this.unlockSmartcardResult = value;
    }

}
