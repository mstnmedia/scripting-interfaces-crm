
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetClientProfileByIdResult" type="{SATSServices}ClientSubscription" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getClientProfileByIdResult"
})
@XmlRootElement(name = "GetClientProfileByIdResponse")
public class GetClientProfileByIdResponse {

    @XmlElement(name = "GetClientProfileByIdResult")
    protected ClientSubscription getClientProfileByIdResult;

    /**
     * Gets the value of the getClientProfileByIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link ClientSubscription }
     *     
     */
    public ClientSubscription getGetClientProfileByIdResult() {
        return getClientProfileByIdResult;
    }

    /**
     * Sets the value of the getClientProfileByIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientSubscription }
     *     
     */
    public void setGetClientProfileByIdResult(ClientSubscription value) {
        this.getClientProfileByIdResult = value;
    }

}
