
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendMessagesResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendMessagesResult"
})
@XmlRootElement(name = "SendMessagesResponse")
public class SendMessagesResponse {

    @XmlElement(name = "SendMessagesResult")
    protected String sendMessagesResult;

    /**
     * Gets the value of the sendMessagesResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendMessagesResult() {
        return sendMessagesResult;
    }

    /**
     * Sets the value of the sendMessagesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendMessagesResult(String value) {
        this.sendMessagesResult = value;
    }

}
