
package satsservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDTHSpace complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDTHSpace">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DTHSpace" type="{SATSServices}DTHSpace" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDTHSpace", propOrder = {
    "dthSpace"
})
public class ArrayOfDTHSpace {

    @XmlElement(name = "DTHSpace", nillable = true)
    protected List<DTHSpace> dthSpace;

    /**
     * Gets the value of the dthSpace property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dthSpace property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDTHSpace().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DTHSpace }
     * 
     * 
     */
    public List<DTHSpace> getDTHSpace() {
        if (dthSpace == null) {
            dthSpace = new ArrayList<DTHSpace>();
        }
        return this.dthSpace;
    }

}
