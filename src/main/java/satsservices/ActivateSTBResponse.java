
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActivateSTBResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "activateSTBResult"
})
@XmlRootElement(name = "ActivateSTBResponse")
public class ActivateSTBResponse {

    @XmlElement(name = "ActivateSTBResult")
    protected String activateSTBResult;

    /**
     * Gets the value of the activateSTBResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivateSTBResult() {
        return activateSTBResult;
    }

    /**
     * Sets the value of the activateSTBResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivateSTBResult(String value) {
        this.activateSTBResult = value;
    }

}
