
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Smartcard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SetTopBox" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoQuitese" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "smartcard",
    "setTopBox",
    "customerId",
    "tipoQuitese"
})
@XmlRootElement(name = "SendToBlacklist")
public class SendToBlacklist {

    @XmlElement(name = "Smartcard")
    protected String smartcard;
    @XmlElement(name = "SetTopBox")
    protected String setTopBox;
    @XmlElement(name = "CustomerId")
    protected String customerId;
    @XmlElement(name = "TipoQuitese")
    protected String tipoQuitese;

    /**
     * Gets the value of the smartcard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmartcard() {
        return smartcard;
    }

    /**
     * Sets the value of the smartcard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmartcard(String value) {
        this.smartcard = value;
    }

    /**
     * Gets the value of the setTopBox property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetTopBox() {
        return setTopBox;
    }

    /**
     * Sets the value of the setTopBox property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetTopBox(String value) {
        this.setTopBox = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the tipoQuitese property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoQuitese() {
        return tipoQuitese;
    }

    /**
     * Sets the value of the tipoQuitese property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoQuitese(String value) {
        this.tipoQuitese = value;
    }

}
