
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DTHSpace complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DTHSpace">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServItemId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="IdProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STBTypeCRMId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxResolution" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STBId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmartCardId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Created" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Active" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DTHSpace", propOrder = {
    "servItemId",
    "idProducto",
    "stbTypeCRMId",
    "maxResolution",
    "stbId",
    "smartCardId",
    "created",
    "active"
})
public class DTHSpace {

    @XmlElement(name = "ServItemId")
    protected long servItemId;
    @XmlElement(name = "IdProducto")
    protected String idProducto;
    @XmlElement(name = "STBTypeCRMId")
    protected String stbTypeCRMId;
    @XmlElement(name = "MaxResolution")
    protected String maxResolution;
    @XmlElement(name = "STBId")
    protected String stbId;
    @XmlElement(name = "SmartCardId")
    protected String smartCardId;
    @XmlElement(name = "Created")
    protected boolean created;
    @XmlElement(name = "Active")
    protected boolean active;

    /**
     * Gets the value of the servItemId property.
     * 
     */
    public long getServItemId() {
        return servItemId;
    }

    /**
     * Sets the value of the servItemId property.
     * 
     */
    public void setServItemId(long value) {
        this.servItemId = value;
    }

    /**
     * Gets the value of the idProducto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProducto() {
        return idProducto;
    }

    /**
     * Sets the value of the idProducto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProducto(String value) {
        this.idProducto = value;
    }

    /**
     * Gets the value of the stbTypeCRMId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTBTypeCRMId() {
        return stbTypeCRMId;
    }

    /**
     * Sets the value of the stbTypeCRMId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTBTypeCRMId(String value) {
        this.stbTypeCRMId = value;
    }

    /**
     * Gets the value of the maxResolution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxResolution() {
        return maxResolution;
    }

    /**
     * Sets the value of the maxResolution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxResolution(String value) {
        this.maxResolution = value;
    }

    /**
     * Gets the value of the stbId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTBId() {
        return stbId;
    }

    /**
     * Sets the value of the stbId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTBId(String value) {
        this.stbId = value;
    }

    /**
     * Gets the value of the smartCardId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmartCardId() {
        return smartCardId;
    }

    /**
     * Sets the value of the smartCardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmartCardId(String value) {
        this.smartCardId = value;
    }

    /**
     * Gets the value of the created property.
     * 
     */
    public boolean isCreated() {
        return created;
    }

    /**
     * Sets the value of the created property.
     * 
     */
    public void setCreated(boolean value) {
        this.created = value;
    }

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

}
