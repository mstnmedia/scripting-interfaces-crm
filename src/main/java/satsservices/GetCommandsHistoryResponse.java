
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCommandsHistoryResult" type="{SATSServices}CommandHistory" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCommandsHistoryResult"
})
@XmlRootElement(name = "GetCommandsHistoryResponse")
public class GetCommandsHistoryResponse {

    @XmlElement(name = "GetCommandsHistoryResult")
    protected CommandHistory getCommandsHistoryResult;

    /**
     * Gets the value of the getCommandsHistoryResult property.
     * 
     * @return
     *     possible object is
     *     {@link CommandHistory }
     *     
     */
    public CommandHistory getGetCommandsHistoryResult() {
        return getCommandsHistoryResult;
    }

    /**
     * Sets the value of the getCommandsHistoryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommandHistory }
     *     
     */
    public void setGetCommandsHistoryResult(CommandHistory value) {
        this.getCommandsHistoryResult = value;
    }

}
