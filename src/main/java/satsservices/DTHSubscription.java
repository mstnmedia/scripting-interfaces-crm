
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DTHSubscription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DTHSubscription">
 *   &lt;complexContent>
 *     &lt;extension base="{SATSServices}Subscription">
 *       &lt;sequence>
 *         &lt;element name="Plan" type="{SATSServices}DTHPlan" minOccurs="0"/>
 *         &lt;element name="Spaces" type="{SATSServices}ArrayOfDTHSpace" minOccurs="0"/>
 *         &lt;element name="Packages" type="{SATSServices}ArrayOfDTHPackage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DTHSubscription", propOrder = {
    "plan",
    "spaces",
    "packages"
})
public class DTHSubscription
    extends Subscription
{

    @XmlElement(name = "Plan")
    protected DTHPlan plan;
    @XmlElement(name = "Spaces")
    protected ArrayOfDTHSpace spaces;
    @XmlElement(name = "Packages")
    protected ArrayOfDTHPackage packages;

    /**
     * Gets the value of the plan property.
     * 
     * @return
     *     possible object is
     *     {@link DTHPlan }
     *     
     */
    public DTHPlan getPlan() {
        return plan;
    }

    /**
     * Sets the value of the plan property.
     * 
     * @param value
     *     allowed object is
     *     {@link DTHPlan }
     *     
     */
    public void setPlan(DTHPlan value) {
        this.plan = value;
    }

    /**
     * Gets the value of the spaces property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDTHSpace }
     *     
     */
    public ArrayOfDTHSpace getSpaces() {
        return spaces;
    }

    /**
     * Sets the value of the spaces property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDTHSpace }
     *     
     */
    public void setSpaces(ArrayOfDTHSpace value) {
        this.spaces = value;
    }

    /**
     * Gets the value of the packages property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDTHPackage }
     *     
     */
    public ArrayOfDTHPackage getPackages() {
        return packages;
    }

    /**
     * Sets the value of the packages property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDTHPackage }
     *     
     */
    public void setPackages(ArrayOfDTHPackage value) {
        this.packages = value;
    }

}
