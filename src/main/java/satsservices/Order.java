
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Order complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Order">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="PON" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelatedOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastModification" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Notes" type="{SATSServices}ArrayOfNote" minOccurs="0"/>
 *         &lt;element name="Tasks" type="{SATSServices}ArrayOfTask" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Order", propOrder = {
    "number",
    "pon",
    "relatedOrder",
    "lastModification",
    "notes",
    "tasks"
})
public class Order {

    @XmlElement(name = "Number")
    protected long number;
    @XmlElement(name = "PON")
    protected String pon;
    @XmlElement(name = "RelatedOrder")
    protected String relatedOrder;
    @XmlElement(name = "LastModification", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModification;
    @XmlElement(name = "Notes")
    protected ArrayOfNote notes;
    @XmlElement(name = "Tasks")
    protected ArrayOfTask tasks;

    /**
     * Gets the value of the number property.
     * 
     */
    public long getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     */
    public void setNumber(long value) {
        this.number = value;
    }

    /**
     * Gets the value of the pon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPON() {
        return pon;
    }

    /**
     * Sets the value of the pon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPON(String value) {
        this.pon = value;
    }

    /**
     * Gets the value of the relatedOrder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedOrder() {
        return relatedOrder;
    }

    /**
     * Sets the value of the relatedOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedOrder(String value) {
        this.relatedOrder = value;
    }

    /**
     * Gets the value of the lastModification property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModification() {
        return lastModification;
    }

    /**
     * Sets the value of the lastModification property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModification(XMLGregorianCalendar value) {
        this.lastModification = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfNote }
     *     
     */
    public ArrayOfNote getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfNote }
     *     
     */
    public void setNotes(ArrayOfNote value) {
        this.notes = value;
    }

    /**
     * Gets the value of the tasks property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTask }
     *     
     */
    public ArrayOfTask getTasks() {
        return tasks;
    }

    /**
     * Sets the value of the tasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTask }
     *     
     */
    public void setTasks(ArrayOfTask value) {
        this.tasks = value;
    }

}
