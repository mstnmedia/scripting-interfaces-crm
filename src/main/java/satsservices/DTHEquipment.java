
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DTHEquipment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DTHEquipment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Espacio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoSTB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmartCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DTHEquipment", propOrder = {
    "espacio",
    "stb",
    "tipoSTB",
    "smartCard",
    "estado"
})
public class DTHEquipment {

    @XmlElement(name = "Espacio")
    protected String espacio;
    @XmlElement(name = "STB")
    protected String stb;
    @XmlElement(name = "TipoSTB")
    protected String tipoSTB;
    @XmlElement(name = "SmartCard")
    protected String smartCard;
    @XmlElement(name = "Estado")
    protected String estado;

    /**
     * Gets the value of the espacio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEspacio() {
        return espacio;
    }

    /**
     * Sets the value of the espacio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEspacio(String value) {
        this.espacio = value;
    }

    /**
     * Gets the value of the stb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTB() {
        return stb;
    }

    /**
     * Sets the value of the stb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTB(String value) {
        this.stb = value;
    }

    /**
     * Gets the value of the tipoSTB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoSTB() {
        return tipoSTB;
    }

    /**
     * Sets the value of the tipoSTB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoSTB(String value) {
        this.tipoSTB = value;
    }

    /**
     * Gets the value of the smartCard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmartCard() {
        return smartCard;
    }

    /**
     * Sets the value of the smartCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmartCard(String value) {
        this.smartCard = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

}
