
package satsservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDTHOperation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDTHOperation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DTHOperation" type="{SATSServices}DTHOperation" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDTHOperation", propOrder = {
    "dthOperation"
})
public class ArrayOfDTHOperation {

    @XmlElement(name = "DTHOperation", nillable = true)
    protected List<DTHOperation> dthOperation;

    /**
     * Gets the value of the dthOperation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dthOperation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDTHOperation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DTHOperation }
     * 
     * 
     */
    public List<DTHOperation> getDTHOperation() {
        if (dthOperation == null) {
            dthOperation = new ArrayList<DTHOperation>();
        }
        return this.dthOperation;
    }

}
