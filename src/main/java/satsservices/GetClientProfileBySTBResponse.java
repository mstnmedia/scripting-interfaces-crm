
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetClientProfileBySTBResult" type="{SATSServices}ArrayOfClientSubscription" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getClientProfileBySTBResult"
})
@XmlRootElement(name = "GetClientProfileBySTBResponse")
public class GetClientProfileBySTBResponse {

    @XmlElement(name = "GetClientProfileBySTBResult")
    protected ArrayOfClientSubscription getClientProfileBySTBResult;

    /**
     * Gets the value of the getClientProfileBySTBResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfClientSubscription }
     *     
     */
    public ArrayOfClientSubscription getGetClientProfileBySTBResult() {
        return getClientProfileBySTBResult;
    }

    /**
     * Sets the value of the getClientProfileBySTBResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfClientSubscription }
     *     
     */
    public void setGetClientProfileBySTBResult(ArrayOfClientSubscription value) {
        this.getClientProfileBySTBResult = value;
    }

}
