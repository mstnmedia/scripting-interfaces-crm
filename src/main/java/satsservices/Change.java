
package satsservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OpType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oldSTB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oldSMC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="newSTB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="newSMC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Technician" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "opType",
    "oldSTB",
    "oldSMC",
    "newSTB",
    "newSMC",
    "technician"
})
@XmlRootElement(name = "Change")
public class Change {

    @XmlElement(name = "Id")
    protected String id;
    @XmlElement(name = "OpType")
    protected String opType;
    protected String oldSTB;
    protected String oldSMC;
    protected String newSTB;
    protected String newSMC;
    @XmlElement(name = "Technician")
    protected String technician;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the opType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpType() {
        return opType;
    }

    /**
     * Sets the value of the opType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpType(String value) {
        this.opType = value;
    }

    /**
     * Gets the value of the oldSTB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldSTB() {
        return oldSTB;
    }

    /**
     * Sets the value of the oldSTB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldSTB(String value) {
        this.oldSTB = value;
    }

    /**
     * Gets the value of the oldSMC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldSMC() {
        return oldSMC;
    }

    /**
     * Sets the value of the oldSMC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldSMC(String value) {
        this.oldSMC = value;
    }

    /**
     * Gets the value of the newSTB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewSTB() {
        return newSTB;
    }

    /**
     * Sets the value of the newSTB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewSTB(String value) {
        this.newSTB = value;
    }

    /**
     * Gets the value of the newSMC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewSMC() {
        return newSMC;
    }

    /**
     * Sets the value of the newSMC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewSMC(String value) {
        this.newSMC = value;
    }

    /**
     * Gets the value of the technician property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnician() {
        return technician;
    }

    /**
     * Sets the value of the technician property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnician(String value) {
        this.technician = value;
    }

}
