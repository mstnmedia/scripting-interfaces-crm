
package com.claro.omsorderdetails;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for orderActionsDetailsModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="orderActionsDetailsModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternalID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNAsociado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NombreProyecto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceRequestDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Canalventas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RazonOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fecha1raInstalacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaAprov" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Product" type="{http://omsorderdetails.claro.com/}dynamicProductDetailsModel" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "orderActionsDetailsModel", propOrder = {
    "idCliente",
    "externalID",
    "snAsociado",
    "nombreProyecto",
    "serviceRequestDate",
    "applicationDate",
    "canalventas",
    "razonOrden",
    "status",
    "nameStatus",
    "fecha1RaInstalacion",
    "fechaAprov",
    "product"
})
public class OrderActionsDetailsModel {

    @XmlElement(name = "IdCliente")
    protected String idCliente;
    @XmlElement(name = "ExternalID")
    protected String externalID;
    @XmlElement(name = "SNAsociado")
    protected String snAsociado;
    @XmlElement(name = "NombreProyecto")
    protected String nombreProyecto;
    @XmlElement(name = "ServiceRequestDate")
    protected String serviceRequestDate;
    @XmlElement(name = "ApplicationDate")
    protected String applicationDate;
    @XmlElement(name = "Canalventas")
    protected String canalventas;
    @XmlElement(name = "RazonOrden")
    protected String razonOrden;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "NameStatus")
    protected String nameStatus;
    @XmlElement(name = "Fecha1raInstalacion")
    protected String fecha1RaInstalacion;
    @XmlElement(name = "FechaAprov")
    protected String fechaAprov;
    @XmlElement(name = "Product")
    protected List<DynamicProductDetailsModel> product;

    /**
     * Gets the value of the idCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCliente() {
        return idCliente;
    }

    /**
     * Sets the value of the idCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCliente(String value) {
        this.idCliente = value;
    }

    /**
     * Gets the value of the externalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalID() {
        return externalID;
    }

    /**
     * Sets the value of the externalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalID(String value) {
        this.externalID = value;
    }

    /**
     * Gets the value of the snAsociado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNAsociado() {
        return snAsociado;
    }

    /**
     * Sets the value of the snAsociado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNAsociado(String value) {
        this.snAsociado = value;
    }

    /**
     * Gets the value of the nombreProyecto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreProyecto() {
        return nombreProyecto;
    }

    /**
     * Sets the value of the nombreProyecto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreProyecto(String value) {
        this.nombreProyecto = value;
    }

    /**
     * Gets the value of the serviceRequestDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceRequestDate() {
        return serviceRequestDate;
    }

    /**
     * Sets the value of the serviceRequestDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceRequestDate(String value) {
        this.serviceRequestDate = value;
    }

    /**
     * Gets the value of the applicationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationDate() {
        return applicationDate;
    }

    /**
     * Sets the value of the applicationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationDate(String value) {
        this.applicationDate = value;
    }

    /**
     * Gets the value of the canalventas property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanalventas() {
        return canalventas;
    }

    /**
     * Sets the value of the canalventas property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanalventas(String value) {
        this.canalventas = value;
    }

    /**
     * Gets the value of the razonOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazonOrden() {
        return razonOrden;
    }

    /**
     * Sets the value of the razonOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazonOrden(String value) {
        this.razonOrden = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the nameStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameStatus() {
        return nameStatus;
    }

    /**
     * Sets the value of the nameStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameStatus(String value) {
        this.nameStatus = value;
    }

    /**
     * Gets the value of the fecha1RaInstalacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha1RaInstalacion() {
        return fecha1RaInstalacion;
    }

    /**
     * Sets the value of the fecha1RaInstalacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha1RaInstalacion(String value) {
        this.fecha1RaInstalacion = value;
    }

    /**
     * Gets the value of the fechaAprov property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAprov() {
        return fechaAprov;
    }

    /**
     * Sets the value of the fechaAprov property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAprov(String value) {
        this.fechaAprov = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the product property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProduct().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DynamicProductDetailsModel }
     * 
     * 
     */
    public List<DynamicProductDetailsModel> getProduct() {
        if (product == null) {
            product = new ArrayList<DynamicProductDetailsModel>();
        }
        return this.product;
    }

}
