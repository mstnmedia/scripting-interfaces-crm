
package com.claro.omsorderdetails;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dynamicProductDetailsModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dynamicProductDetailsModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="APID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InstalAddressId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoOrden" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeficionOferta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoComponente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fecha1raInstalacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaAprov" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Razon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Atributo" type="{http://omsorderdetails.claro.com/}attributesModel" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Component" type="{http://omsorderdetails.claro.com/}componentModel" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dynamicProductDetailsModel", propOrder = {
    "idCliente",
    "apid",
    "nombre",
    "serviceType",
    "instalAddressId",
    "tipoOrden",
    "status",
    "nameStatus",
    "deficionOferta",
    "tipoComponente",
    "fecha1RaInstalacion",
    "fechaAprov",
    "razon",
    "atributo",
    "component"
})
public class DynamicProductDetailsModel {

    @XmlElement(name = "IdCliente")
    protected String idCliente;
    @XmlElement(name = "APID")
    protected String apid;
    @XmlElement(name = "Nombre")
    protected String nombre;
    @XmlElement(name = "ServiceType")
    protected String serviceType;
    @XmlElement(name = "InstalAddressId")
    protected String instalAddressId;
    @XmlElement(name = "TipoOrden")
    protected String tipoOrden;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "NameStatus")
    protected String nameStatus;
    @XmlElement(name = "DeficionOferta")
    protected String deficionOferta;
    @XmlElement(name = "TipoComponente")
    protected String tipoComponente;
    @XmlElement(name = "Fecha1raInstalacion")
    protected String fecha1RaInstalacion;
    @XmlElement(name = "FechaAprov")
    protected String fechaAprov;
    @XmlElement(name = "Razon")
    protected String razon;
    @XmlElement(name = "Atributo")
    protected List<AttributesModel> atributo;
    @XmlElement(name = "Component")
    protected List<ComponentModel> component;

    /**
     * Gets the value of the idCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCliente() {
        return idCliente;
    }

    /**
     * Sets the value of the idCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCliente(String value) {
        this.idCliente = value;
    }

    /**
     * Gets the value of the apid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPID() {
        return apid;
    }

    /**
     * Sets the value of the apid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPID(String value) {
        this.apid = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the instalAddressId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstalAddressId() {
        return instalAddressId;
    }

    /**
     * Sets the value of the instalAddressId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstalAddressId(String value) {
        this.instalAddressId = value;
    }

    /**
     * Gets the value of the tipoOrden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOrden() {
        return tipoOrden;
    }

    /**
     * Sets the value of the tipoOrden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOrden(String value) {
        this.tipoOrden = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the nameStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameStatus() {
        return nameStatus;
    }

    /**
     * Sets the value of the nameStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameStatus(String value) {
        this.nameStatus = value;
    }

    /**
     * Gets the value of the deficionOferta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeficionOferta() {
        return deficionOferta;
    }

    /**
     * Sets the value of the deficionOferta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeficionOferta(String value) {
        this.deficionOferta = value;
    }

    /**
     * Gets the value of the tipoComponente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoComponente() {
        return tipoComponente;
    }

    /**
     * Sets the value of the tipoComponente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoComponente(String value) {
        this.tipoComponente = value;
    }

    /**
     * Gets the value of the fecha1RaInstalacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha1RaInstalacion() {
        return fecha1RaInstalacion;
    }

    /**
     * Sets the value of the fecha1RaInstalacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha1RaInstalacion(String value) {
        this.fecha1RaInstalacion = value;
    }

    /**
     * Gets the value of the fechaAprov property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAprov() {
        return fechaAprov;
    }

    /**
     * Sets the value of the fechaAprov property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAprov(String value) {
        this.fechaAprov = value;
    }

    /**
     * Gets the value of the razon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazon() {
        return razon;
    }

    /**
     * Sets the value of the razon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazon(String value) {
        this.razon = value;
    }

    /**
     * Gets the value of the atributo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the atributo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAtributo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributesModel }
     * 
     * 
     */
    public List<AttributesModel> getAtributo() {
        if (atributo == null) {
            atributo = new ArrayList<AttributesModel>();
        }
        return this.atributo;
    }

    /**
     * Gets the value of the component property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the component property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComponentModel }
     * 
     * 
     */
    public List<ComponentModel> getComponent() {
        if (component == null) {
            component = new ArrayList<ComponentModel>();
        }
        return this.component;
    }

}
