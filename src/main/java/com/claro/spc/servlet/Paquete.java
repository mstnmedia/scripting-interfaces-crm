
package com.claro.spc.servlet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Paquete complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Paquete">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fecha_asignado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="crmid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="grupo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Paquete", propOrder = {
    "fechaAsignado",
    "crmid",
    "grupo"
})
public class Paquete {

    @XmlElement(name = "fecha_asignado", required = true)
    protected String fechaAsignado;
    @XmlElement(required = true)
    protected String crmid;
    @XmlElement(required = true)
    protected String grupo;

    /**
     * Gets the value of the fechaAsignado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAsignado() {
        return fechaAsignado;
    }

    /**
     * Sets the value of the fechaAsignado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAsignado(String value) {
        this.fechaAsignado = value;
    }

    /**
     * Gets the value of the crmid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrmid() {
        return crmid;
    }

    /**
     * Sets the value of the crmid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrmid(String value) {
        this.crmid = value;
    }

    /**
     * Gets the value of the grupo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Sets the value of the grupo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupo(String value) {
        this.grupo = value;
    }

}
