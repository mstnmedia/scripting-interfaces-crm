
package com.claro.spc.servlet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for STB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="STB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="component_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecha_activacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecha_creacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="global_component_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="guid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="modelo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "STB", propOrder = {
    "componentId",
    "dvr",
    "fechaActivacion",
    "fechaCreacion",
    "globalComponentId",
    "guid",
    "modelo"
})
public class STB {

    @XmlElement(name = "component_id", required = true, nillable = true)
    protected String componentId;
    @XmlElement(required = true, nillable = true)
    protected String dvr;
    @XmlElement(name = "fecha_activacion", required = true, nillable = true)
    protected String fechaActivacion;
    @XmlElement(name = "fecha_creacion", required = true, nillable = true)
    protected String fechaCreacion;
    @XmlElement(name = "global_component_id", required = true, nillable = true)
    protected String globalComponentId;
    @XmlElement(required = true, nillable = true)
    protected String guid;
    @XmlElement(required = true, nillable = true)
    protected String modelo;

    /**
     * Gets the value of the componentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComponentId() {
        return componentId;
    }

    /**
     * Sets the value of the componentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComponentId(String value) {
        this.componentId = value;
    }

    /**
     * Gets the value of the dvr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvr() {
        return dvr;
    }

    /**
     * Sets the value of the dvr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvr(String value) {
        this.dvr = value;
    }

    /**
     * Gets the value of the fechaActivacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaActivacion() {
        return fechaActivacion;
    }

    /**
     * Sets the value of the fechaActivacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaActivacion(String value) {
        this.fechaActivacion = value;
    }

    /**
     * Gets the value of the fechaCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * Sets the value of the fechaCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCreacion(String value) {
        this.fechaCreacion = value;
    }

    /**
     * Gets the value of the globalComponentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalComponentId() {
        return globalComponentId;
    }

    /**
     * Sets the value of the globalComponentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalComponentId(String value) {
        this.globalComponentId = value;
    }

    /**
     * Gets the value of the guid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuid() {
        return guid;
    }

    /**
     * Sets the value of the guid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuid(String value) {
        this.guid = value;
    }

    /**
     * Gets the value of the modelo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Sets the value of the modelo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelo(String value) {
        this.modelo = value;
    }

}
