
package com.claro.spc.servlet;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.claro.spc.servlet package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.claro.spc.servlet
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DesbloquearCompra }
     * 
     */
    public DesbloquearCompra createDesbloquearCompra() {
        return new DesbloquearCompra();
    }

    /**
     * Create an instance of {@link ResetearPinAdultosPorGuidResponse }
     * 
     */
    public ResetearPinAdultosPorGuidResponse createResetearPinAdultosPorGuidResponse() {
        return new ResetearPinAdultosPorGuidResponse();
    }

    /**
     * Create an instance of {@link ResetearPinParentalPorGuidResponse }
     * 
     */
    public ResetearPinParentalPorGuidResponse createResetearPinParentalPorGuidResponse() {
        return new ResetearPinParentalPorGuidResponse();
    }

    /**
     * Create an instance of {@link Compra }
     * 
     */
    public Compra createCompra() {
        return new Compra();
    }

    /**
     * Create an instance of {@link ConsultarCanalesResponse }
     * 
     */
    public ConsultarCanalesResponse createConsultarCanalesResponse() {
        return new ConsultarCanalesResponse();
    }

    /**
     * Create an instance of {@link ConsultarClientePorCuenta }
     * 
     */
    public ConsultarClientePorCuenta createConsultarClientePorCuenta() {
        return new ConsultarClientePorCuenta();
    }

    /**
     * Create an instance of {@link ReemplazarSTBResponse }
     * 
     */
    public ReemplazarSTBResponse createReemplazarSTBResponse() {
        return new ReemplazarSTBResponse();
    }

    /**
     * Create an instance of {@link ConsultarClientePorCuentaResponse }
     * 
     */
    public ConsultarClientePorCuentaResponse createConsultarClientePorCuentaResponse() {
        return new ConsultarClientePorCuentaResponse();
    }

    /**
     * Create an instance of {@link ConsultarClientePorGuidParcial }
     * 
     */
    public ConsultarClientePorGuidParcial createConsultarClientePorGuidParcial() {
        return new ConsultarClientePorGuidParcial();
    }

    /**
     * Create an instance of {@link Canal }
     * 
     */
    public Canal createCanal() {
        return new Canal();
    }

    /**
     * Create an instance of {@link ConsultarCanales }
     * 
     */
    public ConsultarCanales createConsultarCanales() {
        return new ConsultarCanales();
    }

    /**
     * Create an instance of {@link Cliente }
     * 
     */
    public Cliente createCliente() {
        return new Cliente();
    }

    /**
     * Create an instance of {@link DRASTBPorGuidResponse }
     * 
     */
    public DRASTBPorGuidResponse createDRASTBPorGuidResponse() {
        return new DRASTBPorGuidResponse();
    }

    /**
     * Create an instance of {@link BloquearCompraResponse }
     * 
     */
    public BloquearCompraResponse createBloquearCompraResponse() {
        return new BloquearCompraResponse();
    }

    /**
     * Create an instance of {@link DRASTBPorGuid }
     * 
     */
    public DRASTBPorGuid createDRASTBPorGuid() {
        return new DRASTBPorGuid();
    }

    /**
     * Create an instance of {@link ProbarCambioCanalDeSTBPorGuid }
     * 
     */
    public ProbarCambioCanalDeSTBPorGuid createProbarCambioCanalDeSTBPorGuid() {
        return new ProbarCambioCanalDeSTBPorGuid();
    }

    /**
     * Create an instance of {@link Transaccion }
     * 
     */
    public Transaccion createTransaccion() {
        return new Transaccion();
    }

    /**
     * Create an instance of {@link RemoverPinAdultosPorGuidResponse }
     * 
     */
    public RemoverPinAdultosPorGuidResponse createRemoverPinAdultosPorGuidResponse() {
        return new RemoverPinAdultosPorGuidResponse();
    }

    /**
     * Create an instance of {@link ResetearPinAdultosPorGuid }
     * 
     */
    public ResetearPinAdultosPorGuid createResetearPinAdultosPorGuid() {
        return new ResetearPinAdultosPorGuid();
    }

    /**
     * Create an instance of {@link ReiniciarSTBPorGuidResponse }
     * 
     */
    public ReiniciarSTBPorGuidResponse createReiniciarSTBPorGuidResponse() {
        return new ReiniciarSTBPorGuidResponse();
    }

    /**
     * Create an instance of {@link EnviarMensajeASTBPorGuidResponse }
     * 
     */
    public EnviarMensajeASTBPorGuidResponse createEnviarMensajeASTBPorGuidResponse() {
        return new EnviarMensajeASTBPorGuidResponse();
    }

    /**
     * Create an instance of {@link DesbloquearCompraResponse }
     * 
     */
    public DesbloquearCompraResponse createDesbloquearCompraResponse() {
        return new DesbloquearCompraResponse();
    }

    /**
     * Create an instance of {@link ProbarCambioCanalDeSTBPorGuidResponse }
     * 
     */
    public ProbarCambioCanalDeSTBPorGuidResponse createProbarCambioCanalDeSTBPorGuidResponse() {
        return new ProbarCambioCanalDeSTBPorGuidResponse();
    }

    /**
     * Create an instance of {@link RemoverPinAdultosPorGuid }
     * 
     */
    public RemoverPinAdultosPorGuid createRemoverPinAdultosPorGuid() {
        return new RemoverPinAdultosPorGuid();
    }

    /**
     * Create an instance of {@link ReemplazarSTB }
     * 
     */
    public ReemplazarSTB createReemplazarSTB() {
        return new ReemplazarSTB();
    }

    /**
     * Create an instance of {@link ConsultarSTBPorCuentaResponse }
     * 
     */
    public ConsultarSTBPorCuentaResponse createConsultarSTBPorCuentaResponse() {
        return new ConsultarSTBPorCuentaResponse();
    }

    /**
     * Create an instance of {@link ResetearPinComprasPorGuidResponse }
     * 
     */
    public ResetearPinComprasPorGuidResponse createResetearPinComprasPorGuidResponse() {
        return new ResetearPinComprasPorGuidResponse();
    }

    /**
     * Create an instance of {@link ConsultarPaquetesPorCuentaResponse }
     * 
     */
    public ConsultarPaquetesPorCuentaResponse createConsultarPaquetesPorCuentaResponse() {
        return new ConsultarPaquetesPorCuentaResponse();
    }

    /**
     * Create an instance of {@link RemoverPinComprasPorGuidResponse }
     * 
     */
    public RemoverPinComprasPorGuidResponse createRemoverPinComprasPorGuidResponse() {
        return new RemoverPinComprasPorGuidResponse();
    }

    /**
     * Create an instance of {@link EnviarMensajeASTBPorGuid }
     * 
     */
    public EnviarMensajeASTBPorGuid createEnviarMensajeASTBPorGuid() {
        return new EnviarMensajeASTBPorGuid();
    }

    /**
     * Create an instance of {@link ConsultarSTBPorCuenta }
     * 
     */
    public ConsultarSTBPorCuenta createConsultarSTBPorCuenta() {
        return new ConsultarSTBPorCuenta();
    }

    /**
     * Create an instance of {@link Bitacora }
     * 
     */
    public Bitacora createBitacora() {
        return new Bitacora();
    }

    /**
     * Create an instance of {@link ConsultarPaquetesPorCuenta }
     * 
     */
    public ConsultarPaquetesPorCuenta createConsultarPaquetesPorCuenta() {
        return new ConsultarPaquetesPorCuenta();
    }

    /**
     * Create an instance of {@link ConsultarHistoricoCambios }
     * 
     */
    public ConsultarHistoricoCambios createConsultarHistoricoCambios() {
        return new ConsultarHistoricoCambios();
    }

    /**
     * Create an instance of {@link RemoverPinComprasPorGuid }
     * 
     */
    public RemoverPinComprasPorGuid createRemoverPinComprasPorGuid() {
        return new RemoverPinComprasPorGuid();
    }

    /**
     * Create an instance of {@link ConsultarComprasResponse }
     * 
     */
    public ConsultarComprasResponse createConsultarComprasResponse() {
        return new ConsultarComprasResponse();
    }

    /**
     * Create an instance of {@link BloquearCompra }
     * 
     */
    public BloquearCompra createBloquearCompra() {
        return new BloquearCompra();
    }

    /**
     * Create an instance of {@link STB }
     * 
     */
    public STB createSTB() {
        return new STB();
    }

    /**
     * Create an instance of {@link ConsultarHistoricoCambiosResponse }
     * 
     */
    public ConsultarHistoricoCambiosResponse createConsultarHistoricoCambiosResponse() {
        return new ConsultarHistoricoCambiosResponse();
    }

    /**
     * Create an instance of {@link RemoverPinParentalPorGuid }
     * 
     */
    public RemoverPinParentalPorGuid createRemoverPinParentalPorGuid() {
        return new RemoverPinParentalPorGuid();
    }

    /**
     * Create an instance of {@link ResetearPinParentalPorGuid }
     * 
     */
    public ResetearPinParentalPorGuid createResetearPinParentalPorGuid() {
        return new ResetearPinParentalPorGuid();
    }

    /**
     * Create an instance of {@link Paquete }
     * 
     */
    public Paquete createPaquete() {
        return new Paquete();
    }

    /**
     * Create an instance of {@link ReiniciarSTBPorGuid }
     * 
     */
    public ReiniciarSTBPorGuid createReiniciarSTBPorGuid() {
        return new ReiniciarSTBPorGuid();
    }

    /**
     * Create an instance of {@link ResetearPinComprasPorGuid }
     * 
     */
    public ResetearPinComprasPorGuid createResetearPinComprasPorGuid() {
        return new ResetearPinComprasPorGuid();
    }

    /**
     * Create an instance of {@link RemoverPinParentalPorGuidResponse }
     * 
     */
    public RemoverPinParentalPorGuidResponse createRemoverPinParentalPorGuidResponse() {
        return new RemoverPinParentalPorGuidResponse();
    }

    /**
     * Create an instance of {@link ConsultarClientePorGuidParcialResponse }
     * 
     */
    public ConsultarClientePorGuidParcialResponse createConsultarClientePorGuidParcialResponse() {
        return new ConsultarClientePorGuidParcialResponse();
    }

    /**
     * Create an instance of {@link ConsultarCompras }
     * 
     */
    public ConsultarCompras createConsultarCompras() {
        return new ConsultarCompras();
    }

}
