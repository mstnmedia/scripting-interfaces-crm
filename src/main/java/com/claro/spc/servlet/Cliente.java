
package com.claro.spc.servlet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Cliente complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cliente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capacidad_hd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="channel_map" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ciclo_facturacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="download_vod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecha_creacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="hd_streams" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="limite_credito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="permite_compra" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="plan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="reproduce_vod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sd_streams" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="zipcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cliente", propOrder = {
    "capacidadHd",
    "channelMap",
    "cicloFacturacion",
    "cuenta",
    "downloadVod",
    "fechaCreacion",
    "hdStreams",
    "limiteCredito",
    "permiteCompra",
    "plan",
    "reproduceVod",
    "sdStreams",
    "status",
    "zipcode"
})
public class Cliente {

    @XmlElement(name = "capacidad_hd", required = true, nillable = true)
    protected String capacidadHd;
    @XmlElement(name = "channel_map", required = true, nillable = true)
    protected String channelMap;
    @XmlElement(name = "ciclo_facturacion", required = true, nillable = true)
    protected String cicloFacturacion;
    @XmlElement(required = true, nillable = true)
    protected String cuenta;
    @XmlElement(name = "download_vod", required = true, nillable = true)
    protected String downloadVod;
    @XmlElement(name = "fecha_creacion", required = true, nillable = true)
    protected String fechaCreacion;
    @XmlElement(name = "hd_streams", required = true, nillable = true)
    protected String hdStreams;
    @XmlElement(name = "limite_credito", required = true, nillable = true)
    protected String limiteCredito;
    @XmlElement(name = "permite_compra", required = true, nillable = true)
    protected String permiteCompra;
    @XmlElement(required = true, nillable = true)
    protected String plan;
    @XmlElement(name = "reproduce_vod", required = true, nillable = true)
    protected String reproduceVod;
    @XmlElement(name = "sd_streams", required = true, nillable = true)
    protected String sdStreams;
    @XmlElement(required = true, nillable = true)
    protected String status;
    @XmlElement(required = true, nillable = true)
    protected String zipcode;

    /**
     * Gets the value of the capacidadHd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapacidadHd() {
        return capacidadHd;
    }

    /**
     * Sets the value of the capacidadHd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapacidadHd(String value) {
        this.capacidadHd = value;
    }

    /**
     * Gets the value of the channelMap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelMap() {
        return channelMap;
    }

    /**
     * Sets the value of the channelMap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelMap(String value) {
        this.channelMap = value;
    }

    /**
     * Gets the value of the cicloFacturacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCicloFacturacion() {
        return cicloFacturacion;
    }

    /**
     * Sets the value of the cicloFacturacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCicloFacturacion(String value) {
        this.cicloFacturacion = value;
    }

    /**
     * Gets the value of the cuenta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Sets the value of the cuenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Gets the value of the downloadVod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDownloadVod() {
        return downloadVod;
    }

    /**
     * Sets the value of the downloadVod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDownloadVod(String value) {
        this.downloadVod = value;
    }

    /**
     * Gets the value of the fechaCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * Sets the value of the fechaCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCreacion(String value) {
        this.fechaCreacion = value;
    }

    /**
     * Gets the value of the hdStreams property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHdStreams() {
        return hdStreams;
    }

    /**
     * Sets the value of the hdStreams property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHdStreams(String value) {
        this.hdStreams = value;
    }

    /**
     * Gets the value of the limiteCredito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimiteCredito() {
        return limiteCredito;
    }

    /**
     * Sets the value of the limiteCredito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimiteCredito(String value) {
        this.limiteCredito = value;
    }

    /**
     * Gets the value of the permiteCompra property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPermiteCompra() {
        return permiteCompra;
    }

    /**
     * Sets the value of the permiteCompra property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPermiteCompra(String value) {
        this.permiteCompra = value;
    }

    /**
     * Gets the value of the plan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlan() {
        return plan;
    }

    /**
     * Sets the value of the plan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlan(String value) {
        this.plan = value;
    }

    /**
     * Gets the value of the reproduceVod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReproduceVod() {
        return reproduceVod;
    }

    /**
     * Sets the value of the reproduceVod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReproduceVod(String value) {
        this.reproduceVod = value;
    }

    /**
     * Gets the value of the sdStreams property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSdStreams() {
        return sdStreams;
    }

    /**
     * Sets the value of the sdStreams property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSdStreams(String value) {
        this.sdStreams = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the zipcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * Sets the value of the zipcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZipcode(String value) {
        this.zipcode = value;
    }

}
