/**
 * Este paquete contiene las clases que fueron autogeneradas por ws-import para
 * implementar la conexión de Scripting con el sistema SAD.
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://core.service.sad.api.claro.com/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.claro.api.sad.service.core;
