
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDireccionesPorIDTSector complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDireccionesPorIDTSector">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDTSector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDireccionesPorIDTSector", propOrder = {
    "idtSector"
})
public class GetDireccionesPorIDTSector {

    @XmlElement(name = "IDTSector", namespace = "")
    protected String idtSector;

    /**
     * Gets the value of the idtSector property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDTSector() {
        return idtSector;
    }

    /**
     * Sets the value of the idtSector property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDTSector(String value) {
        this.idtSector = value;
    }

}
