
package com.claro.api.sad.service.core;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDireccionPorIDTResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDireccionPorIDTResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SADResponse" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="direccion" type="{http://core.service.sad.api.claro.com/}direccion" minOccurs="0"/>
 *                   &lt;element name="direcciones" type="{http://core.service.sad.api.claro.com/}direcciones" minOccurs="0"/>
 *                   &lt;element name="direccionesArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="direccion" type="{http://core.service.sad.api.claro.com/}direccion" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="calle" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
 *                   &lt;element name="callesArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="calle" type="{http://core.service.sad.api.claro.com/}calle" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ciudad" type="{http://core.service.sad.api.claro.com/}ciudad" minOccurs="0"/>
 *                   &lt;element name="ciudadArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ciudad" type="{http://core.service.sad.api.claro.com/}ciudad" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="distrito" type="{http://core.service.sad.api.claro.com/}distrito" minOccurs="0"/>
 *                   &lt;element name="distritoArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="distrito" type="{http://core.service.sad.api.claro.com/}distrito" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="edificacion" type="{http://core.service.sad.api.claro.com/}edificacion" minOccurs="0"/>
 *                   &lt;element name="edificacionArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="edificacion" type="{http://core.service.sad.api.claro.com/}edificacion" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="nivel" type="{http://core.service.sad.api.claro.com/}nivel" minOccurs="0"/>
 *                   &lt;element name="nivelArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="nivel" type="{http://core.service.sad.api.claro.com/}nivel" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="arbolNivel" type="{http://core.service.sad.api.claro.com/}nivelPadre" minOccurs="0"/>
 *                   &lt;element name="arbolNivelArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="arbolNivel" type="{http://core.service.sad.api.claro.com/}nivelPadre" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="nivelServicio" type="{http://core.service.sad.api.claro.com/}nivelServicio" minOccurs="0"/>
 *                   &lt;element name="nivelServicioArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="nivelServicio" type="{http://core.service.sad.api.claro.com/}nivelServicio" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="nivelServicioFibra" type="{http://core.service.sad.api.claro.com/}nivelServicioFibra" minOccurs="0"/>
 *                   &lt;element name="nivelServiciofibraArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="nivelServicioFibra" type="{http://core.service.sad.api.claro.com/}nivelServicioFibra" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="provincia" type="{http://core.service.sad.api.claro.com/}provincia" minOccurs="0"/>
 *                   &lt;element name="provinciaArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="provincia" type="{http://core.service.sad.api.claro.com/}provincia" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="rangoDireccion" type="{http://core.service.sad.api.claro.com/}rangoDireccion" minOccurs="0"/>
 *                   &lt;element name="rangoDireccionArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="rangoDireccion" type="{http://core.service.sad.api.claro.com/}rangoDireccion" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="rangoServicioFibra" type="{http://core.service.sad.api.claro.com/}rangoServicioFibra" minOccurs="0"/>
 *                   &lt;element name="rangoServicioFibraArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="rangoServicioFibra" type="{http://core.service.sad.api.claro.com/}rangoServicioFibra" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="rangoServicio" type="{http://core.service.sad.api.claro.com/}rangoServicio" minOccurs="0"/>
 *                   &lt;element name="rangoServicioArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="rangoServicio" type="{http://core.service.sad.api.claro.com/}rangoServicio" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="region" type="{http://core.service.sad.api.claro.com/}region" minOccurs="0"/>
 *                   &lt;element name="regionArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="region" type="{http://core.service.sad.api.claro.com/}region" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="sector" type="{http://core.service.sad.api.claro.com/}sector" minOccurs="0"/>
 *                   &lt;element name="sectorArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="sector" type="{http://core.service.sad.api.claro.com/}sector" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="tipo" type="{http://core.service.sad.api.claro.com/}tipo" minOccurs="0"/>
 *                   &lt;element name="tipoArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tipo" type="{http://core.service.sad.api.claro.com/}tipo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="nombreTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="categoriaTipo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="nombreUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="listaRangoServicioArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="listaRangoServicio" type="{http://core.service.sad.api.claro.com/}listaRangoServicio" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="IddDirecionRangoServicio" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element ref="{http://core.service.sad.api.claro.com/}errorMessage" minOccurs="0"/>
 *                   &lt;element name="existeInmueble" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="tipoCalle" type="{http://core.service.sad.api.claro.com/}tipo" minOccurs="0"/>
 *                   &lt;element name="zonaTarifaria" type="{http://core.service.sad.api.claro.com/}zonaTarifaria" minOccurs="0"/>
 *                   &lt;element name="zonaTarifariaArray" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="zonaTarifaria" type="{http://core.service.sad.api.claro.com/}zonaTarifaria" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDireccionPorIDTResponse", propOrder = {
    "sadResponse"
})
public class GetDireccionPorIDTResponse {

    @XmlElement(name = "SADResponse", namespace = "")
    protected GetDireccionPorIDTResponse.SADResponse sadResponse;

    /**
     * Gets the value of the sadResponse property.
     * 
     * @return
     *     possible object is
     *     {@link GetDireccionPorIDTResponse.SADResponse }
     *     
     */
    public GetDireccionPorIDTResponse.SADResponse getSADResponse() {
        return sadResponse;
    }

    /**
     * Sets the value of the sadResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDireccionPorIDTResponse.SADResponse }
     *     
     */
    public void setSADResponse(GetDireccionPorIDTResponse.SADResponse value) {
        this.sadResponse = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="direccion" type="{http://core.service.sad.api.claro.com/}direccion" minOccurs="0"/>
     *         &lt;element name="direcciones" type="{http://core.service.sad.api.claro.com/}direcciones" minOccurs="0"/>
     *         &lt;element name="direccionesArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="direccion" type="{http://core.service.sad.api.claro.com/}direccion" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="calle" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
     *         &lt;element name="callesArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="calle" type="{http://core.service.sad.api.claro.com/}calle" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ciudad" type="{http://core.service.sad.api.claro.com/}ciudad" minOccurs="0"/>
     *         &lt;element name="ciudadArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ciudad" type="{http://core.service.sad.api.claro.com/}ciudad" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="distrito" type="{http://core.service.sad.api.claro.com/}distrito" minOccurs="0"/>
     *         &lt;element name="distritoArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="distrito" type="{http://core.service.sad.api.claro.com/}distrito" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="edificacion" type="{http://core.service.sad.api.claro.com/}edificacion" minOccurs="0"/>
     *         &lt;element name="edificacionArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="edificacion" type="{http://core.service.sad.api.claro.com/}edificacion" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="nivel" type="{http://core.service.sad.api.claro.com/}nivel" minOccurs="0"/>
     *         &lt;element name="nivelArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="nivel" type="{http://core.service.sad.api.claro.com/}nivel" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="arbolNivel" type="{http://core.service.sad.api.claro.com/}nivelPadre" minOccurs="0"/>
     *         &lt;element name="arbolNivelArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="arbolNivel" type="{http://core.service.sad.api.claro.com/}nivelPadre" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="nivelServicio" type="{http://core.service.sad.api.claro.com/}nivelServicio" minOccurs="0"/>
     *         &lt;element name="nivelServicioArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="nivelServicio" type="{http://core.service.sad.api.claro.com/}nivelServicio" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="nivelServicioFibra" type="{http://core.service.sad.api.claro.com/}nivelServicioFibra" minOccurs="0"/>
     *         &lt;element name="nivelServiciofibraArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="nivelServicioFibra" type="{http://core.service.sad.api.claro.com/}nivelServicioFibra" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="provincia" type="{http://core.service.sad.api.claro.com/}provincia" minOccurs="0"/>
     *         &lt;element name="provinciaArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="provincia" type="{http://core.service.sad.api.claro.com/}provincia" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="rangoDireccion" type="{http://core.service.sad.api.claro.com/}rangoDireccion" minOccurs="0"/>
     *         &lt;element name="rangoDireccionArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="rangoDireccion" type="{http://core.service.sad.api.claro.com/}rangoDireccion" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="rangoServicioFibra" type="{http://core.service.sad.api.claro.com/}rangoServicioFibra" minOccurs="0"/>
     *         &lt;element name="rangoServicioFibraArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="rangoServicioFibra" type="{http://core.service.sad.api.claro.com/}rangoServicioFibra" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="rangoServicio" type="{http://core.service.sad.api.claro.com/}rangoServicio" minOccurs="0"/>
     *         &lt;element name="rangoServicioArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="rangoServicio" type="{http://core.service.sad.api.claro.com/}rangoServicio" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="region" type="{http://core.service.sad.api.claro.com/}region" minOccurs="0"/>
     *         &lt;element name="regionArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="region" type="{http://core.service.sad.api.claro.com/}region" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="sector" type="{http://core.service.sad.api.claro.com/}sector" minOccurs="0"/>
     *         &lt;element name="sectorArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="sector" type="{http://core.service.sad.api.claro.com/}sector" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="tipo" type="{http://core.service.sad.api.claro.com/}tipo" minOccurs="0"/>
     *         &lt;element name="tipoArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tipo" type="{http://core.service.sad.api.claro.com/}tipo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="nombreTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="categoriaTipo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="nombreUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="listaRangoServicioArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="listaRangoServicio" type="{http://core.service.sad.api.claro.com/}listaRangoServicio" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="IddDirecionRangoServicio" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element ref="{http://core.service.sad.api.claro.com/}errorMessage" minOccurs="0"/>
     *         &lt;element name="existeInmueble" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="tipoCalle" type="{http://core.service.sad.api.claro.com/}tipo" minOccurs="0"/>
     *         &lt;element name="zonaTarifaria" type="{http://core.service.sad.api.claro.com/}zonaTarifaria" minOccurs="0"/>
     *         &lt;element name="zonaTarifariaArray" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="zonaTarifaria" type="{http://core.service.sad.api.claro.com/}zonaTarifaria" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "direccion",
        "direcciones",
        "direccionesArray",
        "calle",
        "callesArray",
        "ciudad",
        "ciudadArray",
        "distrito",
        "distritoArray",
        "edificacion",
        "edificacionArray",
        "nivel",
        "nivelArray",
        "arbolNivel",
        "arbolNivelArray",
        "nivelServicio",
        "nivelServicioArray",
        "nivelServicioFibra",
        "nivelServiciofibraArray",
        "provincia",
        "provinciaArray",
        "rangoDireccion",
        "rangoDireccionArray",
        "rangoServicioFibra",
        "rangoServicioFibraArray",
        "rangoServicio",
        "rangoServicioArray",
        "region",
        "regionArray",
        "sector",
        "sectorArray",
        "tipo",
        "tipoArray",
        "nombreTipo",
        "categoriaTipo",
        "nombreUsuario",
        "listaRangoServicioArray",
        "iddDirecionRangoServicio",
        "errorMessage",
        "existeInmueble",
        "tipoCalle",
        "zonaTarifaria",
        "zonaTarifariaArray"
    })
    public static class SADResponse {

        protected Direccion direccion;
        protected Direcciones direcciones;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.DireccionesArray direccionesArray;
        protected Calle calle;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.CallesArray callesArray;
        protected Ciudad ciudad;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.CiudadArray ciudadArray;
        protected Distrito distrito;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.DistritoArray distritoArray;
        protected Edificacion edificacion;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.EdificacionArray edificacionArray;
        protected Nivel nivel;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.NivelArray nivelArray;
        protected NivelPadre arbolNivel;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.ArbolNivelArray arbolNivelArray;
        protected NivelServicio nivelServicio;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.NivelServicioArray nivelServicioArray;
        protected NivelServicioFibra nivelServicioFibra;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.NivelServiciofibraArray nivelServiciofibraArray;
        protected Provincia provincia;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.ProvinciaArray provinciaArray;
        protected RangoDireccion rangoDireccion;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.RangoDireccionArray rangoDireccionArray;
        protected RangoServicioFibra rangoServicioFibra;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.RangoServicioFibraArray rangoServicioFibraArray;
        protected RangoServicio rangoServicio;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.RangoServicioArray rangoServicioArray;
        protected Region region;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.RegionArray regionArray;
        protected Sector sector;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.SectorArray sectorArray;
        protected Tipo tipo;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.TipoArray tipoArray;
        @XmlElement(namespace = "")
        protected String nombreTipo;
        @XmlElement(namespace = "")
        protected Long categoriaTipo;
        @XmlElement(namespace = "")
        protected String nombreUsuario;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.ListaRangoServicioArray listaRangoServicioArray;
        @XmlElement(name = "IddDirecionRangoServicio")
        protected Long iddDirecionRangoServicio;
        protected ErrorMessage errorMessage;
        protected Boolean existeInmueble;
        protected Tipo tipoCalle;
        protected ZonaTarifaria zonaTarifaria;
        @XmlElement(namespace = "")
        protected GetDireccionPorIDTResponse.SADResponse.ZonaTarifariaArray zonaTarifariaArray;

        /**
         * Gets the value of the direccion property.
         * 
         * @return
         *     possible object is
         *     {@link Direccion }
         *     
         */
        public Direccion getDireccion() {
            return direccion;
        }

        /**
         * Sets the value of the direccion property.
         * 
         * @param value
         *     allowed object is
         *     {@link Direccion }
         *     
         */
        public void setDireccion(Direccion value) {
            this.direccion = value;
        }

        /**
         * Gets the value of the direcciones property.
         * 
         * @return
         *     possible object is
         *     {@link Direcciones }
         *     
         */
        public Direcciones getDirecciones() {
            return direcciones;
        }

        /**
         * Sets the value of the direcciones property.
         * 
         * @param value
         *     allowed object is
         *     {@link Direcciones }
         *     
         */
        public void setDirecciones(Direcciones value) {
            this.direcciones = value;
        }

        /**
         * Gets the value of the direccionesArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.DireccionesArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.DireccionesArray getDireccionesArray() {
            return direccionesArray;
        }

        /**
         * Sets the value of the direccionesArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.DireccionesArray }
         *     
         */
        public void setDireccionesArray(GetDireccionPorIDTResponse.SADResponse.DireccionesArray value) {
            this.direccionesArray = value;
        }

        /**
         * Gets the value of the calle property.
         * 
         * @return
         *     possible object is
         *     {@link Calle }
         *     
         */
        public Calle getCalle() {
            return calle;
        }

        /**
         * Sets the value of the calle property.
         * 
         * @param value
         *     allowed object is
         *     {@link Calle }
         *     
         */
        public void setCalle(Calle value) {
            this.calle = value;
        }

        /**
         * Gets the value of the callesArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.CallesArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.CallesArray getCallesArray() {
            return callesArray;
        }

        /**
         * Sets the value of the callesArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.CallesArray }
         *     
         */
        public void setCallesArray(GetDireccionPorIDTResponse.SADResponse.CallesArray value) {
            this.callesArray = value;
        }

        /**
         * Gets the value of the ciudad property.
         * 
         * @return
         *     possible object is
         *     {@link Ciudad }
         *     
         */
        public Ciudad getCiudad() {
            return ciudad;
        }

        /**
         * Sets the value of the ciudad property.
         * 
         * @param value
         *     allowed object is
         *     {@link Ciudad }
         *     
         */
        public void setCiudad(Ciudad value) {
            this.ciudad = value;
        }

        /**
         * Gets the value of the ciudadArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.CiudadArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.CiudadArray getCiudadArray() {
            return ciudadArray;
        }

        /**
         * Sets the value of the ciudadArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.CiudadArray }
         *     
         */
        public void setCiudadArray(GetDireccionPorIDTResponse.SADResponse.CiudadArray value) {
            this.ciudadArray = value;
        }

        /**
         * Gets the value of the distrito property.
         * 
         * @return
         *     possible object is
         *     {@link Distrito }
         *     
         */
        public Distrito getDistrito() {
            return distrito;
        }

        /**
         * Sets the value of the distrito property.
         * 
         * @param value
         *     allowed object is
         *     {@link Distrito }
         *     
         */
        public void setDistrito(Distrito value) {
            this.distrito = value;
        }

        /**
         * Gets the value of the distritoArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.DistritoArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.DistritoArray getDistritoArray() {
            return distritoArray;
        }

        /**
         * Sets the value of the distritoArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.DistritoArray }
         *     
         */
        public void setDistritoArray(GetDireccionPorIDTResponse.SADResponse.DistritoArray value) {
            this.distritoArray = value;
        }

        /**
         * Gets the value of the edificacion property.
         * 
         * @return
         *     possible object is
         *     {@link Edificacion }
         *     
         */
        public Edificacion getEdificacion() {
            return edificacion;
        }

        /**
         * Sets the value of the edificacion property.
         * 
         * @param value
         *     allowed object is
         *     {@link Edificacion }
         *     
         */
        public void setEdificacion(Edificacion value) {
            this.edificacion = value;
        }

        /**
         * Gets the value of the edificacionArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.EdificacionArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.EdificacionArray getEdificacionArray() {
            return edificacionArray;
        }

        /**
         * Sets the value of the edificacionArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.EdificacionArray }
         *     
         */
        public void setEdificacionArray(GetDireccionPorIDTResponse.SADResponse.EdificacionArray value) {
            this.edificacionArray = value;
        }

        /**
         * Gets the value of the nivel property.
         * 
         * @return
         *     possible object is
         *     {@link Nivel }
         *     
         */
        public Nivel getNivel() {
            return nivel;
        }

        /**
         * Sets the value of the nivel property.
         * 
         * @param value
         *     allowed object is
         *     {@link Nivel }
         *     
         */
        public void setNivel(Nivel value) {
            this.nivel = value;
        }

        /**
         * Gets the value of the nivelArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.NivelArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.NivelArray getNivelArray() {
            return nivelArray;
        }

        /**
         * Sets the value of the nivelArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.NivelArray }
         *     
         */
        public void setNivelArray(GetDireccionPorIDTResponse.SADResponse.NivelArray value) {
            this.nivelArray = value;
        }

        /**
         * Gets the value of the arbolNivel property.
         * 
         * @return
         *     possible object is
         *     {@link NivelPadre }
         *     
         */
        public NivelPadre getArbolNivel() {
            return arbolNivel;
        }

        /**
         * Sets the value of the arbolNivel property.
         * 
         * @param value
         *     allowed object is
         *     {@link NivelPadre }
         *     
         */
        public void setArbolNivel(NivelPadre value) {
            this.arbolNivel = value;
        }

        /**
         * Gets the value of the arbolNivelArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.ArbolNivelArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.ArbolNivelArray getArbolNivelArray() {
            return arbolNivelArray;
        }

        /**
         * Sets the value of the arbolNivelArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.ArbolNivelArray }
         *     
         */
        public void setArbolNivelArray(GetDireccionPorIDTResponse.SADResponse.ArbolNivelArray value) {
            this.arbolNivelArray = value;
        }

        /**
         * Gets the value of the nivelServicio property.
         * 
         * @return
         *     possible object is
         *     {@link NivelServicio }
         *     
         */
        public NivelServicio getNivelServicio() {
            return nivelServicio;
        }

        /**
         * Sets the value of the nivelServicio property.
         * 
         * @param value
         *     allowed object is
         *     {@link NivelServicio }
         *     
         */
        public void setNivelServicio(NivelServicio value) {
            this.nivelServicio = value;
        }

        /**
         * Gets the value of the nivelServicioArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.NivelServicioArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.NivelServicioArray getNivelServicioArray() {
            return nivelServicioArray;
        }

        /**
         * Sets the value of the nivelServicioArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.NivelServicioArray }
         *     
         */
        public void setNivelServicioArray(GetDireccionPorIDTResponse.SADResponse.NivelServicioArray value) {
            this.nivelServicioArray = value;
        }

        /**
         * Gets the value of the nivelServicioFibra property.
         * 
         * @return
         *     possible object is
         *     {@link NivelServicioFibra }
         *     
         */
        public NivelServicioFibra getNivelServicioFibra() {
            return nivelServicioFibra;
        }

        /**
         * Sets the value of the nivelServicioFibra property.
         * 
         * @param value
         *     allowed object is
         *     {@link NivelServicioFibra }
         *     
         */
        public void setNivelServicioFibra(NivelServicioFibra value) {
            this.nivelServicioFibra = value;
        }

        /**
         * Gets the value of the nivelServiciofibraArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.NivelServiciofibraArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.NivelServiciofibraArray getNivelServiciofibraArray() {
            return nivelServiciofibraArray;
        }

        /**
         * Sets the value of the nivelServiciofibraArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.NivelServiciofibraArray }
         *     
         */
        public void setNivelServiciofibraArray(GetDireccionPorIDTResponse.SADResponse.NivelServiciofibraArray value) {
            this.nivelServiciofibraArray = value;
        }

        /**
         * Gets the value of the provincia property.
         * 
         * @return
         *     possible object is
         *     {@link Provincia }
         *     
         */
        public Provincia getProvincia() {
            return provincia;
        }

        /**
         * Sets the value of the provincia property.
         * 
         * @param value
         *     allowed object is
         *     {@link Provincia }
         *     
         */
        public void setProvincia(Provincia value) {
            this.provincia = value;
        }

        /**
         * Gets the value of the provinciaArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.ProvinciaArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.ProvinciaArray getProvinciaArray() {
            return provinciaArray;
        }

        /**
         * Sets the value of the provinciaArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.ProvinciaArray }
         *     
         */
        public void setProvinciaArray(GetDireccionPorIDTResponse.SADResponse.ProvinciaArray value) {
            this.provinciaArray = value;
        }

        /**
         * Gets the value of the rangoDireccion property.
         * 
         * @return
         *     possible object is
         *     {@link RangoDireccion }
         *     
         */
        public RangoDireccion getRangoDireccion() {
            return rangoDireccion;
        }

        /**
         * Sets the value of the rangoDireccion property.
         * 
         * @param value
         *     allowed object is
         *     {@link RangoDireccion }
         *     
         */
        public void setRangoDireccion(RangoDireccion value) {
            this.rangoDireccion = value;
        }

        /**
         * Gets the value of the rangoDireccionArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.RangoDireccionArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.RangoDireccionArray getRangoDireccionArray() {
            return rangoDireccionArray;
        }

        /**
         * Sets the value of the rangoDireccionArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.RangoDireccionArray }
         *     
         */
        public void setRangoDireccionArray(GetDireccionPorIDTResponse.SADResponse.RangoDireccionArray value) {
            this.rangoDireccionArray = value;
        }

        /**
         * Gets the value of the rangoServicioFibra property.
         * 
         * @return
         *     possible object is
         *     {@link RangoServicioFibra }
         *     
         */
        public RangoServicioFibra getRangoServicioFibra() {
            return rangoServicioFibra;
        }

        /**
         * Sets the value of the rangoServicioFibra property.
         * 
         * @param value
         *     allowed object is
         *     {@link RangoServicioFibra }
         *     
         */
        public void setRangoServicioFibra(RangoServicioFibra value) {
            this.rangoServicioFibra = value;
        }

        /**
         * Gets the value of the rangoServicioFibraArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.RangoServicioFibraArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.RangoServicioFibraArray getRangoServicioFibraArray() {
            return rangoServicioFibraArray;
        }

        /**
         * Sets the value of the rangoServicioFibraArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.RangoServicioFibraArray }
         *     
         */
        public void setRangoServicioFibraArray(GetDireccionPorIDTResponse.SADResponse.RangoServicioFibraArray value) {
            this.rangoServicioFibraArray = value;
        }

        /**
         * Gets the value of the rangoServicio property.
         * 
         * @return
         *     possible object is
         *     {@link RangoServicio }
         *     
         */
        public RangoServicio getRangoServicio() {
            return rangoServicio;
        }

        /**
         * Sets the value of the rangoServicio property.
         * 
         * @param value
         *     allowed object is
         *     {@link RangoServicio }
         *     
         */
        public void setRangoServicio(RangoServicio value) {
            this.rangoServicio = value;
        }

        /**
         * Gets the value of the rangoServicioArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.RangoServicioArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.RangoServicioArray getRangoServicioArray() {
            return rangoServicioArray;
        }

        /**
         * Sets the value of the rangoServicioArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.RangoServicioArray }
         *     
         */
        public void setRangoServicioArray(GetDireccionPorIDTResponse.SADResponse.RangoServicioArray value) {
            this.rangoServicioArray = value;
        }

        /**
         * Gets the value of the region property.
         * 
         * @return
         *     possible object is
         *     {@link Region }
         *     
         */
        public Region getRegion() {
            return region;
        }

        /**
         * Sets the value of the region property.
         * 
         * @param value
         *     allowed object is
         *     {@link Region }
         *     
         */
        public void setRegion(Region value) {
            this.region = value;
        }

        /**
         * Gets the value of the regionArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.RegionArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.RegionArray getRegionArray() {
            return regionArray;
        }

        /**
         * Sets the value of the regionArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.RegionArray }
         *     
         */
        public void setRegionArray(GetDireccionPorIDTResponse.SADResponse.RegionArray value) {
            this.regionArray = value;
        }

        /**
         * Gets the value of the sector property.
         * 
         * @return
         *     possible object is
         *     {@link Sector }
         *     
         */
        public Sector getSector() {
            return sector;
        }

        /**
         * Sets the value of the sector property.
         * 
         * @param value
         *     allowed object is
         *     {@link Sector }
         *     
         */
        public void setSector(Sector value) {
            this.sector = value;
        }

        /**
         * Gets the value of the sectorArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.SectorArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.SectorArray getSectorArray() {
            return sectorArray;
        }

        /**
         * Sets the value of the sectorArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.SectorArray }
         *     
         */
        public void setSectorArray(GetDireccionPorIDTResponse.SADResponse.SectorArray value) {
            this.sectorArray = value;
        }

        /**
         * Gets the value of the tipo property.
         * 
         * @return
         *     possible object is
         *     {@link Tipo }
         *     
         */
        public Tipo getTipo() {
            return tipo;
        }

        /**
         * Sets the value of the tipo property.
         * 
         * @param value
         *     allowed object is
         *     {@link Tipo }
         *     
         */
        public void setTipo(Tipo value) {
            this.tipo = value;
        }

        /**
         * Gets the value of the tipoArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.TipoArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.TipoArray getTipoArray() {
            return tipoArray;
        }

        /**
         * Sets the value of the tipoArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.TipoArray }
         *     
         */
        public void setTipoArray(GetDireccionPorIDTResponse.SADResponse.TipoArray value) {
            this.tipoArray = value;
        }

        /**
         * Gets the value of the nombreTipo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreTipo() {
            return nombreTipo;
        }

        /**
         * Sets the value of the nombreTipo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreTipo(String value) {
            this.nombreTipo = value;
        }

        /**
         * Gets the value of the categoriaTipo property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getCategoriaTipo() {
            return categoriaTipo;
        }

        /**
         * Sets the value of the categoriaTipo property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setCategoriaTipo(Long value) {
            this.categoriaTipo = value;
        }

        /**
         * Gets the value of the nombreUsuario property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreUsuario() {
            return nombreUsuario;
        }

        /**
         * Sets the value of the nombreUsuario property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreUsuario(String value) {
            this.nombreUsuario = value;
        }

        /**
         * Gets the value of the listaRangoServicioArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.ListaRangoServicioArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.ListaRangoServicioArray getListaRangoServicioArray() {
            return listaRangoServicioArray;
        }

        /**
         * Sets the value of the listaRangoServicioArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.ListaRangoServicioArray }
         *     
         */
        public void setListaRangoServicioArray(GetDireccionPorIDTResponse.SADResponse.ListaRangoServicioArray value) {
            this.listaRangoServicioArray = value;
        }

        /**
         * Gets the value of the iddDirecionRangoServicio property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getIddDirecionRangoServicio() {
            return iddDirecionRangoServicio;
        }

        /**
         * Sets the value of the iddDirecionRangoServicio property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setIddDirecionRangoServicio(Long value) {
            this.iddDirecionRangoServicio = value;
        }

        /**
         * Gets the value of the errorMessage property.
         * 
         * @return
         *     possible object is
         *     {@link ErrorMessage }
         *     
         */
        public ErrorMessage getErrorMessage() {
            return errorMessage;
        }

        /**
         * Sets the value of the errorMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link ErrorMessage }
         *     
         */
        public void setErrorMessage(ErrorMessage value) {
            this.errorMessage = value;
        }

        /**
         * Gets the value of the existeInmueble property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isExisteInmueble() {
            return existeInmueble;
        }

        /**
         * Sets the value of the existeInmueble property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setExisteInmueble(Boolean value) {
            this.existeInmueble = value;
        }

        /**
         * Gets the value of the tipoCalle property.
         * 
         * @return
         *     possible object is
         *     {@link Tipo }
         *     
         */
        public Tipo getTipoCalle() {
            return tipoCalle;
        }

        /**
         * Sets the value of the tipoCalle property.
         * 
         * @param value
         *     allowed object is
         *     {@link Tipo }
         *     
         */
        public void setTipoCalle(Tipo value) {
            this.tipoCalle = value;
        }

        /**
         * Gets the value of the zonaTarifaria property.
         * 
         * @return
         *     possible object is
         *     {@link ZonaTarifaria }
         *     
         */
        public ZonaTarifaria getZonaTarifaria() {
            return zonaTarifaria;
        }

        /**
         * Sets the value of the zonaTarifaria property.
         * 
         * @param value
         *     allowed object is
         *     {@link ZonaTarifaria }
         *     
         */
        public void setZonaTarifaria(ZonaTarifaria value) {
            this.zonaTarifaria = value;
        }

        /**
         * Gets the value of the zonaTarifariaArray property.
         * 
         * @return
         *     possible object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.ZonaTarifariaArray }
         *     
         */
        public GetDireccionPorIDTResponse.SADResponse.ZonaTarifariaArray getZonaTarifariaArray() {
            return zonaTarifariaArray;
        }

        /**
         * Sets the value of the zonaTarifariaArray property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetDireccionPorIDTResponse.SADResponse.ZonaTarifariaArray }
         *     
         */
        public void setZonaTarifariaArray(GetDireccionPorIDTResponse.SADResponse.ZonaTarifariaArray value) {
            this.zonaTarifariaArray = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="arbolNivel" type="{http://core.service.sad.api.claro.com/}nivelPadre" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "arbolNivel"
        })
        public static class ArbolNivelArray {

            protected List<NivelPadre> arbolNivel;

            /**
             * Gets the value of the arbolNivel property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the arbolNivel property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getArbolNivel().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link NivelPadre }
             * 
             * 
             */
            public List<NivelPadre> getArbolNivel() {
                if (arbolNivel == null) {
                    arbolNivel = new ArrayList<NivelPadre>();
                }
                return this.arbolNivel;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="calle" type="{http://core.service.sad.api.claro.com/}calle" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "calle"
        })
        public static class CallesArray {

            protected List<Calle> calle;

            /**
             * Gets the value of the calle property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the calle property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCalle().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Calle }
             * 
             * 
             */
            public List<Calle> getCalle() {
                if (calle == null) {
                    calle = new ArrayList<Calle>();
                }
                return this.calle;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ciudad" type="{http://core.service.sad.api.claro.com/}ciudad" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ciudad"
        })
        public static class CiudadArray {

            protected List<Ciudad> ciudad;

            /**
             * Gets the value of the ciudad property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the ciudad property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCiudad().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Ciudad }
             * 
             * 
             */
            public List<Ciudad> getCiudad() {
                if (ciudad == null) {
                    ciudad = new ArrayList<Ciudad>();
                }
                return this.ciudad;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="direccion" type="{http://core.service.sad.api.claro.com/}direccion" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "direccion"
        })
        public static class DireccionesArray {

            protected List<Direccion> direccion;

            /**
             * Gets the value of the direccion property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the direccion property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDireccion().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Direccion }
             * 
             * 
             */
            public List<Direccion> getDireccion() {
                if (direccion == null) {
                    direccion = new ArrayList<Direccion>();
                }
                return this.direccion;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="distrito" type="{http://core.service.sad.api.claro.com/}distrito" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "distrito"
        })
        public static class DistritoArray {

            protected List<Distrito> distrito;

            /**
             * Gets the value of the distrito property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the distrito property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDistrito().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Distrito }
             * 
             * 
             */
            public List<Distrito> getDistrito() {
                if (distrito == null) {
                    distrito = new ArrayList<Distrito>();
                }
                return this.distrito;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="edificacion" type="{http://core.service.sad.api.claro.com/}edificacion" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "edificacion"
        })
        public static class EdificacionArray {

            protected List<Edificacion> edificacion;

            /**
             * Gets the value of the edificacion property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the edificacion property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getEdificacion().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Edificacion }
             * 
             * 
             */
            public List<Edificacion> getEdificacion() {
                if (edificacion == null) {
                    edificacion = new ArrayList<Edificacion>();
                }
                return this.edificacion;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="listaRangoServicio" type="{http://core.service.sad.api.claro.com/}listaRangoServicio" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "listaRangoServicio"
        })
        public static class ListaRangoServicioArray {

            protected List<ListaRangoServicio> listaRangoServicio;

            /**
             * Gets the value of the listaRangoServicio property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the listaRangoServicio property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getListaRangoServicio().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ListaRangoServicio }
             * 
             * 
             */
            public List<ListaRangoServicio> getListaRangoServicio() {
                if (listaRangoServicio == null) {
                    listaRangoServicio = new ArrayList<ListaRangoServicio>();
                }
                return this.listaRangoServicio;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="nivel" type="{http://core.service.sad.api.claro.com/}nivel" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nivel"
        })
        public static class NivelArray {

            protected List<Nivel> nivel;

            /**
             * Gets the value of the nivel property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the nivel property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getNivel().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Nivel }
             * 
             * 
             */
            public List<Nivel> getNivel() {
                if (nivel == null) {
                    nivel = new ArrayList<Nivel>();
                }
                return this.nivel;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="nivelServicio" type="{http://core.service.sad.api.claro.com/}nivelServicio" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nivelServicio"
        })
        public static class NivelServicioArray {

            protected List<NivelServicio> nivelServicio;

            /**
             * Gets the value of the nivelServicio property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the nivelServicio property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getNivelServicio().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link NivelServicio }
             * 
             * 
             */
            public List<NivelServicio> getNivelServicio() {
                if (nivelServicio == null) {
                    nivelServicio = new ArrayList<NivelServicio>();
                }
                return this.nivelServicio;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="nivelServicioFibra" type="{http://core.service.sad.api.claro.com/}nivelServicioFibra" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nivelServicioFibra"
        })
        public static class NivelServiciofibraArray {

            protected List<NivelServicioFibra> nivelServicioFibra;

            /**
             * Gets the value of the nivelServicioFibra property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the nivelServicioFibra property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getNivelServicioFibra().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link NivelServicioFibra }
             * 
             * 
             */
            public List<NivelServicioFibra> getNivelServicioFibra() {
                if (nivelServicioFibra == null) {
                    nivelServicioFibra = new ArrayList<NivelServicioFibra>();
                }
                return this.nivelServicioFibra;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="provincia" type="{http://core.service.sad.api.claro.com/}provincia" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "provincia"
        })
        public static class ProvinciaArray {

            protected List<Provincia> provincia;

            /**
             * Gets the value of the provincia property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the provincia property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getProvincia().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Provincia }
             * 
             * 
             */
            public List<Provincia> getProvincia() {
                if (provincia == null) {
                    provincia = new ArrayList<Provincia>();
                }
                return this.provincia;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="rangoDireccion" type="{http://core.service.sad.api.claro.com/}rangoDireccion" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rangoDireccion"
        })
        public static class RangoDireccionArray {

            protected List<RangoDireccion> rangoDireccion;

            /**
             * Gets the value of the rangoDireccion property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rangoDireccion property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRangoDireccion().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RangoDireccion }
             * 
             * 
             */
            public List<RangoDireccion> getRangoDireccion() {
                if (rangoDireccion == null) {
                    rangoDireccion = new ArrayList<RangoDireccion>();
                }
                return this.rangoDireccion;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="rangoServicio" type="{http://core.service.sad.api.claro.com/}rangoServicio" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rangoServicio"
        })
        public static class RangoServicioArray {

            protected List<RangoServicio> rangoServicio;

            /**
             * Gets the value of the rangoServicio property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rangoServicio property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRangoServicio().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RangoServicio }
             * 
             * 
             */
            public List<RangoServicio> getRangoServicio() {
                if (rangoServicio == null) {
                    rangoServicio = new ArrayList<RangoServicio>();
                }
                return this.rangoServicio;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="rangoServicioFibra" type="{http://core.service.sad.api.claro.com/}rangoServicioFibra" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rangoServicioFibra"
        })
        public static class RangoServicioFibraArray {

            protected List<RangoServicioFibra> rangoServicioFibra;

            /**
             * Gets the value of the rangoServicioFibra property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rangoServicioFibra property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRangoServicioFibra().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RangoServicioFibra }
             * 
             * 
             */
            public List<RangoServicioFibra> getRangoServicioFibra() {
                if (rangoServicioFibra == null) {
                    rangoServicioFibra = new ArrayList<RangoServicioFibra>();
                }
                return this.rangoServicioFibra;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="region" type="{http://core.service.sad.api.claro.com/}region" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "region"
        })
        public static class RegionArray {

            protected List<Region> region;

            /**
             * Gets the value of the region property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the region property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRegion().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Region }
             * 
             * 
             */
            public List<Region> getRegion() {
                if (region == null) {
                    region = new ArrayList<Region>();
                }
                return this.region;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="sector" type="{http://core.service.sad.api.claro.com/}sector" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "sector"
        })
        public static class SectorArray {

            protected List<Sector> sector;

            /**
             * Gets the value of the sector property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the sector property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSector().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Sector }
             * 
             * 
             */
            public List<Sector> getSector() {
                if (sector == null) {
                    sector = new ArrayList<Sector>();
                }
                return this.sector;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tipo" type="{http://core.service.sad.api.claro.com/}tipo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tipo"
        })
        public static class TipoArray {

            protected List<Tipo> tipo;

            /**
             * Gets the value of the tipo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the tipo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTipo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Tipo }
             * 
             * 
             */
            public List<Tipo> getTipo() {
                if (tipo == null) {
                    tipo = new ArrayList<Tipo>();
                }
                return this.tipo;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="zonaTarifaria" type="{http://core.service.sad.api.claro.com/}zonaTarifaria" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "zonaTarifaria"
        })
        public static class ZonaTarifariaArray {

            protected List<ZonaTarifaria> zonaTarifaria;

            /**
             * Gets the value of the zonaTarifaria property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the zonaTarifaria property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getZonaTarifaria().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ZonaTarifaria }
             * 
             * 
             */
            public List<ZonaTarifaria> getZonaTarifaria() {
                if (zonaTarifaria == null) {
                    zonaTarifaria = new ArrayList<ZonaTarifaria>();
                }
                return this.zonaTarifaria;
            }

        }

    }

}
