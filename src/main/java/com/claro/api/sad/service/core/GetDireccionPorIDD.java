
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDireccionPorIDD complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDireccionPorIDD">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDireccionPorIDD", propOrder = {
    "idd"
})
public class GetDireccionPorIDD {

    @XmlElement(name = "IDD", namespace = "")
    protected String idd;

    /**
     * Gets the value of the idd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDD() {
        return idd;
    }

    /**
     * Sets the value of the idd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDD(String value) {
        this.idd = value;
    }

}
