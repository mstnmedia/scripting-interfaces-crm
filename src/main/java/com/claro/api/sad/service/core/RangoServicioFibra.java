
package com.claro.api.sad.service.core;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rangoServicioFibra complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rangoServicioFibra">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actualizado" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="calle" type="{http://core.service.sad.api.claro.com/}calle" minOccurs="0"/>
 *         &lt;element name="idt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idtRangoSistExt" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="modo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nivelcad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nivelesServicioFibra" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nivelServicioFibra" type="{http://core.service.sad.api.claro.com/}nivelServicioFibra" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="postFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="postIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="preFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="preIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rangoFin" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rangoIni" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rangoServicioFibra", propOrder = {
    "actualizado",
    "calle",
    "idt",
    "idtRangoSistExt",
    "modo",
    "nivelcad",
    "nivelesServicioFibra",
    "postFin",
    "postIni",
    "preFin",
    "preIni",
    "rangoFin",
    "rangoIni"
})
public class RangoServicioFibra {

    @XmlElement(namespace = "")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actualizado;
    protected Calle calle;
    @XmlElement(namespace = "")
    protected Long idt;
    @XmlElement(namespace = "")
    protected int idtRangoSistExt;
    @XmlElement(namespace = "")
    protected int modo;
    @XmlElement(namespace = "")
    protected String nivelcad;
    @XmlElement(namespace = "")
    protected RangoServicioFibra.NivelesServicioFibra nivelesServicioFibra;
    @XmlElement(namespace = "")
    protected String postFin;
    @XmlElement(namespace = "")
    protected String postIni;
    @XmlElement(namespace = "")
    protected String preFin;
    @XmlElement(namespace = "")
    protected String preIni;
    @XmlElement(namespace = "")
    protected int rangoFin;
    @XmlElement(namespace = "")
    protected int rangoIni;

    /**
     * Gets the value of the actualizado property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualizado() {
        return actualizado;
    }

    /**
     * Sets the value of the actualizado property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualizado(XMLGregorianCalendar value) {
        this.actualizado = value;
    }

    /**
     * Gets the value of the calle property.
     * 
     * @return
     *     possible object is
     *     {@link Calle }
     *     
     */
    public Calle getCalle() {
        return calle;
    }

    /**
     * Sets the value of the calle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Calle }
     *     
     */
    public void setCalle(Calle value) {
        this.calle = value;
    }

    /**
     * Gets the value of the idt property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdt() {
        return idt;
    }

    /**
     * Sets the value of the idt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdt(Long value) {
        this.idt = value;
    }

    /**
     * Gets the value of the idtRangoSistExt property.
     * 
     */
    public int getIdtRangoSistExt() {
        return idtRangoSistExt;
    }

    /**
     * Sets the value of the idtRangoSistExt property.
     * 
     */
    public void setIdtRangoSistExt(int value) {
        this.idtRangoSistExt = value;
    }

    /**
     * Gets the value of the modo property.
     * 
     */
    public int getModo() {
        return modo;
    }

    /**
     * Sets the value of the modo property.
     * 
     */
    public void setModo(int value) {
        this.modo = value;
    }

    /**
     * Gets the value of the nivelcad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNivelcad() {
        return nivelcad;
    }

    /**
     * Sets the value of the nivelcad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNivelcad(String value) {
        this.nivelcad = value;
    }

    /**
     * Gets the value of the nivelesServicioFibra property.
     * 
     * @return
     *     possible object is
     *     {@link RangoServicioFibra.NivelesServicioFibra }
     *     
     */
    public RangoServicioFibra.NivelesServicioFibra getNivelesServicioFibra() {
        return nivelesServicioFibra;
    }

    /**
     * Sets the value of the nivelesServicioFibra property.
     * 
     * @param value
     *     allowed object is
     *     {@link RangoServicioFibra.NivelesServicioFibra }
     *     
     */
    public void setNivelesServicioFibra(RangoServicioFibra.NivelesServicioFibra value) {
        this.nivelesServicioFibra = value;
    }

    /**
     * Gets the value of the postFin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostFin() {
        return postFin;
    }

    /**
     * Sets the value of the postFin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostFin(String value) {
        this.postFin = value;
    }

    /**
     * Gets the value of the postIni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostIni() {
        return postIni;
    }

    /**
     * Sets the value of the postIni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostIni(String value) {
        this.postIni = value;
    }

    /**
     * Gets the value of the preFin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreFin() {
        return preFin;
    }

    /**
     * Sets the value of the preFin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreFin(String value) {
        this.preFin = value;
    }

    /**
     * Gets the value of the preIni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreIni() {
        return preIni;
    }

    /**
     * Sets the value of the preIni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreIni(String value) {
        this.preIni = value;
    }

    /**
     * Gets the value of the rangoFin property.
     * 
     */
    public int getRangoFin() {
        return rangoFin;
    }

    /**
     * Sets the value of the rangoFin property.
     * 
     */
    public void setRangoFin(int value) {
        this.rangoFin = value;
    }

    /**
     * Gets the value of the rangoIni property.
     * 
     */
    public int getRangoIni() {
        return rangoIni;
    }

    /**
     * Sets the value of the rangoIni property.
     * 
     */
    public void setRangoIni(int value) {
        this.rangoIni = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nivelServicioFibra" type="{http://core.service.sad.api.claro.com/}nivelServicioFibra" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nivelServicioFibra"
    })
    public static class NivelesServicioFibra {

        protected List<NivelServicioFibra> nivelServicioFibra;

        /**
         * Gets the value of the nivelServicioFibra property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the nivelServicioFibra property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNivelServicioFibra().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NivelServicioFibra }
         * 
         * 
         */
        public List<NivelServicioFibra> getNivelServicioFibra() {
            if (nivelServicioFibra == null) {
                nivelServicioFibra = new ArrayList<NivelServicioFibra>();
            }
            return this.nivelServicioFibra;
        }

    }

}
