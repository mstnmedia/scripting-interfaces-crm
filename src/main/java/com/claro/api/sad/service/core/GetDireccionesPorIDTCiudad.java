
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDireccionesPorIDTCiudad complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDireccionesPorIDTCiudad">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDTCiudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDireccionesPorIDTCiudad", propOrder = {
    "idtCiudad"
})
public class GetDireccionesPorIDTCiudad {

    @XmlElement(name = "IDTCiudad", namespace = "")
    protected String idtCiudad;

    /**
     * Gets the value of the idtCiudad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDTCiudad() {
        return idtCiudad;
    }

    /**
     * Sets the value of the idtCiudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDTCiudad(String value) {
        this.idtCiudad = value;
    }

}
