
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDireccionesPorIDTBarrio complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDireccionesPorIDTBarrio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDTBarrio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDireccionesPorIDTBarrio", propOrder = {
    "idtBarrio"
})
public class GetDireccionesPorIDTBarrio {

    @XmlElement(name = "IDTBarrio", namespace = "")
    protected String idtBarrio;

    /**
     * Gets the value of the idtBarrio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDTBarrio() {
        return idtBarrio;
    }

    /**
     * Sets the value of the idtBarrio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDTBarrio(String value) {
        this.idtBarrio = value;
    }

}
