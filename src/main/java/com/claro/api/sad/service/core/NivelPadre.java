
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nivelPadre complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nivelPadre">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="establecimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="limite" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ElementoPadre" type="{http://core.service.sad.api.claro.com/}nivelPadre" minOccurs="0"/>
 *         &lt;element name="repetible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="secuencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo" type="{http://core.service.sad.api.claro.com/}tipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nivelPadre", propOrder = {
    "establecimiento",
    "idt",
    "limite",
    "elementoPadre",
    "repetible",
    "secuencia",
    "tipo"
})
public class NivelPadre {

    @XmlElement(namespace = "")
    protected String establecimiento;
    @XmlElement(namespace = "")
    protected Long idt;
    @XmlElement(namespace = "")
    protected Boolean limite;
    @XmlElement(name = "ElementoPadre", namespace = "")
    protected NivelPadre elementoPadre;
    @XmlElement(namespace = "")
    protected Boolean repetible;
    @XmlElement(namespace = "")
    protected String secuencia;
    protected Tipo tipo;

    /**
     * Gets the value of the establecimiento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstablecimiento() {
        return establecimiento;
    }

    /**
     * Sets the value of the establecimiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstablecimiento(String value) {
        this.establecimiento = value;
    }

    /**
     * Gets the value of the idt property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdt() {
        return idt;
    }

    /**
     * Sets the value of the idt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdt(Long value) {
        this.idt = value;
    }

    /**
     * Gets the value of the limite property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLimite() {
        return limite;
    }

    /**
     * Sets the value of the limite property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLimite(Boolean value) {
        this.limite = value;
    }

    /**
     * Gets the value of the elementoPadre property.
     * 
     * @return
     *     possible object is
     *     {@link NivelPadre }
     *     
     */
    public NivelPadre getElementoPadre() {
        return elementoPadre;
    }

    /**
     * Sets the value of the elementoPadre property.
     * 
     * @param value
     *     allowed object is
     *     {@link NivelPadre }
     *     
     */
    public void setElementoPadre(NivelPadre value) {
        this.elementoPadre = value;
    }

    /**
     * Gets the value of the repetible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRepetible() {
        return repetible;
    }

    /**
     * Sets the value of the repetible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRepetible(Boolean value) {
        this.repetible = value;
    }

    /**
     * Gets the value of the secuencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecuencia() {
        return secuencia;
    }

    /**
     * Sets the value of the secuencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecuencia(String value) {
        this.secuencia = value;
    }

    /**
     * Gets the value of the tipo property.
     * 
     * @return
     *     possible object is
     *     {@link Tipo }
     *     
     */
    public Tipo getTipo() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Tipo }
     *     
     */
    public void setTipo(Tipo value) {
        this.tipo = value;
    }

}
