
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDireccionesPorIDTCalle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDireccionesPorIDTCalle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDTCalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDireccionesPorIDTCalle", propOrder = {
    "idtCalle"
})
public class GetDireccionesPorIDTCalle {

    @XmlElement(name = "IDTCalle", namespace = "")
    protected String idtCalle;

    /**
     * Gets the value of the idtCalle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDTCalle() {
        return idtCalle;
    }

    /**
     * Sets the value of the idtCalle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDTCalle(String value) {
        this.idtCalle = value;
    }

}
