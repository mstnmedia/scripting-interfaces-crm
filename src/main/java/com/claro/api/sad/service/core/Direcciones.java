
package com.claro.api.sad.service.core;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for direcciones complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="direcciones">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actualizado" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="inmueble" type="{http://core.service.sad.api.claro.com/}inmuebleCalle" minOccurs="0"/>
 *         &lt;element name="latitud" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="longitud" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="niveles" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nivel" type="{http://core.service.sad.api.claro.com/}niveles" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "direcciones", propOrder = {
    "actualizado",
    "idt",
    "inmueble",
    "latitud",
    "longitud",
    "niveles"
})
public class Direcciones {

    @XmlElement(namespace = "")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actualizado;
    @XmlElement(namespace = "")
    protected Long idt;
    protected InmuebleCalle inmueble;
    @XmlElement(namespace = "")
    protected Double latitud;
    @XmlElement(namespace = "")
    protected Double longitud;
    @XmlElement(namespace = "")
    protected Direcciones.Niveles niveles;

    /**
     * Gets the value of the actualizado property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualizado() {
        return actualizado;
    }

    /**
     * Sets the value of the actualizado property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualizado(XMLGregorianCalendar value) {
        this.actualizado = value;
    }

    /**
     * Gets the value of the idt property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdt() {
        return idt;
    }

    /**
     * Sets the value of the idt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdt(Long value) {
        this.idt = value;
    }

    /**
     * Gets the value of the inmueble property.
     * 
     * @return
     *     possible object is
     *     {@link InmuebleCalle }
     *     
     */
    public InmuebleCalle getInmueble() {
        return inmueble;
    }

    /**
     * Sets the value of the inmueble property.
     * 
     * @param value
     *     allowed object is
     *     {@link InmuebleCalle }
     *     
     */
    public void setInmueble(InmuebleCalle value) {
        this.inmueble = value;
    }

    /**
     * Gets the value of the latitud property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLatitud() {
        return latitud;
    }

    /**
     * Sets the value of the latitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLatitud(Double value) {
        this.latitud = value;
    }

    /**
     * Gets the value of the longitud property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLongitud() {
        return longitud;
    }

    /**
     * Sets the value of the longitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLongitud(Double value) {
        this.longitud = value;
    }

    /**
     * Gets the value of the niveles property.
     * 
     * @return
     *     possible object is
     *     {@link Direcciones.Niveles }
     *     
     */
    public Direcciones.Niveles getNiveles() {
        return niveles;
    }

    /**
     * Sets the value of the niveles property.
     * 
     * @param value
     *     allowed object is
     *     {@link Direcciones.Niveles }
     *     
     */
    public void setNiveles(Direcciones.Niveles value) {
        this.niveles = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nivel" type="{http://core.service.sad.api.claro.com/}niveles" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nivel"
    })
    public static class Niveles {

        protected List<com.claro.api.sad.service.core.Niveles> nivel;

        /**
         * Gets the value of the nivel property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the nivel property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNivel().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link com.claro.api.sad.service.core.Niveles }
         * 
         * 
         */
        public List<com.claro.api.sad.service.core.Niveles> getNivel() {
            if (nivel == null) {
                nivel = new ArrayList<com.claro.api.sad.service.core.Niveles>();
            }
            return this.nivel;
        }

    }

}
