
package com.claro.api.sad.service.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateDireccion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateDireccion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://core.service.sad.api.claro.com/}DireccionBO" minOccurs="0"/>
 *         &lt;element name="IDT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateDireccion", propOrder = {
    "direccionBO",
    "idt"
})
public class UpdateDireccion {

    @XmlElement(name = "DireccionBO")
    protected DireccionBO direccionBO;
    @XmlElement(name = "IDT", namespace = "")
    protected String idt;

    /**
     * Gets the value of the direccionBO property.
     * 
     * @return
     *     possible object is
     *     {@link DireccionBO }
     *     
     */
    public DireccionBO getDireccionBO() {
        return direccionBO;
    }

    /**
     * Sets the value of the direccionBO property.
     * 
     * @param value
     *     allowed object is
     *     {@link DireccionBO }
     *     
     */
    public void setDireccionBO(DireccionBO value) {
        this.direccionBO = value;
    }

    /**
     * Gets the value of the idt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDT() {
        return idt;
    }

    /**
     * Sets the value of the idt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDT(String value) {
        this.idt = value;
    }

}
