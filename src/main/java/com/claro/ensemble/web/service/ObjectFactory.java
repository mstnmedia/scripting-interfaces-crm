
package com.claro.ensemble.web.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.claro.ensemble.web.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetClienteWebServiceFault_QNAME = new QName("http://service.web.ensemble.claro.com/", "GetClienteWebServiceFault");
    private final static QName _GetCustomerInfoResponse_QNAME = new QName("http://service.web.ensemble.claro.com/", "getCustomerInfoResponse");
    private final static QName _GetCustomerInfo_QNAME = new QName("http://service.web.ensemble.claro.com/", "getCustomerInfo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.claro.ensemble.web.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WebServiceFaultDetail }
     * 
     */
    public WebServiceFaultDetail createWebServiceFaultDetail() {
        return new WebServiceFaultDetail();
    }

    /**
     * Create an instance of {@link ErrorDetail }
     * 
     */
    public ErrorDetail createErrorDetail() {
        return new ErrorDetail();
    }

    /**
     * Create an instance of {@link GetCustomerInfo }
     * 
     */
    public GetCustomerInfo createGetCustomerInfo() {
        return new GetCustomerInfo();
    }

    /**
     * Create an instance of {@link OutCustomerInfo }
     * 
     */
    public OutCustomerInfo createOutCustomerInfo() {
        return new OutCustomerInfo();
    }

    /**
     * Create an instance of {@link InCustomerInfo }
     * 
     */
    public InCustomerInfo createInCustomerInfo() {
        return new InCustomerInfo();
    }

    /**
     * Create an instance of {@link GetCustomerInfoResponse }
     * 
     */
    public GetCustomerInfoResponse createGetCustomerInfoResponse() {
        return new GetCustomerInfoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebServiceFaultDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.web.ensemble.claro.com/", name = "GetClienteWebServiceFault")
    public JAXBElement<WebServiceFaultDetail> createGetClienteWebServiceFault(WebServiceFaultDetail value) {
        return new JAXBElement<WebServiceFaultDetail>(_GetClienteWebServiceFault_QNAME, WebServiceFaultDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.web.ensemble.claro.com/", name = "getCustomerInfoResponse")
    public JAXBElement<GetCustomerInfoResponse> createGetCustomerInfoResponse(GetCustomerInfoResponse value) {
        return new JAXBElement<GetCustomerInfoResponse>(_GetCustomerInfoResponse_QNAME, GetCustomerInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.web.ensemble.claro.com/", name = "getCustomerInfo")
    public JAXBElement<GetCustomerInfo> createGetCustomerInfo(GetCustomerInfo value) {
        return new JAXBElement<GetCustomerInfo>(_GetCustomerInfo_QNAME, GetCustomerInfo.class, null, value);
    }

}
