/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.iptv;

import com.claro.spc.servlet.Bitacora;
import com.claro.spc.servlet.Canal;
import com.claro.spc.servlet.Cliente;
import com.claro.spc.servlet.Compra;
import com.claro.spc.servlet.Paquete;
import com.claro.spc.servlet.SPC;
import com.claro.spc.servlet.SPCService;
import com.claro.spc.servlet.STB;
import com.claro.spc.servlet.Transaccion;
import com.mstn.scripting.core.Utils;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Clase que envuelve una instancia del endpoint de IPTV y hace disponibles los
 * métodos del web service a la interfaz dinámica que la utiliza. En esta clase
 * los parámetros que son tipo lista son envueltos en una clase local para
 * lograr que se quede en runtime la información del tipo de lista, así se podrá
 * mostrar al usuario con un formulario con una tabla con las columnas
 * correctas.
 *
 * @author amatos
 */
public class SPCWrapper {

	private SPC base = null;

	public SPCWrapper() {
		try {
			this.base = new SPCService().getSPC();
		} catch (Exception ex) {
			Utils.logException(this.getClass(), "Error inicializando SPC Service", ex);
			throw ex;
		}
	}

	public ListCliente consultarClientePorGuidParcial(String guidParcial, Transaccion tx) {
		ListCliente result = new ListCliente();
		result.list = base.consultarClientePorGuidParcial(guidParcial, tx);
		return result;
	}

	public void probarCambioCanalDeSTBPorGuid(String guid, String callLetter, Transaccion tx) {
		base.probarCambioCanalDeSTBPorGuid(guid, callLetter, tx);
	}

	public void resetearPinComprasPorGuid(String guid, String pin4Numeros, Transaccion tx) {
		base.resetearPinComprasPorGuid(guid, pin4Numeros, tx);
	}

	public void reemplazarSTB(String guid, String newGuid, Transaccion tx) {
		base.reemplazarSTB(guid, newGuid, tx);
	}

	public ListSTB consultarSTBPorCuenta(String cuenta, Transaccion tx) {
		ListSTB result = new ListSTB();
		result.list = base.consultarSTBPorCuenta(cuenta, tx);
		return result;
	}

	public ListPaquete consultarPaquetesPorCuenta(String cuenta, Transaccion tx) {
		ListPaquete result = new ListPaquete();
		result.list = base.consultarPaquetesPorCuenta(cuenta, tx);
		return result;
	}

	public ListCliente consultarClientePorCuenta(String cuenta, Transaccion tx) {
		ListCliente result = new ListCliente();
		result.list = base.consultarClientePorCuenta(cuenta, tx);
		return result;
	}

	public void resetearPinAdultosPorGuid(String guid, String pin4Numeros, Transaccion tx) {
		base.resetearPinAdultosPorGuid(guid, pin4Numeros, tx);
	}

	public void resetearPinParentalPorGuid(String guid, String pin4Numeros, Transaccion tx) {
		base.resetearPinParentalPorGuid(guid, pin4Numeros, tx);
	}

	public void removerPinParentalPorGuid(String guid, Transaccion tx) {
		base.removerPinParentalPorGuid(guid, tx);
	}

	public void removerPinComprasPorGuid(String guid, Transaccion tx) {
		base.removerPinComprasPorGuid(guid, tx);
	}

	public void removerPinAdultosPorGuid(String guid, Transaccion tx) {
		base.removerPinAdultosPorGuid(guid, tx);
	}

	public void enviarMensajeASTBPorGuid(String guid, String mensaje, Transaccion tx) {
		base.enviarMensajeASTBPorGuid(guid, mensaje, tx);
	}

	public ListBitacora consultarHistoricoCambios(String cuenta, XMLGregorianCalendar fechaInicio, XMLGregorianCalendar fechaFin, Transaccion tx) {
		ListBitacora result = new ListBitacora();
		result.list = base.consultarHistoricoCambios(cuenta, fechaInicio, fechaFin, tx);
		return result;
	}

	public ListCompra consultarCompras(String cuenta, XMLGregorianCalendar fechaInicio, XMLGregorianCalendar fechaFin, Transaccion tx) {
		ListCompra result = new ListCompra();
		result.list = base.consultarCompras(cuenta, fechaInicio, fechaFin, tx);
		return result;
	}

	public ListCanal consultarCanales(String cuenta, Transaccion tx) {
		ListCanal result = new ListCanal();
		result.list = base.consultarCanales(cuenta, tx);
		return result;
	}

	public void reiniciarSTBPorGuid(String guid, Transaccion tx) {
		base.reiniciarSTBPorGuid(guid, tx);
	}

	public void drastbPorGuid(String guid, Transaccion tx) {
		base.drastbPorGuid(guid, tx);
	}

	public void desbloquearCompra(String cuenta, Transaccion tx) {
		base.desbloquearCompra(cuenta, tx);
	}

	public void bloquearCompra(String cuenta, Transaccion tx) {
		base.bloquearCompra(cuenta, tx);
	}

	static public class ListCliente {

		public List<Cliente> list;
	}

	static public class ListSTB {

		public List<STB> list;
	}

	static public class ListPaquete {

		public List<Paquete> list;
	}

	static public class ListBitacora {

		public List<Bitacora> list;
	}

	static public class ListCompra {

		public List<Compra> list;
	}

	static public class ListCanal {

		public List<Canal> list;
	}
}
