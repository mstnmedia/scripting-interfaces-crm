/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.iptv;

import com.claro.spc.servlet.Transaccion;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * Interfaz dinámica que conecta a Scripting con IPTV.
 *
 * @author amatos
 */
public class IPTV_SPC extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = IPTV_SPC.class;
		try {
			SOAP = new SPCWrapper();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public IPTV_SPC() {
		super(0, "iptv", SOAP);
//		showBaseLogs = true;
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("iptv"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("cuenta")) {
			field.setLabel("No. de Teléfono");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
		} else if (fieldName.endsWith("guidParcial")) {
			field.setLabel("GUID Parcial");
		} else if (fieldName.endsWith("guid")) {
			field.setLabel("GUID");
		} else if (fieldName.endsWith("newGuid")) {
			field.setLabel("Nuevo GUID");
		} else if (fieldName.endsWith("callLetter")) {
			field.setLabel("Nombre de Canal");
		} else if (fieldName.endsWith("pin4Numeros")) {
			field.setLabel("Pin de 4 Dígitos");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate("return !!value && /^\\d{4}$/.test(value)");
		} else if (fieldName.endsWith("mensaje")) {
			field.setLabel("Mensaje");
		} else if (fieldName.endsWith("fechaInicio")) {
			field.setLabel("Fecha de Inicio");
			field.setId_type(InterfaceFieldTypes.DATETIME);
		} else if (fieldName.endsWith("fechaFin")) {
			field.setLabel("Fecha de Fin");
			field.setId_type(InterfaceFieldTypes.DATETIME);
		}
		return field;
	}

	@Override
	protected List<Interface_Field> getInterfaceFieldFromClass(Interface inter, String paramName, String parentName, Class _class, boolean prefix) throws Exception {
		if (_class == Transaccion.class) {
			return Arrays.asList();
		}
		return super.getInterfaceFieldFromClass(inter, paramName, parentName, _class, prefix);
	}

	/**
	 *
	 * @param paramName
	 * @param paramClass
	 * @param paramInstance
	 * @param paramField
	 * @return
	 * @throws Exception
	 */
	@Override
	protected Object getParamObjectFromForm(String paramName, Class paramClass, Object paramInstance, Field paramField) throws Exception {
		if (paramClass == Transaccion.class) {
			Transaccion tran = new Transaccion();
			tran.setNumeroTransaction(String.valueOf(transaction.getId()));
			tran.setSistema(InterfaceConfigs.get("iptv_system"));
			return tran;
		}
		return super.getParamObjectFromForm(paramName, paramClass, paramInstance, paramField);
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("iptv").toString();
		InterfaceConfigs.get("iptv_system").toString();
		super.test();
	}
}
