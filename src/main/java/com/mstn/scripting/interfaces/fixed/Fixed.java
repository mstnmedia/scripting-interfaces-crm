/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.fixed;

import _do.com.claro.soa.model.billing.Subscription;
import _do.com.claro.soa.model.product.Product;
import _do.com.claro.soa.services.product.getfixedproduct.GetFixedProduct;
import _do.com.claro.soa.services.product.getfixedproduct.GetFixedProductRequest;
import _do.com.claro.soa.services.product.getfixedproduct.GetFixedProductResponse;
import _do.com.claro.soa.services.product.getfixedproduct.GetFixedProduct_Service;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.TelephoneNumber_Ext;

/**
 * Clase que agrega informaciones de detalles de subscripciones a la consulta de
 * subscripciones del cliente de la pantalla de crear nueva transacción.
 *
 * @author amatos
 */
public class Fixed {

	static public Product SetFixedProduct(Subscription subscription) {
		String customerID = CustomerID_Ext.toString(subscription.getCustomerID());
		String subscriberNo = TelephoneNumber_Ext.toString(subscription.getSubscriberNo());
		String productCode = subscription.getProduct() == null ? null : subscription.getProduct().getProductType().getProductCode();
		Product product = GetFixedProduct(customerID, subscriberNo, productCode);
		if (product != null) {
			subscription.setProduct(product);
		}
		return product;
	}

	static public Product GetFixedProduct(String customerID, String subscriberNo, String productCode) {
		Product product = null;
		long start = DATE.nowAsLong();
		try {
			GetFixedProduct soap = GetFixedProduct_Service.getInstance();
			GetFixedProductRequest request = new GetFixedProductRequest();
			request.setCustomerID(new CustomerID_Ext(customerID));
			request.setSubscriberNo(new TelephoneNumber_Ext(subscriberNo));
			request.setProductCode(productCode);

			GetFixedProductResponse response = soap.getFixedProduct(request);
			product = response.getProduct();
			Utils.logFinest(Fixed.class, "Fixed GetFixedProduct:" + JSON.toString(product) + "\n");
		} catch (Exception ex) {
			Utils.logException(Fixed.class, customerID, ex);
		}
		long end = DATE.nowAsLong();
		Utils.logTrace(Fixed.class, "Mixed GetFixedProduct: " + (end - start));
		return product;
	}
}
