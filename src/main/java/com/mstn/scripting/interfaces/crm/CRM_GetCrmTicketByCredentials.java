/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetCrmTicketByCredentials;
import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetCrmTicketByCredentials_Service;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;
import java.util.List;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

/**
 * Interfaz dinámica que obtiene un token que se utiliza para crear
 * interacciones en la interfaz {@link CRM_CreateCustomerInteraction}.
 *
 * @author amatos
 */
public class CRM_GetCrmTicketByCredentials extends InterfaceBase {

	static GetCrmTicketByCredentials SOAP = null;

	static {
		try {
			SOAP = GetCrmTicketByCredentials_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(CRM_GetCrmTicketByCredentials.class, "Error instanciando endpoint GetCrmTicketByCredentials", ex);
			throw ex;
		}
	}

	public CRM_GetCrmTicketByCredentials() {
		super(0, "crmticketbycredentials", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("crm_ticketbycredentials"));
		return inter;
	}

	@Override
	protected List<Interface_Field> getInterfaceFields(Method method, Interface inter) throws Exception {
		List<Interface_Field> fields = super.getInterfaceFields(method, inter);
		for (int i = 0; i < fields.size(); i++) {
			Interface_Field field = fields.get(i);
			if (field.getName().endsWith("_in")) {
				field.setLabel("Entrada");
				field.setRequired(false);
			}
		}
		fields.add(new Interface_Field(0, inter.getId(), inter.getName() + "_user", "Usuario", InterfaceFieldTypes.TEXT, true));
		fields.add(new Interface_Field(0, inter.getId(), inter.getName() + "_password", "Contraseña", InterfaceFieldTypes.PASSWORD, false));
		return fields;
	}

	@Override
	protected List<Interface_Field> getStepFields() throws Exception {
		List<Interface_Field> fields = super.getStepFields();
		return fields;
	}

	@Override
	protected Object sendParamsToMethod(Interface inter, Method method, List<Object> params) throws Exception {
		BindingProvider bp = (BindingProvider) this.INSTANCE;
		String username = JSON.getString(form, inter.getName() + "_user");
		String password = JSON.getString(form, inter.getName() + "_password");
		List<Handler> handlerChain = bp.getBinding().getHandlerChain();
		handlerChain.add(new SOAPHeaderHandler(username, password));
		bp.getBinding().setHandlerChain(handlerChain);

		return super.sendParamsToMethod(inter, method, params);
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("crm_ticketbycredentials").toString();
		super.test();
	}

}
