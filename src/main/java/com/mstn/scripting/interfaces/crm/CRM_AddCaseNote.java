/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.crm.AddCaseNotesResponse;
import _do.com.claro.soa.crm.CrmSoaPort;
import _do.com.claro.soa.crm.StatusTrans;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import java.util.Arrays;
import java.util.List;

/**
 * Interfaz estática que intentó implementar el método  {@link CrmSoaPort#addCaseNotes(_do.com.claro.soa.crm.AddCaseNotesRequest)
 * } implementado en la interfaz dinámica {@link CRM_ESB}.
 *
 * @author amatos
 * @deprecated
 */
public class CRM_AddCaseNote extends InterfaceConnectorBase {

	private final static int ID = 2102;
	private final static String NAME = "crm_add_case_note";
	private final static String LABEL = "CRM - Agregar nota a caso";
	private final static String SERVER_URL = "http://SOADEVENV1:8010/soa-infra/services/crm/CRM_ESB!1.0/CRM.CRM_RS_ep";
	private final static String VALUE_STEP = "success";
	private final static boolean DELETED = false;

	private final String prefix = NAME + "_";

	private AddCaseNotesResponse interfaceResponse;

	public CRM_AddCaseNote() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
		showBaseLogs = true;
	}

	private final String CASE_ID = prefix + "title";
	private final String CLIENT_SYSTEM = prefix + "client_system";
	private final String NOTES = prefix + "notes";

	@Override
	public List<Interface_Field> getFields() {
		return Arrays.asList(
				//(id, id_interface, name, label, id_type, required)
				new Interface_Field(1, ID, CASE_ID, "ID del caso", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(2, ID, CLIENT_SYSTEM, "Sistema de cliente", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(3, ID, NOTES, "Notas", InterfaceFieldTypes.TEXT, true)
		);
	}

	private final String STATUS_TRANS = prefix + "StatusTrans";
	private final String EXCEPTION_TRANS = prefix + "ExceptionTrans";

	@Override
	public List<Transaction_Result> getResults() {
		boolean emptyFields = interfaceResponse == null;

		String StatusTrans = "";
		String ExceptionTrans = "";
		if (!emptyFields) {
			StatusTrans statusTrans = interfaceResponse.getStatusTrans();
			StatusTrans = statusTrans.getStatus();
			if (!statusTrans.getException().isNil()) {
				ExceptionTrans = statusTrans.getException().getValue();
			}
		}

		return Arrays.asList(
				//(id_interface, name, label)
				//(id, id_interface, name, label, value, props)
				new Transaction_Result(1, ID, STATUS_TRANS, "Estado de la transacción", StatusTrans, ""),
				new Transaction_Result(2, ID, EXCEPTION_TRANS, "Error de la transacción", ExceptionTrans, "")
		);
	}

	@Override
	protected TransactionInterfacesResponse setAsNextStepResponse() throws Exception {
		String caseID = JSON.getString(form, CASE_ID);
		String clientSystem = JSON.getString(form, CLIENT_SYSTEM);
		String notes = JSON.getString(form, NOTES);
		interfaceResponse = CRM.AddCaseNotes(caseID, clientSystem, notes);

		results = getResults();
		response.setResults(results);

		String value = getValueFromJavascript();
		response.setValue(value);

		return response;
	}
}
