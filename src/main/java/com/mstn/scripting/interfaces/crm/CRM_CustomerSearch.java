/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import com.claro.spc.servlet.Cliente;
import com.claro.spc.servlet.Transaccion;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Log;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.ensamble.Ensamble;
import com.mstn.scripting.interfaces.Mixed;
import com.mstn.scripting.interfaces.clarovideo.ClaroVideo;
import com.mstn.scripting.interfaces.dth.DTH_RESTApi;
import com.mstn.scripting.interfaces.dth.DTH_SATSoap;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Customer_Ext;
import com.mstn.scripting.interfaces.ext.IdentityCardID_Ext;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import com.mstn.scripting.interfaces.ext.TelephoneNumber_Ext;
import com.mstn.scripting.interfaces.iptv.SPCWrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import satsservices.ArrayOfClientSubscription;
import satsservices.ClientSubscription;

/**
 * Interfaz estática que se consulta desde la pantalla de crear nueva
 * transacción para consultar clientes por diferentes criterios de búsqueda.
 * Esta interfaz muestra cada contacto de cliente como clientes por separado.
 *
 * @author amatos
 */
public class CRM_CustomerSearch extends InterfaceConnectorBase {

	private final static int ID = 2000;
	private final static String NAME = "crm_customer_search";
	private final static String LABEL = "CRM - Consulta de Clientes";
	private final static String SERVER_URL = "crm.org";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = true;

	private final String SEARCH_TYPE = "search_type";
	private final String SEARCH_VALUE = "search_value";

	private final String CUSTOMER_ID = "customer_id";
	private final String PHONE_NUMBER = "phone_number";
	private final String IDENTITY_CARD_ID = "identity_card_id";
	private final String PASSPORT_ID = "passport_id";
	private final String RNC = "tax_id";

	private final String CUSTOMER_NAME = "customer_name";
	private final String CONTACT_NAME = "contact_name";
	private final String BAN_ACCOUNT_NO = "ban_account_no";
	private final String CLARO_VIDEO_NO = "claro_video_no";
	private final String STB = "stb_no";
	private final String SMART_CARD = "smart_card";
	private final String GUID_PARCIAL = "guid_parcial";

	private Customer_Ext customer;
	private List<Customer_Ext> customerList;
	private Subscription_Ext tempSubscription;
	private List<Subscription_Ext> subscriptionList;
	private final int pageSubscriptions = 1;

	public CRM_CustomerSearch() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		return Arrays.asList(
				//(id, id_interface, name, label, id_type, order_index, default_value)
				new Interface_Field(1, ID, SEARCH_TYPE, "Tipo de búsqueda", InterfaceFieldTypes.TEXT, 1, null),
				new Interface_Field(2, ID, SEARCH_VALUE, "Valor", InterfaceFieldTypes.TEXT, 1, null)
		);
	}

	@Override
	public List<Transaction_Result> getResults() throws Exception {
		boolean empty = customerList == null;
		String strCustomerList = empty ? "" : JSON.toString(customerList);
		String strCustomerResults = empty ? "" : JSON.toString(Customer_Ext.getCustomerResults(customer));
		String strSubscriptionList = empty ? "" : JSON.toString(subscriptionList);
		String strPageNum = empty ? "" : Integer.toString(pageNum);
		String strPageSubscriptions = empty ? "" : Integer.toString(pageSubscriptions);

		return Arrays.asList(
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "customerResults", "Propiedades del cliente", strCustomerResults, "{\"notSave\": true}"),
				new Transaction_Result(2, 0, 0, 0, ID, prefix + "subscriptionList", "Lista de servicios", strSubscriptionList, "{\"notSave\": true}"),
				new Transaction_Result(3, 0, 0, 0, ID, prefix + "customerList", "Lista de clientes", strCustomerList, "{\"notSave\": true}"),
				new Transaction_Result(4, 0, 0, 0, ID, prefix + "pageCustomers", "Página de clientes", strPageNum, "{\"notSave\": true}"),
				new Transaction_Result(5, 0, 0, 0, ID, prefix + "pageSubscriptions", "Página de servicios", strPageSubscriptions, "{\"notSave\": true}")
		);
	}

	List<Customer_Ext> customerListByPhone(String phone) throws Exception {
		phone = Utils.replaceNonDigit(phone);
		tempSubscription = Ensamble.getFirstSubscription(phone);
		if (tempSubscription != null && tempSubscription.getOriginal() != null) {
			String customerID = tempSubscription.getCustomerID();
			Utils.logTrace(this.getClass(), "CustomerSearchByPhone customerID: " + customerID);
			return CRM.GetCustomerListByCustomerID(customerID);
		}
		return null;
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) throws Exception {
		long start = DATE.nowAsLong();
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		ObjectNode json = JSON.newObjectNode();
		response.setJson(json);
		try {
			inter = getInterface();
			form = JSON.toNode(payload.getForm());

			String searchType = form.hasNonNull(SEARCH_TYPE) ? form.get(SEARCH_TYPE).asText() : "";
			boolean validType = validateSearchType(searchType);

			String searchValue = form.hasNonNull(SEARCH_VALUE) ? form.get(SEARCH_VALUE).asText() : "";
			boolean validValue = validateSearchValue(searchType, searchValue);

			if (!validType || !validValue) {
				if (!validType) {
					form.remove(SEARCH_TYPE);
				}
				if (!validValue) {
					form.remove(SEARCH_VALUE);
				}
				setAsMissingRequest(response, json, form);
			} else {
				pageNum = getPageNumFromForm(form);
				json.put(PAGE_NUMBER, pageNum)
						.put(SEARCH_TYPE, searchType)
						.put(SEARCH_VALUE, searchValue);

				long valid = DATE.nowAsLong();
				Utils.logTrace(this.getClass(), "CustomerSearch Validated: " + (valid - start));
				customerList = new ArrayList<>();
				switch (searchType) {
					case PHONE_NUMBER:
						customerList = customerListByPhone(searchValue);
						break;
					case BAN_ACCOUNT_NO:
						String customerID = Ensamble.GetCustomerIDFromBanAccountNo(searchValue);
						customerList = CRM.GetCustomerListByCustomerID(customerID);
						break;
					case CLARO_VIDEO_NO:
						String ban = ClaroVideo.GetBanFromSubscriber(searchValue);
						String customerID2 = Ensamble.GetCustomerIDFromBanAccountNo(ban);
						customerList = CRM.GetCustomerListByCustomerID(customerID2);
						break;
					case STB:
						DTH_SATSoap soapSTB = new DTH_SATSoap();
						ArrayOfClientSubscription clientsSubscription = soapSTB.getClientProfileBySTB(searchValue);
						if (clientsSubscription != null) {
							List<ClientSubscription> clientes = Utils.coalesce(clientsSubscription.getClientSubscription(), Arrays.asList());
							for (ClientSubscription clientSubscription : clientes) {
								List<Customer_Ext> result = customerListByPhone(clientSubscription.getNumber());
								if (result != null) {
									customerList.addAll(result);
								}
							}
						}
						break;
					case SMART_CARD:
//						ClientSubscription clientSubscription = DTH.GetClientProfileById(searchValue, "SMC");
//						if (clientSubscription != null) {
//							customerList = customerListByPhone(clientSubscription.getNumber());
//						}
						DTH_RESTApi apiDTH = new DTH_RESTApi();
						DTH_RESTApi.ClientProfileResponse clientProfile = apiDTH.getClientProfileBySMC(new DTH_RESTApi.ClientProfileSMCRequest(
								String.valueOf(user.getId()), searchValue
						));
						if (clientProfile.Succeeded && clientProfile.Value != null
								&& clientProfile.Value.customerInfo != null && clientProfile.Value.customerInfo.Number != null) {
							String number = clientProfile.Value.customerInfo.Number.replaceAll("\\D", "");
							customerList = customerListByPhone(number);
						}
						break;
					case GUID_PARCIAL: {
						SPCWrapper soap = new SPCWrapper();
						Transaccion tx = new Transaccion();
						tx.setNumeroTransaction("0");
						tx.setSistema(InterfaceConfigs.get("iptv_system"));
						SPCWrapper.ListCliente clientes = soap.consultarClientePorGuidParcial(searchValue, tx);
						if (Utils.listNonNullOrEmpty(clientes.list)) {
							for (int i = 0; i < clientes.list.size(); i++) {
								Cliente cliente = clientes.list.get(i);
								List<Customer_Ext> clienteCustomerList = customerListByPhone(cliente.getCuenta());
								if (Utils.listNonNullOrEmpty(clienteCustomerList)) {
									customerList.addAll(clienteCustomerList);
								}
							}
						}
					}
					break;
					case IDENTITY_CARD_ID:
						customerList = CRM.GetPageCustomerByIdentityCardID(searchValue, pageNum);
						break;
					case PASSPORT_ID:
						customerList = CRM.GetPageCustomerByPassportID(searchValue, pageNum);
						break;
					case RNC:
						customerList = CRM.GetPageCustomerByTaxID(searchValue, pageNum);
						break;
					case CUSTOMER_NAME:
						customerList = CRM.GetPageCustomersByCustomerName(searchValue, pageNum);
						break;
					case CONTACT_NAME:
						customerList = CRM.GetPageCustomersByContactName(searchValue, pageNum);
						break;
					case CUSTOMER_ID:
						customerList = CRM.GetCustomerListByCustomerID(searchValue);
						Optional<Customer_Ext> selectedRole = customerList.stream().filter(i -> i.getCustomerID().equals(searchValue)).findFirst();
						if (selectedRole.isPresent()) {
							customerList.removeIf(i -> i.getCustomerID().equals(searchValue));
							customerList.add(0, selectedRole.get());
						}
						break;
				}
				if (pageNum == 1) {
					if (customerList != null && !customerList.isEmpty()) {
						customer = customerList.get(0);
						long subs = DATE.nowAsLong();
						subscriptionList = Mixed.GetPageSubscriptionsByCustomerID(customer, pageSubscriptions);
						long sube = DATE.nowAsLong();
						Utils.logTrace(this.getClass(), "Customer Subscriptions: " + (sube - subs));
						if (tempSubscription != null && tempSubscription.getOriginal() != null) {
//						if (Arrays.asList(PHONE_NUMBER, STB, SMART_CARD, GUID_PARCIAL).equals(searchType)) {
							subscriptionList.removeIf(i -> i.getId().equals(tempSubscription.getId()));
							subscriptionList.add(0, tempSubscription);
						}
						try {
							Mixed.SetCustomerLastOrder(customer);
						} catch (Exception ex) {
							response.getLogs().add(new Log("CRM_CustomerSearch", "error catching", "Error consultando última orden del cliente", JSON.toString(ex)));
						}
						long oms = DATE.nowAsLong();
						Utils.logTrace(this.getClass(), "Customer OMS: " + (oms - sube));
						try {
							Mixed.SetCustomerExistsInMiClaro(customer);
						} catch (Exception ex) {
							response.getLogs().add(new Log("CRM_CustomerSearch", "error catching", "Error consultando cliente en MiClaro", JSON.toString(ex)));
						}
						long miclaro = DATE.nowAsLong();
						Utils.logTrace(this.getClass(), "Customer MiClaro: " + (miclaro - oms));
					}
				}

				long resps = DATE.nowAsLong();
				List<Transaction_Result> results = getResults();
				inter.setInterface_results(results);
				response.setInterface(inter);
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
				response.setValue(InterfaceOptions.SUCCESS);
				long respe = DATE.nowAsLong();
				Utils.logTrace(this.getClass(), "Setting response: " + (respe - resps));
			}
		} catch (Exception ex) {
			setAsExceptionResponse(response, ex);
		}
		long end = DATE.nowAsLong();
		Utils.logTrace(this.getClass(), "CustomerSearch Total: " + (end - start));
		return response;
	}

	private boolean validateSearchType(String searchType) {
		return !searchType.isEmpty()
				&& Arrays.asList(
						PHONE_NUMBER, IDENTITY_CARD_ID, PASSPORT_ID, RNC,
						CUSTOMER_ID, CONTACT_NAME, CUSTOMER_NAME, BAN_ACCOUNT_NO,
						CLARO_VIDEO_NO, STB, SMART_CARD, GUID_PARCIAL
				).contains(searchType);
	}

	private boolean validateSearchValue(String searchType, String searchValue) {
		if (Utils.stringNonNullOrEmpty(searchValue)) {
			// TODO: Add more search validations
			switch (searchType) {
				case CUSTOMER_ID:
					return CustomerID_Ext.isValid(searchValue);
				case CLARO_VIDEO_NO:
				case PHONE_NUMBER:
					return TelephoneNumber_Ext.isValid(searchValue);
				case IDENTITY_CARD_ID:
					return IdentityCardID_Ext.isValid(searchValue);
				case BAN_ACCOUNT_NO:
					return Utils.stringIsDigits(searchValue);
				case CUSTOMER_NAME:
				case CONTACT_NAME:
				default:
					return true;
			}
		}
		return false;
	}

	@Override
	protected TransactionInterfacesResponse setAsMissingRequest(TransactionInterfacesResponse response, ObjectNode json, ObjectNode form) {
		response.setAction(TransactionInterfaceActions.INVALID_REQUEST);
		response.setValue(InterfaceOptions.MISSING_DATA);
		ArrayNode missing_fields = json.putArray("missing_fields");
		if (!form.hasNonNull(SEARCH_TYPE)) {
			missing_fields.add(SEARCH_TYPE);
		}
		if (!form.hasNonNull(SEARCH_VALUE)) {
			missing_fields.add(SEARCH_VALUE);
		}
		return response;
	}
}
