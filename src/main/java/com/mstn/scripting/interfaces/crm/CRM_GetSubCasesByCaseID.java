/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.services.customer.getsubcasesbycaseid.GetSubCasesByCaseID;
import _do.com.claro.soa.services.customer.getsubcasesbycaseid.GetSubCasesByCaseID_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.lang.reflect.Method;

/**
 *
 * @author amatos
 */
public class CRM_GetSubCasesByCaseID extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = CRM_GetSubCasesByCaseID.class;
		try {
			SOAP = GetSubCasesByCaseID_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public CRM_GetSubCasesByCaseID() {
		super(0, "crmgetsubcasesbycaseid", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("crm_getsubcasesbycaseid"));
		return inter;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("crm_getsubcasesbycaseid").toString();
		super.test();
	}

}
