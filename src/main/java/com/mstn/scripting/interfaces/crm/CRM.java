/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.crm.AddCaseNotesRequest;
import _do.com.claro.soa.crm.AddCaseNotesResponse;
import _do.com.claro.soa.crm.CloseCaseRequest;
import _do.com.claro.soa.crm.CloseCaseResponse;
import _do.com.claro.soa.crm.CreateCaseRequest;
import _do.com.claro.soa.crm.CreateCaseResponse;
import _do.com.claro.soa.crm.CreateInteractionRequest;
import _do.com.claro.soa.crm.CreateInteractionResponse;
import _do.com.claro.soa.crm.CrmSoaPort;
import _do.com.claro.soa.crm.DispatchCaseRequest;
import _do.com.claro.soa.crm.DispatchCaseResponse;
import _do.com.claro.soa.crm.ESBCRMRSService;
import _do.com.claro.soa.crm.UpdateCaseRequest;
import _do.com.claro.soa.crm.UpdateCaseResponse;
import _do.com.claro.soa.crm.UpdateParentChildRequest;
import _do.com.claro.soa.crm.UpdateParentChildResponse;
import _do.com.claro.soa.model.customer.Case;
import _do.com.claro.soa.model.customer.Cases;
import _do.com.claro.soa.model.customer.Contact;
import _do.com.claro.soa.model.customer.Contacts;
import _do.com.claro.soa.model.customer.Customer;
import _do.com.claro.soa.model.customer.Customers;
import _do.com.claro.soa.model.customer.Interaction;
import _do.com.claro.soa.model.customer.Interactions;
import _do.com.claro.soa.model.personidentity.Passport;
import _do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetBusinessCustomersAndContactsByCustomerName;
import _do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetBusinessCustomersAndContactsByCustomerName_Service;
import _do.com.claro.soa.services.customer.getcustomerbycustomerid.GetCustomerByCustomerID;
import _do.com.claro.soa.services.customer.getcustomerbycustomerid.GetCustomerByCustomerID_Service;
import _do.com.claro.soa.services.customer.getcustomercasesbycustomerid.GetCustomerCasesByCustomerID;
import _do.com.claro.soa.services.customer.getcustomercasesbycustomerid.GetCustomerCasesByCustomerID_Service;
import _do.com.claro.soa.services.customer.getcustomercasesbycustomerid.GetRequest;
import _do.com.claro.soa.services.customer.getcustomercasesbycustomerid.GetResponse;
import _do.com.claro.soa.services.customer.getcustomerinteractionsbycustomerid.GetCustomerInteractionsByCustomerID;
import _do.com.claro.soa.services.customer.getcustomerinteractionsbycustomerid.GetCustomerInteractionsByCustomerID_Service;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactidentitycardid.GetCustomersAndContactsByContactIdentityCardID;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactidentitycardid.GetCustomersAndContactsByContactIdentityCardID_Service;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames.GetCustomersAndContactsByContactNames;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames.GetCustomersAndContactsByContactNames_Service;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactpassportid.GetCustomersAndContactsByContactPassportID;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactpassportid.GetCustomersAndContactsByContactPassportID_Service;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbytaxid.GetCustomersAndContactsByTaxID;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbytaxid.GetCustomersAndContactsByTaxID_Service;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.interfaces.ext.Case_Ext;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Customer_Ext;
import com.mstn.scripting.interfaces.ext.IdentityCardID_Ext;
import com.mstn.scripting.interfaces.ext.Interaction_Ext;
import com.mstn.scripting.interfaces.ext.Note_Ext;
import com.mstn.scripting.core.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Clase que contiene las principales consultas a métodos de CRM para obtener
 * información del cliente para la pantalla de crear nueva transacción.
 *
 * @author amatos
 */
public class CRM {

	static public Customer GetCustomerByCustomerID(String customerID) throws Exception {
		Customer customer = null;
		try {
			GetCustomerByCustomerID soap = GetCustomerByCustomerID_Service.getInstance();
			_do.com.claro.soa.services.customer.getcustomerbycustomerid.GetRequest request
					= new _do.com.claro.soa.services.customer.getcustomerbycustomerid.GetRequest();
			request.setCustomerID(new CustomerID_Ext(customerID));

			_do.com.claro.soa.services.customer.getcustomerbycustomerid.GetResponse getResponse
					= soap.get(request);
			customer = getResponse.getCustomer();
			Utils.logFinest(CRM.class, "CRM GetCustomerByCustomerID: \n");
			Utils.logFinest(CRM.class, JSON.toString(customer) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(CRM.class, "CRM GetCustomersAndContactsByContactIdentityCardID", ex);
		}
		return customer;
	}

	static public List<Customer> CustomerListFromCustomerContacts(Customer customer) {
		List<Customer> customers = new ArrayList<>();
		Contacts objContacts = customer.getContacts();
		if (objContacts != null && !objContacts.getContact().isEmpty()) {
			Customer temp;
			List<Contact> contacts = objContacts.getContact();
			for (int i = 0; i < contacts.size(); i++) {
				Contact tempContact = contacts.get(i);
				temp = new Customer();
				temp.setAccountExecutives(customer.getAccountExecutives());
				temp.setAccountOfficer(customer.getAccountOfficer());
				temp.setAdditionalContacts(customer.getAdditionalContacts());
				temp.setComercialActivity(customer.getComercialActivity());
				temp.setComercialSector(customer.getComercialSector());
				Contacts newContacts = new Contacts() {
					{
						contact = Arrays.asList(tempContact);
					}
				};
				temp.setContacts(newContacts);
				temp.setCustomerID(customer.getCustomerID());
				temp.setCustomerSegment(customer.getCustomerSegment());
				temp.setCustomerType(customer.getCustomerType());
				temp.setEmail(customer.getEmail());
				temp.setFax(customer.getFax());
				temp.setLegalName(customer.getLegalName());
				temp.setName(customer.getName());
				temp.setPhone(customer.getPhone());
				temp.setProducts(customer.getProducts());
				temp.setSicCode(customer.getSicCode());
				temp.setTaxID(customer.getTaxID());
				temp.setWebsite(customer.getWebsite());
				customers.add(temp);
			}
		} else {
			customers.add(customer);
		}

		return customers;
	}

	static public List<Customer_Ext> GetCustomerListByCustomerID(String customerID) {
		long start = DATE.nowAsLong();
		List<Customer> customers = new ArrayList<>();
		try {
			Customer customer = GetCustomerByCustomerID(customerID);
			if (customer != null) {
				customers.addAll(CustomerListFromCustomerContacts(customer));
			}
		} catch (Exception ex) {
			Utils.logException(CRM.class, "CRM GetCustomerListByCustomerID", ex);
		}

		List<Customer_Ext> customerList = customers.stream()
				.map(i -> new Customer_Ext(i))
				.collect(Collectors.toList());
		Optional<Customer_Ext> selectedRole = customerList.stream()
				.filter(i -> i.getCustomerID().equals(customerID))
				.findFirst();
		if (selectedRole.isPresent()) {
			customerList.removeIf(i -> i.getCustomerID().equals(customerID));
			customerList.add(0, selectedRole.get());
		}
		long end = DATE.nowAsLong();
		Utils.logTrace(CRM.class, "CustomerByID Total: " + (end - start));
		return customerList;
	}

	static public List<Customer> GetCustomerByIdentityCardID(String identityCardID, int pageNum) {
		List<Customer> customers = new ArrayList<>();
		try {
			GetCustomersAndContactsByContactIdentityCardID soap
					= GetCustomersAndContactsByContactIdentityCardID_Service.getInstance();
			_do.com.claro.soa.services.customer.getcustomersandcontactsbycontactidentitycardid.GetRequest request
					= new _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactidentitycardid.GetRequest();
			request.setPageNum(pageNum);
			request.setIdentityCardID(new IdentityCardID_Ext(identityCardID));

			_do.com.claro.soa.services.customer.getcustomersandcontactsbycontactidentitycardid.GetResponse getResponse
					= soap.get(request);
			Customers objCustomers = getResponse.getCustomers();
			if (objCustomers != null) {
				List<Customer> tempCustomers = objCustomers.getCustomer();
				for (int i = 0; i < tempCustomers.size(); i++) {
					Customer customer = tempCustomers.get(i);
					customers.addAll(CustomerListFromCustomerContacts(customer));
				}
			}
			Utils.logFinest(CRM.class, "CRM GetCustomersAndContactsByContactIdentityCardID: \n");
			Utils.logFinest(CRM.class, JSON.toString(customers) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(CRM.class, "CRM GetCustomersAndContactsByContactIdentityCardID", ex);
		}
		return customers;
	}

	static public List<Customer_Ext> GetPageCustomerByIdentityCardID(String identityCardID, int pageNum) {
		return GetCustomerByIdentityCardID(identityCardID, pageNum)
				.stream()
				.map(i -> new Customer_Ext(i))
				.collect(Collectors.toList());
	}

	static public List<Customer_Ext> GetAllCustomerByIdentityCardID(String identityCardID) {
		List<Customer_Ext> customers = new ArrayList<>();
		List<Customer_Ext> temp;
		int pageNum = 1;
		do {
			temp = GetPageCustomerByIdentityCardID(identityCardID, pageNum);
			customers.addAll(temp);
			pageNum++;
		} while (!temp.isEmpty());

		return customers;
	}

	static public List<Customer> GetCustomerByPassportID(String passportID, int pageNum) {
		List<Customer> customers = new ArrayList<>();
		try {
			GetCustomersAndContactsByContactPassportID soap = GetCustomersAndContactsByContactPassportID_Service.getInstance();
			_do.com.claro.soa.services.customer.getcustomersandcontactsbycontactpassportid.GetRequest request
					= new _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactpassportid.GetRequest();
			request.setPageNum(pageNum);
			Passport passport = new Passport();
			passport.setPassportID(passportID);
			request.setPassport(passport);
			_do.com.claro.soa.services.customer.getcustomersandcontactsbycontactpassportid.GetResponse getResponse
					= soap.get(request);

			Customers objCustomers = getResponse.getCustomers();
			if (objCustomers != null) {
				List<Customer> tempCustomers = objCustomers.getCustomer();
				for (int i = 0; i < tempCustomers.size(); i++) {
					Customer customer = tempCustomers.get(i);
					customers.addAll(CustomerListFromCustomerContacts(customer));
				}
			}
			Utils.logFinest(CRM.class, "CRM GetCustomersAndContactsByContactPassportID: \n");
			Utils.logFinest(CRM.class, JSON.toString(customers) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(CRM.class, "CRM GetCustomersAndContactsByContactPassportID", ex);
		}
		return customers;
	}

	static public List<Customer_Ext> GetPageCustomerByPassportID(String passportID, int pageNum) {
		return GetCustomerByPassportID(passportID, pageNum)
				.stream()
				.map(i -> new Customer_Ext(i))
				.collect(Collectors.toList());
	}

	static public List<Customer_Ext> GetAllCustomerByPassportID(String passportID) {
		List<Customer_Ext> customers = new ArrayList<>();
		List<Customer_Ext> temp;
		int pageNum = 1;
		do {
			temp = GetPageCustomerByPassportID(passportID, pageNum);
			customers.addAll(temp);
			pageNum++;
		} while (!temp.isEmpty());

		return customers;
	}

	static public List<Customer_Ext> GetBusinessCustomersAndContactsByCustomerName(String searchValue, int pageNum) {
		List<Customer> customers = new ArrayList<>();
		try {
			GetBusinessCustomersAndContactsByCustomerName soap = GetBusinessCustomersAndContactsByCustomerName_Service.getInstance();
			_do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetRequest request
					= new _do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetRequest();
			request.setPageNum(pageNum);
			request.setName(searchValue);
			_do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetResponse getResponse
					= soap.get(request);

			Customers objCustomers = getResponse.getCustomers();
			if (objCustomers != null) {
				List<Customer> tempCustomers = objCustomers.getCustomer();
				for (int i = 0; i < tempCustomers.size(); i++) {
					Customer customer = tempCustomers.get(i);
					customers.addAll(CustomerListFromCustomerContacts(customer));
				}
			}
			Utils.logFinest(CRM.class, "CRM GetBusinessCustomersAndContactsByCustomerName: \n");
			Utils.logFinest(CRM.class, JSON.toString(customers) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(CRM.class, "CRM GetBusinessCustomersAndContactsByCustomerName", ex);
		}
		return customers.stream()
				.map(i -> new Customer_Ext(i))
				.collect(Collectors.toList());
	}

	static public List<Customer> GetCustomerByTaxID(String taxID, int pageNum) {
		List<Customer> customers = new ArrayList<>();
		try {
			GetCustomersAndContactsByTaxID soap = GetCustomersAndContactsByTaxID_Service.getInstance();
			_do.com.claro.soa.services.customer.getcustomersandcontactsbytaxid.GetRequest request
					= new _do.com.claro.soa.services.customer.getcustomersandcontactsbytaxid.GetRequest();

			request.setPageNum(pageNum);
			request.setTaxID(taxID);
			_do.com.claro.soa.services.customer.getcustomersandcontactsbytaxid.GetResponse getResponse
					= soap.get(request);

			Customers objCustomers = getResponse.getCustomers();
			if (objCustomers != null) {
				List<Customer> tempCustomers = objCustomers.getCustomer();
				for (int i = 0; i < tempCustomers.size(); i++) {
					Customer customer = tempCustomers.get(i);
					customers.addAll(CustomerListFromCustomerContacts(customer));
				}
			}
			Utils.logFinest(CRM.class, "CRM GetCustomersAndContactsByTaxID customerList: \n");
			Utils.logFinest(CRM.class, JSON.toString(customers) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(CRM.class, "CRM GetCustomersAndContactsByTaxID", ex);
		}
		return customers;
	}

	static public List<Customer_Ext> GetPageCustomerByTaxID(String taxID, int pageNum) {
		return GetCustomerByTaxID(taxID, pageNum)
				.stream()
				.map(i -> new Customer_Ext(i))
				.collect(Collectors.toList());
	}

	static public List<Customer_Ext> GetAllCustomerByTaxID(String taxID) {
		List<Customer_Ext> customers = new ArrayList<>();
		List<Customer_Ext> temp;
		int pageNum = 1;
		do {
			temp = GetPageCustomerByTaxID(taxID, pageNum);
			customers.addAll(temp);
			pageNum++;
		} while (!temp.isEmpty());

		return customers;
	}

	static public List<Customer> GetCustomersByCustomerName(String customerName, int pageNum) {
		List<Customer> customers = new ArrayList<>();
		try {
			GetBusinessCustomersAndContactsByCustomerName soap = GetBusinessCustomersAndContactsByCustomerName_Service.getInstance();
			_do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetRequest request
					= new _do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetRequest();
			request.setPageNum(pageNum);
			request.setName(customerName);

			_do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetResponse getResponse
					= soap.get(request);

			Customers objCustomers = getResponse.getCustomers();
			fillListFromCustomerResponse(objCustomers, customers);

			Utils.logFinest(CRM.class, "CRM GetCustomersByCustomerName customerList: \n");
			Utils.logFinest(CRM.class, JSON.toString(customers) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(CRM.class, "CRM GetCustomersByCustomerName", ex);
		}
		return customers;
	}

	static public List<Customer_Ext> GetPageCustomersByCustomerName(String taxID, int pageNum) {
		return GetCustomersByCustomerName(taxID, pageNum)
				.stream()
				.map(i -> new Customer_Ext(i))
				.collect(Collectors.toList());
	}

	static public List<Customer_Ext> GetAllCustomersByCustomerName(String customerName) {
		List<Customer_Ext> customers = new ArrayList<>();
		List<Customer_Ext> temp;
		int pageNum = 1;
		do {
			temp = GetPageCustomersByCustomerName(customerName, pageNum);
			customers.addAll(temp);
			pageNum++;
		} while (!temp.isEmpty());

		return customers;
	}

	static public List<Customer> GetCustomersByContactName(String contactName_Lastname, int pageNum) {
		List<Customer> customers = new ArrayList<>();
		try {
			GetCustomersAndContactsByContactNames soap = GetCustomersAndContactsByContactNames_Service.getInstance();
			_do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames.GetRequest request
					= new _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames.GetRequest();

			request.setPageNum(pageNum);
			List<String> parts = Utils.splitAsList(contactName_Lastname, "_");
			request.setName(parts.get(0));
			if (parts.size() > 1) {
				request.setLastname(parts.get(1));
			}

			_do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames.GetResponse getResponse
					= soap.get(request);

			Customers objCustomers = getResponse.getCustomers();
			fillListFromCustomerResponse(objCustomers, customers);

			Utils.logFinest(CRM.class, "CRM GetCustomersByContactName customerList: \n");
			Utils.logFinest(CRM.class, JSON.toString(customers) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(CRM.class, "CRM GetCustomersByContactName", ex);
		}
		return customers;
	}

	static public List<Customer_Ext> GetPageCustomersByContactName(String contactName_Lastname, int pageNum) {
		return GetCustomersByContactName(contactName_Lastname, pageNum)
				.stream()
				.map(i -> new Customer_Ext(i))
				.collect(Collectors.toList());
	}

	static public List<Customer_Ext> GetAllCustomersByContactName(String contactName_Lastname) {
		List<Customer_Ext> customers = new ArrayList<>();
		List<Customer_Ext> temp;
		int pageNum = 1;
		do {
			temp = GetPageCustomersByCustomerName(contactName_Lastname, pageNum);
			customers.addAll(temp);
			pageNum++;
		} while (!temp.isEmpty());

		return customers;
	}

	static public void fillListFromCustomerResponse(Customers objCustomers, List<Customer> customers) {
		if (objCustomers != null) {
			List<Customer> tempCustomers = objCustomers.getCustomer();
			for (int i = 0; i < tempCustomers.size(); i++) {
				Customer customer = tempCustomers.get(i);
				customers.addAll(CustomerListFromCustomerContacts(customer));
			}
		}
	}

	static public List<Case_Ext> GetCustomerCasesByCustomerID(String customerID, int pageNum) throws Exception {
		GetCustomerCasesByCustomerID soap = GetCustomerCasesByCustomerID_Service.getInstance();
		GetRequest request = new GetRequest();
		request.setPageNum(pageNum);
		request.setCustomerID(new CustomerID_Ext(customerID));
		GetResponse getResponse = soap.get(request);

		List<Case> cases = new ArrayList<>();
		Cases objCases = getResponse.getCases();
		if (objCases != null) {
			cases = objCases.getCase();
		}
		return cases.stream()
				.map(c -> new Case_Ext(c))
				.collect(Collectors.toList());
	}

	static public List<Note_Ext> GetCustomerCasesNotesByCustomerID(String customerID, int pageNum) throws Exception {
		List<Note_Ext> notes = new ArrayList<>();
		List<Case_Ext> casesList = GetCustomerCasesByCustomerID(customerID, pageNum);
		casesList.forEach((caseItem) -> {
			caseItem.getNotes().forEach(note -> notes.add(note));
		});
		return notes;
	}

	static public List<Interaction_Ext> GetInteractionsByCustomerID(String customerID, int pageNum) throws Exception {
		GetCustomerInteractionsByCustomerID soap = GetCustomerInteractionsByCustomerID_Service.getInstance();
		_do.com.claro.soa.services.customer.getcustomerinteractionsbycustomerid.GetRequest request
				= new _do.com.claro.soa.services.customer.getcustomerinteractionsbycustomerid.GetRequest();
		request.setPageNumber(pageNum);
		request.setCustomerId(new CustomerID_Ext(customerID));
		soap.get(request);
		_do.com.claro.soa.services.customer.getcustomerinteractionsbycustomerid.GetResponse getResponse
				= soap.get(request);

		List<Interaction> interactions = new ArrayList<>();
		Interactions objInteractions = getResponse.getInteractions();
		if (objInteractions != null) {
			interactions = objInteractions.getInteraction();
		}

		return interactions.stream()
				.map(s -> new Interaction_Ext(s))
				.collect(Collectors.toList());
	}

	static public CreateCaseResponse CreateCase(CreateCaseRequest request) throws Exception {
		CrmSoaPort soap = ESBCRMRSService.getInstance();
		CreateCaseResponse response = soap.createCase(request);
		return response;
	}

	static public CreateCaseResponse CreateCase(
			String subscriber,
			String title, String clientSystem,
			String contactPhone, String productType,
			String type1, String type2, String type3,
			String queueName, String visitPeriod,
			String notes, boolean autoDispatch
	) throws Exception {
		CreateCaseRequest request = new CreateCaseRequest();
		request.setSubscriber(subscriber);
		request.setCaseTitle(title);
		request.setClientSystem(clientSystem);
		request.setCaseTypeLevel1(type1);
		request.setCaseTypeLevel2(type2);
		request.setCaseTypeLevel3(type3);
		request.setContactPhone(contactPhone);
		request.setProductType(productType);
		request.setQueueName(queueName);
		request.setVisitPeriod(visitPeriod);
		request.setNotes(notes);
		request.setAutoDispatch(autoDispatch);

		CreateCaseResponse response = CreateCase(request);
		return response;
	}

	static public AddCaseNotesResponse AddCaseNotes(String caseID, String clientSystem, String notes) throws Exception {
		CrmSoaPort soap = ESBCRMRSService.getInstance();
		AddCaseNotesRequest request = new AddCaseNotesRequest();
		request.setCaseId(caseID);
		request.setClientSystem(clientSystem);
		request.setNotes(notes);
		AddCaseNotesResponse response = soap.addCaseNotes(request);
		return response;
	}

	static public UpdateCaseResponse UpdateCase(
			String caseID, String techID, String clientSystem,
			String phoneStation, String status,
			String visitPeriod, String visitDate) throws Exception {
		CrmSoaPort soap = ESBCRMRSService.getInstance();
		UpdateCaseRequest request = new UpdateCaseRequest();
		request.setCaseId(caseID);
		request.setTechId(techID);
		request.setClientSystem(clientSystem);
		request.setPhoneStation(phoneStation);
		request.setStatus(status);
		request.setVisitPeriod(visitPeriod);
		request.setVisitDate(visitDate);
		UpdateCaseResponse response = soap.updateCase(request);
		return response;
	}

	static public UpdateParentChildResponse UpdateParentChild(String clientSystem, String parentCaseId) throws Exception {
		CrmSoaPort soap = ESBCRMRSService.getInstance();
		UpdateParentChildRequest request = new UpdateParentChildRequest();
		request.setClientSystem(clientSystem);
		request.setParentCaseId(parentCaseId);
		UpdateParentChildResponse response = soap.updateParentChild(request);
		return response;
	}

	static public DispatchCaseResponse DispatchCase(String clientSystem, String queue, String notes) throws Exception {
		CrmSoaPort soap = ESBCRMRSService.getInstance();
		DispatchCaseRequest request = new DispatchCaseRequest();
		request.setClientSystem(clientSystem);
		request.setQueue(queue);
		request.setNotes(notes);

		DispatchCaseResponse response = soap.dispatchCase(request);
		return response;
	}

	static public CloseCaseResponse CloseCase(
			String caseId, String technicianId,
			String clientSystem, String sysReq,
			String causeCode, String dispositionCode,
			String subCauseCode, String subDispositionCode,
			String adjustmentType, double adjustmentAmount,
			boolean maintenanceContract, String resolution,
			String status, String notes) throws Exception {
		CrmSoaPort soap = ESBCRMRSService.getInstance();
		CloseCaseRequest request = new CloseCaseRequest();
		request.setCaseId(caseId);
		request.setTechnicianId(technicianId);
		request.setClientSystem(clientSystem);
		request.setSysReq(sysReq);
		request.setMaintenanceContract(maintenanceContract);
		request.setResolution(resolution);
		request.setStatus(status);
		request.setCauseCode(causeCode);
		request.setDispositionCode(dispositionCode);
		request.setSubCauseCode(subCauseCode);
		request.setSubDispositionCode(subDispositionCode);
		request.setAdjustmentType(adjustmentType);
		request.setAdjustmentAmount(adjustmentAmount);
		request.setNotes(notes);
		CloseCaseResponse response = soap.closeCase(request);
		return response;
	}

	static public CreateInteractionResponse CreateInteraction(CreateInteractionRequest request) throws Exception {
		CrmSoaPort soap = ESBCRMRSService.getInstance();
		CreateInteractionResponse response = soap.createInteraction(request);
		return response;
	}

	static public CreateInteractionResponse CreateInteraction(
			String objID, String callerID, String personID,
			String title, String channel, String clientSystem,
			String medium, int direction, String notes) throws Exception {
		CrmSoaPort soap = ESBCRMRSService.getInstance();
		CreateInteractionRequest request = new CreateInteractionRequest();
		request.setObjId(objID);
		request.setCallerId(callerID);
		request.setPersonId(personID);
		request.setTitle(title);
		request.setChannel(channel);
		request.setClientSystem(clientSystem);
		request.setMedium(medium);
		request.setDirection(direction);
		request.setNotes(notes);

		CreateInteractionResponse response = soap.createInteraction(request);
		return response;
	}

}
