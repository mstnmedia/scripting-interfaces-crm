/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.Mixed;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Customer_Ext;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Interfaz estática que se consulta desde la pantalla de crear nueva
 * transacción para consultar las subscripciones del cliente seleccionado. Esta
 * interfaz se consulta los servicios paginados de Prepago y Pospago y obtiene
 * los detalles consultando Fijo.
 *
 * @author amatos
 */
public class CRM_CustomerSubscriptions extends InterfaceConnectorBase {

	private final static int ID = 2050;
	private final static String NAME = "crm_customer_subscriptions";
	private final static String LABEL = "CRM - Servicios del Cliente";
	private final static String SERVER_URL = "crm.org";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	private final static String DOCUMENT = "document";

	private List<Subscription_Ext> subscriptionList = new ArrayList();

	public CRM_CustomerSubscriptions() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		List<Interface_Field> list = new ArrayList();
		list.addAll(getCustomerFields());
		list.add(new Interface_Field(4, ID, PAGE_NUMBER, "Número de página", InterfaceFieldTypes.NUMBER, 4, "1"));
		list.add(new Interface_Field(5, ID, DOCUMENT, "Documento del empleado", InterfaceFieldTypes.TEXT, 5, null));
		return list;
	}

	@Override
	public List<Transaction_Result> getResults() throws Exception {
		String prefix = NAME + "_";
		String strSubscriptionList = JSON.toString(subscriptionList);
		return Arrays.asList(
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "subscriptionList", "Lista de servicios", strSubscriptionList, "{\"notSave\":true}"),
				new Transaction_Result(2, 0, 0, 0, ID, prefix + "pageSubscriptions", "Página de servicios", Integer.toString(pageNum), "{\"notSave\": true}")
		);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		ObjectNode json = JSON.newObjectNode();
		response.setJson(json);
		try {
			Interface inter = getInterface();
			response.setInterface(inter);
			ObjectNode form = JSON.toNode(payload.getForm());

			String customerID = getCustomerIDFromForm(form);
			boolean validID = CustomerID_Ext.isValid(customerID);
			String document = JSON.getString(form, DOCUMENT);

			if (!validID || Utils.isNullOrWhiteSpaces(document)) {
				if (!validID) {
					form.remove(CUSTOMER_ID);
				}
				if (Utils.isNullOrWhiteSpaces(document)) {
					form.remove(DOCUMENT);
				}
				setAsMissingRequest(response, json, form);
			} else {
				pageNum = getPageNumFromForm(form);
				json.put(PAGE_NUMBER, pageNum)
						.put(CUSTOMER_ID, customerID);
				Customer_Ext customer = new Customer_Ext(null);
				customer.setCustomerID(customerID);
				customer.setDocument(document);
				subscriptionList = Mixed.GetPageSubscriptionsByCustomerID(customer, pageNum);

				List<Transaction_Result> results = getResults();
				inter.setInterface_results(results);
				response.setValue(InterfaceOptions.SUCCESS);
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
			}
		} catch (Exception ex) {
			setAsExceptionResponse(response, ex);
		}
		return response;
	}

}
