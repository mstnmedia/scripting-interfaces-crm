/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.services.customer.createcustomerinteraction.CreateCustomerInteraction;
import _do.com.claro.soa.services.customer.createcustomerinteraction.CreateCustomerInteraction_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 * Interfaz para crear interacciones de CRM.
 *
 * @author amatos
 */
public class CRM_CreateCustomerInteraction extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = CRM_CreateCustomerInteraction.class;
		try {
			if ("true".equals(InterfaceConfigs.get("ignore_crm_createcustomerinteraction"))) {
				Utils.logWarn(_class, "Ignorando interfaces en clase " + _class.getCanonicalName());
				SOAP = new Object();
			} else {
				SOAP = CreateCustomerInteraction_Service.getInstance();
			}
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint CreateCustomerInteraction", ex);
			if ("true".equals(InterfaceConfigs.get("ignoreExceptions_crm_createcustomerinteraction"))) {
				Utils.logWarn(_class, "Error ignorado por configuración. Continuará el proceso.");
				SOAP = new Object();
			} else {
				throw ex;
			}
		}
	}

	public CRM_CreateCustomerInteraction() {
		super(0, "crmcreatecustomerinteraction", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("crm_createcustomerinteraction"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("_id")) {
			field.setLabel("ID");
			field.setRequired(false);
		} else if (fieldName.endsWith("groupId")) {
			field.setLabel("ID de Grupo");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("entityId")) {
			field.setLabel("CCU");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("contactID")) {
			field.setLabel("ID de Contacto");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("npa")) {
			field.setLabel("No. de Teléfono");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
			field.setRequired(false);
		} else if (fieldName.endsWith("nxx")) {
			field.setLabel("NXX");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setRequired(false);
		} else if (fieldName.endsWith("stationCode")) {
			field.setLabel("Código de Estación");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setRequired(false);
		} else if (fieldName.endsWith("title")) {
			field.setLabel("Título");
//			field.setId_type(InterfaceFieldTypes.NUMBER);
//			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
//			field.setRequired(false);
		} else if (fieldName.endsWith("creationDate")) {
			field.setLabel("Fecha de creación");
			field.setId_type(InterfaceFieldTypes.DATETIME);
			field.setRequired(false);
		} else if (fieldName.endsWith("interactionDirection")) {
			field.setLabel("Tipo de Llamada");
		} else if (fieldName.endsWith("channel")) {
			field.setLabel("Canal");
		} else if (fieldName.endsWith("medium")) {
			field.setLabel("Medio de Comunicación");
		} else if (fieldName.endsWith("interactionType")) {
			field.setLabel("Tipo");
		} else if (fieldName.endsWith("notes")) {
			field.setLabel("Notas");
		} else if (fieldName.endsWith("topic")) {
			field.setLabel("Tópicos");
		} else if (fieldName.endsWith("classification") && parentName.endsWith("topic")) {
			field.setLabel("Razón 1");
		} else if (fieldName.endsWith("subject") && parentName.endsWith("topic")) {
			field.setLabel("Razón 2");
		} else if (fieldName.endsWith("outcome") && parentName.endsWith("topic")) {
			field.setLabel("Razón 3");
		} else if (fieldName.endsWith("employeeId")) {
			field.setLabel("ID de empleado");
		} else if (fieldName.endsWith("firstName")) {
			field.setLabel("Nombre");
			field.setRequired(false);
		} else if (fieldName.endsWith("secondName")) {
			field.setLabel("Apellido");
			field.setRequired(false);
		} else if (fieldName.endsWith("firstSurname")) {
			field.setLabel("Segundo nombre");
			field.setRequired(false);
		} else if (fieldName.endsWith("secondSurname")) {
			field.setLabel("Segundo apellido");
			field.setRequired(false);
		} else if (fieldName.endsWith("phoneString")) {
			field.setLabel("Phone String");
			field.setRequired(false);
		} else if (fieldName.endsWith("faxString")) {
			field.setLabel("Fax String");
			field.setRequired(false);
		} else if (fieldName.endsWith("email")) {
			field.setLabel("Correo");
			field.setId_type(InterfaceFieldTypes.EMAIL);
			field.setRequired(false);
		} else if (fieldName.endsWith("source")) {
			field.setLabel("Fuente");
			field.setRequired(false);
		} else if (fieldName.endsWith("ticket")) {
			field.setLabel("Token");
		}
		return field;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("crm_createcustomerinteraction").toString();
		super.test();
	}

}
