/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.model.customer.Customer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.Mixed;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Customer_Ext;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import java.util.List;

/**
 * Interfaz que contiene las variables con la información del cliente
 * seleccionado al inicio de la transacción en curso.
 *
 * @author amatos
 * @deprecated
 */
public class CRM_CustomerInfo extends InterfaceConnectorBase {

	private final static int ID = Interfaces.CUSTOMER_INFO_INTERFACE_ID;
	private final static String NAME = Interfaces.CUSTOMER_INFO_INTERFACE_NAME;
	private final static String LABEL = "CRM - Información del Cliente";
	private final static String SERVER_URL = "crm.org";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	private Customer_Ext customer;
	private List<Subscription_Ext> subscriptionList;

	public CRM_CustomerInfo() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		return getCustomerFields();
	}

	@Override
	public List<Transaction_Result> getResults() throws Exception {
		return Customer_Ext.getCustomerResults(customer);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		ObjectNode json = JSON.newObjectNode();
		response.setJson(json);
		try {
			Interface inter = getInterface();
			response.setInterface(inter);
			ObjectNode form = JSON.toNode(payload.getForm());

			String customerID = getCustomerIDFromForm(form);
			boolean validID = CustomerID_Ext.isValid(customerID);

			if (!validID) {
				form.remove(CUSTOMER_ID);
				setAsMissingRequest(response, json, form);
			} else {
				json.put(CUSTOMER_ID, customerID);

				Customer found = CRM.GetCustomerByCustomerID(customerID);
				if (found != null) {
					customer = new Customer_Ext(found);
					subscriptionList = Mixed.GetPageSubscriptionsByCustomerID(customer, 1);
				}

				List<Transaction_Result> results = getResults();
				inter.setInterface_results(results);
				response.setValue(InterfaceOptions.SUCCESS);
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
			}
		} catch (Exception ex) {
			setAsExceptionResponse(response, ex);
		}
		return response;
	}

}
