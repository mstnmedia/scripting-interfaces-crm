/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactidentitycardid.GetCustomersAndContactsByContactIdentityCardID;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactidentitycardid.GetCustomersAndContactsByContactIdentityCardID_Service;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.Utils;

/**
 *
 * @author amatos
 */
public class CRM_GetCustomersAndContactsByContactIdentityCardID extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = CRM_GetCustomersAndContactsByContactIdentityCardID.class;
		try {
			SOAP = GetCustomersAndContactsByContactIdentityCardID_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public CRM_GetCustomersAndContactsByContactIdentityCardID() {
		super(0, "crmgetcustomersandcontactsbycontactidentitycardid", SOAP);
	}

}
