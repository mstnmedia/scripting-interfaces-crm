/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.crm.ESBCRMRSService;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Log;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

/**
 * Interfaz dinámica que implementa el web service CRM_ESB.
 *
 * @author amatos
 */
public class CRM_ESB extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = CRM_ESB.class;
		try {
			if ("true".equals(InterfaceConfigs.get("ignore_crmesb"))) {
				Utils.logWarn(_class, "Ignorando interfaces en clase " + _class.getCanonicalName());
				SOAP = new Object();
			} else {
				SOAP = ESBCRMRSService.getInstance();
			}
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint CRM_ESB", ex);
			if ("true".equals(InterfaceConfigs.get("ignoreExceptions_crmesb"))) {
				Utils.logWarn(_class, "Error ignorado por configuración. Continuará el proceso.");
				SOAP = new Object();
			} else {
				throw ex;
			}
		}
	}

	public CRM_ESB() {
		super(0, "crmesb", SOAP);
		showBaseLogs = false;
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("crm_esb"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("caseId")) {
			field.setLabel("No. de Caso");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("ClientSystem")) {
			field.setLabel("ClientSystem");
		} else if (fieldName.endsWith("ccu")) {
			field.setLabel("CCU");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if ("crmesb_closeCase".equals(inter.getName())) {
			if (fieldName.endsWith("technicianId")) {
				field.setLabel("Tarjeta");
				field.setRequired(false);
			} else if (fieldName.endsWith("dispositionCode")) {
				field.setLabel("Código de Disposición");
			} else if (fieldName.endsWith("subDispositionCode")) {
				field.setLabel("Sub-código de Disposición");
			} else if (fieldName.endsWith("causeCode")) {
				field.setLabel("Código de Causa");
			} else if (fieldName.endsWith("subCauseCode")) {
				field.setLabel("Código de Sub-causa");
			} else if (fieldName.endsWith("adjustmentAmount")) {
				field.setLabel("Monto de Ajuste");
				field.setId_type(InterfaceFieldTypes.NUMBER);
				field.setRequired(false);
			} else if (fieldName.endsWith("adjustmentType")) {
				field.setLabel("Tipo de Ajuste");
				field.setRequired(false);
			} else if (fieldName.endsWith("maintenanceContract")) {
				field.setLabel("Contrato de Mantenimiento");
				field.setId_type(InterfaceFieldTypes.BOOLEAN);
				field.setRequired(false);
			} else if (fieldName.endsWith("notes")) {
				field.setLabel("Notas");
				field.setRequired(false);
			} else if (fieldName.endsWith("resolution")) {
				field.setLabel("Resolución");
				field.setRequired(false);
			} else if (fieldName.endsWith("status")) {
				field.setLabel("Estatus");
				field.setRequired(false);
			} else if (fieldName.endsWith("sysReq")) {
				field.setLabel("SysReq");
				field.setRequired(false);
			}
		} else if ("crmesb_updateCase".equals(inter.getName())) {
			if (fieldName.endsWith("techId")) {
				field.setLabel("Tarjeta");
				field.setRequired(false);
			} else if (fieldName.endsWith("attrs")) {
				field.setLabel("Atributos");
				field.setRequired(false);
			} else if (fieldName.endsWith(LIST_TEMPLATE) && parentName.endsWith("attrs")) {
				field.setLabel("Nombre");
			} else if (fieldName.endsWith("values")) {
				field.setLabel("Valores");
				field.setRequired(false);
			} else if (fieldName.endsWith(LIST_TEMPLATE) && parentName.endsWith("values")) {
				field.setLabel("Valor");
			} else if (fieldName.endsWith("status")) {
				field.setLabel("Estatus");
			} else if (fieldName.endsWith("phoneStation")) {
				field.setLabel("Estacion Telefonica");
				field.setId_type(InterfaceFieldTypes.NUMBER);
				field.setRequired(false);
			} else if (fieldName.endsWith("visitDate")) {
				field.setLabel("Fecha de Visita");
				field.setId_type(InterfaceFieldTypes.DATE);
				field.setRequired(false);
			} else if (fieldName.endsWith("visitPeriod")) {
				field.setLabel("Período de Visita");
				field.setRequired(false);
			}
		} else if ("crmesb_dispatchCase".equals(inter.getName())) {
			if (fieldName.endsWith("cases")) {
				field.setLabel("Casos");
			} else if (fieldName.endsWith(LIST_TEMPLATE) && parentName.endsWith("cases")) {
				field.setLabel("Casos");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			} else if (fieldName.endsWith("notes")) {
				field.setLabel("Notas");
				field.setRequired(false);
			} else if (fieldName.endsWith("queue")) {
				field.setLabel("Cola");
			}
		} else if ("crmesb_getCustomerCases".equals(inter.getName())) {
			if (fieldName.endsWith("idType")) {
				field.setLabel("Tipo de ID");
			} else if (fieldName.endsWith("idValue")) {
				field.setLabel("Valor de ID");
			}
		} else if ("crmesb_createCase".equals(inter.getName())) {
			if (fieldName.endsWith("subscriber")) {
				field.setLabel("No. de Teléfono");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			} else if (fieldName.endsWith("queueName")) {
				field.setLabel("Nombre de la Cola");
				field.setRequired(false);
			} else if (fieldName.endsWith("notes")) {
				field.setLabel("Notas");
				field.setRequired(false);
			} else if (fieldName.endsWith("productType")) {
				field.setLabel("Tipo de Producto");
			} else if (fieldName.endsWith("caseTitle")) {
				field.setLabel("Título del Caso");
			} else if (fieldName.endsWith("caseTypeLevel1")) {
				field.setLabel("Tipo 1");
			} else if (fieldName.endsWith("caseTypeLevel2")) {
				field.setLabel("Tipo 2");
			} else if (fieldName.endsWith("caseTypeLevel3")) {
				field.setLabel("Tipo 3");
			} else if (fieldName.endsWith("contactPhone")) {
				field.setLabel("Número de contacto");
			} else if (fieldName.endsWith("visitPeriod")) {
				field.setLabel("Período de visita");
				field.setRequired(false);
			} else if (fieldName.endsWith("Autodispatch")) {
				field.setLabel("Autodispatch");
				field.setId_type(InterfaceFieldTypes.BOOLEAN);
				field.setDefault_value("true");
			}
		} else if ("crmesb_addCaseNotes".equals(inter.getName())) {
			if (fieldName.endsWith("notes")) {
				field.setLabel("Notas");
			}
		} else if ("crmesb_createInteraction".equals(inter.getName())) {
			if (fieldName.endsWith("objId")) {
				field.setLabel("ObjID");
			} else if (fieldName.endsWith("orgId")) {
				field.setLabel("OrgID");
			} else if (fieldName.endsWith("title")) {
				field.setLabel("Titulo");
			} else if (fieldName.endsWith("direction")) {
				field.setLabel("ID de Dirección");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			} else if (fieldName.endsWith("notes")) {
				field.setLabel("Notas");
			} else if (fieldName.endsWith("topic")) {
				field.setLabel("Tipo");
			} else if (fieldName.endsWith(LIST_TEMPLATE) && parentName.endsWith("topic")) {
				field.setLabel("Tipos");
			} else if (fieldName.endsWith("medium")) {
				field.setLabel("Medio");
			} else if (fieldName.endsWith("channel")) {
				field.setLabel("Canal");
			} else if (fieldName.endsWith("personId")) {
				field.setLabel("ID del Contato");
			} else if (fieldName.endsWith("callerId")) {
				field.setLabel("ID de Llamada");
			}
		} else if ("crmesb_getCustomerProfileDetailsById".equals(inter.getName())) {
			if (fieldName.endsWith("idType")) {
				field.setLabel("Tipo de ID");
			} else if (fieldName.endsWith("idValue")) {
				field.setLabel("Valor de ID");
			} else if (fieldName.endsWith("productTypeFilter")) {
				field.setLabel("Filtro de Tipo de Producto");
				field.setRequired(false);
			} else if (fieldName.endsWith("listBANs")) {
				field.setLabel("Listar de BANs");
				field.setId_type(InterfaceFieldTypes.BOOLEAN);
				field.setRequired(false);
			}
		} else if ("crmesb_getCustomerOpenCases".equals(inter.getName())) {
			if (fieldName.endsWith("idType")) {
				field.setLabel("Tipo de ID");
			} else if (fieldName.endsWith("idValue")) {
				field.setLabel("Valor de ID");
			}
		} else if ("crmesb_updateParentChild".equals(inter.getName())) {
			if (fieldName.endsWith("parentCaseId")) {
				field.setLabel("Caso Padre");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			} else if (fieldName.endsWith("childrenCasesId")) {
				field.setLabel("Caso Hijo");
				field.setId_type(InterfaceFieldTypes.NUMBER);
			}
		} else if ("crmesb_getCustomerDetails".equals(inter.getName())) {
			if (fieldName.endsWith("idType")) {
				field.setLabel("Tipo de ID");
			} else if (fieldName.endsWith("idValue")) {
				field.setLabel("Valor de ID");
			} else if (fieldName.endsWith("productTypeFilter")) {
				field.setLabel("Filtro de Tipo de Producto");
				field.setRequired(false);
			} else if (fieldName.endsWith("listBANs")) {
				field.setLabel("Listar de BANs");
				field.setId_type(InterfaceFieldTypes.BOOLEAN);
				field.setRequired(false);
			}
		}
		return field;
	}

	@Override
	protected Object sendParamsToMethod(Interface inter, Method method, List<Object> params) throws Exception {
		Object responseObj = super.sendParamsToMethod(inter, method, params);
		if ("crmesb_createInteraction".equals(inter.getName())) {
			response.getLogs().add(new Log(
					0, user.getId(), "CRM Interacciones", transaction.getId(),
					"Crear interacción", new Date(),
					JSON.toString(params), JSON.toString(responseObj), user.getIp()));
		} else if ("crmesb_createCase".equals(inter.getName())) {
			response.getLogs().add(new Log(
					0, user.getId(), "CRM Casos", transaction.getId(),
					"Crear caso", new Date(),
					JSON.toString(params), JSON.toString(responseObj), user.getIp()));
		}
		return responseObj;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("crm_esb").toString();
		super.test();
	}
}
