/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetBusinessCustomersAndContactsByCustomerName;
import _do.com.claro.soa.services.customer.getbusinesscustomersandcontactsbycustomername.GetBusinessCustomersAndContactsByCustomerName_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.lang.reflect.Method;

/**
 * Interfaz que consulta clientes de negocios con sus contactos, búscándolos por
 * el nombre del negocio.
 *
 * @author amatos
 */
public class CRM_GetBusinessCustomersAndContactsByCustomerName extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = CRM_GetBusinessCustomersAndContactsByCustomerName.class;
		try {
			if ("true".equals(InterfaceConfigs.get("ignore_crm_getbusinesscustomersandcontactsbycustomername"))) {
				Utils.logWarn(_class, "Ignorando interfaces en clase " + _class.getCanonicalName());
				SOAP = new Object();
			} else {
				SOAP = GetBusinessCustomersAndContactsByCustomerName_Service.getInstance();
			}
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint GetBusinessCustomersAndContactsByCustomerName", ex);
			if ("true".equals(InterfaceConfigs.get("ignoreExceptions_crm_getbusinesscustomersandcontactsbycustomername"))) {
				Utils.logWarn(_class, "Error ignorado por configuración. Continuará el proceso.");
				SOAP = new Object();
			} else {
				throw ex;
			}
		}
	}

	public CRM_GetBusinessCustomersAndContactsByCustomerName() {
		super(0, "crmgetbusinesscustomersandcontactsbycustomername", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("crm_getbusinesscustomersandcontactsbycustomername"));
		return inter;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("crm_getbusinesscustomersandcontactsbycustomername").toString();
		super.test();
	}
}
