/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Interaction_Ext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Interfaz estática que se consulta desde la pantalla de crear nueva
 * transacción y la pantalla transacción en curso para consultar las
 * interacciones del cliente seleccionado. Esta interfaz se conecta a un web
 * service que los devuelve paginados y se ha comprobado que los devuelve
 * ordenados por fecha descendencientemente.
 *
 * @author amatos
 */
public class CRM_CustomerInteractions extends InterfaceConnectorBase {

	private final static int ID = 2200;
	private final static String NAME = "crm_customer_interactions";
	private final static String LABEL = "CRM - Interacciones del Cliente";
	private final static String SERVER_URL = "crm.org";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	private List<Interaction_Ext> customerInteractions = new ArrayList();

	public CRM_CustomerInteractions() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		List<Interface_Field> list = new ArrayList();
		list.addAll(getCustomerFields());
		list.add(new Interface_Field(4, ID, PAGE_NUMBER, "Número de página", InterfaceFieldTypes.NUMBER, 4, "1"));
		return list;
	}

	@Override
	public List<Transaction_Result> getResults() throws Exception {
		String prefix = NAME + "_";
		String strCustomerInteractions = JSON.toString(customerInteractions);
		return Arrays.asList(
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "customerInteractions", "Lista de interacciones", strCustomerInteractions, "{\"notSave\":true}"),
				new Transaction_Result(2, 0, 0, 0, ID, prefix + "pageInteractions", "Página de interacciones", Integer.toString(pageNum), "{\"notSave\": true}")
		);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		ObjectNode json = JSON.newObjectNode();
		response.setJson(json);
		try {
			Interface inter = getInterface();
			response.setInterface(inter);
			ObjectNode form = JSON.toNode(payload.getForm());

			String customerID = getCustomerIDFromForm(form);
			boolean validID = CustomerID_Ext.isValid(customerID);

			if (!validID) {
				form.remove(CUSTOMER_ID);
				setAsMissingRequest(response, json, form);
			} else {
				pageNum = getPageNumFromForm(form);
				json.put(PAGE_NUMBER, pageNum)
						.put(CUSTOMER_ID, customerID);

				customerInteractions = CRM.GetInteractionsByCustomerID(customerID, pageNum);

				List<Transaction_Result> results = getResults();
				inter.setInterface_results(results);
				response.setValue(InterfaceOptions.SUCCESS);
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
			}
		} catch (Exception ex) {
			Utils.logException(this.getClass(), null, ex);
			setAsExceptionResponse(response, ex);
		}
		return response;
	}

}
