/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Note_Ext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Interfaz estática que se consulta desde la pantalla de crear nueva
 * transacción y la pantalla transacción en curso para consultar notas de los
 * casos del cliente seleccionado. Esta interfaz obtiene los casos igual que
 * {@link CRM_CustomerCases} y luego inserta todas las notas de los casos
 * obtenidos en una sola lista.
 *
 * @author amatos
 */
public class CRM_CustomerCasesNotes extends InterfaceConnectorBase {

	private final static int ID = 2110;
	private final static String NAME = "crm_customer_cases_notes";
	private final static String LABEL = "CRM - Notas de Casos";
	private final static String SERVER_URL = "crm.org";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	private List<Note_Ext> customerNotes = new ArrayList();

	public CRM_CustomerCasesNotes() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		return getCustomerFields();
	}

	@Override
	public List<Transaction_Result> getResults() throws Exception {
		String prefix = NAME + "_";
		String strCustomerNotes = JSON.toString(customerNotes);
		return Arrays.asList(
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "customerNotes", "Lista de Notas de Casos", strCustomerNotes, "{\"notSave\":true}"),
				new Transaction_Result(2, 0, 0, 0, ID, prefix + "pageNotes", "Página de notas", Integer.toString(pageNum), "{\"notSave\": true}")
		);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		ObjectNode json = JSON.newObjectNode();
		response.setJson(json);
		try {
			Interface inter = getInterface();
			response.setInterface(inter);
			ObjectNode form = JSON.toNode(payload.getForm());

			String customerID = getCustomerIDFromForm(form);
			boolean validID = CustomerID_Ext.isValid(customerID);

			if (!validID) {
				form.remove(CUSTOMER_ID);
				setAsMissingRequest(response, json, form);
			} else {
				pageNum = getPageNumFromForm(form);
				json.put(PAGE_NUMBER, pageNum)
						.put(CUSTOMER_ID, customerID);

				customerNotes = CRM.GetCustomerCasesNotesByCustomerID(customerID, pageNum);

				List<Transaction_Result> results = getResults();
				inter.setInterface_results(results);
				response.setValue(InterfaceOptions.SUCCESS);
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
			}
		} catch (Exception ex) {
			setAsExceptionResponse(response, ex);
		}
		return response;
	}

}
