/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.ext.Case_Ext;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Interfaz estática que se consulta desde la pantalla de crear nueva
 * transacción y la pantalla transacción en curso para consultar los casos del
 * cliente seleccionado. Esta interfaz se conecta a un web service que los
 * devuelve paginados y se ha comprobado que los devuelve ordenados por fecha
 * descendencientemente.
 *
 * @author amatos
 */
public class CRM_CustomerCases extends InterfaceConnectorBase {

	private final static int ID = 2100;
	private final static String NAME = "crm_customer_cases";
	private final static String LABEL = "CRM - Casos del Cliente";
	private final static String SERVER_URL = "crm.org";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	private List<Case_Ext> customerCases = new ArrayList();

	public CRM_CustomerCases() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		List<Interface_Field> list = new ArrayList();
		list.addAll(getCustomerFields());
		list.add(new Interface_Field(4, ID, PAGE_NUMBER, "Número de página", InterfaceFieldTypes.NUMBER, 4, "1"));
		return list;
	}

	@Override
	public List<Transaction_Result> getResults() throws Exception {
		String prefix = NAME + "_";
		String strCustomerCases = JSON.toString(customerCases);
		return Arrays.asList(
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "customerCases", "Lista de Casos", strCustomerCases, "{\"notSave\":true}"),
				new Transaction_Result(2, 0, 0, 0, ID, prefix + "pageCases", "Página de casos", Integer.toString(pageNum), "{\"notSave\": true}")
		);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		ObjectNode json = JSON.newObjectNode();
		response.setJson(json);
		try {
			Interface inter = getInterface();
			response.setInterface(inter);
			ObjectNode form = JSON.toNode(payload.getForm());

			String customerID = getCustomerIDFromForm(form);
			boolean validID = CustomerID_Ext.isValid(customerID);

			if (!validID) {
				form.remove(CUSTOMER_ID);
				setAsMissingRequest(response, json, form);
			} else {
				pageNum = getPageNumFromForm(form);
				json.put(PAGE_NUMBER, pageNum)
						.put(CUSTOMER_ID, customerID);

				customerCases = CRM.GetCustomerCasesByCustomerID(customerID, pageNum);

				List<Transaction_Result> results = getResults();
				inter.setInterface_results(results);
				response.setValue(InterfaceOptions.SUCCESS);
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
			}
		} catch (Exception ex) {
			setAsExceptionResponse(response, ex);
		}
		return response;
	}

}
