/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.crm.CreateCaseResponse;
import _do.com.claro.soa.crm.StatusTrans;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import java.util.Arrays;
import java.util.List;

/**
 * Interfaz de prueba que simula la creación de casos en CRM.
 *
 * @author amatos
 * @deprecated
 */
public class CRM_CreateCase extends InterfaceConnectorBase {

	private final static int ID = 2101;
	private final static String NAME = "crm_create_case";
	private final static String LABEL = "CRM - Crear caso";
	private final static String SERVER_URL = "http://SOADEVENV1:8010/soa-infra/services/crm/CRM_ESB!1.0/CRM.CRM_RS_ep";
	private final static String VALUE_STEP = "success";
	private final static boolean DELETED = false;

	private final String prefix = NAME + "_";

	private CreateCaseResponse caseResponse;

	public CRM_CreateCase() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
		showBaseLogs = true;
	}

	private final String SUBSCRIBER = prefix + "subscriber";
	private final String TITLE = prefix + "title";
	private final String CLIENT_SYSTEM = prefix + "client_system";
	private final String CONTACT_PHONE = prefix + "contact_phone";
	private final String PRODUCT_TYPE = prefix + "product_type";
	private final String TYPE1 = prefix + "type1";
	private final String TYPE2 = prefix + "type2";
	private final String TYPE3 = prefix + "type3";
	private final String QUEUE_NAME = prefix + "queue_name";
	private final String VISIT_PERIOD = prefix + "visit_period";
	private final String NOTES = prefix + "notes";
	private final String AUTO_DISPATCH = prefix + "auto_dispatch";

	@Override
	public List<Interface_Field> getFields() {
		return Arrays.asList(
				//(id, id_interface, name, label, id_type, required)
				new Interface_Field(0, ID, SUBSCRIBER, "Subscriptor", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(1, ID, TITLE, "Título", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(2, ID, CLIENT_SYSTEM, "Sistema de cliente", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(3, ID, CONTACT_PHONE, "Teléfono de contacto", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(4, ID, PRODUCT_TYPE, "Tipo de producto", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(5, ID, TYPE1, "Tipo 1", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(6, ID, TYPE2, "Tipo 2", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(7, ID, TYPE3, "Tipo 3", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(8, ID, QUEUE_NAME, "Nombre de cola", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(9, ID, VISIT_PERIOD, "Periodo de visita", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(10, ID, NOTES, "Notas", InterfaceFieldTypes.TEXT, true),
				new Interface_Field(11, ID, AUTO_DISPATCH, "Autodispatch", InterfaceFieldTypes.BOOLEAN, true)
		);
	}

	private final String CASE_ID = prefix + "CaseId";
	private final String FECHA_SOLUCION = prefix + "FechaSolucion";
	private final String HORA_SOLUCION = prefix + "HoraSolucion";
	private final String STATUS_TRANS = prefix + "StatusTrans";
	private final String EXCEPTION_TRANS = prefix + "ExceptionTrans";

	@Override
	public List<Transaction_Result> getResults() {
		boolean emptyFields = caseResponse == null;

		String CaseId = emptyFields ? "" : caseResponse.getCaseId();
		String StatusTrans = "";
		String ExceptionTrans = "";
		if (!emptyFields && !caseResponse.getStatusTrans().isEmpty()) {
			StatusTrans statusTrans = caseResponse.getStatusTrans().get(0);
			StatusTrans = statusTrans.getStatus();
			if (!statusTrans.getException().isNil()) {
				ExceptionTrans = statusTrans.getException().getValue();
			}
		}
		String FechaSolucion = emptyFields ? "" : caseResponse.getFechaSolucion();
		String HoraSolucion = emptyFields ? "" : caseResponse.getHoraSolucion();

		return Arrays.asList(
				//(id_interface, name, label)
				//(id, id_interface, name, label, value, props)
				new Transaction_Result(1, ID, CASE_ID, "ID del caso", CaseId, ""),
				new Transaction_Result(2, ID, FECHA_SOLUCION, "Fecha de solución", FechaSolucion, ""),
				new Transaction_Result(3, ID, HORA_SOLUCION, "Hora de la solución", HoraSolucion, ""),
				new Transaction_Result(4, ID, STATUS_TRANS, "Estado de la transacción", StatusTrans, ""),
				new Transaction_Result(5, ID, EXCEPTION_TRANS, "Error de la transacción", ExceptionTrans, "")
		);
	}

	@Override
	protected TransactionInterfacesResponse setAsNextStepResponse() throws Exception {
		String subscriber = JSON.getString(form, SUBSCRIBER);
		String title = JSON.getString(form, TITLE);
		String clientSystem = JSON.getString(form, CLIENT_SYSTEM);
		String contactPhone = JSON.getString(form, CONTACT_PHONE);
		String productType = JSON.getString(form, PRODUCT_TYPE);
		String type1 = JSON.getString(form, TYPE1);
		String type2 = JSON.getString(form, TYPE2);
		String type3 = JSON.getString(form, TYPE3);
		String queueName = JSON.getString(form, QUEUE_NAME);
		String visitPeriod = JSON.getString(form, VISIT_PERIOD);
		String notes = JSON.getString(form, NOTES);
		boolean autodispatch = Boolean.parseBoolean(JSON.getString(form, AUTO_DISPATCH));

		caseResponse = CRM.CreateCase(
				subscriber,
				title, clientSystem,
				contactPhone, productType,
				type1, type2, type3,
				queueName, visitPeriod,
				notes, autodispatch
		);

		results = getResults();
		response.setResults(results);

		//Calculate response value
		String value = getValueFromJavascript();
		response.setValue(value);

		return response;
	}
}
