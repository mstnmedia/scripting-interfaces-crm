/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames.GetCustomersAndContactsByContactNames;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames.GetCustomersAndContactsByContactNames_Service;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.Utils;

/**
 *
 * @author amatos
 */
public class CRM_GetCustomersAndContactsByContactNames extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = CRM_GetCustomersAndContactsByContactNames.class;
		try {
			SOAP = GetCustomersAndContactsByContactNames_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public CRM_GetCustomersAndContactsByContactNames() {
		super(0, "crmgetcustomersandcontactsbycontactnames", SOAP);
	}

}
