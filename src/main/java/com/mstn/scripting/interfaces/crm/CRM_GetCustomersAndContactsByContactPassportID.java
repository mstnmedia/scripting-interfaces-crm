/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.crm;

import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactpassportid.GetCustomersAndContactsByContactPassportID;
import _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactpassportid.GetCustomersAndContactsByContactPassportID_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.InterfaceBase;

/**
 *
 * @author amatos
 */
public class CRM_GetCustomersAndContactsByContactPassportID extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = CRM_GetCustomersAndContactsByContactPassportID.class;
		try {
			SOAP = GetCustomersAndContactsByContactPassportID_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public CRM_GetCustomersAndContactsByContactPassportID() {
		super(0, "crmgetcustomersandcontactsbycontactpassportid", SOAP);
	}

}
