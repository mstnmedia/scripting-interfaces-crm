///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mstn.scripting.interfaces.diagnostico;
//
//import _do.com.claro.soa.model.customer.Diagnoses;
//import _do.com.claro.soa.model.customer.Diagnosis;
//import _do.com.claro.soa.services.customer.getcasediagnosesbycaseid.GetCaseDiagnosesByCaseID;
//import _do.com.claro.soa.services.customer.getcasediagnosesbycaseid.GetCaseDiagnosesByCaseID_Service;
//import _do.com.claro.soa.services.customer.getcasediagnosesbycaseid.GetRequest;
//import _do.com.claro.soa.services.customer.getcasediagnosesbycaseid.GetResponse;
//import com.mstn.scripting.core.JSON;
//import com.mstn.scripting.core.models.InterfaceConfigs;
//import com.mstn.scripting.interfaces.ext.Diagnosis_Ext;
//import com.mstn.scripting.interfaces.ext.DiagnosticResult_Ext;
//import com.mstn.scripting.interfaces.ext.Utils;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//import org.datacontract.schemas._2004._07.claro_sacs_webservices.DiagnosticResult;
//import org.tempuri.DWServices;
//import org.tempuri.IDWServices;
//
///**
// * Clase que implementa métodos del web service de Diagnóstico IDW para ser
// * usados en la interfaz estática {@link DW_DiagnosticResult}.
// *
// * @author amatos
// */
//public class Diagnostico {
//
//	static public List<Diagnosis> getCaseDiagnosesByCaseID(long caseID) {
//		List<Diagnosis> diagnosis = new ArrayList<>();
//		try {
//			GetCaseDiagnosesByCaseID_Service service = new GetCaseDiagnosesByCaseID_Service();
//			GetCaseDiagnosesByCaseID soap = service.getGetCaseDiagnosesByCaseIDSOAP();
//			GetRequest request = new GetRequest();
//			request.setCaseID(caseID);
//			GetResponse response = soap.get(request);
//
//			Diagnoses objDiagnoses = response.getDiagnoses();
//			if (objDiagnoses != null) {
//				diagnosis = objDiagnoses.getDiagnosis();
//			}
//			Utils.logDebug(Diagnostico.class, "Diagnostico getCaseDiagnosesByCaseID: \n");
//			Utils.logDebug(Diagnostico.class, JSON.toString(diagnosis) + "\n");
//		} catch (Exception ex) {
//			Utils.logException("Diagnostico getCaseDiagnosesByCaseID", ex);
//		}
//		return diagnosis;
//	}
//
//	static public List<Diagnosis_Ext> GetCaseDiagnosesByCaseID(long caseID) {
//		return getCaseDiagnosesByCaseID(caseID).stream()
//				.map(i -> new Diagnosis_Ext(i))
//				.collect(Collectors.toList());
//	}
//
//	static public DiagnosticResult getDiagnosticResult(String telefono) {
//		try {
//			DWServices services = new DWServices();
//			IDWServices idwServices = services.getBasicHttpBindingIDWServices();
//			DiagnosticResult diagnostico = idwServices.getDiagnostico(telefono);
//			return diagnostico;
//		} catch (Exception ex) {
//			Utils.logException("Diagnostico ", ex);
//		}
//		return null;
//	}
//
//	static public DiagnosticResult_Ext GetDiagnosticResult(String telefono) {
//		DiagnosticResult diagnostico = getDiagnosticResult(telefono);
//		if (diagnostico != null) {
//			DiagnosticResult_Ext result = new DiagnosticResult_Ext(diagnostico);
//			return result;
//		}
//		return null;
//	}
//}
