///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mstn.scripting.interfaces.diagnostico;
//
//import com.mstn.scripting.core.Utils;
//import com.mstn.scripting.core.models.Interface;
//import com.mstn.scripting.core.models.InterfaceBase;
//import com.mstn.scripting.core.models.InterfaceConfigs;
//import java.lang.reflect.Method;
//import org.tempuri.DWServices;
//import org.tempuri.IDWServices;
//
///**
// * Interfaz dinámica que implementa el web service de Diagnóstico IDW.
// *
// * @author amatos
// */
//public class Diagnostico_IDW extends InterfaceBase {
//
//	public Diagnostico_IDW() {
//		super(0, "diagnosticoidw");
//	}
//
//	@Override
//	protected Object processINSTANCE(Object INSTANCE) {
//		try {
//			IDWServices SOAP = new DWServices().getBasicHttpBindingIDWServices();
//			return SOAP;
//		} catch (Exception ex) {
//			Utils.logException(this.getClass(), "Error instanciando endpoint del web service de Diagnostico IDW", ex);
//			throw ex;
////			return new Object();
//		}
//	}
//
//	@Override
//	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
//		inter = super.getInterface(method, interID, interName, withChildren);
//		inter.setServer_url(InterfaceConfigs.get("diagnostico_web"));
//		return inter;
//	}
//
//	@Override
//	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
//	public void test() throws Exception {
//		InterfaceConfigs.get("diagnostico_web").toString();
//		super.test();
//	}
//
//}
