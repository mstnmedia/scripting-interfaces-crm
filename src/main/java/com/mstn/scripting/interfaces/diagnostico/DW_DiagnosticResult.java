///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mstn.scripting.interfaces.diagnostico;
//
//import com.mstn.scripting.core.DATE;
//import com.mstn.scripting.core.JSON;
//import com.mstn.scripting.core.Utils;
//import com.mstn.scripting.core.auth.jwt.User;
//import com.mstn.scripting.core.enums.InterfaceFieldTypes;
//import com.mstn.scripting.core.enums.TransactionInterfaceActions;
//import com.mstn.scripting.core.models.InterfaceConnectorBase;
//import com.mstn.scripting.core.models.Interface_Field;
//import com.mstn.scripting.core.models.TransactionInterfacesPayload;
//import com.mstn.scripting.core.models.TransactionInterfacesResponse;
//import com.mstn.scripting.core.models.Transaction_Result;
//import com.mstn.scripting.interfaces.ext.DiagnosticResult_Ext;
//import java.util.Arrays;
//import java.util.List;
//
///**
// * Interfaz estática que intentó implementar el web service de Diagnóstico IDW.
// * Se sustituyó por {@link Diagnostico_IDW}
// *
// * @author amatos
// * @deprecated
// */
//public class DW_DiagnosticResult extends InterfaceConnectorBase {
//
//	private final static int ID = 4000;
//	private final static String NAME = "dw_diagnostic_result";
//	private final static String LABEL = "Diagnóstico Web - Resultados";
//	private final static String SERVER_URL = "http://nttappsweb0010.corp.codetel.com.do/SACSWS/DWServices.svc";
//	private final static String VALUE_STEP = "success";
//	private final static boolean DELETED = false;
//
//	private final String prefix = NAME + "_";
//	private final String PHONE_NUMBER = prefix + "phone_number";
//
//	private DiagnosticResult_Ext diagnostico = new DiagnosticResult_Ext(null);
//
//	public DW_DiagnosticResult() {
//		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
//		showBaseLogs = true;
//	}
//
//	@Override
//	public List<Interface_Field> getFields() {
//		return Arrays.asList(//(id, id_interface, name, label, id_type, order_index, default_value, required, readonly)
//				new Interface_Field(22, ID, PHONE_NUMBER, "Teléfono", InterfaceFieldTypes.TEXT, 0, null, true, false)
//		);
//	}
//
//	@Override
//	public List<Transaction_Result> getResults() {
//		return Arrays.asList(
//				//(id_interface, name, label)
//				//(id, id_interface, name, label, value, props)
//				new Transaction_Result(00, ID, prefix + "CodigoPrueba", "CodigoPrueba", diagnostico.CodigoPrueba, ""),
//				new Transaction_Result(1, ID, prefix + "Messege", "Messege", diagnostico.Messege, ""),
//				new Transaction_Result(2, ID, prefix + "TestResult", "TestResult", diagnostico.TestResult, ""),
//				new Transaction_Result(3, ID, prefix + "ID", "ID", diagnostico.ID, ""),
//				new Transaction_Result(4, ID, prefix + "IDFACILIDADES", "IDFACILIDADES", diagnostico.IDFACILIDADES, ""),
//				new Transaction_Result(5, ID, prefix + "Atenuacion", "Atenuacion", diagnostico.Atenuacion, ""),
//				new Transaction_Result(6, ID, prefix + "AtenuacionBajada", "AtenuacionBajada", diagnostico.AtenuacionBajada, ""),
//				new Transaction_Result(7, ID, prefix + "AtenuacionBajadaLoop", "AtenuacionBajadaLoop", diagnostico.AtenuacionBajadaLoop, ""),
//				new Transaction_Result(8, ID, prefix + "AtenuacionSubida", "AtenuacionSubida", diagnostico.AtenuacionSubida, ""),
//				new Transaction_Result(9, ID, prefix + "AtenuacionSubidaLoop", "AtenuacionSubidaLoop", diagnostico.AtenuacionSubidaLoop, ""),
//				new Transaction_Result(10, ID, prefix + "AutenticadorIPTV", "AutenticadorIPTV", diagnostico.AutenticadorIPTV, ""),
//				new Transaction_Result(11, ID, prefix + "CondAdministrativa", "CondAdministrativa", diagnostico.CondAdministrativa, ""),
//				new Transaction_Result(12, ID, prefix + "Configuracion", "Configuracion", diagnostico.Configuracion, ""),
//				new Transaction_Result(13, ID, prefix + "Desbalance", "Desbalance", diagnostico.Desbalance, ""),
//				new Transaction_Result(14, ID, prefix + "IP", "IP", diagnostico.IP, ""),
//				new Transaction_Result(15, ID, prefix + "IPVIDEO", "IPVIDEO", diagnostico.IPVIDEO, ""),
//				new Transaction_Result(16, ID, prefix + "MargenEstabilidad", "MargenEstabilidad", diagnostico.MargenEstabilidad, ""),
//				new Transaction_Result(17, ID, prefix + "PVC33", "PVC33", diagnostico.PVC33, ""),
//				new Transaction_Result(18, ID, prefix + "PVC35", "PVC35", diagnostico.PVC35, ""),
//				new Transaction_Result(19, ID, prefix + "Ruido", "Ruido", diagnostico.Ruido, ""),
//				new Transaction_Result(20, ID, prefix + "RuidoBajada", "RuidoBajada", diagnostico.RuidoBajada, ""),
//				new Transaction_Result(21, ID, prefix + "RuidoSubida", "RuidoSubida", diagnostico.RuidoSubida, ""),
//				new Transaction_Result(22, ID, prefix + "ServiceProfile", "ServiceProfile", diagnostico.ServiceProfile, ""),
//				new Transaction_Result(23, ID, prefix + "VLAN", "VLAN", diagnostico.VLAN, ""),
//				new Transaction_Result(24, ID, prefix + "VLANIPTV", "VLANIPTV", diagnostico.VLANIPTV, ""),
//				new Transaction_Result(25, ID, prefix + "Velocidad", "Velocidad", diagnostico.Velocidad, ""),
//				new Transaction_Result(26, ID, prefix + "IDDiagnosticoGpon", "IDDiagnosticoGpon", diagnostico.IDDiagnosticoGpon, ""),
//				new Transaction_Result(27, ID, prefix + "IDFACILIDADESDiagnosticoGpon", "IDFACILIDADESDiagnosticoGpon", diagnostico.IDFACILIDADESDiagnosticoGpon, ""),
//				new Transaction_Result(28, ID, prefix + "ADSL", "ADSL", diagnostico.ADSL, ""),
//				new Transaction_Result(29, ID, prefix + "ADSLServicePortStatus", "ADSLServicePortStatus", diagnostico.ADSLServicePortStatus, ""),
//				new Transaction_Result(30, ID, prefix + "IPADSL", "IPADSL", diagnostico.IPADSL, ""),
//				new Transaction_Result(31, ID, prefix + "IPIPTV", "IPIPTV", diagnostico.IPIPTV, ""),
//				new Transaction_Result(32, ID, prefix + "IPTV", "IPTV", diagnostico.IPTV, ""),
//				new Transaction_Result(33, ID, prefix + "IPTVServicePortStatus", "IPTVServicePortStatus", diagnostico.IPTVServicePortStatus, ""),
//				new Transaction_Result(34, ID, prefix + "IPTVTxRx", "IPTVTxRx", diagnostico.IPTVTxRx, ""),
//				new Transaction_Result(35, ID, prefix + "IPVOIP", "IPVOIP", diagnostico.IPVOIP, ""),
//				new Transaction_Result(36, ID, prefix + "OLTOperState", "OLTOperState", diagnostico.OLTOperState, ""),
//				new Transaction_Result(37, ID, prefix + "OLTRxONTOpticalPower", "OLTRxONTOpticalPower", diagnostico.OLTRxONTOpticalPower, ""),
//				new Transaction_Result(38, ID, prefix + "ONTAdmState", "ONTAdmState", diagnostico.ONTAdmState, ""),
//				new Transaction_Result(39, ID, prefix + "ONTBiasCurrent", "ONTBiasCurrent", diagnostico.ONTBiasCurrent, ""),
//				new Transaction_Result(40, ID, prefix + "ONTOperState", "ONTOperState", diagnostico.ONTOperState, ""),
//				new Transaction_Result(41, ID, prefix + "ONTRxPower", "ONTRxPower", diagnostico.ONTRxPower, ""),
//				new Transaction_Result(42, ID, prefix + "ONTTemp", "ONTTemp", diagnostico.ONTTemp, ""),
//				new Transaction_Result(43, ID, prefix + "ONTTxPower", "ONTTxPower", diagnostico.ONTTxPower, ""),
//				new Transaction_Result(44, ID, prefix + "ONTVoltage", "ONTVoltage", diagnostico.ONTVoltage, ""),
//				new Transaction_Result(45, ID, prefix + "Password", "Password", diagnostico.Password, ""),
//				new Transaction_Result(46, ID, prefix + "TestResultDiagnosticoGpon", "TestResultDiagnosticoGpon", diagnostico.TestResultDiagnosticoGpon, ""),
//				new Transaction_Result(47, ID, prefix + "VOIP", "VOIP", diagnostico.VOIP, ""),
//				new Transaction_Result(48, ID, prefix + "VOIPServicePortStatus", "VOIPServicePortStatus", diagnostico.VOIPServicePortStatus, ""),
//				new Transaction_Result(49, ID, prefix + "VelConfigurada", "VelConfigurada", diagnostico.VelConfigurada, ""),
//				new Transaction_Result(50, ID, prefix + "VelConfiguradaDN", "VelConfiguradaDN", diagnostico.VelConfiguradaDN, ""),
//				new Transaction_Result(51, ID, prefix + "VelConfiguradaUP", "VelConfiguradaUP", diagnostico.VelConfiguradaUP, ""),
//				new Transaction_Result(52, ID, prefix + "IDDslamInfo", "IDDslamInfo", diagnostico.IDDslamInfo, ""),
//				new Transaction_Result(53, ID, prefix + "IDFACILIDADESDslamInfo", "IDFACILIDADESDslamInfo", diagnostico.IDFACILIDADESDslamInfo, ""),
//				new Transaction_Result(54, ID, prefix + "ActualType", "ActualType", diagnostico.ActualType, ""),
//				new Transaction_Result(55, ID, prefix + "Availability", "Availability", diagnostico.Availability, ""),
//				new Transaction_Result(56, ID, prefix + "Cabina", "Cabina", diagnostico.Cabina, ""),
//				new Transaction_Result(57, ID, prefix + "CalidadServicio", "CalidadServicio", diagnostico.CalidadServicio, ""),
//				new Transaction_Result(58, ID, prefix + "CustomerID", "CustomerID", diagnostico.CustomerID, ""),
//				new Transaction_Result(59, ID, prefix + "DiagnosticoCobre", "DiagnosticoCobre", diagnostico.DiagnosticoCobre, ""),
//				new Transaction_Result(60, ID, prefix + "DistaciaSVDG", "DistaciaSVDG", diagnostico.DistaciaSVDG, ""),
//				new Transaction_Result(61, ID, prefix + "Distancia", "Distancia", diagnostico.Distancia, ""),
//				new Transaction_Result(62, ID, prefix + "ErrorStatus", "ErrorStatus", diagnostico.ErrorStatus, ""),
//				new Transaction_Result(63, ID, prefix + "Facilida", "Facilida", diagnostico.Facilida, ""),
//				new Transaction_Result(64, ID, prefix + "Feeder", "Feeder", diagnostico.Feeder, ""),
//				new Transaction_Result(65, ID, prefix + "LoopAtenuacionBajada", "LoopAtenuacionBajada", diagnostico.LoopAtenuacionBajada, ""),
//				new Transaction_Result(66, ID, prefix + "LoopAtenuacionSubida", "LoopAtenuacionSubida", diagnostico.LoopAtenuacionSubida, ""),
//				new Transaction_Result(67, ID, prefix + "MACAddress", "MACAddress", diagnostico.MACAddress, ""),
//				new Transaction_Result(68, ID, prefix + "MaximaVelocidadBajada", "MaximaVelocidadBajada", diagnostico.MaximaVelocidadBajada, ""),
//				new Transaction_Result(69, ID, prefix + "MaximaVelocidadSubida", "MaximaVelocidadSubida", diagnostico.MaximaVelocidadSubida, ""),
//				new Transaction_Result(70, ID, prefix + "NombreProfile", "NombreProfile", diagnostico.NombreProfile, ""),
//				new Transaction_Result(71, ID, prefix + "NumeroProfile", "NumeroProfile", diagnostico.NumeroProfile, ""),
//				new Transaction_Result(72, ID, prefix + "RestrtCnt", "RestrtCnt", diagnostico.RestrtCnt, ""),
//				new Transaction_Result(73, ID, prefix + "RinitID", "RinitID", diagnostico.RinitID, ""),
//				new Transaction_Result(74, ID, prefix + "RuidoBajadaDslamInfo", "RuidoBajadaDslamInfo", diagnostico.RuidoBajadaDslamInfo, ""),
//				new Transaction_Result(75, ID, prefix + "RuidoSubidaDslamInfo", "RuidoSubidaDslamInfo", diagnostico.RuidoSubidaDslamInfo, ""),
//				new Transaction_Result(76, ID, prefix + "SenalAtenuacionBajada", "SenalAtenuacionBajada", diagnostico.SenalAtenuacionBajada, ""),
//				new Transaction_Result(77, ID, prefix + "SenalAtenuacionSubida", "SenalAtenuacionSubida", diagnostico.SenalAtenuacionSubida, ""),
//				new Transaction_Result(78, ID, prefix + "SerialModem", "SerialModem", diagnostico.SerialModem, ""),
//				new Transaction_Result(79, ID, prefix + "SessionId", "SessionId", diagnostico.SessionId, ""),
//				new Transaction_Result(80, ID, prefix + "SessionIdIPTV", "SessionIdIPTV", diagnostico.SessionIdIPTV, ""),
//				new Transaction_Result(81, ID, prefix + "Slot", "Slot", diagnostico.Slot, ""),
//				new Transaction_Result(82, ID, prefix + "TipoTarjeta", "TipoTarjeta", diagnostico.TipoTarjeta, ""),
//				new Transaction_Result(83, ID, prefix + "UltimaBajada", "UltimaBajada", diagnostico.UltimaBajada, ""),
//				new Transaction_Result(84, ID, prefix + "UltimoSincronismo", "UltimoSincronismo", diagnostico.UltimoSincronismo, ""),
//				new Transaction_Result(85, ID, prefix + "User", "User", diagnostico.User, ""),
//				new Transaction_Result(86, ID, prefix + "VLANId", "VLANId", diagnostico.VLANId, ""),
//				new Transaction_Result(87, ID, prefix + "VelocidadADSL", "VelocidadADSL", diagnostico.VelocidadADSL, ""),
//				new Transaction_Result(88, ID, prefix + "VelocidadActualDN", "VelocidadActualDN", diagnostico.VelocidadActualDN, ""),
//				new Transaction_Result(89, ID, prefix + "VelocidadActualUP", "VelocidadActualUP", diagnostico.VelocidadActualUP, ""),
//				new Transaction_Result(90, ID, prefix + "VelocidadConfiguradaDN", "VelocidadConfiguradaDN", diagnostico.VelocidadConfiguradaDN, ""),
//				new Transaction_Result(91, ID, prefix + "VelocidadConfiguradaUP", "VelocidadConfiguradaUP", diagnostico.VelocidadConfiguradaUP, ""),
//				new Transaction_Result(92, ID, prefix + "VelocidadContratadaDN", "VelocidadContratadaDN", diagnostico.VelocidadContratadaDN, ""),
//				new Transaction_Result(93, ID, prefix + "VelocidadContratadaUP", "VelocidadContratadaUP", diagnostico.VelocidadContratadaUP, ""),
//				new Transaction_Result(94, ID, prefix + "VelocidadPuertoDN", "VelocidadPuertoDN", diagnostico.VelocidadPuertoDN, ""),
//				new Transaction_Result(95, ID, prefix + "VelocidadPuertoUP", "VelocidadPuertoUP", diagnostico.VelocidadPuertoUP, ""),
//				new Transaction_Result(96, ID, prefix + "CondicionAdministrativa", "CondicionAdministrativa", diagnostico.CondicionAdministrativa, ""),
//				new Transaction_Result(97, ID, prefix + "CondicionOperativa", "CondicionOperativa", diagnostico.CondicionOperativa, ""),
//				new Transaction_Result(98, ID, prefix + "DividirPorMillar", "DividirPorMillar", diagnostico.DividirPorMillar, ""),
//				new Transaction_Result(99, ID, prefix + "Enabled", "Enabled", diagnostico.Enabled, ""),
//				new Transaction_Result(100, ID, prefix + "LineaDefectuosa", "LineaDefectuosa", diagnostico.LineaDefectuosa, ""),
//				new Transaction_Result(101, ID, prefix + "PVC33DslamInfo", "PVC33DslamInfo", diagnostico.PVC33DslamInfo, ""),
//				new Transaction_Result(102, ID, prefix + "PVC35DslamInfo", "PVC35DslamInfo", diagnostico.PVC35DslamInfo, ""),
//				new Transaction_Result(103, ID, prefix + "VLANIPTVDslamInfo", "VLANIPTVDslamInfo", diagnostico.VLANIPTVDslamInfo, ""),
//				new Transaction_Result(104, ID, prefix + "IDFACILITY", "IDFACILITY", diagnostico.IDFACILITY, ""),
//				new Transaction_Result(105, ID, prefix + "CIRCUITDESIGNID", "CIRCUITDESIGNID", diagnostico.CIRCUITDESIGNID, ""),
//				new Transaction_Result(106, ID, prefix + "CIRCUITID", "CIRCUITID", diagnostico.CIRCUITID, ""),
//				new Transaction_Result(107, ID, prefix + "CLLICODE", "CLLICODE", diagnostico.CLLICODE, ""),
//				new Transaction_Result(108, ID, prefix + "EQUIPMENTNAME", "EQUIPMENTNAME", diagnostico.EQUIPMENTNAME, ""),
//				new Transaction_Result(109, ID, prefix + "FACILITYCOUNT", "FACILITYCOUNT", diagnostico.FACILITYCOUNT, ""),
//				new Transaction_Result(110, ID, prefix + "IPADDRESS", "IPADDRESS", diagnostico.IPADDRESS, ""),
//				new Transaction_Result(111, ID, prefix + "NODEADDRESS", "NODEADDRESS", diagnostico.NODEADDRESS, ""),
//				new Transaction_Result(112, ID, prefix + "NODENAME", "NODENAME", diagnostico.NODENAME, ""),
//				new Transaction_Result(113, ID, prefix + "PUERTO", "PUERTO", diagnostico.PUERTO, ""),
//				new Transaction_Result(114, ID, prefix + "REFERENCIA", "REFERENCIA", diagnostico.REFERENCIA, ""),
//				new Transaction_Result(115, ID, prefix + "STATUS", "STATUS", diagnostico.STATUS, ""),
//				new Transaction_Result(116, ID, prefix + "TELEFONO", "TELEFONO", diagnostico.TELEFONO, ""),
//				new Transaction_Result(117, ID, prefix + "TIPO", "TIPO", diagnostico.TIPO, ""),
//				new Transaction_Result(118, ID, prefix + "TYPE", "TYPE", diagnostico.TYPE, ""),
//				new Transaction_Result(119, ID, prefix + "VENDORNAME", "VENDORNAME", diagnostico.VENDORNAME, ""),
//				new Transaction_Result(120, ID, prefix + "CurrentThroughput", "CurrentThroughput", diagnostico.CurrentThroughput, ""),
//				new Transaction_Result(121, ID, prefix + "DataUsageDS", "DataUsageDS", diagnostico.DataUsageDS, ""),
//				new Transaction_Result(122, ID, prefix + "DataUsageUS", "DataUsageUS", diagnostico.DataUsageUS, ""),
//				new Transaction_Result(123, ID, prefix + "OLTFirmware", "OLTFirmware", diagnostico.OLTFirmware, ""),
//				new Transaction_Result(124, ID, prefix + "OLTPassword", "OLTPassword", diagnostico.OLTPassword, ""),
//				new Transaction_Result(125, ID, prefix + "OLTPortRxPower", "OLTPortRxPower", diagnostico.OLTPortRxPower, ""),
//				new Transaction_Result(126, ID, prefix + "OLTPortTxPower", "OLTPortTxPower", diagnostico.OLTPortTxPower, ""),
//				new Transaction_Result(127, ID, prefix + "OLTRxONTOpticalPowerGponInfo", "OLTRxONTOpticalPowerGponInfo", diagnostico.OLTRxONTOpticalPowerGponInfo, ""),
//				new Transaction_Result(128, ID, prefix + "OLTUptime", "OLTUptime", diagnostico.OLTUptime, ""),
//				new Transaction_Result(129, ID, prefix + "ONTBatteryStatus", "ONTBatteryStatus", diagnostico.ONTBatteryStatus, ""),
//				new Transaction_Result(130, ID, prefix + "ONTBiasCurrentGponInfo", "ONTBiasCurrentGponInfo", diagnostico.ONTBiasCurrentGponInfo, ""),
//				new Transaction_Result(131, ID, prefix + "ONTCPUUsage", "ONTCPUUsage", diagnostico.ONTCPUUsage, ""),
//				new Transaction_Result(132, ID, prefix + "ONTDescription", "ONTDescription", diagnostico.ONTDescription, ""),
//				new Transaction_Result(133, ID, prefix + "ONTDistancia", "ONTDistancia", diagnostico.ONTDistancia, ""),
//				new Transaction_Result(134, ID, prefix + "ONTLastDown", "ONTLastDown", diagnostico.ONTLastDown, ""),
//				new Transaction_Result(135, ID, prefix + "ONTLastDownCause", "ONTLastDownCause", diagnostico.ONTLastDownCause, ""),
//				new Transaction_Result(136, ID, prefix + "ONTLastDownTime", "ONTLastDownTime", diagnostico.ONTLastDownTime, ""),
//				new Transaction_Result(137, ID, prefix + "ONTLineProfile", "ONTLineProfile", diagnostico.ONTLineProfile, ""),
//				new Transaction_Result(138, ID, prefix + "ONTMemUsage", "ONTMemUsage", diagnostico.ONTMemUsage, ""),
//				new Transaction_Result(139, ID, prefix + "ONTOnlineDuration", "ONTOnlineDuration", diagnostico.ONTOnlineDuration, ""),
//				new Transaction_Result(140, ID, prefix + "ONTRxPowerGponInfo", "ONTRxPowerGponInfo", diagnostico.ONTRxPowerGponInfo, ""),
//				new Transaction_Result(141, ID, prefix + "ONTSerialNumber", "ONTSerialNumber", diagnostico.ONTSerialNumber, ""),
//				new Transaction_Result(142, ID, prefix + "ONTSoftwareVersion", "ONTSoftwareVersion", diagnostico.ONTSoftwareVersion, ""),
//				new Transaction_Result(143, ID, prefix + "ONTTempGponInfo", "ONTTempGponInfo", diagnostico.ONTTempGponInfo, ""),
//				new Transaction_Result(144, ID, prefix + "ONTTxPowerGponInfo", "ONTTxPowerGponInfo", diagnostico.ONTTxPowerGponInfo, ""),
//				new Transaction_Result(145, ID, prefix + "ONTUpTime", "ONTUpTime", diagnostico.ONTUpTime, ""),
//				new Transaction_Result(146, ID, prefix + "ONTVendor", "ONTVendor", diagnostico.ONTVendor, ""),
//				new Transaction_Result(147, ID, prefix + "ONTVersion", "ONTVersion", diagnostico.ONTVersion, ""),
//				new Transaction_Result(148, ID, prefix + "ONTVoltageGponInfo", "ONTVoltageGponInfo", diagnostico.ONTVoltageGponInfo, ""),
//				new Transaction_Result(149, ID, prefix + "OntProductDescription", "OntProductDescription", diagnostico.OntProductDescription, ""),
//				new Transaction_Result(150, ID, prefix + "VelocidadConfiguradaDNGponInfo", "VelocidadConfiguradaDNGponInfo", diagnostico.VelocidadConfiguradaDNGponInfo, ""),
//				new Transaction_Result(151, ID, prefix + "VelocidadConfiguradaUPGponInfo", "VelocidadConfiguradaUPGponInfo", diagnostico.VelocidadConfiguradaUPGponInfo, ""),
//				new Transaction_Result(152, ID, prefix + "VelocidadContratadaDNGponInfo", "VelocidadContratadaDNGponInfo", diagnostico.VelocidadContratadaDNGponInfo, ""),
//				new Transaction_Result(153, ID, prefix + "VelocidadContratadaUPGponInfo", "VelocidadContratadaUPGponInfo", diagnostico.VelocidadContratadaUPGponInfo, "")
//		);
//	}
//
//	@Override
//	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) throws Exception {
//		baseLog("DW_DiagnosticResult payload: " + JSON.toString(payload));
//		try {
//			initializeFromPayload(payload, user);
//			List<Interface_Field> fields = getFields();
//			baseLog("DW_DiagnosticResult formIsFilledOrValid: " + Boolean.toString(formIsFilledOrValid));
//			if (!formIsFilledOrValid) {
//				response.setAction(TransactionInterfaceActions.SHOW_FORM);
//
//				// TODO: LLenar formulario desde el formulario enviado, los valores fijos del paso, 
//				//		valores default de los campos, y variables
//				fields.forEach(field -> {
//					String fieldName = field.getName();
//					if (!JSON.hasValue(form, fieldName) && Utils.stringNonNullOrEmpty(field.getDefault_value())) {
//						form.put(field.getName(), field.getDefault_value());
//					}
//				});
//				form.put("__generationDate", DATE.nowAsLong());
//				inter.setForm(JSON.toString(form));
//
//				//TODO: Llenar aquí los campos que el usuario debe llenar, y los campos
//				//		informativos con readonly = true
//				inter.setInterface_field(fields);
//			}
//			if (formIsFilledOrValid) {
//				response.setAction(TransactionInterfaceActions.NEXT_STEP);
//
//				String telefono = JSON.getString(form, PHONE_NUMBER);
//				diagnostico = Diagnostico.GetDiagnosticResult(telefono);
//
//				results = getResults();
//				response.setResults(results);
//
//				//Calculate response value
//				String value = getValueFromJavascript();
//				response.setValue(value);
//			}
//		} catch (Exception ex) {
//			setAsExceptionResponse(response, ex);
//		}
//		return response;
//	}
//
//}
