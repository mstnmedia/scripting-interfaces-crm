/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.miclaro;

import codetel.agp.webservice.service.MiClaroProvider;
import codetel.agp.webservice.service.MiClaroProviderService;
import codetel.agp.webservice.service.MiClaroRegisteredMemberVerificationInformation;
import codetel.agp.webservice.service.responses.MessageInfo;

/**
 * Clase que consulta el web service de Mi Claro para agregar si el cliente
 * tiene Mi Claro a la transacción en curso.
 *
 * @author amatos
 */
public class MiClaro {

	static public MiClaroProviderService getSoap() {
		return MiClaroProvider.getInstance();
	}

	static public MessageInfo ValidateCustomerExistInClaro(String documentIdentifier) {
		MessageInfo response = getSoap().validateCustomerExistInClaro(documentIdentifier);
		return response;
	}

	static public MiClaroRegisteredMemberVerificationInformation RegisteredMemberVerification(String documentIdentifier) {
		MiClaroRegisteredMemberVerificationInformation response = getSoap().miClaroRegisteredMemberVerification(documentIdentifier);
		return response;
	}

}
