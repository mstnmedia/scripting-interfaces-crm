/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.miclaro;

import codetel.agp.webservice.service.ContractUserModel;
import codetel.agp.webservice.service.CustomerBasicInformationResponse;
import codetel.agp.webservice.service.DeleteUserStatus;
import codetel.agp.webservice.service.MiClaroProvider;
import codetel.agp.webservice.service.MiClaroRegisteredMemberVerificationInformation;
import codetel.agp.webservice.service.Procedence;
import codetel.agp.webservice.service.ReferCreationVoucher;
import codetel.agp.webservice.service.responses.MessageInfo;
import java.util.List;
import responses.service.webservice.agp.codetel.CambiazoInformation;

/**
 * Clase que envuelve una instancia del endpoint del web service de Mi Claro
 * para que Scripting pueda manejar los resultados tipo lista que retornan
 * algunos de sus métodos.
 *
 * @author amatos
 */
public class MiClaroProviderService {

	private final codetel.agp.webservice.service.MiClaroProviderService base;

	public MiClaroProviderService() {
		this.base = MiClaroProvider.getInstance();
	}

	public DeleteUserStatus deleteUser(String username, String clientId, String secret) {
		return base.deleteUser(username, clientId, secret);
	}

	public MessageInfo validateCustomerExistInClaro(String documentIdentifier) {
		return base.validateCustomerExistInClaro(documentIdentifier);
	}

	public CustomerBasicInformationResponse getCustomerBasicInformation(String documentIdentifier) {
		return base.getCustomerBasicInformation(documentIdentifier);
	}

	public MiClaroRegisteredMemberVerificationInformation miClaroRegisteredMemberVerification(String documentId) {
		return base.miClaroRegisteredMemberVerification(documentId);
	}

	public ReferCreationVoucher customerReferance(String login, String email, Procedence procedence) {
		return base.customerReferance(login, email, procedence);
	}

	public CambiazoInformation getCambiazoInformationForMSISDN(String msisdn) {
		return base.getCambiazoInformationForMSISDN(msisdn);
	}

	public ListContractUserModel getCustomerContractByMsisdn(String msisdn) {
		ListContractUserModel result = new ListContractUserModel();
		result.list = base.getCustomerContractByMsisdn(msisdn);
		return result;
	}

	public ReferCreationVoucher employeeReferance(String employeeCode, String email, String documentIdentifier, String phoneNumnberToSms, String location) {
		return base.employeeReferance(employeeCode, email, documentIdentifier, phoneNumnberToSms, location);
	}

	public ListContractUserModel getCustomerContractByDocument(String documentIdentifier) {
		ListContractUserModel result = new ListContractUserModel();
		result.list = base.getCustomerContractByDocument(documentIdentifier);
		return result;
	}

	public class ListContractUserModel {

		List<ContractUserModel> list;
	}
}
