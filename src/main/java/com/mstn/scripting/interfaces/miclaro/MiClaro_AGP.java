/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.miclaro;

import codetel.agp.webservice.service.responses.MessageInfo;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.Transaction_Result;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.JAXBElement;

/**
 * Interfaz dinámica que conecta a Scripting con Mi Claro
 *
 * @author amatos
 */
public class MiClaro_AGP extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = MiClaro_AGP.class;
		try {
			if ("true".equals(InterfaceConfigs.get("ignore_miclaro"))) {
				Utils.logWarn(_class, "Ignorando interfaces en clase " + _class.getCanonicalName());
				SOAP = new Object();
			} else {
				SOAP = new MiClaroProviderService();
			}
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint MiClaro", ex);
			if ("true".equals(InterfaceConfigs.get("ignoreExceptions_miclaro"))) {
				Utils.logWarn(_class, "Error ignorado por configuración. Continuará el proceso.");
				SOAP = new Object();
			} else {
				throw ex;
			}
		}
	}

	public MiClaro_AGP() {
		super(0, "miclaroagp", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("miclaro"));
		return inter;
	}

	@Override
	protected List<Field> getClassFields(Class _class) {
//		if (SubmitSMResp.class == _class) {
//			return Utils.getFieldsUpTo(_class, MessageInfo.class);
//		} else {
//			return super.getClassFields(_class);
//		}
		return Utils.getFieldsUpTo(_class, MessageInfo.class);
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("login")) {
			field.setLabel("Usuario");
		} else if (fieldName.endsWith("email")) {
			field.setLabel("Correo");
			field.setId_type(InterfaceFieldTypes.EMAIL);
			field.setRequired(false);
		} else if (fieldName.endsWith("procedence")) {
			field.setLabel("Procedencia");
			field.setRequired(false);
		} else if (fieldName.endsWith("employeeCode")) {
			field.setLabel("Tarjeta");
			field.setRequired(false);
		} else if (fieldName.endsWith("documentIdentifier")) {
			field.setLabel("Identificación");
		} else if (fieldName.endsWith("phoneNumnberToSms")) {
			field.setLabel("Número para Mensaje de Texto");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setRequired(false);
		} else if (fieldName.endsWith("location")) {
			field.setLabel("Lugar");
			field.setRequired(false);
		} else if (fieldName.endsWith("msisdn")) {
			field.setLabel("No. de Teléfono");
		} else if (fieldName.endsWith("documentId")) {
			field.setLabel("Documento");
		}
		return field;
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter, String paramName, List<Class> inspectedClases) {
		baseLog("ResultsFromClass: " + paramName);
		Class _class = inspectedClases.get(inspectedClases.size() - 1);
		if (inter.getName().endsWith("miClaroRegisteredMemberVerification") && JAXBElement.class == _class) {
			return Arrays.asList(
					super.getTransactionResultFromClass(String.class, inter, paramName + "_name"),
					super.getTransactionResultFromClass(String.class, inter, paramName + "_value")
			);
		}
		return super.getTransactionResultsFromClass(method, inter, paramName, inspectedClases);
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter, String paramName) throws Exception {
		baseLog("ResultsFromResponse: " + paramName);
		if (inter.getName().endsWith("miClaroRegisteredMemberVerification")
				&& value != null && value.getClass() == JAXBElement.class) {
			JAXBElement jax = (JAXBElement) value;
			return Arrays.asList(
					super.getTransactionResultFromResponse(jax.getName().getLocalPart(), inter, paramName + "_name"),
					super.getTransactionResultFromResponse(jax.getValue(), inter, paramName + "_value")
			);
		}
		return super.getTransactionResultsFromResponse(value, inter, paramName);
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("miclaro").toString();
		super.test();
	}

}
