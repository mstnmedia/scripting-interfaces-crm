package com.mstn.scripting.interfaces.sad;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 * Interfaz que agrega a Scripting la conexión a SAD desde una transacción en
 * curso.
 *
 * @author amatos
 */
public class SAD_V2 extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = SAD_V2.class;
		try {
			SOAP = new DireccionServiceImpl();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public SAD_V2() {
		super(0, "sad", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("sad_v2"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("_tn")) {
			field.setLabel("Número de teléfono");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
		} else if (fieldName.endsWith("_idd")) {
			field.setLabel("IDT de dirección única");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("_idt")) {
			field.setLabel("IDT de dirección");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("_idtCalle")) {
			field.setLabel("IDT de calle");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("_idtSector")) {
			field.setLabel("IDT de sector");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("_idtBarrio")) {
			field.setLabel("IDT de barrio");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("_idtCiudad")) {
			field.setLabel("IDT de ciudad");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("_idTs")) {
			field.setLabel("IDTs separados por coma");//IDTs separados por coma
//			field.setId_type(InterfaceFieldTypes.NUMBER);
		}
		return field;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("sad_v2").toString();
		super.test();
	}
}
