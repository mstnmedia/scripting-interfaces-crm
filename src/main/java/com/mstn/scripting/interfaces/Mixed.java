/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces;

import _do.com.claro.soa.model.ordering.OrderAction;
import codetel.agp.webservice.service.MiClaroRegisteredMemberVerificationInformation;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.interfaces.ppg.PPG;
import com.mstn.scripting.interfaces.ensamble.Ensamble;
import com.mstn.scripting.interfaces.crm.CRM;
import com.mstn.scripting.interfaces.ext.Customer_Ext;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import com.mstn.scripting.interfaces.miclaro.MiClaro;
import com.mstn.scripting.interfaces.oms.OMS;
import com.mstn.scripting.interfaces.sad.DireccionServiceImpl;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;

/**
 * Clase con métodos de consulta a CRM, Ensamble y Mi Claro para obtener
 * información para la pantalla de crear nueva transacción.
 *
 * @author amatos
 */
public class Mixed {

	static public List<Customer_Ext> GetCustomerListByPhoneNumber(String phoneNumber) throws Exception {
		String customerID = Ensamble.GetCustomerIDFromSubscriberNo(phoneNumber);
		Utils.logTrace(Mixed.class, "MixedE GetAllCustomerByPhoneNumber customerID: " + customerID);
		//if (Utils.stringIsNullOrEmpty(customerID)) {
		//	customerID = PPG.GetCustomerIDFromSubscriberNo(phoneNumber);
		//	Utils.logDebug(Mixed.class, "MixedP GetAllCustomerByPhoneNumber customerID: " + customerID);
		//}
		List<Customer_Ext> results = CRM.GetCustomerListByCustomerID(customerID);
		return results;
	}

	static public List<Subscription_Ext> GetPageSubscriptionsByCustomerID(Customer_Ext customer, int pageNum) {
		List<Subscription_Ext> subscriptions = new ArrayList<>();
		long start = DATE.nowAsLong();
		List<Subscription_Ext> postpaids = Ensamble.GetPagePostpaidSubscriptionsByCustomerID(customer.getCustomerID(), pageNum);
		long med = DATE.nowAsLong();
		Utils.logTrace(Mixed.class, "Mixed Postpaid: " + (med - start));
		List<Subscription_Ext> prepaids = PPG.GetPagePrepaidSubscriptionsByCustomerID("02_" + customer.getDocument(), pageNum);
		long end = DATE.nowAsLong();
		Utils.logTrace(Mixed.class, "Mixed Prepaid: " + (end - med));
		subscriptions.addAll(postpaids);
		subscriptions.addAll(prepaids);
		return subscriptions;
	}

	static public List<Subscription_Ext> GetAllSubscriptionsByCustomerID(String customerID) {
		List<Subscription_Ext> subscriptions = new ArrayList<>();
		List<Subscription_Ext> postpaids = Ensamble.GetAllPostpaidSubscriptionsByCustomerID(customerID);
		List<Subscription_Ext> prepaids = PPG.GetAllPostpaidSubscriptionsByCustomerID(customerID);
		subscriptions.addAll(postpaids);
		subscriptions.addAll(prepaids);
		return subscriptions;
	}

	static public void SetCustomerLastOrder(Customer_Ext c) {
		List<OrderAction> orders = OMS.getOrderActionsByCustomerID(c.getCustomerID(), 1);
		if (!orders.isEmpty()) {
			OrderAction lastOrder = orders.get(0);
			c.setLastOrderDate(DATE.toString(lastOrder.getDueDate()));
			OrderAction.Reason reason = lastOrder.getReason();
			if (reason != null) {
				c.setLastOrderReason(reason.getDescription());
			}
		}
	}

	static public void SetSADAddress(Subscription_Ext s) {
		DireccionServiceImpl service = new DireccionServiceImpl();
		String address = service.getTextoDireccionPorTN(s.getSubscriberNo());
		s.setAddress(address);
	}

	static public void SetCustomerExistsInMiClaro(Customer_Ext c) {
		try {
			String document = Utils.coalesce(c.getIdentityCardID(), c.getPassport());
			MiClaroRegisteredMemberVerificationInformation response = MiClaro.RegisteredMemberVerification(document);
			boolean exists = false;
			List<JAXBElement> rests = response.getRest();
			for (int i = 0; i < rests.size(); i++) {
				JAXBElement rest = rests.get(i);
				if ("exists".equals(rest.getName().getLocalPart())) {
					exists = (Boolean) rest.getValue();
				}
			}
			if (response.isError()) {
				c.setHasMiClaro("error");
			} else {
				if (exists) {
					c.setHasMiClaro("true");
				} else {
					c.setHasMiClaro("false");
				}
			}
		} catch (Exception ex) {
		}
	}
}
