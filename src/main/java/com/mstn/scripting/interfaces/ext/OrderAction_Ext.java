/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.ordering.Creator;
import _do.com.claro.soa.model.ordering.OrderAction;
import _do.com.claro.soa.model.ordering.OrderActionID;
import _do.com.claro.soa.model.ordering.OrderActivities;
import _do.com.claro.soa.model.ordering.OrderActivity;
import _do.com.claro.soa.model.ordering.OrderID;
import _do.com.claro.soa.model.ordering.Phase;
import _do.com.claro.soa.model.ordering.Phases;
import _do.com.claro.soa.model.ordering.Pricing;
import _do.com.claro.soa.model.ordering.SalesChannel;
import _do.com.claro.soa.model.product.Product;
import com.mstn.scripting.core.DATE;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author amatos
 */
public class OrderAction_Ext {

	private OrderAction original;
	private String actionDisplayID;
	private String actionUid;
	private String orderDisplayID;
	private String orderUid;
	private String parentUID;
	private String action;
	private String creatorID;
	private String creationDate;
	private String dueDate;
	private String effectiveDate;
	private String serviceRequiredDate;
	private String status;
	private String statusLabel;
	private String channelName;
	private String channelCode;
	private String reasonCode;
	private String reasonDescription;
	private Product product;
	private List<Phase> phases;
	private List<OrderActivity> activities;
	private List<Pricing.PricingElement> pricing;

	public OrderAction_Ext(OrderAction base) {
		if (base == null) {
			return;
		}
		this.original = base;
		OrderActionID oaID = base.getOrderActionID();
		if (oaID != null) {
			this.actionDisplayID = oaID.getDisplayID();
			this.actionUid = oaID.getUid();
		}
		OrderID oID = base.getOrderID();
		if (oID != null) {
			this.orderDisplayID = oID.getDisplayID();
			this.orderUid = oID.getUid();
		}
		this.parentUID = base.getParentUID();
		if (base.getAction() != null) {
			this.action = base.getAction().name();
		}
		Creator creator = base.getCreator();
		if (creator != null) {
			this.creatorID = creator.getEmployeeId();
			this.creationDate = DATE.toString(creator.getCreationDate());
		}
		this.dueDate = DATE.toString(base.getDueDate());
		this.effectiveDate = DATE.toString(base.getEffectiveDate());
		this.serviceRequiredDate = DATE.toString(base.getServiceRequiredDate());
		if (base.getStatus() != null) {
			this.status = base.getStatus().name();
			switch (base.getStatus()) {
				case AMENDED:
					this.statusLabel = "Corregida";
					break;
				case BEING_AMENDED:
					this.statusLabel = "Enviada para corregir";
					break;
				case CANCELLED:
					this.statusLabel = "Cancelada";
					break;
				case COMPLETION:
					this.statusLabel = "Terminado";
					break;
				case DELIVERY:
					this.statusLabel = "Entrega";
					break;
				case DISCONTINUED:
					this.statusLabel = "Interrumpida";
					break;
				case DONE:
					this.statusLabel = "Finalizada";
					break;
				case FICTITIOUS:
					this.statusLabel = "Ficticio";
					break;
				case FUTURE:
					this.statusLabel = "Futura";
					break;
				case INITIAL:
					this.statusLabel = "Iniciada";
					break;
				case NEGOTIATION:
					this.statusLabel = "Negociación";
					break;
				case ON_HOLD:
					this.statusLabel = "En espera";
					break;
				case REJECT:
					this.statusLabel = "Rechazada";
					break;
				case TO_BE_CANCELLED:
					this.statusLabel = "Para cancelar";
					break;
				default:
					this.statusLabel = base.getStatus().value();
					break;
			}
		}
		SalesChannel channel = base.getSalesChannel();
		if (channel != null) {
			this.channelName = channel.getName();
			this.channelCode = channel.getCode();
		}
		OrderAction.Reason reason = base.getReason();
		if (reason != null) {
			this.reasonCode = reason.getCode();
			this.reasonDescription = reason.getDescription();
		}
		OrderAction.ProductSnapshot productSnap = base.getProductSnapshot();
		if (productSnap != null) {
			this.product = productSnap.getProduct();
		}
		Phases bPhases = base.getPhases();
		if (bPhases != null) {
			this.phases = bPhases.getPhase().stream()
					.map(phase -> new Phase_Ext(phase))
					.collect(Collectors.toList());
		}
		OrderActivities oActivities = base.getOrderActivities();
		if (oActivities != null) {
			this.activities = oActivities.getOrderActivity().stream()
					.map(item -> new OrderActivity_Ext(item))
					.collect(Collectors.toList());
		}
		Pricing bPsricing = base.getPricing();
		if (bPsricing != null) {
			this.pricing = bPsricing.getPricingElement();
		}
	}

	public OrderAction getOriginal() {
		return original;
	}

	public void setOriginal(OrderAction original) {
		this.original = original;
	}

	public String getActionDisplayID() {
		return actionDisplayID;
	}

	public void setActionDisplayID(String actionDisplayID) {
		this.actionDisplayID = actionDisplayID;
	}

	public String getActionUid() {
		return actionUid;
	}

	public void setActionUid(String actionUid) {
		this.actionUid = actionUid;
	}

	public String getOrderDisplayID() {
		return orderDisplayID;
	}

	public void setOrderDisplayID(String orderDisplayID) {
		this.orderDisplayID = orderDisplayID;
	}

	public String getOrderUid() {
		return orderUid;
	}

	public void setOrderUid(String orderUid) {
		this.orderUid = orderUid;
	}

	public String getParentUID() {
		return parentUID;
	}

	public void setParentUID(String parentUID) {
		this.parentUID = parentUID;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCreatorID() {
		return creatorID;
	}

	public void setCreatorID(String creatorID) {
		this.creatorID = creatorID;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getServiceRequiredDate() {
		return serviceRequiredDate;
	}

	public void setServiceRequiredDate(String serviceRequiredDate) {
		this.serviceRequiredDate = serviceRequiredDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusLabel() {
		return statusLabel;
	}

	public void setStatusLabel(String statusLabel) {
		this.statusLabel = statusLabel;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReasonDescription() {
		return reasonDescription;
	}

	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<Phase> getPhases() {
		return phases;
	}

	public void setPhases(List<Phase> phases) {
		this.phases = phases;
	}

	public List<OrderActivity> getActivities() {
		return activities;
	}

	public void setActivities(List<OrderActivity> activities) {
		this.activities = activities;
	}

	public List<Pricing.PricingElement> getPricing() {
		return pricing;
	}

	public void setPricing(List<Pricing.PricingElement> pricing) {
		this.pricing = pricing;
	}

}
