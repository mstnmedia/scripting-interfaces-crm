/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.ordering.Phase;
import _do.com.claro.soa.model.ordering.PhaseName;

/**
 *
 * @author amatos
 */
public class Phase_Ext extends Phase {

	protected String nameLabel;

	public Phase_Ext() {
	}

	public Phase_Ext(Phase base) {
		this.name = base.getName();
		switch (this.name) {
			case CLOSED:
				this.nameLabel = "Cerrado";
				break;
			case COMPLETION:
				this.nameLabel = "Completado";
				break;
			case DELIVERY:
				this.nameLabel = "Entrega";
				break;
			case DELIVERY_IN_PROGRESS:
				this.nameLabel = "Entrega en progreso";
				break;
			case DELIVERY_NOT_OK:
				this.nameLabel = "Entrega no completada";
				break;
			case DELIVERY_OK:
				this.nameLabel = "Entrega completada";
				break;
			case NEGOTIATION:
				this.nameLabel = "Gestionando";
				break;
			case REQUEST_DELIVERY_SENT:
				this.nameLabel = "Solicitud entrega enviada";
				break;
			default:
				this.nameLabel = name.value();
				break;
		}
		this.startDate = base.getStartDate();
		this.endDate = base.getEndDate();
	}

	public void setNameLabel(PhaseName name) {
	}

	public String getNameLabel() {
		return nameLabel;
	}

	public void setNameLabel(String nameLabel) {
		this.nameLabel = nameLabel;
	}

}
