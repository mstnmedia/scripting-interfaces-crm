/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.customer.CustomerID;
import _do.com.claro.soa.model.employee.Employee;
import _do.com.claro.soa.model.generic.TelephoneNumber;
import _do.com.claro.soa.model.personidentity.Names;

/**
 *
 * @author amatos
 */
public class Utils {

	static public void logException(String method, Exception ex) {
		System.err.println(method + ": ");
		System.err.println(ex);
		System.err.println(ex.getLocalizedMessage());
		System.err.println();
	}

	static public <T> T coalesce(T... values) {
		return com.mstn.scripting.core.Utils.coalesce(values);
	}

	static public String replaceNonDigit(String s) {
		return s.replaceAll("[^\\d]", "");
	}

	static public String phoneToString(TelephoneNumber phone) {
		return new TelephoneNumber_Ext(phone).toString();
	}

	static public String customerIDToString(CustomerID customerID) {
		return new CustomerID_Ext(customerID).toString();
	}

	static boolean stringIsNullOrEmpty(String value) {
		return value == null || value.isEmpty();
	}

	static boolean stringNonNullOrEmpty(String value) {
		return value != null && !value.isEmpty();
	}

	static public String namesToString(Names names) {
		String name = null;
		if (names != null) {
			name = Utils.coalesce(names.getFirstName(), "");
			if (!stringIsNullOrEmpty(names.getFirstSurname())) {
				name += " " + names.getFirstSurname();
			}
			if (!stringIsNullOrEmpty(names.getSecondName())) {
				name += " " + names.getSecondName();
			}
			if (!stringIsNullOrEmpty(names.getSecondSurname())) {
				name += " " + names.getSecondSurname();
			}
		}
		return name;
	}

	static public String getEmployeeName(Employee employee) {
		String name = namesToString(employee.getNames());
		return name;
	}

}
