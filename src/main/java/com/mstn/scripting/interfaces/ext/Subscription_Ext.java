package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.billing.Bill;
import _do.com.claro.soa.model.billing.BillingPlan;
import _do.com.claro.soa.model.billing.FutureBillingPlan;
import _do.com.claro.soa.model.billing.Subscription;
import _do.com.claro.soa.model.product.Component;
import _do.com.claro.soa.model.product.ComponentStatus;
import _do.com.claro.soa.model.product.ComponentStatusName;
import _do.com.claro.soa.model.product.Components;
import _do.com.claro.soa.model.product.Offer;
import _do.com.claro.soa.model.product.Product;
import _do.com.claro.soa.model.product.ProductAttribute;
import _do.com.claro.soa.model.product.ProductAttributes;
import _do.com.claro.soa.model.product.ProductType;
import _do.com.claro.soa.services.billing.getfixedpostpaidrollover.GetResponse;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.models.Transaction_Result;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Subscription_Ext {

	static final String SEPARATOR = ".";
	static final List<String> CPE = Arrays.asList("E", "BSS");
	static final List<String> DATA = Arrays.asList("B", "WEBH", "IFPM", "BBMC", "BKHL", "WBMC", "VSMC", "CVIR", "DCMC", "VPN", "LIDE", "LDIN", "ISAT", "INTD", "INTB", "FNET", "FOMC", "FRMC", "IFPW", "WCON");
	static final List<String> FICTICIO = Arrays.asList("X", "ACLI");
	static final List<String> INTERNET_FIJO = Arrays.asList("D", "U", "DUPA", "DUM", "ADSL");
	static final List<String> TELEFONO_FIJO = Arrays.asList("F", "T1NE", "T1PA", "WILL", "PUBA", "ISDN", "DID", "DLLD", "LICO", "T1PR", "OPXP", "PSCF", "PSTN");
	static final List<String> VOZ_SOBRE_IP = Arrays.asList("V", "VOIP", "ITPA", "VSIP", "CVF", "IPPR", "HPBX", "DIIP");
	static final List<String> INALAMBRICO_FIJO = Arrays.asList("BAR", "SIF", "SIFPREP");
	static final List<String> INTERNET_MOVIL = Arrays.asList("DATOPREP_N", "MIXTODATOS", "PCRF_MDM");
	static final List<String> MOVIL = Arrays.asList("365", "BLACKBERRY", "CALLPOINT", "DATOS_ILM", "DATOS", "JOVEN", "MOBILE", "PRECONTROL", "REGULAR", "RENTAVANZA", "DATOPREP_N", "BAR", "SIF", "SIFPREP", "KID", "MIXTO", "G", "C", "SOPORTE", "PCRF_N_P");
	static final List<String> BEEPER = Arrays.asList("P");
	static final List<String> CLARO_TV = Arrays.asList("I", "DTH", "IPTV");
	static final List<String> RECARGAS = Arrays.asList("CARGAFACIL", "CARGAFACILDATOS");

	protected Subscription original;
	protected String id;
	protected String customerID;
	protected String taxID;
	protected String subscriberNo;
	protected String subscriberNoDash;
	protected String subscriptionStartDate;
	protected String status;
	protected String statusLastUpdate;
	protected String billingAccountNo;
	protected String billingPlanSOC;
	protected String billingPlanDescription;
	protected String futureBillingPlanSOC;
	protected String futureBillingPlanDescription;
	protected String futureBillingPlanEffectiveDate;
	protected String productTypeName;
	protected String productTypeCode;
	protected String productTypeDescription;
	protected String offerName;
	protected String instalationDate;
	protected String address;
	protected String products;

	private String productPlan;
	private String extensionsNumber;
	private String equiposTelefonicos;
	private String productStatusName;
	private String productStatusDate;
	private String commitmentPeriod;
	private String modemType;
	private String bandWidth;
	private String emailQuantity;
	private String emailAccounts;
	private String emailAssigned;
	private String sateliteName;
	private String tnClaroTVAMigrar;
	private String stbQuantity;
	private String stbModels;
	private String contractedPackages;
	private String optionalServices;

	private String billBalanceAmount;
	private String billCloseDay;
	private String billType;
	private String billsAverageAmount;
	private String billConfirmationStatus;
	private String billCreditsOrAjustAppliedAmount;
	private String billCurrentChargesOrCreditsAmount;
	private String billCycleCode;
	private String billCycleRunMonth;
	private String billCycleRunYear;
	private String billEmail;
	private String billPastDueAmount;
	private String billPaymentsReceivedAmount;
	private String billPreviousBalanceAmount;
	private String billSequenceNumber;
	private String billTotalDueAmount;
	private String billCloseDate;
	private String billDate;
	private String billCoverageEndDate;
	private String billCoverageStartDate;
	private String billDueDate;
	private String includedMinutes;

	public Subscription_Ext(Subscription base) {
		if (base == null) {
			return;
		}
		this.original = base;
		this.customerID = Utils.customerIDToString(base.getCustomerID());
		this.taxID = base.getTaxID();
		this.subscriberNo = Utils.phoneToString(base.getSubscriberNo());
		this.subscriberNoDash = TelephoneNumber_Ext.addDashes(this.subscriberNo);
		this.subscriptionStartDate = DATE.toString(base.getSubscriptionStartDate());
		if (base.getStatus() != null) {
			this.status = base.getStatus().name();
		}
		this.statusLastUpdate = DATE.toString(base.getStatusLastUpdate());
		this.billingAccountNo = base.getBillingAccountNo();
		BillingPlan billingPlan = base.getBillingPlan();
		if (billingPlan != null) {
			this.billingPlanSOC = billingPlan.getSOC();
			this.billingPlanDescription = billingPlan.getDescription();
		}
		FutureBillingPlan futureBillingPlan = base.getFutureBillingPlan();
		if (futureBillingPlan != null) {
			this.futureBillingPlanSOC = futureBillingPlan.getSoc();
			this.futureBillingPlanDescription = futureBillingPlan.getDescription();
			this.futureBillingPlanEffectiveDate = DATE.toString(futureBillingPlan.getEffectiveDate());
		}
		Product product = base.getProduct();
		if (product != null) {
			ProductType productType = product.getProductType();
			if (productType != null) {
				this.productTypeCode = productType.getProductCode();
				this.productTypeName = getProductTypeNameFromCode(productTypeCode);
				this.productTypeDescription = productType.getDescription();
			}
			Offer offer = product.getOffer();
			if (offer != null) {
				this.offerName = offer.getName();
			}
			this.instalationDate = DATE.toString(product.getInstallationDate());
//			Address addressObj = product.getAddress();
//			if (addressObj != null) {
//				this.address = addressObj.getAddressDetails()..getAddressDetails().getNeighborhood();
//			}
			Component component = product.getComponent();
			if (component != null) {
				ComponentStatus componentStatus = component.getStatus();
				if (componentStatus != null) {
					this.productStatusName = componentStatus.getStatusName().value();
					this.productStatusDate = DATE.toString(componentStatus.getStartDate());
				}
//				CatalogDefinition catalogDefinition = component.getCatalogDefinition();
//				Component.ComponentType componentType = component.getComponentType();

				Components componentsObj = component.getComponents();
				List<Component> components = componentsObj == null
						? new ArrayList()
						: componentsObj.getComponent();

				ProductAttributes productAttributesObj = component.getProductAttributes();
				List<ProductAttribute> productAttributes = productAttributesObj == null
						? new ArrayList()
						: productAttributesObj.getProductAttribute();

				getProductDetailsByType(productTypeCode, components, productAttributes);
			}
		}
		if (billingAccountNo != null) {
			this.id = billingAccountNo + SEPARATOR + subscriberNo + SEPARATOR + productTypeCode;
		} else {
			this.id = customerID + SEPARATOR + subscriberNo + SEPARATOR + productTypeCode;
		}
	}

	final void sortComponents(List<Component> components) {
		components.sort((Component a, Component b) -> {
			int aO = 1;
			int bO = -1;
			if (a.getStatus().getStatusName() == b.getStatus().getStatusName()) {
				return 0;
			} else {
				if (a.getStatus().getStatusName() == ComponentStatusName.ACTIVE) {
					return aO;
				} else if (b.getStatus().getStatusName() == ComponentStatusName.ACTIVE) {
					return bO;
				} else if (b.getStatus().getStatusName() == ComponentStatusName.SUSPENDED) {
					return bO;
				}
			}
			return aO;
		});
	}

	static public List<Transaction_Result> getResults(Subscription_Ext subscription) throws Exception {
		boolean empty = subscription == null;

		String original = empty ? "" : JSON.toString(subscription.getOriginal());
		String id = empty ? "" : subscription.getId();
		String customerID = empty ? "" : subscription.getCustomerID();
		String taxID = empty ? "" : subscription.getTaxID();
		String subscriberNo = empty ? "" : subscription.getSubscriberNo();
		String subscriberNoDash = empty ? "" : subscription.getSubscriberNoDash();
		String subscriptionStartDate = empty ? "" : subscription.getSubscriptionStartDate();
		String status = empty ? "" : subscription.getStatus();
		String billingAccountNo = empty ? "" : subscription.getBillingAccountNo();
		String billingPlanSOC = empty ? "" : subscription.getBillingPlanSOC();
		String billingPlanDescription = empty ? "" : subscription.getBillingPlanDescription();
		String futureBillingPlanSOC = empty ? "" : subscription.getFutureBillingPlanSOC();
		String futureBillingPlanDescription = empty ? "" : subscription.getFutureBillingPlanDescription();
		String futureBillingPlanEffectiveDate = empty ? "" : subscription.getFutureBillingPlanEffectiveDate();
		String productTypeCode = empty ? "" : subscription.getProductTypeCode();
		String productTypeName = empty ? "" : subscription.getProductTypeName();
		String productTypeDescription = empty ? "" : subscription.getProductTypeDescription();
		String offerName = empty ? "" : subscription.getOfferName();
		String instalationDate = empty ? "" : subscription.getInstalationDate();
		String address = empty ? "" : subscription.getAddress();
		String products = empty ? "" : subscription.getProducts();

		String productPlan = empty ? "" : subscription.getProductPlan();
		String extensionsNumber = empty ? "" : subscription.getExtensionsNumber();
		String equiposTelefonicos = empty ? "" : subscription.getEquiposTelefonicos();
		String productStatusName = empty ? "" : subscription.getProductStatusName();
		String productStatusDate = empty ? "" : subscription.getProductStatusDate();
		String commitmentPeriod = empty ? "" : subscription.getCommitmentPeriod();
		String modemType = empty ? "" : subscription.getModemType();
		String bandWidth = empty ? "" : subscription.getBandWidth();
		String emailQuantity = empty ? "" : subscription.getEmailQuantity();
		String emailAccounts = empty ? "" : subscription.getEmailAccounts();
		String emailAssigned = empty ? "" : subscription.getEmailAssigned();
		String sateliteName = empty ? "" : subscription.getSateliteName();
		String tnClaroTVAMigrar = empty ? "" : subscription.getTnClaroTVAMigrar();
		String stbQuantity = empty ? "" : subscription.getStbQuantity();
		String stbModels = empty ? "" : subscription.getStbModels();
		String contractedPackages = empty ? "" : subscription.getContractedPackages();
		String optionalServices = empty ? "" : subscription.getOptionalServices();

		String billBalanceAmount = empty ? "" : subscription.getBillBalanceAmount();
		String billCloseDay = empty ? "" : subscription.getBillCloseDay();
		String billType = empty ? "" : subscription.getBillType();
		String billsAverageAmount = empty ? "" : subscription.getBillsAverageAmount();
		String billConfirmationStatus = empty ? "" : subscription.getBillConfirmationStatus();
		String billCreditsOrAjustAppliedAmount = empty ? "" : subscription.getBillCreditsOrAjustAppliedAmount();
		String billCurrentChargesOrCreditsAmount = empty ? "" : subscription.getBillCurrentChargesOrCreditsAmount();
		String billCycleCode = empty ? "" : subscription.getBillCycleCode();
		String billCycleRunMonth = empty ? "" : subscription.getBillCycleRunMonth();
		String billCycleRunYear = empty ? "" : subscription.getBillCycleRunYear();
		String billEmail = empty ? "" : subscription.getBillEmail();
		String billPastDueAmount = empty ? "" : subscription.getBillPastDueAmount();
		String billPaymentsReceivedAmount = empty ? "" : subscription.getBillPaymentsReceivedAmount();
		String billPreviousBalanceAmount = empty ? "" : subscription.getBillPreviousBalanceAmount();
		String billSequenceNumber = empty ? "" : subscription.getBillSequenceNumber();
		String billTotalDueAmount = empty ? "" : subscription.getBillTotalDueAmount();
		String billCloseDate = empty ? "" : subscription.getBillCloseDate();
		String billDate = empty ? "" : subscription.getBillDate();
		String billCoverageEndDate = empty ? "" : subscription.getBillCoverageEndDate();
		String billCoverageStartDate = empty ? "" : subscription.getBillCoverageStartDate();
		String billDueDate = empty ? "" : subscription.getBillDueDate();
		String includedMinutes = empty ? "" : subscription.getIncludedMinutes();

		int ID = Interfaces.SUBSCRIPTION_INFO_INTERFACE_ID_BASE;
		String prefix = Interfaces.SUBSCRIPTION_INFO_INTERFACE_NAME_BASE + "_";
		return Arrays.asList( //(id_interface, name, label)/
				//(id, id_interface, name, label, value, props)
				new Transaction_Result(999, ID, prefix + "original", "Original", original, "{\"notSave\": true}"),
				new Transaction_Result(0, ID, prefix + "id", "ID del servicio", id, ""),
				new Transaction_Result(0, ID, prefix + "customerID", "ID del cliente", customerID, ""),
				new Transaction_Result(0, ID, prefix + "taxID", "RNC", taxID, ""),
				new Transaction_Result(0, ID, prefix + "subscriberNo", "Teléfono", subscriberNo, ""),
				new Transaction_Result(0, ID, prefix + "subscriberNoDash", "Teléfono con guiones", subscriberNoDash, ""),
				new Transaction_Result(0, ID, prefix + "subscriptionStartDate", "Fecha de Subscripción", subscriptionStartDate, ""),
				new Transaction_Result(0, ID, prefix + "status", "Estado", status, ""),
				new Transaction_Result(0, ID, prefix + "billingAccountNo", "Número de cuenta", billingAccountNo, ""),
				new Transaction_Result(0, ID, prefix + "billingPlanSOC", "Código del plan", billingPlanSOC, ""),
				new Transaction_Result(0, ID, prefix + "billingPlanDescription", "Descripción del plan", billingPlanDescription, ""),
				new Transaction_Result(0, ID, prefix + "futureBillingPlanSOC", "Código del plan futuro", futureBillingPlanSOC, ""),
				new Transaction_Result(0, ID, prefix + "futureBillingPlanDescription", "Descripción del plan futuro", futureBillingPlanDescription, ""),
				new Transaction_Result(0, ID, prefix + "futureBillingPlanEffectiveDate", "Fecha de efectividad", futureBillingPlanEffectiveDate, ""),
				new Transaction_Result(0, ID, prefix + "productTypeCode", "Código del tipo de producto", productTypeCode, ""),
				new Transaction_Result(0, ID, prefix + "productTypeName", "Nombre del tipo de producto", productTypeName, ""),
				new Transaction_Result(0, ID, prefix + "productTypeDescription", "Descripción del tipo de producto", productTypeDescription, ""),
				new Transaction_Result(0, ID, prefix + "offerName", "Cartera", offerName, ""),
				new Transaction_Result(0, ID, prefix + "instalationDate", "Fecha de instalación", instalationDate, ""),
				new Transaction_Result(0, ID, prefix + "address", "Dirección", address, ""),
				new Transaction_Result(0, ID, prefix + "products", "Productos", products, ""),
				new Transaction_Result(0, ID, prefix + "productPlan", "Plan del cliente", productPlan, ""),
				new Transaction_Result(0, ID, prefix + "extensionsNumber", "No. de extensiones", extensionsNumber, ""),
				new Transaction_Result(0, ID, prefix + "equiposTelefonicos", "Equipo telefónico", equiposTelefonicos, ""),
				new Transaction_Result(0, ID, prefix + "productStatusName", "Estado del producto", productStatusName, ""),
				new Transaction_Result(0, ID, prefix + "productStatusDate", "Fecha de estado del producto", productStatusDate, ""),
				new Transaction_Result(0, ID, prefix + "commitmentPeriod", "Periodo del contrato", commitmentPeriod, ""),
				new Transaction_Result(0, ID, prefix + "modemType", "Tipo de módem", modemType, ""),
				new Transaction_Result(0, ID, prefix + "bandWidth", "Ancho de banda", bandWidth, ""),
				new Transaction_Result(0, ID, prefix + "emailQuantity", "Cantidad de email", emailQuantity, ""),
				new Transaction_Result(0, ID, prefix + "emailAccounts", "Cuentas email", emailAccounts, ""),
				new Transaction_Result(0, ID, prefix + "emailAssigned", "Correo asignado", emailAssigned, ""),
				new Transaction_Result(0, ID, prefix + "sateliteName", "Nombre de satélite", sateliteName, ""),
				new Transaction_Result(0, ID, prefix + "tnClaroTVAMigrar", "TN Claro TV a migrar", tnClaroTVAMigrar, ""),
				new Transaction_Result(0, ID, prefix + "stbQuantity", "Cantidad de cajas STB", stbQuantity, ""),
				new Transaction_Result(0, ID, prefix + "stbModels", "Modelos de las STB", stbModels, ""),
				new Transaction_Result(0, ID, prefix + "contractedPackages", "Paquetes contratados", contractedPackages, ""),
				new Transaction_Result(0, ID, prefix + "optionalServices", "Servicios opcionales", optionalServices, ""),
				new Transaction_Result(0, ID, prefix + "billBalanceAmount", "billBalanceAmount", billBalanceAmount, ""),
				new Transaction_Result(0, ID, prefix + "billCloseDay", "billCloseDay", billCloseDay, ""),
				new Transaction_Result(0, ID, prefix + "billType", "billType", billType, ""),
				new Transaction_Result(0, ID, prefix + "billsAverageAmount", "billsAverageAmount", billsAverageAmount, ""),
				new Transaction_Result(0, ID, prefix + "billConfirmationStatus", "billConfirmationStatus", billConfirmationStatus, ""),
				new Transaction_Result(0, ID, prefix + "billCreditsOrAjustAppliedAmount", "billCreditsOrAjustAppliedAmount", billCreditsOrAjustAppliedAmount, ""),
				new Transaction_Result(0, ID, prefix + "billCurrentChargesOrCreditsAmount", "billCurrentChargesOrCreditsAmount", billCurrentChargesOrCreditsAmount, ""),
				new Transaction_Result(0, ID, prefix + "billCycleCode", "billCycleCode", billCycleCode, ""),
				new Transaction_Result(0, ID, prefix + "billCycleRunMonth", "billCycleRunMonth", billCycleRunMonth, ""),
				new Transaction_Result(0, ID, prefix + "billCycleRunYear", "billCycleRunYear", billCycleRunYear, ""),
				new Transaction_Result(0, ID, prefix + "billEmail", "billEmail", billEmail, ""),
				new Transaction_Result(0, ID, prefix + "billPastDueAmount", "billPastDueAmount", billPastDueAmount, ""),
				new Transaction_Result(0, ID, prefix + "billPaymentsReceivedAmount", "billPaymentsReceivedAmount", billPaymentsReceivedAmount, ""),
				new Transaction_Result(0, ID, prefix + "billPreviousBalanceAmount", "billPreviousBalanceAmount", billPreviousBalanceAmount, ""),
				new Transaction_Result(0, ID, prefix + "billSequenceNumber", "billSequenceNumber", billSequenceNumber, ""),
				new Transaction_Result(0, ID, prefix + "billTotalDueAmount", "billTotalDueAmount", billTotalDueAmount, ""),
				new Transaction_Result(0, ID, prefix + "billCloseDate", "billCloseDate", billCloseDate, ""),
				new Transaction_Result(0, ID, prefix + "billDate", "billDate", billDate, ""),
				new Transaction_Result(0, ID, prefix + "billCoverageEndDate", "billCoverageEndDate", billCoverageEndDate, ""),
				new Transaction_Result(0, ID, prefix + "billCoverageStartDate", "billCoverageStartDate", billCoverageStartDate, ""),
				new Transaction_Result(0, ID, prefix + "billDueDate", "billDueDate", billDueDate, ""),
				new Transaction_Result(0, ID, prefix + "includedMinutes", "Minutos incluidos", includedMinutes, "")
		);
	}

	public static String getProductTypeNameFromCode(String productTypeCode) {
		if (CPE.contains(productTypeCode)) {
			return "CPE";
		} else if (DATA.contains(productTypeCode)) {
			return "Data";
		} else if (FICTICIO.contains(productTypeCode)) {
			return "Ficticio";
		} else if (INTERNET_FIJO.contains(productTypeCode)) {
			return "Internet Fijo";
		} else if (TELEFONO_FIJO.contains(productTypeCode)) {
			return "Teléfono Fijo";
		} else if (VOZ_SOBRE_IP.contains(productTypeCode)) {
			return "Voz sobre IP";
		} else if (INALAMBRICO_FIJO.contains(productTypeCode)) {
			return "Inalámbrico Fijo";
		} else if (INTERNET_MOVIL.contains(productTypeCode)) {
			return "Internet Móvil";
		} else if (MOVIL.contains(productTypeCode)) {
			return "Móvil";
		} else if (BEEPER.contains(productTypeCode)) {
			return "Beeper";
		} else if (CLARO_TV.contains(productTypeCode)) {
			return "Claro TV";
		} else if (RECARGAS.contains(productTypeCode)) {
			return "Recargas";
		}
		return productTypeCode;
	}

	static Component getComponentByName(String name, List<Component> components) {
		for (int i = 0; i < components.size(); i++) {
			Component component = components.get(i);
			if (name.equals(component.getCatalogDefinition().getName())) {
				return component;
			}
		}
		return null;
	}

	static ProductAttribute getAttributeByName(String name, List<ProductAttribute> attributes) {
		for (int i = 0; i < attributes.size(); i++) {
			ProductAttribute attribute = attributes.get(i);
			if (name.equals(attribute.getName())) {
				return attribute;
			}
		}
		return null;
	}

	final void getProductDetailsByType(String productType, List<Component> components, List<ProductAttribute> productAttributes) {
		sortComponents(components);
		switch (productType) {
			case "ADSL":
				Component componentADSL = getComponentByName("Planes de Internet Flash", components);
				if (componentADSL != null) {
					this.productPlan = componentADSL.getCatalogDefinition().getName();
					Components subComponentsObj = componentADSL.getComponents();
					if (subComponentsObj != null) {
						List<Component> subComponents = subComponentsObj.getComponent();
						if (!subComponents.isEmpty()) {
							ProductAttributes subAttributesObj = subComponents.get(0).getProductAttributes();
							List<ProductAttribute> subAttributes = subAttributesObj == null
									? new ArrayList()
									: subAttributesObj.getProductAttribute();
							if (!subAttributes.isEmpty()) {
								ProductAttribute modemTypeObj = getAttributeByName("ADSL_EQUIPMENT_INCLUDED", subAttributes);
								this.modemType = modemTypeObj == null ? "" : modemTypeObj.getValueDesc();
								ProductAttribute bandWidthObj = getAttributeByName("BANDWIDTH", subAttributes);
								this.bandWidth = bandWidthObj == null ? "" : bandWidthObj.getValueDesc();
								ProductAttribute emailQuantityObj = getAttributeByName("MEGABYTE_EMAIL_SIZE", subAttributes);
								this.emailQuantity = emailQuantityObj == null ? "" : emailQuantityObj.getValueDesc();
								ProductAttribute emailAccountsObj = getAttributeByName("GENERAL_ATRIBUTO_EMAIL_ACCOUNT", subAttributes);
								this.emailAccounts = emailAccountsObj == null ? "" : emailAccountsObj.getValueDesc();
								ProductAttribute emailAssignedObj = getAttributeByName("GENERAL_ATRIBUTO_LAST_ALLOCATED_EMAIL_COD", subAttributes);
								this.emailAssigned = emailAssignedObj == null ? "" : emailAssignedObj.getValueDesc();
							}
						}
					}
				}
				List<String> optionalServicesADSL = components.stream()
						.filter(item -> {
							return Arrays.asList("ASCC", "ASCT", "BMUL", "FNPC", "MAIL", "MNTL", "PSCF", "VOCF")
									.contains(item.getComponentType().getCode());
						})
						.map(item -> item.getCatalogDefinition().getName())
						.collect(Collectors.toList());
				this.optionalServices = String.join("\n", optionalServicesADSL);
				break;
			case "PSTN":
				getProductPlan(Arrays.asList("PSTN Plans", "PSTN VoIP"), components);
				ProductAttribute extensionsNumberObj = getAttributeByName("extensionsNumber", productAttributes);
				this.extensionsNumber = extensionsNumberObj == null ? "" : extensionsNumberObj.getValueDesc();
				ProductAttribute equiposTelefonicosObj = getAttributeByName("Equipos Telefonicos", productAttributes);
				this.equiposTelefonicos = equiposTelefonicosObj == null ? "" : equiposTelefonicosObj.getValueDesc();
				break;
			case "IPTV":
				getProductPlan(Arrays.asList("Planes Claro TV"), components);
				Optional<Component> componentSTBG = components.stream().filter(item -> {
					return "Set-Top-Box".equals(item.getCatalogDefinition().getName())
							&& "STBG".equals(item.getComponentType().getCode());
				}).findFirst();
				if (componentSTBG.isPresent()) {
					Components subComponentsObj = componentSTBG.get().getComponents();
					List<Component> subComponents = subComponentsObj == null ? new ArrayList() : subComponentsObj.getComponent();
					if (!subComponents.isEmpty()) {
						this.stbQuantity = String.valueOf(subComponents.size());
						List<String> models = subComponents.stream()
								.map(item -> item.getCatalogDefinition().getName())
								.collect(Collectors.toList());
						this.stbModels = String.join("\n", models);
					}
				}
				break;
			case "DTH":
				getProductPlan(Arrays.asList("Planes DTH"), components);
				Component equiposDTH = getComponentByName("Equipos DTH", components);
				if (equiposDTH != null && equiposDTH.getComponents() != null) {
					List<Component> subComponents = equiposDTH.getComponents().getComponent();
					this.stbQuantity = String.valueOf(
							subComponents.stream().filter(item -> {
								if (item.getProductAttributes() != null /* && item.getStatus() != null
										&& item.getStatus().getStatusName() == ComponentStatusName.ACTIVE*/) {
									List<ProductAttribute> itemAttributes = item.getProductAttributes().getProductAttribute();
									ProductAttribute tipoCaja = getAttributeByName("TIPO_CAJA", itemAttributes);
									return tipoCaja != null;
								}
								return false;
							}).count()
					);
				}
				ProductAttribute tnClaroTVAMigrarObj = getAttributeByName("TN_CLAROTV_A_MIGRAR", productAttributes);
				this.tnClaroTVAMigrar = tnClaroTVAMigrarObj == null ? "" : tnClaroTVAMigrarObj.getValueDesc();
				ProductAttribute sateliteNameObj = getAttributeByName("SATELITE_NAME", productAttributes);
				this.sateliteName = sateliteNameObj == null ? "" : sateliteNameObj.getValueDesc();
				break;
			default:
				break;
		}
		if ("DTH".equals(productType) || "IPTV".equals(productType)) {
			List<String> packages = components.stream()
					.filter(item -> {
						return Arrays.asList("Combos Premium", "Canales Premium", "Paquetes Premium")
								.contains(item.getCatalogDefinition().getName());
					})
					.map(item -> {
						Components subComponentsObj = item.getComponents();
						List<Component> subComponents = subComponentsObj == null ? new ArrayList<>() : subComponentsObj.getComponent();
						return subComponents.stream()
								.map(j -> j.getCatalogDefinition().getName())
								.collect(Collectors.toList());
					})
					.flatMap(List::stream)
					.collect(Collectors.toList());
			this.contractedPackages = String.join("\n", packages);
		}
		ProductAttribute commitmentPeriodObj = getAttributeByName("COMMITMENT_PERIOD", productAttributes);
		this.commitmentPeriod = commitmentPeriodObj == null ? "" : commitmentPeriodObj.getValueDesc();
	}

	final void getProductPlan(List<String> catalogs, List<Component> components) {
		for (int i = 0; i < components.size(); i++) {
			Component component = components.get(i);
			String componentCatalog = component.getCatalogDefinition().getName();
			for (int j = 0; j < catalogs.size(); j++) {
				String catalog = catalogs.get(j);
				if (componentCatalog.equals(catalog)
						&& component.getComponents() != null
						&& !component.getComponents().getComponent().isEmpty()) {
					List<Component> subComponents = component.getComponents().getComponent();
					sortComponents(subComponents);
					this.productPlan = subComponents.get(0).getCatalogDefinition().getName();
					break;
				}
			}
		}
	}

	public void setBillDetails(Bill bill) {
		if (bill != null) {
			this.billBalanceAmount = String.valueOf(bill.getBalanceAmount());
			this.billCloseDay = String.valueOf(bill.getBillCloseDay());
			if (bill.getBillType() != null) {
				this.billType = String.valueOf(bill.getBillType().value());
			}
			this.billsAverageAmount = String.valueOf(bill.getBillsAverageAmount());
			this.billConfirmationStatus = String.valueOf(bill.getConfirmationStatus());
			this.billCreditsOrAjustAppliedAmount = String.valueOf(bill.getCreditsOrAjustAppliedAmount());
			this.billCurrentChargesOrCreditsAmount = String.valueOf(bill.getCurrentChargesOrCreditsAmount());
			this.billCycleCode = String.valueOf(bill.getCycleCode());
			this.billCycleRunMonth = String.valueOf(bill.getCycleRunMonth());
			this.billCycleRunYear = String.valueOf(bill.getCycleRunYear());
			this.billEmail = String.valueOf(bill.getEmail());
			this.billPastDueAmount = String.valueOf(bill.getPastDueAmount());
			this.billPaymentsReceivedAmount = String.valueOf(bill.getPaymentsReceivedAmount());
			this.billPreviousBalanceAmount = String.valueOf(bill.getPreviousBalanceAmount());
			this.billSequenceNumber = String.valueOf(bill.getSequenceNumber());
			this.billTotalDueAmount = String.valueOf(bill.getTotalDueAmount());
			this.billCloseDate = DATE.toString(bill.getBillCloseDate());
			this.billDate = DATE.toString(bill.getBillDate());
			this.billCoverageEndDate = DATE.toString(bill.getCoverageEndDate());
			this.billCoverageStartDate = DATE.toString(bill.getCoverageStartDate());
			this.billDueDate = DATE.toString(bill.getDueDate());
		}
	}

	public void setRolloverDetails(GetResponse.Result result) {
		if (result != null) {
			this.includedMinutes = String.valueOf(result.getIncludedMinutes());
//			GetResponse.Result.Pockets pocketsObj = result.getPockets();
//			if(pocketsObj!=null){
//				List<GetResponse.Result.Pockets.Pocket> pockets = pocketsObj.getPocket();
//			}
		}
	}

	public Subscription getOriginal() {
		return original;
	}

	public void setOriginal(Subscription original) {
		this.original = original;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getTaxID() {
		return taxID;
	}

	public void setTaxID(String taxID) {
		this.taxID = taxID;
	}

	public String getSubscriberNo() {
		return subscriberNo;
	}

	public void setSubscriberNo(String subscriberNo) {
		this.subscriberNo = subscriberNo;
	}

	public String getSubscriberNoDash() {
		return subscriberNoDash;
	}

	public void setSubscriberNoDash(String subscriberNoDash) {
		this.subscriberNoDash = subscriberNoDash;
	}

	public String getSubscriptionStartDate() {
		return subscriptionStartDate;
	}

	public void setSubscriptionStartDate(String subscriptionStartDate) {
		this.subscriptionStartDate = subscriptionStartDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusLastUpdate() {
		return statusLastUpdate;
	}

	public void setStatusLastUpdate(String statusLastUpdate) {
		this.statusLastUpdate = statusLastUpdate;
	}

	public String getBillingAccountNo() {
		return billingAccountNo;
	}

	public void setBillingAccountNo(String billingAccountNo) {
		this.billingAccountNo = billingAccountNo;
	}

	public String getBillingPlanSOC() {
		return billingPlanSOC;
	}

	public void setBillingPlanSOC(String billingPlanSOC) {
		this.billingPlanSOC = billingPlanSOC;
	}

	public String getBillingPlanDescription() {
		return billingPlanDescription;
	}

	public void setBillingPlanDescription(String billingPlanDescription) {
		this.billingPlanDescription = billingPlanDescription;
	}

	public String getFutureBillingPlanSOC() {
		return futureBillingPlanSOC;
	}

	public void setFutureBillingPlanSOC(String futureBillingPlanSOC) {
		this.futureBillingPlanSOC = futureBillingPlanSOC;
	}

	public String getFutureBillingPlanDescription() {
		return futureBillingPlanDescription;
	}

	public void setFutureBillingPlanDescription(String futureBillingPlanDescription) {
		this.futureBillingPlanDescription = futureBillingPlanDescription;
	}

	public String getFutureBillingPlanEffectiveDate() {
		return futureBillingPlanEffectiveDate;
	}

	public void setFutureBillingPlanEffectiveDate(String futureBillingPlanEffectiveDate) {
		this.futureBillingPlanEffectiveDate = futureBillingPlanEffectiveDate;
	}

	public String getProductTypeName() {
		return productTypeName;
	}

	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getProductTypeDescription() {
		return productTypeDescription;
	}

	public void setProductTypeDescription(String productTypeDescription) {
		this.productTypeDescription = productTypeDescription;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getInstalationDate() {
		return instalationDate;
	}

	public void setInstalationDate(String instalationDate) {
		this.instalationDate = instalationDate;
	}

	public String getProducts() {
		return products;
	}

	public Subscription_Ext setProducts(String products) {
		this.products = products;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProductPlan() {
		return productPlan;
	}

	public void setProductPlan(String productPlan) {
		this.productPlan = productPlan;
	}

	public String getExtensionsNumber() {
		return extensionsNumber;
	}

	public void setExtensionsNumber(String extensionsNumber) {
		this.extensionsNumber = extensionsNumber;
	}

	public String getEquiposTelefonicos() {
		return equiposTelefonicos;
	}

	public void setEquiposTelefonicos(String equiposTelefonicos) {
		this.equiposTelefonicos = equiposTelefonicos;
	}

	public String getProductStatusName() {
		return productStatusName;
	}

	public void setProductStatusName(String productStatusName) {
		this.productStatusName = productStatusName;
	}

	public String getProductStatusDate() {
		return productStatusDate;
	}

	public void setProductStatusDate(String productStatusDate) {
		this.productStatusDate = productStatusDate;
	}

	public String getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(String commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getModemType() {
		return modemType;
	}

	public void setModemType(String modemType) {
		this.modemType = modemType;
	}

	public String getBandWidth() {
		return bandWidth;
	}

	public void setBandWidth(String bandWidth) {
		this.bandWidth = bandWidth;
	}

	public String getEmailQuantity() {
		return emailQuantity;
	}

	public void setEmailQuantity(String emailQuantity) {
		this.emailQuantity = emailQuantity;
	}

	public String getEmailAccounts() {
		return emailAccounts;
	}

	public void setEmailAccounts(String emailAccounts) {
		this.emailAccounts = emailAccounts;
	}

	public String getEmailAssigned() {
		return emailAssigned;
	}

	public void setEmailAssigned(String emailAssigned) {
		this.emailAssigned = emailAssigned;
	}

	public String getSateliteName() {
		return sateliteName;
	}

	public void setSateliteName(String sateliteName) {
		this.sateliteName = sateliteName;
	}

	public String getTnClaroTVAMigrar() {
		return tnClaroTVAMigrar;
	}

	public void setTnClaroTVAMigrar(String tnClaroTVAMigrar) {
		this.tnClaroTVAMigrar = tnClaroTVAMigrar;
	}

	public String getStbQuantity() {
		return stbQuantity;
	}

	public void setStbQuantity(String stbQuantity) {
		this.stbQuantity = stbQuantity;
	}

	public String getStbModels() {
		return stbModels;
	}

	public void setStbModels(String stbModels) {
		this.stbModels = stbModels;
	}

	public String getContractedPackages() {
		return contractedPackages;
	}

	public void setContractedPackages(String contractedPackages) {
		this.contractedPackages = contractedPackages;
	}

	public String getOptionalServices() {
		return optionalServices;
	}

	public void setOptionalServices(String optionalServices) {
		this.optionalServices = optionalServices;
	}

	public String getBillBalanceAmount() {
		return billBalanceAmount;
	}

	public void setBillBalanceAmount(String billBalanceAmount) {
		this.billBalanceAmount = billBalanceAmount;
	}

	public String getBillCloseDay() {
		return billCloseDay;
	}

	public void setBillCloseDay(String billCloseDay) {
		this.billCloseDay = billCloseDay;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getBillsAverageAmount() {
		return billsAverageAmount;
	}

	public void setBillsAverageAmount(String billsAverageAmount) {
		this.billsAverageAmount = billsAverageAmount;
	}

	public String getBillConfirmationStatus() {
		return billConfirmationStatus;
	}

	public void setBillConfirmationStatus(String billConfirmationStatus) {
		this.billConfirmationStatus = billConfirmationStatus;
	}

	public String getBillCreditsOrAjustAppliedAmount() {
		return billCreditsOrAjustAppliedAmount;
	}

	public void setBillCreditsOrAjustAppliedAmount(String billCreditsOrAjustAppliedAmount) {
		this.billCreditsOrAjustAppliedAmount = billCreditsOrAjustAppliedAmount;
	}

	public String getBillCurrentChargesOrCreditsAmount() {
		return billCurrentChargesOrCreditsAmount;
	}

	public void setBillCurrentChargesOrCreditsAmount(String billCurrentChargesOrCreditsAmount) {
		this.billCurrentChargesOrCreditsAmount = billCurrentChargesOrCreditsAmount;
	}

	public String getBillCycleCode() {
		return billCycleCode;
	}

	public void setBillCycleCode(String billCycleCode) {
		this.billCycleCode = billCycleCode;
	}

	public String getBillCycleRunMonth() {
		return billCycleRunMonth;
	}

	public void setBillCycleRunMonth(String billCycleRunMonth) {
		this.billCycleRunMonth = billCycleRunMonth;
	}

	public String getBillCycleRunYear() {
		return billCycleRunYear;
	}

	public void setBillCycleRunYear(String billCycleRunYear) {
		this.billCycleRunYear = billCycleRunYear;
	}

	public String getBillEmail() {
		return billEmail;
	}

	public void setBillEmail(String billEmail) {
		this.billEmail = billEmail;
	}

	public String getBillPastDueAmount() {
		return billPastDueAmount;
	}

	public void setBillPastDueAmount(String billPastDueAmount) {
		this.billPastDueAmount = billPastDueAmount;
	}

	public String getBillPaymentsReceivedAmount() {
		return billPaymentsReceivedAmount;
	}

	public void setBillPaymentsReceivedAmount(String billPaymentsReceivedAmount) {
		this.billPaymentsReceivedAmount = billPaymentsReceivedAmount;
	}

	public String getBillPreviousBalanceAmount() {
		return billPreviousBalanceAmount;
	}

	public void setBillPreviousBalanceAmount(String billPreviousBalanceAmount) {
		this.billPreviousBalanceAmount = billPreviousBalanceAmount;
	}

	public String getBillSequenceNumber() {
		return billSequenceNumber;
	}

	public void setBillSequenceNumber(String billSequenceNumber) {
		this.billSequenceNumber = billSequenceNumber;
	}

	public String getBillTotalDueAmount() {
		return billTotalDueAmount;
	}

	public void setBillTotalDueAmount(String billTotalDueAmount) {
		this.billTotalDueAmount = billTotalDueAmount;
	}

	public String getBillCloseDate() {
		return billCloseDate;
	}

	public void setBillCloseDate(String billCloseDate) {
		this.billCloseDate = billCloseDate;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillCoverageEndDate() {
		return billCoverageEndDate;
	}

	public void setBillCoverageEndDate(String billCoverageEndDate) {
		this.billCoverageEndDate = billCoverageEndDate;
	}

	public String getBillCoverageStartDate() {
		return billCoverageStartDate;
	}

	public void setBillCoverageStartDate(String billCoverageStartDate) {
		this.billCoverageStartDate = billCoverageStartDate;
	}

	public String getBillDueDate() {
		return billDueDate;
	}

	public void setBillDueDate(String billDueDate) {
		this.billDueDate = billDueDate;
	}

	public String getIncludedMinutes() {
		return includedMinutes;
	}

	public void setIncludedMinutes(String includedMinutes) {
		this.includedMinutes = includedMinutes;
	}

}
