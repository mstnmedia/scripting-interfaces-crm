package com.mstn.scripting.interfaces.ext;

import com.mstn.scripting.core.Utils;
import _do.com.claro.soa.model.ordering.OrderID;
import java.util.List;

public class OrderID_Ext extends OrderID {

	static public String SEPARATOR = "_";

	public OrderID_Ext(String orderID) {
		if (isValid(orderID)) {
			List<String> parts = Utils.splitAsList(orderID, SEPARATOR);
			this.uid = parts.get(0);
			this.displayID = parts.get(1);
		}
	}

	public OrderID_Ext(OrderID base) {
		this.uid = base.getUid();
		this.displayID = base.getDisplayID();
	}

	static public boolean isValid(String orderID) {
		if (Utils.stringNonNullOrEmpty(orderID)) {
			return Utils.splitAsList(orderID, SEPARATOR).size() > 1;
			//orderID
			//	.trim()
			//	.matches("^[0-9]{11}$");
		}
		return false;
	}

	@Override
	public String toString() {
		return toString(this);
	}

	static public String toString(OrderID orderID) {
		return orderID == null ? null
				: orderID.getUid() + "_" + orderID.getDisplayID();
	}

}
