package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.customer.Contact;
import _do.com.claro.soa.model.customer.Contacts;
import _do.com.claro.soa.model.customer.Customer;
import _do.com.claro.soa.model.personidentity.IdentityCard;
import _do.com.claro.soa.model.personidentity.PersonID;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.models.Transaction_Result;
import java.util.Arrays;
import java.util.List;

public class Customer_Ext {

	public static final String ID_SEPARATOR = "_";

	private String groupID;
	private String entityID;
	private String customerID;
	private String identityCardID;
	private String passport;
	private String taxID;
	private String documentType;
	private String document;
	private String name;
	private String phone;
	private String fax;
	private String email;
	private String segment;
	private String type;
	private String legalName;
	private String sicCode;
	private String comercialSector;
	private String comercialActivity;
	private String website;
	private String contactID;
	private String contactName;
	private String contactPhone;
	private String contactFax;
	private String contactEmail;
	private String contactRole;
	private String contactBirthDate;
	private String contactMaritalStatus;
	private String contactGender;
	private String contactNationality;
	private String lastOrderDate;
	private String lastOrderReason;
	private String hasMiClaro;

	public Customer original;

	public Customer_Ext(Customer customer) {
		if (customer == null) {
			return;
		}
		this.original = customer;

		this.taxID = customer.getTaxID();
		this.name = customer.getName();
		Contact contact = null;
		Contacts contacts = customer.getContacts();
		if (contacts != null) {
			List<Contact> contactList = contacts.getContact();
			contact = !contactList.isEmpty() ? contactList.get(0) : null;
		}

		if (contact != null) {
			this.contactID = contact.getContactID();
			this.contactName = Utils.namesToString(contact.getNames());
			this.contactRole = contact.getRole();
			PersonID person = contact.getPersonID();
			if (person != null) {
				IdentityCard ic = person.getIdentityCard();
				if (ic != null) {
					this.identityCardID = new IdentityCardID_Ext(ic.getIdentityCardID()).toString();
				}
				if (person.getPassport() != null) {
					this.passport = person.getPassport().getPassportID();
				}
			}
			this.contactBirthDate = DATE.toString(contact.getBirthDate());
			if (contact.getMaritalStatus() != null) {
				this.contactMaritalStatus = contact.getMaritalStatus().name();
			}
			if (contact.getGender() != null) {
				switch (contact.getGender()) {
					case MALE:
						this.contactGender = "Masculino";
						break;
					case FEMALE:
						this.contactGender = "Femenino";
						break;
				}
			}
			this.contactNationality = contact.getNationality();

			this.contactPhone = Utils.phoneToString(contact.getPhone());
			this.contactFax = Utils.phoneToString(contact.getFax());
			this.contactEmail = contact.getEmail();
		}

		this.phone = Utils.phoneToString(customer.getPhone());
		this.fax = Utils.phoneToString(customer.getFax());
		this.email = customer.getEmail();

		if (passport != null) {
			documentType = "Pasaporte";
			document = "P" + passport;
		} else if (identityCardID != null) {
			documentType = "Cédula";
			document = "C" + identityCardID;
		}

		if (customer.getCustomerSegment() != null) {
			this.segment = customer.getCustomerSegment().getCode();
		}
		if (customer.getCustomerType() != null) {
			this.type = customer.getCustomerType().name();
		}
		this.legalName = customer.getLegalName();
		this.sicCode = customer.getSicCode();
		this.comercialSector = customer.getComercialSector();
		this.comercialActivity = customer.getComercialActivity();
		this.website = customer.getWebsite();

		CustomerID_Ext cide = new CustomerID_Ext(customer.getCustomerID(), this.contactID);
		this.groupID = cide.getGroupID();
		this.entityID = cide.getEntityID();
		this.customerID = cide.toString();
	}

	static public List<Transaction_Result> getCustomerResults(Customer_Ext customer) throws Exception {
		boolean empty = customer == null;

		String groupID = empty ? "" : customer.getGroupID();
		String entityID = empty ? "" : customer.getEntityID();
		String customerID = empty ? "" : customer.getCustomerID();
		String identityCardID = empty ? "" : customer.getIdentityCardID();
		String passport = empty ? "" : customer.getPassport();
		String taxID = empty ? "" : customer.getTaxID();
		String documentType = empty ? "" : customer.getDocumentType();
		String document = empty ? "" : customer.getDocument();
		String name = empty ? "" : customer.getName();
		String phone = empty ? "" : customer.getPhone();
		String fax = empty ? "" : customer.getFax();
		String email = empty ? "" : customer.getEmail();
		String contactID = empty ? "" : customer.getContactID();
		String contactName = empty ? "" : customer.getContactName();
		String contactPhone = empty ? "" : customer.getContactPhone();
		String contactFax = empty ? "" : customer.getContactFax();
		String contactEmail = empty ? "" : customer.getContactEmail();
		String segment = empty ? "" : customer.getSegment();
		String type = empty ? "" : customer.getType();
		String legalName = empty ? "" : customer.getLegalName();
		String sicCode = empty ? "" : customer.getSicCode();
		String role = empty ? "" : customer.getContactRole();
		String birthDate = empty ? "" : customer.getContactBirthDate();
		String gender = empty ? "" : customer.getContactGender();
		String nacionality = empty ? "" : customer.getContactNationality();
		String lastOrderDate = empty ? "" : customer.getLastOrderDate();
		String lastOrderReason = empty ? "" : customer.getLastOrderReason();
		String hasMiClaro = empty ? "" : customer.getHasMiClaro();
		String original = empty ? "" : JSON.toString(customer.original);

		int ID = Interfaces.CUSTOMER_INFO_INTERFACE_ID_BASE;
		String prefix = Interfaces.CUSTOMER_INFO_INTERFACE_NAME_BASE + "_";
		return Arrays.asList(
				//(id_interface, name, label)
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(0, ID, prefix + "groupID", "Tipo de Entidad", groupID, ""),
				new Transaction_Result(0, ID, prefix + "entityID", "ID de Entidad", entityID, ""),
				new Transaction_Result(0, ID, prefix + "customerID", "ID del cliente", customerID, ""),
				new Transaction_Result(0, ID, prefix + "identityCardID", "Cédula", identityCardID, ""),
				new Transaction_Result(0, ID, prefix + "passport", "Pasaporte", passport, ""),
				new Transaction_Result(0, ID, prefix + "taxID", "RNC", taxID, ""),
				new Transaction_Result(0, ID, prefix + "documentType", "Documento (Tipo)", documentType, ""),
				new Transaction_Result(0, ID, prefix + "document", "Documento", document, ""),
				new Transaction_Result(0, ID, prefix + "name", "Nombre", name, ""),
				new Transaction_Result(0, ID, prefix + "phone", "Teléfono", phone, ""),
				new Transaction_Result(0, ID, prefix + "fax", "Fax", fax, ""),
				new Transaction_Result(0, ID, prefix + "email", "Email", email, ""),
				new Transaction_Result(0, ID, prefix + "contactID", "ID de Contacto", contactID, ""),
				new Transaction_Result(0, ID, prefix + "contactName", "Nombre de Contacto", contactName, ""),
				new Transaction_Result(0, ID, prefix + "contactPhone", "Teléfono de Contacto", contactPhone, ""),
				new Transaction_Result(0, ID, prefix + "contactFax", "Fax de Contacto", contactFax, ""),
				new Transaction_Result(0, ID, prefix + "contactEmail", "Email de Contacto", contactEmail, ""),
				new Transaction_Result(0, ID, prefix + "segment", "Segmento", segment, ""),
				new Transaction_Result(0, ID, prefix + "type", "Tipo de cliente", type, ""),
				new Transaction_Result(0, ID, prefix + "legalName", "Nombre legal", legalName, ""),
				new Transaction_Result(0, ID, prefix + "sicCode", "Código SIC", sicCode, ""),
				new Transaction_Result(0, ID, prefix + "role", "Rol", role, ""),
				new Transaction_Result(0, ID, prefix + "birthDate", "Fecha de Nacimiento", birthDate, ""),
				new Transaction_Result(0, ID, prefix + "gender", "Género", gender, ""),
				new Transaction_Result(0, ID, prefix + "nacionality", "Nacionalidad", nacionality, ""),
				new Transaction_Result(0, ID, prefix + "lastOrderDate", "Fecha de última orden", lastOrderDate, ""),
				new Transaction_Result(0, ID, prefix + "lastOrderReason", "Razón de última orden", lastOrderReason, ""),
				new Transaction_Result(0, ID, prefix + "hasMiClaro", "Registrado en MiClaro", hasMiClaro, ""),
				new Transaction_Result(0, ID, prefix + "original", "Original", original, "{\"notSave\": true}")
		);
	}

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public String getEntityID() {
		return entityID;
	}

	public void setEntityID(String entityID) {
		this.entityID = entityID;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getIdentityCardID() {
		return identityCardID;
	}

	public void setIdentityCardID(String identityCardID) {
		this.identityCardID = identityCardID;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getTaxID() {
		return taxID;
	}

	public void setTaxID(String taxID) {
		this.taxID = taxID;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getSicCode() {
		return sicCode;
	}

	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

	public String getComercialSector() {
		return comercialSector;
	}

	public void setComercialSector(String comercialSector) {
		this.comercialSector = comercialSector;
	}

	public String getComercialActivity() {
		return comercialActivity;
	}

	public void setComercialActivity(String comercialActivity) {
		this.comercialActivity = comercialActivity;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getContactID() {
		return contactID;
	}

	public void setContactID(String contactID) {
		this.contactID = contactID;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactFax() {
		return contactFax;
	}

	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}

	public String getContactRole() {
		return contactRole;
	}

	public void setContactRole(String contactRole) {
		this.contactRole = contactRole;
	}

	public String getContactBirthDate() {
		return contactBirthDate;
	}

	public void setContactBirthDate(String contactBirthDate) {
		this.contactBirthDate = contactBirthDate;
	}

	public String getContactMaritalStatus() {
		return contactMaritalStatus;
	}

	public void setContactMaritalStatus(String contactMaritalStatus) {
		this.contactMaritalStatus = contactMaritalStatus;
	}

	public String getContactGender() {
		return contactGender;
	}

	public void setContactGender(String contactGender) {
		this.contactGender = contactGender;
	}

	public String getContactNationality() {
		return contactNationality;
	}

	public void setContactNationality(String contactNationality) {
		this.contactNationality = contactNationality;
	}

	public String getLastOrderDate() {
		return lastOrderDate;
	}

	public void setLastOrderDate(String lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}

	public String getLastOrderReason() {
		return lastOrderReason;
	}

	public void setLastOrderReason(String lastOrderReason) {
		this.lastOrderReason = lastOrderReason;
	}

	public String getHasMiClaro() {
		return hasMiClaro;
	}

	public void setHasMiClaro(String hasMiClaro) {
		this.hasMiClaro = hasMiClaro;
	}

	public _do.com.claro.soa.model.customer.Customer getOriginal() {
		return original;
	}

	public void setOriginal(_do.com.claro.soa.model.customer.Customer original) {
		this.original = original;
	}

}
