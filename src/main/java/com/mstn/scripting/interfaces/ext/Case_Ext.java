package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.customer.Case;
import _do.com.claro.soa.model.customer.CaseContact;
import _do.com.claro.soa.model.customer.Closer;
import _do.com.claro.soa.model.customer.Creator;
import _do.com.claro.soa.model.customer.Note;
import _do.com.claro.soa.model.customer.Notes;
import _do.com.claro.soa.model.customer.Topic;
import _do.com.claro.soa.model.employee.Employee;
import com.mstn.scripting.core.DATE;
import java.util.ArrayList;
import java.util.List;

public class Case_Ext {

	private String id;
	private String customerID;
	private String channelName;
	private String contactName;
	private String contactPhone;
	private String closeDate;
	private String closeReason;
	private String closerID;
	private String closerName;
	private String creationDate;
	private String dueDate;
	private String status;
	private String contactInfo;
	private String contactMode;
	private String creatorID;
	private String creatorName;
	private String includesMaintenanceInContract;
	private String source;
	private String subscriberNo;
	private String topicSubject;
	private String topicClassification;
	private String topicOutcome;
	private List<Note_Ext> notes = new ArrayList();

	public Case original;

	public Case_Ext(Case base) {
		if (base == null) {
			return;
		}
		this.original = base;
		this.id = base.getId();
		this.customerID = new CustomerID_Ext(base.getCustomerID()).toString();
		if (base.getChannel() != null) {
			this.channelName = base.getChannel().getName();
		}
		CaseContact contact = base.getCaseContact();
		if (contact != null) {
			this.contactName = Utils.namesToString(contact.getNames());
			this.contactPhone = contact.getPhoneString();
		}
		this.closeDate = DATE.toString(base.getCloseDate());
		this.closeReason = base.getCloseReason();
		Closer closer = base.getCloser();
		if (closer != null) {
			Employee employee = closer.getEmployee();
			if (employee != null) {
				this.closerID = employee.getEmployeeId();
				this.closerName = Utils.getEmployeeName(employee);
			}
		}
		this.creationDate = DATE.toString(base.getCreationDate());
		this.dueDate = DATE.toString(base.getDueDate());
		this.status = base.getStatus();
		this.contactInfo = base.getContactInfo();
		this.contactMode = base.getContactMode();
		Creator creator = base.getCreator();
		if (creator != null) {
			Employee employee = creator.getEmployee();
			if (employee != null) {
				this.creatorID = employee.getEmployeeId();
				this.creatorName = Utils.getEmployeeName(employee);
			}
		}
		this.includesMaintenanceInContract = base.getIncludesMaintenanceInContract().name();
		if (base.getSource() != null) {
			this.source = base.getSource().name();
		}
		this.subscriberNo = new TelephoneNumber_Ext(base.getSubscriberNo()).toString();
		Topic topicBase = base.getTopic();
		if (topicBase != null) {
			this.topicSubject = topicBase.getSubject();
			this.topicClassification = topicBase.getClassification();
			this.topicOutcome = topicBase.getOutcome();
		}

		Notes Notes = base.getNotes();
		if (Notes != null) {
			List<Note> caseNotes = Notes.getNote();
			for (int i = 0; i < caseNotes.size(); i++) {
				Note note = caseNotes.get(i);
				this.notes.add(new Note_Ext(note, id + i));
			}
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}

	public String getCloseReason() {
		return closeReason;
	}

	public void setCloseReason(String closeReason) {
		this.closeReason = closeReason;
	}

	public String getCloserID() {
		return closerID;
	}

	public void setCloserID(String closerID) {
		this.closerID = closerID;
	}

	public String getCloserName() {
		return closerName;
	}

	public void setCloserName(String closerName) {
		this.closerName = closerName;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getContactMode() {
		return contactMode;
	}

	public void setContactMode(String contactMode) {
		this.contactMode = contactMode;
	}

	public String getCreatorID() {
		return creatorID;
	}

	public void setCreatorID(String creatorID) {
		this.creatorID = creatorID;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getIncludesMaintenanceInContract() {
		return includesMaintenanceInContract;
	}

	public void setIncludesMaintenanceInContract(String includesMaintenanceInContract) {
		this.includesMaintenanceInContract = includesMaintenanceInContract;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSubscriberNo() {
		return subscriberNo;
	}

	public void setSubscriberNo(String subscriberNo) {
		this.subscriberNo = subscriberNo;
	}

	public String getTopicSubject() {
		return topicSubject;
	}

	public void setTopicSubject(String topicSubject) {
		this.topicSubject = topicSubject;
	}

	public String getTopicClassification() {
		return topicClassification;
	}

	public void setTopicClassification(String topicClassification) {
		this.topicClassification = topicClassification;
	}

	public String getTopicOutcome() {
		return topicOutcome;
	}

	public void setTopicOutcome(String topicOutcome) {
		this.topicOutcome = topicOutcome;
	}

	public List<Note_Ext> getNotes() {
		return notes;
	}

	public void setNotes(List<Note_Ext> notes) {
		this.notes = notes;
	}

	public Case getOriginal() {
		return original;
	}

	public void setOriginal(Case original) {
		this.original = original;
	}

}
