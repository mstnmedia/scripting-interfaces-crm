package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.generic.TelephoneNumber;

public class TelephoneNumber_Ext extends TelephoneNumber {

	public TelephoneNumber_Ext(String phone) {
		if (isValid(phone)) {
			this.npa = phone.substring(0, phone.length() - 7);
			this.nxx = phone.substring(phone.length() - 7, phone.length() - 4);
			this.stationCode = phone.substring(phone.length() - 4);
		}
	}

	public TelephoneNumber_Ext(TelephoneNumber telephoneNumber) {
		if (telephoneNumber != null) {
			this.npa = telephoneNumber.getNpa();
			this.nxx = telephoneNumber.getNxx();
			this.stationCode = telephoneNumber.getStationCode();
		}
	}

	static public boolean isValid(String telephoneNumber) {
		return telephoneNumber != null ? telephoneNumber.matches("^(1\\-)?[0-9]{10}$") : false;
	}

	@Override
	public String toString() {
		return toString(this);
	}

	static public String toString(TelephoneNumber telephoneNumber) {
		if (telephoneNumber != null) {
			String npa = telephoneNumber.getNpa();
			String nxx = telephoneNumber.getNxx();
			String stationCode = telephoneNumber.getStationCode();
			if (npa != null && nxx != null && stationCode != null) {
				return npa + nxx + stationCode;
			}
		}
		return null;
	}

	static public String addDashes(String phone) {
		if (phone != null && phone.length() >= 10) {
			return new StringBuilder(phone)
					.insert(phone.length() - 4, "-")
					.insert(phone.length() - 7, "-")
					.toString();
		}
		return phone;
	}

}
