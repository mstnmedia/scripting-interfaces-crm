/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.customer.Diagnosis;
import _do.com.claro.soa.model.customer.Result;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.Utils;
import org.datacontract.schemas._2004._07.claro_sacs_webservices.DiagnosticResult;

/**
 *
 * @author amatos
 */
public class Diagnosis_Ext {

	private String name;
	private String state;
	private String startDate;
	private String endDate;
	private String notes;
	private String result;

	public Diagnosis_Ext(Diagnosis base) {
		this.name = base.getName();
		this.state = base.getState();
		this.startDate = DATE.toString(base.getStartDate());
		this.endDate = DATE.toString(base.getEndDate());
		this.notes = base.getNotes();
		Result objResult = base.getResult();
		if (objResult == null) {
			this.result = objResult.value();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
