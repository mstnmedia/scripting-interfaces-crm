package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.personidentity.IdentityCardID;

public class IdentityCardID_Ext extends IdentityCardID {

	public IdentityCardID_Ext(String ic) {
		ic = Utils.replaceNonDigit(ic);
		if (isValid(ic)) {
			this.seriesNum = ic.substring(0, ic.length() - 3);
			this.documentNum = ic.substring(ic.length() - 3, ic.length() - 1);
			this.verificationNum = ic.substring(ic.length() - 1);
		}
	}

	public IdentityCardID_Ext(IdentityCardID ic) {
		if (ic != null) {
			this.seriesNum = ic.getSeriesNum();
			this.documentNum = ic.getDocumentNum();
			this.verificationNum = ic.getVerificationNum();
		}
	}

	static public boolean isValid(String ic) {
		if (ic != null) {
			return ic
					.replaceAll("-", "")
					.trim()
					.matches("^[0-9]{11}$");
		}
		return false;
	}

	@Override
	public String toString() {
		if (seriesNum != null && documentNum != null && verificationNum != null) {
			return seriesNum + documentNum + verificationNum;
		}
		return null;
	}

}
