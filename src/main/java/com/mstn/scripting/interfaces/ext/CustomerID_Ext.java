package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.customer.Customer;
import _do.com.claro.soa.model.customer.CustomerID;
import _do.com.claro.soa.model.customer.TransientID;

public class CustomerID_Ext extends CustomerID {

	public static final String ID_SEPARATOR = "_";

	protected String contactID;

	public CustomerID_Ext(TransientID transientID) {
		this.transientID = transientID;
	}

	public CustomerID_Ext(CustomerID base) {
		this.transientID = base.getTransientID();
	}

	public CustomerID_Ext(CustomerID base, String contactID) {
		this.transientID = base.getTransientID();
		this.contactID = contactID;
	}

	public CustomerID_Ext(String customerID) {
		if (isValid(customerID)) {
			TransientID tID = new TransientID();
			String[] parts = splitCustomerID(customerID);
			tID.setGroupId(parts[0]);
			tID.setEntityId(parts[1]);
			this.contactID = parts[2];
			this.transientID = tID;
		}
	}

	public String getGroupID() {
		return transientID == null ? null : transientID.getGroupId();
	}

	public String getEntityID() {
		return transientID == null ? null : transientID.getEntityId();
	}

	static public boolean isValid(String customerID) {
		boolean valid = Utils.stringNonNullOrEmpty(customerID);
		if (valid) {
			String[] parts = splitCustomerID(customerID);
			valid = !parts[0].isEmpty() && !parts[1].isEmpty();
		}
		return valid;
	}

	private static String[] splitCustomerID(String client_id) {
		String[] results = new String[]{"", "", ""};
		String[] parts = client_id.split(Customer_Ext.ID_SEPARATOR);
		for (int i = 0; i < parts.length; i++) {
			String part = parts[i];
			results[i] = part;
		}
		return results;
	}

	public static CustomerID_Ext from(Customer customer) {
		String contactID = null;
		if (customer.getContacts() != null && !customer.getContacts().getContact().isEmpty()) {
			contactID = customer.getContacts().getContact().get(0).getContactID();
		}
		return new CustomerID_Ext(customer.getCustomerID(), contactID);
	}

	@Override
	public String toString() {
		return toString(this, contactID);
	}

	public static String toString(CustomerID customerID) {
		return toString(customerID, "");
	}

	public static String toString(CustomerID customerID, String contactID) {
		TransientID transientID = customerID.getTransientID();
		if (transientID != null && transientID.getGroupId() != null && transientID.getEntityId() != null) {
			String result = transientID.getGroupId() + ID_SEPARATOR + transientID.getEntityId();
			if (Utils.stringNonNullOrEmpty(contactID)) {
				result = result + ID_SEPARATOR + contactID;
			}
			return result;
		}
		return null;
	}
}
