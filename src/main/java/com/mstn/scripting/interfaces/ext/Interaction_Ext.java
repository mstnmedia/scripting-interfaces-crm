/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.customer.Interaction;
import _do.com.claro.soa.model.customer.Topic;
import _do.com.claro.soa.model.customer.Topics;
import _do.com.claro.soa.model.employee.Employee;
import com.mstn.scripting.core.DATE;
import java.util.List;

/**
 *
 * @author amatos
 */
public class Interaction_Ext {

	private String id;
	private String customerID;
	private String channelName;
	private String contactID;
	private String creatorID;
	private String creatorName;
	private String creationDate;
	private String interactionDirection;
	private String interactionType;
	private String medium;
	private String notes;
	private String phone;
	private String source;
	private String title;
	private List<Topic> topics;

	private Interaction original;

	public Interaction_Ext(Interaction base) {
		if (base == null) {
			return;
		}
		this.original = base;
		this.id = base.getId();
		this.customerID = new CustomerID_Ext(base.getCustomerID()).toString();
		this.channelName = base.getChannel();
		this.contactID = base.getContactID();
		this.creationDate = DATE.toString(base.getCreationDate());
		Interaction.Creator creator = base.getCreator();
		if (creator != null) {
			Employee employee = creator.getEmployee();
			if (employee != null) {
				this.creatorID = employee.getEmployeeId();
				this.creatorName = Utils.getEmployeeName(employee);
			}
		}
		if (base.getInteractionDirection() != null) {
			this.interactionDirection = base.getInteractionDirection().name();
		}
		if (base.getInteractionType() != null) {
			this.interactionType = base.getInteractionType().name();
		}
		this.medium = base.getMedium();
		this.notes = base.getNotes();
		this.phone = new TelephoneNumber_Ext(base.getPhone()).toString();
		if (base.getSource() != null) {
			this.source = base.getSource().name();
		}
		this.title = base.getTitle();
		Topics objTopics = base.getTopics();
		if (objTopics != null) {
			this.topics = objTopics.getTopic();
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getContactID() {
		return contactID;
	}

	public void setContactID(String contactID) {
		this.contactID = contactID;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getInteractionDirection() {
		return interactionDirection;
	}

	public void setInteractionDirection(String interactionDirection) {
		this.interactionDirection = interactionDirection;
	}

	public String getInteractionType() {
		return interactionType;
	}

	public void setInteractionType(String interactionType) {
		this.interactionType = interactionType;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCreatorID() {
		return creatorID;
	}

	public void setCreatorID(String creatorID) {
		this.creatorID = creatorID;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

	public Interaction getOriginal() {
		return original;
	}

	public void setOriginal(Interaction original) {
		this.original = original;
	}

}
