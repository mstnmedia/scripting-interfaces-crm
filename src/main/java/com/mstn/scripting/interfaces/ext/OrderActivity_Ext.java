/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.ordering.OrderActivity;

/**
 *
 * @author amatos
 */
public class OrderActivity_Ext extends OrderActivity {

	protected String nameLabel;
	protected String statusLabel;

	public OrderActivity_Ext() {
	}

	public OrderActivity_Ext(OrderActivity base) {
		this.name = base.getName();
		switch (name) {
			default:
				this.nameLabel = this.name;
				break;
		}
		this.employeeId = base.getEmployeeId();
		this.status = base.getStatus();
		switch (status) {
			case ACTIVATED:
				this.statusLabel = "Activado";
				break;
			case CANCELLED:
				this.statusLabel = "Cancelado";
				break;
			case COMPLETED:
				this.statusLabel = "Completado";
				break;
			case FAILED:
				this.statusLabel = "Fallido";
				break;
			case LOADED:
				this.statusLabel = "Cargado";
				break;
			case OVER_DUE:
				this.statusLabel = "Vencido";
				break;
			case READY_FOR_EXECUTION:
				this.statusLabel = "Listo para ejecución";
				break;
			case RJ:
				this.statusLabel = "Rechazado";
				break;
			case SEMI_FINAL:
				this.statusLabel = "Semi-final";
				break;
			case SUSPENDED:
				this.statusLabel = "Suspendido";
				break;
			default:
				this.statusLabel = this.status.value();
				break;
		}
		this.creationDate = base.getCreationDate();
		this.executionDate = base.getExecutionDate();
	}

	public void getNameLabel(String name) {
	}

	public String getNameLabel() {
		return nameLabel;
	}

	public void setNameLabel(String nameLabel) {
		this.nameLabel = nameLabel;
	}

	public String getStatusLabel() {
		return statusLabel;
	}

	public void setStatusLabel(String statusLabel) {
		this.statusLabel = statusLabel;
	}

}
