/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mstn.scripting.interfaces.ext;

import _do.com.claro.soa.model.customer.Note;

/**
 *
 * @author amatos
 */
public class Note_Ext extends Note {
	protected String id;
	public Note_Ext(Note base, String id) {
		this.creationDate = base.getCreationDate();
		this.description = base.getDescription();
		this.type = base.getType();
		
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
