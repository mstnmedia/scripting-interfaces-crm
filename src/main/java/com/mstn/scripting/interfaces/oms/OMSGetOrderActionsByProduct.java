/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.oms;

import _do.com.claro.soa.services.ordering.getorderactionsbyproduct.GetOrderActionsByProduct;
import _do.com.claro.soa.services.ordering.getorderactionsbyproduct.GetOrderActionsByProduct_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 * Interfaz que obtiene las órdenes del producto indicado de una subscripción
 * indicada de un cliente.
 *
 * @author amatos
 */
public class OMSGetOrderActionsByProduct extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = OMSGetOrderActionsByProduct.class;
		try {
			SOAP = GetOrderActionsByProduct_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public OMSGetOrderActionsByProduct() {
		super(0, "omsgetorderactionsbyproduct", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("oms_getorderactionsbyproduct"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("groupId")) {
			field.setLabel("ID de Grupo");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("entityId")) {
			field.setLabel("CCU");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("npa")) {
			field.setLabel("No. de Teléfono");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
		} else if (fieldName.endsWith("nxx")) {
			field.setLabel("NXX");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setRequired(false);
		} else if (fieldName.endsWith("stationCode")) {
			field.setLabel("Código de Estación");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setRequired(false);
		} else if (fieldName.endsWith("productCode")) {
			field.setLabel("Código del producto");
		} else if (fieldName.endsWith("pageNum")) {
			field.setLabel("Número de página");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		}
		return field;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("oms_getorderactionsbyproduct").toString();
		super.test();
	}

}
