/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.oms;

import _do.com.claro.soa.model.ordering.Order;
import _do.com.claro.soa.model.ordering.OrderAction;
import _do.com.claro.soa.model.ordering.OrderActions;
import _do.com.claro.soa.services.ordering.getorderactionsbycustomerid.GetOrderActionsByCustomerID;
import _do.com.claro.soa.services.ordering.getorderactionsbycustomerid.GetOrderActionsByCustomerID_Service;
import _do.com.claro.soa.services.ordering.getorderactionsbycustomerid.GetRequest;
import _do.com.claro.soa.services.ordering.getorderactionsbycustomerid.GetResponse;
import _do.com.claro.soa.services.ordering.getorderactionsbyproduct.GetOrderActionsByProduct;
import _do.com.claro.soa.services.ordering.getorderactionsbyproduct.GetOrderActionsByProductRequest;
import _do.com.claro.soa.services.ordering.getorderactionsbyproduct.GetOrderActionsByProductResponse;
import _do.com.claro.soa.services.ordering.getorderactionsbyproduct.GetOrderActionsByProduct_Service;
import _do.com.claro.soa.services.ordering.getorderbyorderid.GetOrderByOrderID;
import _do.com.claro.soa.services.ordering.getorderbyorderid.GetOrderByOrderID_Service;
import _do.com.claro.soa.services.ordering.getorderbyorderid.GetOrderRequest;
import _do.com.claro.soa.services.ordering.getorderbyorderid.GetOrderResponse;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.OrderAction_Ext;
import com.mstn.scripting.interfaces.ext.OrderID_Ext;
import com.mstn.scripting.interfaces.ext.TelephoneNumber_Ext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Clase que implementa algunos métodos de OMS para obtener información.
 *
 * @author amatos
 */
public class OMS {

	/**
	 *
	 * @param customerID
	 * @param pageNum
	 * @return
	 */
	static public List<OrderAction> getOrderActionsByCustomerID(String customerID, int pageNum) {
		List<OrderAction> items = new ArrayList<>();
		long start = DATE.nowAsLong();
		try {
			GetOrderActionsByCustomerID soap = GetOrderActionsByCustomerID_Service.getInstance();
			GetRequest request = new GetRequest();
			request.setCustomerID(new CustomerID_Ext(customerID));
			request.setPageNum(pageNum);

			GetResponse getResponse = soap.get(request);
			OrderActions itemsClass = getResponse.getOrderActions();
			if (itemsClass != null) {
				items = itemsClass.getOrderAction();
			}
			Utils.logFinest(OMS.class, "OMS GetOrderActionsByCustomerID: \n");
			Utils.logFinest(OMS.class, JSON.toString(items) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(OMS.class, "getOrderActionsByCustomerID", ex);
		}
		long end = DATE.nowAsLong();
		Utils.logTrace(OMS.class, "OMS ActionsByCID: " + (end - start));
		return items;
	}

	static public List<OrderAction_Ext> GetOrderActionsByCustomerID(String customerID, int pageNum) {
		List<OrderAction_Ext> items = getOrderActionsByCustomerID(customerID, pageNum)
				.stream()
				.map(item -> new OrderAction_Ext(item))
				.collect(Collectors.toList());
		return items;
	}

	static public List<OrderAction> GetOrderActionsByProduct(String customerID, String productCode, String subscriberNo, int pageNum) {
		List<OrderAction> items = new ArrayList<>();
		try {
			GetOrderActionsByProduct soap = GetOrderActionsByProduct_Service.getInstance();
			GetOrderActionsByProductRequest request = new GetOrderActionsByProductRequest();
			request.setCustomerID(new CustomerID_Ext(customerID));
			request.setProductCode(productCode);
			request.setSubscriberNo(new TelephoneNumber_Ext(subscriberNo));
			request.setPageNum(pageNum);

			GetOrderActionsByProductResponse getResponse = soap.get(request);
			OrderActions itemsClass = getResponse.getOrderActions();
			if (itemsClass != null) {
				items = itemsClass.getOrderAction();
			}
			Utils.logFinest(OMS.class, "OMS OrderActionsByProduct: \n");
			Utils.logFinest(OMS.class, JSON.toString(items) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(OMS.class, "OrderActionsByProduct", ex);
		}
		return items;
	}

	static public Order GetOrderByOrderID(String orderID) {
		Order order = null;
		try {
			GetOrderByOrderID soap = GetOrderByOrderID_Service.getInstance();
			GetOrderRequest request = new GetOrderRequest();
			request.setOrderID(new OrderID_Ext(orderID));

			GetOrderResponse getResponse = soap.get(request);
			order = getResponse.getOrder();

			Utils.logFinest(OMS.class, "OMS GetOrderByOrderID: \n");
			Utils.logFinest(OMS.class, JSON.toString(order) + "\n\n");
		} catch (Exception ex) {
			Utils.logException(OMS.class, "GetOrderByOrderID", ex);
		}
		return order;
	}

}
