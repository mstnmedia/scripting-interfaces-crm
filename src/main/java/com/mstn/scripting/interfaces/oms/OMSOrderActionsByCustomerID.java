/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.oms;

import _do.com.claro.soa.services.ordering.getorderactionsbycustomerid.GetOrderActionsByCustomerID;
import _do.com.claro.soa.services.ordering.getorderactionsbycustomerid.GetOrderActionsByCustomerID_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 * Interfaz dinámica que consulta una lista de órdenes que pertenecen el detalle
 * de una orden.
 *
 * @author amatos
 */
public class OMSOrderActionsByCustomerID extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = OMSOrderActionsByCustomerID.class;
		try {
			SOAP = GetOrderActionsByCustomerID_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public OMSOrderActionsByCustomerID() {
		super(0, "omsgetorderactionsbycustomerid", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("oms_getorderactionsbycustomerid"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("groupId")) {
			field.setLabel("ID de Grupo");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("entityId")) {
			field.setLabel("CCU");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("pageNum")) {
			field.setLabel("Número de página");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		}
		return field;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("oms_getorderactionsbycustomerid").toString();
		super.test();
	}

}
