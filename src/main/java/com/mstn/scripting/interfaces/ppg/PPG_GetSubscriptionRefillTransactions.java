/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ppg;

import _do.com.claro.soa.services.com.claro.soa.getsubscriptionrefilltransactions.getsubscriptionrefilltransactions.GetSubscriptionRefillTransactions;
import _do.com.claro.soa.services.com.claro.soa.getsubscriptionrefilltransactions.getsubscriptionrefilltransactions.GetSubscriptionRefillTransactions_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.lang.reflect.Method;

/**
 *
 * @author amatos
 */
public class PPG_GetSubscriptionRefillTransactions extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = PPG_GetSubscriptionRefillTransactions.class;
		try {
			SOAP = GetSubscriptionRefillTransactions_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public PPG_GetSubscriptionRefillTransactions() {
		super(0, "ppggetsubscriptionrefilltransactions", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("ppg_getsubscriptionrefilltransactions"));
		return inter;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("ppg_getsubscriptionrefilltransactions").toString();
		super.test();
	}

}
