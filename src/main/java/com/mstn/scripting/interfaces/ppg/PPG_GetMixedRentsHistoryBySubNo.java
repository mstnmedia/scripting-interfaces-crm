/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ppg;

import _do.com.claro.soa.services.billing.getmixedrentshistorybysubno.GetMixedRentsHistoryBySubNo;
import _do.com.claro.soa.services.billing.getmixedrentshistorybysubno.GetMixedRentsHistoryBySubNo_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.lang.reflect.Method;

/**
 * Interfaz
 *
 * @author amatos
 */
public class PPG_GetMixedRentsHistoryBySubNo extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = PPG_GetMixedRentsHistoryBySubNo.class;
		try {
			SOAP = GetMixedRentsHistoryBySubNo_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public PPG_GetMixedRentsHistoryBySubNo() {
		super(0, "ppggetmixedrentshistorybysubno", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("ensamble_getmixedrentshistorybysubno"));
		return inter;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("ensamble_getmixedrentshistorybysubno").toString();
		super.test();
	}

}
