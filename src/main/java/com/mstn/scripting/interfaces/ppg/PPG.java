/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ppg;

import _do.com.claro.soa.model.billing.Subscription;
import _do.com.claro.soa.model.billing.Subscriptions;
import _do.com.claro.soa.services.billing.getprepaidsubscriptionsbycustomerid.GetPrepaidSubscriptionsByCustomerID;
import _do.com.claro.soa.services.billing.getprepaidsubscriptionsbycustomerid.GetPrepaidSubscriptionsByCustomerID_Service;
import _do.com.claro.soa.services.billing.getprepaidsubscriptionsbycustomerid.GetRequest;
import _do.com.claro.soa.services.billing.getprepaidsubscriptionsbycustomerid.GetResponse;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Clase que implementa la consulta paginada de números prepago por el ID del
 * cliente.
 *
 * @author amatos
 */
public class PPG {

	//static public String GetCustomerIDFromSubscriberNo(String searchPhone) throws Exception {
	//	String customerID = null;
	//	//List<Subscription_Ext> subscriptions = GetSubscriptionsBySubscriberNo(searchPhone);
	//	//if (!subscriptions.isEmpty()) {
	//	//	customerID = subscriptions.get(0).getCustomerID();
	//	//}
	//	System.out.println("PPG GetCustomerIDFromSubscriberNo customerID: " + customerID);
	//	return customerID;
	//}
	static public List<Subscription> GetPrepaidSubscriptionsByCustomerID(String customerID, int pageNum) {
		List<Subscription> subscriptions = new ArrayList<>();
		long start = DATE.nowAsLong();
		try {
			GetPrepaidSubscriptionsByCustomerID soap = GetPrepaidSubscriptionsByCustomerID_Service.getInstance();
			GetRequest request = new GetRequest();
			request.setPageNum(pageNum);
			request.setCustomerID(new CustomerID_Ext(customerID));
			GetResponse getResponse = soap.get(request);
			Subscriptions objSubscriptions = getResponse.getSubscriptions();
			if (objSubscriptions != null) {
				subscriptions = objSubscriptions.getSubscription();
			}
			Utils.logFinest(PPG.class, "PPG subscriptions: \n " + JSON.toString(subscriptions) + "\n\n\n");
		} catch (Exception ex) {
			Utils.logException(PPG.class, "PPG subscriptions", ex);
		}
		long end = DATE.nowAsLong();
		Utils.logTrace(PPG.class, "PPG ActionsByCID: " + (end - start));
		return subscriptions;
	}

	static public List<Subscription_Ext> GetPagePrepaidSubscriptionsByCustomerID(String customerID, int pageNum) {
		return GetPrepaidSubscriptionsByCustomerID(customerID, pageNum)
				.stream()
				.map(s -> new Subscription_Ext(s))
				.collect(Collectors.toList());
	}

	static public List<Subscription_Ext> GetAllPostpaidSubscriptionsByCustomerID(String customerID) {
		List<Subscription_Ext> subscriptions = new ArrayList<>();
		List<Subscription_Ext> temp;
		int pageNum = 1;
		do {
			temp = GetPagePrepaidSubscriptionsByCustomerID(customerID, pageNum);
			subscriptions.addAll(temp);
			pageNum++;
		} while (!temp.isEmpty());

		return subscriptions;
	}
}
