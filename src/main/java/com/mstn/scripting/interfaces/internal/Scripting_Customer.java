/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.ext.Customer_Ext;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Interfaz que contiene las variables con la información del cliente
 * seleccionado al inicio de la transacción en curso.
 *
 * @author amatos
 */
public class Scripting_Customer extends InterfaceBase {

	static Getter SOAP = new Scripting_Customer.Getter();

	public Scripting_Customer() {
		super(0, "customer", SOAP);
		INTERFACES_ID.put("customer_info", Interfaces.CUSTOMER_INFO_INTERFACE_ID_BASE);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setServer_url(InterfaceConfigs.get("crm_getcustomerbycustomerid"));
		item.setLabel("Información del cliente");
		item.setKeep_results(true);
		return item;
	}

	@Override
	protected Object invokeMethod() throws Exception {
		if (Utils.stringAreEquals("customer_info", inter.getName())) {
			return null;
		}
		return super.invokeMethod();
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter) {
		if (Utils.stringAreEquals("customer_info", inter.getName())) {
			try {
				return getCustomerVariables();
			} catch (Exception ex) {
				Logger.getLogger(Scripting_Customer.class.getName()).log(Level.SEVERE, null, ex);
				return Arrays.asList();
			}
		}
		return super.getTransactionResultsFromClass(method, inter);
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter) throws Exception {
		if (Utils.stringAreEquals("customer_info", inter.getName())) {
			return getCustomerVariables();
		}
		return super.getTransactionResultsFromResponse(value, inter);
	}

	static public List<Transaction_Result> getCustomerVariables() throws Exception {
		return Customer_Ext.getCustomerResults(null);
//		int ID = Interfaces.CUSTOMER_INFO_INTERFACE_ID;
//		String prefix = Interfaces.CUSTOMER_INFO_INTERFACE_NAME_BASE + "_";
//		return Arrays.asList(
//				new Transaction_Result(ID, prefix + "groupID", "Tipo de Entidad"),
//				new Transaction_Result(ID, prefix + "entityID", "ID de Entidad"),
//				new Transaction_Result(ID, prefix + "customerID", "ID del cliente"),
//				new Transaction_Result(ID, prefix + "identityCardID", "Cédula"),
//				new Transaction_Result(ID, prefix + "passport", "Pasaporte"),
//				new Transaction_Result(ID, prefix + "taxID", "RNC"),
//				new Transaction_Result(ID, prefix + "documentType", "Documento (Tipo)"),
//				new Transaction_Result(ID, prefix + "document", "Documento"),
//				new Transaction_Result(ID, prefix + "name", "Nombre"),
//				new Transaction_Result(ID, prefix + "phone", "Teléfono"),
//				new Transaction_Result(ID, prefix + "fax", "Fax"),
//				new Transaction_Result(ID, prefix + "email", "Email"),
//				new Transaction_Result(ID, prefix + "contactID", "ID de Contacto"),
//				new Transaction_Result(ID, prefix + "contactName", "Nombre de Contacto"),
//				new Transaction_Result(ID, prefix + "contactPhone", "Teléfono de Contacto"),
//				new Transaction_Result(ID, prefix + "contactFax", "Fax de Contacto"),
//				new Transaction_Result(ID, prefix + "contactEmail", "Email de Contacto"),
//				new Transaction_Result(ID, prefix + "segment", "Segmento"),
//				new Transaction_Result(ID, prefix + "type", "Tipo de cliente"),
//				new Transaction_Result(ID, prefix + "legalName", "Nombre legal"),
//				new Transaction_Result(ID, prefix + "sicCode", "Código SIC"),
//				new Transaction_Result(ID, prefix + "role", "Rol"),
//				new Transaction_Result(ID, prefix + "birthDate", "Fecha de Nacimiento"),
//				new Transaction_Result(ID, prefix + "gender", "Género"),
//				new Transaction_Result(ID, prefix + "nacionality", "Nacionalidad"),
//				new Transaction_Result(ID, prefix + "lastOrderDate", "Fecha de última orden"),
//				new Transaction_Result(ID, prefix + "lastOrderReason", "Razón de última orden"),
//				new Transaction_Result(ID, prefix + "hasMiClaro", "Registrado en MiClaro")
//		);
	}

	static public class Getter {

		public void info() {
		}
	}
}
