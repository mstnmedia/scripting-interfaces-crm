/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.internal;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Interfaz que contiene las variables con la información de la subscripción
 * seleccionada al inicio de la transacción en curso.
 *
 * @author amatos
 */
public class Scripting_Subscription extends InterfaceBase {

	static Getter SOAP = new Scripting_Subscription.Getter();

	public Scripting_Subscription() {
		super(0, "subscription", SOAP);
		INTERFACES_ID.put("subscription_info", Interfaces.SUBSCRIPTION_INFO_INTERFACE_ID_BASE);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		Interface item = super.getInterface(method, interID, interName, withChildren);
		item.setLabel("Información de la subscripción");
		item.setKeep_results(true);
		return item;
	}

	@Override
	protected Object invokeMethod() throws Exception {
		if (Utils.stringAreEquals("subscription_info", inter.getName())) {
			return transaction;
		}
		return super.invokeMethod();
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter) {
		if (Utils.stringAreEquals("subscription_info", inter.getName())) {
			try {
				return getSubscriptionVariables();
			} catch (Exception ex) {
				Logger.getLogger(Scripting_Subscription.class.getName()).log(Level.SEVERE, null, ex);
				return Arrays.asList();
			}
		}
		return super.getTransactionResultsFromClass(method, inter);
	}

	@Override
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter) throws Exception {
		if (Utils.stringAreEquals("subscription_info", inter.getName())) {
			return getSubscriptionVariables();
		}
		return super.getTransactionResultsFromResponse(value, inter);
	}

	static public List<Transaction_Result> getSubscriptionVariables() throws Exception {
		return Subscription_Ext.getResults(null);
//		int ID = Interfaces.SUBSCRIPTION_INFO_INTERFACE_ID;
//		String prefix = Interfaces.SUBSCRIPTION_INFO_INTERFACE_NAME_BASE + "_";
//		return Arrays.asList(
//				new Transaction_Result(ID, prefix + "id", "ID del servicio"),
//				new Transaction_Result(ID, prefix + "customerID", "ID del cliente"),
//				new Transaction_Result(ID, prefix + "taxID", "RNC"),
//				new Transaction_Result(ID, prefix + "subscriberNo", "Teléfono"),
//				new Transaction_Result(ID, prefix + "subscriberNoDash", "Teléfono con guiones"),
//				new Transaction_Result(ID, prefix + "subscriptionStartDate", "Fecha de inicio de subscripción"),
//				new Transaction_Result(ID, prefix + "status", "Estado"),
//				new Transaction_Result(ID, prefix + "billingAccountNo", "Número de cuenta"),
//				new Transaction_Result(ID, prefix + "billingPlanSOC", "Código del plan"),
//				new Transaction_Result(ID, prefix + "billingPlanDescription", "Descripción del plan"),
//				new Transaction_Result(ID, prefix + "futureBillingPlanSOC", "Código del plan futuro"),
//				new Transaction_Result(ID, prefix + "futureBillingPlanDescription", "Descripción del plan futuro"),
//				new Transaction_Result(ID, prefix + "futureBillingPlanEffectiveDate", "Fecha de efectividad"),
//				new Transaction_Result(ID, prefix + "productTypeCode", "Código del tipo de producto"),
//				new Transaction_Result(ID, prefix + "productTypeName", "Nombre del tipo de producto"),
//				new Transaction_Result(ID, prefix + "productTypeDescription", "Descripción del tipo de producto"),
//				new Transaction_Result(ID, prefix + "offerName", "Cartera"),
//				new Transaction_Result(ID, prefix + "instalationDate", "Fecha de instalación"),
//				new Transaction_Result(ID, prefix + "products", "Productos"),
//				//
//				new Transaction_Result(ID, prefix + "productPlan", "Plan del cliente"),
//				new Transaction_Result(ID, prefix + "extensionsNumber", "No. de extensiones"),
//				new Transaction_Result(ID, prefix + "equiposTelefonicos", "Equipo telefónico"),
//				new Transaction_Result(ID, prefix + "productStatusName", "Estado del producto"),
//				new Transaction_Result(ID, prefix + "productStatusDate", "Fecha de estado del producto"),
//				new Transaction_Result(ID, prefix + "boxesQuantity", "Cantidad de cajas STB"),
//				new Transaction_Result(ID, prefix + "commitmentPeriod", "Periodo del contrato"),
//				new Transaction_Result(ID, prefix + "modemType", "Tipo de módem"),
//				new Transaction_Result(ID, prefix + "bandWidth", "Ancho de banda"),
//				new Transaction_Result(ID, prefix + "emailQuantity", "Cantidad de email"),
//				new Transaction_Result(ID, prefix + "emailAccounts", "Cuentas email"),
//				new Transaction_Result(ID, prefix + "emailAssigned", "Correo asignado"),
//				new Transaction_Result(ID, prefix + "sateliteName", "Nombre de satélite"),
//				new Transaction_Result(ID, prefix + "tnClaroTVAMigrar", "TN Claro TV a migrar"),
//				new Transaction_Result(ID, prefix + "stbQuantity", "Cantidad de STB"),
//				new Transaction_Result(ID, prefix + "stbModels", "Modelos de las STB"),
//				new Transaction_Result(ID, prefix + "contractedPackages", "Paquetes contratados"),
//				new Transaction_Result(ID, prefix + "optionalServices", "Servicios opcionales"),
//				new Transaction_Result(ID, prefix + "billBalanceAmount", "Monto de la factura"),
//				new Transaction_Result(ID, prefix + "billCloseDay", "Día de corte de la factura"),
//				new Transaction_Result(ID, prefix + "billType", "Tipo de factura"),
//				new Transaction_Result(ID, prefix + "billsAverageAmount", "Monto promedio de la factura"),
//				new Transaction_Result(ID, prefix + "billConfirmationStatus", "Estado de confirmación de la factura"),
//				new Transaction_Result(ID, prefix + "billCreditsOrAjustAppliedAmount", "Crédito ajuste aplicado a la factura"),
//				new Transaction_Result(ID, prefix + "billCurrentChargesOrCreditsAmount", "Créditos o cargos actuales a la factura"),
//				new Transaction_Result(ID, prefix + "billCycleCode", "billCycleCode"),
//				new Transaction_Result(ID, prefix + "billCycleRunMonth", "billCycleRunMonth"),
//				new Transaction_Result(ID, prefix + "billCycleRunYear", "billCycleRunYear"),
//				new Transaction_Result(ID, prefix + "billEmail", "Correo de la factura"),
//				new Transaction_Result(ID, prefix + "billPastDueAmount", "billPastDueAmount"),
//				new Transaction_Result(ID, prefix + "billPaymentsReceivedAmount", "Monto de pago recibido de la factura"),
//				new Transaction_Result(ID, prefix + "billPreviousBalanceAmount", "Balance previo de la factura"),
//				new Transaction_Result(ID, prefix + "billSequenceNumber", "billSequenceNumber"),
//				new Transaction_Result(ID, prefix + "billTotalDueAmount", "Monto total"),
//				new Transaction_Result(ID, prefix + "billCloseDate", "Fecha de corte de la factura"),
//				new Transaction_Result(ID, prefix + "billDate", "Fecha de la factura"),
//				new Transaction_Result(ID, prefix + "billCoverageEndDate", "billCoverageEndDate"),
//				new Transaction_Result(ID, prefix + "billCoverageStartDate", "billCoverageStartDate"),
//				new Transaction_Result(ID, prefix + "billDueDate", "Fecha de vencimiento de la factura"),
//				new Transaction_Result(ID, prefix + "includedMinutes", "Minutos incluidos")
//		);
	}

	static public class Getter {

		public void info() {
		}
	}
}
