/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.clarovideo;

import com.claro.ensemble.web.service.InCustomerInfo;
import com.claro.ensemble.web.service.OTTEndpoint;
import com.claro.ensemble.web.service.OTTEndpointService;
import com.claro.ensemble.web.service.OutCustomerInfo;
import com.mstn.scripting.core.Utils;

/**
 *
 * @author amatos
 */
public class ClaroVideo {

	static public OutCustomerInfo GetCustomerInfo(String subscriber) {
		try {
			OTTEndpoint soap = OTTEndpointService.getInstance();
			InCustomerInfo request = new InCustomerInfo();
			request.setSubscriber(subscriber);

			OutCustomerInfo customerInfo = soap.getCustomerInfo(request);
			return customerInfo;
		} catch (Exception ex) {
			Utils.logException(ClaroVideo.class, subscriber, ex);
			return null;
		}
	}

	static public String GetBanFromSubscriber(String subscriber) {
		OutCustomerInfo customerInfo = GetCustomerInfo(subscriber);
		return customerInfo.getBan();
	}
}
