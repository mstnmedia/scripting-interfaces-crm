/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.clarovideo;

import com.claro.ensemble.web.service.OTTEndpointService;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.lang.reflect.Method;

/**
 * Interfaz que agrega a Scripting la información del cliente de Claro Video
 * desde una transacción en curso.
 *
 * @author amatos
 */
public class ClaroVideo_OTT extends InterfaceBase {

	static Object SOAP = null;

	static {
		try {
			if ("true".equals(InterfaceConfigs.get("ignore_claro_video"))) {
				Utils.logWarn(ClaroVideo_OTT.class, "Ignorando interfaces en clase " + ClaroVideo_OTT.class.getCanonicalName());
				SOAP = new Object();
			} else {
				SOAP = OTTEndpointService.getInstance();
			}
		} catch (Exception ex) {
			Utils.logException(ClaroVideo_OTT.class, "Error instanciando endpoint del web service de Claro Video", ex);
			if ("true".equals(InterfaceConfigs.get("ignoreExceptions_claro_video"))) {
				Utils.logWarn(ClaroVideo_OTT.class, "Error ignorado por configuración. Continuará el proceso.");
				SOAP = new Object();
			} else {
				throw ex;
			}
		}
	}

	public ClaroVideo_OTT() {
		super(0, "clarovideo", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("claro_video"));
		return inter;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("claro_video").toString();
		super.test();
	}

}
