/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ensamble;

import _do.com.claro.soa.services.billing.getsubscriptionmemos.GetSubscriptionMemos;
import _do.com.claro.soa.services.billing.getsubscriptionmemos.GetSubscriptionMemos_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 *
 * @author amatos
 */
public class Ensamble_GetSubscriptionMemos extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = Ensamble_GetSubscriptionMemos.class;
		try {
			SOAP = GetSubscriptionMemos_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public Ensamble_GetSubscriptionMemos() {
		super(0, "ensamblegetsubscriptionmemos", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("ensamble_getsubscriptionmemos"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("billingAccount")) {
			field.setLabel("BAN");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("npa")) {
			field.setLabel("No. de Teléfono");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_PHONE);
		} else if (fieldName.endsWith("nxx")) {
			field.setLabel("NXX");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setRequired(false);
		} else if (fieldName.endsWith("stationCode")) {
			field.setLabel("Código de Estación");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setRequired(false);
		} else if (fieldName.endsWith("productType")) {
			field.setLabel("Tipo de producto");
		} else if (fieldName.endsWith("pageNum")) {
			field.setLabel("Número de página");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		}
		return field;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("ensamble_getsubscriptionmemos").toString();
		super.test();
	}

}
