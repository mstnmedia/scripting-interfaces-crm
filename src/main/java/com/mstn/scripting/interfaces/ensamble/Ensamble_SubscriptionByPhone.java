/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ensamble;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.Mixed;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import com.mstn.scripting.interfaces.ext.TelephoneNumber_Ext;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.NotFoundException;

/**
 * @author amatos
 */
public class Ensamble_SubscriptionByPhone extends InterfaceConnectorBase {

	private final static int ID = Interfaces.SUBSCRIPTION_INFO_INTERFACE_ID;
	private final static String NAME = Interfaces.SUBSCRIPTION_INFO_INTERFACE_NAME;
	private final static String LABEL = "Ensamble - Información del Servicio";
	private final static String SERVER_URL = "";
	private final static String VALUE_STEP = "";
	private final static boolean DELETED = false;

	private final String PHONE_NUMBER = "phone_number";
	private final String PRODUCT_CODE = "product_code";

	private Subscription_Ext subscription;

	public Ensamble_SubscriptionByPhone() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		return Arrays.asList(
				//(id, id_interface, name, label, id_type, order_index, default_value)
				new Interface_Field(1, ID, PHONE_NUMBER, "Teléfono", InterfaceFieldTypes.NUMBER, 1, null),
				new Interface_Field(2, ID, PRODUCT_CODE, "Código de producto", InterfaceFieldTypes.TEXT, 2, null)
		);
	}

	@Override
	public List<Transaction_Result> getResults() throws Exception {
		return Subscription_Ext.getResults(subscription);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		ObjectNode json = JSON.newObjectNode();
		response.setJson(json);
		try {
			Interface inter = getInterface();
			ObjectNode form = JSON.toNode(payload.getForm());

			String searchPhone = getPhone(form);
			boolean validPhone = TelephoneNumber_Ext.isValid(searchPhone);

			if (!validPhone) {
				form.remove(PHONE_NUMBER);
				setAsMissingRequest(response, json, form);
			} else {
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
				response.setValue(InterfaceOptions.SUCCESS);
				String productCode = JSON.getString(form, PRODUCT_CODE);
				json.put(PHONE_NUMBER, searchPhone)
						.put(PRODUCT_CODE, productCode);

				subscription = Ensamble.getFirstSubscription(searchPhone, productCode);
				if (subscription.getOriginal() == null) {
					throw new NotFoundException("No se ha encontrado este teléfono.");
				}
				Mixed.SetSADAddress(subscription);

				List<Transaction_Result> results = getResults();
				inter.setInterface_results(results);
				response.setInterface(inter);
			}
		} catch (Exception ex) {
			setAsExceptionResponse(response, ex);
		}
		return response;
	}

	private String getPhone(ObjectNode form) {
		String searchPhone = form.hasNonNull(PHONE_NUMBER) ? form.get(PHONE_NUMBER).asText() : "";
		return searchPhone.replaceAll("[^\\d.]", "");
	}

	@Override
	protected TransactionInterfacesResponse setAsMissingRequest(TransactionInterfacesResponse response, ObjectNode json, ObjectNode form) {
		response.setAction(TransactionInterfaceActions.INVALID_REQUEST);
		response.setValue(InterfaceOptions.MISSING_DATA);
		ArrayNode missing_fields = json.putArray("missing_fields");
		if (!form.hasNonNull(PHONE_NUMBER)) {
			missing_fields.add(PHONE_NUMBER);
		}
		return response;
	}

}
