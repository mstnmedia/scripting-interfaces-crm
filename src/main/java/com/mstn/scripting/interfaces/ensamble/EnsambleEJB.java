/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ensamble;

import com.codetel.postpaid.ejb.EnWrapperService;
import com.codetel.postpaid.ejb.EnWrapperService_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.lang.reflect.Method;

/**
 * Interfac dinámica que implementa el web service Ensamble EJB.
 *
 * @author amatos
 */
public class EnsambleEJB extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = EnsambleEJB.class;
		try {
			SOAP = EnWrapperService_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public EnsambleEJB() {
		super(0, "ensambleejb", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("ensamble_ejb"));
		return inter;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("ensamble_ejb").toString();
		super.test();
	}

}
