/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ensamble;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConnectorBase;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author amatos
 */
public class Ensamble_SubscriptionsByCustomerID extends InterfaceConnectorBase {

	private final static int ID = 3100;
	private final static String NAME = "ensamble_subscriptionsbycustomerid";
	private final static String LABEL = "Ensamble - SubscriptionsByCustomerID";
	private final static String SERVER_URL = "ensamble.claro.com.do";
	private final static String VALUE_STEP = "returned";
	private final static boolean DELETED = false;

	private List<Subscription_Ext> subscriptionList = new ArrayList();

	public Ensamble_SubscriptionsByCustomerID() {
		super(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, DELETED);
	}

	@Override
	public List<Interface_Field> getFields() {
		return getCustomerFields();
	}

	@Override
	public List<Transaction_Result> getResults() throws Exception {
		String prefix = NAME + "_";
		String strSubcriptionList = JSON.toString(subscriptionList);
		return Arrays.asList(
				//(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props)
				new Transaction_Result(1, 0, 0, 0, ID, prefix + "customer_subscriptions", "Lista de Casos",
						strSubcriptionList, "{\"notSave\":true}"
				)
		);
	}

	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) {
		TransactionInterfacesResponse response = new TransactionInterfacesResponse();
		ObjectNode json = JSON.newObjectNode();
		response.setJson(json);
		try {
			Interface inter = getInterface();
			ObjectNode form = JSON.toNode(payload.getForm());

			String customerID = getCustomerIDFromForm(form);
			boolean validID = CustomerID_Ext.isValid(customerID);

			if (!validID) {
				form.remove(CUSTOMER_ID);
				setAsMissingRequest(response, json, form);
			} else {
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
				pageNum = getPageNumFromForm(form);
				json.put(PAGE_NUMBER, pageNum)
						.put(CUSTOMER_ID, customerID);

				subscriptionList = Ensamble.GetSubscriptionsByCustomerID(customerID, pageNum);

				List<Transaction_Result> results = getResults();
				inter.setInterface_results(results);
				response.setInterface(inter);
				response.setValue(InterfaceOptions.SUCCESS);
			}
		} catch (Exception ex) {
			setAsExceptionResponse(response, ex);
		}
		return response;
	}

}
