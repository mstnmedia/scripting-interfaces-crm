/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ensamble;

import _do.com.claro.soa.model.billing.Bill;
import _do.com.claro.soa.model.billing.BillingAccount;
import _do.com.claro.soa.model.billing.Subscription;
import _do.com.claro.soa.model.billing.SubscriptionStatus;
import _do.com.claro.soa.model.billing.Subscriptions;
import _do.com.claro.soa.model.product.Comment;
import _do.com.claro.soa.model.product.Comments;
import _do.com.claro.soa.services.billing.getbilldetailsbyban.GetBillDetailsByBAN;
import _do.com.claro.soa.services.billing.getbilldetailsbyban.GetBillDetailsByBAN_Service;
import _do.com.claro.soa.services.billing.getbillingaccountbyban.GetBillingAccountByBAN;
import _do.com.claro.soa.services.billing.getbillingaccountbyban.GetBillingAccountByBAN_Service;
import _do.com.claro.soa.services.billing.getfixedpostpaidrollover.GetFixedPostpaidRollover;
import _do.com.claro.soa.services.billing.getfixedpostpaidrollover.GetFixedPostpaidRollover_Service;
import _do.com.claro.soa.services.billing.getpostpaidsubscriptionsbycustomerid.GetPostpaidSubscriptionsByCustomerID;
import _do.com.claro.soa.services.billing.getpostpaidsubscriptionsbycustomerid.GetPostpaidSubscriptionsByCustomerID_Service;
import _do.com.claro.soa.services.billing.getsubscriptionsbycustomerid.GetSubscriptionsByCustomerID;
import _do.com.claro.soa.services.billing.getsubscriptionsbycustomerid.GetSubscriptionsByCustomerID_Service;
import _do.com.claro.soa.services.billing.getsubscriptionsbycustomerid.UnsupportedCustomerIdFault;
import _do.com.claro.soa.services.billing.getsubscriptionsbysubscriberno.GetRequest;
import _do.com.claro.soa.services.billing.getsubscriptionsbysubscriberno.GetResponse;
import _do.com.claro.soa.services.billing.getsubscriptionsbysubscriberno.GetSubscriptionsBySubscriberNo;
import _do.com.claro.soa.services.billing.getsubscriptionsbysubscriberno.GetSubscriptionsBySubscriberNo_Service;
import _do.com.claro.soa.services.product.getproductcomments.GetProductComments;
import _do.com.claro.soa.services.product.getproductcomments.GetProductComments_Service;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import com.mstn.scripting.interfaces.ext.TelephoneNumber_Ext;
import com.mstn.scripting.interfaces.fixed.Fixed;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Clase que implementa métodos de consulta a Ensamble-Horizonte para obtener
 * información para la pantalla de crear nueva transacción.
 *
 * @author amatos
 */
public class Ensamble {

	static public Subscription_Ext getFirstSubscription(String searchPhone) throws Exception {
		return getFirstSubscription(searchPhone, "");
	}

	static public Subscription_Ext getFirstSubscription(String searchPhone, String productCode) throws Exception {
		Subscription subscription = null;
		long date1 = DATE.nowAsLong();
		List<Subscription> subscriptions = getSubscriptionsBySubscriberNo(searchPhone);
		long date2 = DATE.nowAsLong();
		Utils.logTrace(Ensamble.class, "First Subscriptions: " + (date2 - date1));
		if (subscriptions.isEmpty()) {
			return new Subscription_Ext(subscription);
		}
		if (Utils.stringNonNullOrEmpty(productCode)) {
			Subscription aux;
			for (int i = 0; i < subscriptions.size(); i++) {
				aux = subscriptions.get(i);
				if (productCode.equals(aux.getProduct().getProductType().getProductCode())) {
					subscription = aux;
					break;
				}
			}
		}
		if (subscription == null) {
			subscription = subscriptions.get(0);
		}

		long date3 = DATE.nowAsLong();
		Utils.logTrace(Ensamble.class, "First Selected: " + (date3 - date2));
		Fixed.SetFixedProduct(subscription);
		long date4 = DATE.nowAsLong();
		Utils.logTrace(Ensamble.class, "First Fixed: " + (date4 - date3));
		Subscription_Ext result = new Subscription_Ext(subscription)
				.setProducts(getProducts(subscriptions, subscription));
		if (Utils.stringNonNullOrEmpty(result.getBillingAccountNo())) {
			long before3 = DATE.nowAsLong();
			result.setBillDetails(Ensamble.GetBillDetailsByBAN(subscription.getBillingAccountNo()));
			long after3 = DATE.nowAsLong();
			Utils.logTrace(Ensamble.class, "First BillDetails: " + (after3 - before3));
			long before4 = DATE.nowAsLong();
			result.setRolloverDetails(Ensamble.GetFixedPostpaidRollover(result.getSubscriberNo()));
			long after4 = DATE.nowAsLong();
			Utils.logTrace(Ensamble.class, "First Rollover: " + (after4 - before4));
		}
		long end = DATE.nowAsLong();
		Utils.logTrace(Ensamble.class, "First Subscription Total: " + (end - date1));
		return result;
	}

	static String getProducts(List<Subscription> list, Subscription subscription) {
		String products = "";
		boolean addComma = false;
		for (int i = 0; i < list.size(); i++) {
			Subscription item = list.get(i);
			boolean hasEvaluableStatus = item.getStatus() == SubscriptionStatus.ACTIVE || item.getStatus() == SubscriptionStatus.SUSPENDED;
			boolean isSameCustomer = Utils.stringAreEquals(
					CustomerID_Ext.toString(item.getCustomerID()),
					CustomerID_Ext.toString(subscription.getCustomerID())
			);
			boolean isSameBillingAccountNo = Utils.stringAreEquals(item.getBillingAccountNo(), subscription.getBillingAccountNo());
			if (hasEvaluableStatus && isSameCustomer && isSameBillingAccountNo) {
				if (addComma) {
					products += ",";
				}
				addComma = true;
				products += item.getProduct() != null && item.getProduct().getProductType() != null
						? item.getProduct().getProductType().getProductCode() : null;
			} else {
			}
		}
		return products;
	}

	static public List<Subscription> getSubscriptionsBySubscriberNo(String searchPhone) throws Exception {
		GetSubscriptionsBySubscriberNo soap = GetSubscriptionsBySubscriberNo_Service.getInstance();
		GetRequest request = new GetRequest();
		request.setTelephoneNumber(new TelephoneNumber_Ext(searchPhone));
		GetResponse getResponse = soap.get(request);

		List<Subscription> subscriptions = new ArrayList<>();
		Subscriptions objSubscriptions = getResponse.getSubscriptions();
		if (objSubscriptions != null) {
			subscriptions = objSubscriptions.getSubscription();
			// Ordena 
			//	- por Status, los ACTIVE primero;
			//	- por SubscriptionStartDate descendientemente, los nulos al final
			subscriptions.sort((Subscription a, Subscription b) -> {
				if (a.getStatus() == SubscriptionStatus.ACTIVE && b.getStatus() != SubscriptionStatus.ACTIVE) {
					return -1;
				} else if (b.getStatus() == SubscriptionStatus.ACTIVE && a.getStatus() != SubscriptionStatus.ACTIVE) {
					return 1;
				}
				if (a.getSubscriptionStartDate() != null && b.getSubscriptionStartDate() != null) {
					return -1 * a.getSubscriptionStartDate().compare(b.getSubscriptionStartDate());
				} else if (a.getSubscriptionStartDate() != null) {
					return -1;
				} else if (b.getSubscriptionStartDate() != null) {
					return 1;
				}
				return 0;
			});
		}
		Utils.logFinest(Ensamble.class, "Ensamble GetSubscriptionsBySubscriberNo subscriptions " + searchPhone + ": \n");
		Utils.logFinest(Ensamble.class, JSON.toString(subscriptions) + "\n\n\n");
		return subscriptions;
	}

	static public List<Subscription_Ext> GetSubscriptionsBySubscriberNo(String searchPhone) throws Exception {
		List<Subscription> subscriptions = getSubscriptionsBySubscriberNo(searchPhone);
		return subscriptions.stream()
				.map(s -> new Subscription_Ext(s).setProducts(getProducts(subscriptions, s)))
				.collect(Collectors.toList());
	}

	static public String GetCustomerIDFromBanAccountNo(String banAccountNo) throws Exception {
		String customerID = null;
		try {
			GetBillingAccountByBAN soap = GetBillingAccountByBAN_Service.getInstance();
			_do.com.claro.soa.services.billing.getbillingaccountbyban.GetRequest request
					= new _do.com.claro.soa.services.billing.getbillingaccountbyban.GetRequest();
			request.setBan(banAccountNo);
			_do.com.claro.soa.services.billing.getbillingaccountbyban.GetResponse getResponse
					= soap.get(request);

			BillingAccount account = getResponse.getBillingAccount();
			if (account != null) {
				customerID = CustomerID_Ext.toString(account.getCustomerID());
			}
		} catch (Exception ex) {
			Utils.logException(Ensamble.class, "Ensamble GetCustomerIDFromBanAccountNo", ex);
		}
		Utils.logTrace(Ensamble.class, "GetCustomerIDFromBanAccountNo " + banAccountNo);
		return customerID;
	}

	static public String GetCustomerIDFromSubscriberNo(String searchPhone) throws Exception {
		String customerID = null;
		List<Subscription> subscriptions = getSubscriptionsBySubscriberNo(searchPhone);
		if (!subscriptions.isEmpty()) {
			customerID = CustomerID_Ext.toString(subscriptions.get(0).getCustomerID());
		}
		Utils.logTrace(Ensamble.class, "CustomerIDFromSubscriberNo " + searchPhone + ": " + customerID);
		return customerID;
	}

	static public List<Subscription_Ext> GetSubscriptionsByCustomerID(String customerID, int pageNum) throws UnsupportedCustomerIdFault {
		GetSubscriptionsByCustomerID soap = GetSubscriptionsByCustomerID_Service.getInstance();
		_do.com.claro.soa.services.billing.getsubscriptionsbycustomerid.GetRequest request = new _do.com.claro.soa.services.billing.getsubscriptionsbycustomerid.GetRequest();
		request.setPageNum(pageNum);
		request.setCustomerID(new CustomerID_Ext(customerID));
		_do.com.claro.soa.services.billing.getsubscriptionsbycustomerid.GetResponse getResponse = soap.get(request);

		List<Subscription> subscriptions = new ArrayList<>();
		Subscriptions objSubscriptions = getResponse.getSubscriptions();
		if (objSubscriptions != null) {
			subscriptions = objSubscriptions.getSubscription();
		}
		return subscriptions.stream()
				.map(s -> new Subscription_Ext(s))
				.collect(Collectors.toList());
	}

	static public List<Subscription> GetPostpaidSubscriptionsByCustomerID(String customerID, int pageNum) {
		List<Subscription> subscriptions = new ArrayList<>();
		try {
			GetPostpaidSubscriptionsByCustomerID soap = GetPostpaidSubscriptionsByCustomerID_Service.getInstance();
			_do.com.claro.soa.services.billing.getpostpaidsubscriptionsbycustomerid.GetRequest request
					= new _do.com.claro.soa.services.billing.getpostpaidsubscriptionsbycustomerid.GetRequest();
			request.setPageNum(pageNum);
			request.setCustomerID(new CustomerID_Ext(customerID));
			_do.com.claro.soa.services.billing.getpostpaidsubscriptionsbycustomerid.GetResponse getResponse = soap.get(request);

			Subscriptions objSubscriptions = getResponse.getSubscriptions();
			if (objSubscriptions != null) {
				subscriptions = objSubscriptions.getSubscription();
			}
			Utils.logFinest(Ensamble.class, "Ensamble postpaid_subscriptions: \n");
			Utils.logFinest(Ensamble.class, JSON.toString(subscriptions) + "\n");
		} catch (Exception ex) {
			Utils.logException(Ensamble.class, "postpaid_subscriptions " + customerID + " " + pageNum, ex);
		}
		return subscriptions;
	}

	static public List<Subscription_Ext> GetPagePostpaidSubscriptionsByCustomerID(String customerID, int pageNum) {
		List<Subscription> subscriptions = GetPostpaidSubscriptionsByCustomerID(customerID, pageNum);
		List<Subscription_Ext> results = subscriptions
				.stream()
				.map((Subscription s) -> {
					Subscription_Ext subscription = new Subscription_Ext(s);
//					if (s.getStatus() == SubscriptionStatus.ACTIVE || s.getStatus() == SubscriptionStatus.SUSPENDED) {
					Fixed.SetFixedProduct(s);
					subscription.setProducts(getProducts(subscriptions, s));
//					}
					return subscription;
				})
				.collect(Collectors.toList());
		return results;
	}

	static public List<Subscription_Ext> GetAllPostpaidSubscriptionsByCustomerID(String customerID) {
		List<Subscription_Ext> subscriptions = new ArrayList<>();
		List<Subscription_Ext> temp;
		int pageNum = 1;
		do {
			temp = GetPagePostpaidSubscriptionsByCustomerID(customerID, pageNum);
			subscriptions.addAll(temp);
			pageNum++;
		} while (!temp.isEmpty());

		return subscriptions;
	}

	static public Bill GetBillDetailsByBAN(String ban) {
		Bill bill = null;
		try {
			GetBillDetailsByBAN soap = GetBillDetailsByBAN_Service.getInstance();
			_do.com.claro.soa.services.billing.getbilldetailsbyban.GetRequest request = new _do.com.claro.soa.services.billing.getbilldetailsbyban.GetRequest();
			request.setBan(ban);

			_do.com.claro.soa.services.billing.getbilldetailsbyban.GetResponse response = soap.get(request);
			bill = response.getBill();
			Utils.logFinest(Ensamble.class, "Ensamble GetBillDetailsByBAN: \n");
			Utils.logFinest(Ensamble.class, JSON.toString(bill) + "\n");
		} catch (Exception ex) {
			Utils.logException(Ensamble.class, ban, ex);
		}
		return bill;
	}

	static public _do.com.claro.soa.services.billing.getfixedpostpaidrollover.GetResponse.Result
			GetFixedPostpaidRollover(String subscriberNo) {
		_do.com.claro.soa.services.billing.getfixedpostpaidrollover.GetResponse.Result result = null;
		try {
			GetFixedPostpaidRollover soap = GetFixedPostpaidRollover_Service.getInstance();
			_do.com.claro.soa.services.billing.getfixedpostpaidrollover.GetRequest request = new _do.com.claro.soa.services.billing.getfixedpostpaidrollover.GetRequest();
			request.setSubscriberNo(new TelephoneNumber_Ext(subscriberNo));

			_do.com.claro.soa.services.billing.getfixedpostpaidrollover.GetResponse response = soap.get(request);
			result = response.getResult();
			Utils.logFinest(Ensamble.class, "Ensamble GetFixedPostpaidRollover: \n");
			Utils.logFinest(Ensamble.class, JSON.toString(result) + "\n");
		} catch (Exception ex) {
			Utils.logException(Ensamble.class, subscriberNo, ex);
		}
		return result;
	}

	static public List<Comment> GetProductComments(String customerID, String productTypeCode, String subscriberNo, int pageNum) {
		List<Comment> comments = new ArrayList();
		try {
			GetProductComments soap = GetProductComments_Service.getInstance();
			_do.com.claro.soa.services.product.getproductcomments.GetRequest request = new _do.com.claro.soa.services.product.getproductcomments.GetRequest();
			request.setCustomerID(new CustomerID_Ext(customerID));
			request.setServiceType(productTypeCode);
			request.setPhone(new TelephoneNumber_Ext(subscriberNo));
			request.setPageNum(pageNum);

			_do.com.claro.soa.services.product.getproductcomments.GetResponse response = soap.get(request);
			Comments commentsObj = response.getComments();
			comments = commentsObj.getComment();
			Utils.logFinest(Ensamble.class, "Ensamble GetProductComments: \n");
			Utils.logFinest(Ensamble.class, JSON.toString(comments) + "\n");
		} catch (Exception ex) {
			Utils.logException(Ensamble.class, customerID + "_" + productTypeCode + "_" + subscriberNo + "_" + pageNum, ex);
		}
		return comments;
	}

}
