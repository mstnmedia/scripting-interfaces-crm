/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ensamble;

import com.codetel.postpaid.ejb.EnWrapperService;
import com.codetel.postpaid.ejb.EnWrapperService_Service;
import com.codetel.postpaid.ejb.types.InGetBanInfo;
import com.codetel.postpaid.ejb.types.OutGetBanInfo;

/**
 *
 * @author amatos
 */
public class Ensamble_EJB {

	static public final EnWrapperService SOAP = EnWrapperService_Service.getInstance();

	static public OutGetBanInfo GetBanInfo(Long banID, String clientSystem) throws Exception {
		InGetBanInfo request = new InGetBanInfo();
		request.setBanId(banID);
		request.setClientSystem(clientSystem);
		OutGetBanInfo response = SOAP.getBanInfo(request);
		return response;
	}

}
