/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.ensamble;

import _do.com.claro.soa.services.billing.getbillshistorybybillingaccountno.GetBillsHistoryByBillingAccountNo;
import _do.com.claro.soa.services.billing.getbillshistorybybillingaccountno.GetBillsHistoryByBillingAccountNo_Service;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 *
 * @author amatos
 */
public class Ensamble_GetBillsHistoryByBillingAccountNo extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = Ensamble_GetBillsHistoryByBillingAccountNo.class;
		try {
			SOAP = GetBillsHistoryByBillingAccountNo_Service.getInstance();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public Ensamble_GetBillsHistoryByBillingAccountNo() {
		super(0, "ensamblegetbillshistorybybillingaccountno", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("ensamble_getbillshistorybybillingaccountno"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("billingAccountNumber")) {
			field.setLabel("BAN");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("pageNum")) {
			field.setLabel("Número de página");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		}
		return field;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("ensamble_getbillshistorybybillingaccountno").toString();
		super.test();
	}

}
