/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.dth;

import com.mstn.scripting.core.Utils;
import satsservices.ClientProfile;
import satsservices.ClientSubscription;
import satsservices.DTHSubscription;
import satsservices.SATSServices;
import satsservices.SATSServicesSoap;

/**
 * Clase que permite la consulta de clientes por su número de SmartCard
 * registrado en DTH. Esta clase 
 *
 * @author amatos
 */
public class DTH {

	static public DTHSubscription GetClientProfile(String subscriptionNumber) {
		DTHSubscription subscription = null;
		try {
			SATSServicesSoap soap = SATSServices.getInstance();
			ClientProfile response = soap.getClientProfile(subscriptionNumber);
			subscription = response.getSubscription();
		} catch (Exception ex) {
			Utils.logException(DTH.class, subscriptionNumber, ex);
		}
		return subscription;
	}

	static public ClientSubscription GetClientProfileById(String id, String opType) {
		ClientSubscription clientSubscription = null;
		try {
			SATSServicesSoap soap = SATSServices.getInstance();
			clientSubscription = soap.getClientProfileById(id, opType);

		} catch (Exception ex) {
			Utils.logException(DTH.class, id + " " + opType, ex);
		}
		return clientSubscription;
	}
}
