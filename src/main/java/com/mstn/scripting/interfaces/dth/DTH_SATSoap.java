/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.dth;

import com.mstn.scripting.core.Utils;
import satsservices.ArrayOfClientSubscription;
import satsservices.ClientProfile;
import satsservices.ClientSubscription;
import satsservices.CommandHistory;
import satsservices.Job;
import satsservices.OperationHistory;
import satsservices.SATSServices;
import satsservices.STBInfo;

/**
 * Clase que crea una instancia del endpoint de DTH e implementa todos sus
 * métodos para disponerlos a una interfaz dinámica.
 *
 * @author amatos
 */
public class DTH_SATSoap implements satsservices.SATSServicesSoap {

	private final satsservices.SATSServicesSoap base;

	public DTH_SATSoap() {
		try {
			this.base = SATSServices.getInstance();
		} catch (Exception ex) {
			Utils.logException(this.getClass(), "Error instanciando endpoint DTH_SAT", ex);
			throw ex;
		}
	}

	@Override
	public String createCustomer(String id, String accountNumber, int typeId, String plan, String technician) {
		return base.createCustomer(id, accountNumber, typeId, plan, technician);
	}

	@Override
	public String createSpace(String id, String espace, String technician) {
		return base.createSpace(id, espace, technician);
	}

	@Override
	public String addPackage(String id, String opType, String _package, String technician) {
		return base.addPackage(id, opType, _package, technician);
	}

	@Override
	public String removePackage(String id, String opType, String _package, String technician) {
		return base.removePackage(id, opType, _package, technician);
	}

	@Override
	public STBInfo getSTBInfo(String stbId) {
		return base.getSTBInfo(stbId);
	}

	@Override
	public String getSpaceCount(String pon) {
		return base.getSpaceCount(pon);
	}

	@Override
	public String activateSTB(String id, String opType, String stb, String smc, String technician) {
		return base.activateSTB(id, opType, stb, smc, technician);
	}

	@Override
	public String change(String id, String opType, String oldSTB, String oldSMC, String newSTB, String newSMC, String technician) {
		return base.change(id, opType, oldSTB, oldSMC, newSTB, newSMC, technician);
	}

	@Override
	public String changePlan(String id, String oldPlan, String newPlan, String opType, String technician) {
		return base.changePlan(id, oldPlan, newPlan, opType, technician);
	}

	@Override
	public String removeClientProfile(String orden, String technician) {
		return base.removeClientProfile(orden, technician);
	}

	@Override
	public String removeSpace(String id, String space, String technician) {
		return base.removeSpace(id, space, technician);
	}

	@Override
	public String suspend(String id, String opType, String technician) {
		return base.suspend(id, opType, technician);
	}

	@Override
	public String reactivate(String id, String opType, String technician) {
		return base.reactivate(id, opType, technician);
	}

	@Override
	public String refresh(String id, String opType, String technician) {
		return base.refresh(id, opType, technician);
	}

	@Override
	public String reset(String id, String opType, String technician) {
		return base.reset(id, opType, technician);
	}

	@Override
	public String clearMessages(String id, String opType, String technician) {
		return base.clearMessages(id, opType, technician);
	}

	@Override
	public String sendMessages(String id, String opType, String messageId, String technician) {
		return base.sendMessages(id, opType, messageId, technician);
	}

	@Override
	public ClientProfile getClientProfile(String subscriptionNumber) {
		return base.getClientProfile(subscriptionNumber);
	}

	@Override
	public ClientSubscription getClientProfileById(String id, String opType) {
		return base.getClientProfileById(id, opType);
	}

	@Override
	public ArrayOfClientSubscription getClientProfileBySTB(String id) {
		return base.getClientProfileBySTB(id);
	}

	@Override
	public String changeStatesEquipment(String order) {
		return base.changeStatesEquipment(order);
	}

	@Override
	public String getBlacklistStatus(String serial) {
		return base.getBlacklistStatus(serial);
	}

	@Override
	public String unlockSmartcard(String smartcard, String user) {
		return base.unlockSmartcard(smartcard, user);
	}

	@Override
	public String pairSmartcardSTB(String smartcard, String stb, String grilla) {
		return base.pairSmartcardSTB(smartcard, stb, grilla);
	}

	@Override
	public String unPairSmartcard(String smartcard) {
		return base.unPairSmartcard(smartcard);
	}

	@Override
	public OperationHistory getOperationsHistory(String smartcard, String quantityMax) {
		return base.getOperationsHistory(smartcard, quantityMax);
	}

	@Override
	public CommandHistory getCommandsHistory(String smartcard, String quantityMax) {
		return base.getCommandsHistory(smartcard, quantityMax);
	}

	@Override
	public Job getJobInformation(String jobName) {
		return base.getJobInformation(jobName);
	}

	@Override
	public String changeCronjob(String name, String time, String groupName, String option) {
		return base.changeCronjob(name, time, groupName, option);
	}

	@Override
	public String sendToBlacklist(String smartcard, String setTopBox, String customerId, String tipoQuitese) {
		return base.sendToBlacklist(smartcard, setTopBox, customerId, tipoQuitese);
	}

	@Override
	public String refreshPackage(String id, String opType, String technician) {
		return base.refreshPackage(id, opType, technician);
	}

}
