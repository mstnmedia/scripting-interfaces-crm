/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.dth;

import com.mstn.scripting.core.RESTTemplate;
import com.mstn.scripting.core.RESTTemplate.Request;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.InterfaceConfigs;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author amatos
 */
public class DTH_RESTApi {

	static String DTH_REST_API_URL = "http://nttappsweb0009/satsws/api";
	static String DTH_REST_SYSTEMNAME = "Scripting";
	static String DTH_REST_SYSTEMPASSWORD = "DR&eeNz4<3yVClaroxGfn";
	static HashMap<String, Class> GENERIC_CLASS = new HashMap();

	public DTH_RESTApi() {
		DTH_REST_API_URL = InterfaceConfigs.get("dth_rest_api");
		DTH_REST_SYSTEMNAME = InterfaceConfigs.get("dth_rest_systemname");
		DTH_REST_SYSTEMPASSWORD = InterfaceConfigs.get("dth_rest_systempassword");
	}

	static String setToken(String user, Request request) throws Exception {
		try {
			LoginDTH login = new LoginDTH(DTH_REST_SYSTEMNAME, DTH_REST_SYSTEMPASSWORD, user);
			ResponseEntity<AuthenticateResponse> response = RESTTemplate.post(DTH_REST_API_URL + "/Auth/user", login, AuthenticateResponse.class);
			AuthenticateResponse responseBody = response.getBody();
			if (responseBody.Succeeded) {
				String token = responseBody.Value.Token;
				request.headers.setBearerAuth(token);
				return token;
			}
			String errorMsg = responseBody.Message;
			if (Utils.nonNullOrEmpty(responseBody.Errors)) {
				for (int i = 0; i < responseBody.Errors.size(); i++) {
					GenericResponseErrorDTH error = responseBody.Errors.get(i);
					errorMsg += "\n" + error.Code + " " + error.Message;
				}
			}
			throw new Exception(errorMsg);
		} catch (Exception ex) {
			throw new Exception("La autenticación no fue éxitosa: " + ex.getMessage());
		}
	}

	public CommandResponse reset(DTHRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Command/Send/Reset/Customer/" + params.number + "/" + params.user;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(params.user, request);
		ResponseEntity<CommandResponse> response = RESTTemplate.request(request, CommandResponse.class);
		return response.getBody();
	}

	public CommandResponse reactivate(DTHRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Command/Send/Reactivate/Customer/" + params.number + "/" + params.user;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(params.user, request);
		ResponseEntity<CommandResponse> response = RESTTemplate.request(request, CommandResponse.class);
		return response.getBody();
	}

	public CommandResponse refresh(DTHRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Command/Send/Refresh/Customer/" + params.number + "/" + params.user;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(params.user, request);
		ResponseEntity<CommandResponse> response = RESTTemplate.request(request, CommandResponse.class);
		return response.getBody();
	}

	public CommandResponse removeMessage(DTHRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Command/Send/RemoveMessage/Customer/" + params.number + "/" + params.user;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(params.user, request);
		ResponseEntity<CommandResponse> response = RESTTemplate.request(request, CommandResponse.class);
		return response.getBody();

	}

	private Request getSTBRequest(String url, EquipmentCommandRequest params) throws Exception {
		if (params != null && Utils.nonNullOrEmpty(params.equiments)) {
			params.equiments.forEach(e -> e.User = params.user);
		}
		Request request = new Request(url, HttpMethod.POST, params.equiments);
		setToken(params.user, request);
		return request;
	}

	public CommandResponse refreshSTB(EquipmentCommandRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Command/Send/Refresh";
		Request request = getSTBRequest(url, params);
		ResponseEntity<CommandResponse> response = RESTTemplate.request(request, CommandResponse.class);
		return response.getBody();
	}

	public CommandResponse factoryResetSTB(EquipmentCommandRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Command/Send/FactoryReset";
		Request request = getSTBRequest(url, params);
		ResponseEntity<CommandResponse> response = RESTTemplate.request(request, CommandResponse.class);
		return response.getBody();
	}

	public CommandResponse unlockSuscriberSTB(EquipmentCommandRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Command/Send/UnlockSuscriber";
		Request request = getSTBRequest(url, params);
		ResponseEntity<CommandResponse> response = RESTTemplate.request(request, CommandResponse.class);
		return response.getBody();
	}

	public CommandResponse refreshPackageSTB(EquipmentCommandRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Command/Send/RefreshPackage";
		Request request = getSTBRequest(url, params);
		ResponseEntity<CommandResponse> response = RESTTemplate.request(request, CommandResponse.class);
		return response.getBody();
	}

	public ClientProfileResponse getClientProfile(DTHRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Netcracker/Customer/Profile/" + params.number;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(params.user, request);
		ResponseEntity<ClientProfileResponse> response = RESTTemplate.request(request, ClientProfileResponse.class);
		return response.getBody();
	}

	public ClientProfileResponse getClientProfileBySMC(ClientProfileSMCRequest params) throws Exception {
		String url = DTH_REST_API_URL + "/Netcracker/Customer/Profile/Smartcard/" + params.smartCard;
		Request request = new Request(url, HttpMethod.GET, null);
		setToken(params.user, request);
		ResponseEntity<ClientProfileResponse> response = RESTTemplate.request(request, ClientProfileResponse.class);
		return response.getBody();
	}

	static class LoginDTH {

		public String UserName;
		public String Password;
		public String UserCard;

		public LoginDTH() {
		}

		public LoginDTH(String Username, String Password, String UserCard) {
			this.UserName = Username;
			this.Password = Password;
			this.UserCard = UserCard;
		}
	}

	static public class GenericResponseErrorDTH {

		public int Code;
		public String Message;

		public GenericResponseErrorDTH() {
		}
	}

	static public class AuthenticateResponse {

		public boolean Succeeded;
		public String Message;
		public AuthenticateValueResponse Value;
		public List<GenericResponseErrorDTH> Errors;

		public AuthenticateResponse() {
		}
	}

	static public class AuthenticateValueResponse {

		public String Token;
		public String RefreshToken;
		public Double ExpireTime;

		public AuthenticateValueResponse() {
		}
	}

	static public class DTHRequest {

		public String user;
		public long number;

		public DTHRequest() {
		}

		public DTHRequest(String user, long number) {
			this.user = user;
			this.number = number;
		}
	}

	static public class ClientProfileSMCRequest {

		public String user;
		public String smartCard;

		public ClientProfileSMCRequest() {
		}

		public ClientProfileSMCRequest(String user, String smartCard) {
			this.user = user;
			this.smartCard = smartCard;
		}
	}

	static public class CommandResponse {

		public boolean Succeeded;
		public String Message;
		public List<GenericResponseErrorDTH> Errors;

		public CommandResponse() {
		}
	}

	static public class RefreshValueResponse {

		public String Token;
		public String RefreshToken;

		public RefreshValueResponse() {
		}
	}

	static public class ClientProfileResponse {

		public boolean Succeeded;
		public String Message;
		public ClientProfileValueResponse Value;
		public List<GenericResponseErrorDTH> Errors;

		public ClientProfileResponse() {
		}
	}

	static public class ClientProfileValueResponse {

		public CustomerInfo customerInfo;
		public Plan Plan;
		public List<Package> Package;
		public List<Equipment> Equipments;

		public ClientProfileValueResponse() {
		}

	}

	static public class CustomerInfo {

		public String Id;// "9500035",
		public String Number;// "101-001-3911",
		public String Name;// "COMPANIA DOMINICANA DE TELEFONO",
		public String Type;// "BUSINESS",
		public String Status;// "ACTIVE",
		public Date UpdateAt;// "2015-12-01T00:00:00.000-04:00"

		public CustomerInfo() {
		}

	}

	static public class Plan {

		public String Id;// "CVA0000"
		public String Name;// "Basico"

		public Plan() {
		}

	}

	static public class Package {

		public String Id;// "157571"
		public String Name;// "Paquete Canales HD Plus"
		public String Type;// "HD+"

		public Package() {
		}

	}

	static public class Equipment {

		public String prop;
		public long Id;// 751256013,
		public String ComponentCode;// "DTH STB",
		public String SetTopBox;// "0000000002",
		public String Smartcard;// "1303939931",
		public Date InstallDate;// "2018-11-16T13:33:21-04:00",
		public String Status;// "Active"

		public Equipment() {
		}

	}

	static public class EquipmentCommandParam {

		public long Id;// 751256013,
		public String User;// "56798",
		public String Ficticio;// "XXX-XXX-XXXX",
		public String SetTopBox;// "1303939931",
		public String Smartcard;// "1303939931",

		public EquipmentCommandParam() {
		}

	}

	static public class EquipmentCommandRequest {

		public String user;
		public List<EquipmentCommandParam> equiments;

		public EquipmentCommandRequest() {
		}
	}

}
