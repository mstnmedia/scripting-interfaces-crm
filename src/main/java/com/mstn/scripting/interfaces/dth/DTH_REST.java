/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.dth;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Clase que implementa métodos del web service de Diagnóstico IDW para ser
 * usados en la interfaz estática {@link DW_DiagnosticResult}.
 *
 * @author amatos
 */
public class DTH_REST extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = DTH_REST.class;
		try {
			SOAP = new DTH_RESTApi();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public DTH_REST() {
		super(0, "dthrest", SOAP);
		IGNORED_METHODS.addAll(Arrays.asList("setToken", "getSTBRequest"));
	}

	@Override
	public List<Method> getMethods() {
		List<Method> methods = Arrays.asList(getCLASS().getDeclaredMethods())
				.stream()
				//sólo muestre los métodos públicos (modifiers == 1)
				.filter(i -> !IGNORED_METHODS.contains(i.getName()) && i.getModifiers() == 1)
				.sorted((a, b) -> a.getName().compareTo(b.getName()))
				.collect(Collectors.toList());
		return methods;
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("dth_rest_api"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("user") || fieldName.endsWith("User")) {
			return null;
		} else if (fieldName.endsWith("number")) {
			field.setLabel("Número de Suscripción");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_TEXTUAL_LENGTH
					.replace("@operator", "==").replace("@length", "10"));
		} else if (fieldName.endsWith("smartCard") || fieldName.endsWith("Smartcard")) {
			field.setLabel("SmartCard");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_TEXTUAL_LENGTH
					.replace("@operator", ">=").replace("@length", "10"));
		} else if (fieldName.endsWith("Id")) {
			field.setLabel("ID");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("Ficticio")) {
			field.setLabel("Número de subscripción (con guiones)");
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_DASHED_PHONE);
		} else if (fieldName.endsWith("SetTopBox")) {
			field.setLabel("SetTopBox");
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_TEXTUAL_LENGTH
					.replace("@operator", ">=").replace("@length", "10"));
		} else if (fieldName.endsWith("_equiments")) {
			field.setLabel("Equipos");
		}
		return field;
	}

	@Override
	protected Object getParamValueFromForm(Class paramType, String paramName) throws Exception {
		if (paramName.endsWith("user")) {
			return user.getId();
		}
		return super.getParamValueFromForm(paramType, paramName);
	}

	@Override
	protected List<Interface_Field> getStepFields() throws Exception {
		List<Interface_Field> stepFields = super.getStepFields().stream()
				.filter(i -> !i.getName().endsWith("user"))
				.collect(Collectors.toList());
		return stepFields;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("dth_rest_api").toString();
		InterfaceConfigs.get("dth_rest_systemname").toString();
		InterfaceConfigs.get("dth_rest_systempassword").toString();
		super.test();
	}

}
