/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.interfaces.dth;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import java.lang.reflect.Method;

/**
 * Clase que implementa métodos del web service de Diagnóstico IDW para ser
 * usados en la interfaz estática {@link DW_DiagnosticResult}.
 *
 * @author amatos
 */
public class DTH_SAT extends InterfaceBase {

	static Object SOAP = null;

	static {
		Class _class = DTH_SAT.class;
		try {
			SOAP = new DTH_SATSoap();
		} catch (Exception ex) {
			Utils.logException(_class, "Error instanciando endpoint");
			throw ex;
		}
	}

	public DTH_SAT() {
		super(0, "dth", SOAP);
	}

	@Override
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		inter = super.getInterface(method, interID, interName, withChildren);
		inter.setServer_url(InterfaceConfigs.get("dth_satsservices"));
		return inter;
	}

	@Override
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		Interface_Field field = super.getInterfaceField(_class, inter, fieldName, parentName, prefix);
		if (fieldName.endsWith("_id")) {
			field.setLabel("ID");
		} else if (fieldName.endsWith("opType")) {
			field.setLabel("Tipo de Operador");
		} else if (fieldName.endsWith("stb")) {
			field.setLabel("STB");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("smc") || fieldName.endsWith("smartcard")) {
			field.setLabel("SmartCard");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_TEXTUAL_LENGTH
					.replace("@compare", ">=").replace("@value", "10"));
		} else if (fieldName.endsWith("technician")) {
			field.setLabel("Técnico");
		} else if (fieldName.endsWith("package")) {
			field.setLabel("Paquete");
		} else if (fieldName.endsWith("oldSTB")) {
			field.setLabel("STB Anterior");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("oldSMC")) {
			field.setLabel("SmartCard Anterior");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_TEXTUAL_LENGTH
					.replace("@compare", ">=").replace("@value", "10"));
		} else if (fieldName.endsWith("newSTB")) {
			field.setLabel("STB Nueva");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("newSMC")) {
			field.setLabel("SmartCard Nueva");
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_TEXTUAL_LENGTH
					.replace("@compare", ">=").replace("@value", "10"));
		} else if (fieldName.endsWith("oldPlan")) {
			field.setLabel("Plan Anterior");
		} else if (fieldName.endsWith("newPlan")) {
			field.setLabel("Plan Nuevo");
		} else if (fieldName.endsWith("accountNumber")) {
			field.setLabel("Número de Cuenta");
		} else if (fieldName.endsWith("typeId")) {
			field.setLabel("Tipo de ID");
		} else if (fieldName.endsWith("plan")) {
			field.setLabel("Plan");
		} else if (fieldName.endsWith("espace")) {
			field.setLabel("Espacio");
		} else if (fieldName.endsWith("subscriptionNumber")) {
			field.setLabel("Número de Suscripción");
		} else if (fieldName.endsWith("stbId")) {
			field.setLabel("ID de STB");
		} else if (fieldName.endsWith("pon")) {
			field.setLabel("PON");
		} else if (fieldName.endsWith("orden")) {
			field.setLabel("Orden");
		} else if (fieldName.endsWith("messageId")) {
			field.setLabel("ID de Mensaje");
		} else if (fieldName.endsWith("name")) {
			field.setLabel("Nombre");
		} else if (fieldName.endsWith("time")) {
			field.setLabel("Fecha");
			field.setId_type(InterfaceFieldTypes.DATE);
		} else if (fieldName.endsWith("groupName")) {
			field.setLabel("Nombre de Grupo");
		} else if (fieldName.endsWith("option")) {
			field.setLabel("Opción");
		} else if (fieldName.endsWith("order")) {
			field.setLabel("Orden");
		} else if (fieldName.endsWith("serial")) {
			field.setLabel("Serie");
		} else if (fieldName.endsWith("quantityMax")) {
			field.setLabel("Cantidad Máxima");
			field.setId_type(InterfaceFieldTypes.NUMBER);
		} else if (fieldName.endsWith("jobName")) {
			field.setLabel("Nombre");
		} else if (fieldName.endsWith("grilla")) {
			field.setLabel("Grilla");
		} else if (fieldName.endsWith("user")) {
			field.setLabel("Usuario");
		}
		return field;
	}

	@Override
	@SuppressWarnings({"ResultOfMethodCallIgnored", "RedundantStringToString"})
	public void test() throws Exception {
		InterfaceConfigs.get("dth_satsservices").toString();
		super.test();
	}

}
