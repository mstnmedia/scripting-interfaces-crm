
package com.codetel.postpaid.ejb;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import com.codetel.postpaid.ejb.types.InAddFavoriteInfo;
import com.codetel.postpaid.ejb.types.InAssignResourceNumberInfo;
import com.codetel.postpaid.ejb.types.InCancelSubscriberInfo;
import com.codetel.postpaid.ejb.types.InChangeAddressCatInfo;
import com.codetel.postpaid.ejb.types.InChangeFavoriteInfo;
import com.codetel.postpaid.ejb.types.InCheckStreetInfo;
import com.codetel.postpaid.ejb.types.InCheckToAddFavoriteInfo;
import com.codetel.postpaid.ejb.types.InCheckToChangeFavoriteInfo;
import com.codetel.postpaid.ejb.types.InCreateAdjReversalInfo;
import com.codetel.postpaid.ejb.types.InCreateAdjustmentByBanInfo;
import com.codetel.postpaid.ejb.types.InCreateAdjustmentByCrgInfo;
import com.codetel.postpaid.ejb.types.InCreateBillImageInfo;
import com.codetel.postpaid.ejb.types.InCreateChargeInfo;
import com.codetel.postpaid.ejb.types.InCreateLoanEquipmentInfo;
import com.codetel.postpaid.ejb.types.InCreateMemoInfo;
import com.codetel.postpaid.ejb.types.InCreatePaymentArrangInfo;
import com.codetel.postpaid.ejb.types.InCreateWlsSubInfo;
import com.codetel.postpaid.ejb.types.InCreateWlsSubSubLvlInfo;
import com.codetel.postpaid.ejb.types.InGetAvailableFavCountInfo;
import com.codetel.postpaid.ejb.types.InGetBanDetailsInfo;
import com.codetel.postpaid.ejb.types.InGetBanInfo;
import com.codetel.postpaid.ejb.types.InGetBillingHistByBanInfo;
import com.codetel.postpaid.ejb.types.InGetCreditLimitInfo;
import com.codetel.postpaid.ejb.types.InGetCustListBySubInfo;
import com.codetel.postpaid.ejb.types.InGetFavoriteListInfo;
import com.codetel.postpaid.ejb.types.InGetPaymentArrangByBanInfo;
import com.codetel.postpaid.ejb.types.InGetPaymentArrangQuoteInfo;
import com.codetel.postpaid.ejb.types.InGetPenaltyCalcInfo;
import com.codetel.postpaid.ejb.types.InGetPenaltyCommitmentsInfo;
import com.codetel.postpaid.ejb.types.InGetSubscriberInfo;
import com.codetel.postpaid.ejb.types.InRegisterLoyaltyProgInfo;
import com.codetel.postpaid.ejb.types.InReleaseResourceNumberInfo;
import com.codetel.postpaid.ejb.types.InRestoreSubscriberInfo;
import com.codetel.postpaid.ejb.types.InSuspendSubscriberInfo;
import com.codetel.postpaid.ejb.types.InUpdateSocBanLevelInfo;
import com.codetel.postpaid.ejb.types.InUpdateSubscriberFtrSwPrm;
import com.codetel.postpaid.ejb.types.InUpdateSubscriberPlanInfo;
import com.codetel.postpaid.ejb.types.InUpdateSubscriberSIMInfo;
import com.codetel.postpaid.ejb.types.InUpdateWirelineLeaseInfo;
import com.codetel.postpaid.ejb.types.OutAddFavoriteInfo;
import com.codetel.postpaid.ejb.types.OutAssignResourceNumberInfo;
import com.codetel.postpaid.ejb.types.OutCancelSubscriberInfo;
import com.codetel.postpaid.ejb.types.OutChangeAddressCatInfo;
import com.codetel.postpaid.ejb.types.OutChangeFavoriteInfo;
import com.codetel.postpaid.ejb.types.OutCheckStreetInfo;
import com.codetel.postpaid.ejb.types.OutCreateAdjReversalInfo;
import com.codetel.postpaid.ejb.types.OutCreateAdjustmentByBanInfo;
import com.codetel.postpaid.ejb.types.OutCreateAdjustmentByCrgInfo;
import com.codetel.postpaid.ejb.types.OutCreateBillImageInfo;
import com.codetel.postpaid.ejb.types.OutCreateChargeInfo;
import com.codetel.postpaid.ejb.types.OutCreateLoanEquipmentInfo;
import com.codetel.postpaid.ejb.types.OutCreateMemoInfo;
import com.codetel.postpaid.ejb.types.OutCreatePaymentArrangInfo;
import com.codetel.postpaid.ejb.types.OutCreateWlsSubInfo;
import com.codetel.postpaid.ejb.types.OutCreateWlsSubSubLvlInfo;
import com.codetel.postpaid.ejb.types.OutGetAvailableFavCountInfo;
import com.codetel.postpaid.ejb.types.OutGetBanDetailsInfo;
import com.codetel.postpaid.ejb.types.OutGetBanInfo;
import com.codetel.postpaid.ejb.types.OutGetBillingHistByBanInfo;
import com.codetel.postpaid.ejb.types.OutGetCreditLimitInfo;
import com.codetel.postpaid.ejb.types.OutGetCustListBySubInfo;
import com.codetel.postpaid.ejb.types.OutGetFavoriteListInfo;
import com.codetel.postpaid.ejb.types.OutGetPaymentArrangByBanInfo;
import com.codetel.postpaid.ejb.types.OutGetPaymentArrangQuoteInfo;
import com.codetel.postpaid.ejb.types.OutGetPenaltyCalcInfo;
import com.codetel.postpaid.ejb.types.OutGetPenaltyCommitmentsInfo;
import com.codetel.postpaid.ejb.types.OutGetSubscriberInfo;
import com.codetel.postpaid.ejb.types.OutRegisterLoyaltyProgInfo;
import com.codetel.postpaid.ejb.types.OutReleaseResourceNumberInfo;
import com.codetel.postpaid.ejb.types.OutRestoreSubscriberInfo;
import com.codetel.postpaid.ejb.types.OutSuspendSubscriberInfo;
import com.codetel.postpaid.ejb.types.OutUpdateSocBanLevelInfo;
import com.codetel.postpaid.ejb.types.OutUpdateSubscriberFtrSwPrm;
import com.codetel.postpaid.ejb.types.OutUpdateSubscriberPlanInfo;
import com.codetel.postpaid.ejb.types.OutUpdateSubscriberSIMInfo;
import com.codetel.postpaid.ejb.types.OutUpdateWirelineLeaseInfo;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.3-b02-
 * Generated source version: 2.1
 * 
 */
@WebService(name = "EnWrapperService", targetNamespace = "http://ejb.postpaid.codetel.com/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    com.codetel.postpaid.ejb.types.ObjectFactory.class,
    com.codetel.postpaid.ejb.ObjectFactory.class
})
public interface EnWrapperService {


    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutAddFavoriteInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//addFavorite")
    @WebResult(name = "addFavoriteResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutAddFavoriteInfo addFavorite(
        @WebParam(name = "addFavoriteElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InAddFavoriteInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCancelSubscriberInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//cancelSubscriber")
    @WebResult(name = "cancelSubscriberResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCancelSubscriberInfo cancelSubscriber(
        @WebParam(name = "cancelSubscriberElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCancelSubscriberInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutChangeAddressCatInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//changeAddressCatalog")
    @WebResult(name = "changeAddressCatalogResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutChangeAddressCatInfo changeAddressCatalog(
        @WebParam(name = "changeAddressCatalogElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InChangeAddressCatInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutChangeFavoriteInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//changeFavorite")
    @WebResult(name = "changeFavoriteResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutChangeFavoriteInfo changeFavorite(
        @WebParam(name = "changeFavoriteElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InChangeFavoriteInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCheckStreetInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//checkStreetOnBanById")
    @WebResult(name = "checkStreetOnBanByIdResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCheckStreetInfo checkStreetOnBanById(
        @WebParam(name = "checkStreetOnBanByIdElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCheckStreetInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutAddFavoriteInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//checkToAddFavorite")
    @WebResult(name = "checkToAddFavoriteResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutAddFavoriteInfo checkToAddFavorite(
        @WebParam(name = "checkToAddFavoriteElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCheckToAddFavoriteInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutChangeFavoriteInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//checkToChangeFavorite")
    @WebResult(name = "checkToChangeFavoriteResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutChangeFavoriteInfo checkToChangeFavorite(
        @WebParam(name = "checkToChangeFavoriteElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCheckToChangeFavoriteInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreateAdjReversalInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createAdjReversal")
    @WebResult(name = "createAdjReversalResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreateAdjReversalInfo createAdjReversal(
        @WebParam(name = "createAdjReversalElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreateAdjReversalInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreateAdjustmentByBanInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createAdjustmentByBan")
    @WebResult(name = "createAdjustmentByBanResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreateAdjustmentByBanInfo createAdjustmentByBan(
        @WebParam(name = "createAdjustmentByBanElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreateAdjustmentByBanInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreateAdjustmentByCrgInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createAdjustmentByCrg")
    @WebResult(name = "createAdjustmentByCrgResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreateAdjustmentByCrgInfo createAdjustmentByCrg(
        @WebParam(name = "createAdjustmentByCrgElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreateAdjustmentByCrgInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreateBillImageInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createBillImage")
    @WebResult(name = "createBillImageResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreateBillImageInfo createBillImage(
        @WebParam(name = "createBillImageElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreateBillImageInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreateChargeInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createCharge")
    @WebResult(name = "createChargeResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreateChargeInfo createCharge(
        @WebParam(name = "createChargeElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreateChargeInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreateLoanEquipmentInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createLoanEquipment")
    @WebResult(name = "createLoanEquipmentResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreateLoanEquipmentInfo createLoanEquipment(
        @WebParam(name = "createLoanEquipmentElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreateLoanEquipmentInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreateMemoInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createMemo")
    @WebResult(name = "createMemoResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreateMemoInfo createMemo(
        @WebParam(name = "createMemoElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreateMemoInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreatePaymentArrangInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createPaymentArrang")
    @WebResult(name = "createPaymentArrangResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreatePaymentArrangInfo createPaymentArrang(
        @WebParam(name = "createPaymentArrangElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreatePaymentArrangInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreateWlsSubInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createWlsSub")
    @WebResult(name = "createWlsSubResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreateWlsSubInfo createWlsSub(
        @WebParam(name = "createWlsSubElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreateWlsSubInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutCreateWlsSubSubLvlInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//createWlsSubSubLvl")
    @WebResult(name = "createWlsSubSubLvlResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutCreateWlsSubSubLvlInfo createWlsSubSubLvl(
        @WebParam(name = "createWlsSubSubLvlElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InCreateWlsSubSubLvlInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetAvailableFavCountInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getAvailableFavoriteCount")
    @WebResult(name = "getAvailableFavoriteCountResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetAvailableFavCountInfo getAvailableFavoriteCount(
        @WebParam(name = "getAvailableFavoriteCountElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetAvailableFavCountInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetBanDetailsInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getBanDetails")
    @WebResult(name = "getBanDetailsResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetBanDetailsInfo getBanDetails(
        @WebParam(name = "getBanDetailsElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetBanDetailsInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetBanInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getBanInfo")
    @WebResult(name = "getBanInfoResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetBanInfo getBanInfo(
        @WebParam(name = "getBanInfoElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetBanInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetBillingHistByBanInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getBillingHistByBan")
    @WebResult(name = "getBillingHistByBanResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetBillingHistByBanInfo getBillingHistByBan(
        @WebParam(name = "getBillingHistByBanElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetBillingHistByBanInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetCreditLimitInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getCreditLimit")
    @WebResult(name = "getCreditLimitResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetCreditLimitInfo getCreditLimit(
        @WebParam(name = "getCreditLimitElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetCreditLimitInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetCustListBySubInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getCustListBySub")
    @WebResult(name = "getCustListBySubResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetCustListBySubInfo getCustListBySub(
        @WebParam(name = "getCustListBySubElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetCustListBySubInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetFavoriteListInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getFavoriteList")
    @WebResult(name = "getFavoriteListResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetFavoriteListInfo getFavoriteList(
        @WebParam(name = "getFavoriteListElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetFavoriteListInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetPaymentArrangByBanInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getPaymentArrangByBan")
    @WebResult(name = "getPaymentArrangByBanResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetPaymentArrangByBanInfo getPaymentArrangByBan(
        @WebParam(name = "getPaymentArrangByBanElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetPaymentArrangByBanInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetPaymentArrangQuoteInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getPaymentArrangQuote")
    @WebResult(name = "getPaymentArrangQuoteResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetPaymentArrangQuoteInfo getPaymentArrangQuote(
        @WebParam(name = "getPaymentArrangQuoteElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetPaymentArrangQuoteInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetPenaltyCalcInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getPenaltyCalc")
    @WebResult(name = "getPenaltyCalcResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetPenaltyCalcInfo getPenaltyCalc(
        @WebParam(name = "getPenaltyCalcElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetPenaltyCalcInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetPenaltyCommitmentsInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getPenaltyCommitments")
    @WebResult(name = "getPenaltyCommitmentsResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetPenaltyCommitmentsInfo getPenaltyCommitments(
        @WebParam(name = "getPenaltyCommitmentsElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetPenaltyCommitmentsInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutGetSubscriberInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//getSubscriberInfo")
    @WebResult(name = "getSubscriberInfoResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutGetSubscriberInfo getSubscriberInfo(
        @WebParam(name = "getSubscriberInfoElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InGetSubscriberInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutRegisterLoyaltyProgInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//registerLoyaltyProgram")
    @WebResult(name = "registerLoyaltyProgramResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutRegisterLoyaltyProgInfo registerLoyaltyProgram(
        @WebParam(name = "registerLoyaltyProgramElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InRegisterLoyaltyProgInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutRestoreSubscriberInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//restoreSubscriber")
    @WebResult(name = "restoreSubscriberResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutRestoreSubscriberInfo restoreSubscriber(
        @WebParam(name = "restoreSubscriberElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InRestoreSubscriberInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns java.lang.String
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//showServiceList")
    @WebResult(name = "showServiceListResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public String showServiceList(
        @WebParam(name = "showServiceListElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        String parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutSuspendSubscriberInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//suspendSubscriber")
    @WebResult(name = "suspendSubscriberResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutSuspendSubscriberInfo suspendSubscriber(
        @WebParam(name = "suspendSubscriberElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InSuspendSubscriberInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutUpdateSocBanLevelInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//updateSocBanLevel")
    @WebResult(name = "updateSocBanLevelResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutUpdateSocBanLevelInfo updateSocBanLevel(
        @WebParam(name = "updateSocBanLevelElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InUpdateSocBanLevelInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutUpdateSubscriberFtrSwPrm
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//updateSubscriberFtrSwPrm")
    @WebResult(name = "updateSubscriberFtrSwPrmResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutUpdateSubscriberFtrSwPrm updateSubscriberFtrSwPrm(
        @WebParam(name = "updateSubscriberFtrSwPrmElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InUpdateSubscriberFtrSwPrm parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutReleaseResourceNumberInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//releaseResourceNumber")
    @WebResult(name = "releaseResourceNumberResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutReleaseResourceNumberInfo releaseResourceNumber(
        @WebParam(name = "releaseResourceNumberElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InReleaseResourceNumberInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutAssignResourceNumberInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//assignResourceNumber")
    @WebResult(name = "assignResourceNumberResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutAssignResourceNumberInfo assignResourceNumber(
        @WebParam(name = "assignResourceNumberElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InAssignResourceNumberInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutUpdateSubscriberPlanInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//updateSubscriberPlan")
    @WebResult(name = "updateSubscriberPlanResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutUpdateSubscriberPlanInfo updateSubscriberPlan(
        @WebParam(name = "updateSubscriberPlanElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InUpdateSubscriberPlanInfo parameters)
        throws ValidateException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutUpdateSubscriberSIMInfo
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//updateSubscriberSIM")
    @WebResult(name = "updateSubscriberSIMResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutUpdateSubscriberSIMInfo updateSubscriberSIM(
        @WebParam(name = "updateSubscriberSIMElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InUpdateSubscriberSIMInfo parameters);

    /**
     * 
     * @param parameters
     * @return
     *     returns com.codetel.postpaid.ejb.types.OutUpdateWirelineLeaseInfo
     * @throws ValidateException
     */
    @WebMethod(action = "http://ejb.postpaid.codetel.com//updateWireLineLease")
    @WebResult(name = "updateWireLineLeaseResponseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "result")
    public OutUpdateWirelineLeaseInfo updateWireLineLease(
        @WebParam(name = "updateWireLineLeaseElement", targetNamespace = "http://ejb.postpaid.codetel.com/", partName = "parameters")
        InUpdateWirelineLeaseInfo parameters)
        throws ValidateException
    ;

}
