
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InCreateChargeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InCreateChargeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chargeCreationInfo" type="{http://ejb.postpaid.codetel.com/types/}ChargeCreationInfo"/>
 *         &lt;element name="ban" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="subscriberNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="orderID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="memoText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="advancedPaymentInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="throwExceptionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InCreateChargeInfo", propOrder = {
    "chargeCreationInfo",
    "ban",
    "subscriberNo",
    "orderID",
    "memoText",
    "advancedPaymentInd",
    "productType",
    "clientSystem",
    "throwExceptionInd"
})
public class InCreateChargeInfo {

    @XmlElement(required = true, nillable = true)
    protected ChargeCreationInfo chargeCreationInfo;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long ban;
    @XmlElement(required = true, nillable = true)
    protected String subscriberNo;
    @XmlElement(required = true, nillable = true)
    protected String orderID;
    @XmlElement(required = true, nillable = true)
    protected String memoText;
    @XmlElement(required = true, nillable = true)
    protected String advancedPaymentInd;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, nillable = true)
    protected String clientSystem;
    @XmlElement(required = true, nillable = true)
    protected String throwExceptionInd;

    /**
     * Gets the value of the chargeCreationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeCreationInfo }
     *     
     */
    public ChargeCreationInfo getChargeCreationInfo() {
        return chargeCreationInfo;
    }

    /**
     * Sets the value of the chargeCreationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeCreationInfo }
     *     
     */
    public void setChargeCreationInfo(ChargeCreationInfo value) {
        this.chargeCreationInfo = value;
    }

    /**
     * Gets the value of the ban property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBan() {
        return ban;
    }

    /**
     * Sets the value of the ban property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBan(Long value) {
        this.ban = value;
    }

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNo(String value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the orderID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderID() {
        return orderID;
    }

    /**
     * Sets the value of the orderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderID(String value) {
        this.orderID = value;
    }

    /**
     * Gets the value of the memoText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemoText() {
        return memoText;
    }

    /**
     * Sets the value of the memoText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemoText(String value) {
        this.memoText = value;
    }

    /**
     * Gets the value of the advancedPaymentInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvancedPaymentInd() {
        return advancedPaymentInd;
    }

    /**
     * Sets the value of the advancedPaymentInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvancedPaymentInd(String value) {
        this.advancedPaymentInd = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the throwExceptionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThrowExceptionInd() {
        return throwExceptionInd;
    }

    /**
     * Sets the value of the throwExceptionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThrowExceptionInd(String value) {
        this.throwExceptionInd = value;
    }

}
