
package com.codetel.postpaid.ejb.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InAssignResourceNumberInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InAssignResourceNumberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resourceNumberList" type="{http://ejb.postpaid.codetel.com/types/}ResourceNumberInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="toNumberLocation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="throwExceptionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InAssignResourceNumberInfo", propOrder = {
    "resourceNumberList",
    "toNumberLocation",
    "productType",
    "clientSystem",
    "throwExceptionInd"
})
public class InAssignResourceNumberInfo {

    @XmlElement(nillable = true)
    protected List<ResourceNumberInfo> resourceNumberList;
    @XmlElement(required = true, nillable = true)
    protected String toNumberLocation;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, nillable = true)
    protected String clientSystem;
    @XmlElement(required = true, nillable = true)
    protected String throwExceptionInd;

    /**
     * Gets the value of the resourceNumberList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resourceNumberList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResourceNumberList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceNumberInfo }
     * 
     * 
     */
    public List<ResourceNumberInfo> getResourceNumberList() {
        if (resourceNumberList == null) {
            resourceNumberList = new ArrayList<ResourceNumberInfo>();
        }
        return this.resourceNumberList;
    }

    /**
     * Gets the value of the toNumberLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToNumberLocation() {
        return toNumberLocation;
    }

    /**
     * Sets the value of the toNumberLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToNumberLocation(String value) {
        this.toNumberLocation = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the throwExceptionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThrowExceptionInd() {
        return throwExceptionInd;
    }

    /**
     * Sets the value of the throwExceptionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThrowExceptionInd(String value) {
        this.throwExceptionInd = value;
    }

}
