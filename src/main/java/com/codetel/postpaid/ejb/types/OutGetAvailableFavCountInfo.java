
package com.codetel.postpaid.ejb.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutGetAvailableFavCountInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutGetAvailableFavCountInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorInfo" type="{http://ejb.postpaid.codetel.com/types/}CodedMessageInfo"/>
 *         &lt;element name="ban" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="subscriberNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="featurePtpListInfo" type="{http://ejb.postpaid.codetel.com/types/}FeaturePtpInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutGetAvailableFavCountInfo", propOrder = {
    "apiStatus",
    "errorInfo",
    "ban",
    "subscriberNo",
    "featurePtpListInfo",
    "productType"
})
public class OutGetAvailableFavCountInfo {

    @XmlElement(required = true, nillable = true)
    protected String apiStatus;
    @XmlElement(required = true, nillable = true)
    protected CodedMessageInfo errorInfo;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long ban;
    @XmlElement(required = true, nillable = true)
    protected String subscriberNo;
    @XmlElement(nillable = true)
    protected List<FeaturePtpInfo> featurePtpListInfo;
    @XmlElement(required = true, nillable = true)
    protected String productType;

    /**
     * Gets the value of the apiStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * Sets the value of the apiStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiStatus(String value) {
        this.apiStatus = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CodedMessageInfo }
     *     
     */
    public CodedMessageInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedMessageInfo }
     *     
     */
    public void setErrorInfo(CodedMessageInfo value) {
        this.errorInfo = value;
    }

    /**
     * Gets the value of the ban property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBan() {
        return ban;
    }

    /**
     * Sets the value of the ban property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBan(Long value) {
        this.ban = value;
    }

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNo(String value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the featurePtpListInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the featurePtpListInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeaturePtpListInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeaturePtpInfo }
     * 
     * 
     */
    public List<FeaturePtpInfo> getFeaturePtpListInfo() {
        if (featurePtpListInfo == null) {
            featurePtpListInfo = new ArrayList<FeaturePtpInfo>();
        }
        return this.featurePtpListInfo;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

}
