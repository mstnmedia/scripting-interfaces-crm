
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BanInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BanInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="writeOffInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paymentIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ban" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="banStatusActivityReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accountSubTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="banStatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="banStatusActivityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ARBalance" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="accountSubType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sumariaIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="banStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="reconnectionPaymentAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="sumariaBan" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="billCycle" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="accountType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="banLastStatusDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CCU" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accountTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BanInfo", propOrder = {
    "writeOffInd",
    "paymentIndicator",
    "ban",
    "banStatusActivityReasonCode",
    "accountSubTypeDescription",
    "banStatusDescription",
    "serviceStartDate",
    "banStatusActivityCode",
    "arBalance",
    "accountSubType",
    "sumariaIndicator",
    "banStatus",
    "reconnectionPaymentAmount",
    "sumariaBan",
    "billCycle",
    "accountType",
    "banLastStatusDate",
    "ccu",
    "accountTypeDescription"
})
public class BanInfo {

    @XmlElement(required = true, nillable = true)
    protected String writeOffInd;
    @XmlElement(required = true, nillable = true)
    protected String paymentIndicator;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long ban;
    @XmlElement(required = true, nillable = true)
    protected String banStatusActivityReasonCode;
    @XmlElement(required = true, nillable = true)
    protected String accountSubTypeDescription;
    @XmlElement(required = true, nillable = true)
    protected String banStatusDescription;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar serviceStartDate;
    @XmlElement(required = true, nillable = true)
    protected String banStatusActivityCode;
    @XmlElement(name = "ARBalance")
    protected double arBalance;
    @XmlElement(required = true, nillable = true)
    protected String accountSubType;
    @XmlElement(required = true, nillable = true)
    protected String sumariaIndicator;
    @XmlElement(required = true, nillable = true)
    protected String banStatus;
    protected double reconnectionPaymentAmount;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long sumariaBan;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer billCycle;
    @XmlElement(required = true, nillable = true)
    protected String accountType;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar banLastStatusDate;
    @XmlElement(name = "CCU", required = true, nillable = true)
    protected String ccu;
    @XmlElement(required = true, nillable = true)
    protected String accountTypeDescription;

    /**
     * Gets the value of the writeOffInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWriteOffInd() {
        return writeOffInd;
    }

    /**
     * Sets the value of the writeOffInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWriteOffInd(String value) {
        this.writeOffInd = value;
    }

    /**
     * Gets the value of the paymentIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentIndicator() {
        return paymentIndicator;
    }

    /**
     * Sets the value of the paymentIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentIndicator(String value) {
        this.paymentIndicator = value;
    }

    /**
     * Gets the value of the ban property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBan() {
        return ban;
    }

    /**
     * Sets the value of the ban property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBan(Long value) {
        this.ban = value;
    }

    /**
     * Gets the value of the banStatusActivityReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanStatusActivityReasonCode() {
        return banStatusActivityReasonCode;
    }

    /**
     * Sets the value of the banStatusActivityReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanStatusActivityReasonCode(String value) {
        this.banStatusActivityReasonCode = value;
    }

    /**
     * Gets the value of the accountSubTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountSubTypeDescription() {
        return accountSubTypeDescription;
    }

    /**
     * Sets the value of the accountSubTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountSubTypeDescription(String value) {
        this.accountSubTypeDescription = value;
    }

    /**
     * Gets the value of the banStatusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanStatusDescription() {
        return banStatusDescription;
    }

    /**
     * Sets the value of the banStatusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanStatusDescription(String value) {
        this.banStatusDescription = value;
    }

    /**
     * Gets the value of the serviceStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServiceStartDate() {
        return serviceStartDate;
    }

    /**
     * Sets the value of the serviceStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServiceStartDate(XMLGregorianCalendar value) {
        this.serviceStartDate = value;
    }

    /**
     * Gets the value of the banStatusActivityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanStatusActivityCode() {
        return banStatusActivityCode;
    }

    /**
     * Sets the value of the banStatusActivityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanStatusActivityCode(String value) {
        this.banStatusActivityCode = value;
    }

    /**
     * Gets the value of the arBalance property.
     * 
     */
    public double getARBalance() {
        return arBalance;
    }

    /**
     * Sets the value of the arBalance property.
     * 
     */
    public void setARBalance(double value) {
        this.arBalance = value;
    }

    /**
     * Gets the value of the accountSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountSubType() {
        return accountSubType;
    }

    /**
     * Sets the value of the accountSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountSubType(String value) {
        this.accountSubType = value;
    }

    /**
     * Gets the value of the sumariaIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSumariaIndicator() {
        return sumariaIndicator;
    }

    /**
     * Sets the value of the sumariaIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSumariaIndicator(String value) {
        this.sumariaIndicator = value;
    }

    /**
     * Gets the value of the banStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanStatus() {
        return banStatus;
    }

    /**
     * Sets the value of the banStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanStatus(String value) {
        this.banStatus = value;
    }

    /**
     * Gets the value of the reconnectionPaymentAmount property.
     * 
     */
    public double getReconnectionPaymentAmount() {
        return reconnectionPaymentAmount;
    }

    /**
     * Sets the value of the reconnectionPaymentAmount property.
     * 
     */
    public void setReconnectionPaymentAmount(double value) {
        this.reconnectionPaymentAmount = value;
    }

    /**
     * Gets the value of the sumariaBan property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSumariaBan() {
        return sumariaBan;
    }

    /**
     * Sets the value of the sumariaBan property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSumariaBan(Long value) {
        this.sumariaBan = value;
    }

    /**
     * Gets the value of the billCycle property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBillCycle() {
        return billCycle;
    }

    /**
     * Sets the value of the billCycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBillCycle(Integer value) {
        this.billCycle = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the banLastStatusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBanLastStatusDate() {
        return banLastStatusDate;
    }

    /**
     * Sets the value of the banLastStatusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBanLastStatusDate(XMLGregorianCalendar value) {
        this.banLastStatusDate = value;
    }

    /**
     * Gets the value of the ccu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCU() {
        return ccu;
    }

    /**
     * Sets the value of the ccu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCU(String value) {
        this.ccu = value;
    }

    /**
     * Gets the value of the accountTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTypeDescription() {
        return accountTypeDescription;
    }

    /**
     * Sets the value of the accountTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTypeDescription(String value) {
        this.accountTypeDescription = value;
    }

}
