
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressCatalogFieldInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressCatalogFieldInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="newValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fieldNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="refFieldName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oldValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressCatalogFieldInfo", propOrder = {
    "newValue",
    "fieldNo",
    "refFieldName",
    "oldValue"
})
public class AddressCatalogFieldInfo {

    @XmlElement(required = true, nillable = true)
    protected String newValue;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long fieldNo;
    @XmlElement(required = true, nillable = true)
    protected String refFieldName;
    @XmlElement(required = true, nillable = true)
    protected String oldValue;

    /**
     * Gets the value of the newValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewValue() {
        return newValue;
    }

    /**
     * Sets the value of the newValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewValue(String value) {
        this.newValue = value;
    }

    /**
     * Gets the value of the fieldNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFieldNo() {
        return fieldNo;
    }

    /**
     * Sets the value of the fieldNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFieldNo(Long value) {
        this.fieldNo = value;
    }

    /**
     * Gets the value of the refFieldName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefFieldName() {
        return refFieldName;
    }

    /**
     * Sets the value of the refFieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefFieldName(String value) {
        this.refFieldName = value;
    }

    /**
     * Gets the value of the oldValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldValue() {
        return oldValue;
    }

    /**
     * Sets the value of the oldValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldValue(String value) {
        this.oldValue = value;
    }

}
