
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OutGetCreditLimitInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutGetCreditLimitInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorInfo" type="{http://ejb.postpaid.codetel.com/types/}CodedMessageInfo"/>
 *         &lt;element name="banCreditLimitAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="subscriberCurrentBalance" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="subscriberAvailableAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="subscriberRecentCallDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="banCurrentBalanceAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="ARBalance" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="UCAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutGetCreditLimitInfo", propOrder = {
    "apiStatus",
    "errorInfo",
    "banCreditLimitAmount",
    "subscriberCurrentBalance",
    "subscriberAvailableAmount",
    "subscriberRecentCallDate",
    "banCurrentBalanceAmount",
    "arBalance",
    "ucAmount"
})
public class OutGetCreditLimitInfo {

    @XmlElement(required = true, nillable = true)
    protected String apiStatus;
    @XmlElement(required = true, nillable = true)
    protected CodedMessageInfo errorInfo;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double banCreditLimitAmount;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double subscriberCurrentBalance;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double subscriberAvailableAmount;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar subscriberRecentCallDate;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double banCurrentBalanceAmount;
    @XmlElement(name = "ARBalance", required = true, type = Double.class, nillable = true)
    protected Double arBalance;
    @XmlElement(name = "UCAmount", required = true, type = Double.class, nillable = true)
    protected Double ucAmount;

    /**
     * Gets the value of the apiStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * Sets the value of the apiStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiStatus(String value) {
        this.apiStatus = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CodedMessageInfo }
     *     
     */
    public CodedMessageInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedMessageInfo }
     *     
     */
    public void setErrorInfo(CodedMessageInfo value) {
        this.errorInfo = value;
    }

    /**
     * Gets the value of the banCreditLimitAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBanCreditLimitAmount() {
        return banCreditLimitAmount;
    }

    /**
     * Sets the value of the banCreditLimitAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBanCreditLimitAmount(Double value) {
        this.banCreditLimitAmount = value;
    }

    /**
     * Gets the value of the subscriberCurrentBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSubscriberCurrentBalance() {
        return subscriberCurrentBalance;
    }

    /**
     * Sets the value of the subscriberCurrentBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSubscriberCurrentBalance(Double value) {
        this.subscriberCurrentBalance = value;
    }

    /**
     * Gets the value of the subscriberAvailableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSubscriberAvailableAmount() {
        return subscriberAvailableAmount;
    }

    /**
     * Sets the value of the subscriberAvailableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSubscriberAvailableAmount(Double value) {
        this.subscriberAvailableAmount = value;
    }

    /**
     * Gets the value of the subscriberRecentCallDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSubscriberRecentCallDate() {
        return subscriberRecentCallDate;
    }

    /**
     * Sets the value of the subscriberRecentCallDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSubscriberRecentCallDate(XMLGregorianCalendar value) {
        this.subscriberRecentCallDate = value;
    }

    /**
     * Gets the value of the banCurrentBalanceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBanCurrentBalanceAmount() {
        return banCurrentBalanceAmount;
    }

    /**
     * Sets the value of the banCurrentBalanceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBanCurrentBalanceAmount(Double value) {
        this.banCurrentBalanceAmount = value;
    }

    /**
     * Gets the value of the arBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getARBalance() {
        return arBalance;
    }

    /**
     * Sets the value of the arBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setARBalance(Double value) {
        this.arBalance = value;
    }

    /**
     * Gets the value of the ucAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getUCAmount() {
        return ucAmount;
    }

    /**
     * Sets the value of the ucAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setUCAmount(Double value) {
        this.ucAmount = value;
    }

}
