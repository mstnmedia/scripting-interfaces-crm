
package com.codetel.postpaid.ejb.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InUpdateSocBanLevelInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InUpdateSocBanLevelInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="banID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="socListInfo" type="{http://ejb.postpaid.codetel.com/types/}SocInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="activityDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="throwExceptionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InUpdateSocBanLevelInfo", propOrder = {
    "banID",
    "socListInfo",
    "activityDate",
    "productType",
    "clientSystem",
    "throwExceptionInd"
})
public class InUpdateSocBanLevelInfo {

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long banID;
    @XmlElement(nillable = true)
    protected List<SocInfo> socListInfo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activityDate;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, nillable = true)
    protected String clientSystem;
    @XmlElement(required = true, nillable = true)
    protected String throwExceptionInd;

    /**
     * Gets the value of the banID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBanID() {
        return banID;
    }

    /**
     * Sets the value of the banID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBanID(Long value) {
        this.banID = value;
    }

    /**
     * Gets the value of the socListInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the socListInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSocListInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SocInfo }
     * 
     * 
     */
    public List<SocInfo> getSocListInfo() {
        if (socListInfo == null) {
            socListInfo = new ArrayList<SocInfo>();
        }
        return this.socListInfo;
    }

    /**
     * Gets the value of the activityDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivityDate() {
        return activityDate;
    }

    /**
     * Sets the value of the activityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivityDate(XMLGregorianCalendar value) {
        this.activityDate = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the throwExceptionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThrowExceptionInd() {
        return throwExceptionInd;
    }

    /**
     * Sets the value of the throwExceptionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThrowExceptionInd(String value) {
        this.throwExceptionInd = value;
    }

}
