
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.codetel.postpaid.ejb.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ValidateExceptionElement_QNAME = new QName("http://ejb.postpaid.codetel.com/types/", "ValidateExceptionElement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.codetel.postpaid.ejb.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OutRestoreSubscriberInfo }
     * 
     */
    public OutRestoreSubscriberInfo createOutRestoreSubscriberInfo() {
        return new OutRestoreSubscriberInfo();
    }

    /**
     * Create an instance of {@link ValidateException }
     * 
     */
    public ValidateException createValidateException() {
        return new ValidateException();
    }

    /**
     * Create an instance of {@link CreditEvaluationInfo }
     * 
     */
    public CreditEvaluationInfo createCreditEvaluationInfo() {
        return new CreditEvaluationInfo();
    }

    /**
     * Create an instance of {@link OutGetPenaltyCommitmentsInfo }
     * 
     */
    public OutGetPenaltyCommitmentsInfo createOutGetPenaltyCommitmentsInfo() {
        return new OutGetPenaltyCommitmentsInfo();
    }

    /**
     * Create an instance of {@link FeaturePtpInfo }
     * 
     */
    public FeaturePtpInfo createFeaturePtpInfo() {
        return new FeaturePtpInfo();
    }

    /**
     * Create an instance of {@link OutChangeAddressCatInfo }
     * 
     */
    public OutChangeAddressCatInfo createOutChangeAddressCatInfo() {
        return new OutChangeAddressCatInfo();
    }

    /**
     * Create an instance of {@link OutCreateWlsSubSubLvlInfo }
     * 
     */
    public OutCreateWlsSubSubLvlInfo createOutCreateWlsSubSubLvlInfo() {
        return new OutCreateWlsSubSubLvlInfo();
    }

    /**
     * Create an instance of {@link OutCancelSubscriberInfo }
     * 
     */
    public OutCancelSubscriberInfo createOutCancelSubscriberInfo() {
        return new OutCancelSubscriberInfo();
    }

    /**
     * Create an instance of {@link InUpdateSubscriberFtrSwPrm }
     * 
     */
    public InUpdateSubscriberFtrSwPrm createInUpdateSubscriberFtrSwPrm() {
        return new InUpdateSubscriberFtrSwPrm();
    }

    /**
     * Create an instance of {@link InUpdateSubscriberSIMInfo }
     * 
     */
    public InUpdateSubscriberSIMInfo createInUpdateSubscriberSIMInfo() {
        return new InUpdateSubscriberSIMInfo();
    }

    /**
     * Create an instance of {@link AddressInfo }
     * 
     */
    public AddressInfo createAddressInfo() {
        return new AddressInfo();
    }

    /**
     * Create an instance of {@link OutGetCreditLimitInfo }
     * 
     */
    public OutGetCreditLimitInfo createOutGetCreditLimitInfo() {
        return new OutGetCreditLimitInfo();
    }

    /**
     * Create an instance of {@link OrderEquipmentInputInfo }
     * 
     */
    public OrderEquipmentInputInfo createOrderEquipmentInputInfo() {
        return new OrderEquipmentInputInfo();
    }

    /**
     * Create an instance of {@link PenaltyCommitmentInfo }
     * 
     */
    public PenaltyCommitmentInfo createPenaltyCommitmentInfo() {
        return new PenaltyCommitmentInfo();
    }

    /**
     * Create an instance of {@link CodeValueInfo }
     * 
     */
    public CodeValueInfo createCodeValueInfo() {
        return new CodeValueInfo();
    }

    /**
     * Create an instance of {@link OutCheckStreetInfo }
     * 
     */
    public OutCheckStreetInfo createOutCheckStreetInfo() {
        return new OutCheckStreetInfo();
    }

    /**
     * Create an instance of {@link OutUpdateSubscriberPlanInfo }
     * 
     */
    public OutUpdateSubscriberPlanInfo createOutUpdateSubscriberPlanInfo() {
        return new OutUpdateSubscriberPlanInfo();
    }

    /**
     * Create an instance of {@link AdjCreationByCrgInfo }
     * 
     */
    public AdjCreationByCrgInfo createAdjCreationByCrgInfo() {
        return new AdjCreationByCrgInfo();
    }

    /**
     * Create an instance of {@link InCreateAdjustmentByBanInfo }
     * 
     */
    public InCreateAdjustmentByBanInfo createInCreateAdjustmentByBanInfo() {
        return new InCreateAdjustmentByBanInfo();
    }

    /**
     * Create an instance of {@link InGetBanDetailsInfo }
     * 
     */
    public InGetBanDetailsInfo createInGetBanDetailsInfo() {
        return new InGetBanDetailsInfo();
    }

    /**
     * Create an instance of {@link InGetSubscriberInfo }
     * 
     */
    public InGetSubscriberInfo createInGetSubscriberInfo() {
        return new InGetSubscriberInfo();
    }

    /**
     * Create an instance of {@link OutCreateChargeInfo }
     * 
     */
    public OutCreateChargeInfo createOutCreateChargeInfo() {
        return new OutCreateChargeInfo();
    }

    /**
     * Create an instance of {@link InCreateWlsSubInfo }
     * 
     */
    public InCreateWlsSubInfo createInCreateWlsSubInfo() {
        return new InCreateWlsSubInfo();
    }

    /**
     * Create an instance of {@link OutReleaseResourceNumberInfo }
     * 
     */
    public OutReleaseResourceNumberInfo createOutReleaseResourceNumberInfo() {
        return new OutReleaseResourceNumberInfo();
    }

    /**
     * Create an instance of {@link InCreateLoanEquipmentInfo }
     * 
     */
    public InCreateLoanEquipmentInfo createInCreateLoanEquipmentInfo() {
        return new InCreateLoanEquipmentInfo();
    }

    /**
     * Create an instance of {@link SocInfo }
     * 
     */
    public SocInfo createSocInfo() {
        return new SocInfo();
    }

    /**
     * Create an instance of {@link InAddFavoriteInfo }
     * 
     */
    public InAddFavoriteInfo createInAddFavoriteInfo() {
        return new InAddFavoriteInfo();
    }

    /**
     * Create an instance of {@link InGetPaymentArrangQuoteInfo }
     * 
     */
    public InGetPaymentArrangQuoteInfo createInGetPaymentArrangQuoteInfo() {
        return new InGetPaymentArrangQuoteInfo();
    }

    /**
     * Create an instance of {@link InRestoreSubscriberInfo }
     * 
     */
    public InRestoreSubscriberInfo createInRestoreSubscriberInfo() {
        return new InRestoreSubscriberInfo();
    }

    /**
     * Create an instance of {@link OutCreateAdjReversalInfo }
     * 
     */
    public OutCreateAdjReversalInfo createOutCreateAdjReversalInfo() {
        return new OutCreateAdjReversalInfo();
    }

    /**
     * Create an instance of {@link ChargeCreationInfo }
     * 
     */
    public ChargeCreationInfo createChargeCreationInfo() {
        return new ChargeCreationInfo();
    }

    /**
     * Create an instance of {@link OutChangeFavoriteInfo }
     * 
     */
    public OutChangeFavoriteInfo createOutChangeFavoriteInfo() {
        return new OutChangeFavoriteInfo();
    }

    /**
     * Create an instance of {@link InUpdateWirelineLeaseInfo }
     * 
     */
    public InUpdateWirelineLeaseInfo createInUpdateWirelineLeaseInfo() {
        return new InUpdateWirelineLeaseInfo();
    }

    /**
     * Create an instance of {@link InCreateWlsSubSubLvlInfo }
     * 
     */
    public InCreateWlsSubSubLvlInfo createInCreateWlsSubSubLvlInfo() {
        return new InCreateWlsSubSubLvlInfo();
    }

    /**
     * Create an instance of {@link InCreateMemoInfo }
     * 
     */
    public InCreateMemoInfo createInCreateMemoInfo() {
        return new InCreateMemoInfo();
    }

    /**
     * Create an instance of {@link OutUpdateSubscriberSIMInfo }
     * 
     */
    public OutUpdateSubscriberSIMInfo createOutUpdateSubscriberSIMInfo() {
        return new OutUpdateSubscriberSIMInfo();
    }

    /**
     * Create an instance of {@link InAssignResourceNumberInfo }
     * 
     */
    public InAssignResourceNumberInfo createInAssignResourceNumberInfo() {
        return new InAssignResourceNumberInfo();
    }

    /**
     * Create an instance of {@link CodedMessageInfo }
     * 
     */
    public CodedMessageInfo createCodedMessageInfo() {
        return new CodedMessageInfo();
    }

    /**
     * Create an instance of {@link OutCreateAdjustmentByBanInfo }
     * 
     */
    public OutCreateAdjustmentByBanInfo createOutCreateAdjustmentByBanInfo() {
        return new OutCreateAdjustmentByBanInfo();
    }

    /**
     * Create an instance of {@link InCreateChargeInfo }
     * 
     */
    public InCreateChargeInfo createInCreateChargeInfo() {
        return new InCreateChargeInfo();
    }

    /**
     * Create an instance of {@link InCreateAdjReversalInfo }
     * 
     */
    public InCreateAdjReversalInfo createInCreateAdjReversalInfo() {
        return new InCreateAdjReversalInfo();
    }

    /**
     * Create an instance of {@link OutAddFavoriteInfo }
     * 
     */
    public OutAddFavoriteInfo createOutAddFavoriteInfo() {
        return new OutAddFavoriteInfo();
    }

    /**
     * Create an instance of {@link OutUpdateWirelineLeaseInfo }
     * 
     */
    public OutUpdateWirelineLeaseInfo createOutUpdateWirelineLeaseInfo() {
        return new OutUpdateWirelineLeaseInfo();
    }

    /**
     * Create an instance of {@link AddressCatalogFieldInfo }
     * 
     */
    public AddressCatalogFieldInfo createAddressCatalogFieldInfo() {
        return new AddressCatalogFieldInfo();
    }

    /**
     * Create an instance of {@link InGetFavoriteListInfo }
     * 
     */
    public InGetFavoriteListInfo createInGetFavoriteListInfo() {
        return new InGetFavoriteListInfo();
    }

    /**
     * Create an instance of {@link OutCreatePaymentArrangInfo }
     * 
     */
    public OutCreatePaymentArrangInfo createOutCreatePaymentArrangInfo() {
        return new OutCreatePaymentArrangInfo();
    }

    /**
     * Create an instance of {@link OutGetBanInfo }
     * 
     */
    public OutGetBanInfo createOutGetBanInfo() {
        return new OutGetBanInfo();
    }

    /**
     * Create an instance of {@link OutCreateBillImageInfo }
     * 
     */
    public OutCreateBillImageInfo createOutCreateBillImageInfo() {
        return new OutCreateBillImageInfo();
    }

    /**
     * Create an instance of {@link InGetCustListBySubInfo }
     * 
     */
    public InGetCustListBySubInfo createInGetCustListBySubInfo() {
        return new InGetCustListBySubInfo();
    }

    /**
     * Create an instance of {@link InCreatePaymentArrangInfo }
     * 
     */
    public InCreatePaymentArrangInfo createInCreatePaymentArrangInfo() {
        return new InCreatePaymentArrangInfo();
    }

    /**
     * Create an instance of {@link NameInfo }
     * 
     */
    public NameInfo createNameInfo() {
        return new NameInfo();
    }

    /**
     * Create an instance of {@link OutCreateAdjustmentByCrgInfo }
     * 
     */
    public OutCreateAdjustmentByCrgInfo createOutCreateAdjustmentByCrgInfo() {
        return new OutCreateAdjustmentByCrgInfo();
    }

    /**
     * Create an instance of {@link InChangeAddressCatInfo }
     * 
     */
    public InChangeAddressCatInfo createInChangeAddressCatInfo() {
        return new InChangeAddressCatInfo();
    }

    /**
     * Create an instance of {@link InUpdateSocBanLevelInfo }
     * 
     */
    public InUpdateSocBanLevelInfo createInUpdateSocBanLevelInfo() {
        return new InUpdateSocBanLevelInfo();
    }

    /**
     * Create an instance of {@link OutCreateMemoInfo }
     * 
     */
    public OutCreateMemoInfo createOutCreateMemoInfo() {
        return new OutCreateMemoInfo();
    }

    /**
     * Create an instance of {@link OutSuspendSubscriberInfo }
     * 
     */
    public OutSuspendSubscriberInfo createOutSuspendSubscriberInfo() {
        return new OutSuspendSubscriberInfo();
    }

    /**
     * Create an instance of {@link InSuspendSubscriberInfo }
     * 
     */
    public InSuspendSubscriberInfo createInSuspendSubscriberInfo() {
        return new InSuspendSubscriberInfo();
    }

    /**
     * Create an instance of {@link OutGetCustListBySubInfo }
     * 
     */
    public OutGetCustListBySubInfo createOutGetCustListBySubInfo() {
        return new OutGetCustListBySubInfo();
    }

    /**
     * Create an instance of {@link OutCreateWlsSubInfo }
     * 
     */
    public OutCreateWlsSubInfo createOutCreateWlsSubInfo() {
        return new OutCreateWlsSubInfo();
    }

    /**
     * Create an instance of {@link InUpdateSubscriberPlanInfo }
     * 
     */
    public InUpdateSubscriberPlanInfo createInUpdateSubscriberPlanInfo() {
        return new InUpdateSubscriberPlanInfo();
    }

    /**
     * Create an instance of {@link OutGetBanDetailsInfo }
     * 
     */
    public OutGetBanDetailsInfo createOutGetBanDetailsInfo() {
        return new OutGetBanDetailsInfo();
    }

    /**
     * Create an instance of {@link ActivityInfo }
     * 
     */
    public ActivityInfo createActivityInfo() {
        return new ActivityInfo();
    }

    /**
     * Create an instance of {@link InGetAvailableFavCountInfo }
     * 
     */
    public InGetAvailableFavCountInfo createInGetAvailableFavCountInfo() {
        return new InGetAvailableFavCountInfo();
    }

    /**
     * Create an instance of {@link OutGetBillingHistByBanInfo }
     * 
     */
    public OutGetBillingHistByBanInfo createOutGetBillingHistByBanInfo() {
        return new OutGetBillingHistByBanInfo();
    }

    /**
     * Create an instance of {@link InGetPenaltyCommitmentsInfo }
     * 
     */
    public InGetPenaltyCommitmentsInfo createInGetPenaltyCommitmentsInfo() {
        return new InGetPenaltyCommitmentsInfo();
    }

    /**
     * Create an instance of {@link OutGetSubscriberInfo }
     * 
     */
    public OutGetSubscriberInfo createOutGetSubscriberInfo() {
        return new OutGetSubscriberInfo();
    }

    /**
     * Create an instance of {@link InCheckStreetInfo }
     * 
     */
    public InCheckStreetInfo createInCheckStreetInfo() {
        return new InCheckStreetInfo();
    }

    /**
     * Create an instance of {@link InCancelSubscriberInfo }
     * 
     */
    public InCancelSubscriberInfo createInCancelSubscriberInfo() {
        return new InCancelSubscriberInfo();
    }

    /**
     * Create an instance of {@link InCheckToChangeFavoriteInfo }
     * 
     */
    public InCheckToChangeFavoriteInfo createInCheckToChangeFavoriteInfo() {
        return new InCheckToChangeFavoriteInfo();
    }

    /**
     * Create an instance of {@link OrderInputInfo }
     * 
     */
    public OrderInputInfo createOrderInputInfo() {
        return new OrderInputInfo();
    }

    /**
     * Create an instance of {@link InGetPenaltyCalcInfo }
     * 
     */
    public InGetPenaltyCalcInfo createInGetPenaltyCalcInfo() {
        return new InGetPenaltyCalcInfo();
    }

    /**
     * Create an instance of {@link OutAssignResourceNumberInfo }
     * 
     */
    public OutAssignResourceNumberInfo createOutAssignResourceNumberInfo() {
        return new OutAssignResourceNumberInfo();
    }

    /**
     * Create an instance of {@link PenaltyInfo }
     * 
     */
    public PenaltyInfo createPenaltyInfo() {
        return new PenaltyInfo();
    }

    /**
     * Create an instance of {@link InCheckToAddFavoriteInfo }
     * 
     */
    public InCheckToAddFavoriteInfo createInCheckToAddFavoriteInfo() {
        return new InCheckToAddFavoriteInfo();
    }

    /**
     * Create an instance of {@link InGetBillingHistByBanInfo }
     * 
     */
    public InGetBillingHistByBanInfo createInGetBillingHistByBanInfo() {
        return new InGetBillingHistByBanInfo();
    }

    /**
     * Create an instance of {@link AdjustmentCreationInfo }
     * 
     */
    public AdjustmentCreationInfo createAdjustmentCreationInfo() {
        return new AdjustmentCreationInfo();
    }

    /**
     * Create an instance of {@link InCreateAdjustmentByCrgInfo }
     * 
     */
    public InCreateAdjustmentByCrgInfo createInCreateAdjustmentByCrgInfo() {
        return new InCreateAdjustmentByCrgInfo();
    }

    /**
     * Create an instance of {@link InChangeFavoriteInfo }
     * 
     */
    public InChangeFavoriteInfo createInChangeFavoriteInfo() {
        return new InChangeFavoriteInfo();
    }

    /**
     * Create an instance of {@link InGetBanInfo }
     * 
     */
    public InGetBanInfo createInGetBanInfo() {
        return new InGetBanInfo();
    }

    /**
     * Create an instance of {@link OutGetAvailableFavCountInfo }
     * 
     */
    public OutGetAvailableFavCountInfo createOutGetAvailableFavCountInfo() {
        return new OutGetAvailableFavCountInfo();
    }

    /**
     * Create an instance of {@link InCreateWlsSubSubLvlSocInfo }
     * 
     */
    public InCreateWlsSubSubLvlSocInfo createInCreateWlsSubSubLvlSocInfo() {
        return new InCreateWlsSubSubLvlSocInfo();
    }

    /**
     * Create an instance of {@link OutRegisterLoyaltyProgInfo }
     * 
     */
    public OutRegisterLoyaltyProgInfo createOutRegisterLoyaltyProgInfo() {
        return new OutRegisterLoyaltyProgInfo();
    }

    /**
     * Create an instance of {@link InRegisterLoyaltyProgInfo }
     * 
     */
    public InRegisterLoyaltyProgInfo createInRegisterLoyaltyProgInfo() {
        return new InRegisterLoyaltyProgInfo();
    }

    /**
     * Create an instance of {@link PAInstallmentInfo }
     * 
     */
    public PAInstallmentInfo createPAInstallmentInfo() {
        return new PAInstallmentInfo();
    }

    /**
     * Create an instance of {@link OutUpdateSocBanLevelInfo }
     * 
     */
    public OutUpdateSocBanLevelInfo createOutUpdateSocBanLevelInfo() {
        return new OutUpdateSocBanLevelInfo();
    }

    /**
     * Create an instance of {@link OutGetPaymentArrangQuoteInfo }
     * 
     */
    public OutGetPaymentArrangQuoteInfo createOutGetPaymentArrangQuoteInfo() {
        return new OutGetPaymentArrangQuoteInfo();
    }

    /**
     * Create an instance of {@link OutGetFavoriteListInfo }
     * 
     */
    public OutGetFavoriteListInfo createOutGetFavoriteListInfo() {
        return new OutGetFavoriteListInfo();
    }

    /**
     * Create an instance of {@link OutCreateLoanEquipmentInfo }
     * 
     */
    public OutCreateLoanEquipmentInfo createOutCreateLoanEquipmentInfo() {
        return new OutCreateLoanEquipmentInfo();
    }

    /**
     * Create an instance of {@link InGetCreditLimitInfo }
     * 
     */
    public InGetCreditLimitInfo createInGetCreditLimitInfo() {
        return new InGetCreditLimitInfo();
    }

    /**
     * Create an instance of {@link ResourceNumberInfo }
     * 
     */
    public ResourceNumberInfo createResourceNumberInfo() {
        return new ResourceNumberInfo();
    }

    /**
     * Create an instance of {@link BanInfo }
     * 
     */
    public BanInfo createBanInfo() {
        return new BanInfo();
    }

    /**
     * Create an instance of {@link InCreateBillImageInfo }
     * 
     */
    public InCreateBillImageInfo createInCreateBillImageInfo() {
        return new InCreateBillImageInfo();
    }

    /**
     * Create an instance of {@link BanSubInfo }
     * 
     */
    public BanSubInfo createBanSubInfo() {
        return new BanSubInfo();
    }

    /**
     * Create an instance of {@link SubscriberCounterInfo }
     * 
     */
    public SubscriberCounterInfo createSubscriberCounterInfo() {
        return new SubscriberCounterInfo();
    }

    /**
     * Create an instance of {@link PaymentArrangInfo }
     * 
     */
    public PaymentArrangInfo createPaymentArrangInfo() {
        return new PaymentArrangInfo();
    }

    /**
     * Create an instance of {@link OutGetPenaltyCalcInfo }
     * 
     */
    public OutGetPenaltyCalcInfo createOutGetPenaltyCalcInfo() {
        return new OutGetPenaltyCalcInfo();
    }

    /**
     * Create an instance of {@link UpdateFeatureInfo }
     * 
     */
    public UpdateFeatureInfo createUpdateFeatureInfo() {
        return new UpdateFeatureInfo();
    }

    /**
     * Create an instance of {@link InReleaseResourceNumberInfo }
     * 
     */
    public InReleaseResourceNumberInfo createInReleaseResourceNumberInfo() {
        return new InReleaseResourceNumberInfo();
    }

    /**
     * Create an instance of {@link OutUpdateSubscriberFtrSwPrm }
     * 
     */
    public OutUpdateSubscriberFtrSwPrm createOutUpdateSubscriberFtrSwPrm() {
        return new OutUpdateSubscriberFtrSwPrm();
    }

    /**
     * Create an instance of {@link BillImageInfo }
     * 
     */
    public BillImageInfo createBillImageInfo() {
        return new BillImageInfo();
    }

    /**
     * Create an instance of {@link FavoriteNumberInfo }
     * 
     */
    public FavoriteNumberInfo createFavoriteNumberInfo() {
        return new FavoriteNumberInfo();
    }

    /**
     * Create an instance of {@link InGetPaymentArrangByBanInfo }
     * 
     */
    public InGetPaymentArrangByBanInfo createInGetPaymentArrangByBanInfo() {
        return new InGetPaymentArrangByBanInfo();
    }

    /**
     * Create an instance of {@link OutGetPaymentArrangByBanInfo }
     * 
     */
    public OutGetPaymentArrangByBanInfo createOutGetPaymentArrangByBanInfo() {
        return new OutGetPaymentArrangByBanInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/types/", name = "ValidateExceptionElement")
    public JAXBElement<ValidateException> createValidateExceptionElement(ValidateException value) {
        return new JAXBElement<ValidateException>(_ValidateExceptionElement_QNAME, ValidateException.class, null, value);
    }

}
