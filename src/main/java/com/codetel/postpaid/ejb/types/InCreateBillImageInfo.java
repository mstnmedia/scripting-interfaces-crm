
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InCreateBillImageInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InCreateBillImageInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="runYear" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cycleCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="runMonth" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="billSequenceNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="formatCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="banId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="throwExceptionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InCreateBillImageInfo", propOrder = {
    "runYear",
    "cycleCode",
    "runMonth",
    "billSequenceNo",
    "formatCode",
    "banId",
    "clientSystem",
    "throwExceptionInd"
})
public class InCreateBillImageInfo {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer runYear;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer cycleCode;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer runMonth;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer billSequenceNo;
    @XmlElement(required = true, nillable = true)
    protected String formatCode;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long banId;
    @XmlElement(required = true, nillable = true)
    protected String clientSystem;
    @XmlElement(required = true, nillable = true)
    protected String throwExceptionInd;

    /**
     * Gets the value of the runYear property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRunYear() {
        return runYear;
    }

    /**
     * Sets the value of the runYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRunYear(Integer value) {
        this.runYear = value;
    }

    /**
     * Gets the value of the cycleCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCycleCode() {
        return cycleCode;
    }

    /**
     * Sets the value of the cycleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCycleCode(Integer value) {
        this.cycleCode = value;
    }

    /**
     * Gets the value of the runMonth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRunMonth() {
        return runMonth;
    }

    /**
     * Sets the value of the runMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRunMonth(Integer value) {
        this.runMonth = value;
    }

    /**
     * Gets the value of the billSequenceNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBillSequenceNo() {
        return billSequenceNo;
    }

    /**
     * Sets the value of the billSequenceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBillSequenceNo(Integer value) {
        this.billSequenceNo = value;
    }

    /**
     * Gets the value of the formatCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormatCode() {
        return formatCode;
    }

    /**
     * Sets the value of the formatCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormatCode(String value) {
        this.formatCode = value;
    }

    /**
     * Gets the value of the banId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBanId() {
        return banId;
    }

    /**
     * Sets the value of the banId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBanId(Long value) {
        this.banId = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the throwExceptionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThrowExceptionInd() {
        return throwExceptionInd;
    }

    /**
     * Sets the value of the throwExceptionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThrowExceptionInd(String value) {
        this.throwExceptionInd = value;
    }

}
