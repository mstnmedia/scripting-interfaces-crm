
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdjCreationByCrgInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdjCreationByCrgInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="roamingTaxAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxType3" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="taxType4" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="taxType1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="taxCustAmt1" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxType2" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="taxCustAmt2" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxCustAmt3" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxCustAmt4" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxCustAmt5" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxCustAmt6" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stateTaxCustAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="countyTaxAuthAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxAuth2" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="taxAuth1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="chargeStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="misc1TaxCustAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="misc2TaxAuthAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="balanceImpactCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cityTaxAuthAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="billId" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="taxType6" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="taxType5" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="misc1TaxAuthAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="stateTaxAuthAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="chargeId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="taxAuth5" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="adjustmentAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxAuth6" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="taxAuth3" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="misc2TaxCustAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxAuth4" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="userComment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="taxCalculatedInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adjustmentReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="federalTaxAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="cityTaxCustAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="countyTaxCustAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdjCreationByCrgInfo", propOrder = {
    "roamingTaxAmt",
    "taxType3",
    "taxType4",
    "taxType1",
    "taxCustAmt1",
    "taxType2",
    "taxCustAmt2",
    "taxCustAmt3",
    "taxCustAmt4",
    "taxCustAmt5",
    "taxCustAmt6",
    "productType",
    "stateTaxCustAmt",
    "countyTaxAuthAmt",
    "taxAuth2",
    "taxAuth1",
    "chargeStatus",
    "misc1TaxCustAmt",
    "misc2TaxAuthAmt",
    "balanceImpactCode",
    "cityTaxAuthAmt",
    "billId",
    "taxType6",
    "taxType5",
    "misc1TaxAuthAmt",
    "stateTaxAuthAmt",
    "chargeId",
    "taxAuth5",
    "adjustmentAmount",
    "taxAuth6",
    "taxAuth3",
    "misc2TaxCustAmt",
    "taxAuth4",
    "userComment",
    "taxCalculatedInd",
    "adjustmentReason",
    "federalTaxAmt",
    "cityTaxCustAmt",
    "countyTaxCustAmt"
})
public class AdjCreationByCrgInfo {

    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double roamingTaxAmt;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxType3;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxType4;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxType1;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double taxCustAmt1;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxType2;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double taxCustAmt2;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double taxCustAmt3;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double taxCustAmt4;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double taxCustAmt5;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double taxCustAmt6;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double stateTaxCustAmt;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double countyTaxAuthAmt;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxAuth2;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxAuth1;
    @XmlElement(required = true, nillable = true)
    protected String chargeStatus;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double misc1TaxCustAmt;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double misc2TaxAuthAmt;
    @XmlElement(required = true, nillable = true)
    protected String balanceImpactCode;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double cityTaxAuthAmt;
    @XmlElement(required = true, type = Short.class, nillable = true)
    protected Short billId;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxType6;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxType5;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double misc1TaxAuthAmt;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double stateTaxAuthAmt;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer chargeId;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxAuth5;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double adjustmentAmount;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxAuth6;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxAuth3;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double misc2TaxCustAmt;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer taxAuth4;
    @XmlElement(required = true, nillable = true)
    protected String userComment;
    @XmlElement(required = true, nillable = true)
    protected String taxCalculatedInd;
    @XmlElement(required = true, nillable = true)
    protected String adjustmentReason;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double federalTaxAmt;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double cityTaxCustAmt;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double countyTaxCustAmt;

    /**
     * Gets the value of the roamingTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getRoamingTaxAmt() {
        return roamingTaxAmt;
    }

    /**
     * Sets the value of the roamingTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setRoamingTaxAmt(Double value) {
        this.roamingTaxAmt = value;
    }

    /**
     * Gets the value of the taxType3 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxType3() {
        return taxType3;
    }

    /**
     * Sets the value of the taxType3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxType3(Integer value) {
        this.taxType3 = value;
    }

    /**
     * Gets the value of the taxType4 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxType4() {
        return taxType4;
    }

    /**
     * Sets the value of the taxType4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxType4(Integer value) {
        this.taxType4 = value;
    }

    /**
     * Gets the value of the taxType1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxType1() {
        return taxType1;
    }

    /**
     * Sets the value of the taxType1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxType1(Integer value) {
        this.taxType1 = value;
    }

    /**
     * Gets the value of the taxCustAmt1 property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxCustAmt1() {
        return taxCustAmt1;
    }

    /**
     * Sets the value of the taxCustAmt1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxCustAmt1(Double value) {
        this.taxCustAmt1 = value;
    }

    /**
     * Gets the value of the taxType2 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxType2() {
        return taxType2;
    }

    /**
     * Sets the value of the taxType2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxType2(Integer value) {
        this.taxType2 = value;
    }

    /**
     * Gets the value of the taxCustAmt2 property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxCustAmt2() {
        return taxCustAmt2;
    }

    /**
     * Sets the value of the taxCustAmt2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxCustAmt2(Double value) {
        this.taxCustAmt2 = value;
    }

    /**
     * Gets the value of the taxCustAmt3 property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxCustAmt3() {
        return taxCustAmt3;
    }

    /**
     * Sets the value of the taxCustAmt3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxCustAmt3(Double value) {
        this.taxCustAmt3 = value;
    }

    /**
     * Gets the value of the taxCustAmt4 property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxCustAmt4() {
        return taxCustAmt4;
    }

    /**
     * Sets the value of the taxCustAmt4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxCustAmt4(Double value) {
        this.taxCustAmt4 = value;
    }

    /**
     * Gets the value of the taxCustAmt5 property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxCustAmt5() {
        return taxCustAmt5;
    }

    /**
     * Sets the value of the taxCustAmt5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxCustAmt5(Double value) {
        this.taxCustAmt5 = value;
    }

    /**
     * Gets the value of the taxCustAmt6 property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxCustAmt6() {
        return taxCustAmt6;
    }

    /**
     * Sets the value of the taxCustAmt6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxCustAmt6(Double value) {
        this.taxCustAmt6 = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the stateTaxCustAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getStateTaxCustAmt() {
        return stateTaxCustAmt;
    }

    /**
     * Sets the value of the stateTaxCustAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setStateTaxCustAmt(Double value) {
        this.stateTaxCustAmt = value;
    }

    /**
     * Gets the value of the countyTaxAuthAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCountyTaxAuthAmt() {
        return countyTaxAuthAmt;
    }

    /**
     * Sets the value of the countyTaxAuthAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCountyTaxAuthAmt(Double value) {
        this.countyTaxAuthAmt = value;
    }

    /**
     * Gets the value of the taxAuth2 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxAuth2() {
        return taxAuth2;
    }

    /**
     * Sets the value of the taxAuth2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxAuth2(Integer value) {
        this.taxAuth2 = value;
    }

    /**
     * Gets the value of the taxAuth1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxAuth1() {
        return taxAuth1;
    }

    /**
     * Sets the value of the taxAuth1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxAuth1(Integer value) {
        this.taxAuth1 = value;
    }

    /**
     * Gets the value of the chargeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeStatus() {
        return chargeStatus;
    }

    /**
     * Sets the value of the chargeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeStatus(String value) {
        this.chargeStatus = value;
    }

    /**
     * Gets the value of the misc1TaxCustAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMisc1TaxCustAmt() {
        return misc1TaxCustAmt;
    }

    /**
     * Sets the value of the misc1TaxCustAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMisc1TaxCustAmt(Double value) {
        this.misc1TaxCustAmt = value;
    }

    /**
     * Gets the value of the misc2TaxAuthAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMisc2TaxAuthAmt() {
        return misc2TaxAuthAmt;
    }

    /**
     * Sets the value of the misc2TaxAuthAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMisc2TaxAuthAmt(Double value) {
        this.misc2TaxAuthAmt = value;
    }

    /**
     * Gets the value of the balanceImpactCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceImpactCode() {
        return balanceImpactCode;
    }

    /**
     * Sets the value of the balanceImpactCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceImpactCode(String value) {
        this.balanceImpactCode = value;
    }

    /**
     * Gets the value of the cityTaxAuthAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCityTaxAuthAmt() {
        return cityTaxAuthAmt;
    }

    /**
     * Sets the value of the cityTaxAuthAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCityTaxAuthAmt(Double value) {
        this.cityTaxAuthAmt = value;
    }

    /**
     * Gets the value of the billId property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getBillId() {
        return billId;
    }

    /**
     * Sets the value of the billId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setBillId(Short value) {
        this.billId = value;
    }

    /**
     * Gets the value of the taxType6 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxType6() {
        return taxType6;
    }

    /**
     * Sets the value of the taxType6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxType6(Integer value) {
        this.taxType6 = value;
    }

    /**
     * Gets the value of the taxType5 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxType5() {
        return taxType5;
    }

    /**
     * Sets the value of the taxType5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxType5(Integer value) {
        this.taxType5 = value;
    }

    /**
     * Gets the value of the misc1TaxAuthAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMisc1TaxAuthAmt() {
        return misc1TaxAuthAmt;
    }

    /**
     * Sets the value of the misc1TaxAuthAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMisc1TaxAuthAmt(Double value) {
        this.misc1TaxAuthAmt = value;
    }

    /**
     * Gets the value of the stateTaxAuthAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getStateTaxAuthAmt() {
        return stateTaxAuthAmt;
    }

    /**
     * Sets the value of the stateTaxAuthAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setStateTaxAuthAmt(Double value) {
        this.stateTaxAuthAmt = value;
    }

    /**
     * Gets the value of the chargeId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChargeId() {
        return chargeId;
    }

    /**
     * Sets the value of the chargeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChargeId(Integer value) {
        this.chargeId = value;
    }

    /**
     * Gets the value of the taxAuth5 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxAuth5() {
        return taxAuth5;
    }

    /**
     * Sets the value of the taxAuth5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxAuth5(Integer value) {
        this.taxAuth5 = value;
    }

    /**
     * Gets the value of the adjustmentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAdjustmentAmount() {
        return adjustmentAmount;
    }

    /**
     * Sets the value of the adjustmentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAdjustmentAmount(Double value) {
        this.adjustmentAmount = value;
    }

    /**
     * Gets the value of the taxAuth6 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxAuth6() {
        return taxAuth6;
    }

    /**
     * Sets the value of the taxAuth6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxAuth6(Integer value) {
        this.taxAuth6 = value;
    }

    /**
     * Gets the value of the taxAuth3 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxAuth3() {
        return taxAuth3;
    }

    /**
     * Sets the value of the taxAuth3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxAuth3(Integer value) {
        this.taxAuth3 = value;
    }

    /**
     * Gets the value of the misc2TaxCustAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMisc2TaxCustAmt() {
        return misc2TaxCustAmt;
    }

    /**
     * Sets the value of the misc2TaxCustAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMisc2TaxCustAmt(Double value) {
        this.misc2TaxCustAmt = value;
    }

    /**
     * Gets the value of the taxAuth4 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxAuth4() {
        return taxAuth4;
    }

    /**
     * Sets the value of the taxAuth4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxAuth4(Integer value) {
        this.taxAuth4 = value;
    }

    /**
     * Gets the value of the userComment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserComment() {
        return userComment;
    }

    /**
     * Sets the value of the userComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserComment(String value) {
        this.userComment = value;
    }

    /**
     * Gets the value of the taxCalculatedInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCalculatedInd() {
        return taxCalculatedInd;
    }

    /**
     * Sets the value of the taxCalculatedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCalculatedInd(String value) {
        this.taxCalculatedInd = value;
    }

    /**
     * Gets the value of the adjustmentReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustmentReason() {
        return adjustmentReason;
    }

    /**
     * Sets the value of the adjustmentReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustmentReason(String value) {
        this.adjustmentReason = value;
    }

    /**
     * Gets the value of the federalTaxAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getFederalTaxAmt() {
        return federalTaxAmt;
    }

    /**
     * Sets the value of the federalTaxAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setFederalTaxAmt(Double value) {
        this.federalTaxAmt = value;
    }

    /**
     * Gets the value of the cityTaxCustAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCityTaxCustAmt() {
        return cityTaxCustAmt;
    }

    /**
     * Sets the value of the cityTaxCustAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCityTaxCustAmt(Double value) {
        this.cityTaxCustAmt = value;
    }

    /**
     * Gets the value of the countyTaxCustAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCountyTaxCustAmt() {
        return countyTaxCustAmt;
    }

    /**
     * Sets the value of the countyTaxCustAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCountyTaxCustAmt(Double value) {
        this.countyTaxCustAmt = value;
    }

}
