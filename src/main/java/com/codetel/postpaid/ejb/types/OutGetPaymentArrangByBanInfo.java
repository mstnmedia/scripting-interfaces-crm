
package com.codetel.postpaid.ejb.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutGetPaymentArrangByBanInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutGetPaymentArrangByBanInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorInfo" type="{http://ejb.postpaid.codetel.com/types/}CodedMessageInfo"/>
 *         &lt;element name="totalInstallment" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PASeqNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PAInstallmentInfo" type="{http://ejb.postpaid.codetel.com/types/}PAInstallmentInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutGetPaymentArrangByBanInfo", propOrder = {
    "apiStatus",
    "errorInfo",
    "totalInstallment",
    "paSeqNo",
    "paInstallmentInfo"
})
public class OutGetPaymentArrangByBanInfo {

    @XmlElement(required = true, nillable = true)
    protected String apiStatus;
    @XmlElement(required = true, nillable = true)
    protected CodedMessageInfo errorInfo;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer totalInstallment;
    @XmlElement(name = "PASeqNo", required = true, type = Integer.class, nillable = true)
    protected Integer paSeqNo;
    @XmlElement(name = "PAInstallmentInfo", nillable = true)
    protected List<PAInstallmentInfo> paInstallmentInfo;

    /**
     * Gets the value of the apiStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * Sets the value of the apiStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiStatus(String value) {
        this.apiStatus = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CodedMessageInfo }
     *     
     */
    public CodedMessageInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedMessageInfo }
     *     
     */
    public void setErrorInfo(CodedMessageInfo value) {
        this.errorInfo = value;
    }

    /**
     * Gets the value of the totalInstallment property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalInstallment() {
        return totalInstallment;
    }

    /**
     * Sets the value of the totalInstallment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalInstallment(Integer value) {
        this.totalInstallment = value;
    }

    /**
     * Gets the value of the paSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPASeqNo() {
        return paSeqNo;
    }

    /**
     * Sets the value of the paSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPASeqNo(Integer value) {
        this.paSeqNo = value;
    }

    /**
     * Gets the value of the paInstallmentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paInstallmentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPAInstallmentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PAInstallmentInfo }
     * 
     * 
     */
    public List<PAInstallmentInfo> getPAInstallmentInfo() {
        if (paInstallmentInfo == null) {
            paInstallmentInfo = new ArrayList<PAInstallmentInfo>();
        }
        return this.paInstallmentInfo;
    }

}
