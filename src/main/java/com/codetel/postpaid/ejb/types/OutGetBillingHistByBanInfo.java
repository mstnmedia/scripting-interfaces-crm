
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OutGetBillingHistByBanInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutGetBillingHistByBanInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ban" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="billDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="totalBilledCharge" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="billDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="paymentLimitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="totalBilledAdjusts" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="paymentReceivedAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="currentChargeAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="LDCTollAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="currentCreditAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="RCAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="LDCAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="pastDueAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="previousBalanceAmt" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="apiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorInfo" type="{http://ejb.postpaid.codetel.com/types/}CodedMessageInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutGetBillingHistByBanInfo", propOrder = {
    "ban",
    "billDueDate",
    "totalBilledCharge",
    "status",
    "billDate",
    "paymentLimitDate",
    "totalBilledAdjusts",
    "paymentReceivedAmt",
    "currentChargeAmt",
    "ldcTollAmount",
    "currentCreditAmt",
    "taxAmount",
    "rcAmount",
    "ldcAmount",
    "pastDueAmt",
    "previousBalanceAmt",
    "apiStatus",
    "errorInfo"
})
public class OutGetBillingHistByBanInfo {

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long ban;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar billDueDate;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double totalBilledCharge;
    @XmlElement(required = true, nillable = true)
    protected String status;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar billDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paymentLimitDate;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double totalBilledAdjusts;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double paymentReceivedAmt;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double currentChargeAmt;
    @XmlElement(name = "LDCTollAmount", required = true, type = Double.class, nillable = true)
    protected Double ldcTollAmount;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double currentCreditAmt;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double taxAmount;
    @XmlElement(name = "RCAmount", required = true, type = Double.class, nillable = true)
    protected Double rcAmount;
    @XmlElement(name = "LDCAmount", required = true, type = Double.class, nillable = true)
    protected Double ldcAmount;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double pastDueAmt;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double previousBalanceAmt;
    @XmlElement(required = true, nillable = true)
    protected String apiStatus;
    @XmlElement(required = true, nillable = true)
    protected CodedMessageInfo errorInfo;

    /**
     * Gets the value of the ban property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBan() {
        return ban;
    }

    /**
     * Sets the value of the ban property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBan(Long value) {
        this.ban = value;
    }

    /**
     * Gets the value of the billDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBillDueDate() {
        return billDueDate;
    }

    /**
     * Sets the value of the billDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBillDueDate(XMLGregorianCalendar value) {
        this.billDueDate = value;
    }

    /**
     * Gets the value of the totalBilledCharge property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTotalBilledCharge() {
        return totalBilledCharge;
    }

    /**
     * Sets the value of the totalBilledCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTotalBilledCharge(Double value) {
        this.totalBilledCharge = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the billDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBillDate() {
        return billDate;
    }

    /**
     * Sets the value of the billDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBillDate(XMLGregorianCalendar value) {
        this.billDate = value;
    }

    /**
     * Gets the value of the paymentLimitDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentLimitDate() {
        return paymentLimitDate;
    }

    /**
     * Sets the value of the paymentLimitDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentLimitDate(XMLGregorianCalendar value) {
        this.paymentLimitDate = value;
    }

    /**
     * Gets the value of the totalBilledAdjusts property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTotalBilledAdjusts() {
        return totalBilledAdjusts;
    }

    /**
     * Sets the value of the totalBilledAdjusts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTotalBilledAdjusts(Double value) {
        this.totalBilledAdjusts = value;
    }

    /**
     * Gets the value of the paymentReceivedAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPaymentReceivedAmt() {
        return paymentReceivedAmt;
    }

    /**
     * Sets the value of the paymentReceivedAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPaymentReceivedAmt(Double value) {
        this.paymentReceivedAmt = value;
    }

    /**
     * Gets the value of the currentChargeAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCurrentChargeAmt() {
        return currentChargeAmt;
    }

    /**
     * Sets the value of the currentChargeAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCurrentChargeAmt(Double value) {
        this.currentChargeAmt = value;
    }

    /**
     * Gets the value of the ldcTollAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLDCTollAmount() {
        return ldcTollAmount;
    }

    /**
     * Sets the value of the ldcTollAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLDCTollAmount(Double value) {
        this.ldcTollAmount = value;
    }

    /**
     * Gets the value of the currentCreditAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCurrentCreditAmt() {
        return currentCreditAmt;
    }

    /**
     * Sets the value of the currentCreditAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCurrentCreditAmt(Double value) {
        this.currentCreditAmt = value;
    }

    /**
     * Gets the value of the taxAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxAmount() {
        return taxAmount;
    }

    /**
     * Sets the value of the taxAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxAmount(Double value) {
        this.taxAmount = value;
    }

    /**
     * Gets the value of the rcAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getRCAmount() {
        return rcAmount;
    }

    /**
     * Sets the value of the rcAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setRCAmount(Double value) {
        this.rcAmount = value;
    }

    /**
     * Gets the value of the ldcAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLDCAmount() {
        return ldcAmount;
    }

    /**
     * Sets the value of the ldcAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLDCAmount(Double value) {
        this.ldcAmount = value;
    }

    /**
     * Gets the value of the pastDueAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPastDueAmt() {
        return pastDueAmt;
    }

    /**
     * Sets the value of the pastDueAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPastDueAmt(Double value) {
        this.pastDueAmt = value;
    }

    /**
     * Gets the value of the previousBalanceAmt property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPreviousBalanceAmt() {
        return previousBalanceAmt;
    }

    /**
     * Sets the value of the previousBalanceAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPreviousBalanceAmt(Double value) {
        this.previousBalanceAmt = value;
    }

    /**
     * Gets the value of the apiStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * Sets the value of the apiStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiStatus(String value) {
        this.apiStatus = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CodedMessageInfo }
     *     
     */
    public CodedMessageInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedMessageInfo }
     *     
     */
    public void setErrorInfo(CodedMessageInfo value) {
        this.errorInfo = value;
    }

}
