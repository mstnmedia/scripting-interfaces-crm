
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdjustmentCreationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdjustmentCreationInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userComment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="taxCalculatedInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adjustmentReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="balanceImpactCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msgExistInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adjustmentAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="taxCommissionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdjustmentCreationInfo", propOrder = {
    "userComment",
    "taxCalculatedInd",
    "adjustmentReason",
    "balanceImpactCode",
    "msgExistInd",
    "adjustmentAmount",
    "productType",
    "taxCommissionInd"
})
public class AdjustmentCreationInfo {

    @XmlElement(required = true, nillable = true)
    protected String userComment;
    @XmlElement(required = true, nillable = true)
    protected String taxCalculatedInd;
    @XmlElement(required = true, nillable = true)
    protected String adjustmentReason;
    @XmlElement(required = true, nillable = true)
    protected String balanceImpactCode;
    @XmlElement(required = true, nillable = true)
    protected String msgExistInd;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double adjustmentAmount;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, nillable = true)
    protected String taxCommissionInd;

    /**
     * Gets the value of the userComment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserComment() {
        return userComment;
    }

    /**
     * Sets the value of the userComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserComment(String value) {
        this.userComment = value;
    }

    /**
     * Gets the value of the taxCalculatedInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCalculatedInd() {
        return taxCalculatedInd;
    }

    /**
     * Sets the value of the taxCalculatedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCalculatedInd(String value) {
        this.taxCalculatedInd = value;
    }

    /**
     * Gets the value of the adjustmentReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustmentReason() {
        return adjustmentReason;
    }

    /**
     * Sets the value of the adjustmentReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustmentReason(String value) {
        this.adjustmentReason = value;
    }

    /**
     * Gets the value of the balanceImpactCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceImpactCode() {
        return balanceImpactCode;
    }

    /**
     * Sets the value of the balanceImpactCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceImpactCode(String value) {
        this.balanceImpactCode = value;
    }

    /**
     * Gets the value of the msgExistInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgExistInd() {
        return msgExistInd;
    }

    /**
     * Sets the value of the msgExistInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgExistInd(String value) {
        this.msgExistInd = value;
    }

    /**
     * Gets the value of the adjustmentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAdjustmentAmount() {
        return adjustmentAmount;
    }

    /**
     * Sets the value of the adjustmentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAdjustmentAmount(Double value) {
        this.adjustmentAmount = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the taxCommissionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCommissionInd() {
        return taxCommissionInd;
    }

    /**
     * Sets the value of the taxCommissionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCommissionInd(String value) {
        this.taxCommissionInd = value;
    }

}
