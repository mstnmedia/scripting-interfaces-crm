
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="floorType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="buildingName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="apartmentNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="territorialUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="houseNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="territorialUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="urbanizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="distributionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="section" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="secondaryLine" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="county" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="floorNumber" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="apartmentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sectionType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="buildingType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="line1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="streetType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="attention" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="line2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressInfo", propOrder = {
    "floorType",
    "buildingName",
    "apartmentNumber",
    "street",
    "state",
    "territorialUnitType",
    "houseNumber",
    "territorialUnit",
    "urbanizationCode",
    "type",
    "distributionCode",
    "section",
    "city",
    "secondaryLine",
    "county",
    "floorNumber",
    "apartmentType",
    "sectionType",
    "buildingType",
    "line1",
    "streetType",
    "attention",
    "line2"
})
public class AddressInfo {

    @XmlElement(required = true, nillable = true)
    protected String floorType;
    @XmlElement(required = true, nillable = true)
    protected String buildingName;
    @XmlElement(required = true, nillable = true)
    protected String apartmentNumber;
    @XmlElement(required = true, nillable = true)
    protected String street;
    @XmlElement(required = true, nillable = true)
    protected String state;
    @XmlElement(required = true, nillable = true)
    protected String territorialUnitType;
    @XmlElement(required = true, nillable = true)
    protected String houseNumber;
    @XmlElement(required = true, nillable = true)
    protected String territorialUnit;
    @XmlElement(required = true, nillable = true)
    protected String urbanizationCode;
    @XmlElement(required = true, nillable = true)
    protected String type;
    @XmlElement(required = true, nillable = true)
    protected String distributionCode;
    @XmlElement(required = true, nillable = true)
    protected String section;
    @XmlElement(required = true, nillable = true)
    protected String city;
    @XmlElement(required = true, nillable = true)
    protected String secondaryLine;
    @XmlElement(required = true, nillable = true)
    protected String county;
    @XmlElement(required = true, type = Short.class, nillable = true)
    protected Short floorNumber;
    @XmlElement(required = true, nillable = true)
    protected String apartmentType;
    @XmlElement(required = true, nillable = true)
    protected String sectionType;
    @XmlElement(required = true, nillable = true)
    protected String buildingType;
    @XmlElement(required = true, nillable = true)
    protected String line1;
    @XmlElement(required = true, nillable = true)
    protected String streetType;
    @XmlElement(required = true, nillable = true)
    protected String attention;
    @XmlElement(required = true, nillable = true)
    protected String line2;

    /**
     * Gets the value of the floorType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFloorType() {
        return floorType;
    }

    /**
     * Sets the value of the floorType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFloorType(String value) {
        this.floorType = value;
    }

    /**
     * Gets the value of the buildingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildingName() {
        return buildingName;
    }

    /**
     * Sets the value of the buildingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildingName(String value) {
        this.buildingName = value;
    }

    /**
     * Gets the value of the apartmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApartmentNumber() {
        return apartmentNumber;
    }

    /**
     * Sets the value of the apartmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApartmentNumber(String value) {
        this.apartmentNumber = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the territorialUnitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerritorialUnitType() {
        return territorialUnitType;
    }

    /**
     * Sets the value of the territorialUnitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerritorialUnitType(String value) {
        this.territorialUnitType = value;
    }

    /**
     * Gets the value of the houseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseNumber() {
        return houseNumber;
    }

    /**
     * Sets the value of the houseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseNumber(String value) {
        this.houseNumber = value;
    }

    /**
     * Gets the value of the territorialUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerritorialUnit() {
        return territorialUnit;
    }

    /**
     * Sets the value of the territorialUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerritorialUnit(String value) {
        this.territorialUnit = value;
    }

    /**
     * Gets the value of the urbanizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrbanizationCode() {
        return urbanizationCode;
    }

    /**
     * Sets the value of the urbanizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrbanizationCode(String value) {
        this.urbanizationCode = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the distributionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributionCode() {
        return distributionCode;
    }

    /**
     * Sets the value of the distributionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributionCode(String value) {
        this.distributionCode = value;
    }

    /**
     * Gets the value of the section property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSection() {
        return section;
    }

    /**
     * Sets the value of the section property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSection(String value) {
        this.section = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the secondaryLine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryLine() {
        return secondaryLine;
    }

    /**
     * Sets the value of the secondaryLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryLine(String value) {
        this.secondaryLine = value;
    }

    /**
     * Gets the value of the county property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCounty(String value) {
        this.county = value;
    }

    /**
     * Gets the value of the floorNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getFloorNumber() {
        return floorNumber;
    }

    /**
     * Sets the value of the floorNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setFloorNumber(Short value) {
        this.floorNumber = value;
    }

    /**
     * Gets the value of the apartmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApartmentType() {
        return apartmentType;
    }

    /**
     * Sets the value of the apartmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApartmentType(String value) {
        this.apartmentType = value;
    }

    /**
     * Gets the value of the sectionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSectionType() {
        return sectionType;
    }

    /**
     * Sets the value of the sectionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSectionType(String value) {
        this.sectionType = value;
    }

    /**
     * Gets the value of the buildingType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildingType() {
        return buildingType;
    }

    /**
     * Sets the value of the buildingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildingType(String value) {
        this.buildingType = value;
    }

    /**
     * Gets the value of the line1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLine1() {
        return line1;
    }

    /**
     * Sets the value of the line1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLine1(String value) {
        this.line1 = value;
    }

    /**
     * Gets the value of the streetType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetType() {
        return streetType;
    }

    /**
     * Sets the value of the streetType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetType(String value) {
        this.streetType = value;
    }

    /**
     * Gets the value of the attention property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttention() {
        return attention;
    }

    /**
     * Sets the value of the attention property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttention(String value) {
        this.attention = value;
    }

    /**
     * Gets the value of the line2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLine2() {
        return line2;
    }

    /**
     * Sets the value of the line2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLine2(String value) {
        this.line2 = value;
    }

}
