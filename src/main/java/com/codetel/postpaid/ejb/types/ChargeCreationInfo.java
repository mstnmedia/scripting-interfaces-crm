
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ChargeCreationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChargeCreationInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numberOfInstallment" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="chargeEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="chargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="chargeOverridenTotalAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargeCreationInfo", propOrder = {
    "numberOfInstallment",
    "chargeEffectiveDate",
    "chargeCode",
    "chargeOverridenTotalAmount"
})
public class ChargeCreationInfo {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer numberOfInstallment;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar chargeEffectiveDate;
    @XmlElement(required = true, nillable = true)
    protected String chargeCode;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double chargeOverridenTotalAmount;

    /**
     * Gets the value of the numberOfInstallment property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfInstallment() {
        return numberOfInstallment;
    }

    /**
     * Sets the value of the numberOfInstallment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfInstallment(Integer value) {
        this.numberOfInstallment = value;
    }

    /**
     * Gets the value of the chargeEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getChargeEffectiveDate() {
        return chargeEffectiveDate;
    }

    /**
     * Sets the value of the chargeEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setChargeEffectiveDate(XMLGregorianCalendar value) {
        this.chargeEffectiveDate = value;
    }

    /**
     * Gets the value of the chargeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * Sets the value of the chargeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeCode(String value) {
        this.chargeCode = value;
    }

    /**
     * Gets the value of the chargeOverridenTotalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getChargeOverridenTotalAmount() {
        return chargeOverridenTotalAmount;
    }

    /**
     * Sets the value of the chargeOverridenTotalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setChargeOverridenTotalAmount(Double value) {
        this.chargeOverridenTotalAmount = value;
    }

}
