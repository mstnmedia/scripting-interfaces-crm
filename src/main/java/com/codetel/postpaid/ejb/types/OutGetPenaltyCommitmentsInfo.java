
package com.codetel.postpaid.ejb.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutGetPenaltyCommitmentsInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutGetPenaltyCommitmentsInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorInfo" type="{http://ejb.postpaid.codetel.com/types/}CodedMessageInfo"/>
 *         &lt;element name="totalEffectivePenalty" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="penaltyCommitments" type="{http://ejb.postpaid.codetel.com/types/}PenaltyCommitmentInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutGetPenaltyCommitmentsInfo", propOrder = {
    "apiStatus",
    "errorInfo",
    "totalEffectivePenalty",
    "penaltyCommitments"
})
public class OutGetPenaltyCommitmentsInfo {

    @XmlElement(required = true, nillable = true)
    protected String apiStatus;
    @XmlElement(required = true, nillable = true)
    protected CodedMessageInfo errorInfo;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double totalEffectivePenalty;
    @XmlElement(nillable = true)
    protected List<PenaltyCommitmentInfo> penaltyCommitments;

    /**
     * Gets the value of the apiStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * Sets the value of the apiStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiStatus(String value) {
        this.apiStatus = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CodedMessageInfo }
     *     
     */
    public CodedMessageInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedMessageInfo }
     *     
     */
    public void setErrorInfo(CodedMessageInfo value) {
        this.errorInfo = value;
    }

    /**
     * Gets the value of the totalEffectivePenalty property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTotalEffectivePenalty() {
        return totalEffectivePenalty;
    }

    /**
     * Sets the value of the totalEffectivePenalty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTotalEffectivePenalty(Double value) {
        this.totalEffectivePenalty = value;
    }

    /**
     * Gets the value of the penaltyCommitments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the penaltyCommitments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPenaltyCommitments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PenaltyCommitmentInfo }
     * 
     * 
     */
    public List<PenaltyCommitmentInfo> getPenaltyCommitments() {
        if (penaltyCommitments == null) {
            penaltyCommitments = new ArrayList<PenaltyCommitmentInfo>();
        }
        return this.penaltyCommitments;
    }

}
