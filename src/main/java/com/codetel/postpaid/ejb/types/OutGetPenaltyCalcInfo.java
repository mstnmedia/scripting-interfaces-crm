
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutGetPenaltyCalcInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutGetPenaltyCalcInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorInfo" type="{http://ejb.postpaid.codetel.com/types/}CodedMessageInfo"/>
 *         &lt;element name="penaltySubsidy" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="penaltyDiscount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="remainingMounths" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="commitmentMonths" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutGetPenaltyCalcInfo", propOrder = {
    "apiStatus",
    "errorInfo",
    "penaltySubsidy",
    "penaltyDiscount",
    "remainingMounths",
    "commitmentMonths"
})
public class OutGetPenaltyCalcInfo {

    @XmlElement(required = true, nillable = true)
    protected String apiStatus;
    @XmlElement(required = true, nillable = true)
    protected CodedMessageInfo errorInfo;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double penaltySubsidy;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double penaltyDiscount;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer remainingMounths;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer commitmentMonths;

    /**
     * Gets the value of the apiStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * Sets the value of the apiStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiStatus(String value) {
        this.apiStatus = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CodedMessageInfo }
     *     
     */
    public CodedMessageInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedMessageInfo }
     *     
     */
    public void setErrorInfo(CodedMessageInfo value) {
        this.errorInfo = value;
    }

    /**
     * Gets the value of the penaltySubsidy property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPenaltySubsidy() {
        return penaltySubsidy;
    }

    /**
     * Sets the value of the penaltySubsidy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPenaltySubsidy(Double value) {
        this.penaltySubsidy = value;
    }

    /**
     * Gets the value of the penaltyDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPenaltyDiscount() {
        return penaltyDiscount;
    }

    /**
     * Sets the value of the penaltyDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPenaltyDiscount(Double value) {
        this.penaltyDiscount = value;
    }

    /**
     * Gets the value of the remainingMounths property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRemainingMounths() {
        return remainingMounths;
    }

    /**
     * Sets the value of the remainingMounths property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRemainingMounths(Integer value) {
        this.remainingMounths = value;
    }

    /**
     * Gets the value of the commitmentMonths property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCommitmentMonths() {
        return commitmentMonths;
    }

    /**
     * Sets the value of the commitmentMonths property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCommitmentMonths(Integer value) {
        this.commitmentMonths = value;
    }

}
