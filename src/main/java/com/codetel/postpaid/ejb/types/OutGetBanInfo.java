
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutGetBanInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutGetBanInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paymentIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ban" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="telcomBalance" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="leasingBalance" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="ARBalance" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="accountSubType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nonTelcomPastDueAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="nonTelcomBalance" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="reconnectionPaymentAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="billCycle" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="telcomPastDueAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="accountType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pastDueAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="CCU" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="leasingPastDueAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="apiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorInfo" type="{http://ejb.postpaid.codetel.com/types/}CodedMessageInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutGetBanInfo", propOrder = {
    "paymentIndicator",
    "ban",
    "status",
    "telcomBalance",
    "leasingBalance",
    "arBalance",
    "accountSubType",
    "nonTelcomPastDueAmount",
    "nonTelcomBalance",
    "reconnectionPaymentAmount",
    "billCycle",
    "telcomPastDueAmount",
    "accountType",
    "pastDueAmount",
    "ccu",
    "leasingPastDueAmount",
    "apiStatus",
    "errorInfo"
})
public class OutGetBanInfo {

    @XmlElement(required = true, nillable = true)
    protected String paymentIndicator;
    protected long ban;
    @XmlElement(required = true, nillable = true)
    protected String status;
    protected double telcomBalance;
    protected double leasingBalance;
    @XmlElement(name = "ARBalance")
    protected double arBalance;
    @XmlElement(required = true, nillable = true)
    protected String accountSubType;
    protected double nonTelcomPastDueAmount;
    protected double nonTelcomBalance;
    protected double reconnectionPaymentAmount;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer billCycle;
    protected double telcomPastDueAmount;
    @XmlElement(required = true, nillable = true)
    protected String accountType;
    protected double pastDueAmount;
    @XmlElement(name = "CCU", required = true, nillable = true)
    protected String ccu;
    protected double leasingPastDueAmount;
    @XmlElement(required = true, nillable = true)
    protected String apiStatus;
    @XmlElement(required = true, nillable = true)
    protected CodedMessageInfo errorInfo;

    /**
     * Gets the value of the paymentIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentIndicator() {
        return paymentIndicator;
    }

    /**
     * Sets the value of the paymentIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentIndicator(String value) {
        this.paymentIndicator = value;
    }

    /**
     * Gets the value of the ban property.
     * 
     */
    public long getBan() {
        return ban;
    }

    /**
     * Sets the value of the ban property.
     * 
     */
    public void setBan(long value) {
        this.ban = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the telcomBalance property.
     * 
     */
    public double getTelcomBalance() {
        return telcomBalance;
    }

    /**
     * Sets the value of the telcomBalance property.
     * 
     */
    public void setTelcomBalance(double value) {
        this.telcomBalance = value;
    }

    /**
     * Gets the value of the leasingBalance property.
     * 
     */
    public double getLeasingBalance() {
        return leasingBalance;
    }

    /**
     * Sets the value of the leasingBalance property.
     * 
     */
    public void setLeasingBalance(double value) {
        this.leasingBalance = value;
    }

    /**
     * Gets the value of the arBalance property.
     * 
     */
    public double getARBalance() {
        return arBalance;
    }

    /**
     * Sets the value of the arBalance property.
     * 
     */
    public void setARBalance(double value) {
        this.arBalance = value;
    }

    /**
     * Gets the value of the accountSubType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountSubType() {
        return accountSubType;
    }

    /**
     * Sets the value of the accountSubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountSubType(String value) {
        this.accountSubType = value;
    }

    /**
     * Gets the value of the nonTelcomPastDueAmount property.
     * 
     */
    public double getNonTelcomPastDueAmount() {
        return nonTelcomPastDueAmount;
    }

    /**
     * Sets the value of the nonTelcomPastDueAmount property.
     * 
     */
    public void setNonTelcomPastDueAmount(double value) {
        this.nonTelcomPastDueAmount = value;
    }

    /**
     * Gets the value of the nonTelcomBalance property.
     * 
     */
    public double getNonTelcomBalance() {
        return nonTelcomBalance;
    }

    /**
     * Sets the value of the nonTelcomBalance property.
     * 
     */
    public void setNonTelcomBalance(double value) {
        this.nonTelcomBalance = value;
    }

    /**
     * Gets the value of the reconnectionPaymentAmount property.
     * 
     */
    public double getReconnectionPaymentAmount() {
        return reconnectionPaymentAmount;
    }

    /**
     * Sets the value of the reconnectionPaymentAmount property.
     * 
     */
    public void setReconnectionPaymentAmount(double value) {
        this.reconnectionPaymentAmount = value;
    }

    /**
     * Gets the value of the billCycle property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBillCycle() {
        return billCycle;
    }

    /**
     * Sets the value of the billCycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBillCycle(Integer value) {
        this.billCycle = value;
    }

    /**
     * Gets the value of the telcomPastDueAmount property.
     * 
     */
    public double getTelcomPastDueAmount() {
        return telcomPastDueAmount;
    }

    /**
     * Sets the value of the telcomPastDueAmount property.
     * 
     */
    public void setTelcomPastDueAmount(double value) {
        this.telcomPastDueAmount = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the pastDueAmount property.
     * 
     */
    public double getPastDueAmount() {
        return pastDueAmount;
    }

    /**
     * Sets the value of the pastDueAmount property.
     * 
     */
    public void setPastDueAmount(double value) {
        this.pastDueAmount = value;
    }

    /**
     * Gets the value of the ccu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCU() {
        return ccu;
    }

    /**
     * Sets the value of the ccu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCU(String value) {
        this.ccu = value;
    }

    /**
     * Gets the value of the leasingPastDueAmount property.
     * 
     */
    public double getLeasingPastDueAmount() {
        return leasingPastDueAmount;
    }

    /**
     * Sets the value of the leasingPastDueAmount property.
     * 
     */
    public void setLeasingPastDueAmount(double value) {
        this.leasingPastDueAmount = value;
    }

    /**
     * Gets the value of the apiStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * Sets the value of the apiStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiStatus(String value) {
        this.apiStatus = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CodedMessageInfo }
     *     
     */
    public CodedMessageInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedMessageInfo }
     *     
     */
    public void setErrorInfo(CodedMessageInfo value) {
        this.errorInfo = value;
    }

}
