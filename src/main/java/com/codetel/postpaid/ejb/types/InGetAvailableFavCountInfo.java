
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InGetAvailableFavCountInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InGetAvailableFavCountInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ejb.postpaid.codetel.com/types/}InGetFavoriteListInfo">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InGetAvailableFavCountInfo")
public class InGetAvailableFavCountInfo
    extends InGetFavoriteListInfo
{


}
