
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InSuspendSubscriberInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InSuspendSubscriberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subscriberNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="activityInfo" type="{http://ejb.postpaid.codetel.com/types/}ActivityInfo"/>
 *         &lt;element name="memoFreeText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="trxSequence" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="throwExceptionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InSuspendSubscriberInfo", propOrder = {
    "subscriberNo",
    "activityInfo",
    "memoFreeText",
    "trxSequence",
    "productType",
    "clientSystem",
    "throwExceptionInd"
})
public class InSuspendSubscriberInfo {

    @XmlElement(required = true, nillable = true)
    protected String subscriberNo;
    @XmlElement(required = true, nillable = true)
    protected ActivityInfo activityInfo;
    @XmlElement(required = true, nillable = true)
    protected String memoFreeText;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long trxSequence;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, nillable = true)
    protected String clientSystem;
    @XmlElement(required = true, nillable = true)
    protected String throwExceptionInd;

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNo(String value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the activityInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityInfo }
     *     
     */
    public ActivityInfo getActivityInfo() {
        return activityInfo;
    }

    /**
     * Sets the value of the activityInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityInfo }
     *     
     */
    public void setActivityInfo(ActivityInfo value) {
        this.activityInfo = value;
    }

    /**
     * Gets the value of the memoFreeText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemoFreeText() {
        return memoFreeText;
    }

    /**
     * Sets the value of the memoFreeText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemoFreeText(String value) {
        this.memoFreeText = value;
    }

    /**
     * Gets the value of the trxSequence property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTrxSequence() {
        return trxSequence;
    }

    /**
     * Sets the value of the trxSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTrxSequence(Long value) {
        this.trxSequence = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the throwExceptionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThrowExceptionInd() {
        return throwExceptionInd;
    }

    /**
     * Sets the value of the throwExceptionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThrowExceptionInd(String value) {
        this.throwExceptionInd = value;
    }

}
