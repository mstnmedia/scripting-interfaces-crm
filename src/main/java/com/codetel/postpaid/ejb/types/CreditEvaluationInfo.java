
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CreditEvaluationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditEvaluationInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="creditEvalRequiredInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="hasCreditCardInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="creditCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="creditClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="creditExpDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="beaconScore" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="creditStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditEvaluationInfo", propOrder = {
    "creditEvalRequiredInd",
    "hasCreditCardInd",
    "creditCategory",
    "creditClass",
    "creditExpDate",
    "beaconScore",
    "creditStatus"
})
public class CreditEvaluationInfo {

    @XmlElement(required = true, nillable = true)
    protected String creditEvalRequiredInd;
    @XmlElement(required = true, nillable = true)
    protected String hasCreditCardInd;
    @XmlElement(required = true, nillable = true)
    protected String creditCategory;
    @XmlElement(required = true, nillable = true)
    protected String creditClass;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creditExpDate;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer beaconScore;
    @XmlElement(required = true, nillable = true)
    protected String creditStatus;

    /**
     * Gets the value of the creditEvalRequiredInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditEvalRequiredInd() {
        return creditEvalRequiredInd;
    }

    /**
     * Sets the value of the creditEvalRequiredInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditEvalRequiredInd(String value) {
        this.creditEvalRequiredInd = value;
    }

    /**
     * Gets the value of the hasCreditCardInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHasCreditCardInd() {
        return hasCreditCardInd;
    }

    /**
     * Sets the value of the hasCreditCardInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHasCreditCardInd(String value) {
        this.hasCreditCardInd = value;
    }

    /**
     * Gets the value of the creditCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCategory() {
        return creditCategory;
    }

    /**
     * Sets the value of the creditCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCategory(String value) {
        this.creditCategory = value;
    }

    /**
     * Gets the value of the creditClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditClass() {
        return creditClass;
    }

    /**
     * Sets the value of the creditClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditClass(String value) {
        this.creditClass = value;
    }

    /**
     * Gets the value of the creditExpDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreditExpDate() {
        return creditExpDate;
    }

    /**
     * Sets the value of the creditExpDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreditExpDate(XMLGregorianCalendar value) {
        this.creditExpDate = value;
    }

    /**
     * Gets the value of the beaconScore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBeaconScore() {
        return beaconScore;
    }

    /**
     * Sets the value of the beaconScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBeaconScore(Integer value) {
        this.beaconScore = value;
    }

    /**
     * Gets the value of the creditStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditStatus() {
        return creditStatus;
    }

    /**
     * Sets the value of the creditStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditStatus(String value) {
        this.creditStatus = value;
    }

}
