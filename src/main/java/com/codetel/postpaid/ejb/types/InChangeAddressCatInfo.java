
package com.codetel.postpaid.ejb.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InChangeAddressCatInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InChangeAddressCatInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="applyToCstInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refTableName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="reqProcDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="chAddrFieldListInfo" type="{http://ejb.postpaid.codetel.com/types/}AddressCatalogFieldInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reqDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="throwExceptionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InChangeAddressCatInfo", propOrder = {
    "applyToCstInd",
    "refTableName",
    "reqProcDate",
    "chAddrFieldListInfo",
    "reqDate",
    "clientSystem",
    "throwExceptionInd"
})
public class InChangeAddressCatInfo {

    @XmlElement(required = true, nillable = true)
    protected String applyToCstInd;
    @XmlElement(required = true, nillable = true)
    protected String refTableName;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reqProcDate;
    @XmlElement(nillable = true)
    protected List<AddressCatalogFieldInfo> chAddrFieldListInfo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reqDate;
    @XmlElement(required = true, nillable = true)
    protected String clientSystem;
    @XmlElement(required = true, nillable = true)
    protected String throwExceptionInd;

    /**
     * Gets the value of the applyToCstInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyToCstInd() {
        return applyToCstInd;
    }

    /**
     * Sets the value of the applyToCstInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyToCstInd(String value) {
        this.applyToCstInd = value;
    }

    /**
     * Gets the value of the refTableName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefTableName() {
        return refTableName;
    }

    /**
     * Sets the value of the refTableName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefTableName(String value) {
        this.refTableName = value;
    }

    /**
     * Gets the value of the reqProcDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReqProcDate() {
        return reqProcDate;
    }

    /**
     * Sets the value of the reqProcDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReqProcDate(XMLGregorianCalendar value) {
        this.reqProcDate = value;
    }

    /**
     * Gets the value of the chAddrFieldListInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chAddrFieldListInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChAddrFieldListInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddressCatalogFieldInfo }
     * 
     * 
     */
    public List<AddressCatalogFieldInfo> getChAddrFieldListInfo() {
        if (chAddrFieldListInfo == null) {
            chAddrFieldListInfo = new ArrayList<AddressCatalogFieldInfo>();
        }
        return this.chAddrFieldListInfo;
    }

    /**
     * Gets the value of the reqDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReqDate() {
        return reqDate;
    }

    /**
     * Sets the value of the reqDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReqDate(XMLGregorianCalendar value) {
        this.reqDate = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the throwExceptionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThrowExceptionInd() {
        return throwExceptionInd;
    }

    /**
     * Sets the value of the throwExceptionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThrowExceptionInd(String value) {
        this.throwExceptionInd = value;
    }

}
