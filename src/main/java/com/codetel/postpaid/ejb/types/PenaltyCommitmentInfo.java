
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PenaltyCommitmentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PenaltyCommitmentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="seqNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="subscriberNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="socSequenceNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="socCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="socDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="socServiceClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="penaltyStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="penaltyStatusReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="commitmentNoMonths" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="remainingNoMonths" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="commitmentStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="commitmentEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="effectivePenaltyAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="specialIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PenaltyCommitmentInfo", propOrder = {
    "seqNo",
    "subscriberNo",
    "productType",
    "socSequenceNo",
    "socCode",
    "socDescription",
    "socServiceClass",
    "penaltyStatusCode",
    "penaltyStatusReasonCode",
    "commitmentNoMonths",
    "remainingNoMonths",
    "commitmentStartDate",
    "commitmentEndDate",
    "effectivePenaltyAmount",
    "specialIndicator"
})
public class PenaltyCommitmentInfo {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer seqNo;
    @XmlElement(required = true, nillable = true)
    protected String subscriberNo;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long socSequenceNo;
    @XmlElement(required = true, nillable = true)
    protected String socCode;
    @XmlElement(required = true, nillable = true)
    protected String socDescription;
    @XmlElement(required = true, nillable = true)
    protected String socServiceClass;
    @XmlElement(required = true, nillable = true)
    protected String penaltyStatusCode;
    @XmlElement(required = true, nillable = true)
    protected String penaltyStatusReasonCode;
    @XmlElement(required = true, type = Short.class, nillable = true)
    protected Short commitmentNoMonths;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long remainingNoMonths;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar commitmentStartDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar commitmentEndDate;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double effectivePenaltyAmount;
    @XmlElement(required = true, nillable = true)
    protected String specialIndicator;

    /**
     * Gets the value of the seqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSeqNo() {
        return seqNo;
    }

    /**
     * Sets the value of the seqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSeqNo(Integer value) {
        this.seqNo = value;
    }

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNo(String value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the socSequenceNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSocSequenceNo() {
        return socSequenceNo;
    }

    /**
     * Sets the value of the socSequenceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSocSequenceNo(Long value) {
        this.socSequenceNo = value;
    }

    /**
     * Gets the value of the socCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSocCode() {
        return socCode;
    }

    /**
     * Sets the value of the socCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSocCode(String value) {
        this.socCode = value;
    }

    /**
     * Gets the value of the socDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSocDescription() {
        return socDescription;
    }

    /**
     * Sets the value of the socDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSocDescription(String value) {
        this.socDescription = value;
    }

    /**
     * Gets the value of the socServiceClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSocServiceClass() {
        return socServiceClass;
    }

    /**
     * Sets the value of the socServiceClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSocServiceClass(String value) {
        this.socServiceClass = value;
    }

    /**
     * Gets the value of the penaltyStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPenaltyStatusCode() {
        return penaltyStatusCode;
    }

    /**
     * Sets the value of the penaltyStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPenaltyStatusCode(String value) {
        this.penaltyStatusCode = value;
    }

    /**
     * Gets the value of the penaltyStatusReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPenaltyStatusReasonCode() {
        return penaltyStatusReasonCode;
    }

    /**
     * Sets the value of the penaltyStatusReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPenaltyStatusReasonCode(String value) {
        this.penaltyStatusReasonCode = value;
    }

    /**
     * Gets the value of the commitmentNoMonths property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getCommitmentNoMonths() {
        return commitmentNoMonths;
    }

    /**
     * Sets the value of the commitmentNoMonths property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setCommitmentNoMonths(Short value) {
        this.commitmentNoMonths = value;
    }

    /**
     * Gets the value of the remainingNoMonths property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRemainingNoMonths() {
        return remainingNoMonths;
    }

    /**
     * Sets the value of the remainingNoMonths property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRemainingNoMonths(Long value) {
        this.remainingNoMonths = value;
    }

    /**
     * Gets the value of the commitmentStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCommitmentStartDate() {
        return commitmentStartDate;
    }

    /**
     * Sets the value of the commitmentStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCommitmentStartDate(XMLGregorianCalendar value) {
        this.commitmentStartDate = value;
    }

    /**
     * Gets the value of the commitmentEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCommitmentEndDate() {
        return commitmentEndDate;
    }

    /**
     * Sets the value of the commitmentEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCommitmentEndDate(XMLGregorianCalendar value) {
        this.commitmentEndDate = value;
    }

    /**
     * Gets the value of the effectivePenaltyAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getEffectivePenaltyAmount() {
        return effectivePenaltyAmount;
    }

    /**
     * Sets the value of the effectivePenaltyAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setEffectivePenaltyAmount(Double value) {
        this.effectivePenaltyAmount = value;
    }

    /**
     * Gets the value of the specialIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialIndicator() {
        return specialIndicator;
    }

    /**
     * Sets the value of the specialIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialIndicator(String value) {
        this.specialIndicator = value;
    }

}
