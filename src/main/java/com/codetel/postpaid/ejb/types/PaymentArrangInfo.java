
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PaymentArrangInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentArrangInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paymentArrangAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="expectedPaymentDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentArrangInfo", propOrder = {
    "paymentArrangAmount",
    "expectedPaymentDate"
})
public class PaymentArrangInfo {

    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double paymentArrangAmount;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expectedPaymentDate;

    /**
     * Gets the value of the paymentArrangAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPaymentArrangAmount() {
        return paymentArrangAmount;
    }

    /**
     * Sets the value of the paymentArrangAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPaymentArrangAmount(Double value) {
        this.paymentArrangAmount = value;
    }

    /**
     * Gets the value of the expectedPaymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpectedPaymentDate() {
        return expectedPaymentDate;
    }

    /**
     * Sets the value of the expectedPaymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpectedPaymentDate(XMLGregorianCalendar value) {
        this.expectedPaymentDate = value;
    }

}
