
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BanSubInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BanSubInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ban" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriberNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="banStatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriberStatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriberStatusDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="subscriberStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="banStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BanSubInfo", propOrder = {
    "ban",
    "subscriberNo",
    "customerId",
    "banStatusDescription",
    "productTypeDescription",
    "subscriberStatusDescription",
    "subscriberStatusDate",
    "subscriberStatus",
    "productType",
    "banStatus"
})
public class BanSubInfo {

    @XmlElement(required = true, nillable = true)
    protected String ban;
    @XmlElement(required = true, nillable = true)
    protected String subscriberNo;
    @XmlElement(required = true, nillable = true)
    protected String customerId;
    @XmlElement(required = true, nillable = true)
    protected String banStatusDescription;
    @XmlElement(required = true, nillable = true)
    protected String productTypeDescription;
    @XmlElement(required = true, nillable = true)
    protected String subscriberStatusDescription;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar subscriberStatusDate;
    @XmlElement(required = true, nillable = true)
    protected String subscriberStatus;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, nillable = true)
    protected String banStatus;

    /**
     * Gets the value of the ban property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBan() {
        return ban;
    }

    /**
     * Sets the value of the ban property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBan(String value) {
        this.ban = value;
    }

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNo(String value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the banStatusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanStatusDescription() {
        return banStatusDescription;
    }

    /**
     * Sets the value of the banStatusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanStatusDescription(String value) {
        this.banStatusDescription = value;
    }

    /**
     * Gets the value of the productTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductTypeDescription() {
        return productTypeDescription;
    }

    /**
     * Sets the value of the productTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductTypeDescription(String value) {
        this.productTypeDescription = value;
    }

    /**
     * Gets the value of the subscriberStatusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberStatusDescription() {
        return subscriberStatusDescription;
    }

    /**
     * Sets the value of the subscriberStatusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberStatusDescription(String value) {
        this.subscriberStatusDescription = value;
    }

    /**
     * Gets the value of the subscriberStatusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSubscriberStatusDate() {
        return subscriberStatusDate;
    }

    /**
     * Sets the value of the subscriberStatusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSubscriberStatusDate(XMLGregorianCalendar value) {
        this.subscriberStatusDate = value;
    }

    /**
     * Gets the value of the subscriberStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberStatus() {
        return subscriberStatus;
    }

    /**
     * Sets the value of the subscriberStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberStatus(String value) {
        this.subscriberStatus = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the banStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanStatus() {
        return banStatus;
    }

    /**
     * Sets the value of the banStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanStatus(String value) {
        this.banStatus = value;
    }

}
