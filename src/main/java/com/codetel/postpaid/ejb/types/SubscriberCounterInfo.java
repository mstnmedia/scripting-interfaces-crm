
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubscriberCounterInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriberCounterInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subscriberSuspended" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="subscriberCancelled" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="productTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriberOpen" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="subscriberReserved" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriberCounterInfo", propOrder = {
    "subscriberSuspended",
    "subscriberCancelled",
    "productTypeDescription",
    "subscriberOpen",
    "subscriberReserved",
    "productType"
})
public class SubscriberCounterInfo {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer subscriberSuspended;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer subscriberCancelled;
    @XmlElement(required = true, nillable = true)
    protected String productTypeDescription;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer subscriberOpen;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer subscriberReserved;
    @XmlElement(required = true, nillable = true)
    protected String productType;

    /**
     * Gets the value of the subscriberSuspended property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubscriberSuspended() {
        return subscriberSuspended;
    }

    /**
     * Sets the value of the subscriberSuspended property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubscriberSuspended(Integer value) {
        this.subscriberSuspended = value;
    }

    /**
     * Gets the value of the subscriberCancelled property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubscriberCancelled() {
        return subscriberCancelled;
    }

    /**
     * Sets the value of the subscriberCancelled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubscriberCancelled(Integer value) {
        this.subscriberCancelled = value;
    }

    /**
     * Gets the value of the productTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductTypeDescription() {
        return productTypeDescription;
    }

    /**
     * Sets the value of the productTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductTypeDescription(String value) {
        this.productTypeDescription = value;
    }

    /**
     * Gets the value of the subscriberOpen property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubscriberOpen() {
        return subscriberOpen;
    }

    /**
     * Sets the value of the subscriberOpen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubscriberOpen(Integer value) {
        this.subscriberOpen = value;
    }

    /**
     * Gets the value of the subscriberReserved property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubscriberReserved() {
        return subscriberReserved;
    }

    /**
     * Sets the value of the subscriberReserved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubscriberReserved(Integer value) {
        this.subscriberReserved = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

}
