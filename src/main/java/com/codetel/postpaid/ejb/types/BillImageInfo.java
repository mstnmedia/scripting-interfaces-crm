
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillImageInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillImageInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="totalSections" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="totalOptionalPages" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="totalCnt" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="billFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalPages" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillImageInfo", propOrder = {
    "totalSections",
    "totalOptionalPages",
    "totalCnt",
    "billFileName",
    "totalPages"
})
public class BillImageInfo {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer totalSections;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer totalOptionalPages;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer totalCnt;
    @XmlElement(required = true, nillable = true)
    protected String billFileName;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer totalPages;

    /**
     * Gets the value of the totalSections property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalSections() {
        return totalSections;
    }

    /**
     * Sets the value of the totalSections property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalSections(Integer value) {
        this.totalSections = value;
    }

    /**
     * Gets the value of the totalOptionalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalOptionalPages() {
        return totalOptionalPages;
    }

    /**
     * Sets the value of the totalOptionalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalOptionalPages(Integer value) {
        this.totalOptionalPages = value;
    }

    /**
     * Gets the value of the totalCnt property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalCnt() {
        return totalCnt;
    }

    /**
     * Sets the value of the totalCnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalCnt(Integer value) {
        this.totalCnt = value;
    }

    /**
     * Gets the value of the billFileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillFileName() {
        return billFileName;
    }

    /**
     * Sets the value of the billFileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillFileName(String value) {
        this.billFileName = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

}
