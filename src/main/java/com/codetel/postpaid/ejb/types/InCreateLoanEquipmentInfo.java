
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InCreateLoanEquipmentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InCreateLoanEquipmentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subscriberNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="STROrderID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IMEI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="STRUserID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="rmsLocationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="throwExceptionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InCreateLoanEquipmentInfo", propOrder = {
    "subscriberNo",
    "strOrderID",
    "imei",
    "strUserID",
    "rmsLocationID",
    "clientSystem",
    "throwExceptionInd"
})
public class InCreateLoanEquipmentInfo {

    @XmlElement(required = true, nillable = true)
    protected String subscriberNo;
    @XmlElement(name = "STROrderID", required = true, nillable = true)
    protected String strOrderID;
    @XmlElement(name = "IMEI", required = true, nillable = true)
    protected String imei;
    @XmlElement(name = "STRUserID", required = true, type = Integer.class, nillable = true)
    protected Integer strUserID;
    @XmlElement(required = true, nillable = true)
    protected String rmsLocationID;
    @XmlElement(required = true, nillable = true)
    protected String clientSystem;
    @XmlElement(required = true, nillable = true)
    protected String throwExceptionInd;

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNo(String value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the strOrderID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTROrderID() {
        return strOrderID;
    }

    /**
     * Sets the value of the strOrderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTROrderID(String value) {
        this.strOrderID = value;
    }

    /**
     * Gets the value of the imei property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMEI() {
        return imei;
    }

    /**
     * Sets the value of the imei property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMEI(String value) {
        this.imei = value;
    }

    /**
     * Gets the value of the strUserID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSTRUserID() {
        return strUserID;
    }

    /**
     * Sets the value of the strUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSTRUserID(Integer value) {
        this.strUserID = value;
    }

    /**
     * Gets the value of the rmsLocationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRmsLocationID() {
        return rmsLocationID;
    }

    /**
     * Sets the value of the rmsLocationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRmsLocationID(String value) {
        this.rmsLocationID = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the throwExceptionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThrowExceptionInd() {
        return throwExceptionInd;
    }

    /**
     * Sets the value of the throwExceptionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThrowExceptionInd(String value) {
        this.throwExceptionInd = value;
    }

}
