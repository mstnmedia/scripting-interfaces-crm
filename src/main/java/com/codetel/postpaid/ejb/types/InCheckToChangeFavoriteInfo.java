
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InCheckToChangeFavoriteInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InCheckToChangeFavoriteInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="oldFavoriteNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriberNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oldFeatureSeqNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="effectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="newFavoriteNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="banId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="newFeatureSeqNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="throwExceptionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InCheckToChangeFavoriteInfo", propOrder = {
    "oldFavoriteNo",
    "subscriberNo",
    "oldFeatureSeqNo",
    "effectiveDate",
    "newFavoriteNo",
    "banId",
    "newFeatureSeqNo",
    "productType",
    "clientSystem",
    "throwExceptionInd"
})
public class InCheckToChangeFavoriteInfo {

    @XmlElement(required = true, nillable = true)
    protected String oldFavoriteNo;
    @XmlElement(required = true, nillable = true)
    protected String subscriberNo;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long oldFeatureSeqNo;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(required = true, nillable = true)
    protected String newFavoriteNo;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long banId;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long newFeatureSeqNo;
    @XmlElement(required = true, nillable = true)
    protected String productType;
    @XmlElement(required = true, nillable = true)
    protected String clientSystem;
    @XmlElement(required = true, nillable = true)
    protected String throwExceptionInd;

    /**
     * Gets the value of the oldFavoriteNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldFavoriteNo() {
        return oldFavoriteNo;
    }

    /**
     * Sets the value of the oldFavoriteNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldFavoriteNo(String value) {
        this.oldFavoriteNo = value;
    }

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNo(String value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the oldFeatureSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOldFeatureSeqNo() {
        return oldFeatureSeqNo;
    }

    /**
     * Sets the value of the oldFeatureSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOldFeatureSeqNo(Long value) {
        this.oldFeatureSeqNo = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the newFavoriteNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewFavoriteNo() {
        return newFavoriteNo;
    }

    /**
     * Sets the value of the newFavoriteNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewFavoriteNo(String value) {
        this.newFavoriteNo = value;
    }

    /**
     * Gets the value of the banId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBanId() {
        return banId;
    }

    /**
     * Sets the value of the banId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBanId(Long value) {
        this.banId = value;
    }

    /**
     * Gets the value of the newFeatureSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNewFeatureSeqNo() {
        return newFeatureSeqNo;
    }

    /**
     * Sets the value of the newFeatureSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNewFeatureSeqNo(Long value) {
        this.newFeatureSeqNo = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the throwExceptionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThrowExceptionInd() {
        return throwExceptionInd;
    }

    /**
     * Sets the value of the throwExceptionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThrowExceptionInd(String value) {
        this.throwExceptionInd = value;
    }

}
