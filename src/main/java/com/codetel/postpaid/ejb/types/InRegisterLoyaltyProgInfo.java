
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InRegisterLoyaltyProgInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InRegisterLoyaltyProgInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="loyaltyEnrollInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="banId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="throwExceptionInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InRegisterLoyaltyProgInfo", propOrder = {
    "loyaltyEnrollInd",
    "banId",
    "clientSystem",
    "throwExceptionInd"
})
public class InRegisterLoyaltyProgInfo {

    @XmlElement(required = true, nillable = true)
    protected String loyaltyEnrollInd;
    protected long banId;
    @XmlElement(required = true, nillable = true)
    protected String clientSystem;
    @XmlElement(required = true, nillable = true)
    protected String throwExceptionInd;

    /**
     * Gets the value of the loyaltyEnrollInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyEnrollInd() {
        return loyaltyEnrollInd;
    }

    /**
     * Sets the value of the loyaltyEnrollInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyEnrollInd(String value) {
        this.loyaltyEnrollInd = value;
    }

    /**
     * Gets the value of the banId property.
     * 
     */
    public long getBanId() {
        return banId;
    }

    /**
     * Sets the value of the banId property.
     * 
     */
    public void setBanId(long value) {
        this.banId = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the throwExceptionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThrowExceptionInd() {
        return throwExceptionInd;
    }

    /**
     * Sets the value of the throwExceptionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThrowExceptionInd(String value) {
        this.throwExceptionInd = value;
    }

}
