
package com.codetel.postpaid.ejb.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutGetBanDetailsInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutGetBanDetailsInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hasBanLevelPPInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="banInfo" type="{http://ejb.postpaid.codetel.com/types/}BanInfo"/>
 *         &lt;element name="creditEvaluationInfo" type="{http://ejb.postpaid.codetel.com/types/}CreditEvaluationInfo"/>
 *         &lt;element name="billingNameInfo" type="{http://ejb.postpaid.codetel.com/types/}NameInfo"/>
 *         &lt;element name="customerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriberCounterInfo" type="{http://ejb.postpaid.codetel.com/types/}SubscriberCounterInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="negativeIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="billingAddressInfo" type="{http://ejb.postpaid.codetel.com/types/}AddressInfo"/>
 *         &lt;element name="apiStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorInfo" type="{http://ejb.postpaid.codetel.com/types/}CodedMessageInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutGetBanDetailsInfo", propOrder = {
    "hasBanLevelPPInd",
    "banInfo",
    "creditEvaluationInfo",
    "billingNameInfo",
    "customerId",
    "subscriberCounterInfo",
    "negativeIndicator",
    "billingAddressInfo",
    "apiStatus",
    "errorInfo"
})
public class OutGetBanDetailsInfo {

    @XmlElement(required = true, nillable = true)
    protected String hasBanLevelPPInd;
    @XmlElement(required = true, nillable = true)
    protected BanInfo banInfo;
    @XmlElement(required = true, nillable = true)
    protected CreditEvaluationInfo creditEvaluationInfo;
    @XmlElement(required = true, nillable = true)
    protected NameInfo billingNameInfo;
    @XmlElement(required = true, nillable = true)
    protected String customerId;
    @XmlElement(nillable = true)
    protected List<SubscriberCounterInfo> subscriberCounterInfo;
    @XmlElement(required = true, nillable = true)
    protected String negativeIndicator;
    @XmlElement(required = true, nillable = true)
    protected AddressInfo billingAddressInfo;
    @XmlElement(required = true, nillable = true)
    protected String apiStatus;
    @XmlElement(required = true, nillable = true)
    protected CodedMessageInfo errorInfo;

    /**
     * Gets the value of the hasBanLevelPPInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHasBanLevelPPInd() {
        return hasBanLevelPPInd;
    }

    /**
     * Sets the value of the hasBanLevelPPInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHasBanLevelPPInd(String value) {
        this.hasBanLevelPPInd = value;
    }

    /**
     * Gets the value of the banInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BanInfo }
     *     
     */
    public BanInfo getBanInfo() {
        return banInfo;
    }

    /**
     * Sets the value of the banInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BanInfo }
     *     
     */
    public void setBanInfo(BanInfo value) {
        this.banInfo = value;
    }

    /**
     * Gets the value of the creditEvaluationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CreditEvaluationInfo }
     *     
     */
    public CreditEvaluationInfo getCreditEvaluationInfo() {
        return creditEvaluationInfo;
    }

    /**
     * Sets the value of the creditEvaluationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditEvaluationInfo }
     *     
     */
    public void setCreditEvaluationInfo(CreditEvaluationInfo value) {
        this.creditEvaluationInfo = value;
    }

    /**
     * Gets the value of the billingNameInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NameInfo }
     *     
     */
    public NameInfo getBillingNameInfo() {
        return billingNameInfo;
    }

    /**
     * Sets the value of the billingNameInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameInfo }
     *     
     */
    public void setBillingNameInfo(NameInfo value) {
        this.billingNameInfo = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the subscriberCounterInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subscriberCounterInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubscriberCounterInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubscriberCounterInfo }
     * 
     * 
     */
    public List<SubscriberCounterInfo> getSubscriberCounterInfo() {
        if (subscriberCounterInfo == null) {
            subscriberCounterInfo = new ArrayList<SubscriberCounterInfo>();
        }
        return this.subscriberCounterInfo;
    }

    /**
     * Gets the value of the negativeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNegativeIndicator() {
        return negativeIndicator;
    }

    /**
     * Sets the value of the negativeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNegativeIndicator(String value) {
        this.negativeIndicator = value;
    }

    /**
     * Gets the value of the billingAddressInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AddressInfo }
     *     
     */
    public AddressInfo getBillingAddressInfo() {
        return billingAddressInfo;
    }

    /**
     * Sets the value of the billingAddressInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInfo }
     *     
     */
    public void setBillingAddressInfo(AddressInfo value) {
        this.billingAddressInfo = value;
    }

    /**
     * Gets the value of the apiStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * Sets the value of the apiStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiStatus(String value) {
        this.apiStatus = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CodedMessageInfo }
     *     
     */
    public CodedMessageInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedMessageInfo }
     *     
     */
    public void setErrorInfo(CodedMessageInfo value) {
        this.errorInfo = value;
    }

}
