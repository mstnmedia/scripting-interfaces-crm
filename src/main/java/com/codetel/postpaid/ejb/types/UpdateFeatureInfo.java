
package com.codetel.postpaid.ejb.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateFeatureInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateFeatureInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="featureCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="updateSwitchParamsList" type="{http://ejb.postpaid.codetel.com/types/}CodeValueInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateFeatureInfo", propOrder = {
    "featureCode",
    "updateSwitchParamsList"
})
public class UpdateFeatureInfo {

    @XmlElement(required = true, nillable = true)
    protected String featureCode;
    @XmlElement(nillable = true)
    protected List<CodeValueInfo> updateSwitchParamsList;

    /**
     * Gets the value of the featureCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureCode() {
        return featureCode;
    }

    /**
     * Sets the value of the featureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureCode(String value) {
        this.featureCode = value;
    }

    /**
     * Gets the value of the updateSwitchParamsList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateSwitchParamsList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateSwitchParamsList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodeValueInfo }
     * 
     * 
     */
    public List<CodeValueInfo> getUpdateSwitchParamsList() {
        if (updateSwitchParamsList == null) {
            updateSwitchParamsList = new ArrayList<CodeValueInfo>();
        }
        return this.updateSwitchParamsList;
    }

}
