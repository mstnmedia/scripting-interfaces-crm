
package com.codetel.postpaid.ejb.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FeaturePtpInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FeaturePtpInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="availableNumberCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="socCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="featureCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sequenceNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeaturePtpInfo", propOrder = {
    "availableNumberCount",
    "socCode",
    "featureCode",
    "sequenceNo"
})
public class FeaturePtpInfo {

    protected int availableNumberCount;
    @XmlElement(required = true, nillable = true)
    protected String socCode;
    @XmlElement(required = true, nillable = true)
    protected String featureCode;
    protected int sequenceNo;

    /**
     * Gets the value of the availableNumberCount property.
     * 
     */
    public int getAvailableNumberCount() {
        return availableNumberCount;
    }

    /**
     * Sets the value of the availableNumberCount property.
     * 
     */
    public void setAvailableNumberCount(int value) {
        this.availableNumberCount = value;
    }

    /**
     * Gets the value of the socCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSocCode() {
        return socCode;
    }

    /**
     * Sets the value of the socCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSocCode(String value) {
        this.socCode = value;
    }

    /**
     * Gets the value of the featureCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureCode() {
        return featureCode;
    }

    /**
     * Sets the value of the featureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureCode(String value) {
        this.featureCode = value;
    }

    /**
     * Gets the value of the sequenceNo property.
     * 
     */
    public int getSequenceNo() {
        return sequenceNo;
    }

    /**
     * Sets the value of the sequenceNo property.
     * 
     */
    public void setSequenceNo(int value) {
        this.sequenceNo = value;
    }

}
