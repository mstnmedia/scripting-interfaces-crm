
package com.codetel.postpaid.ejb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.codetel.postpaid.ejb.types.InAddFavoriteInfo;
import com.codetel.postpaid.ejb.types.InAssignResourceNumberInfo;
import com.codetel.postpaid.ejb.types.InCancelSubscriberInfo;
import com.codetel.postpaid.ejb.types.InChangeAddressCatInfo;
import com.codetel.postpaid.ejb.types.InChangeFavoriteInfo;
import com.codetel.postpaid.ejb.types.InCheckStreetInfo;
import com.codetel.postpaid.ejb.types.InCheckToAddFavoriteInfo;
import com.codetel.postpaid.ejb.types.InCheckToChangeFavoriteInfo;
import com.codetel.postpaid.ejb.types.InCreateAdjReversalInfo;
import com.codetel.postpaid.ejb.types.InCreateAdjustmentByBanInfo;
import com.codetel.postpaid.ejb.types.InCreateAdjustmentByCrgInfo;
import com.codetel.postpaid.ejb.types.InCreateBillImageInfo;
import com.codetel.postpaid.ejb.types.InCreateChargeInfo;
import com.codetel.postpaid.ejb.types.InCreateLoanEquipmentInfo;
import com.codetel.postpaid.ejb.types.InCreateMemoInfo;
import com.codetel.postpaid.ejb.types.InCreatePaymentArrangInfo;
import com.codetel.postpaid.ejb.types.InCreateWlsSubInfo;
import com.codetel.postpaid.ejb.types.InCreateWlsSubSubLvlInfo;
import com.codetel.postpaid.ejb.types.InGetAvailableFavCountInfo;
import com.codetel.postpaid.ejb.types.InGetBanDetailsInfo;
import com.codetel.postpaid.ejb.types.InGetBanInfo;
import com.codetel.postpaid.ejb.types.InGetBillingHistByBanInfo;
import com.codetel.postpaid.ejb.types.InGetCreditLimitInfo;
import com.codetel.postpaid.ejb.types.InGetCustListBySubInfo;
import com.codetel.postpaid.ejb.types.InGetFavoriteListInfo;
import com.codetel.postpaid.ejb.types.InGetPaymentArrangByBanInfo;
import com.codetel.postpaid.ejb.types.InGetPaymentArrangQuoteInfo;
import com.codetel.postpaid.ejb.types.InGetPenaltyCalcInfo;
import com.codetel.postpaid.ejb.types.InGetPenaltyCommitmentsInfo;
import com.codetel.postpaid.ejb.types.InGetSubscriberInfo;
import com.codetel.postpaid.ejb.types.InRegisterLoyaltyProgInfo;
import com.codetel.postpaid.ejb.types.InReleaseResourceNumberInfo;
import com.codetel.postpaid.ejb.types.InRestoreSubscriberInfo;
import com.codetel.postpaid.ejb.types.InSuspendSubscriberInfo;
import com.codetel.postpaid.ejb.types.InUpdateSocBanLevelInfo;
import com.codetel.postpaid.ejb.types.InUpdateSubscriberFtrSwPrm;
import com.codetel.postpaid.ejb.types.InUpdateSubscriberPlanInfo;
import com.codetel.postpaid.ejb.types.InUpdateSubscriberSIMInfo;
import com.codetel.postpaid.ejb.types.InUpdateWirelineLeaseInfo;
import com.codetel.postpaid.ejb.types.OutAddFavoriteInfo;
import com.codetel.postpaid.ejb.types.OutAssignResourceNumberInfo;
import com.codetel.postpaid.ejb.types.OutCancelSubscriberInfo;
import com.codetel.postpaid.ejb.types.OutChangeAddressCatInfo;
import com.codetel.postpaid.ejb.types.OutChangeFavoriteInfo;
import com.codetel.postpaid.ejb.types.OutCheckStreetInfo;
import com.codetel.postpaid.ejb.types.OutCreateAdjReversalInfo;
import com.codetel.postpaid.ejb.types.OutCreateAdjustmentByBanInfo;
import com.codetel.postpaid.ejb.types.OutCreateAdjustmentByCrgInfo;
import com.codetel.postpaid.ejb.types.OutCreateBillImageInfo;
import com.codetel.postpaid.ejb.types.OutCreateChargeInfo;
import com.codetel.postpaid.ejb.types.OutCreateLoanEquipmentInfo;
import com.codetel.postpaid.ejb.types.OutCreateMemoInfo;
import com.codetel.postpaid.ejb.types.OutCreatePaymentArrangInfo;
import com.codetel.postpaid.ejb.types.OutCreateWlsSubInfo;
import com.codetel.postpaid.ejb.types.OutCreateWlsSubSubLvlInfo;
import com.codetel.postpaid.ejb.types.OutGetAvailableFavCountInfo;
import com.codetel.postpaid.ejb.types.OutGetBanDetailsInfo;
import com.codetel.postpaid.ejb.types.OutGetBanInfo;
import com.codetel.postpaid.ejb.types.OutGetBillingHistByBanInfo;
import com.codetel.postpaid.ejb.types.OutGetCreditLimitInfo;
import com.codetel.postpaid.ejb.types.OutGetCustListBySubInfo;
import com.codetel.postpaid.ejb.types.OutGetFavoriteListInfo;
import com.codetel.postpaid.ejb.types.OutGetPaymentArrangByBanInfo;
import com.codetel.postpaid.ejb.types.OutGetPaymentArrangQuoteInfo;
import com.codetel.postpaid.ejb.types.OutGetPenaltyCalcInfo;
import com.codetel.postpaid.ejb.types.OutGetPenaltyCommitmentsInfo;
import com.codetel.postpaid.ejb.types.OutGetSubscriberInfo;
import com.codetel.postpaid.ejb.types.OutRegisterLoyaltyProgInfo;
import com.codetel.postpaid.ejb.types.OutReleaseResourceNumberInfo;
import com.codetel.postpaid.ejb.types.OutRestoreSubscriberInfo;
import com.codetel.postpaid.ejb.types.OutSuspendSubscriberInfo;
import com.codetel.postpaid.ejb.types.OutUpdateSocBanLevelInfo;
import com.codetel.postpaid.ejb.types.OutUpdateSubscriberFtrSwPrm;
import com.codetel.postpaid.ejb.types.OutUpdateSubscriberPlanInfo;
import com.codetel.postpaid.ejb.types.OutUpdateSubscriberSIMInfo;
import com.codetel.postpaid.ejb.types.OutUpdateWirelineLeaseInfo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.codetel.postpaid.ejb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateBillImageElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createBillImageElement");
    private final static QName _CreateLoanEquipmentElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createLoanEquipmentElement");
    private final static QName _SuspendSubscriberElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "suspendSubscriberElement");
    private final static QName _GetPaymentArrangQuoteElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getPaymentArrangQuoteElement");
    private final static QName _CancelSubscriberElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "cancelSubscriberElement");
    private final static QName _CreateAdjustmentByCrgResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createAdjustmentByCrgResponseElement");
    private final static QName _GetCustListBySubResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getCustListBySubResponseElement");
    private final static QName _CheckStreetOnBanByIdResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "checkStreetOnBanByIdResponseElement");
    private final static QName _AssignResourceNumberResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "assignResourceNumberResponseElement");
    private final static QName _GetSubscriberInfoResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getSubscriberInfoResponseElement");
    private final static QName _CheckToChangeFavoriteResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "checkToChangeFavoriteResponseElement");
    private final static QName _ChangeAddressCatalogElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "changeAddressCatalogElement");
    private final static QName _CreateChargeElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createChargeElement");
    private final static QName _GetFavoriteListResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getFavoriteListResponseElement");
    private final static QName _CreatePaymentArrangElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createPaymentArrangElement");
    private final static QName _GetBanDetailsElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getBanDetailsElement");
    private final static QName _GetCreditLimitElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getCreditLimitElement");
    private final static QName _GetPenaltyCommitmentsElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getPenaltyCommitmentsElement");
    private final static QName _UpdateSubscriberFtrSwPrmElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateSubscriberFtrSwPrmElement");
    private final static QName _GetFavoriteListElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getFavoriteListElement");
    private final static QName _CreateWlsSubSubLvlResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createWlsSubSubLvlResponseElement");
    private final static QName _UpdateSubscriberPlanResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateSubscriberPlanResponseElement");
    private final static QName _AddFavoriteResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "addFavoriteResponseElement");
    private final static QName _GetBanDetailsResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getBanDetailsResponseElement");
    private final static QName _CreateAdjReversalResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createAdjReversalResponseElement");
    private final static QName _CheckToChangeFavoriteElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "checkToChangeFavoriteElement");
    private final static QName _SuspendSubscriberResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "suspendSubscriberResponseElement");
    private final static QName _UpdateSocBanLevelElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateSocBanLevelElement");
    private final static QName _CheckStreetOnBanByIdElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "checkStreetOnBanByIdElement");
    private final static QName _CreateChargeResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createChargeResponseElement");
    private final static QName _CreateWlsSubElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createWlsSubElement");
    private final static QName _GetBillingHistByBanElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getBillingHistByBanElement");
    private final static QName _ShowServiceListElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "showServiceListElement");
    private final static QName _UpdateSubscriberPlanElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateSubscriberPlanElement");
    private final static QName _ReleaseResourceNumberResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "releaseResourceNumberResponseElement");
    private final static QName _UpdateWireLineLeaseResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateWireLineLeaseResponseElement");
    private final static QName _CancelSubscriberResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "cancelSubscriberResponseElement");
    private final static QName _UpdateSocBanLevelResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateSocBanLevelResponseElement");
    private final static QName _AssignResourceNumberElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "assignResourceNumberElement");
    private final static QName _ChangeAddressCatalogResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "changeAddressCatalogResponseElement");
    private final static QName _CreateAdjustmentByBanElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createAdjustmentByBanElement");
    private final static QName _GetPenaltyCommitmentsResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getPenaltyCommitmentsResponseElement");
    private final static QName _CreateWlsSubResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createWlsSubResponseElement");
    private final static QName _CreateAdjustmentByBanResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createAdjustmentByBanResponseElement");
    private final static QName _CreateBillImageResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createBillImageResponseElement");
    private final static QName _GetPaymentArrangByBanResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getPaymentArrangByBanResponseElement");
    private final static QName _UpdateSubscriberSIMResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateSubscriberSIMResponseElement");
    private final static QName _GetSubscriberInfoElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getSubscriberInfoElement");
    private final static QName _GetAvailableFavoriteCountElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getAvailableFavoriteCountElement");
    private final static QName _RegisterLoyaltyProgramResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "registerLoyaltyProgramResponseElement");
    private final static QName _GetBanInfoElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getBanInfoElement");
    private final static QName _UpdateWireLineLeaseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateWireLineLeaseElement");
    private final static QName _CreateWlsSubSubLvlElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createWlsSubSubLvlElement");
    private final static QName _ChangeFavoriteElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "changeFavoriteElement");
    private final static QName _GetAvailableFavoriteCountResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getAvailableFavoriteCountResponseElement");
    private final static QName _CreateAdjustmentByCrgElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createAdjustmentByCrgElement");
    private final static QName _CreateMemoElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createMemoElement");
    private final static QName _CreateAdjReversalElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createAdjReversalElement");
    private final static QName _GetBanInfoResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getBanInfoResponseElement");
    private final static QName _GetCustListBySubElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getCustListBySubElement");
    private final static QName _CheckToAddFavoriteElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "checkToAddFavoriteElement");
    private final static QName _AddFavoriteElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "addFavoriteElement");
    private final static QName _RegisterLoyaltyProgramElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "registerLoyaltyProgramElement");
    private final static QName _GetPenaltyCalcResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getPenaltyCalcResponseElement");
    private final static QName _GetBillingHistByBanResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getBillingHistByBanResponseElement");
    private final static QName _RestoreSubscriberElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "restoreSubscriberElement");
    private final static QName _CreatePaymentArrangResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createPaymentArrangResponseElement");
    private final static QName _CreateLoanEquipmentResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createLoanEquipmentResponseElement");
    private final static QName _CreateMemoResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "createMemoResponseElement");
    private final static QName _GetPaymentArrangByBanElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getPaymentArrangByBanElement");
    private final static QName _ChangeFavoriteResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "changeFavoriteResponseElement");
    private final static QName _RestoreSubscriberResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "restoreSubscriberResponseElement");
    private final static QName _CheckToAddFavoriteResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "checkToAddFavoriteResponseElement");
    private final static QName _UpdateSubscriberSIMElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateSubscriberSIMElement");
    private final static QName _GetCreditLimitResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getCreditLimitResponseElement");
    private final static QName _UpdateSubscriberFtrSwPrmResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "updateSubscriberFtrSwPrmResponseElement");
    private final static QName _GetPaymentArrangQuoteResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getPaymentArrangQuoteResponseElement");
    private final static QName _GetPenaltyCalcElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "getPenaltyCalcElement");
    private final static QName _ReleaseResourceNumberElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "releaseResourceNumberElement");
    private final static QName _ShowServiceListResponseElement_QNAME = new QName("http://ejb.postpaid.codetel.com/", "showServiceListResponseElement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.codetel.postpaid.ejb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreateBillImageInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createBillImageElement")
    public JAXBElement<InCreateBillImageInfo> createCreateBillImageElement(InCreateBillImageInfo value) {
        return new JAXBElement<InCreateBillImageInfo>(_CreateBillImageElement_QNAME, InCreateBillImageInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreateLoanEquipmentInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createLoanEquipmentElement")
    public JAXBElement<InCreateLoanEquipmentInfo> createCreateLoanEquipmentElement(InCreateLoanEquipmentInfo value) {
        return new JAXBElement<InCreateLoanEquipmentInfo>(_CreateLoanEquipmentElement_QNAME, InCreateLoanEquipmentInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InSuspendSubscriberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "suspendSubscriberElement")
    public JAXBElement<InSuspendSubscriberInfo> createSuspendSubscriberElement(InSuspendSubscriberInfo value) {
        return new JAXBElement<InSuspendSubscriberInfo>(_SuspendSubscriberElement_QNAME, InSuspendSubscriberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetPaymentArrangQuoteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getPaymentArrangQuoteElement")
    public JAXBElement<InGetPaymentArrangQuoteInfo> createGetPaymentArrangQuoteElement(InGetPaymentArrangQuoteInfo value) {
        return new JAXBElement<InGetPaymentArrangQuoteInfo>(_GetPaymentArrangQuoteElement_QNAME, InGetPaymentArrangQuoteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCancelSubscriberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "cancelSubscriberElement")
    public JAXBElement<InCancelSubscriberInfo> createCancelSubscriberElement(InCancelSubscriberInfo value) {
        return new JAXBElement<InCancelSubscriberInfo>(_CancelSubscriberElement_QNAME, InCancelSubscriberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreateAdjustmentByCrgInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createAdjustmentByCrgResponseElement")
    public JAXBElement<OutCreateAdjustmentByCrgInfo> createCreateAdjustmentByCrgResponseElement(OutCreateAdjustmentByCrgInfo value) {
        return new JAXBElement<OutCreateAdjustmentByCrgInfo>(_CreateAdjustmentByCrgResponseElement_QNAME, OutCreateAdjustmentByCrgInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetCustListBySubInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getCustListBySubResponseElement")
    public JAXBElement<OutGetCustListBySubInfo> createGetCustListBySubResponseElement(OutGetCustListBySubInfo value) {
        return new JAXBElement<OutGetCustListBySubInfo>(_GetCustListBySubResponseElement_QNAME, OutGetCustListBySubInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCheckStreetInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "checkStreetOnBanByIdResponseElement")
    public JAXBElement<OutCheckStreetInfo> createCheckStreetOnBanByIdResponseElement(OutCheckStreetInfo value) {
        return new JAXBElement<OutCheckStreetInfo>(_CheckStreetOnBanByIdResponseElement_QNAME, OutCheckStreetInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutAssignResourceNumberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "assignResourceNumberResponseElement")
    public JAXBElement<OutAssignResourceNumberInfo> createAssignResourceNumberResponseElement(OutAssignResourceNumberInfo value) {
        return new JAXBElement<OutAssignResourceNumberInfo>(_AssignResourceNumberResponseElement_QNAME, OutAssignResourceNumberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetSubscriberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getSubscriberInfoResponseElement")
    public JAXBElement<OutGetSubscriberInfo> createGetSubscriberInfoResponseElement(OutGetSubscriberInfo value) {
        return new JAXBElement<OutGetSubscriberInfo>(_GetSubscriberInfoResponseElement_QNAME, OutGetSubscriberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutChangeFavoriteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "checkToChangeFavoriteResponseElement")
    public JAXBElement<OutChangeFavoriteInfo> createCheckToChangeFavoriteResponseElement(OutChangeFavoriteInfo value) {
        return new JAXBElement<OutChangeFavoriteInfo>(_CheckToChangeFavoriteResponseElement_QNAME, OutChangeFavoriteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InChangeAddressCatInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "changeAddressCatalogElement")
    public JAXBElement<InChangeAddressCatInfo> createChangeAddressCatalogElement(InChangeAddressCatInfo value) {
        return new JAXBElement<InChangeAddressCatInfo>(_ChangeAddressCatalogElement_QNAME, InChangeAddressCatInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreateChargeInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createChargeElement")
    public JAXBElement<InCreateChargeInfo> createCreateChargeElement(InCreateChargeInfo value) {
        return new JAXBElement<InCreateChargeInfo>(_CreateChargeElement_QNAME, InCreateChargeInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetFavoriteListInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getFavoriteListResponseElement")
    public JAXBElement<OutGetFavoriteListInfo> createGetFavoriteListResponseElement(OutGetFavoriteListInfo value) {
        return new JAXBElement<OutGetFavoriteListInfo>(_GetFavoriteListResponseElement_QNAME, OutGetFavoriteListInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreatePaymentArrangInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createPaymentArrangElement")
    public JAXBElement<InCreatePaymentArrangInfo> createCreatePaymentArrangElement(InCreatePaymentArrangInfo value) {
        return new JAXBElement<InCreatePaymentArrangInfo>(_CreatePaymentArrangElement_QNAME, InCreatePaymentArrangInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetBanDetailsInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getBanDetailsElement")
    public JAXBElement<InGetBanDetailsInfo> createGetBanDetailsElement(InGetBanDetailsInfo value) {
        return new JAXBElement<InGetBanDetailsInfo>(_GetBanDetailsElement_QNAME, InGetBanDetailsInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetCreditLimitInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getCreditLimitElement")
    public JAXBElement<InGetCreditLimitInfo> createGetCreditLimitElement(InGetCreditLimitInfo value) {
        return new JAXBElement<InGetCreditLimitInfo>(_GetCreditLimitElement_QNAME, InGetCreditLimitInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetPenaltyCommitmentsInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getPenaltyCommitmentsElement")
    public JAXBElement<InGetPenaltyCommitmentsInfo> createGetPenaltyCommitmentsElement(InGetPenaltyCommitmentsInfo value) {
        return new JAXBElement<InGetPenaltyCommitmentsInfo>(_GetPenaltyCommitmentsElement_QNAME, InGetPenaltyCommitmentsInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InUpdateSubscriberFtrSwPrm }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateSubscriberFtrSwPrmElement")
    public JAXBElement<InUpdateSubscriberFtrSwPrm> createUpdateSubscriberFtrSwPrmElement(InUpdateSubscriberFtrSwPrm value) {
        return new JAXBElement<InUpdateSubscriberFtrSwPrm>(_UpdateSubscriberFtrSwPrmElement_QNAME, InUpdateSubscriberFtrSwPrm.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetFavoriteListInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getFavoriteListElement")
    public JAXBElement<InGetFavoriteListInfo> createGetFavoriteListElement(InGetFavoriteListInfo value) {
        return new JAXBElement<InGetFavoriteListInfo>(_GetFavoriteListElement_QNAME, InGetFavoriteListInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreateWlsSubSubLvlInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createWlsSubSubLvlResponseElement")
    public JAXBElement<OutCreateWlsSubSubLvlInfo> createCreateWlsSubSubLvlResponseElement(OutCreateWlsSubSubLvlInfo value) {
        return new JAXBElement<OutCreateWlsSubSubLvlInfo>(_CreateWlsSubSubLvlResponseElement_QNAME, OutCreateWlsSubSubLvlInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutUpdateSubscriberPlanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateSubscriberPlanResponseElement")
    public JAXBElement<OutUpdateSubscriberPlanInfo> createUpdateSubscriberPlanResponseElement(OutUpdateSubscriberPlanInfo value) {
        return new JAXBElement<OutUpdateSubscriberPlanInfo>(_UpdateSubscriberPlanResponseElement_QNAME, OutUpdateSubscriberPlanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutAddFavoriteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "addFavoriteResponseElement")
    public JAXBElement<OutAddFavoriteInfo> createAddFavoriteResponseElement(OutAddFavoriteInfo value) {
        return new JAXBElement<OutAddFavoriteInfo>(_AddFavoriteResponseElement_QNAME, OutAddFavoriteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetBanDetailsInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getBanDetailsResponseElement")
    public JAXBElement<OutGetBanDetailsInfo> createGetBanDetailsResponseElement(OutGetBanDetailsInfo value) {
        return new JAXBElement<OutGetBanDetailsInfo>(_GetBanDetailsResponseElement_QNAME, OutGetBanDetailsInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreateAdjReversalInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createAdjReversalResponseElement")
    public JAXBElement<OutCreateAdjReversalInfo> createCreateAdjReversalResponseElement(OutCreateAdjReversalInfo value) {
        return new JAXBElement<OutCreateAdjReversalInfo>(_CreateAdjReversalResponseElement_QNAME, OutCreateAdjReversalInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCheckToChangeFavoriteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "checkToChangeFavoriteElement")
    public JAXBElement<InCheckToChangeFavoriteInfo> createCheckToChangeFavoriteElement(InCheckToChangeFavoriteInfo value) {
        return new JAXBElement<InCheckToChangeFavoriteInfo>(_CheckToChangeFavoriteElement_QNAME, InCheckToChangeFavoriteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutSuspendSubscriberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "suspendSubscriberResponseElement")
    public JAXBElement<OutSuspendSubscriberInfo> createSuspendSubscriberResponseElement(OutSuspendSubscriberInfo value) {
        return new JAXBElement<OutSuspendSubscriberInfo>(_SuspendSubscriberResponseElement_QNAME, OutSuspendSubscriberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InUpdateSocBanLevelInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateSocBanLevelElement")
    public JAXBElement<InUpdateSocBanLevelInfo> createUpdateSocBanLevelElement(InUpdateSocBanLevelInfo value) {
        return new JAXBElement<InUpdateSocBanLevelInfo>(_UpdateSocBanLevelElement_QNAME, InUpdateSocBanLevelInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCheckStreetInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "checkStreetOnBanByIdElement")
    public JAXBElement<InCheckStreetInfo> createCheckStreetOnBanByIdElement(InCheckStreetInfo value) {
        return new JAXBElement<InCheckStreetInfo>(_CheckStreetOnBanByIdElement_QNAME, InCheckStreetInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreateChargeInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createChargeResponseElement")
    public JAXBElement<OutCreateChargeInfo> createCreateChargeResponseElement(OutCreateChargeInfo value) {
        return new JAXBElement<OutCreateChargeInfo>(_CreateChargeResponseElement_QNAME, OutCreateChargeInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreateWlsSubInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createWlsSubElement")
    public JAXBElement<InCreateWlsSubInfo> createCreateWlsSubElement(InCreateWlsSubInfo value) {
        return new JAXBElement<InCreateWlsSubInfo>(_CreateWlsSubElement_QNAME, InCreateWlsSubInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetBillingHistByBanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getBillingHistByBanElement")
    public JAXBElement<InGetBillingHistByBanInfo> createGetBillingHistByBanElement(InGetBillingHistByBanInfo value) {
        return new JAXBElement<InGetBillingHistByBanInfo>(_GetBillingHistByBanElement_QNAME, InGetBillingHistByBanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "showServiceListElement")
    public JAXBElement<String> createShowServiceListElement(String value) {
        return new JAXBElement<String>(_ShowServiceListElement_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InUpdateSubscriberPlanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateSubscriberPlanElement")
    public JAXBElement<InUpdateSubscriberPlanInfo> createUpdateSubscriberPlanElement(InUpdateSubscriberPlanInfo value) {
        return new JAXBElement<InUpdateSubscriberPlanInfo>(_UpdateSubscriberPlanElement_QNAME, InUpdateSubscriberPlanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutReleaseResourceNumberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "releaseResourceNumberResponseElement")
    public JAXBElement<OutReleaseResourceNumberInfo> createReleaseResourceNumberResponseElement(OutReleaseResourceNumberInfo value) {
        return new JAXBElement<OutReleaseResourceNumberInfo>(_ReleaseResourceNumberResponseElement_QNAME, OutReleaseResourceNumberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutUpdateWirelineLeaseInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateWireLineLeaseResponseElement")
    public JAXBElement<OutUpdateWirelineLeaseInfo> createUpdateWireLineLeaseResponseElement(OutUpdateWirelineLeaseInfo value) {
        return new JAXBElement<OutUpdateWirelineLeaseInfo>(_UpdateWireLineLeaseResponseElement_QNAME, OutUpdateWirelineLeaseInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCancelSubscriberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "cancelSubscriberResponseElement")
    public JAXBElement<OutCancelSubscriberInfo> createCancelSubscriberResponseElement(OutCancelSubscriberInfo value) {
        return new JAXBElement<OutCancelSubscriberInfo>(_CancelSubscriberResponseElement_QNAME, OutCancelSubscriberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutUpdateSocBanLevelInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateSocBanLevelResponseElement")
    public JAXBElement<OutUpdateSocBanLevelInfo> createUpdateSocBanLevelResponseElement(OutUpdateSocBanLevelInfo value) {
        return new JAXBElement<OutUpdateSocBanLevelInfo>(_UpdateSocBanLevelResponseElement_QNAME, OutUpdateSocBanLevelInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InAssignResourceNumberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "assignResourceNumberElement")
    public JAXBElement<InAssignResourceNumberInfo> createAssignResourceNumberElement(InAssignResourceNumberInfo value) {
        return new JAXBElement<InAssignResourceNumberInfo>(_AssignResourceNumberElement_QNAME, InAssignResourceNumberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutChangeAddressCatInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "changeAddressCatalogResponseElement")
    public JAXBElement<OutChangeAddressCatInfo> createChangeAddressCatalogResponseElement(OutChangeAddressCatInfo value) {
        return new JAXBElement<OutChangeAddressCatInfo>(_ChangeAddressCatalogResponseElement_QNAME, OutChangeAddressCatInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreateAdjustmentByBanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createAdjustmentByBanElement")
    public JAXBElement<InCreateAdjustmentByBanInfo> createCreateAdjustmentByBanElement(InCreateAdjustmentByBanInfo value) {
        return new JAXBElement<InCreateAdjustmentByBanInfo>(_CreateAdjustmentByBanElement_QNAME, InCreateAdjustmentByBanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetPenaltyCommitmentsInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getPenaltyCommitmentsResponseElement")
    public JAXBElement<OutGetPenaltyCommitmentsInfo> createGetPenaltyCommitmentsResponseElement(OutGetPenaltyCommitmentsInfo value) {
        return new JAXBElement<OutGetPenaltyCommitmentsInfo>(_GetPenaltyCommitmentsResponseElement_QNAME, OutGetPenaltyCommitmentsInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreateWlsSubInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createWlsSubResponseElement")
    public JAXBElement<OutCreateWlsSubInfo> createCreateWlsSubResponseElement(OutCreateWlsSubInfo value) {
        return new JAXBElement<OutCreateWlsSubInfo>(_CreateWlsSubResponseElement_QNAME, OutCreateWlsSubInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreateAdjustmentByBanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createAdjustmentByBanResponseElement")
    public JAXBElement<OutCreateAdjustmentByBanInfo> createCreateAdjustmentByBanResponseElement(OutCreateAdjustmentByBanInfo value) {
        return new JAXBElement<OutCreateAdjustmentByBanInfo>(_CreateAdjustmentByBanResponseElement_QNAME, OutCreateAdjustmentByBanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreateBillImageInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createBillImageResponseElement")
    public JAXBElement<OutCreateBillImageInfo> createCreateBillImageResponseElement(OutCreateBillImageInfo value) {
        return new JAXBElement<OutCreateBillImageInfo>(_CreateBillImageResponseElement_QNAME, OutCreateBillImageInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetPaymentArrangByBanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getPaymentArrangByBanResponseElement")
    public JAXBElement<OutGetPaymentArrangByBanInfo> createGetPaymentArrangByBanResponseElement(OutGetPaymentArrangByBanInfo value) {
        return new JAXBElement<OutGetPaymentArrangByBanInfo>(_GetPaymentArrangByBanResponseElement_QNAME, OutGetPaymentArrangByBanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutUpdateSubscriberSIMInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateSubscriberSIMResponseElement")
    public JAXBElement<OutUpdateSubscriberSIMInfo> createUpdateSubscriberSIMResponseElement(OutUpdateSubscriberSIMInfo value) {
        return new JAXBElement<OutUpdateSubscriberSIMInfo>(_UpdateSubscriberSIMResponseElement_QNAME, OutUpdateSubscriberSIMInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetSubscriberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getSubscriberInfoElement")
    public JAXBElement<InGetSubscriberInfo> createGetSubscriberInfoElement(InGetSubscriberInfo value) {
        return new JAXBElement<InGetSubscriberInfo>(_GetSubscriberInfoElement_QNAME, InGetSubscriberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetAvailableFavCountInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getAvailableFavoriteCountElement")
    public JAXBElement<InGetAvailableFavCountInfo> createGetAvailableFavoriteCountElement(InGetAvailableFavCountInfo value) {
        return new JAXBElement<InGetAvailableFavCountInfo>(_GetAvailableFavoriteCountElement_QNAME, InGetAvailableFavCountInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutRegisterLoyaltyProgInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "registerLoyaltyProgramResponseElement")
    public JAXBElement<OutRegisterLoyaltyProgInfo> createRegisterLoyaltyProgramResponseElement(OutRegisterLoyaltyProgInfo value) {
        return new JAXBElement<OutRegisterLoyaltyProgInfo>(_RegisterLoyaltyProgramResponseElement_QNAME, OutRegisterLoyaltyProgInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetBanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getBanInfoElement")
    public JAXBElement<InGetBanInfo> createGetBanInfoElement(InGetBanInfo value) {
        return new JAXBElement<InGetBanInfo>(_GetBanInfoElement_QNAME, InGetBanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InUpdateWirelineLeaseInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateWireLineLeaseElement")
    public JAXBElement<InUpdateWirelineLeaseInfo> createUpdateWireLineLeaseElement(InUpdateWirelineLeaseInfo value) {
        return new JAXBElement<InUpdateWirelineLeaseInfo>(_UpdateWireLineLeaseElement_QNAME, InUpdateWirelineLeaseInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreateWlsSubSubLvlInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createWlsSubSubLvlElement")
    public JAXBElement<InCreateWlsSubSubLvlInfo> createCreateWlsSubSubLvlElement(InCreateWlsSubSubLvlInfo value) {
        return new JAXBElement<InCreateWlsSubSubLvlInfo>(_CreateWlsSubSubLvlElement_QNAME, InCreateWlsSubSubLvlInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InChangeFavoriteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "changeFavoriteElement")
    public JAXBElement<InChangeFavoriteInfo> createChangeFavoriteElement(InChangeFavoriteInfo value) {
        return new JAXBElement<InChangeFavoriteInfo>(_ChangeFavoriteElement_QNAME, InChangeFavoriteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetAvailableFavCountInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getAvailableFavoriteCountResponseElement")
    public JAXBElement<OutGetAvailableFavCountInfo> createGetAvailableFavoriteCountResponseElement(OutGetAvailableFavCountInfo value) {
        return new JAXBElement<OutGetAvailableFavCountInfo>(_GetAvailableFavoriteCountResponseElement_QNAME, OutGetAvailableFavCountInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreateAdjustmentByCrgInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createAdjustmentByCrgElement")
    public JAXBElement<InCreateAdjustmentByCrgInfo> createCreateAdjustmentByCrgElement(InCreateAdjustmentByCrgInfo value) {
        return new JAXBElement<InCreateAdjustmentByCrgInfo>(_CreateAdjustmentByCrgElement_QNAME, InCreateAdjustmentByCrgInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreateMemoInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createMemoElement")
    public JAXBElement<InCreateMemoInfo> createCreateMemoElement(InCreateMemoInfo value) {
        return new JAXBElement<InCreateMemoInfo>(_CreateMemoElement_QNAME, InCreateMemoInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCreateAdjReversalInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createAdjReversalElement")
    public JAXBElement<InCreateAdjReversalInfo> createCreateAdjReversalElement(InCreateAdjReversalInfo value) {
        return new JAXBElement<InCreateAdjReversalInfo>(_CreateAdjReversalElement_QNAME, InCreateAdjReversalInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetBanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getBanInfoResponseElement")
    public JAXBElement<OutGetBanInfo> createGetBanInfoResponseElement(OutGetBanInfo value) {
        return new JAXBElement<OutGetBanInfo>(_GetBanInfoResponseElement_QNAME, OutGetBanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetCustListBySubInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getCustListBySubElement")
    public JAXBElement<InGetCustListBySubInfo> createGetCustListBySubElement(InGetCustListBySubInfo value) {
        return new JAXBElement<InGetCustListBySubInfo>(_GetCustListBySubElement_QNAME, InGetCustListBySubInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InCheckToAddFavoriteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "checkToAddFavoriteElement")
    public JAXBElement<InCheckToAddFavoriteInfo> createCheckToAddFavoriteElement(InCheckToAddFavoriteInfo value) {
        return new JAXBElement<InCheckToAddFavoriteInfo>(_CheckToAddFavoriteElement_QNAME, InCheckToAddFavoriteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InAddFavoriteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "addFavoriteElement")
    public JAXBElement<InAddFavoriteInfo> createAddFavoriteElement(InAddFavoriteInfo value) {
        return new JAXBElement<InAddFavoriteInfo>(_AddFavoriteElement_QNAME, InAddFavoriteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InRegisterLoyaltyProgInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "registerLoyaltyProgramElement")
    public JAXBElement<InRegisterLoyaltyProgInfo> createRegisterLoyaltyProgramElement(InRegisterLoyaltyProgInfo value) {
        return new JAXBElement<InRegisterLoyaltyProgInfo>(_RegisterLoyaltyProgramElement_QNAME, InRegisterLoyaltyProgInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetPenaltyCalcInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getPenaltyCalcResponseElement")
    public JAXBElement<OutGetPenaltyCalcInfo> createGetPenaltyCalcResponseElement(OutGetPenaltyCalcInfo value) {
        return new JAXBElement<OutGetPenaltyCalcInfo>(_GetPenaltyCalcResponseElement_QNAME, OutGetPenaltyCalcInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetBillingHistByBanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getBillingHistByBanResponseElement")
    public JAXBElement<OutGetBillingHistByBanInfo> createGetBillingHistByBanResponseElement(OutGetBillingHistByBanInfo value) {
        return new JAXBElement<OutGetBillingHistByBanInfo>(_GetBillingHistByBanResponseElement_QNAME, OutGetBillingHistByBanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InRestoreSubscriberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "restoreSubscriberElement")
    public JAXBElement<InRestoreSubscriberInfo> createRestoreSubscriberElement(InRestoreSubscriberInfo value) {
        return new JAXBElement<InRestoreSubscriberInfo>(_RestoreSubscriberElement_QNAME, InRestoreSubscriberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreatePaymentArrangInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createPaymentArrangResponseElement")
    public JAXBElement<OutCreatePaymentArrangInfo> createCreatePaymentArrangResponseElement(OutCreatePaymentArrangInfo value) {
        return new JAXBElement<OutCreatePaymentArrangInfo>(_CreatePaymentArrangResponseElement_QNAME, OutCreatePaymentArrangInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreateLoanEquipmentInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createLoanEquipmentResponseElement")
    public JAXBElement<OutCreateLoanEquipmentInfo> createCreateLoanEquipmentResponseElement(OutCreateLoanEquipmentInfo value) {
        return new JAXBElement<OutCreateLoanEquipmentInfo>(_CreateLoanEquipmentResponseElement_QNAME, OutCreateLoanEquipmentInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutCreateMemoInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "createMemoResponseElement")
    public JAXBElement<OutCreateMemoInfo> createCreateMemoResponseElement(OutCreateMemoInfo value) {
        return new JAXBElement<OutCreateMemoInfo>(_CreateMemoResponseElement_QNAME, OutCreateMemoInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetPaymentArrangByBanInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getPaymentArrangByBanElement")
    public JAXBElement<InGetPaymentArrangByBanInfo> createGetPaymentArrangByBanElement(InGetPaymentArrangByBanInfo value) {
        return new JAXBElement<InGetPaymentArrangByBanInfo>(_GetPaymentArrangByBanElement_QNAME, InGetPaymentArrangByBanInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutChangeFavoriteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "changeFavoriteResponseElement")
    public JAXBElement<OutChangeFavoriteInfo> createChangeFavoriteResponseElement(OutChangeFavoriteInfo value) {
        return new JAXBElement<OutChangeFavoriteInfo>(_ChangeFavoriteResponseElement_QNAME, OutChangeFavoriteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutRestoreSubscriberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "restoreSubscriberResponseElement")
    public JAXBElement<OutRestoreSubscriberInfo> createRestoreSubscriberResponseElement(OutRestoreSubscriberInfo value) {
        return new JAXBElement<OutRestoreSubscriberInfo>(_RestoreSubscriberResponseElement_QNAME, OutRestoreSubscriberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutAddFavoriteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "checkToAddFavoriteResponseElement")
    public JAXBElement<OutAddFavoriteInfo> createCheckToAddFavoriteResponseElement(OutAddFavoriteInfo value) {
        return new JAXBElement<OutAddFavoriteInfo>(_CheckToAddFavoriteResponseElement_QNAME, OutAddFavoriteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InUpdateSubscriberSIMInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateSubscriberSIMElement")
    public JAXBElement<InUpdateSubscriberSIMInfo> createUpdateSubscriberSIMElement(InUpdateSubscriberSIMInfo value) {
        return new JAXBElement<InUpdateSubscriberSIMInfo>(_UpdateSubscriberSIMElement_QNAME, InUpdateSubscriberSIMInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetCreditLimitInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getCreditLimitResponseElement")
    public JAXBElement<OutGetCreditLimitInfo> createGetCreditLimitResponseElement(OutGetCreditLimitInfo value) {
        return new JAXBElement<OutGetCreditLimitInfo>(_GetCreditLimitResponseElement_QNAME, OutGetCreditLimitInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutUpdateSubscriberFtrSwPrm }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "updateSubscriberFtrSwPrmResponseElement")
    public JAXBElement<OutUpdateSubscriberFtrSwPrm> createUpdateSubscriberFtrSwPrmResponseElement(OutUpdateSubscriberFtrSwPrm value) {
        return new JAXBElement<OutUpdateSubscriberFtrSwPrm>(_UpdateSubscriberFtrSwPrmResponseElement_QNAME, OutUpdateSubscriberFtrSwPrm.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutGetPaymentArrangQuoteInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getPaymentArrangQuoteResponseElement")
    public JAXBElement<OutGetPaymentArrangQuoteInfo> createGetPaymentArrangQuoteResponseElement(OutGetPaymentArrangQuoteInfo value) {
        return new JAXBElement<OutGetPaymentArrangQuoteInfo>(_GetPaymentArrangQuoteResponseElement_QNAME, OutGetPaymentArrangQuoteInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InGetPenaltyCalcInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "getPenaltyCalcElement")
    public JAXBElement<InGetPenaltyCalcInfo> createGetPenaltyCalcElement(InGetPenaltyCalcInfo value) {
        return new JAXBElement<InGetPenaltyCalcInfo>(_GetPenaltyCalcElement_QNAME, InGetPenaltyCalcInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InReleaseResourceNumberInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "releaseResourceNumberElement")
    public JAXBElement<InReleaseResourceNumberInfo> createReleaseResourceNumberElement(InReleaseResourceNumberInfo value) {
        return new JAXBElement<InReleaseResourceNumberInfo>(_ReleaseResourceNumberElement_QNAME, InReleaseResourceNumberInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.postpaid.codetel.com/", name = "showServiceListResponseElement")
    public JAXBElement<String> createShowServiceListResponseElement(String value) {
        return new JAXBElement<String>(_ShowServiceListResponseElement_QNAME, String.class, null, value);
    }

}
