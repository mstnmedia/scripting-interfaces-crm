
package codetel.agp.webservice.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import codetel.agp.webservice.service.responses.MessageInfo;


/**
 * <p>Java class for customerBasicInformationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="customerBasicInformationResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://responses.service.webservice.agp.codetel}MessageInfo">
 *       &lt;sequence>
 *         &lt;element name="universalCustomerProfileInfo" type="{http://service.webservice.agp.codetel/}universalCustomerProfileInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customerBasicInformationResponse", propOrder = {
    "universalCustomerProfileInfo"
})
public class CustomerBasicInformationResponse
    extends MessageInfo
{

    protected UniversalCustomerProfileInfo universalCustomerProfileInfo;

    /**
     * Gets the value of the universalCustomerProfileInfo property.
     * 
     * @return
     *     possible object is
     *     {@link UniversalCustomerProfileInfo }
     *     
     */
    public UniversalCustomerProfileInfo getUniversalCustomerProfileInfo() {
        return universalCustomerProfileInfo;
    }

    /**
     * Sets the value of the universalCustomerProfileInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalCustomerProfileInfo }
     *     
     */
    public void setUniversalCustomerProfileInfo(UniversalCustomerProfileInfo value) {
        this.universalCustomerProfileInfo = value;
    }

}
