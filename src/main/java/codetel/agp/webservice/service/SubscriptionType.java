
package codetel.agp.webservice.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for subscriptionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="subscriptionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="POSTPAGO"/>
 *     &lt;enumeration value="PREPAGO"/>
 *     &lt;enumeration value="NONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "subscriptionType")
@XmlEnum
public enum SubscriptionType {

    POSTPAGO,
    PREPAGO,
    NONE;

    public String value() {
        return name();
    }

    public static SubscriptionType fromValue(String v) {
        return valueOf(v);
    }

}
