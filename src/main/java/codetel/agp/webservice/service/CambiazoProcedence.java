
package codetel.agp.webservice.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cambiazoProcedence.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="cambiazoProcedence">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="POSTPAID"/>
 *     &lt;enumeration value="PREPAID"/>
 *     &lt;enumeration value="UNDEFINED"/>
 *     &lt;enumeration value="NOT_FOUND"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "cambiazoProcedence")
@XmlEnum
public enum CambiazoProcedence {

    POSTPAID,
    PREPAID,
    UNDEFINED,
    NOT_FOUND;

    public String value() {
        return name();
    }

    public static CambiazoProcedence fromValue(String v) {
        return valueOf(v);
    }

}
