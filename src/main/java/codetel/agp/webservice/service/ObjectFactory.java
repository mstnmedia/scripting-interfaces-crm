
package codetel.agp.webservice.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import codetel.agp.webservice.service.responses.MessageInfo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the codetel.agp.webservice.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeleteUserResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "deleteUserResponse");
    private final static QName _MiClaroRegisteredMemberVerificationResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "miClaroRegisteredMemberVerificationResponse");
    private final static QName _GetCustomerContractByDocumentResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "getCustomerContractByDocumentResponse");
    private final static QName _GetCustomerBasicInformation_QNAME = new QName("http://service.webservice.agp.codetel/", "getCustomerBasicInformation");
    private final static QName _GetCustomerContractByMsisdnResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "getCustomerContractByMsisdnResponse");
    private final static QName _MiClaroRegisteredMemberVerification_QNAME = new QName("http://service.webservice.agp.codetel/", "miClaroRegisteredMemberVerification");
    private final static QName _ValidateCustomerExistInClaroResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "validateCustomerExistInClaroResponse");
    private final static QName _DeleteUser_QNAME = new QName("http://service.webservice.agp.codetel/", "deleteUser");
    private final static QName _EmployeeReferance_QNAME = new QName("http://service.webservice.agp.codetel/", "employeeReferance");
    private final static QName _EmployeeReferanceResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "employeeReferanceResponse");
    private final static QName _DeleteUserStatus_QNAME = new QName("http://service.webservice.agp.codetel/", "DeleteUserStatus");
    private final static QName _CustomerBasicInformationResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "customerBasicInformationResponse");
    private final static QName _GetCustomerBasicInformationResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "getCustomerBasicInformationResponse");
    private final static QName _ValidateCustomerExistInClaro_QNAME = new QName("http://service.webservice.agp.codetel/", "validateCustomerExistInClaro");
    private final static QName _GetCambiazoInformationForMSISDNResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "getCambiazoInformationForMSISDNResponse");
    private final static QName _CustomerReferance_QNAME = new QName("http://service.webservice.agp.codetel/", "customerReferance");
    private final static QName _GetCambiazoInformationForMSISDN_QNAME = new QName("http://service.webservice.agp.codetel/", "getCambiazoInformationForMSISDN");
    private final static QName _GetCustomerContractByMsisdn_QNAME = new QName("http://service.webservice.agp.codetel/", "getCustomerContractByMsisdn");
    private final static QName _CustomerReferanceResponse_QNAME = new QName("http://service.webservice.agp.codetel/", "customerReferanceResponse");
    private final static QName _MessageInfo_QNAME = new QName("http://service.webservice.agp.codetel/", "messageInfo");
    private final static QName _GetCustomerContractByDocument_QNAME = new QName("http://service.webservice.agp.codetel/", "getCustomerContractByDocument");
    private final static QName _MiClaroRegisteredMemberVerificationInformationCode_QNAME = new QName("", "code");
    private final static QName _MiClaroRegisteredMemberVerificationInformationExists_QNAME = new QName("", "exists");
    private final static QName _MiClaroRegisteredMemberVerificationInformationMessage_QNAME = new QName("", "message");
    private final static QName _MiClaroRegisteredMemberVerificationInformationError_QNAME = new QName("", "error");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: codetel.agp.webservice.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCustomerContractByDocumentResponse }
     * 
     */
    public GetCustomerContractByDocumentResponse createGetCustomerContractByDocumentResponse() {
        return new GetCustomerContractByDocumentResponse();
    }

    /**
     * Create an instance of {@link DeleteUserResponse }
     * 
     */
    public DeleteUserResponse createDeleteUserResponse() {
        return new DeleteUserResponse();
    }

    /**
     * Create an instance of {@link MiClaroRegisteredMemberVerification }
     * 
     */
    public MiClaroRegisteredMemberVerification createMiClaroRegisteredMemberVerification() {
        return new MiClaroRegisteredMemberVerification();
    }

    /**
     * Create an instance of {@link UniversalCustomerProfileInfo }
     * 
     */
    public UniversalCustomerProfileInfo createUniversalCustomerProfileInfo() {
        return new UniversalCustomerProfileInfo();
    }

    /**
     * Create an instance of {@link EmployeeReferance }
     * 
     */
    public EmployeeReferance createEmployeeReferance() {
        return new EmployeeReferance();
    }

    /**
     * Create an instance of {@link GetCambiazoInformationForMSISDN }
     * 
     */
    public GetCambiazoInformationForMSISDN createGetCambiazoInformationForMSISDN() {
        return new GetCambiazoInformationForMSISDN();
    }

    /**
     * Create an instance of {@link EmployeeReferanceResponse }
     * 
     */
    public EmployeeReferanceResponse createEmployeeReferanceResponse() {
        return new EmployeeReferanceResponse();
    }

    /**
     * Create an instance of {@link DeleteUser }
     * 
     */
    public DeleteUser createDeleteUser() {
        return new DeleteUser();
    }

    /**
     * Create an instance of {@link DeleteUserStatus }
     * 
     */
    public DeleteUserStatus createDeleteUserStatus() {
        return new DeleteUserStatus();
    }

    /**
     * Create an instance of {@link GetCustomerContractByDocument }
     * 
     */
    public GetCustomerContractByDocument createGetCustomerContractByDocument() {
        return new GetCustomerContractByDocument();
    }

    /**
     * Create an instance of {@link GetCustomerBasicInformationResponse }
     * 
     */
    public GetCustomerBasicInformationResponse createGetCustomerBasicInformationResponse() {
        return new GetCustomerBasicInformationResponse();
    }

    /**
     * Create an instance of {@link MiClaroRegisteredMemberVerificationInformation }
     * 
     */
    public MiClaroRegisteredMemberVerificationInformation createMiClaroRegisteredMemberVerificationInformation() {
        return new MiClaroRegisteredMemberVerificationInformation();
    }

    /**
     * Create an instance of {@link GetCustomerBasicInformation }
     * 
     */
    public GetCustomerBasicInformation createGetCustomerBasicInformation() {
        return new GetCustomerBasicInformation();
    }

    /**
     * Create an instance of {@link ContractUserModel }
     * 
     */
    public ContractUserModel createContractUserModel() {
        return new ContractUserModel();
    }

    /**
     * Create an instance of {@link CustomerReferanceResponse }
     * 
     */
    public CustomerReferanceResponse createCustomerReferanceResponse() {
        return new CustomerReferanceResponse();
    }

    /**
     * Create an instance of {@link CustomerSubscription }
     * 
     */
    public CustomerSubscription createCustomerSubscription() {
        return new CustomerSubscription();
    }

    /**
     * Create an instance of {@link ValidateCustomerExistInClaro }
     * 
     */
    public ValidateCustomerExistInClaro createValidateCustomerExistInClaro() {
        return new ValidateCustomerExistInClaro();
    }

    /**
     * Create an instance of {@link MiClaroRegisteredMemberVerificationResponse }
     * 
     */
    public MiClaroRegisteredMemberVerificationResponse createMiClaroRegisteredMemberVerificationResponse() {
        return new MiClaroRegisteredMemberVerificationResponse();
    }

    /**
     * Create an instance of {@link CustomerBasicInformationResponse }
     * 
     */
    public CustomerBasicInformationResponse createCustomerBasicInformationResponse() {
        return new CustomerBasicInformationResponse();
    }

    /**
     * Create an instance of {@link GetCustomerContractByMsisdnResponse }
     * 
     */
    public GetCustomerContractByMsisdnResponse createGetCustomerContractByMsisdnResponse() {
        return new GetCustomerContractByMsisdnResponse();
    }

    /**
     * Create an instance of {@link GetCambiazoInformationForMSISDNResponse }
     * 
     */
    public GetCambiazoInformationForMSISDNResponse createGetCambiazoInformationForMSISDNResponse() {
        return new GetCambiazoInformationForMSISDNResponse();
    }

    /**
     * Create an instance of {@link GetCustomerContractByMsisdn }
     * 
     */
    public GetCustomerContractByMsisdn createGetCustomerContractByMsisdn() {
        return new GetCustomerContractByMsisdn();
    }

    /**
     * Create an instance of {@link ReferCreationVoucher }
     * 
     */
    public ReferCreationVoucher createReferCreationVoucher() {
        return new ReferCreationVoucher();
    }

    /**
     * Create an instance of {@link ContractsModel }
     * 
     */
    public ContractsModel createContractsModel() {
        return new ContractsModel();
    }

    /**
     * Create an instance of {@link ContractTypeModel }
     * 
     */
    public ContractTypeModel createContractTypeModel() {
        return new ContractTypeModel();
    }

    /**
     * Create an instance of {@link CustomerReferance }
     * 
     */
    public CustomerReferance createCustomerReferance() {
        return new CustomerReferance();
    }

    /**
     * Create an instance of {@link ValidateCustomerExistInClaroResponse }
     * 
     */
    public ValidateCustomerExistInClaroResponse createValidateCustomerExistInClaroResponse() {
        return new ValidateCustomerExistInClaroResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "deleteUserResponse")
    public JAXBElement<DeleteUserResponse> createDeleteUserResponse(DeleteUserResponse value) {
        return new JAXBElement<DeleteUserResponse>(_DeleteUserResponse_QNAME, DeleteUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MiClaroRegisteredMemberVerificationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "miClaroRegisteredMemberVerificationResponse")
    public JAXBElement<MiClaroRegisteredMemberVerificationResponse> createMiClaroRegisteredMemberVerificationResponse(MiClaroRegisteredMemberVerificationResponse value) {
        return new JAXBElement<MiClaroRegisteredMemberVerificationResponse>(_MiClaroRegisteredMemberVerificationResponse_QNAME, MiClaroRegisteredMemberVerificationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerContractByDocumentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "getCustomerContractByDocumentResponse")
    public JAXBElement<GetCustomerContractByDocumentResponse> createGetCustomerContractByDocumentResponse(GetCustomerContractByDocumentResponse value) {
        return new JAXBElement<GetCustomerContractByDocumentResponse>(_GetCustomerContractByDocumentResponse_QNAME, GetCustomerContractByDocumentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerBasicInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "getCustomerBasicInformation")
    public JAXBElement<GetCustomerBasicInformation> createGetCustomerBasicInformation(GetCustomerBasicInformation value) {
        return new JAXBElement<GetCustomerBasicInformation>(_GetCustomerBasicInformation_QNAME, GetCustomerBasicInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerContractByMsisdnResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "getCustomerContractByMsisdnResponse")
    public JAXBElement<GetCustomerContractByMsisdnResponse> createGetCustomerContractByMsisdnResponse(GetCustomerContractByMsisdnResponse value) {
        return new JAXBElement<GetCustomerContractByMsisdnResponse>(_GetCustomerContractByMsisdnResponse_QNAME, GetCustomerContractByMsisdnResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MiClaroRegisteredMemberVerification }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "miClaroRegisteredMemberVerification")
    public JAXBElement<MiClaroRegisteredMemberVerification> createMiClaroRegisteredMemberVerification(MiClaroRegisteredMemberVerification value) {
        return new JAXBElement<MiClaroRegisteredMemberVerification>(_MiClaroRegisteredMemberVerification_QNAME, MiClaroRegisteredMemberVerification.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCustomerExistInClaroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "validateCustomerExistInClaroResponse")
    public JAXBElement<ValidateCustomerExistInClaroResponse> createValidateCustomerExistInClaroResponse(ValidateCustomerExistInClaroResponse value) {
        return new JAXBElement<ValidateCustomerExistInClaroResponse>(_ValidateCustomerExistInClaroResponse_QNAME, ValidateCustomerExistInClaroResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "deleteUser")
    public JAXBElement<DeleteUser> createDeleteUser(DeleteUser value) {
        return new JAXBElement<DeleteUser>(_DeleteUser_QNAME, DeleteUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmployeeReferance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "employeeReferance")
    public JAXBElement<EmployeeReferance> createEmployeeReferance(EmployeeReferance value) {
        return new JAXBElement<EmployeeReferance>(_EmployeeReferance_QNAME, EmployeeReferance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmployeeReferanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "employeeReferanceResponse")
    public JAXBElement<EmployeeReferanceResponse> createEmployeeReferanceResponse(EmployeeReferanceResponse value) {
        return new JAXBElement<EmployeeReferanceResponse>(_EmployeeReferanceResponse_QNAME, EmployeeReferanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "DeleteUserStatus")
    public JAXBElement<DeleteUserStatus> createDeleteUserStatus(DeleteUserStatus value) {
        return new JAXBElement<DeleteUserStatus>(_DeleteUserStatus_QNAME, DeleteUserStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerBasicInformationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "customerBasicInformationResponse")
    public JAXBElement<CustomerBasicInformationResponse> createCustomerBasicInformationResponse(CustomerBasicInformationResponse value) {
        return new JAXBElement<CustomerBasicInformationResponse>(_CustomerBasicInformationResponse_QNAME, CustomerBasicInformationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerBasicInformationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "getCustomerBasicInformationResponse")
    public JAXBElement<GetCustomerBasicInformationResponse> createGetCustomerBasicInformationResponse(GetCustomerBasicInformationResponse value) {
        return new JAXBElement<GetCustomerBasicInformationResponse>(_GetCustomerBasicInformationResponse_QNAME, GetCustomerBasicInformationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCustomerExistInClaro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "validateCustomerExistInClaro")
    public JAXBElement<ValidateCustomerExistInClaro> createValidateCustomerExistInClaro(ValidateCustomerExistInClaro value) {
        return new JAXBElement<ValidateCustomerExistInClaro>(_ValidateCustomerExistInClaro_QNAME, ValidateCustomerExistInClaro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCambiazoInformationForMSISDNResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "getCambiazoInformationForMSISDNResponse")
    public JAXBElement<GetCambiazoInformationForMSISDNResponse> createGetCambiazoInformationForMSISDNResponse(GetCambiazoInformationForMSISDNResponse value) {
        return new JAXBElement<GetCambiazoInformationForMSISDNResponse>(_GetCambiazoInformationForMSISDNResponse_QNAME, GetCambiazoInformationForMSISDNResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerReferance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "customerReferance")
    public JAXBElement<CustomerReferance> createCustomerReferance(CustomerReferance value) {
        return new JAXBElement<CustomerReferance>(_CustomerReferance_QNAME, CustomerReferance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCambiazoInformationForMSISDN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "getCambiazoInformationForMSISDN")
    public JAXBElement<GetCambiazoInformationForMSISDN> createGetCambiazoInformationForMSISDN(GetCambiazoInformationForMSISDN value) {
        return new JAXBElement<GetCambiazoInformationForMSISDN>(_GetCambiazoInformationForMSISDN_QNAME, GetCambiazoInformationForMSISDN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerContractByMsisdn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "getCustomerContractByMsisdn")
    public JAXBElement<GetCustomerContractByMsisdn> createGetCustomerContractByMsisdn(GetCustomerContractByMsisdn value) {
        return new JAXBElement<GetCustomerContractByMsisdn>(_GetCustomerContractByMsisdn_QNAME, GetCustomerContractByMsisdn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerReferanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "customerReferanceResponse")
    public JAXBElement<CustomerReferanceResponse> createCustomerReferanceResponse(CustomerReferanceResponse value) {
        return new JAXBElement<CustomerReferanceResponse>(_CustomerReferanceResponse_QNAME, CustomerReferanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "messageInfo")
    public JAXBElement<MessageInfo> createMessageInfo(MessageInfo value) {
        return new JAXBElement<MessageInfo>(_MessageInfo_QNAME, MessageInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerContractByDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.webservice.agp.codetel/", name = "getCustomerContractByDocument")
    public JAXBElement<GetCustomerContractByDocument> createGetCustomerContractByDocument(GetCustomerContractByDocument value) {
        return new JAXBElement<GetCustomerContractByDocument>(_GetCustomerContractByDocument_QNAME, GetCustomerContractByDocument.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "code", scope = MiClaroRegisteredMemberVerificationInformation.class)
    public JAXBElement<Integer> createMiClaroRegisteredMemberVerificationInformationCode(Integer value) {
        return new JAXBElement<Integer>(_MiClaroRegisteredMemberVerificationInformationCode_QNAME, Integer.class, MiClaroRegisteredMemberVerificationInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "exists", scope = MiClaroRegisteredMemberVerificationInformation.class)
    public JAXBElement<Boolean> createMiClaroRegisteredMemberVerificationInformationExists(Boolean value) {
        return new JAXBElement<Boolean>(_MiClaroRegisteredMemberVerificationInformationExists_QNAME, Boolean.class, MiClaroRegisteredMemberVerificationInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "message", scope = MiClaroRegisteredMemberVerificationInformation.class)
    public JAXBElement<String> createMiClaroRegisteredMemberVerificationInformationMessage(String value) {
        return new JAXBElement<String>(_MiClaroRegisteredMemberVerificationInformationMessage_QNAME, String.class, MiClaroRegisteredMemberVerificationInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "error", scope = MiClaroRegisteredMemberVerificationInformation.class)
    public JAXBElement<Boolean> createMiClaroRegisteredMemberVerificationInformationError(Boolean value) {
        return new JAXBElement<Boolean>(_MiClaroRegisteredMemberVerificationInformationError_QNAME, Boolean.class, MiClaroRegisteredMemberVerificationInformation.class, value);
    }

}
