
package codetel.agp.webservice.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for subscriptionCategory.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="subscriptionCategory">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PREPAID"/>
 *     &lt;enumeration value="CONTROL"/>
 *     &lt;enumeration value="POSTPAID"/>
 *     &lt;enumeration value="POSTPAID_DATARATING"/>
 *     &lt;enumeration value="PREPAID_BROADBAND"/>
 *     &lt;enumeration value="CONTROL_BROADBAND"/>
 *     &lt;enumeration value="POSTPAID_BROADBAND"/>
 *     &lt;enumeration value="PREPAID_BLACKBERRY"/>
 *     &lt;enumeration value="RURAL_BROADBAND"/>
 *     &lt;enumeration value="KID"/>
 *     &lt;enumeration value="VPN"/>
 *     &lt;enumeration value="ADSL"/>
 *     &lt;enumeration value="PSTN"/>
 *     &lt;enumeration value="VOIP"/>
 *     &lt;enumeration value="CLARO_TV"/>
 *     &lt;enumeration value="DIAL_UP"/>
 *     &lt;enumeration value="MULTISIM"/>
 *     &lt;enumeration value="MULTISIMC"/>
 *     &lt;enumeration value="PREPAID_SIF"/>
 *     &lt;enumeration value="SIF"/>
 *     &lt;enumeration value="NONE"/>
 *     &lt;enumeration value="PCRF_LIMIT"/>
 *     &lt;enumeration value="PCRF_N_L"/>
 *     &lt;enumeration value="PCRF_N_P"/>
 *     &lt;enumeration value="PCRF_MDM"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "subscriptionCategory")
@XmlEnum
public enum SubscriptionCategory {

    PREPAID,
    CONTROL,
    POSTPAID,
    POSTPAID_DATARATING,
    PREPAID_BROADBAND,
    CONTROL_BROADBAND,
    POSTPAID_BROADBAND,
    PREPAID_BLACKBERRY,
    RURAL_BROADBAND,
    KID,
    VPN,
    ADSL,
    PSTN,
    VOIP,
    CLARO_TV,
    DIAL_UP,
    MULTISIM,
    MULTISIMC,
    PREPAID_SIF,
    SIF,
    NONE,
    PCRF_LIMIT,
    PCRF_N_L,
    PCRF_N_P,
    PCRF_MDM;

    public String value() {
        return name();
    }

    public static SubscriptionCategory fromValue(String v) {
        return valueOf(v);
    }

}
