
package codetel.agp.webservice.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for subscriptionTypeSubCategory.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="subscriptionTypeSubCategory">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MOVIL"/>
 *     &lt;enumeration value="FIJO"/>
 *     &lt;enumeration value="TV"/>
 *     &lt;enumeration value="NONE"/>
 *     &lt;enumeration value="MODEM"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "subscriptionTypeSubCategory")
@XmlEnum
public enum SubscriptionTypeSubCategory {

    MOVIL,
    FIJO,
    TV,
    NONE,
    MODEM;

    public String value() {
        return name();
    }

    public static SubscriptionTypeSubCategory fromValue(String v) {
        return valueOf(v);
    }

}
