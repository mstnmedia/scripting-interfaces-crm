
package codetel.agp.webservice.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for universalCustomerProfileInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="universalCustomerProfileInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="birthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ccu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documentIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="postpaidCustomer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="prepaidCustomer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="subscriptions" type="{http://service.webservice.agp.codetel/}customerSubscription" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "universalCustomerProfileInfo", propOrder = {
    "birthDate",
    "ccu",
    "documentIdentifier",
    "documentType",
    "name",
    "postpaidCustomer",
    "prepaidCustomer",
    "subscriptions"
})
public class UniversalCustomerProfileInfo {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar birthDate;
    protected String ccu;
    protected String documentIdentifier;
    protected String documentType;
    protected String name;
    protected boolean postpaidCustomer;
    protected boolean prepaidCustomer;
    @XmlElement(nillable = true)
    protected List<CustomerSubscription> subscriptions;

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDate(XMLGregorianCalendar value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the ccu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcu() {
        return ccu;
    }

    /**
     * Sets the value of the ccu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcu(String value) {
        this.ccu = value;
    }

    /**
     * Gets the value of the documentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentIdentifier() {
        return documentIdentifier;
    }

    /**
     * Sets the value of the documentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentIdentifier(String value) {
        this.documentIdentifier = value;
    }

    /**
     * Gets the value of the documentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentType() {
        return documentType;
    }

    /**
     * Sets the value of the documentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentType(String value) {
        this.documentType = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the postpaidCustomer property.
     * 
     */
    public boolean isPostpaidCustomer() {
        return postpaidCustomer;
    }

    /**
     * Sets the value of the postpaidCustomer property.
     * 
     */
    public void setPostpaidCustomer(boolean value) {
        this.postpaidCustomer = value;
    }

    /**
     * Gets the value of the prepaidCustomer property.
     * 
     */
    public boolean isPrepaidCustomer() {
        return prepaidCustomer;
    }

    /**
     * Sets the value of the prepaidCustomer property.
     * 
     */
    public void setPrepaidCustomer(boolean value) {
        this.prepaidCustomer = value;
    }

    /**
     * Gets the value of the subscriptions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subscriptions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubscriptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerSubscription }
     * 
     * 
     */
    public List<CustomerSubscription> getSubscriptions() {
        if (subscriptions == null) {
            subscriptions = new ArrayList<CustomerSubscription>();
        }
        return this.subscriptions;
    }

}
