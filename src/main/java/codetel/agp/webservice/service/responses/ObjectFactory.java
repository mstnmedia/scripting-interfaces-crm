
package codetel.agp.webservice.service.responses;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import codetel.agp.webservice.service.MiClaroRegisteredMemberVerificationInformation;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the codetel.agp.webservice.service.responses package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MiClaroRegisteredMemberVerificationInformation_QNAME = new QName("http://responses.service.webservice.agp.codetel", "MiClaroRegisteredMemberVerificationInformation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: codetel.agp.webservice.service.responses
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MessageInfo }
     * 
     */
    public MessageInfo createMessageInfo() {
        return new MessageInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MiClaroRegisteredMemberVerificationInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://responses.service.webservice.agp.codetel", name = "MiClaroRegisteredMemberVerificationInformation")
    public JAXBElement<MiClaroRegisteredMemberVerificationInformation> createMiClaroRegisteredMemberVerificationInformation(MiClaroRegisteredMemberVerificationInformation value) {
        return new JAXBElement<MiClaroRegisteredMemberVerificationInformation>(_MiClaroRegisteredMemberVerificationInformation_QNAME, MiClaroRegisteredMemberVerificationInformation.class, null, value);
    }

}
