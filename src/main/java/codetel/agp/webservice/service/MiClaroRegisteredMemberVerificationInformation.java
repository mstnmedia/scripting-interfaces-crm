package codetel.agp.webservice.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import codetel.agp.webservice.service.responses.MessageInfo;

/**
 * <p>
 * Java class for MiClaroRegisteredMemberVerificationInformation complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="MiClaroRegisteredMemberVerificationInformation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://responses.service.webservice.agp.codetel}MessageInfo">
 *       &lt;sequence>
 *         &lt;element name="exists" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MiClaroRegisteredMemberVerificationInformation", propOrder = {
	"rest"
})
public class MiClaroRegisteredMemberVerificationInformation extends MessageInfo {

	@XmlElementRefs({
		@XmlElementRef(name = "exists", type = JAXBElement.class)
		,
        @XmlElementRef(name = "message", type = JAXBElement.class)
		,
        @XmlElementRef(name = "error", type = JAXBElement.class)
		,
        @XmlElementRef(name = "code", type = JAXBElement.class)
	})
	protected List<JAXBElement> rest;

	/**
	 * Gets the rest of the content model.
	 *
	 * <p>
	 * You are getting this "catch-all" property because of the following
	 * reason: The field name "Message" is used by two different parts of a
	 * schema. See: line 109 of
	 * https://lxtwlcluts01:7015/agpws/ws/miclaroproviderservices?wsdl line 313
	 * of https://lxtwlcluts01:7015/agpws/ws/miclaroproviderservices?wsdl
	 * <p>
	 * To get rid of this property, apply a property customization to one of
	 * both of the following declarations to change their names: Gets the value
	 * of the rest property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the rest property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getRest().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list null	null	null	 {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
	 * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
	 * {@link JAXBElement }{@code <}{@link Integer }{@code >}
	 *
	 *
	 */
	public List<JAXBElement> getRest() {
		if (rest == null) {
			rest = new ArrayList<JAXBElement>();
		}
		return this.rest;
	}

}
