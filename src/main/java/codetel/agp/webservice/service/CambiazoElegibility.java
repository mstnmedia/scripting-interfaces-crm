
package codetel.agp.webservice.service;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cambiazoElegibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="cambiazoElegibility">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CAMBIAZO_12"/>
 *     &lt;enumeration value="CAMBIAZO_18"/>
 *     &lt;enumeration value="CAMBIAZO_12_FINANCED"/>
 *     &lt;enumeration value="CAMBIAZO_18_FINANCED"/>
 *     &lt;enumeration value="NONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "cambiazoElegibility")
@XmlEnum
public enum CambiazoElegibility {

    CAMBIAZO_12,
    CAMBIAZO_18,
    CAMBIAZO_12_FINANCED,
    CAMBIAZO_18_FINANCED,
    NONE;

    public String value() {
        return name();
    }

    public static CambiazoElegibility fromValue(String v) {
        return valueOf(v);
    }

}
