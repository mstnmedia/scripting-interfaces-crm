
package codetel.agp.webservice.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for customerSubscription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="customerSubscription">
 *   &lt;complexContent>
 *     &lt;extension base="{http://service.webservice.agp.codetel/}basicSubscription">
 *       &lt;sequence>
 *         &lt;element name="ban" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iccid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subscriptionType" type="{http://service.webservice.agp.codetel/}subscriptionType" minOccurs="0"/>
 *         &lt;element name="subscriptionTypeSubCategory" type="{http://service.webservice.agp.codetel/}subscriptionTypeSubCategory" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customerSubscription", propOrder = {
    "ban",
    "iccid",
    "planCategory",
    "planDescription",
    "subscriptionType",
    "subscriptionTypeSubCategory"
})
public class CustomerSubscription
    extends BasicSubscription
{

    protected String ban;
    protected String iccid;
    protected String planCategory;
    protected String planDescription;
    protected SubscriptionType subscriptionType;
    protected SubscriptionTypeSubCategory subscriptionTypeSubCategory;

    /**
     * Gets the value of the ban property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBan() {
        return ban;
    }

    /**
     * Sets the value of the ban property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBan(String value) {
        this.ban = value;
    }

    /**
     * Gets the value of the iccid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIccid() {
        return iccid;
    }

    /**
     * Sets the value of the iccid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIccid(String value) {
        this.iccid = value;
    }

    /**
     * Gets the value of the planCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanCategory() {
        return planCategory;
    }

    /**
     * Sets the value of the planCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanCategory(String value) {
        this.planCategory = value;
    }

    /**
     * Gets the value of the planDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanDescription() {
        return planDescription;
    }

    /**
     * Sets the value of the planDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanDescription(String value) {
        this.planDescription = value;
    }

    /**
     * Gets the value of the subscriptionType property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriptionType }
     *     
     */
    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    /**
     * Sets the value of the subscriptionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriptionType }
     *     
     */
    public void setSubscriptionType(SubscriptionType value) {
        this.subscriptionType = value;
    }

    /**
     * Gets the value of the subscriptionTypeSubCategory property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriptionTypeSubCategory }
     *     
     */
    public SubscriptionTypeSubCategory getSubscriptionTypeSubCategory() {
        return subscriptionTypeSubCategory;
    }

    /**
     * Sets the value of the subscriptionTypeSubCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriptionTypeSubCategory }
     *     
     */
    public void setSubscriptionTypeSubCategory(SubscriptionTypeSubCategory value) {
        this.subscriptionTypeSubCategory = value;
    }

}
