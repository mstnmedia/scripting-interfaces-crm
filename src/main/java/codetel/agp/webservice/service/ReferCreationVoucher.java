
package codetel.agp.webservice.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import codetel.agp.webservice.service.responses.MessageInfo;


/**
 * <p>Java class for referCreationVoucher complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="referCreationVoucher">
 *   &lt;complexContent>
 *     &lt;extension base="{http://responses.service.webservice.agp.codetel}MessageInfo">
 *       &lt;sequence>
 *         &lt;element name="referedCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "referCreationVoucher", propOrder = {
    "referedCode"
})
public class ReferCreationVoucher
    extends MessageInfo
{

    protected String referedCode;

    /**
     * Gets the value of the referedCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferedCode() {
        return referedCode;
    }

    /**
     * Sets the value of the referedCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferedCode(String value) {
        this.referedCode = value;
    }

}
