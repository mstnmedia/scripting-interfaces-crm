
package _do.com.claro.soa.services.ordering.getorderbyorderid;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * 
 *       @Created: SOA-140
 *       Returns the details of an Order that matches the input orderID.
 *       Either the order's uid, its displayID, or both, may be specified. The provided uid is given priority,
 *       otherwise the displayID is used to search if the former is not provided.
 *     
 * 
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.3-b02-
 * Generated source version: 2.1
 * 
 */
@WebService(name = "GetOrderByOrderID", targetNamespace = "http://www.claro.com.do/soa/services/ordering/GetOrderByOrderID")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    _do.com.claro.soa.model.billing.ObjectFactory.class,
    _do.com.claro.soa.model.ordering.ObjectFactory.class,
    _do.com.claro.soa.model.address.ObjectFactory.class,
    _do.com.claro.soa.model.generic.ObjectFactory.class,
    _do.com.claro.soa.model.customer.ObjectFactory.class,
    _do.com.claro.soa.services.ordering.getorderbyorderid.ObjectFactory.class,
    _do.com.claro.soa.model.product.ObjectFactory.class,
    _do.com.claro.soa.model.personidentity.ObjectFactory.class,
    _do.com.claro.soa.model.employee.ObjectFactory.class
})
public interface GetOrderByOrderID {


    /**
     * 
     * @param parameters
     * @return
     *     returns _do.com.claro.soa.services.ordering.getorderbyorderid.GetOrderResponse
     */
    @WebMethod(action = "http://www.claro.com.do/soa/services/ordering/GetOrderByOrderID/get")
    @WebResult(name = "getOrderResponse", targetNamespace = "http://www.claro.com.do/soa/services/ordering/GetOrderByOrderID", partName = "parameters")
    public GetOrderResponse get(
        @WebParam(name = "getOrderRequest", targetNamespace = "http://www.claro.com.do/soa/services/ordering/GetOrderByOrderID", partName = "parameters")
        GetOrderRequest parameters);

}
