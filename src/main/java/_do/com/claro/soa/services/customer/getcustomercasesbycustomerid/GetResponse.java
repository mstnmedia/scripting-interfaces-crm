
package _do.com.claro.soa.services.customer.getcustomercasesbycustomerid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.customer.Cases;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}cases"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cases"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/customer", required = true, nillable = true)
    protected Cases cases;

    /**
     * Gets the value of the cases property.
     * 
     * @return
     *     possible object is
     *     {@link Cases }
     *     
     */
    public Cases getCases() {
        return cases;
    }

    /**
     * Sets the value of the cases property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cases }
     *     
     */
    public void setCases(Cases value) {
        this.cases = value;
    }

}
