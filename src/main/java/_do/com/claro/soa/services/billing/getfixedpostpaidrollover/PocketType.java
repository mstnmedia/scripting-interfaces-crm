
package _do.com.claro.soa.services.billing.getfixedpostpaidrollover;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PocketType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PocketType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INCLUDED"/>
 *     &lt;enumeration value="CONSUMED"/>
 *     &lt;enumeration value="ROLLOVER30"/>
 *     &lt;enumeration value="ROLLOVER60"/>
 *     &lt;enumeration value="ROLLOVER90"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PocketType")
@XmlEnum
public enum PocketType {

    INCLUDED("INCLUDED"),
    CONSUMED("CONSUMED"),
    @XmlEnumValue("ROLLOVER30")
    ROLLOVER_30("ROLLOVER30"),
    @XmlEnumValue("ROLLOVER60")
    ROLLOVER_60("ROLLOVER60"),
    @XmlEnumValue("ROLLOVER90")
    ROLLOVER_90("ROLLOVER90");
    private final String value;

    PocketType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PocketType fromValue(String v) {
        for (PocketType c: PocketType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
