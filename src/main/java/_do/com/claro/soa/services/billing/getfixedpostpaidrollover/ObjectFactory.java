
package _do.com.claro.soa.services.billing.getfixedpostpaidrollover;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.services.billing.getfixedpostpaidrollover package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetResponse_QNAME = new QName("http://www.claro.com.do/soa/services/billing/GetFixedPostpaidRollover", "getResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.services.billing.getfixedpostpaidrollover
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetResponse.Result.Pockets.Pocket.Usage }
     * 
     */
    public GetResponse.Result.Pockets.Pocket.Usage createGetResponseResultPocketsPocketUsage() {
        return new GetResponse.Result.Pockets.Pocket.Usage();
    }

    /**
     * Create an instance of {@link GetResponse.Result.Pockets }
     * 
     */
    public GetResponse.Result.Pockets createGetResponseResultPockets() {
        return new GetResponse.Result.Pockets();
    }

    /**
     * Create an instance of {@link GetResponse.Result.Pockets.Pocket }
     * 
     */
    public GetResponse.Result.Pockets.Pocket createGetResponseResultPocketsPocket() {
        return new GetResponse.Result.Pockets.Pocket();
    }

    /**
     * Create an instance of {@link GetResponse.Result }
     * 
     */
    public GetResponse.Result createGetResponseResult() {
        return new GetResponse.Result();
    }

    /**
     * Create an instance of {@link GetResponse }
     * 
     */
    public GetResponse createGetResponse() {
        return new GetResponse();
    }

    /**
     * Create an instance of {@link GetRequest }
     * 
     */
    public GetRequest createGetRequest() {
        return new GetRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/services/billing/GetFixedPostpaidRollover", name = "getResponse")
    public JAXBElement<GetResponse> createGetResponse(GetResponse value) {
        return new JAXBElement<GetResponse>(_GetResponse_QNAME, GetResponse.class, null, value);
    }

}
