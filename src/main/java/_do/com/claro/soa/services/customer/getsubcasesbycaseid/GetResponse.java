
package _do.com.claro.soa.services.customer.getsubcasesbycaseid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.customer.SubCases;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}subcases"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "subcases"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/customer", required = true)
    protected SubCases subcases;

    /**
     * Gets the value of the subcases property.
     * 
     * @return
     *     possible object is
     *     {@link SubCases }
     *     
     */
    public SubCases getSubcases() {
        return subcases;
    }

    /**
     * Sets the value of the subcases property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubCases }
     *     
     */
    public void setSubcases(SubCases value) {
        this.subcases = value;
    }

}
