
package _do.com.claro.soa.services.customer.getcustomeractionitemsbycustomerid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.customer.ActionItems;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}actionItems"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actionItems"
})
@XmlRootElement(name = "getCustomerActionItemsByCustomerIDResponse")
public class GetCustomerActionItemsByCustomerIDResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/customer", required = true, nillable = true)
    protected ActionItems actionItems;

    /**
     * Gets the value of the actionItems property.
     * 
     * @return
     *     possible object is
     *     {@link ActionItems }
     *     
     */
    public ActionItems getActionItems() {
        return actionItems;
    }

    /**
     * Sets the value of the actionItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionItems }
     *     
     */
    public void setActionItems(ActionItems value) {
        this.actionItems = value;
    }

}
