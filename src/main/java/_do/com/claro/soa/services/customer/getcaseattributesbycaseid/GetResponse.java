
package _do.com.claro.soa.services.customer.getcaseattributesbycaseid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.customer.FlexibleAttributes;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}flexibleAttributes"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flexibleAttributes"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/customer", required = true, nillable = true)
    protected FlexibleAttributes flexibleAttributes;

    /**
     * Gets the value of the flexibleAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link FlexibleAttributes }
     *     
     */
    public FlexibleAttributes getFlexibleAttributes() {
        return flexibleAttributes;
    }

    /**
     * Sets the value of the flexibleAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexibleAttributes }
     *     
     */
    public void setFlexibleAttributes(FlexibleAttributes value) {
        this.flexibleAttributes = value;
    }

}
