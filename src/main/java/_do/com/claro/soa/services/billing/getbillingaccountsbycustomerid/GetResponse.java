
package _do.com.claro.soa.services.billing.getbillingaccountsbycustomerid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.billing.BillingAccounts;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}billingAccounts"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "billingAccounts"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/billing", required = true, nillable = true)
    protected BillingAccounts billingAccounts;

    /**
     * Gets the value of the billingAccounts property.
     * 
     * @return
     *     possible object is
     *     {@link BillingAccounts }
     *     
     */
    public BillingAccounts getBillingAccounts() {
        return billingAccounts;
    }

    /**
     * Sets the value of the billingAccounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingAccounts }
     *     
     */
    public void setBillingAccounts(BillingAccounts value) {
        this.billingAccounts = value;
    }

}
