
package _do.com.claro.soa.services.com.claro.soa.getsubscriptionrefilltransactions.getsubscriptionrefilltransactions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.billing.SubscriptionRefillTransactions;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}subscriptionRefillTransactions"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "subscriptionRefillTransactions"
})
@XmlRootElement(name = "getsubscriptionRefillTransactionsResponse")
public class GetsubscriptionRefillTransactionsResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/billing", required = true, nillable = true)
    protected SubscriptionRefillTransactions subscriptionRefillTransactions;

    /**
     * Gets the value of the subscriptionRefillTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link SubscriptionRefillTransactions }
     *     
     */
    public SubscriptionRefillTransactions getSubscriptionRefillTransactions() {
        return subscriptionRefillTransactions;
    }

    /**
     * Sets the value of the subscriptionRefillTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubscriptionRefillTransactions }
     *     
     */
    public void setSubscriptionRefillTransactions(SubscriptionRefillTransactions value) {
        this.subscriptionRefillTransactions = value;
    }

}
