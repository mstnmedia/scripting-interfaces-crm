
package _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.3-b02-
 * Generated source version: 2.1
 * 
 */
@WebService(name = "GetCustomersAndContactsByContactNames", targetNamespace = "http://www.claro.com.do/soa/services/customer/GetCustomersAndContactsByContactNames")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    _do.com.claro.soa.model.employee.ObjectFactory.class,
    _do.com.claro.soa.model.generic.ObjectFactory.class,
    _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames.ObjectFactory.class,
    _do.com.claro.soa.model.personidentity.ObjectFactory.class,
    _do.com.claro.soa.model.address.ObjectFactory.class,
    _do.com.claro.soa.model.product.ObjectFactory.class,
    _do.com.claro.soa.services.common.faults.ObjectFactory.class,
    _do.com.claro.soa.model.billing.ObjectFactory.class,
    _do.com.claro.soa.model.customer.ObjectFactory.class
})
public interface GetCustomersAndContactsByContactNames {


    /**
     * 
     *         @Created: SOA-163
     *         Returns a list of customers whose contact name and lastname starts with the input name 
     *         and lastname.
     * 
     *         This is a 'paged' operation due to the excessive amount of data potentially produced from this 
     *         operation. The pageSize is fixed at 10.
     * 
     *         Not all fields are included for the 'customer' and 'contact' elements:
     *           - For 'customer' only customerID, name, phone, fax, email, taxID, customerSegment, customerType, legalName, sicCode,
     *             comercialSector, comercialActivity, website, and contacts.
     *           - For 'contact' only contactID, customerID, names, role, phone, fax, email, address/addressID, birthDate, maritalStatus,
     *             gender, personID, and nationality.
     *           
     *         This service is throttled to limit its long-running processes from retaining too many resources. A regular SOAP 1.1 Fault
     *         with an appropriate message is thrown if a request is rejected when the throttle limit is exceeded.
     *       
     * 
     * @param parameters
     * @return
     *     returns _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactnames.GetResponse
     */
    @WebMethod(action = "http://www.claro.com.do/soa/services/customer/GetCustomersAndContactsByContactNames/get")
    @WebResult(name = "getResponse", targetNamespace = "http://www.claro.com.do/soa/services/customer/GetCustomersAndContactsByContactNames", partName = "parameters")
    public GetResponse get(
        @WebParam(name = "getRequest", targetNamespace = "http://www.claro.com.do/soa/services/customer/GetCustomersAndContactsByContactNames", partName = "parameters")
        GetRequest parameters);

}
