
package _do.com.claro.soa.services.customer.getsubcasesbycaseid;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.3-b02-
 * Generated source version: 2.1
 * 
 */
@WebService(name = "GetSubCasesByCaseID", targetNamespace = "http://www.claro.com.do/soa/services/customer/GetSubCasesByCaseID")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    _do.com.claro.soa.model.product.ObjectFactory.class,
    _do.com.claro.soa.services.customer.getsubcasesbycaseid.ObjectFactory.class,
    _do.com.claro.soa.model.employee.ObjectFactory.class,
    _do.com.claro.soa.model.generic.ObjectFactory.class,
    _do.com.claro.soa.model.billing.ObjectFactory.class,
    _do.com.claro.soa.model.address.ObjectFactory.class,
    _do.com.claro.soa.model.personidentity.ObjectFactory.class,
    _do.com.claro.soa.model.customer.ObjectFactory.class
})
public interface GetSubCasesByCaseID {


    /**
     * 
     * @param parameters
     * @return
     *     returns _do.com.claro.soa.services.customer.getsubcasesbycaseid.GetResponse
     */
    @WebMethod(action = "http://www.claro.com.do/soa/services/customer/GetSubCasesByCaseID/get")
    @WebResult(name = "getResponse", targetNamespace = "http://www.claro.com.do/soa/services/customer/GetSubCasesByCaseID", partName = "parameters")
    public GetResponse get(
        @WebParam(name = "getRequest", targetNamespace = "http://www.claro.com.do/soa/services/customer/GetSubCasesByCaseID", partName = "parameters")
        GetRequest parameters);

}
