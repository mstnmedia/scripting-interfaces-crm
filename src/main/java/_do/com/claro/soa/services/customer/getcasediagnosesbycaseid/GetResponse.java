
package _do.com.claro.soa.services.customer.getcasediagnosesbycaseid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.customer.Diagnoses;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}diagnoses"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "diagnoses"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/customer", required = true)
    protected Diagnoses diagnoses;

    /**
     * Gets the value of the diagnoses property.
     * 
     * @return
     *     possible object is
     *     {@link Diagnoses }
     *     
     */
    public Diagnoses getDiagnoses() {
        return diagnoses;
    }

    /**
     * Sets the value of the diagnoses property.
     * 
     * @param value
     *     allowed object is
     *     {@link Diagnoses }
     *     
     */
    public void setDiagnoses(Diagnoses value) {
        this.diagnoses = value;
    }

}
