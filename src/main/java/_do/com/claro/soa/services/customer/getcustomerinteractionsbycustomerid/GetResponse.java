
package _do.com.claro.soa.services.customer.getcustomerinteractionsbycustomerid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.customer.Interactions;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}interactions"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "interactions"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/customer", required = true, nillable = true)
    protected Interactions interactions;

    /**
     * Gets the value of the interactions property.
     * 
     * @return
     *     possible object is
     *     {@link Interactions }
     *     
     */
    public Interactions getInteractions() {
        return interactions;
    }

    /**
     * Sets the value of the interactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Interactions }
     *     
     */
    public void setInteractions(Interactions value) {
        this.interactions = value;
    }

}
