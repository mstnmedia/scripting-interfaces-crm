
package _do.com.claro.soa.services.billing.getfixedpostpaidrollover;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Result">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="includedMinutes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="Pockets">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Pocket" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="type" type="{http://www.claro.com.do/soa/services/billing/GetFixedPostpaidRollover}PocketType"/>
 *                                       &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                                       &lt;element name="usage">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="slm" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="slm_ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="slm_ldn_cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="slm_ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "result"
})
public class GetResponse {

    @XmlElement(name = "Result", required = true)
    protected GetResponse.Result result;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link GetResponse.Result }
     *     
     */
    public GetResponse.Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetResponse.Result }
     *     
     */
    public void setResult(GetResponse.Result value) {
        this.result = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="includedMinutes" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="Pockets">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Pocket" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="type" type="{http://www.claro.com.do/soa/services/billing/GetFixedPostpaidRollover}PocketType"/>
     *                             &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *                             &lt;element name="usage">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="slm" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="slm_ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="slm_ldn_cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="slm_ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "includedMinutes",
        "pockets"
    })
    public static class Result {

        @XmlElement(required = true, type = Integer.class, nillable = true)
        protected Integer includedMinutes;
        @XmlElement(name = "Pockets", required = true, nillable = true)
        protected GetResponse.Result.Pockets pockets;

        /**
         * Gets the value of the includedMinutes property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getIncludedMinutes() {
            return includedMinutes;
        }

        /**
         * Sets the value of the includedMinutes property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setIncludedMinutes(Integer value) {
            this.includedMinutes = value;
        }

        /**
         * Gets the value of the pockets property.
         * 
         * @return
         *     possible object is
         *     {@link GetResponse.Result.Pockets }
         *     
         */
        public GetResponse.Result.Pockets getPockets() {
            return pockets;
        }

        /**
         * Sets the value of the pockets property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetResponse.Result.Pockets }
         *     
         */
        public void setPockets(GetResponse.Result.Pockets value) {
            this.pockets = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Pocket" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="type" type="{http://www.claro.com.do/soa/services/billing/GetFixedPostpaidRollover}PocketType"/>
         *                   &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
         *                   &lt;element name="usage">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="slm" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="slm_ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="slm_ldn_cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="slm_ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pocket"
        })
        public static class Pockets {

            @XmlElement(name = "Pocket", required = true, nillable = true)
            protected List<GetResponse.Result.Pockets.Pocket> pocket;

            /**
             * Gets the value of the pocket property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the pocket property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPocket().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link GetResponse.Result.Pockets.Pocket }
             * 
             * 
             */
            public List<GetResponse.Result.Pockets.Pocket> getPocket() {
                if (pocket == null) {
                    pocket = new ArrayList<GetResponse.Result.Pockets.Pocket>();
                }
                return this.pocket;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="type" type="{http://www.claro.com.do/soa/services/billing/GetFixedPostpaidRollover}PocketType"/>
             *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
             *         &lt;element name="usage">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="slm" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="slm_ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="slm_ldn_cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="slm_ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "type",
                "expirationDate",
                "usage"
            })
            public static class Pocket {

                @XmlElement(required = true)
                protected PocketType type;
                @XmlElement(required = true, nillable = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar expirationDate;
                @XmlElement(required = true, nillable = true)
                protected GetResponse.Result.Pockets.Pocket.Usage usage;

                /**
                 * Gets the value of the type property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link PocketType }
                 *     
                 */
                public PocketType getType() {
                    return type;
                }

                /**
                 * Sets the value of the type property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PocketType }
                 *     
                 */
                public void setType(PocketType value) {
                    this.type = value;
                }

                /**
                 * Gets the value of the expirationDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getExpirationDate() {
                    return expirationDate;
                }

                /**
                 * Sets the value of the expirationDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setExpirationDate(XMLGregorianCalendar value) {
                    this.expirationDate = value;
                }

                /**
                 * Gets the value of the usage property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link GetResponse.Result.Pockets.Pocket.Usage }
                 *     
                 */
                public GetResponse.Result.Pockets.Pocket.Usage getUsage() {
                    return usage;
                }

                /**
                 * Sets the value of the usage property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link GetResponse.Result.Pockets.Pocket.Usage }
                 *     
                 */
                public void setUsage(GetResponse.Result.Pockets.Pocket.Usage value) {
                    this.usage = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="slm" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="slm_ldn" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="slm_ldn_cpp" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="slm_ldn_ldi" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "ldn",
                    "cpp",
                    "slm",
                    "ldi",
                    "ldnLdi",
                    "slmLdn",
                    "slmLdnCpp",
                    "slmLdnLdi"
                })
                public static class Usage {

                    protected int ldn;
                    protected int cpp;
                    protected int slm;
                    protected int ldi;
                    @XmlElement(name = "ldn_ldi")
                    protected int ldnLdi;
                    @XmlElement(name = "slm_ldn")
                    protected int slmLdn;
                    @XmlElement(name = "slm_ldn_cpp")
                    protected int slmLdnCpp;
                    @XmlElement(name = "slm_ldn_ldi")
                    protected int slmLdnLdi;

                    /**
                     * Gets the value of the ldn property.
                     * 
                     */
                    public int getLdn() {
                        return ldn;
                    }

                    /**
                     * Sets the value of the ldn property.
                     * 
                     */
                    public void setLdn(int value) {
                        this.ldn = value;
                    }

                    /**
                     * Gets the value of the cpp property.
                     * 
                     */
                    public int getCpp() {
                        return cpp;
                    }

                    /**
                     * Sets the value of the cpp property.
                     * 
                     */
                    public void setCpp(int value) {
                        this.cpp = value;
                    }

                    /**
                     * Gets the value of the slm property.
                     * 
                     */
                    public int getSlm() {
                        return slm;
                    }

                    /**
                     * Sets the value of the slm property.
                     * 
                     */
                    public void setSlm(int value) {
                        this.slm = value;
                    }

                    /**
                     * Gets the value of the ldi property.
                     * 
                     */
                    public int getLdi() {
                        return ldi;
                    }

                    /**
                     * Sets the value of the ldi property.
                     * 
                     */
                    public void setLdi(int value) {
                        this.ldi = value;
                    }

                    /**
                     * Gets the value of the ldnLdi property.
                     * 
                     */
                    public int getLdnLdi() {
                        return ldnLdi;
                    }

                    /**
                     * Sets the value of the ldnLdi property.
                     * 
                     */
                    public void setLdnLdi(int value) {
                        this.ldnLdi = value;
                    }

                    /**
                     * Gets the value of the slmLdn property.
                     * 
                     */
                    public int getSlmLdn() {
                        return slmLdn;
                    }

                    /**
                     * Sets the value of the slmLdn property.
                     * 
                     */
                    public void setSlmLdn(int value) {
                        this.slmLdn = value;
                    }

                    /**
                     * Gets the value of the slmLdnCpp property.
                     * 
                     */
                    public int getSlmLdnCpp() {
                        return slmLdnCpp;
                    }

                    /**
                     * Sets the value of the slmLdnCpp property.
                     * 
                     */
                    public void setSlmLdnCpp(int value) {
                        this.slmLdnCpp = value;
                    }

                    /**
                     * Gets the value of the slmLdnLdi property.
                     * 
                     */
                    public int getSlmLdnLdi() {
                        return slmLdnLdi;
                    }

                    /**
                     * Sets the value of the slmLdnLdi property.
                     * 
                     */
                    public void setSlmLdnLdi(int value) {
                        this.slmLdnLdi = value;
                    }

                }

            }

        }

    }

}
