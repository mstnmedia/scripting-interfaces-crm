package _do.com.claro.soa.services.customer.getcustomersandcontactsbytaxid;

import com.mstn.scripting.core.models.InterfaceConfigs;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.3-b02- Generated
 * source version: 2.1
 *
 */
@WebServiceClient(name = "GetCustomersAndContactsByTaxID", targetNamespace = "http://www.claro.com.do/soa/services/customer/GetCustomersAndContactsByTaxID", wsdlLocation = "http://SOADEVENV1:6010/services/customer/GetCustomersAndContactsByTaxID?WSDL")
public class GetCustomersAndContactsByTaxID_Service extends Service {

	private final static URL GETCUSTOMERSANDCONTACTSBYTAXID_WSDL_LOCATION;
	private final static Logger LOGGER = Logger.getLogger(_do.com.claro.soa.services.customer.getcustomersandcontactsbytaxid.GetCustomersAndContactsByTaxID_Service.class.getName());

	static public final String CONFIG_KEY = "crm_getcustomersandcontactsbytaxid";

	static public GetCustomersAndContactsByTaxID getInstance() {
		if (!InterfaceConfigs.wsClients.containsKey(CONFIG_KEY)) {
			GetCustomersAndContactsByTaxID port = new GetCustomersAndContactsByTaxID_Service().getGetCustomersAndContactsByTaxIDSOAP();
			InterfaceConfigs.wsClients.put(CONFIG_KEY, port);
		}
		return (GetCustomersAndContactsByTaxID) InterfaceConfigs.wsClients.get(CONFIG_KEY);
	}

	static {
		URL url = null;
		String configURL = InterfaceConfigs.get(CONFIG_KEY);
		try {
			URL baseUrl;
			baseUrl = _do.com.claro.soa.services.customer.getcustomersandcontactsbytaxid.GetCustomersAndContactsByTaxID_Service.class.getResource(".");
			url = new URL(baseUrl, configURL);
		} catch (MalformedURLException e) {
			LOGGER.log(Level.WARNING, "Failed to create URL for the wsdl Location: '{0}', retrying as a local file", configURL);
			LOGGER.warning(e.getMessage());
		}
		GETCUSTOMERSANDCONTACTSBYTAXID_WSDL_LOCATION = url;
	}

	public GetCustomersAndContactsByTaxID_Service(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public GetCustomersAndContactsByTaxID_Service() {
		super(GETCUSTOMERSANDCONTACTSBYTAXID_WSDL_LOCATION, new QName("http://www.claro.com.do/soa/services/customer/GetCustomersAndContactsByTaxID", "GetCustomersAndContactsByTaxID"));
	}

	/**
	 *
	 * @return returns GetCustomersAndContactsByTaxID
	 */
	@WebEndpoint(name = "GetCustomersAndContactsByTaxIDSOAP")
	public GetCustomersAndContactsByTaxID getGetCustomersAndContactsByTaxIDSOAP() {
		return super.getPort(new QName("http://www.claro.com.do/soa/services/customer/GetCustomersAndContactsByTaxID", "GetCustomersAndContactsByTaxIDSOAP"), GetCustomersAndContactsByTaxID.class);
	}

	/**
	 *
	 * @param features A list of {@link javax.xml.ws.WebServiceFeature} to
	 * configure on the proxy. Supported features not in the
	 * <code>features</code> parameter will have their default values.
	 * @return returns GetCustomersAndContactsByTaxID
	 */
	@WebEndpoint(name = "GetCustomersAndContactsByTaxIDSOAP")
	public GetCustomersAndContactsByTaxID getGetCustomersAndContactsByTaxIDSOAP(WebServiceFeature... features) {
		return super.getPort(new QName("http://www.claro.com.do/soa/services/customer/GetCustomersAndContactsByTaxID", "GetCustomersAndContactsByTaxIDSOAP"), GetCustomersAndContactsByTaxID.class, features);
	}

}
