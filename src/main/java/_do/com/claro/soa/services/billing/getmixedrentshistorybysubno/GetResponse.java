
package _do.com.claro.soa.services.billing.getmixedrentshistorybysubno;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.generic.Money;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MixedRentList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MixedRent" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="transactionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                             &lt;element name="planId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="planDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="voiceAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *                             &lt;element name="bonusAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *                             &lt;element name="smsAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *                             &lt;element name="mmsAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *                             &lt;element name="dataAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *                             &lt;element name="voiceMinPackageAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *                             &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mixedRentList"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(name = "MixedRentList", required = true, nillable = true)
    protected GetResponse.MixedRentList mixedRentList;

    /**
     * Gets the value of the mixedRentList property.
     * 
     * @return
     *     possible object is
     *     {@link GetResponse.MixedRentList }
     *     
     */
    public GetResponse.MixedRentList getMixedRentList() {
        return mixedRentList;
    }

    /**
     * Sets the value of the mixedRentList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetResponse.MixedRentList }
     *     
     */
    public void setMixedRentList(GetResponse.MixedRentList value) {
        this.mixedRentList = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MixedRent" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="transactionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *                   &lt;element name="planId" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="planDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="voiceAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
     *                   &lt;element name="bonusAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
     *                   &lt;element name="smsAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
     *                   &lt;element name="mmsAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
     *                   &lt;element name="dataAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
     *                   &lt;element name="voiceMinPackageAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
     *                   &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mixedRent"
    })
    public static class MixedRentList {

        @XmlElement(name = "MixedRent", required = true)
        protected List<GetResponse.MixedRentList.MixedRent> mixedRent;

        /**
         * Gets the value of the mixedRent property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the mixedRent property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMixedRent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetResponse.MixedRentList.MixedRent }
         * 
         * 
         */
        public List<GetResponse.MixedRentList.MixedRent> getMixedRent() {
            if (mixedRent == null) {
                mixedRent = new ArrayList<GetResponse.MixedRentList.MixedRent>();
            }
            return this.mixedRent;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="transactionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
         *         &lt;element name="planId" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="planDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="voiceAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
         *         &lt;element name="bonusAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
         *         &lt;element name="smsAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
         *         &lt;element name="mmsAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
         *         &lt;element name="dataAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
         *         &lt;element name="voiceMinPackageAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
         *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "transactionDate",
            "planId",
            "planDescription",
            "voiceAmount",
            "bonusAmount",
            "smsAmount",
            "mmsAmount",
            "dataAmount",
            "voiceMinPackageAmount",
            "user",
            "status"
        })
        public static class MixedRent {

            @XmlElement(required = true)
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar transactionDate;
            protected int planId;
            @XmlElement(required = true)
            protected String planDescription;
            @XmlElement(required = true)
            protected Money voiceAmount;
            @XmlElement(required = true)
            protected Money bonusAmount;
            @XmlElement(required = true)
            protected Money smsAmount;
            @XmlElement(required = true)
            protected Money mmsAmount;
            @XmlElement(required = true)
            protected Money dataAmount;
            @XmlElement(required = true)
            protected Money voiceMinPackageAmount;
            @XmlElement(required = true)
            protected String user;
            @XmlElement(required = true)
            protected String status;

            /**
             * Gets the value of the transactionDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getTransactionDate() {
                return transactionDate;
            }

            /**
             * Sets the value of the transactionDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setTransactionDate(XMLGregorianCalendar value) {
                this.transactionDate = value;
            }

            /**
             * Gets the value of the planId property.
             * 
             */
            public int getPlanId() {
                return planId;
            }

            /**
             * Sets the value of the planId property.
             * 
             */
            public void setPlanId(int value) {
                this.planId = value;
            }

            /**
             * Gets the value of the planDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlanDescription() {
                return planDescription;
            }

            /**
             * Sets the value of the planDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlanDescription(String value) {
                this.planDescription = value;
            }

            /**
             * Gets the value of the voiceAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Money }
             *     
             */
            public Money getVoiceAmount() {
                return voiceAmount;
            }

            /**
             * Sets the value of the voiceAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Money }
             *     
             */
            public void setVoiceAmount(Money value) {
                this.voiceAmount = value;
            }

            /**
             * Gets the value of the bonusAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Money }
             *     
             */
            public Money getBonusAmount() {
                return bonusAmount;
            }

            /**
             * Sets the value of the bonusAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Money }
             *     
             */
            public void setBonusAmount(Money value) {
                this.bonusAmount = value;
            }

            /**
             * Gets the value of the smsAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Money }
             *     
             */
            public Money getSmsAmount() {
                return smsAmount;
            }

            /**
             * Sets the value of the smsAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Money }
             *     
             */
            public void setSmsAmount(Money value) {
                this.smsAmount = value;
            }

            /**
             * Gets the value of the mmsAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Money }
             *     
             */
            public Money getMmsAmount() {
                return mmsAmount;
            }

            /**
             * Sets the value of the mmsAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Money }
             *     
             */
            public void setMmsAmount(Money value) {
                this.mmsAmount = value;
            }

            /**
             * Gets the value of the dataAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Money }
             *     
             */
            public Money getDataAmount() {
                return dataAmount;
            }

            /**
             * Sets the value of the dataAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Money }
             *     
             */
            public void setDataAmount(Money value) {
                this.dataAmount = value;
            }

            /**
             * Gets the value of the voiceMinPackageAmount property.
             * 
             * @return
             *     possible object is
             *     {@link Money }
             *     
             */
            public Money getVoiceMinPackageAmount() {
                return voiceMinPackageAmount;
            }

            /**
             * Sets the value of the voiceMinPackageAmount property.
             * 
             * @param value
             *     allowed object is
             *     {@link Money }
             *     
             */
            public void setVoiceMinPackageAmount(Money value) {
                this.voiceMinPackageAmount = value;
            }

            /**
             * Gets the value of the user property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUser() {
                return user;
            }

            /**
             * Sets the value of the user property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUser(String value) {
                this.user = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

        }

    }

}
