
package _do.com.claro.soa.services.billing.getbillshistorybybillingaccountno;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.billing.Bills;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}bills"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bills"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/billing", required = true, nillable = true)
    protected Bills bills;

    /**
     * Gets the value of the bills property.
     * 
     * @return
     *     possible object is
     *     {@link Bills }
     *     
     */
    public Bills getBills() {
        return bills;
    }

    /**
     * Sets the value of the bills property.
     * 
     * @param value
     *     allowed object is
     *     {@link Bills }
     *     
     */
    public void setBills(Bills value) {
        this.bills = value;
    }

}
