
package _do.com.claro.soa.services.ordering.getorderactionsbycustomerid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.ordering.OrderActions;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}orderActions"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderActions"
})
@XmlRootElement(name = "getResponse")
public class GetResponse {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/ordering", required = true, nillable = true)
    protected OrderActions orderActions;

    /**
     * Gets the value of the orderActions property.
     * 
     * @return
     *     possible object is
     *     {@link OrderActions }
     *     
     */
    public OrderActions getOrderActions() {
        return orderActions;
    }

    /**
     * Sets the value of the orderActions property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderActions }
     *     
     */
    public void setOrderActions(OrderActions value) {
        this.orderActions = value;
    }

}
