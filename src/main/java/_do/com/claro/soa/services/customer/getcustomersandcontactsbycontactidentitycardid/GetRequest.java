
package _do.com.claro.soa.services.customer.getcustomersandcontactsbycontactidentitycardid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.personidentity.IdentityCardID;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}identityCardID"/>
 *         &lt;element name="pageNum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "identityCardID",
    "pageNum"
})
@XmlRootElement(name = "getRequest")
public class GetRequest {

    @XmlElement(namespace = "http://www.claro.com.do/soa/model/personidentity", required = true)
    protected IdentityCardID identityCardID;
    @XmlElement(namespace = "")
    protected int pageNum;

    /**
     * 
     * 									A identityCardID matching any of the customer's contacts.
     * 								
     * 
     * @return
     *     possible object is
     *     {@link IdentityCardID }
     *     
     */
    public IdentityCardID getIdentityCardID() {
        return identityCardID;
    }

    /**
     * Sets the value of the identityCardID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentityCardID }
     *     
     */
    public void setIdentityCardID(IdentityCardID value) {
        this.identityCardID = value;
    }

    /**
     * Gets the value of the pageNum property.
     * 
     */
    public int getPageNum() {
        return pageNum;
    }

    /**
     * Sets the value of the pageNum property.
     * 
     */
    public void setPageNum(int value) {
        this.pageNum = value;
    }

}
