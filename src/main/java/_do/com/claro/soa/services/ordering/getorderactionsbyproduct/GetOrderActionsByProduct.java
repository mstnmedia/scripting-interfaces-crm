
package _do.com.claro.soa.services.ordering.getorderactionsbyproduct;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * 
 *       @Created: SOA-140
 *       Returns a list of canonical orderActions associated with the product identified by the given
 *       subscriberNo, customerID and productCode. If more than one instance of a product exists with the
 *       same criteria then the orderActions of the most recent one are returned.
 * 
 *       Note: due to performance reasons, the Product.address element in the productSnapshot will only 
 *       contain the service's addressID (if any). Users should query the 'GetAddressByAddressID' service
 *       with this value to obtain the address details.
 * 
 *       This is a paged operation due to the potentially large number of order actions returned. The pageSize
 *       is fixed at 10.
 * 
 *       This is a throttled service: concurrency is limited to 10 requests. A SOAP Fault with a generic
 *       message will be thrown indicating that the request was rejected for this reason.
 *     
 * 
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.3-b02-
 * Generated source version: 2.1
 * 
 */
@WebService(name = "GetOrderActionsByProduct", targetNamespace = "http://www.claro.com.do/soa/services/ordering/GetOrderActionsByProduct")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    _do.com.claro.soa.model.customer.ObjectFactory.class,
    _do.com.claro.soa.model.address.ObjectFactory.class,
    _do.com.claro.soa.model.billing.ObjectFactory.class,
    _do.com.claro.soa.model.ordering.ObjectFactory.class,
    _do.com.claro.soa.model.product.ObjectFactory.class,
    _do.com.claro.soa.model.personidentity.ObjectFactory.class,
    _do.com.claro.soa.model.generic.ObjectFactory.class,
    _do.com.claro.soa.services.ordering.getorderactionsbyproduct.ObjectFactory.class,
    _do.com.claro.soa.model.employee.ObjectFactory.class
})
public interface GetOrderActionsByProduct {


    /**
     * 
     * @param parameters
     * @return
     *     returns _do.com.claro.soa.services.ordering.getorderactionsbyproduct.GetOrderActionsByProductResponse
     */
    @WebMethod(action = "http://www.claro.com.do/soa/services/ordering/GetOrderActionsByProduct/get")
    @WebResult(name = "getOrderActionsByProductResponse", targetNamespace = "http://www.claro.com.do/soa/services/ordering/GetOrderActionsByProduct", partName = "parameters")
    public GetOrderActionsByProductResponse get(
        @WebParam(name = "getOrderActionsByProductRequest", targetNamespace = "http://www.claro.com.do/soa/services/ordering/GetOrderActionsByProduct", partName = "parameters")
        GetOrderActionsByProductRequest parameters);

}
