
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderActionStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderActionStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AMENDED"/>
 *     &lt;enumeration value="BEING_AMENDED"/>
 *     &lt;enumeration value="CANCELLED"/>
 *     &lt;enumeration value="DISCONTINUED"/>
 *     &lt;enumeration value="DELIVERY"/>
 *     &lt;enumeration value="DONE"/>
 *     &lt;enumeration value="FICTITIOUS"/>
 *     &lt;enumeration value="FUTURE"/>
 *     &lt;enumeration value="INITIAL"/>
 *     &lt;enumeration value="NEGOTIATION"/>
 *     &lt;enumeration value="COMPLETION"/>
 *     &lt;enumeration value="ON_HOLD"/>
 *     &lt;enumeration value="REJECT"/>
 *     &lt;enumeration value="TO_BE_CANCELLED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderActionStatus")
@XmlEnum
public enum OrderActionStatus {


    /**
     * 
     *           The order action was amended by another OA.
     *           This is a final status - the order action has been finalized and in effect replaced by the 
     *           OA that performed the amend.
     *           See "BEING_AMMENDED".
     *         
     * 
     */
    AMENDED,

    /**
     * 
     *           The order action is in the process of being amended (see "AMENDED").
     *           An order action can be amended if the following conditions are met:
     *             - The OA has been delivered to the backend systems
     *             - The backend systems indicate that the OA has not yet reached the Point-Of-No-Return.
     *           An OA that is being amended is replaced by a new child OA.
     *         
     * 
     */
    BEING_AMENDED,

    /**
     * 
     *           The order action has been cancelled and its cancellation notified to the backend systems 
     *           (if the OA was previously delivered).
     *           This is a final status.
     *           See "TO_BE_CANCELLED".
     *         
     * 
     */
    CANCELLED,

    /**
     * 
     *           The order action has been cancelled and its cancellation was NOT notified to the backend systems 
     *           regardless of whether it previously reached DELIVERY or not.
     *         
     * 
     */
    DISCONTINUED,

    /**
     * 
     *           The order action has been - or is in process of being - delivered to the backend systems.
     *         
     * 
     */
    DELIVERY,

    /**
     * 
     *           The order action has been completed.
     *           This is a final status.
     *         
     * 
     */
    DONE,

    /**
     * 
     *           Not used.
     *         
     * 
     */
    FICTITIOUS,

    /**
     * 
     *           Not used.
     *         
     * 
     */
    FUTURE,

    /**
     * 
     *           The order action has just been created ("initialized") but not yet configured.
     *         
     * 
     */
    INITIAL,

    /**
     * 
     *           The order action is currently being configured.
     *         
     * 
     */
    NEGOTIATION,

    /**
     * 
     *           The order action has completed the delivery phase and is currently performing final steps in its
     *           lifecycle which include completing the "notify billing system" step and others.
     *         
     * 
     */
    COMPLETION,

    /**
     * 
     *           The order action's flow is currently paused while it waits for another OA to reach some milestone.
     *         
     * 
     */
    ON_HOLD,

    /**
     * 
     *           Not used.
     *         
     * 
     */
    REJECT,

    /**
     * 
     *           The order action is currently being cancelled.
     *           A cancelled OA may or may not be replaced by a child OA depending on whether it has been delivered (see "DELIVERY") 
     *           or not. If it had reached DELIVERY then a child OA is created and sent to the backend systems carrying the "cancel 
     *           this order" notification. This is called the "two-step cancel process".
     *           The two-step cancel process is executed only when:
     *             - The OA has reached DELIVERY
     *             - The backend systems indicate that the OA has not reached the Point-Of-No-Return.
     *             If the OA has reached DELIVERY but has passed the PoNR then it cannot be cancelled. Options are then to either
     *             discontinue (see "DISCONTINUED") or to complete it (see "COMPLETION").
     *             If the OA has not reached DELIVERY then a normal "one-step cancel" is performed where the OA is directly cancelled
     *             without the creation of a child OA.
     *           See "CANCELLED".
     *         
     * 
     */
    TO_BE_CANCELLED;

    public String value() {
        return name();
    }

    public static OrderActionStatus fromValue(String v) {
        return valueOf(v);
    }

}
