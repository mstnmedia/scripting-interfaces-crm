
package _do.com.claro.soa.model.billing;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *         @Created SOA-235
 *         Represents the collectios automatic treatment steps data.
 *         AutomaticTreatmentStep includes the following fields:
 *         -stepNumber: represents the step number (EX 1,2,3, etc)
 *         -stepDescription: Represents a description of the current step
 *         -stepDate: represents the date on which the step was executed
 *       
 * 
 * <p>Java class for AutomaticTreatmentStep complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutomaticTreatmentStep">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stepNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="stepDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stepDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutomaticTreatmentStep", propOrder = {
    "stepNumber",
    "stepDescription",
    "stepDate"
})
public class AutomaticTreatmentStep {

    @XmlElement(required = true)
    protected BigInteger stepNumber;
    @XmlElement(required = true)
    protected String stepDescription;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar stepDate;

    /**
     * Gets the value of the stepNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStepNumber() {
        return stepNumber;
    }

    /**
     * Sets the value of the stepNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStepNumber(BigInteger value) {
        this.stepNumber = value;
    }

    /**
     * Gets the value of the stepDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStepDescription() {
        return stepDescription;
    }

    /**
     * Sets the value of the stepDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStepDescription(String value) {
        this.stepDescription = value;
    }

    /**
     * Gets the value of the stepDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStepDate() {
        return stepDate;
    }

    /**
     * Sets the value of the stepDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStepDate(XMLGregorianCalendar value) {
        this.stepDate = value;
    }

}
