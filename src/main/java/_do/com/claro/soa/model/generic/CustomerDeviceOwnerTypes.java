
package _do.com.claro.soa.model.generic;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerDeviceOwnerTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerDeviceOwnerTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DISPONIBLE"/>
 *     &lt;enumeration value="CAMBIAZO_FINANCIADO"/>
 *     &lt;enumeration value="INVEN_REGISTRO_SAL"/>
 *     &lt;enumeration value="DEMO"/>
 *     &lt;enumeration value="EMPLEADO"/>
 *     &lt;enumeration value="VENDIDO_AGENTE"/>
 *     &lt;enumeration value="VENDIDO_INVEN"/>
 *     &lt;enumeration value="PRESTAMO"/>
 *     &lt;enumeration value="TITULAR"/>
 *     &lt;enumeration value="ALQUILER"/>
 *     &lt;enumeration value="ARRENDAR"/>
 *     &lt;enumeration value="CLARO"/>
 *     &lt;enumeration value="BORRADO"/>
 *     &lt;enumeration value="NO_DISPONIBLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CustomerDeviceOwnerTypes")
@XmlEnum
public enum CustomerDeviceOwnerTypes {

    DISPONIBLE,
    CAMBIAZO_FINANCIADO,
    INVEN_REGISTRO_SAL,
    DEMO,
    EMPLEADO,
    VENDIDO_AGENTE,
    VENDIDO_INVEN,
    PRESTAMO,
    TITULAR,
    ALQUILER,
    ARRENDAR,
    CLARO,
    BORRADO,
    NO_DISPONIBLE;

    public String value() {
        return name();
    }

    public static CustomerDeviceOwnerTypes fromValue(String v) {
        return valueOf(v);
    }

}
