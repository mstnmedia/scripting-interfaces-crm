
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REGULAR"/>
 *     &lt;enumeration value="RETROACTIVE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderMode")
@XmlEnum
public enum OrderMode {


    /**
     * 
     *           The order's order actions will follow the regular flow: NEGOTIATION -> DELIVERY -> COMPLETION.
     *         
     * 
     */
    REGULAR,

    /**
     * 
     *           The order's order actions are not intended to be delivered to the backend systems.
     *           This kind of orders are created in order to align the OMS and billing systems with the 
     *           service status at the backend systems.
     *           The order's order actions will follow this flow instead: NEGOTIATION -> COMPLETION.
     *         
     * 
     */
    RETROACTIVE;

    public String value() {
        return name();
    }

    public static OrderMode fromValue(String v) {
        return valueOf(v);
    }

}
