
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *         @Created: SOA-140
 *         An order groups several orderActions under it.
 * 
 *         @Modified: SOA-239:
 *         Added the 'salesChannel' field. The 'reason' field was not added 
 *         because only orderActions have them in OMS.
 *       
 * 
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}orderID"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}creator"/>
 *         &lt;element name="effectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="serviceRequiredDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="mode" type="{http://www.claro.com.do/soa/model/ordering}OrderMode"/>
 *         &lt;element name="status" type="{http://www.claro.com.do/soa/model/ordering}OrderStatus"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}salesChannel"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderID",
    "creator",
    "effectiveDate",
    "serviceRequiredDate",
    "mode",
    "status",
    "salesChannel"
})
public class Order {

    @XmlElement(required = true)
    protected OrderID orderID;
    @XmlElement(required = true)
    protected Creator creator;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar serviceRequiredDate;
    @XmlElement(required = true)
    protected OrderMode mode;
    @XmlElement(required = true)
    protected OrderStatus status;
    @XmlElement(required = true, nillable = true)
    protected SalesChannel salesChannel;

    /**
     * Gets the value of the orderID property.
     * 
     * @return
     *     possible object is
     *     {@link OrderID }
     *     
     */
    public OrderID getOrderID() {
        return orderID;
    }

    /**
     * Sets the value of the orderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderID }
     *     
     */
    public void setOrderID(OrderID value) {
        this.orderID = value;
    }

    /**
     * Gets the value of the creator property.
     * 
     * @return
     *     possible object is
     *     {@link Creator }
     *     
     */
    public Creator getCreator() {
        return creator;
    }

    /**
     * Sets the value of the creator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Creator }
     *     
     */
    public void setCreator(Creator value) {
        this.creator = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the serviceRequiredDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServiceRequiredDate() {
        return serviceRequiredDate;
    }

    /**
     * Sets the value of the serviceRequiredDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServiceRequiredDate(XMLGregorianCalendar value) {
        this.serviceRequiredDate = value;
    }

    /**
     * Gets the value of the mode property.
     * 
     * @return
     *     possible object is
     *     {@link OrderMode }
     *     
     */
    public OrderMode getMode() {
        return mode;
    }

    /**
     * Sets the value of the mode property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderMode }
     *     
     */
    public void setMode(OrderMode value) {
        this.mode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link OrderStatus }
     *     
     */
    public OrderStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderStatus }
     *     
     */
    public void setStatus(OrderStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the salesChannel property.
     * 
     * @return
     *     possible object is
     *     {@link SalesChannel }
     *     
     */
    public SalesChannel getSalesChannel() {
        return salesChannel;
    }

    /**
     * Sets the value of the salesChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesChannel }
     *     
     */
    public void setSalesChannel(SalesChannel value) {
        this.salesChannel = value;
    }

}
