
package _do.com.claro.soa.model.billing;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created: SOA-236
 *       
 * 
 * <p>Java class for PendingChargesAndCredits complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PendingChargesAndCredits">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}pendingChargeAndCredit" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PendingChargesAndCredits", propOrder = {
    "pendingChargeAndCredit"
})
public class PendingChargesAndCredits {

    @XmlElement(required = true, nillable = true)
    protected List<PendingChargeAndCredit> pendingChargeAndCredit;

    /**
     * Gets the value of the pendingChargeAndCredit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pendingChargeAndCredit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPendingChargeAndCredit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PendingChargeAndCredit }
     * 
     * 
     */
    public List<PendingChargeAndCredit> getPendingChargeAndCredit() {
        if (pendingChargeAndCredit == null) {
            pendingChargeAndCredit = new ArrayList<PendingChargeAndCredit>();
        }
        return this.pendingChargeAndCredit;
    }

}
