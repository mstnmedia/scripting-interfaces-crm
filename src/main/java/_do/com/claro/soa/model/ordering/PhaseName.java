
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PhaseName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PhaseName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NEGOTIATION"/>
 *     &lt;enumeration value="DELIVERY"/>
 *     &lt;enumeration value="REQUEST_DELIVERY_SENT"/>
 *     &lt;enumeration value="DELIVERY_IN_PROGRESS"/>
 *     &lt;enumeration value="DELIVERY_NOT_OK"/>
 *     &lt;enumeration value="DELIVERY_OK"/>
 *     &lt;enumeration value="COMPLETION"/>
 *     &lt;enumeration value="CLOSED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PhaseName")
@XmlEnum
public enum PhaseName {


    /**
     * 
     *           The order action is still being configured by the CSR and has not yet been committed.
     *         
     * 
     */
    NEGOTIATION,

    /**
     * 
     *           The order action has been committed and is currently in DELIVERY.
     *           The OA has not yet been sent to the OSS.
     *         
     * 
     */
    DELIVERY,

    /**
     * 
     *           The order action reached DELIVERY and has been sent to the OSS.
     *           The OA is currently awaiting response from the backend systems.
     *         
     * 
     */
    REQUEST_DELIVERY_SENT,

    /**
     * 
     *           The order action is in DELIVERY and has been sent to the OSS. 
     *           It has received partial responses from the backend systems and is still waiting for a final response.
     *         
     * 
     */
    DELIVERY_IN_PROGRESS,

    /**
     * 
     *           The order action is still in DELIVERY and has received an error response from the OSS indicating 
     *           that provisioning was not possible.
     *         
     * 
     */
    DELIVERY_NOT_OK,

    /**
     * 
     *           The order action is still in DELIVERY and has received a positive response from the OSS indicating 
     *           that provisioning was a success.
     *         
     * 
     */
    DELIVERY_OK,

    /**
     * 
     *           The order action has completed the DELIVERY phase and is currently performing "closing" activies such as
     *           synchronizing with other OAs, notifying the billing system, etc.
     *         
     * 
     */
    COMPLETION,

    /**
     * 
     *           The order action has been completed and finalized.
     *         
     * 
     */
    CLOSED;

    public String value() {
        return name();
    }

    public static PhaseName fromValue(String v) {
        return valueOf(v);
    }

}
