
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TECHNICAL"/>
 *     &lt;enumeration value="SALES"/>
 *     &lt;enumeration value="NOTE"/>
 *     &lt;enumeration value="FREE_TEXT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommentType")
@XmlEnum
public enum CommentType {

    TECHNICAL,
    SALES,
    NOTE,
    FREE_TEXT;

    public String value() {
        return name();
    }

    public static CommentType fromValue(String v) {
        return valueOf(v);
    }

}
