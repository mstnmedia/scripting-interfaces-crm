
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *       @Created SOA-303
 *       Represents the financing details associated with a financed device
 *       This type is composed by the following fields:
 * 
 *       -financedAvailability: Represents the financing availability information (number of active financing, max amount available for financing)
 *       -financedDevices: A list of financed devices.
 *     
 * 
 * <p>Java class for Financing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Financing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}financingAvailability"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}financedDevices"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Financing", propOrder = {
    "financingAvailability",
    "financedDevices"
})
public class Financing {

    @XmlElement(required = true, nillable = true)
    protected FinancingAvailability financingAvailability;
    @XmlElement(required = true, nillable = true)
    protected FinancedDevices financedDevices;

    /**
     * Gets the value of the financingAvailability property.
     * 
     * @return
     *     possible object is
     *     {@link FinancingAvailability }
     *     
     */
    public FinancingAvailability getFinancingAvailability() {
        return financingAvailability;
    }

    /**
     * Sets the value of the financingAvailability property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancingAvailability }
     *     
     */
    public void setFinancingAvailability(FinancingAvailability value) {
        this.financingAvailability = value;
    }

    /**
     * Gets the value of the financedDevices property.
     * 
     * @return
     *     possible object is
     *     {@link FinancedDevices }
     *     
     */
    public FinancedDevices getFinancedDevices() {
        return financedDevices;
    }

    /**
     * Sets the value of the financedDevices property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancedDevices }
     *     
     */
    public void setFinancedDevices(FinancedDevices value) {
        this.financedDevices = value;
    }

}
