
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.address.Address;
import _do.com.claro.soa.model.customer.CustomerID;


/**
 * <p>Java class for BillingAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillingAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}customerID"/>
 *         &lt;element name="status" type="{http://www.claro.com.do/soa/model/billing}BillingAccountStatus"/>
 *         &lt;element name="statusLastUpdate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="statusActivationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusActivationRsnCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="csaActivityDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="csaActivityRsnDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}subscriptions" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/address}address"/>
 *         &lt;element name="billingStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}billingAccountType"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}billFormat"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}billingPlan" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}guarantor"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}guarantorOf"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingAccount", propOrder = {
    "accountNo",
    "customerID",
    "status",
    "statusLastUpdate",
    "statusActivationCode",
    "statusActivationRsnCode",
    "csaActivityDesc",
    "csaActivityRsnDesc",
    "subscriptions",
    "address",
    "billingStartDate",
    "billingAccountType",
    "billFormat",
    "billingPlan",
    "guarantor",
    "guarantorOf"
})
public class BillingAccount {

    @XmlElement(required = true)
    protected String accountNo;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/customer", required = true)
    protected CustomerID customerID;
    @XmlElement(required = true)
    protected BillingAccountStatus status;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusLastUpdate;
    @XmlElement(required = true, nillable = true)
    protected String statusActivationCode;
    @XmlElement(required = true, nillable = true)
    protected String statusActivationRsnCode;
    @XmlElement(required = true, nillable = true)
    protected String csaActivityDesc;
    @XmlElement(required = true, nillable = true)
    protected String csaActivityRsnDesc;
    @XmlElement(nillable = true)
    protected Subscriptions subscriptions;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/address", required = true, nillable = true)
    protected Address address;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar billingStartDate;
    @XmlElement(required = true)
    protected BillingAccountType billingAccountType;
    @XmlElement(required = true)
    protected BillFormat billFormat;
    @XmlElement(nillable = true)
    protected BillingPlan billingPlan;
    @XmlElement(required = true, nillable = true)
    protected Guarantor guarantor;
    @XmlElement(required = true)
    protected GuarantorOf guarantorOf;

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNo(String value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the customerID property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerID }
     *     
     */
    public CustomerID getCustomerID() {
        return customerID;
    }

    /**
     * Sets the value of the customerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerID }
     *     
     */
    public void setCustomerID(CustomerID value) {
        this.customerID = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link BillingAccountStatus }
     *     
     */
    public BillingAccountStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingAccountStatus }
     *     
     */
    public void setStatus(BillingAccountStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the statusLastUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusLastUpdate() {
        return statusLastUpdate;
    }

    /**
     * Sets the value of the statusLastUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusLastUpdate(XMLGregorianCalendar value) {
        this.statusLastUpdate = value;
    }

    /**
     * Gets the value of the statusActivationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusActivationCode() {
        return statusActivationCode;
    }

    /**
     * Sets the value of the statusActivationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusActivationCode(String value) {
        this.statusActivationCode = value;
    }

    /**
     * Gets the value of the statusActivationRsnCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusActivationRsnCode() {
        return statusActivationRsnCode;
    }

    /**
     * Sets the value of the statusActivationRsnCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusActivationRsnCode(String value) {
        this.statusActivationRsnCode = value;
    }

    /**
     * Gets the value of the csaActivityDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsaActivityDesc() {
        return csaActivityDesc;
    }

    /**
     * Sets the value of the csaActivityDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsaActivityDesc(String value) {
        this.csaActivityDesc = value;
    }

    /**
     * Gets the value of the csaActivityRsnDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsaActivityRsnDesc() {
        return csaActivityRsnDesc;
    }

    /**
     * Sets the value of the csaActivityRsnDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsaActivityRsnDesc(String value) {
        this.csaActivityRsnDesc = value;
    }

    /**
     * Gets the value of the subscriptions property.
     * 
     * @return
     *     possible object is
     *     {@link Subscriptions }
     *     
     */
    public Subscriptions getSubscriptions() {
        return subscriptions;
    }

    /**
     * Sets the value of the subscriptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Subscriptions }
     *     
     */
    public void setSubscriptions(Subscriptions value) {
        this.subscriptions = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the billingStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBillingStartDate() {
        return billingStartDate;
    }

    /**
     * Sets the value of the billingStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBillingStartDate(XMLGregorianCalendar value) {
        this.billingStartDate = value;
    }

    /**
     * Gets the value of the billingAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link BillingAccountType }
     *     
     */
    public BillingAccountType getBillingAccountType() {
        return billingAccountType;
    }

    /**
     * Sets the value of the billingAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingAccountType }
     *     
     */
    public void setBillingAccountType(BillingAccountType value) {
        this.billingAccountType = value;
    }

    /**
     * Gets the value of the billFormat property.
     * 
     * @return
     *     possible object is
     *     {@link BillFormat }
     *     
     */
    public BillFormat getBillFormat() {
        return billFormat;
    }

    /**
     * Sets the value of the billFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillFormat }
     *     
     */
    public void setBillFormat(BillFormat value) {
        this.billFormat = value;
    }

    /**
     * Gets the value of the billingPlan property.
     * 
     * @return
     *     possible object is
     *     {@link BillingPlan }
     *     
     */
    public BillingPlan getBillingPlan() {
        return billingPlan;
    }

    /**
     * Sets the value of the billingPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingPlan }
     *     
     */
    public void setBillingPlan(BillingPlan value) {
        this.billingPlan = value;
    }

    /**
     * Gets the value of the guarantor property.
     * 
     * @return
     *     possible object is
     *     {@link Guarantor }
     *     
     */
    public Guarantor getGuarantor() {
        return guarantor;
    }

    /**
     * Sets the value of the guarantor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Guarantor }
     *     
     */
    public void setGuarantor(Guarantor value) {
        this.guarantor = value;
    }

    /**
     * Gets the value of the guarantorOf property.
     * 
     * @return
     *     possible object is
     *     {@link GuarantorOf }
     *     
     */
    public GuarantorOf getGuarantorOf() {
        return guarantorOf;
    }

    /**
     * Sets the value of the guarantorOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link GuarantorOf }
     *     
     */
    public void setGuarantorOf(GuarantorOf value) {
        this.guarantorOf = value;
    }

}
