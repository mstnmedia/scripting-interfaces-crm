
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.address.Address;
import _do.com.claro.soa.model.generic.Gender;
import _do.com.claro.soa.model.generic.TelephoneNumber;
import _do.com.claro.soa.model.personidentity.Names;
import _do.com.claro.soa.model.personidentity.PersonID;


/**
 * <p>Java class for Contact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Contact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contactID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}customerID"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}names"/>
 *         &lt;element name="role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/generic}phone"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/generic}fax"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/address}address" minOccurs="0"/>
 *         &lt;element name="birthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="maritalStatus" type="{http://www.claro.com.do/soa/model/customer}MaritalStatus"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/generic}gender"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}personID"/>
 *         &lt;element name="nationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="preferredContactMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="preferredContactTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}alternateContactMethods" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contact", propOrder = {
    "contactID",
    "customerID",
    "names",
    "role",
    "phone",
    "fax",
    "email",
    "address",
    "birthDate",
    "maritalStatus",
    "gender",
    "personID",
    "nationality",
    "preferredContactMethod",
    "preferredContactTime",
    "alternateContactMethods"
})
public class Contact {

    @XmlElement(required = true, nillable = true)
    protected String contactID;
    @XmlElement(required = true)
    protected CustomerID customerID;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/personidentity", required = true)
    protected Names names;
    @XmlElement(required = true)
    protected String role;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/generic", required = true, nillable = true)
    protected TelephoneNumber phone;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/generic", required = true, nillable = true)
    protected TelephoneNumber fax;
    @XmlElement(required = true, nillable = true)
    protected String email;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/address", nillable = true)
    protected Address address;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar birthDate;
    @XmlElement(required = true)
    protected MaritalStatus maritalStatus;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/generic", required = true, nillable = true)
    protected Gender gender;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/personidentity", required = true)
    protected PersonID personID;
    @XmlElement(required = true, nillable = true)
    protected String nationality;
    @XmlElement(required = true, nillable = true)
    protected String preferredContactMethod;
    @XmlElement(required = true, nillable = true)
    protected String preferredContactTime;
    @XmlElement(nillable = true)
    protected AlternateContactMethods alternateContactMethods;

    /**
     * Gets the value of the contactID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactID() {
        return contactID;
    }

    /**
     * Sets the value of the contactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactID(String value) {
        this.contactID = value;
    }

    /**
     * Gets the value of the customerID property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerID }
     *     
     */
    public CustomerID getCustomerID() {
        return customerID;
    }

    /**
     * Sets the value of the customerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerID }
     *     
     */
    public void setCustomerID(CustomerID value) {
        this.customerID = value;
    }

    /**
     * Gets the value of the names property.
     * 
     * @return
     *     possible object is
     *     {@link Names }
     *     
     */
    public Names getNames() {
        return names;
    }

    /**
     * Sets the value of the names property.
     * 
     * @param value
     *     allowed object is
     *     {@link Names }
     *     
     */
    public void setNames(Names value) {
        this.names = value;
    }

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumber }
     *     
     */
    public TelephoneNumber getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumber }
     *     
     */
    public void setPhone(TelephoneNumber value) {
        this.phone = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumber }
     *     
     */
    public TelephoneNumber getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumber }
     *     
     */
    public void setFax(TelephoneNumber value) {
        this.fax = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDate(XMLGregorianCalendar value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the maritalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link MaritalStatus }
     *     
     */
    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * Sets the value of the maritalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaritalStatus }
     *     
     */
    public void setMaritalStatus(MaritalStatus value) {
        this.maritalStatus = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link Gender }
     *     
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link Gender }
     *     
     */
    public void setGender(Gender value) {
        this.gender = value;
    }

    /**
     * Gets the value of the personID property.
     * 
     * @return
     *     possible object is
     *     {@link PersonID }
     *     
     */
    public PersonID getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonID }
     *     
     */
    public void setPersonID(PersonID value) {
        this.personID = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the preferredContactMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredContactMethod() {
        return preferredContactMethod;
    }

    /**
     * Sets the value of the preferredContactMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredContactMethod(String value) {
        this.preferredContactMethod = value;
    }

    /**
     * Gets the value of the preferredContactTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredContactTime() {
        return preferredContactTime;
    }

    /**
     * Sets the value of the preferredContactTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredContactTime(String value) {
        this.preferredContactTime = value;
    }

    /**
     * Gets the value of the alternateContactMethods property.
     * 
     * @return
     *     possible object is
     *     {@link AlternateContactMethods }
     *     
     */
    public AlternateContactMethods getAlternateContactMethods() {
        return alternateContactMethods;
    }

    /**
     * Sets the value of the alternateContactMethods property.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateContactMethods }
     *     
     */
    public void setAlternateContactMethods(AlternateContactMethods value) {
        this.alternateContactMethods = value;
    }

}
