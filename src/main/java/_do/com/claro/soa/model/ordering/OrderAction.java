
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.product.Product;


/**
 * 
 *         @Created: SOA-140
 *         A singular action that is to be executed under a given order.
 * 
 *         @Modified SOA-239:
 *         Added the 'salesChannel' and 'reason' fields.
 *       
 * 
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}orderActionID"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}orderID"/>
 *         &lt;element name="parentUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="action" type="{http://www.claro.com.do/soa/model/ordering}OrderActionType"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}creator"/>
 *         &lt;element name="dueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="effectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="serviceRequiredDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="status" type="{http://www.claro.com.do/soa/model/ordering}OrderActionStatus"/>
 *         &lt;element name="productSnapshot">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://www.claro.com.do/soa/model/product}product"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}phases"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}orderActivities"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}pricing"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/ordering}salesChannel"/>
 *         &lt;element name="reason">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderActionID",
    "orderID",
    "parentUID",
    "action",
    "creator",
    "dueDate",
    "effectiveDate",
    "serviceRequiredDate",
    "status",
    "productSnapshot",
    "phases",
    "orderActivities",
    "pricing",
    "salesChannel",
    "reason"
})
public class OrderAction {

    @XmlElement(required = true)
    protected OrderActionID orderActionID;
    @XmlElement(required = true)
    protected OrderID orderID;
    @XmlElement(required = true)
    protected String parentUID;
    @XmlElement(required = true)
    protected OrderActionType action;
    @XmlElement(required = true)
    protected Creator creator;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dueDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar serviceRequiredDate;
    @XmlElement(required = true)
    protected OrderActionStatus status;
    @XmlElement(required = true)
    protected OrderAction.ProductSnapshot productSnapshot;
    @XmlElement(required = true)
    protected Phases phases;
    @XmlElement(required = true)
    protected OrderActivities orderActivities;
    @XmlElement(required = true, nillable = true)
    protected Pricing pricing;
    @XmlElement(required = true, nillable = true)
    protected SalesChannel salesChannel;
    @XmlElement(required = true, nillable = true)
    protected OrderAction.Reason reason;

    /**
     * Gets the value of the orderActionID property.
     * 
     * @return
     *     possible object is
     *     {@link OrderActionID }
     *     
     */
    public OrderActionID getOrderActionID() {
        return orderActionID;
    }

    /**
     * Sets the value of the orderActionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderActionID }
     *     
     */
    public void setOrderActionID(OrderActionID value) {
        this.orderActionID = value;
    }

    /**
     * Gets the value of the orderID property.
     * 
     * @return
     *     possible object is
     *     {@link OrderID }
     *     
     */
    public OrderID getOrderID() {
        return orderID;
    }

    /**
     * Sets the value of the orderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderID }
     *     
     */
    public void setOrderID(OrderID value) {
        this.orderID = value;
    }

    /**
     * Gets the value of the parentUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentUID() {
        return parentUID;
    }

    /**
     * Sets the value of the parentUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentUID(String value) {
        this.parentUID = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link OrderActionType }
     *     
     */
    public OrderActionType getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderActionType }
     *     
     */
    public void setAction(OrderActionType value) {
        this.action = value;
    }

    /**
     * Gets the value of the creator property.
     * 
     * @return
     *     possible object is
     *     {@link Creator }
     *     
     */
    public Creator getCreator() {
        return creator;
    }

    /**
     * Sets the value of the creator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Creator }
     *     
     */
    public void setCreator(Creator value) {
        this.creator = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the serviceRequiredDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServiceRequiredDate() {
        return serviceRequiredDate;
    }

    /**
     * Sets the value of the serviceRequiredDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServiceRequiredDate(XMLGregorianCalendar value) {
        this.serviceRequiredDate = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link OrderActionStatus }
     *     
     */
    public OrderActionStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderActionStatus }
     *     
     */
    public void setStatus(OrderActionStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the productSnapshot property.
     * 
     * @return
     *     possible object is
     *     {@link OrderAction.ProductSnapshot }
     *     
     */
    public OrderAction.ProductSnapshot getProductSnapshot() {
        return productSnapshot;
    }

    /**
     * Sets the value of the productSnapshot property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderAction.ProductSnapshot }
     *     
     */
    public void setProductSnapshot(OrderAction.ProductSnapshot value) {
        this.productSnapshot = value;
    }

    /**
     * Gets the value of the phases property.
     * 
     * @return
     *     possible object is
     *     {@link Phases }
     *     
     */
    public Phases getPhases() {
        return phases;
    }

    /**
     * Sets the value of the phases property.
     * 
     * @param value
     *     allowed object is
     *     {@link Phases }
     *     
     */
    public void setPhases(Phases value) {
        this.phases = value;
    }

    /**
     * Gets the value of the orderActivities property.
     * 
     * @return
     *     possible object is
     *     {@link OrderActivities }
     *     
     */
    public OrderActivities getOrderActivities() {
        return orderActivities;
    }

    /**
     * Sets the value of the orderActivities property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderActivities }
     *     
     */
    public void setOrderActivities(OrderActivities value) {
        this.orderActivities = value;
    }

    /**
     * Gets the value of the pricing property.
     * 
     * @return
     *     possible object is
     *     {@link Pricing }
     *     
     */
    public Pricing getPricing() {
        return pricing;
    }

    /**
     * Sets the value of the pricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Pricing }
     *     
     */
    public void setPricing(Pricing value) {
        this.pricing = value;
    }

    /**
     * Gets the value of the salesChannel property.
     * 
     * @return
     *     possible object is
     *     {@link SalesChannel }
     *     
     */
    public SalesChannel getSalesChannel() {
        return salesChannel;
    }

    /**
     * Sets the value of the salesChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesChannel }
     *     
     */
    public void setSalesChannel(SalesChannel value) {
        this.salesChannel = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link OrderAction.Reason }
     *     
     */
    public OrderAction.Reason getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderAction.Reason }
     *     
     */
    public void setReason(OrderAction.Reason value) {
        this.reason = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://www.claro.com.do/soa/model/product}product"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "product"
    })
    public static class ProductSnapshot {

        @XmlElement(namespace = "http://www.claro.com.do/soa/model/product", required = true, nillable = true)
        protected Product product;

        /**
         * Gets the value of the product property.
         * 
         * @return
         *     possible object is
         *     {@link Product }
         *     
         */
        public Product getProduct() {
            return product;
        }

        /**
         * Sets the value of the product property.
         * 
         * @param value
         *     allowed object is
         *     {@link Product }
         *     
         */
        public void setProduct(Product value) {
            this.product = value;
        }

    }


    /**
     * 
     *                 @Created: SOA-239
     *                 Identifies the reason the orderAction was created.
     *                 An example reason would have code="R001" and description="A solicitud del cliente".
     * 
     *                 Note: Some orderActions don't have a registered reason for unknown causes.
     *               
     * 
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "description"
    })
    public static class Reason {

        @XmlElement(required = true)
        protected String code;
        @XmlElement(required = true)
        protected String description;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }

}
