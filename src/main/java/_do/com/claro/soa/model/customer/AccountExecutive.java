
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.employee.Employee;


/**
 * 
 *         @Created: SOA-81
 *         An account executive is an employee that is part of a team ("account executives") that executes 
 *         actions on a customer's account as directed by the account officer. Only customers of non-INDIVIDUAL 
 *         type are assigned account executives.
 *         Each account executive has an associated role within the team that identifies his or her functions, 
 *         eg.: "Executive Sponsor" or "Consultor de Negocios Movil".
 *       
 * 
 * <p>Java class for AccountExecutive complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountExecutive">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.claro.com.do/soa/model/employee}Employee">
 *       &lt;sequence>
 *         &lt;element name="role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountExecutive", propOrder = {
    "role"
})
public class AccountExecutive
    extends Employee
{

    @XmlElement(required = true)
    protected String role;

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

}
