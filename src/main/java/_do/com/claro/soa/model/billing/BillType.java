
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BillType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REGULAR"/>
 *     &lt;enumeration value="FIRST"/>
 *     &lt;enumeration value="FINAL"/>
 *     &lt;enumeration value="REVISED_FINAL"/>
 *     &lt;enumeration value="NOT_AVAILABLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BillType")
@XmlEnum
public enum BillType {

    REGULAR,
    FIRST,
    FINAL,
    REVISED_FINAL,
    NOT_AVAILABLE;

    public String value() {
        return name();
    }

    public static BillType fromValue(String v) {
        return valueOf(v);
    }

}
