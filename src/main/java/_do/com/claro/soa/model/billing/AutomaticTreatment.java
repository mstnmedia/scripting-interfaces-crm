
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created SOA-235
 *         Describes the steps to follow in the collection process
 *       
 * 
 * <p>Java class for AutomaticTreatment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutomaticTreatment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nextStep" type="{http://www.claro.com.do/soa/model/billing}AutomaticTreatmentStep"/>
 *         &lt;element name="lastStep" type="{http://www.claro.com.do/soa/model/billing}AutomaticTreatmentStep"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutomaticTreatment", propOrder = {
    "nextStep",
    "lastStep"
})
public class AutomaticTreatment {

    @XmlElement(required = true, nillable = true)
    protected AutomaticTreatmentStep nextStep;
    @XmlElement(required = true, nillable = true)
    protected AutomaticTreatmentStep lastStep;

    /**
     * Gets the value of the nextStep property.
     * 
     * @return
     *     possible object is
     *     {@link AutomaticTreatmentStep }
     *     
     */
    public AutomaticTreatmentStep getNextStep() {
        return nextStep;
    }

    /**
     * Sets the value of the nextStep property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutomaticTreatmentStep }
     *     
     */
    public void setNextStep(AutomaticTreatmentStep value) {
        this.nextStep = value;
    }

    /**
     * Gets the value of the lastStep property.
     * 
     * @return
     *     possible object is
     *     {@link AutomaticTreatmentStep }
     *     
     */
    public AutomaticTreatmentStep getLastStep() {
        return lastStep;
    }

    /**
     * Sets the value of the lastStep property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutomaticTreatmentStep }
     *     
     */
    public void setLastStep(AutomaticTreatmentStep value) {
        this.lastStep = value;
    }

}
