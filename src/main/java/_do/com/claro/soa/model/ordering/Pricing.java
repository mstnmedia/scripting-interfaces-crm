
package _do.com.claro.soa.model.ordering;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.billing.Charge;


/**
 * 
 *         @Created: SOA-140
 *         The charges generated against the customer for this orderAction.
 *       
 * 
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pricingElement" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="componentName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="pricePlanName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element ref="{http://www.claro.com.do/soa/model/billing}charge"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pricingElement"
})
public class Pricing {

    @XmlElement(required = true)
    protected List<Pricing.PricingElement> pricingElement;

    /**
     * Gets the value of the pricingElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pricingElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPricingElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Pricing.PricingElement }
     * 
     * 
     */
    public List<Pricing.PricingElement> getPricingElement() {
        if (pricingElement == null) {
            pricingElement = new ArrayList<Pricing.PricingElement>();
        }
        return this.pricingElement;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="componentName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="pricePlanName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}charge"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "componentName",
        "pricePlanName",
        "charge"
    })
    public static class PricingElement {

        @XmlElement(required = true)
        protected String componentName;
        @XmlElement(required = true)
        protected String pricePlanName;
        @XmlElement(namespace = "http://www.claro.com.do/soa/model/billing", required = true)
        protected Charge charge;

        /**
         * Gets the value of the componentName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComponentName() {
            return componentName;
        }

        /**
         * Sets the value of the componentName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComponentName(String value) {
            this.componentName = value;
        }

        /**
         * Gets the value of the pricePlanName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPricePlanName() {
            return pricePlanName;
        }

        /**
         * Sets the value of the pricePlanName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPricePlanName(String value) {
            this.pricePlanName = value;
        }

        /**
         * Gets the value of the charge property.
         * 
         * @return
         *     possible object is
         *     {@link Charge }
         *     
         */
        public Charge getCharge() {
            return charge;
        }

        /**
         * Sets the value of the charge property.
         * 
         * @param value
         *     allowed object is
         *     {@link Charge }
         *     
         */
        public void setCharge(Charge value) {
            this.charge = value;
        }

    }

}
