
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.generic.Money;


/**
 * 
 *         @Created SOA-235
 * 
 *         Financial sumary indicates the state of the pendding amount to be payed
 *         FinancialSummary includes the following fields:
 * 
 *         -amt_1to30: Indicates that the amount have between 1 and 30 days of being generated
 *         -amt_31To60: Indicates that the amount have between 31 and 60 days of being generated
 *         -amt_61To90: Indicates that the amount have between 61 and 90 days of being generated
 *         -amt_90_plus: Indicates that the amount have more than 90 days of being generated
 *         -pastDueAmount: represents the total sum of the pastDue amounts
 *         -currentAmount: represents the amount of the current bill (not past due)
 *       
 * 
 * <p>Java class for FinancialSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialSummary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amt_1To30" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *         &lt;element name="amt_31To60" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *         &lt;element name="amt_61To90" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *         &lt;element name="amt_90_plus" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *         &lt;element name="pastDueAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *         &lt;element name="currentAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialSummary", propOrder = {
    "amt1To30",
    "amt31To60",
    "amt61To90",
    "amt90Plus",
    "pastDueAmount",
    "currentAmount"
})
public class FinancialSummary {

    @XmlElement(name = "amt_1To30", required = true)
    protected Money amt1To30;
    @XmlElement(name = "amt_31To60", required = true)
    protected Money amt31To60;
    @XmlElement(name = "amt_61To90", required = true)
    protected Money amt61To90;
    @XmlElement(name = "amt_90_plus", required = true)
    protected Money amt90Plus;
    @XmlElement(required = true)
    protected Money pastDueAmount;
    @XmlElement(required = true)
    protected Money currentAmount;

    /**
     * Gets the value of the amt1To30 property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getAmt1To30() {
        return amt1To30;
    }

    /**
     * Sets the value of the amt1To30 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setAmt1To30(Money value) {
        this.amt1To30 = value;
    }

    /**
     * Gets the value of the amt31To60 property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getAmt31To60() {
        return amt31To60;
    }

    /**
     * Sets the value of the amt31To60 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setAmt31To60(Money value) {
        this.amt31To60 = value;
    }

    /**
     * Gets the value of the amt61To90 property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getAmt61To90() {
        return amt61To90;
    }

    /**
     * Sets the value of the amt61To90 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setAmt61To90(Money value) {
        this.amt61To90 = value;
    }

    /**
     * Gets the value of the amt90Plus property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getAmt90Plus() {
        return amt90Plus;
    }

    /**
     * Sets the value of the amt90Plus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setAmt90Plus(Money value) {
        this.amt90Plus = value;
    }

    /**
     * Gets the value of the pastDueAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getPastDueAmount() {
        return pastDueAmount;
    }

    /**
     * Sets the value of the pastDueAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setPastDueAmount(Money value) {
        this.pastDueAmount = value;
    }

    /**
     * Gets the value of the currentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getCurrentAmount() {
        return currentAmount;
    }

    /**
     * Sets the value of the currentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setCurrentAmount(Money value) {
        this.currentAmount = value;
    }

}
