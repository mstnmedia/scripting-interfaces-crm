
package _do.com.claro.soa.model.product;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.model.product package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CatalogDefinition_QNAME = new QName("http://www.claro.com.do/soa/model/product", "catalogDefinition");
    private final static QName _Components_QNAME = new QName("http://www.claro.com.do/soa/model/product", "components");
    private final static QName _Products_QNAME = new QName("http://www.claro.com.do/soa/model/product", "products");
    private final static QName _Component_QNAME = new QName("http://www.claro.com.do/soa/model/product", "component");
    private final static QName _ProductCode_QNAME = new QName("http://www.claro.com.do/soa/model/product", "productCode");
    private final static QName _Product_QNAME = new QName("http://www.claro.com.do/soa/model/product", "product");
    private final static QName _Offer_QNAME = new QName("http://www.claro.com.do/soa/model/product", "offer");
    private final static QName _ProductType_QNAME = new QName("http://www.claro.com.do/soa/model/product", "productType");
    private final static QName _Comment_QNAME = new QName("http://www.claro.com.do/soa/model/product", "comment");
    private final static QName _Comments_QNAME = new QName("http://www.claro.com.do/soa/model/product", "comments");
    private final static QName _ProductAttributes_QNAME = new QName("http://www.claro.com.do/soa/model/product", "productAttributes");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.model.product
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProductAttributes }
     * 
     */
    public ProductAttributes createProductAttributes() {
        return new ProductAttributes();
    }

    /**
     * Create an instance of {@link ComponentStatus }
     * 
     */
    public ComponentStatus createComponentStatus() {
        return new ComponentStatus();
    }

    /**
     * Create an instance of {@link Comments }
     * 
     */
    public Comments createComments() {
        return new Comments();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link ProductAttribute }
     * 
     */
    public ProductAttribute createProductAttribute() {
        return new ProductAttribute();
    }

    /**
     * Create an instance of {@link Comment }
     * 
     */
    public Comment createComment() {
        return new Comment();
    }

    /**
     * Create an instance of {@link ProductType }
     * 
     */
    public ProductType createProductType() {
        return new ProductType();
    }

    /**
     * Create an instance of {@link Offer }
     * 
     */
    public Offer createOffer() {
        return new Offer();
    }

    /**
     * Create an instance of {@link Component }
     * 
     */
    public Component createComponent() {
        return new Component();
    }

    /**
     * Create an instance of {@link Components }
     * 
     */
    public Components createComponents() {
        return new Components();
    }

    /**
     * Create an instance of {@link Component.ComponentType }
     * 
     */
    public Component.ComponentType createComponentComponentType() {
        return new Component.ComponentType();
    }

    /**
     * Create an instance of {@link Products }
     * 
     */
    public Products createProducts() {
        return new Products();
    }

    /**
     * Create an instance of {@link CatalogDefinition }
     * 
     */
    public CatalogDefinition createCatalogDefinition() {
        return new CatalogDefinition();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CatalogDefinition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "catalogDefinition")
    public JAXBElement<CatalogDefinition> createCatalogDefinition(CatalogDefinition value) {
        return new JAXBElement<CatalogDefinition>(_CatalogDefinition_QNAME, CatalogDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Components }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "components")
    public JAXBElement<Components> createComponents(Components value) {
        return new JAXBElement<Components>(_Components_QNAME, Components.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Products }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "products")
    public JAXBElement<Products> createProducts(Products value) {
        return new JAXBElement<Products>(_Products_QNAME, Products.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Component }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "component")
    public JAXBElement<Component> createComponent(Component value) {
        return new JAXBElement<Component>(_Component_QNAME, Component.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "productCode")
    public JAXBElement<String> createProductCode(String value) {
        return new JAXBElement<String>(_ProductCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Product }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "product")
    public JAXBElement<Product> createProduct(Product value) {
        return new JAXBElement<Product>(_Product_QNAME, Product.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Offer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "offer")
    public JAXBElement<Offer> createOffer(Offer value) {
        return new JAXBElement<Offer>(_Offer_QNAME, Offer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "productType")
    public JAXBElement<ProductType> createProductType(ProductType value) {
        return new JAXBElement<ProductType>(_ProductType_QNAME, ProductType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Comment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "comment")
    public JAXBElement<Comment> createComment(Comment value) {
        return new JAXBElement<Comment>(_Comment_QNAME, Comment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Comments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "comments")
    public JAXBElement<Comments> createComments(Comments value) {
        return new JAXBElement<Comments>(_Comments_QNAME, Comments.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductAttributes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/product", name = "productAttributes")
    public JAXBElement<ProductAttributes> createProductAttributes(ProductAttributes value) {
        return new JAXBElement<ProductAttributes>(_ProductAttributes_QNAME, ProductAttributes.class, null, value);
    }

}
