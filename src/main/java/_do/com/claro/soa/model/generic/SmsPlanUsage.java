
package _do.com.claro.soa.model.generic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created: SOA-258 Information about a mobile postpaid
 *         sms plan usage
 *       
 * 
 * <p>Java class for SmsPlanUsage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SmsPlanUsage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="totalSmsInPlan" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="usedSms" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="availableSms" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="planCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rolledSms" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SmsPlanUsage", propOrder = {
    "totalSmsInPlan",
    "usedSms",
    "availableSms",
    "planCode",
    "rolledSms"
})
public class SmsPlanUsage {

    protected int totalSmsInPlan;
    protected int usedSms;
    protected int availableSms;
    @XmlElement(required = true)
    protected String planCode;
    protected int rolledSms;

    /**
     * Gets the value of the totalSmsInPlan property.
     * 
     */
    public int getTotalSmsInPlan() {
        return totalSmsInPlan;
    }

    /**
     * Sets the value of the totalSmsInPlan property.
     * 
     */
    public void setTotalSmsInPlan(int value) {
        this.totalSmsInPlan = value;
    }

    /**
     * Gets the value of the usedSms property.
     * 
     */
    public int getUsedSms() {
        return usedSms;
    }

    /**
     * Sets the value of the usedSms property.
     * 
     */
    public void setUsedSms(int value) {
        this.usedSms = value;
    }

    /**
     * Gets the value of the availableSms property.
     * 
     */
    public int getAvailableSms() {
        return availableSms;
    }

    /**
     * Sets the value of the availableSms property.
     * 
     */
    public void setAvailableSms(int value) {
        this.availableSms = value;
    }

    /**
     * Gets the value of the planCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanCode() {
        return planCode;
    }

    /**
     * Sets the value of the planCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanCode(String value) {
        this.planCode = value;
    }

    /**
     * Gets the value of the rolledSms property.
     * 
     */
    public int getRolledSms() {
        return rolledSms;
    }

    /**
     * Sets the value of the rolledSms property.
     * 
     */
    public void setRolledSms(int value) {
        this.rolledSms = value;
    }

}
