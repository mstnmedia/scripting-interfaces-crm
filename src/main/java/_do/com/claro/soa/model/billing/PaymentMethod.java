
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CUPON"/>
 *     &lt;enumeration value="CASH"/>
 *     &lt;enumeration value="CHECK"/>
 *     &lt;enumeration value="CREDIT_CARD"/>
 *     &lt;enumeration value="BONUS"/>
 *     &lt;enumeration value="RENT"/>
 *     &lt;enumeration value="LOYALTY_POINTS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PaymentMethod")
@XmlEnum
public enum PaymentMethod {

    CUPON,
    CASH,
    CHECK,
    CREDIT_CARD,
    BONUS,
    RENT,
    LOYALTY_POINTS;

    public String value() {
        return name();
    }

    public static PaymentMethod fromValue(String v) {
        return valueOf(v);
    }

}
