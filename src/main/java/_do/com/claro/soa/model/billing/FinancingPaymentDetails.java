
package _do.com.claro.soa.model.billing;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.generic.Money;


/**
 * 
 *       @Created SOA-260
 *       Represents the payment information associated with the financing of a device
 *       this entity has the following fields:
 * 
 *       -financingCode: A code to identify the financing
 *       -startDate: Indicates the date on which the financing start.
 *       -endDate: Represents the date on which the financing concluded.
 *       -totalFinancedAmount: Represents the total cost of the device.
 *       -paidInstallments: Indicates the number of installments the customer has paid.
 *       -totalNumberOfInstallments: Indicates the number of payments that the customer has to make
 *         to completely pay the device.
 *       -monthlyPaymentAmount: Represents the amount to be payed each month.
 *       -pendingAmount: Represents the amount the customer have left to pay.
 * 
 *       @Modified SOA-303:
 *       -Removed activeFinancing and amountAvailableForFinancing fields
 *       -Added paidInstallment field
 *       -Renamed totalNumberOfPayments to totalNumberOfInstallments
 *     
 * 
 * <p>Java class for FinancingPaymentDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancingPaymentDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="financingCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="totalFinancedAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *         &lt;element name="paidInstallments" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="totalNumberOfInstallments" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="monthlyPaymentAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *         &lt;element name="pendingAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancingPaymentDetails", propOrder = {
    "financingCode",
    "startDate",
    "endDate",
    "totalFinancedAmount",
    "paidInstallments",
    "totalNumberOfInstallments",
    "monthlyPaymentAmount",
    "pendingAmount"
})
public class FinancingPaymentDetails {

    @XmlElement(required = true, nillable = true)
    protected String financingCode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    @XmlElement(required = true, nillable = true)
    protected Money totalFinancedAmount;
    @XmlElement(required = true, nillable = true)
    protected BigInteger paidInstallments;
    @XmlElement(required = true, nillable = true)
    protected BigInteger totalNumberOfInstallments;
    @XmlElement(required = true, nillable = true)
    protected Money monthlyPaymentAmount;
    @XmlElement(required = true, nillable = true)
    protected Money pendingAmount;

    /**
     * Gets the value of the financingCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinancingCode() {
        return financingCode;
    }

    /**
     * Sets the value of the financingCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinancingCode(String value) {
        this.financingCode = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the totalFinancedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getTotalFinancedAmount() {
        return totalFinancedAmount;
    }

    /**
     * Sets the value of the totalFinancedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setTotalFinancedAmount(Money value) {
        this.totalFinancedAmount = value;
    }

    /**
     * Gets the value of the paidInstallments property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPaidInstallments() {
        return paidInstallments;
    }

    /**
     * Sets the value of the paidInstallments property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPaidInstallments(BigInteger value) {
        this.paidInstallments = value;
    }

    /**
     * Gets the value of the totalNumberOfInstallments property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalNumberOfInstallments() {
        return totalNumberOfInstallments;
    }

    /**
     * Sets the value of the totalNumberOfInstallments property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalNumberOfInstallments(BigInteger value) {
        this.totalNumberOfInstallments = value;
    }

    /**
     * Gets the value of the monthlyPaymentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getMonthlyPaymentAmount() {
        return monthlyPaymentAmount;
    }

    /**
     * Sets the value of the monthlyPaymentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setMonthlyPaymentAmount(Money value) {
        this.monthlyPaymentAmount = value;
    }

    /**
     * Gets the value of the pendingAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getPendingAmount() {
        return pendingAmount;
    }

    /**
     * Sets the value of the pendingAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setPendingAmount(Money value) {
        this.pendingAmount = value;
    }

}
