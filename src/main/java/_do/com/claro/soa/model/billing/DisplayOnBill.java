
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DisplayOnBill.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DisplayOnBill">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="YES"/>
 *     &lt;enumeration value="NO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DisplayOnBill")
@XmlEnum
public enum DisplayOnBill {

    YES,
    NO;

    public String value() {
        return name();
    }

    public static DisplayOnBill fromValue(String v) {
        return valueOf(v);
    }

}
