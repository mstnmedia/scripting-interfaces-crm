
package _do.com.claro.soa.model.generic;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerDeviceTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerDeviceTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="IMEI"/>
 *     &lt;enumeration value="SIM_CARD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CustomerDeviceTypes")
@XmlEnum
public enum CustomerDeviceTypes {

    IMEI,
    SIM_CARD;

    public String value() {
        return name();
    }

    public static CustomerDeviceTypes fromValue(String v) {
        return valueOf(v);
    }

}
