
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *         @Created: SOA-186
 *         Type used to present received Payment of a Ensemble Billing Account (BAN).
 *       
 * 
 * <p>Java class for Payment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Payment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="depositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="postingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="source" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="method" type="{http://www.claro.com.do/soa/model/billing}PaymentMethod"/>
 *         &lt;element name="omsOrderID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="displayOnBill" type="{http://www.claro.com.do/soa/model/billing}DisplayOnBill"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Payment", propOrder = {
    "depositDate",
    "postingDate",
    "amount",
    "source",
    "method",
    "omsOrderID",
    "displayOnBill"
})
public class Payment {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar depositDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar postingDate;
    protected double amount;
    @XmlElement(required = true)
    protected String source;
    @XmlElement(required = true)
    protected PaymentMethod method;
    @XmlElement(required = true)
    protected String omsOrderID;
    @XmlElement(required = true)
    protected DisplayOnBill displayOnBill;

    /**
     * Gets the value of the depositDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepositDate() {
        return depositDate;
    }

    /**
     * Sets the value of the depositDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepositDate(XMLGregorianCalendar value) {
        this.depositDate = value;
    }

    /**
     * Gets the value of the postingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPostingDate() {
        return postingDate;
    }

    /**
     * Sets the value of the postingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPostingDate(XMLGregorianCalendar value) {
        this.postingDate = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAmount(double value) {
        this.amount = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the method property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentMethod }
     *     
     */
    public PaymentMethod getMethod() {
        return method;
    }

    /**
     * Sets the value of the method property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentMethod }
     *     
     */
    public void setMethod(PaymentMethod value) {
        this.method = value;
    }

    /**
     * Gets the value of the omsOrderID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOmsOrderID() {
        return omsOrderID;
    }

    /**
     * Sets the value of the omsOrderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOmsOrderID(String value) {
        this.omsOrderID = value;
    }

    /**
     * Gets the value of the displayOnBill property.
     * 
     * @return
     *     possible object is
     *     {@link DisplayOnBill }
     *     
     */
    public DisplayOnBill getDisplayOnBill() {
        return displayOnBill;
    }

    /**
     * Sets the value of the displayOnBill property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisplayOnBill }
     *     
     */
    public void setDisplayOnBill(DisplayOnBill value) {
        this.displayOnBill = value;
    }

}
