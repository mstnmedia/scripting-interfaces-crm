
package _do.com.claro.soa.model.generic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created: SOA-256 Represents a Balance. A balance is
 *         composed by the following fields: sign: represents the
 *         sign of the balence. If sign is NEGATIVE of POSITIVE
 *         amount: represents the amount of the balance.
 *       
 * 
 * <p>Java class for Balance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Balance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sign" type="{http://www.claro.com.do/soa/model/generic}Sign"/>
 *         &lt;element name="amount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Balance", propOrder = {
    "sign",
    "amount"
})
public class Balance {

    @XmlElement(required = true)
    protected Sign sign;
    @XmlElement(required = true)
    protected Money amount;

    /**
     * Gets the value of the sign property.
     * 
     * @return
     *     possible object is
     *     {@link Sign }
     *     
     */
    public Sign getSign() {
        return sign;
    }

    /**
     * Sets the value of the sign property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sign }
     *     
     */
    public void setSign(Sign value) {
        this.sign = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setAmount(Money value) {
        this.amount = value;
    }

}
