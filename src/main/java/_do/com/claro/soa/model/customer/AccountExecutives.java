
package _do.com.claro.soa.model.customer;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountExecutives complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountExecutives">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}accountExecutive" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountExecutives", propOrder = {
    "accountExecutive"
})
public class AccountExecutives {

    @XmlElement(required = true)
    protected List<AccountExecutive> accountExecutive;

    /**
     * Gets the value of the accountExecutive property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountExecutive property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountExecutive().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountExecutive }
     * 
     * 
     */
    public List<AccountExecutive> getAccountExecutive() {
        if (accountExecutive == null) {
            accountExecutive = new ArrayList<AccountExecutive>();
        }
        return this.accountExecutive;
    }

}
