
package _do.com.claro.soa.model.personidentity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WorkPermit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkPermit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workPermitID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkPermit", propOrder = {
    "workPermitID"
})
public class WorkPermit {

    @XmlElement(required = true)
    protected String workPermitID;

    /**
     * Gets the value of the workPermitID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkPermitID() {
        return workPermitID;
    }

    /**
     * Sets the value of the workPermitID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkPermitID(String value) {
        this.workPermitID = value;
    }

}
