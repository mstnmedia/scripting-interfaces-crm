
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubscriptionStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SubscriptionStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACTIVE"/>
 *     &lt;enumeration value="PREACTIVE"/>
 *     &lt;enumeration value="SUSPENDED"/>
 *     &lt;enumeration value="CANCELLED"/>
 *     &lt;enumeration value="RESERVED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SubscriptionStatus")
@XmlEnum
public enum SubscriptionStatus {

    ACTIVE,
    PREACTIVE,
    SUSPENDED,
    CANCELLED,

    /**
     * 
     *             Reserved status is used in case of customer layaway of a subscriber
     *             number.
     *           
     * 
     */
    RESERVED;

    public String value() {
        return name();
    }

    public static SubscriptionStatus fromValue(String v) {
        return valueOf(v);
    }

}
