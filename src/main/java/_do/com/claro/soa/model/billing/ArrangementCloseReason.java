
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrangementCloseReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ArrangementCloseReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SUCCESS"/>
 *     &lt;enumeration value="FAILURE"/>
 *     &lt;enumeration value="ON_LINE"/>
 *     &lt;enumeration value="NOT_AVAILABLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ArrangementCloseReason")
@XmlEnum
public enum ArrangementCloseReason {

    SUCCESS,
    FAILURE,
    ON_LINE,
    NOT_AVAILABLE;

    public String value() {
        return name();
    }

    public static ArrangementCloseReason fromValue(String v) {
        return valueOf(v);
    }

}
