
package _do.com.claro.soa.model.offer;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.model.offer package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UpcomingEligiblities_QNAME = new QName("http://www.claro.com.do/soa/model/offer", "upcomingEligiblities");
    private final static QName _Eligibility_QNAME = new QName("http://www.claro.com.do/soa/model/offer", "eligibility");
    private final static QName _DeviceSwitchElegibility_QNAME = new QName("http://www.claro.com.do/soa/model/offer", "deviceSwitchElegibility");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.model.offer
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeviceSwitchElegibility }
     * 
     */
    public DeviceSwitchElegibility createDeviceSwitchElegibility() {
        return new DeviceSwitchElegibility();
    }

    /**
     * Create an instance of {@link Eligibility }
     * 
     */
    public Eligibility createEligibility() {
        return new Eligibility();
    }

    /**
     * Create an instance of {@link UpcomingEligiblities }
     * 
     */
    public UpcomingEligiblities createUpcomingEligiblities() {
        return new UpcomingEligiblities();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpcomingEligiblities }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/offer", name = "upcomingEligiblities")
    public JAXBElement<UpcomingEligiblities> createUpcomingEligiblities(UpcomingEligiblities value) {
        return new JAXBElement<UpcomingEligiblities>(_UpcomingEligiblities_QNAME, UpcomingEligiblities.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Eligibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/offer", name = "eligibility")
    public JAXBElement<Eligibility> createEligibility(Eligibility value) {
        return new JAXBElement<Eligibility>(_Eligibility_QNAME, Eligibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeviceSwitchElegibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/offer", name = "deviceSwitchElegibility")
    public JAXBElement<DeviceSwitchElegibility> createDeviceSwitchElegibility(DeviceSwitchElegibility value) {
        return new JAXBElement<DeviceSwitchElegibility>(_DeviceSwitchElegibility_QNAME, DeviceSwitchElegibility.class, null, value);
    }

}
