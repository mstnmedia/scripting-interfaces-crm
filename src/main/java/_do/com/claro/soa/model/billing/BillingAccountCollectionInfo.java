
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created: SOA-235
 *         Billing account collection info is the collection information associated with a ban.
 *         The BillingAccountCollectionInfo element is composed by the following fields:
 * 
 *         -defaulter: is a boolean field that indicates if the customer have not paid after the bill due date
 *         -specialInstructions: a boolean field, if true indicates that account have special instructions tied to it
 *         -fallowing: a boolean field, if true indicates that a reminder of open following is activated.
 *         -newCycle: a boolean that indicates if the account have changed of cycle
 *         -vip: a boolean field, if true indicates that account belongs to an important customer
 *         -holdAutomaticTreatment: a boolean field, if true means that the automatic treatment is activated
 *         -financialSumary: indicates the pendding amount to be payed
 *         -automaticTreatment: Describes the steps to follow in the collection process
 *         -historyInformation : Represents the lists of activities of the account such as suspensions, payments, cancelations, etc.
 *       
 * 
 * <p>Java class for BillingAccountCollectionInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillingAccountCollectionInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="defaulter" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="specialInstructions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="fallowing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="newCycle" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="vip" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="holdAutomaticTreatment" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}finacialSummary"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}automaticTreatment"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}historyInformation"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingAccountCollectionInfo", propOrder = {
    "defaulter",
    "specialInstructions",
    "fallowing",
    "newCycle",
    "vip",
    "holdAutomaticTreatment",
    "finacialSummary",
    "automaticTreatment",
    "historyInformation"
})
public class BillingAccountCollectionInfo {

    protected boolean defaulter;
    protected boolean specialInstructions;
    protected boolean fallowing;
    protected boolean newCycle;
    protected boolean vip;
    protected boolean holdAutomaticTreatment;
    @XmlElement(required = true)
    protected FinancialSummary finacialSummary;
    @XmlElement(required = true)
    protected AutomaticTreatment automaticTreatment;
    @XmlElement(required = true)
    protected HistoryInformation historyInformation;

    /**
     * Gets the value of the defaulter property.
     * 
     */
    public boolean isDefaulter() {
        return defaulter;
    }

    /**
     * Sets the value of the defaulter property.
     * 
     */
    public void setDefaulter(boolean value) {
        this.defaulter = value;
    }

    /**
     * Gets the value of the specialInstructions property.
     * 
     */
    public boolean isSpecialInstructions() {
        return specialInstructions;
    }

    /**
     * Sets the value of the specialInstructions property.
     * 
     */
    public void setSpecialInstructions(boolean value) {
        this.specialInstructions = value;
    }

    /**
     * Gets the value of the fallowing property.
     * 
     */
    public boolean isFallowing() {
        return fallowing;
    }

    /**
     * Sets the value of the fallowing property.
     * 
     */
    public void setFallowing(boolean value) {
        this.fallowing = value;
    }

    /**
     * Gets the value of the newCycle property.
     * 
     */
    public boolean isNewCycle() {
        return newCycle;
    }

    /**
     * Sets the value of the newCycle property.
     * 
     */
    public void setNewCycle(boolean value) {
        this.newCycle = value;
    }

    /**
     * Gets the value of the vip property.
     * 
     */
    public boolean isVip() {
        return vip;
    }

    /**
     * Sets the value of the vip property.
     * 
     */
    public void setVip(boolean value) {
        this.vip = value;
    }

    /**
     * Gets the value of the holdAutomaticTreatment property.
     * 
     */
    public boolean isHoldAutomaticTreatment() {
        return holdAutomaticTreatment;
    }

    /**
     * Sets the value of the holdAutomaticTreatment property.
     * 
     */
    public void setHoldAutomaticTreatment(boolean value) {
        this.holdAutomaticTreatment = value;
    }

    /**
     * Gets the value of the finacialSummary property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialSummary }
     *     
     */
    public FinancialSummary getFinacialSummary() {
        return finacialSummary;
    }

    /**
     * Sets the value of the finacialSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialSummary }
     *     
     */
    public void setFinacialSummary(FinancialSummary value) {
        this.finacialSummary = value;
    }

    /**
     * Gets the value of the automaticTreatment property.
     * 
     * @return
     *     possible object is
     *     {@link AutomaticTreatment }
     *     
     */
    public AutomaticTreatment getAutomaticTreatment() {
        return automaticTreatment;
    }

    /**
     * Sets the value of the automaticTreatment property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutomaticTreatment }
     *     
     */
    public void setAutomaticTreatment(AutomaticTreatment value) {
        this.automaticTreatment = value;
    }

    /**
     * Gets the value of the historyInformation property.
     * 
     * @return
     *     possible object is
     *     {@link HistoryInformation }
     *     
     */
    public HistoryInformation getHistoryInformation() {
        return historyInformation;
    }

    /**
     * Sets the value of the historyInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoryInformation }
     *     
     */
    public void setHistoryInformation(HistoryInformation value) {
        this.historyInformation = value;
    }

}
