
package _do.com.claro.soa.model.generic;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerDeviceStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerDeviceStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INICIAL(CARGA)"/>
 *     &lt;enumeration value="ACTIVACION"/>
 *     &lt;enumeration value="LIBERACION(PENDIENTE)"/>
 *     &lt;enumeration value="CAMBIO_DE_EQUIPO"/>
 *     &lt;enumeration value="CAMBIO_DE_NUMERO"/>
 *     &lt;enumeration value="CANCELACION"/>
 *     &lt;enumeration value="LIBERACION"/>
 *     &lt;enumeration value="REPORTE"/>
 *     &lt;enumeration value="ROBADO(REPORTE)"/>
 *     &lt;enumeration value="EXTRAVIADO(REPORTE)"/>
 *     &lt;enumeration value="BOLETINADO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CustomerDeviceStatus")
@XmlEnum
public enum CustomerDeviceStatus {

    @XmlEnumValue("INICIAL(CARGA)")
    INICIAL_CARGA("INICIAL(CARGA)"),
    ACTIVACION("ACTIVACION"),
    @XmlEnumValue("LIBERACION(PENDIENTE)")
    LIBERACION_PENDIENTE("LIBERACION(PENDIENTE)"),
    CAMBIO_DE_EQUIPO("CAMBIO_DE_EQUIPO"),
    CAMBIO_DE_NUMERO("CAMBIO_DE_NUMERO"),
    CANCELACION("CANCELACION"),
    LIBERACION("LIBERACION"),
    REPORTE("REPORTE"),
    @XmlEnumValue("ROBADO(REPORTE)")
    ROBADO_REPORTE("ROBADO(REPORTE)"),
    @XmlEnumValue("EXTRAVIADO(REPORTE)")
    EXTRAVIADO_REPORTE("EXTRAVIADO(REPORTE)"),
    BOLETINADO("BOLETINADO");
    private final String value;

    CustomerDeviceStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomerDeviceStatus fromValue(String v) {
        for (CustomerDeviceStatus c: CustomerDeviceStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
