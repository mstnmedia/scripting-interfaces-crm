
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderActionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CANCEL"/>
 *     &lt;enumeration value="CEASE"/>
 *     &lt;enumeration value="CHANGE"/>
 *     &lt;enumeration value="CHANGE_OWNERSHIP"/>
 *     &lt;enumeration value="MOVE_BAN"/>
 *     &lt;enumeration value="PROVIDE"/>
 *     &lt;enumeration value="CEASE_PART_OF_REPLACE_OFFER"/>
 *     &lt;enumeration value="CHANGE_PART_OF_REPLACE_OFFER"/>
 *     &lt;enumeration value="REPLACE_OFFER"/>
 *     &lt;enumeration value="RESUME"/>
 *     &lt;enumeration value="SUSPEND"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderActionType")
@XmlEnum
public enum OrderActionType {


    /**
     * 
     *           The order action will cancel another order action. 
     *           The order action to be cancelled is usually a parent of the one doing the cancelling.
     *         
     * 
     */
    CANCEL,

    /**
     * 
     *           The order action will cease ("cancel") the product, thus ending the customer's service.
     *         
     * 
     */
    CEASE,

    /**
     * 
     *           The order action will change some aspect of the product. This could be a price plan, some feature, etc.
     *           Note that these are also used when changing the product's address instead of "MOVE".
     *           Also note that changing a product's BAN is done using "MOVE_BAN".
     *         
     * 
     */
    CHANGE,

    /**
     * 
     *           The order action will change ownership of the product from one customer (CCU) to another.
     *         
     * 
     */
    CHANGE_OWNERSHIP,

    /**
     * 
     *           The order action will switch the product's billing account number for another one and notify the billing system.
     *         
     * 
     */
    MOVE_BAN,

    /**
     * 
     *           The order action will provision (ie. "install") a new service.
     *         
     * 
     */
    PROVIDE,

    /**
     * 
     *           The order action will cease the product because it does not form part of the new offer being provisioned.
     *           @see REPLACE_OFFER
     *         
     * 
     */
    CEASE_PART_OF_REPLACE_OFFER,

    /**
     * 
     *           The order action will change the product's configuration in order to match the parameters set by the new offer being
     *           provisioned.
     *           @see REPLACE_OFFER
     *         
     * 
     */
    CHANGE_PART_OF_REPLACE_OFFER,

    /**
     * 
     *           The order action will replace the product's offer for another one.
     *           An OA of this type generally groups together two or more child OAs that perform the actual changes.
     *           @see CEASE_PART_OF_REPLACE_OFFER
     *           @see CHANGE_PART_OF_REPLACE_OFFER
     *         
     * 
     */
    REPLACE_OFFER,

    /**
     * 
     *           The order action will resume the product's service after a previous suspend.
     *         
     * 
     */
    RESUME,

    /**
     * 
     *           The order action will suspend a product's service.
     *         
     * 
     */
    SUSPEND;

    public String value() {
        return name();
    }

    public static OrderActionType fromValue(String v) {
        return valueOf(v);
    }

}
