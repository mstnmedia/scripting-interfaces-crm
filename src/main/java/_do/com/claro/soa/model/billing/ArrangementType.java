
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrangementType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ArrangementType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="GENERAL"/>
 *     &lt;enumeration value="COMMUNICATION"/>
 *     &lt;enumeration value="YELLOW_PAGES"/>
 *     &lt;enumeration value="OTHER"/>
 *     &lt;enumeration value="NOT_AVAILABLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ArrangementType")
@XmlEnum
public enum ArrangementType {

    GENERAL,
    COMMUNICATION,
    YELLOW_PAGES,
    OTHER,
    NOT_AVAILABLE;

    public String value() {
        return name();
    }

    public static ArrangementType fromValue(String v) {
        return valueOf(v);
    }

}
