
package _do.com.claro.soa.model.personidentity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PersonID complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonID">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idType" type="{http://www.claro.com.do/soa/model/personidentity}IDType"/>
 *         &lt;choice>
 *           &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}identityCard" minOccurs="0"/>
 *           &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}passport" minOccurs="0"/>
 *           &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}taxID" minOccurs="0"/>
 *           &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}workPermit" minOccurs="0"/>
 *           &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}other" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonID", propOrder = {
    "idType",
    "identityCard",
    "passport",
    "taxID",
    "workPermit",
    "other"
})
public class PersonID {

    @XmlElement(required = true)
    protected IDType idType;
    protected IdentityCard identityCard;
    protected Passport passport;
    protected TaxID taxID;
    protected WorkPermit workPermit;
    protected Other other;

    /**
     * Gets the value of the idType property.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getIdType() {
        return idType;
    }

    /**
     * Sets the value of the idType property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setIdType(IDType value) {
        this.idType = value;
    }

    /**
     * Gets the value of the identityCard property.
     * 
     * @return
     *     possible object is
     *     {@link IdentityCard }
     *     
     */
    public IdentityCard getIdentityCard() {
        return identityCard;
    }

    /**
     * Sets the value of the identityCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentityCard }
     *     
     */
    public void setIdentityCard(IdentityCard value) {
        this.identityCard = value;
    }

    /**
     * Gets the value of the passport property.
     * 
     * @return
     *     possible object is
     *     {@link Passport }
     *     
     */
    public Passport getPassport() {
        return passport;
    }

    /**
     * Sets the value of the passport property.
     * 
     * @param value
     *     allowed object is
     *     {@link Passport }
     *     
     */
    public void setPassport(Passport value) {
        this.passport = value;
    }

    /**
     * Gets the value of the taxID property.
     * 
     * @return
     *     possible object is
     *     {@link TaxID }
     *     
     */
    public TaxID getTaxID() {
        return taxID;
    }

    /**
     * Sets the value of the taxID property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxID }
     *     
     */
    public void setTaxID(TaxID value) {
        this.taxID = value;
    }

    /**
     * Gets the value of the workPermit property.
     * 
     * @return
     *     possible object is
     *     {@link WorkPermit }
     *     
     */
    public WorkPermit getWorkPermit() {
        return workPermit;
    }

    /**
     * Sets the value of the workPermit property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkPermit }
     *     
     */
    public void setWorkPermit(WorkPermit value) {
        this.workPermit = value;
    }

    /**
     * Gets the value of the other property.
     * 
     * @return
     *     possible object is
     *     {@link Other }
     *     
     */
    public Other getOther() {
        return other;
    }

    /**
     * Sets the value of the other property.
     * 
     * @param value
     *     allowed object is
     *     {@link Other }
     *     
     */
    public void setOther(Other value) {
        this.other = value;
    }

}
