
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AlternateContactMethodType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AlternateContactMethodType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EMAIL"/>
 *     &lt;enumeration value="FAX"/>
 *     &lt;enumeration value="HOUSE_PHONE_NUMBER"/>
 *     &lt;enumeration value="CELLPHONE"/>
 *     &lt;enumeration value="WORK_PHONE"/>
 *     &lt;enumeration value="WORK_PHONE_EXTENSION"/>
 *     &lt;enumeration value="SMS_NUMBER"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AlternateContactMethodType")
@XmlEnum
public enum AlternateContactMethodType {

    EMAIL,
    FAX,
    HOUSE_PHONE_NUMBER,
    CELLPHONE,
    WORK_PHONE,
    WORK_PHONE_EXTENSION,
    SMS_NUMBER;

    public String value() {
        return name();
    }

    public static AlternateContactMethodType fromValue(String v) {
        return valueOf(v);
    }

}
