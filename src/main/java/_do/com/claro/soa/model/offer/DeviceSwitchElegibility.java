
package _do.com.claro.soa.model.offer;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.generic.TelephoneNumber;


/**
 * <p>Java class for DeviceSwitchElegibility complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeviceSwitchElegibility">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subscriberNo" type="{http://www.claro.com.do/soa/model/generic}TelephoneNumber"/>
 *         &lt;element name="lastSwitchDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="currentEligibilityType" type="{http://www.claro.com.do/soa/model/offer}Eligibility"/>
 *         &lt;element name="upcomingEligiblities" type="{http://www.claro.com.do/soa/model/offer}UpcomingEligiblities"/>
 *         &lt;element name="deviceDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="switchGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="averageConsumptionSinceLastSwitch" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="bonus" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="hasFinancing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceSwitchElegibility", propOrder = {
    "subscriberNo",
    "lastSwitchDate",
    "currentEligibilityType",
    "upcomingEligiblities",
    "deviceDiscount",
    "switchGroup",
    "averageConsumptionSinceLastSwitch",
    "bonus",
    "hasFinancing"
})
public class DeviceSwitchElegibility {

    @XmlElement(required = true)
    protected TelephoneNumber subscriberNo;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastSwitchDate;
    @XmlElement(required = true, nillable = true)
    protected Eligibility currentEligibilityType;
    @XmlElement(required = true, nillable = true)
    protected UpcomingEligiblities upcomingEligiblities;
    @XmlElement(required = true)
    protected BigDecimal deviceDiscount;
    @XmlElement(required = true, nillable = true)
    protected String switchGroup;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal averageConsumptionSinceLastSwitch;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal bonus;
    protected boolean hasFinancing;

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumber }
     *     
     */
    public TelephoneNumber getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumber }
     *     
     */
    public void setSubscriberNo(TelephoneNumber value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the lastSwitchDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastSwitchDate() {
        return lastSwitchDate;
    }

    /**
     * Sets the value of the lastSwitchDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastSwitchDate(XMLGregorianCalendar value) {
        this.lastSwitchDate = value;
    }

    /**
     * Gets the value of the currentEligibilityType property.
     * 
     * @return
     *     possible object is
     *     {@link Eligibility }
     *     
     */
    public Eligibility getCurrentEligibilityType() {
        return currentEligibilityType;
    }

    /**
     * Sets the value of the currentEligibilityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Eligibility }
     *     
     */
    public void setCurrentEligibilityType(Eligibility value) {
        this.currentEligibilityType = value;
    }

    /**
     * Gets the value of the upcomingEligiblities property.
     * 
     * @return
     *     possible object is
     *     {@link UpcomingEligiblities }
     *     
     */
    public UpcomingEligiblities getUpcomingEligiblities() {
        return upcomingEligiblities;
    }

    /**
     * Sets the value of the upcomingEligiblities property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpcomingEligiblities }
     *     
     */
    public void setUpcomingEligiblities(UpcomingEligiblities value) {
        this.upcomingEligiblities = value;
    }

    /**
     * Gets the value of the deviceDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDeviceDiscount() {
        return deviceDiscount;
    }

    /**
     * Sets the value of the deviceDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDeviceDiscount(BigDecimal value) {
        this.deviceDiscount = value;
    }

    /**
     * Gets the value of the switchGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSwitchGroup() {
        return switchGroup;
    }

    /**
     * Sets the value of the switchGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSwitchGroup(String value) {
        this.switchGroup = value;
    }

    /**
     * Gets the value of the averageConsumptionSinceLastSwitch property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAverageConsumptionSinceLastSwitch() {
        return averageConsumptionSinceLastSwitch;
    }

    /**
     * Sets the value of the averageConsumptionSinceLastSwitch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAverageConsumptionSinceLastSwitch(BigDecimal value) {
        this.averageConsumptionSinceLastSwitch = value;
    }

    /**
     * Gets the value of the bonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBonus() {
        return bonus;
    }

    /**
     * Sets the value of the bonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBonus(BigDecimal value) {
        this.bonus = value;
    }

    /**
     * Gets the value of the hasFinancing property.
     * 
     */
    public boolean isHasFinancing() {
        return hasFinancing;
    }

    /**
     * Sets the value of the hasFinancing property.
     * 
     */
    public void setHasFinancing(boolean value) {
        this.hasFinancing = value;
    }

}
