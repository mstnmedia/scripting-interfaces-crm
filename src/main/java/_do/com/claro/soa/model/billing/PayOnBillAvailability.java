
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         @Created: SOA-268
 * 
 *         Represents if the customer applies to pay on bill
 *       
 * 
 * <p>Java class for PayOnBillAvailability complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayOnBillAvailability">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="payOnBill" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="payOnBillMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayOnBillAvailability", propOrder = {
    "payOnBill",
    "payOnBillMessage"
})
public class PayOnBillAvailability {

    protected boolean payOnBill;
    @XmlElement(required = true)
    protected String payOnBillMessage;

    /**
     * Gets the value of the payOnBill property.
     * 
     */
    public boolean isPayOnBill() {
        return payOnBill;
    }

    /**
     * Sets the value of the payOnBill property.
     * 
     */
    public void setPayOnBill(boolean value) {
        this.payOnBill = value;
    }

    /**
     * Gets the value of the payOnBillMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayOnBillMessage() {
        return payOnBillMessage;
    }

    /**
     * Sets the value of the payOnBillMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayOnBillMessage(String value) {
        this.payOnBillMessage = value;
    }

}
