
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INDIVIDUAL"/>
 *     &lt;enumeration value="SME"/>
 *     &lt;enumeration value="BUSINESS"/>
 *     &lt;enumeration value="GOVERNMENT"/>
 *     &lt;enumeration value="EXCEPTIONAL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CustomerType")
@XmlEnum
public enum CustomerType {

    INDIVIDUAL,
    SME,
    BUSINESS,
    GOVERNMENT,
    EXCEPTIONAL;

    public String value() {
        return name();
    }

    public static CustomerType fromValue(String v) {
        return valueOf(v);
    }

}
