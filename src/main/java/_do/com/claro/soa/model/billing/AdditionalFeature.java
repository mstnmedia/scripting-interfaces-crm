
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.generic.Money;


/**
 * 
 *         @Created: SOA-210
 *         An AdditionalFeature is an extra service requested by de customer not included
 *         in the features of his plan
 *         An AdditionalFeature is composed by the following:
 *         -level: indicates if the SOC is linked to the BAN or if is linked to the Subscriber.
 *         -type: indicates the service type (Regular,Plan,Promocion, Etc).
 *         -soc: represents the code of the service.
 *         -Description: is a description of the service.
 *         -rentAmount: represents the rent, this is a double value.
 *         -oneTimeChargeAmount: represents a one time charge applied to the customer.
 *         -serviceActivationDate: represents the service activation date.
 *         -serviceExpirationDate: represents the date on which the service will end, this field can be null.
 *         -dealerCode: represents the dealer code.
 *         -convrType: represents the service type of coverage.
 *       
 * 
 * <p>Java class for AdditionalFeature complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdditionalFeature">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="level" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="soc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rentAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *         &lt;element name="oneTimeChargeAmount" type="{http://www.claro.com.do/soa/model/generic}Money"/>
 *         &lt;element name="serviceActivationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="serviceExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dealerCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="coverageType" type="{http://www.claro.com.do/soa/model/billing}featureCoverage"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalFeature", propOrder = {
    "level",
    "type",
    "soc",
    "description",
    "rentAmount",
    "oneTimeChargeAmount",
    "serviceActivationDate",
    "serviceExpirationDate",
    "dealerCode",
    "coverageType"
})
public class AdditionalFeature {

    @XmlElement(required = true, nillable = true)
    protected String level;
    @XmlElement(required = true, nillable = true)
    protected String type;
    @XmlElement(required = true, nillable = true)
    protected String soc;
    @XmlElement(required = true, nillable = true)
    protected String description;
    @XmlElement(required = true, nillable = true)
    protected Money rentAmount;
    @XmlElement(required = true, nillable = true)
    protected Money oneTimeChargeAmount;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar serviceActivationDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar serviceExpirationDate;
    @XmlElement(required = true, nillable = true)
    protected String dealerCode;
    @XmlElement(required = true, nillable = true)
    protected FeatureCoverage coverageType;

    /**
     * Gets the value of the level property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevel() {
        return level;
    }

    /**
     * Sets the value of the level property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevel(String value) {
        this.level = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the soc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoc() {
        return soc;
    }

    /**
     * Sets the value of the soc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoc(String value) {
        this.soc = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the rentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getRentAmount() {
        return rentAmount;
    }

    /**
     * Sets the value of the rentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setRentAmount(Money value) {
        this.rentAmount = value;
    }

    /**
     * Gets the value of the oneTimeChargeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getOneTimeChargeAmount() {
        return oneTimeChargeAmount;
    }

    /**
     * Sets the value of the oneTimeChargeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setOneTimeChargeAmount(Money value) {
        this.oneTimeChargeAmount = value;
    }

    /**
     * Gets the value of the serviceActivationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServiceActivationDate() {
        return serviceActivationDate;
    }

    /**
     * Sets the value of the serviceActivationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServiceActivationDate(XMLGregorianCalendar value) {
        this.serviceActivationDate = value;
    }

    /**
     * Gets the value of the serviceExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServiceExpirationDate() {
        return serviceExpirationDate;
    }

    /**
     * Sets the value of the serviceExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServiceExpirationDate(XMLGregorianCalendar value) {
        this.serviceExpirationDate = value;
    }

    /**
     * Gets the value of the dealerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerCode() {
        return dealerCode;
    }

    /**
     * Sets the value of the dealerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerCode(String value) {
        this.dealerCode = value;
    }

    /**
     * Gets the value of the coverageType property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureCoverage }
     *     
     */
    public FeatureCoverage getCoverageType() {
        return coverageType;
    }

    /**
     * Sets the value of the coverageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureCoverage }
     *     
     */
    public void setCoverageType(FeatureCoverage value) {
        this.coverageType = value;
    }

}
