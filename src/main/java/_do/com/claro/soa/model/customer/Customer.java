
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.employee.Employee;
import _do.com.claro.soa.model.generic.TelephoneNumber;
import _do.com.claro.soa.model.product.Products;


/**
 * <p>Java class for Customer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Customer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}customerID"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/generic}phone"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/generic}fax"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="taxID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}customerSegment"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}customerType"/>
 *         &lt;element name="legalName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sicCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comercialSector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comercialActivity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="website" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}contacts" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/product}products" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}accountOfficer" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}accountExecutives" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/customer}additionalContacts" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer", propOrder = {
    "customerID",
    "name",
    "phone",
    "fax",
    "email",
    "taxID",
    "customerSegment",
    "customerType",
    "legalName",
    "sicCode",
    "comercialSector",
    "comercialActivity",
    "website",
    "contacts",
    "products",
    "accountOfficer",
    "accountExecutives",
    "additionalContacts"
})
public class Customer {

    @XmlElement(required = true)
    protected CustomerID customerID;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/generic", required = true, nillable = true)
    protected TelephoneNumber phone;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/generic", required = true, nillable = true)
    protected TelephoneNumber fax;
    @XmlElement(required = true, nillable = true)
    protected String email;
    @XmlElement(required = true, nillable = true)
    protected String taxID;
    @XmlElement(required = true, nillable = true)
    protected CustomerSegment customerSegment;
    @XmlElement(required = true, nillable = true)
    protected CustomerType customerType;
    @XmlElement(required = true, nillable = true)
    protected String legalName;
    @XmlElement(required = true, nillable = true)
    protected String sicCode;
    @XmlElement(required = true, nillable = true)
    protected String comercialSector;
    @XmlElement(required = true, nillable = true)
    protected String comercialActivity;
    @XmlElement(required = true, nillable = true)
    protected String website;
    @XmlElement(nillable = true)
    protected Contacts contacts;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/product", nillable = true)
    protected Products products;
    @XmlElement(nillable = true)
    protected Employee accountOfficer;
    @XmlElement(nillable = true)
    protected AccountExecutives accountExecutives;
    @XmlElement(nillable = true)
    protected AdditionalContacts additionalContacts;

    /**
     * Gets the value of the customerID property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerID }
     *     
     */
    public CustomerID getCustomerID() {
        return customerID;
    }

    /**
     * Sets the value of the customerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerID }
     *     
     */
    public void setCustomerID(CustomerID value) {
        this.customerID = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumber }
     *     
     */
    public TelephoneNumber getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumber }
     *     
     */
    public void setPhone(TelephoneNumber value) {
        this.phone = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneNumber }
     *     
     */
    public TelephoneNumber getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneNumber }
     *     
     */
    public void setFax(TelephoneNumber value) {
        this.fax = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the taxID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxID() {
        return taxID;
    }

    /**
     * Sets the value of the taxID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxID(String value) {
        this.taxID = value;
    }

    /**
     * Gets the value of the customerSegment property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerSegment }
     *     
     */
    public CustomerSegment getCustomerSegment() {
        return customerSegment;
    }

    /**
     * Sets the value of the customerSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerSegment }
     *     
     */
    public void setCustomerSegment(CustomerSegment value) {
        this.customerSegment = value;
    }

    /**
     * Gets the value of the customerType property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerType }
     *     
     */
    public CustomerType getCustomerType() {
        return customerType;
    }

    /**
     * Sets the value of the customerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerType }
     *     
     */
    public void setCustomerType(CustomerType value) {
        this.customerType = value;
    }

    /**
     * Gets the value of the legalName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegalName() {
        return legalName;
    }

    /**
     * Sets the value of the legalName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegalName(String value) {
        this.legalName = value;
    }

    /**
     * Gets the value of the sicCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSicCode() {
        return sicCode;
    }

    /**
     * Sets the value of the sicCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSicCode(String value) {
        this.sicCode = value;
    }

    /**
     * Gets the value of the comercialSector property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComercialSector() {
        return comercialSector;
    }

    /**
     * Sets the value of the comercialSector property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComercialSector(String value) {
        this.comercialSector = value;
    }

    /**
     * Gets the value of the comercialActivity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComercialActivity() {
        return comercialActivity;
    }

    /**
     * Sets the value of the comercialActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComercialActivity(String value) {
        this.comercialActivity = value;
    }

    /**
     * Gets the value of the website property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebsite() {
        return website;
    }

    /**
     * Sets the value of the website property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebsite(String value) {
        this.website = value;
    }

    /**
     * Gets the value of the contacts property.
     * 
     * @return
     *     possible object is
     *     {@link Contacts }
     *     
     */
    public Contacts getContacts() {
        return contacts;
    }

    /**
     * Sets the value of the contacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Contacts }
     *     
     */
    public void setContacts(Contacts value) {
        this.contacts = value;
    }

    /**
     * Gets the value of the products property.
     * 
     * @return
     *     possible object is
     *     {@link Products }
     *     
     */
    public Products getProducts() {
        return products;
    }

    /**
     * Sets the value of the products property.
     * 
     * @param value
     *     allowed object is
     *     {@link Products }
     *     
     */
    public void setProducts(Products value) {
        this.products = value;
    }

    /**
     * Gets the value of the accountOfficer property.
     * 
     * @return
     *     possible object is
     *     {@link Employee }
     *     
     */
    public Employee getAccountOfficer() {
        return accountOfficer;
    }

    /**
     * Sets the value of the accountOfficer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Employee }
     *     
     */
    public void setAccountOfficer(Employee value) {
        this.accountOfficer = value;
    }

    /**
     * Gets the value of the accountExecutives property.
     * 
     * @return
     *     possible object is
     *     {@link AccountExecutives }
     *     
     */
    public AccountExecutives getAccountExecutives() {
        return accountExecutives;
    }

    /**
     * Sets the value of the accountExecutives property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountExecutives }
     *     
     */
    public void setAccountExecutives(AccountExecutives value) {
        this.accountExecutives = value;
    }

    /**
     * Gets the value of the additionalContacts property.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalContacts }
     *     
     */
    public AdditionalContacts getAdditionalContacts() {
        return additionalContacts;
    }

    /**
     * Sets the value of the additionalContacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalContacts }
     *     
     */
    public void setAdditionalContacts(AdditionalContacts value) {
        this.additionalContacts = value;
    }

}
