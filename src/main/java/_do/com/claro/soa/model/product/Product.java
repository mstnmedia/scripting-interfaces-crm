
package _do.com.claro.soa.model.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import _do.com.claro.soa.model.address.Address;
import _do.com.claro.soa.model.billing.BillingAccount;
import _do.com.claro.soa.model.billing.Subscription;


/**
 * 
 *         @Created: genesis...
 * 
 *         @Modified: SOA-162
 *         The extensions made to Product, specifically the 'component' hierarchy, closely resemble the data model
 *         implemented in Amdocs OMS. Generalizing a canonical product model for all product definitions was out of
 *         scope due to the heterogenous schemas employed by the different catalog systems (PPG, Ensemble, OMS) and 
 *         the limited resources and domain knowledge invested in this requirement.
 * 
 *         For more details regarding this design and the analysis that led up to it, please read the solution design
 *         document for requirement SOA-162.
 * 
 *         Note: a product's status is obtained by querying its component's ComponentStatus.
 *       
 * 
 * <p>Java class for Product complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Product">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/product}productType"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/address}address" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}billingAccount" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}subscription" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/product}offer" minOccurs="0"/>
 *         &lt;element name="installationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/product}component" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Product", propOrder = {
    "productType",
    "address",
    "billingAccount",
    "subscription",
    "offer",
    "installationDate",
    "component"
})
public class Product {

    @XmlElement(required = true)
    protected ProductType productType;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/address", nillable = true)
    protected Address address;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/billing")
    protected BillingAccount billingAccount;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/billing", nillable = true)
    protected Subscription subscription;
    protected Offer offer;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar installationDate;
    protected Component component;

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link ProductType }
     *     
     */
    public ProductType getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductType }
     *     
     */
    public void setProductType(ProductType value) {
        this.productType = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the billingAccount property.
     * 
     * @return
     *     possible object is
     *     {@link BillingAccount }
     *     
     */
    public BillingAccount getBillingAccount() {
        return billingAccount;
    }

    /**
     * Sets the value of the billingAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingAccount }
     *     
     */
    public void setBillingAccount(BillingAccount value) {
        this.billingAccount = value;
    }

    /**
     * Gets the value of the subscription property.
     * 
     * @return
     *     possible object is
     *     {@link Subscription }
     *     
     */
    public Subscription getSubscription() {
        return subscription;
    }

    /**
     * Sets the value of the subscription property.
     * 
     * @param value
     *     allowed object is
     *     {@link Subscription }
     *     
     */
    public void setSubscription(Subscription value) {
        this.subscription = value;
    }

    /**
     * Gets the value of the offer property.
     * 
     * @return
     *     possible object is
     *     {@link Offer }
     *     
     */
    public Offer getOffer() {
        return offer;
    }

    /**
     * Sets the value of the offer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Offer }
     *     
     */
    public void setOffer(Offer value) {
        this.offer = value;
    }

    /**
     * Gets the value of the installationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInstallationDate() {
        return installationDate;
    }

    /**
     * Sets the value of the installationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInstallationDate(XMLGregorianCalendar value) {
        this.installationDate = value;
    }

    /**
     * Gets the value of the component property.
     * 
     * @return
     *     possible object is
     *     {@link Component }
     *     
     */
    public Component getComponent() {
        return component;
    }

    /**
     * Sets the value of the component property.
     * 
     * @param value
     *     allowed object is
     *     {@link Component }
     *     
     */
    public void setComponent(Component value) {
        this.component = value;
    }

}
