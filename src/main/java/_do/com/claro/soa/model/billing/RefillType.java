
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RefillType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RefillType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="VALUE_VOUCHER"/>
 *     &lt;enumeration value="STANDARD_REFILL_VOUCHER"/>
 *     &lt;enumeration value="REFILL"/>
 *     &lt;enumeration value="GRANTED_LOAN"/>
 *     &lt;enumeration value="REFILL_WITH_CHARGED_LOAN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RefillType")
@XmlEnum
public enum RefillType {

    VALUE_VOUCHER,
    STANDARD_REFILL_VOUCHER,

    /**
     * 
     *             @Created: SOA-75
     *             Type "Recarga". This type represents the normal workflow of a refill. The customer refill its mobile phone 
     *             through MiClaro, a CAC (Centro Antecion a Cliente), a supermarket retail cashier or any other traditional method.
     *           
     * 
     */
    REFILL,

    /**
     * 
     *             @Created: SOA-75
     *             Type "Prestamo otorgado". This type represents the scenario where the customer needs to refill its mobile phone and due to some reason
     *             the customer prefers to pay it later.
     *           
     * 
     */
    GRANTED_LOAN,

    /**
     * 
     *             @Created: SOA-75
     *             Type "Recarga con prestamo cobrado". This type represents the scenario where the customer follows the normal flow to perform a refill
     *             and the loan taken previously is debited from the amount refilled.
     *           
     * 
     */
    REFILL_WITH_CHARGED_LOAN;

    public String value() {
        return name();
    }

    public static RefillType fromValue(String v) {
        return valueOf(v);
    }

}
