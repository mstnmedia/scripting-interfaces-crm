
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingAccountStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BillingAccountStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OPEN"/>
 *     &lt;enumeration value="CLOSED"/>
 *     &lt;enumeration value="TENTATIVE"/>
 *     &lt;enumeration value="RESERVED"/>
 *     &lt;enumeration value="SUSPENDED"/>
 *     &lt;enumeration value="CANCELLED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BillingAccountStatus")
@XmlEnum
public enum BillingAccountStatus {

    OPEN,
    CLOSED,
    TENTATIVE,

    /**
     * 
     *             Reserved status is used in case of customer layaway of a BillingAccount.
     *           
     * 
     */
    RESERVED,
    SUSPENDED,
    CANCELLED;

    public String value() {
        return name();
    }

    public static BillingAccountStatus fromValue(String v) {
        return valueOf(v);
    }

}
