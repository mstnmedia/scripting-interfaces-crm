
package _do.com.claro.soa.model.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *        @Created: SOA-162
 *        A product's component defines its technical specifications and may also
 *        be composed of additional components recursively. The position of each component in the
 *        resulting hierarchy then establishes context for the meaning of the component and its attributes.
 * 
 *        Each component has a status and not all of a product's components may be ACTIVE. It is up to the caller
 *        to select the components of interest. Generally, if a product is ACTIVE then at least some components 
 *        will be ACTIVE as well (although others may be CEASED). If the product is SUSPENDED then at least some 
 *        components will be SUSPENDED and some may be CEASED. If the product is CEASED then all components will
 *        be CEASED.
 *      
 * 
 * <p>Java class for Component complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Component">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/product}catalogDefinition"/>
 *         &lt;element name="componentType">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="status" type="{http://www.claro.com.do/soa/model/product}ComponentStatus"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/product}productAttributes"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/product}components"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Component", propOrder = {
    "catalogDefinition",
    "componentType",
    "status",
    "productAttributes",
    "components"
})
public class Component {

    @XmlElement(required = true)
    protected CatalogDefinition catalogDefinition;
    @XmlElement(required = true)
    protected Component.ComponentType componentType;
    @XmlElement(required = true)
    protected ComponentStatus status;
    @XmlElement(required = true, nillable = true)
    protected ProductAttributes productAttributes;
    @XmlElement(required = true, nillable = true)
    protected Components components;

    /**
     * Gets the value of the catalogDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link CatalogDefinition }
     *     
     */
    public CatalogDefinition getCatalogDefinition() {
        return catalogDefinition;
    }

    /**
     * Sets the value of the catalogDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link CatalogDefinition }
     *     
     */
    public void setCatalogDefinition(CatalogDefinition value) {
        this.catalogDefinition = value;
    }

    /**
     * Gets the value of the componentType property.
     * 
     * @return
     *     possible object is
     *     {@link Component.ComponentType }
     *     
     */
    public Component.ComponentType getComponentType() {
        return componentType;
    }

    /**
     * Sets the value of the componentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Component.ComponentType }
     *     
     */
    public void setComponentType(Component.ComponentType value) {
        this.componentType = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ComponentStatus }
     *     
     */
    public ComponentStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentStatus }
     *     
     */
    public void setStatus(ComponentStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the productAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link ProductAttributes }
     *     
     */
    public ProductAttributes getProductAttributes() {
        return productAttributes;
    }

    /**
     * Sets the value of the productAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductAttributes }
     *     
     */
    public void setProductAttributes(ProductAttributes value) {
        this.productAttributes = value;
    }

    /**
     * Gets the value of the components property.
     * 
     * @return
     *     possible object is
     *     {@link Components }
     *     
     */
    public Components getComponents() {
        return components;
    }

    /**
     * Sets the value of the components property.
     * 
     * @param value
     *     allowed object is
     *     {@link Components }
     *     
     */
    public void setComponents(Components value) {
        this.components = value;
    }


    /**
     * 
     *             @Created: SOA-203
     *             Analogous to Product.productType, this field indicates the component's type.
     *           
     * 
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "description"
    })
    public static class ComponentType {

        @XmlElement(required = true)
        protected String code;
        @XmlElement(required = true)
        protected String description;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }

}
