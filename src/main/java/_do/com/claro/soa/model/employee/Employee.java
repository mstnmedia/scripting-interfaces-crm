
package _do.com.claro.soa.model.employee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import _do.com.claro.soa.model.customer.AccountExecutive;
import _do.com.claro.soa.model.personidentity.Names;


/**
 * 
 *       The canonical representation of an employee.
 *     
 * 
 * <p>Java class for Employee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Employee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/employee}employeeId"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/personidentity}names"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/generic}phoneString"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/generic}faxString"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Employee", propOrder = {
    "employeeId",
    "names",
    "phoneString",
    "faxString",
    "email"
})
@XmlSeeAlso({
    AccountExecutive.class
})
public class Employee {

    @XmlElement(required = true, nillable = true)
    protected String employeeId;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/personidentity", required = true)
    protected Names names;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/generic", required = true, nillable = true)
    protected String phoneString;
    @XmlElement(namespace = "http://www.claro.com.do/soa/model/generic", required = true, nillable = true)
    protected String faxString;
    @XmlElement(required = true, nillable = true)
    protected String email;

    /**
     * Gets the value of the employeeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * Sets the value of the employeeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeId(String value) {
        this.employeeId = value;
    }

    /**
     * Gets the value of the names property.
     * 
     * @return
     *     possible object is
     *     {@link Names }
     *     
     */
    public Names getNames() {
        return names;
    }

    /**
     * Sets the value of the names property.
     * 
     * @param value
     *     allowed object is
     *     {@link Names }
     *     
     */
    public void setNames(Names value) {
        this.names = value;
    }

    /**
     * Gets the value of the phoneString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneString() {
        return phoneString;
    }

    /**
     * Sets the value of the phoneString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneString(String value) {
        this.phoneString = value;
    }

    /**
     * Gets the value of the faxString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxString() {
        return faxString;
    }

    /**
     * Sets the value of the faxString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxString(String value) {
        this.faxString = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

}
