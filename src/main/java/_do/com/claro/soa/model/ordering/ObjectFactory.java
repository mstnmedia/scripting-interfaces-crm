
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.model.ordering package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Orders_QNAME = new QName("http://www.claro.com.do/soa/model/ordering", "orders");
    private final static QName _Order_QNAME = new QName("http://www.claro.com.do/soa/model/ordering", "order");
    private final static QName _OrderActions_QNAME = new QName("http://www.claro.com.do/soa/model/ordering", "orderActions");
    private final static QName _OrderAction_QNAME = new QName("http://www.claro.com.do/soa/model/ordering", "orderAction");
    private final static QName _DisplayID_QNAME = new QName("http://www.claro.com.do/soa/model/ordering", "displayID");
    private final static QName _SalesChannel_QNAME = new QName("http://www.claro.com.do/soa/model/ordering", "salesChannel");
    private final static QName _Pricing_QNAME = new QName("http://www.claro.com.do/soa/model/ordering", "pricing");
    private final static QName _Comments_QNAME = new QName("http://www.claro.com.do/soa/model/ordering", "comments");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.model.ordering
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OrderActions }
     * 
     */
    public OrderActions createOrderActions() {
        return new OrderActions();
    }

    /**
     * Create an instance of {@link Orders }
     * 
     */
    public Orders createOrders() {
        return new Orders();
    }

    /**
     * Create an instance of {@link Phases }
     * 
     */
    public Phases createPhases() {
        return new Phases();
    }

    /**
     * Create an instance of {@link OrderAction.Reason }
     * 
     */
    public OrderAction.Reason createOrderActionReason() {
        return new OrderAction.Reason();
    }

    /**
     * Create an instance of {@link Phase }
     * 
     */
    public Phase createPhase() {
        return new Phase();
    }

    /**
     * Create an instance of {@link OrderID }
     * 
     */
    public OrderID createOrderID() {
        return new OrderID();
    }

    /**
     * Create an instance of {@link OrderActivities }
     * 
     */
    public OrderActivities createOrderActivities() {
        return new OrderActivities();
    }

    /**
     * Create an instance of {@link Comment }
     * 
     */
    public Comment createComment() {
        return new Comment();
    }

    /**
     * Create an instance of {@link OrderActionID }
     * 
     */
    public OrderActionID createOrderActionID() {
        return new OrderActionID();
    }

    /**
     * Create an instance of {@link OrderActivity }
     * 
     */
    public OrderActivity createOrderActivity() {
        return new OrderActivity();
    }

    /**
     * Create an instance of {@link OrderAction.ProductSnapshot }
     * 
     */
    public OrderAction.ProductSnapshot createOrderActionProductSnapshot() {
        return new OrderAction.ProductSnapshot();
    }

    /**
     * Create an instance of {@link Pricing.PricingElement }
     * 
     */
    public Pricing.PricingElement createPricingPricingElement() {
        return new Pricing.PricingElement();
    }

    /**
     * Create an instance of {@link Comments }
     * 
     */
    public Comments createComments() {
        return new Comments();
    }

    /**
     * Create an instance of {@link OrderAction }
     * 
     */
    public OrderAction createOrderAction() {
        return new OrderAction();
    }

    /**
     * Create an instance of {@link Pricing }
     * 
     */
    public Pricing createPricing() {
        return new Pricing();
    }

    /**
     * Create an instance of {@link Order }
     * 
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Create an instance of {@link Creator }
     * 
     */
    public Creator createCreator() {
        return new Creator();
    }

    /**
     * Create an instance of {@link SalesChannel }
     * 
     */
    public SalesChannel createSalesChannel() {
        return new SalesChannel();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Orders }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/ordering", name = "orders")
    public JAXBElement<Orders> createOrders(Orders value) {
        return new JAXBElement<Orders>(_Orders_QNAME, Orders.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Order }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/ordering", name = "order")
    public JAXBElement<Order> createOrder(Order value) {
        return new JAXBElement<Order>(_Order_QNAME, Order.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderActions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/ordering", name = "orderActions")
    public JAXBElement<OrderActions> createOrderActions(OrderActions value) {
        return new JAXBElement<OrderActions>(_OrderActions_QNAME, OrderActions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderAction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/ordering", name = "orderAction")
    public JAXBElement<OrderAction> createOrderAction(OrderAction value) {
        return new JAXBElement<OrderAction>(_OrderAction_QNAME, OrderAction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/ordering", name = "displayID")
    public JAXBElement<String> createDisplayID(String value) {
        return new JAXBElement<String>(_DisplayID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesChannel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/ordering", name = "salesChannel")
    public JAXBElement<SalesChannel> createSalesChannel(SalesChannel value) {
        return new JAXBElement<SalesChannel>(_SalesChannel_QNAME, SalesChannel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Pricing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/ordering", name = "pricing")
    public JAXBElement<Pricing> createPricing(Pricing value) {
        return new JAXBElement<Pricing>(_Pricing_QNAME, Pricing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Comments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.claro.com.do/soa/model/ordering", name = "comments")
    public JAXBElement<Comments> createComments(Comments value) {
        return new JAXBElement<Comments>(_Comments_QNAME, Comments.class, null, value);
    }

}
