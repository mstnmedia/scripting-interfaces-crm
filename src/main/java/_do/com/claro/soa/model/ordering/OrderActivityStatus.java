
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderActivityStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderActivityStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACTIVATED"/>
 *     &lt;enumeration value="CANCELLED"/>
 *     &lt;enumeration value="COMPLETED"/>
 *     &lt;enumeration value="FAILED"/>
 *     &lt;enumeration value="LOADED"/>
 *     &lt;enumeration value="OVER_DUE"/>
 *     &lt;enumeration value="READY_FOR_EXECUTION"/>
 *     &lt;enumeration value="RJ"/>
 *     &lt;enumeration value="SEMI_FINAL"/>
 *     &lt;enumeration value="SUSPENDED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderActivityStatus")
@XmlEnum
public enum OrderActivityStatus {


    /**
     * 
     *           The activity was loaded and activated.
     *           @see "LOADED"
     *         
     * 
     */
    ACTIVATED,

    /**
     * 
     *           The activity was cancelled.
     *         
     * 
     */
    CANCELLED,

    /**
     * 
     *           The activity was successfully executed and finished.
     *         
     * 
     */
    COMPLETED,

    /**
     * 
     *           The activity's execution failed.
     *         
     * 
     */
    FAILED,

    /**
     * 
     *           The activity was loaded and is ready to be activated.
     *         
     * 
     */
    LOADED,

    /**
     * 
     *           The activity's execution is overdue (see orderActivity.dueDate).
     *         
     * 
     */
    OVER_DUE,

    /**
     * 
     *           The activity has been loaded, activated, and is ready to be executed.
     *         
     * 
     */
    READY_FOR_EXECUTION,

    /**
     * 
     *           Not used.
     *           (no idea what this is!)
     *         
     * 
     */
    RJ,

    /**
     * 
     *           The first half of the activity has been executed successfully and the activity is now
     *           waiting for a response from some external system.
     *         
     * 
     */
    SEMI_FINAL,

    /**
     * 
     *           Execution of the activity has been suspended.
     *         
     * 
     */
    SUSPENDED;

    public String value() {
        return name();
    }

    public static OrderActivityStatus fromValue(String v) {
        return valueOf(v);
    }

}
