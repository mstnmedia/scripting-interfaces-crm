
package _do.com.claro.soa.model.ordering;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CANCELLED"/>
 *     &lt;enumeration value="DISCONTINUED"/>
 *     &lt;enumeration value="DONE"/>
 *     &lt;enumeration value="EXECUTING"/>
 *     &lt;enumeration value="FUTURE"/>
 *     &lt;enumeration value="INITIAL"/>
 *     &lt;enumeration value="REJECT"/>
 *     &lt;enumeration value="TO_BE_CANCELLED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderStatus")
@XmlEnum
public enum OrderStatus {


    /**
     * 
     *           The order has been cancelled.
     *           This is a final status.
     *         
     * 
     */
    CANCELLED,

    /**
     * 
     *           The order has been cancelled without notifying the backend systems.
     *         
     * 
     */
    DISCONTINUED,

    /**
     * 
     *           The order has been completed.
     *           This is a final status.
     *         
     * 
     */
    DONE,

    /**
     * 
     *           The order's order actions are currently in either the NEGOTIATION phase, the DELIVERY phase
     *           or the COMPLETION phase.
     *         
     * 
     */
    EXECUTING,

    /**
     * 
     *           Not used.
     *         
     * 
     */
    FUTURE,

    /**
     * 
     *           The order's order actions have just been created ("initialized") but not yet configured.         
     *         
     * 
     */
    INITIAL,

    /**
     * 
     *           Not used.
     *         
     * 
     */
    REJECT,

    /**
     * 
     *           The order is in the process of being cancelled.
     *         
     * 
     */
    TO_BE_CANCELLED;

    public String value() {
        return name();
    }

    public static OrderStatus fromValue(String v) {
        return valueOf(v);
    }

}
