
package _do.com.claro.soa.model.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillingPlan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SOC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}planFeatures"/>
 *         &lt;element ref="{http://www.claro.com.do/soa/model/billing}additionalFeatures"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingPlan", propOrder = {
    "soc",
    "description",
    "planFeatures",
    "additionalFeatures"
})
public class BillingPlan {

    @XmlElement(name = "SOC", required = true)
    protected String soc;
    @XmlElement(required = true)
    protected String description;
    @XmlElement(required = true, nillable = true)
    protected PlanFeatures planFeatures;
    @XmlElement(required = true, nillable = true)
    protected AdditionalFeatures additionalFeatures;

    /**
     * Gets the value of the soc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOC() {
        return soc;
    }

    /**
     * Sets the value of the soc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOC(String value) {
        this.soc = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the planFeatures property.
     * 
     * @return
     *     possible object is
     *     {@link PlanFeatures }
     *     
     */
    public PlanFeatures getPlanFeatures() {
        return planFeatures;
    }

    /**
     * Sets the value of the planFeatures property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanFeatures }
     *     
     */
    public void setPlanFeatures(PlanFeatures value) {
        this.planFeatures = value;
    }

    /**
     * Gets the value of the additionalFeatures property.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalFeatures }
     *     
     */
    public AdditionalFeatures getAdditionalFeatures() {
        return additionalFeatures;
    }

    /**
     * Sets the value of the additionalFeatures property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalFeatures }
     *     
     */
    public void setAdditionalFeatures(AdditionalFeatures value) {
        this.additionalFeatures = value;
    }

}
