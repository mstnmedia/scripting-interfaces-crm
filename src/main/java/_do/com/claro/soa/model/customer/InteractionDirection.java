
package _do.com.claro.soa.model.customer;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InteractionDirection.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InteractionDirection">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNKNOWN"/>
 *     &lt;enumeration value="INBOUND"/>
 *     &lt;enumeration value="OUTBOUND"/>
 *     &lt;enumeration value="BOTH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InteractionDirection")
@XmlEnum
public enum InteractionDirection {

    UNKNOWN,
    INBOUND,
    OUTBOUND,
    BOTH;

    public String value() {
        return name();
    }

    public static InteractionDirection fromValue(String v) {
        return valueOf(v);
    }

}
