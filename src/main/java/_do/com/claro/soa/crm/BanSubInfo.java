
package _do.com.claro.soa.crm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BanSubInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BanSubInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BAN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BanStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BanStatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustomerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubscriberNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubscriberStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubscriberStatusDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SubscriberStatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BanSubInfo", propOrder = {
    "ban",
    "banStatus",
    "banStatusDescription",
    "customerId",
    "productType",
    "productTypeDescription",
    "subscriberNo",
    "subscriberStatus",
    "subscriberStatusDate",
    "subscriberStatusDescription"
})
public class BanSubInfo {

    @XmlElement(name = "BAN", required = true)
    protected String ban;
    @XmlElement(name = "BanStatus", required = true)
    protected String banStatus;
    @XmlElement(name = "BanStatusDescription", required = true, nillable = true)
    protected String banStatusDescription;
    @XmlElement(name = "CustomerId", required = true, nillable = true)
    protected String customerId;
    @XmlElement(name = "ProductType", required = true, nillable = true)
    protected String productType;
    @XmlElement(name = "ProductTypeDescription", required = true, nillable = true)
    protected String productTypeDescription;
    @XmlElement(name = "SubscriberNo", required = true)
    protected String subscriberNo;
    @XmlElement(name = "SubscriberStatus", required = true)
    protected String subscriberStatus;
    @XmlElement(name = "SubscriberStatusDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar subscriberStatusDate;
    @XmlElement(name = "SubscriberStatusDescription", required = true, nillable = true)
    protected String subscriberStatusDescription;

    /**
     * Gets the value of the ban property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBAN() {
        return ban;
    }

    /**
     * Sets the value of the ban property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBAN(String value) {
        this.ban = value;
    }

    /**
     * Gets the value of the banStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanStatus() {
        return banStatus;
    }

    /**
     * Sets the value of the banStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanStatus(String value) {
        this.banStatus = value;
    }

    /**
     * Gets the value of the banStatusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanStatusDescription() {
        return banStatusDescription;
    }

    /**
     * Sets the value of the banStatusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanStatusDescription(String value) {
        this.banStatusDescription = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the productTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductTypeDescription() {
        return productTypeDescription;
    }

    /**
     * Sets the value of the productTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductTypeDescription(String value) {
        this.productTypeDescription = value;
    }

    /**
     * Gets the value of the subscriberNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberNo() {
        return subscriberNo;
    }

    /**
     * Sets the value of the subscriberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberNo(String value) {
        this.subscriberNo = value;
    }

    /**
     * Gets the value of the subscriberStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberStatus() {
        return subscriberStatus;
    }

    /**
     * Sets the value of the subscriberStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberStatus(String value) {
        this.subscriberStatus = value;
    }

    /**
     * Gets the value of the subscriberStatusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSubscriberStatusDate() {
        return subscriberStatusDate;
    }

    /**
     * Sets the value of the subscriberStatusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSubscriberStatusDate(XMLGregorianCalendar value) {
        this.subscriberStatusDate = value;
    }

    /**
     * Gets the value of the subscriberStatusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberStatusDescription() {
        return subscriberStatusDescription;
    }

    /**
     * Sets the value of the subscriberStatusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberStatusDescription(String value) {
        this.subscriberStatusDescription = value;
    }

}
