
package _do.com.claro.soa.crm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CloseCaseRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CloseCaseRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="caseId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="technicianId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dispositionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subDispositionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="causeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subCauseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adjustmentAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="adjustmentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="maintenanceContract" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="resolution" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sysReq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CloseCaseRequest", propOrder = {
    "caseId",
    "technicianId",
    "dispositionCode",
    "subDispositionCode",
    "causeCode",
    "subCauseCode",
    "adjustmentAmount",
    "adjustmentType",
    "maintenanceContract",
    "notes",
    "resolution",
    "status",
    "sysReq",
    "clientSystem"
})
public class CloseCaseRequest {

    @XmlElement(required = true)
    protected String caseId;
    @XmlElement(required = true)
    protected String technicianId;
    @XmlElement(required = true)
    protected String dispositionCode;
    @XmlElement(required = true)
    protected String subDispositionCode;
    @XmlElement(required = true)
    protected String causeCode;
    @XmlElement(required = true)
    protected String subCauseCode;
    protected double adjustmentAmount;
    @XmlElement(required = true)
    protected String adjustmentType;
    protected boolean maintenanceContract;
    @XmlElement(required = true)
    protected String notes;
    @XmlElement(required = true)
    protected String resolution;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    protected String sysReq;
    @XmlElement(required = true)
    protected String clientSystem;

    /**
     * Gets the value of the caseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseId() {
        return caseId;
    }

    /**
     * Sets the value of the caseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseId(String value) {
        this.caseId = value;
    }

    /**
     * Gets the value of the technicianId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicianId() {
        return technicianId;
    }

    /**
     * Sets the value of the technicianId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicianId(String value) {
        this.technicianId = value;
    }

    /**
     * Gets the value of the dispositionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispositionCode() {
        return dispositionCode;
    }

    /**
     * Sets the value of the dispositionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispositionCode(String value) {
        this.dispositionCode = value;
    }

    /**
     * Gets the value of the subDispositionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubDispositionCode() {
        return subDispositionCode;
    }

    /**
     * Sets the value of the subDispositionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubDispositionCode(String value) {
        this.subDispositionCode = value;
    }

    /**
     * Gets the value of the causeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCauseCode() {
        return causeCode;
    }

    /**
     * Sets the value of the causeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCauseCode(String value) {
        this.causeCode = value;
    }

    /**
     * Gets the value of the subCauseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubCauseCode() {
        return subCauseCode;
    }

    /**
     * Sets the value of the subCauseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubCauseCode(String value) {
        this.subCauseCode = value;
    }

    /**
     * Gets the value of the adjustmentAmount property.
     * 
     */
    public double getAdjustmentAmount() {
        return adjustmentAmount;
    }

    /**
     * Sets the value of the adjustmentAmount property.
     * 
     */
    public void setAdjustmentAmount(double value) {
        this.adjustmentAmount = value;
    }

    /**
     * Gets the value of the adjustmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustmentType() {
        return adjustmentType;
    }

    /**
     * Sets the value of the adjustmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustmentType(String value) {
        this.adjustmentType = value;
    }

    /**
     * Gets the value of the maintenanceContract property.
     * 
     */
    public boolean isMaintenanceContract() {
        return maintenanceContract;
    }

    /**
     * Sets the value of the maintenanceContract property.
     * 
     */
    public void setMaintenanceContract(boolean value) {
        this.maintenanceContract = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the resolution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResolution() {
        return resolution;
    }

    /**
     * Sets the value of the resolution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResolution(String value) {
        this.resolution = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the sysReq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysReq() {
        return sysReq;
    }

    /**
     * Sets the value of the sysReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysReq(String value) {
        this.sysReq = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

}
