
package _do.com.claro.soa.crm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerContactsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerContactsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Ccu" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactoAdicionalResponseCollection" type="{http://soa.claro.com.do/crm}ContactoAdicionalResponseCollection"/>
 *         &lt;element name="ContactoAlternoResponseCollection" type="{http://soa.claro.com.do/crm}ContactoAlternoResponseCollection"/>
 *         &lt;element name="StatusTrans" type="{http://soa.claro.com.do/crm}StatusTrans" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerContactsResponse", propOrder = {
    "ccu",
    "contactoAdicionalResponseCollection",
    "contactoAlternoResponseCollection",
    "statusTrans"
})
public class CustomerContactsResponse {

    @XmlElement(name = "Ccu", required = true)
    protected String ccu;
    @XmlElement(name = "ContactoAdicionalResponseCollection", required = true)
    protected ContactoAdicionalResponseCollection contactoAdicionalResponseCollection;
    @XmlElement(name = "ContactoAlternoResponseCollection", required = true)
    protected ContactoAlternoResponseCollection contactoAlternoResponseCollection;
    @XmlElementRef(name = "StatusTrans", namespace = "http://soa.claro.com.do/crm", type = JAXBElement.class)
    protected JAXBElement<StatusTrans> statusTrans;

    /**
     * Gets the value of the ccu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcu() {
        return ccu;
    }

    /**
     * Sets the value of the ccu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcu(String value) {
        this.ccu = value;
    }

    /**
     * Gets the value of the contactoAdicionalResponseCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ContactoAdicionalResponseCollection }
     *     
     */
    public ContactoAdicionalResponseCollection getContactoAdicionalResponseCollection() {
        return contactoAdicionalResponseCollection;
    }

    /**
     * Sets the value of the contactoAdicionalResponseCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactoAdicionalResponseCollection }
     *     
     */
    public void setContactoAdicionalResponseCollection(ContactoAdicionalResponseCollection value) {
        this.contactoAdicionalResponseCollection = value;
    }

    /**
     * Gets the value of the contactoAlternoResponseCollection property.
     * 
     * @return
     *     possible object is
     *     {@link ContactoAlternoResponseCollection }
     *     
     */
    public ContactoAlternoResponseCollection getContactoAlternoResponseCollection() {
        return contactoAlternoResponseCollection;
    }

    /**
     * Sets the value of the contactoAlternoResponseCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactoAlternoResponseCollection }
     *     
     */
    public void setContactoAlternoResponseCollection(ContactoAlternoResponseCollection value) {
        this.contactoAlternoResponseCollection = value;
    }

    /**
     * Gets the value of the statusTrans property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}
     *     
     */
    public JAXBElement<StatusTrans> getStatusTrans() {
        return statusTrans;
    }

    /**
     * Sets the value of the statusTrans property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}
     *     
     */
    public void setStatusTrans(JAXBElement<StatusTrans> value) {
        this.statusTrans = ((JAXBElement<StatusTrans> ) value);
    }

}
