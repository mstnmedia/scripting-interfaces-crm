
package _do.com.claro.soa.crm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactoAdicionalResponseCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactoAdicionalResponseCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContactoAdicional" type="{http://soa.claro.com.do/crm}ContactoAdicional" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactoAdicionalResponseCollection", propOrder = {
    "contactoAdicional"
})
public class ContactoAdicionalResponseCollection {

    @XmlElement(name = "ContactoAdicional")
    protected List<ContactoAdicional> contactoAdicional;

    /**
     * Gets the value of the contactoAdicional property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactoAdicional property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactoAdicional().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactoAdicional }
     * 
     * 
     */
    public List<ContactoAdicional> getContactoAdicional() {
        if (contactoAdicional == null) {
            contactoAdicional = new ArrayList<ContactoAdicional>();
        }
        return this.contactoAdicional;
    }

}
