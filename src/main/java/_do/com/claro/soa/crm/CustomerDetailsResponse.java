
package _do.com.claro.soa.crm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CustomerDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerDetailsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactBirthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ContactEmail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactFax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactFirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactGender" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactLastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactMaritalStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactNationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactPersonID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactPersonIDType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactSecondLastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CustomerSegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EstablishDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegalName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoginName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RetentionFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RockwellID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TaxPayerID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BanSubInfo" type="{http://soa.claro.com.do/crm}BanSubInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="StatusTrans" type="{http://soa.claro.com.do/crm}StatusTrans"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerDetailsResponse", propOrder = {
    "accountId",
    "accountName",
    "accountType",
    "address",
    "city",
    "contactBirthDate",
    "contactEmail",
    "contactFax",
    "contactFirstName",
    "contactGender",
    "contactLastName",
    "contactMaritalStatus",
    "contactNationality",
    "contactPersonID",
    "contactPersonIDType",
    "contactPhone",
    "contactSecondLastName",
    "contactTitle",
    "country",
    "customerSegment",
    "establishDate",
    "fax",
    "legalName",
    "loginName",
    "phone",
    "retentionFlag",
    "rockwellID",
    "taxPayerID",
    "banSubInfo",
    "statusTrans"
})
public class CustomerDetailsResponse {

    @XmlElement(name = "AccountId", required = true, nillable = true)
    protected String accountId;
    @XmlElement(name = "AccountName", required = true, nillable = true)
    protected String accountName;
    @XmlElement(name = "AccountType", required = true, nillable = true)
    protected String accountType;
    @XmlElement(name = "Address", required = true, nillable = true)
    protected String address;
    @XmlElement(name = "City", required = true, nillable = true)
    protected String city;
    @XmlElement(name = "ContactBirthDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar contactBirthDate;
    @XmlElement(name = "ContactEmail", required = true, nillable = true)
    protected String contactEmail;
    @XmlElement(name = "ContactFax", required = true, nillable = true)
    protected String contactFax;
    @XmlElement(name = "ContactFirstName", required = true, nillable = true)
    protected String contactFirstName;
    @XmlElement(name = "ContactGender", required = true, nillable = true)
    protected String contactGender;
    @XmlElement(name = "ContactLastName", required = true, nillable = true)
    protected String contactLastName;
    @XmlElement(name = "ContactMaritalStatus", required = true, nillable = true)
    protected String contactMaritalStatus;
    @XmlElement(name = "ContactNationality", required = true, nillable = true)
    protected String contactNationality;
    @XmlElement(name = "ContactPersonID", required = true, nillable = true)
    protected String contactPersonID;
    @XmlElement(name = "ContactPersonIDType", required = true, nillable = true)
    protected String contactPersonIDType;
    @XmlElement(name = "ContactPhone", required = true, nillable = true)
    protected String contactPhone;
    @XmlElement(name = "ContactSecondLastName", required = true, nillable = true)
    protected String contactSecondLastName;
    @XmlElement(name = "ContactTitle", required = true, nillable = true)
    protected String contactTitle;
    @XmlElement(name = "Country", required = true, nillable = true)
    protected String country;
    @XmlElement(name = "CustomerSegment", required = true, nillable = true)
    protected String customerSegment;
    @XmlElement(name = "EstablishDate", required = true, nillable = true)
    protected String establishDate;
    @XmlElement(name = "Fax", required = true, nillable = true)
    protected String fax;
    @XmlElement(name = "LegalName", required = true, nillable = true)
    protected String legalName;
    @XmlElement(name = "LoginName", required = true, nillable = true)
    protected String loginName;
    @XmlElement(name = "Phone", required = true, nillable = true)
    protected String phone;
    @XmlElement(name = "RetentionFlag", required = true, nillable = true)
    protected String retentionFlag;
    @XmlElement(name = "RockwellID", required = true, nillable = true)
    protected String rockwellID;
    @XmlElement(name = "TaxPayerID", required = true, nillable = true)
    protected String taxPayerID;
    @XmlElement(name = "BanSubInfo", nillable = true)
    protected List<BanSubInfo> banSubInfo;
    @XmlElement(name = "StatusTrans", required = true)
    protected StatusTrans statusTrans;

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountId(String value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the accountName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the contactBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getContactBirthDate() {
        return contactBirthDate;
    }

    /**
     * Sets the value of the contactBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setContactBirthDate(XMLGregorianCalendar value) {
        this.contactBirthDate = value;
    }

    /**
     * Gets the value of the contactEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * Sets the value of the contactEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactEmail(String value) {
        this.contactEmail = value;
    }

    /**
     * Gets the value of the contactFax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactFax() {
        return contactFax;
    }

    /**
     * Sets the value of the contactFax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactFax(String value) {
        this.contactFax = value;
    }

    /**
     * Gets the value of the contactFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactFirstName() {
        return contactFirstName;
    }

    /**
     * Sets the value of the contactFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactFirstName(String value) {
        this.contactFirstName = value;
    }

    /**
     * Gets the value of the contactGender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactGender() {
        return contactGender;
    }

    /**
     * Sets the value of the contactGender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactGender(String value) {
        this.contactGender = value;
    }

    /**
     * Gets the value of the contactLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactLastName() {
        return contactLastName;
    }

    /**
     * Sets the value of the contactLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactLastName(String value) {
        this.contactLastName = value;
    }

    /**
     * Gets the value of the contactMaritalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactMaritalStatus() {
        return contactMaritalStatus;
    }

    /**
     * Sets the value of the contactMaritalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactMaritalStatus(String value) {
        this.contactMaritalStatus = value;
    }

    /**
     * Gets the value of the contactNationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactNationality() {
        return contactNationality;
    }

    /**
     * Sets the value of the contactNationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactNationality(String value) {
        this.contactNationality = value;
    }

    /**
     * Gets the value of the contactPersonID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPersonID() {
        return contactPersonID;
    }

    /**
     * Sets the value of the contactPersonID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPersonID(String value) {
        this.contactPersonID = value;
    }

    /**
     * Gets the value of the contactPersonIDType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPersonIDType() {
        return contactPersonIDType;
    }

    /**
     * Sets the value of the contactPersonIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPersonIDType(String value) {
        this.contactPersonIDType = value;
    }

    /**
     * Gets the value of the contactPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * Sets the value of the contactPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPhone(String value) {
        this.contactPhone = value;
    }

    /**
     * Gets the value of the contactSecondLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactSecondLastName() {
        return contactSecondLastName;
    }

    /**
     * Sets the value of the contactSecondLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactSecondLastName(String value) {
        this.contactSecondLastName = value;
    }

    /**
     * Gets the value of the contactTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactTitle() {
        return contactTitle;
    }

    /**
     * Sets the value of the contactTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactTitle(String value) {
        this.contactTitle = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the customerSegment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerSegment() {
        return customerSegment;
    }

    /**
     * Sets the value of the customerSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerSegment(String value) {
        this.customerSegment = value;
    }

    /**
     * Gets the value of the establishDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstablishDate() {
        return establishDate;
    }

    /**
     * Sets the value of the establishDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstablishDate(String value) {
        this.establishDate = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the legalName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegalName() {
        return legalName;
    }

    /**
     * Sets the value of the legalName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegalName(String value) {
        this.legalName = value;
    }

    /**
     * Gets the value of the loginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * Sets the value of the loginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoginName(String value) {
        this.loginName = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the retentionFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetentionFlag() {
        return retentionFlag;
    }

    /**
     * Sets the value of the retentionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetentionFlag(String value) {
        this.retentionFlag = value;
    }

    /**
     * Gets the value of the rockwellID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRockwellID() {
        return rockwellID;
    }

    /**
     * Sets the value of the rockwellID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRockwellID(String value) {
        this.rockwellID = value;
    }

    /**
     * Gets the value of the taxPayerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxPayerID() {
        return taxPayerID;
    }

    /**
     * Sets the value of the taxPayerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxPayerID(String value) {
        this.taxPayerID = value;
    }

    /**
     * Gets the value of the banSubInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the banSubInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBanSubInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BanSubInfo }
     * 
     * 
     */
    public List<BanSubInfo> getBanSubInfo() {
        if (banSubInfo == null) {
            banSubInfo = new ArrayList<BanSubInfo>();
        }
        return this.banSubInfo;
    }

    /**
     * Gets the value of the statusTrans property.
     * 
     * @return
     *     possible object is
     *     {@link StatusTrans }
     *     
     */
    public StatusTrans getStatusTrans() {
        return statusTrans;
    }

    /**
     * Sets the value of the statusTrans property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusTrans }
     *     
     */
    public void setStatusTrans(StatusTrans value) {
        this.statusTrans = value;
    }

}
