
package _do.com.claro.soa.crm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateCaseRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateCaseRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subscriber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="queueName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTypeLevel1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTypeLevel2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="caseTypeLevel3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contactPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="visitPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="autoDispatch" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCaseRequest", propOrder = {
    "subscriber",
    "queueName",
    "notes",
    "productType",
    "caseTitle",
    "caseTypeLevel1",
    "caseTypeLevel2",
    "caseTypeLevel3",
    "contactPhone",
    "visitPeriod",
    "clientSystem",
    "autoDispatch"
})
public class CreateCaseRequest {

    @XmlElement(required = true)
    protected String subscriber;
    @XmlElement(required = true)
    protected String queueName;
    @XmlElement(required = true)
    protected String notes;
    @XmlElement(required = true)
    protected String productType;
    @XmlElement(required = true)
    protected String caseTitle;
    @XmlElement(required = true)
    protected String caseTypeLevel1;
    @XmlElement(required = true)
    protected String caseTypeLevel2;
    @XmlElement(required = true)
    protected String caseTypeLevel3;
    @XmlElement(required = true)
    protected String contactPhone;
    @XmlElement(required = true)
    protected String visitPeriod;
    @XmlElement(required = true)
    protected String clientSystem;
    protected boolean autoDispatch;

    /**
     * Gets the value of the subscriber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriber() {
        return subscriber;
    }

    /**
     * Sets the value of the subscriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriber(String value) {
        this.subscriber = value;
    }

    /**
     * Gets the value of the queueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueueName() {
        return queueName;
    }

    /**
     * Sets the value of the queueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueueName(String value) {
        this.queueName = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the caseTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTitle() {
        return caseTitle;
    }

    /**
     * Sets the value of the caseTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTitle(String value) {
        this.caseTitle = value;
    }

    /**
     * Gets the value of the caseTypeLevel1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel1() {
        return caseTypeLevel1;
    }

    /**
     * Sets the value of the caseTypeLevel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel1(String value) {
        this.caseTypeLevel1 = value;
    }

    /**
     * Gets the value of the caseTypeLevel2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel2() {
        return caseTypeLevel2;
    }

    /**
     * Sets the value of the caseTypeLevel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel2(String value) {
        this.caseTypeLevel2 = value;
    }

    /**
     * Gets the value of the caseTypeLevel3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel3() {
        return caseTypeLevel3;
    }

    /**
     * Sets the value of the caseTypeLevel3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel3(String value) {
        this.caseTypeLevel3 = value;
    }

    /**
     * Gets the value of the contactPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * Sets the value of the contactPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPhone(String value) {
        this.contactPhone = value;
    }

    /**
     * Gets the value of the visitPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitPeriod() {
        return visitPeriod;
    }

    /**
     * Sets the value of the visitPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitPeriod(String value) {
        this.visitPeriod = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

    /**
     * Gets the value of the autoDispatch property.
     * 
     */
    public boolean isAutoDispatch() {
        return autoDispatch;
    }

    /**
     * Sets the value of the autoDispatch property.
     * 
     */
    public void setAutoDispatch(boolean value) {
        this.autoDispatch = value;
    }

}
