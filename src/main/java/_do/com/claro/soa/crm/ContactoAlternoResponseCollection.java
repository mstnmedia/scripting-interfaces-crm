
package _do.com.claro.soa.crm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactoAlternoResponseCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactoAlternoResponseCollection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContactoAlterno" type="{http://soa.claro.com.do/crm}ContactoAlterno" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactoAlternoResponseCollection", propOrder = {
    "contactoAlterno"
})
public class ContactoAlternoResponseCollection {

    @XmlElement(name = "ContactoAlterno")
    protected List<ContactoAlterno> contactoAlterno;

    /**
     * Gets the value of the contactoAlterno property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactoAlterno property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactoAlterno().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactoAlterno }
     * 
     * 
     */
    public List<ContactoAlterno> getContactoAlterno() {
        if (contactoAlterno == null) {
            contactoAlterno = new ArrayList<ContactoAlterno>();
        }
        return this.contactoAlterno;
    }

}
