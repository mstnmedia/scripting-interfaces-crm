
package _do.com.claro.soa.crm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TopicInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TopicInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Reason1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Reason2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TopicInfo", propOrder = {
    "entityId",
    "entityType",
    "reason1",
    "reason2",
    "result"
})
public class TopicInfo {

    @XmlElement(name = "EntityId", required = true)
    protected String entityId;
    @XmlElement(name = "EntityType")
    protected int entityType;
    @XmlElement(name = "Reason1", required = true)
    protected String reason1;
    @XmlElement(name = "Reason2", required = true)
    protected String reason2;
    @XmlElement(name = "Result", required = true)
    protected String result;

    /**
     * Gets the value of the entityId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * Sets the value of the entityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityId(String value) {
        this.entityId = value;
    }

    /**
     * Gets the value of the entityType property.
     * 
     */
    public int getEntityType() {
        return entityType;
    }

    /**
     * Sets the value of the entityType property.
     * 
     */
    public void setEntityType(int value) {
        this.entityType = value;
    }

    /**
     * Gets the value of the reason1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason1() {
        return reason1;
    }

    /**
     * Sets the value of the reason1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason1(String value) {
        this.reason1 = value;
    }

    /**
     * Gets the value of the reason2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason2() {
        return reason2;
    }

    /**
     * Sets the value of the reason2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason2(String value) {
        this.reason2 = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResult(String value) {
        this.result = value;
    }

}
