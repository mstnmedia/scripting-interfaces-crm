
package _do.com.claro.soa.crm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.claro.soa.crm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateCaseResponse_QNAME = new QName("http://soa.claro.com.do/crm", "CreateCaseResponse");
    private final static QName _CloseCaseRequest_QNAME = new QName("http://soa.claro.com.do/crm", "CloseCaseRequest");
    private final static QName _CloseCaseResponse_QNAME = new QName("http://soa.claro.com.do/crm", "CloseCaseResponse");
    private final static QName _UpdateCaseRequest_QNAME = new QName("http://soa.claro.com.do/crm", "UpdateCaseRequest");
    private final static QName _InteractionValuesResponse_QNAME = new QName("http://soa.claro.com.do/crm", "InteractionValuesResponse");
    private final static QName _CustomerContactsRequest_QNAME = new QName("http://soa.claro.com.do/crm", "CustomerContactsRequest");
    private final static QName _CustomerContactsResponse_QNAME = new QName("http://soa.claro.com.do/crm", "CustomerContactsResponse");
    private final static QName _CustomerDetailsResponse_QNAME = new QName("http://soa.claro.com.do/crm", "CustomerDetailsResponse");
    private final static QName _BanSubInfo_QNAME = new QName("http://soa.claro.com.do/crm", "BanSubInfo");
    private final static QName _FailureReason_QNAME = new QName("http://soa.claro.com.do/crm", "FailureReason");
    private final static QName _ContactoAlternoResponseCollection_QNAME = new QName("http://soa.claro.com.do/crm", "ContactoAlternoResponseCollection");
    private final static QName _InteractionValuesRequest_QNAME = new QName("http://soa.claro.com.do/crm", "InteractionValuesRequest");
    private final static QName _DispatchCaseRequest_QNAME = new QName("http://soa.claro.com.do/crm", "DispatchCaseRequest");
    private final static QName _UpdateParentChildResponse_QNAME = new QName("http://soa.claro.com.do/crm", "UpdateParentChildResponse");
    private final static QName _CreateInteractionResponse_QNAME = new QName("http://soa.claro.com.do/crm", "CreateInteractionResponse");
    private final static QName _CustomerCasesRequest_QNAME = new QName("http://soa.claro.com.do/crm", "CustomerCasesRequest");
    private final static QName _CaseDetailsInfo_QNAME = new QName("http://soa.claro.com.do/crm", "CaseDetailsInfo");
    private final static QName _CreateCaseRequest_QNAME = new QName("http://soa.claro.com.do/crm", "CreateCaseRequest");
    private final static QName _AddCaseNotesRequest_QNAME = new QName("http://soa.claro.com.do/crm", "AddCaseNotesRequest");
    private final static QName _ContactoAlterno_QNAME = new QName("http://soa.claro.com.do/crm", "ContactoAlterno");
    private final static QName _TopicInfo_QNAME = new QName("http://soa.claro.com.do/crm", "TopicInfo");
    private final static QName _CreateInteractionRequest_QNAME = new QName("http://soa.claro.com.do/crm", "CreateInteractionRequest");
    private final static QName _ContactoAdicionalResponseCollection_QNAME = new QName("http://soa.claro.com.do/crm", "ContactoAdicionalResponseCollection");
    private final static QName _DispatchCaseResponse_QNAME = new QName("http://soa.claro.com.do/crm", "DispatchCaseResponse");
    private final static QName _CustomerDetailsRequest_QNAME = new QName("http://soa.claro.com.do/crm", "CustomerDetailsRequest");
    private final static QName _UpdateCaseResponse_QNAME = new QName("http://soa.claro.com.do/crm", "UpdateCaseResponse");
    private final static QName _CustomerCasesResponse_QNAME = new QName("http://soa.claro.com.do/crm", "CustomerCasesResponse");
    private final static QName _CustomerDetailsByIdResponse_QNAME = new QName("http://soa.claro.com.do/crm", "CustomerDetailsByIdResponse");
    private final static QName _AddCaseNotesResponse_QNAME = new QName("http://soa.claro.com.do/crm", "AddCaseNotesResponse");
    private final static QName _ContactoAdicional_QNAME = new QName("http://soa.claro.com.do/crm", "ContactoAdicional");
    private final static QName _UpdateParentChildRequest_QNAME = new QName("http://soa.claro.com.do/crm", "UpdateParentChildRequest");
    private final static QName _StatusTrans_QNAME = new QName("http://soa.claro.com.do/crm", "StatusTrans");
    private final static QName _StatusTransException_QNAME = new QName("http://soa.claro.com.do/crm", "Exception");
    private final static QName _FailureReasonReasonDesc_QNAME = new QName("http://soa.claro.com.do/crm", "reasonDesc");
    private final static QName _FailureReasonErrorLog_QNAME = new QName("http://soa.claro.com.do/crm", "errorLog");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.claro.soa.crm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InteractionValuesRequest }
     * 
     */
    public InteractionValuesRequest createInteractionValuesRequest() {
        return new InteractionValuesRequest();
    }

    /**
     * Create an instance of {@link UpdateParentChildRequest }
     * 
     */
    public UpdateParentChildRequest createUpdateParentChildRequest() {
        return new UpdateParentChildRequest();
    }

    /**
     * Create an instance of {@link DispatchCaseResponse }
     * 
     */
    public DispatchCaseResponse createDispatchCaseResponse() {
        return new DispatchCaseResponse();
    }

    /**
     * Create an instance of {@link CaseDetailsInfo }
     * 
     */
    public CaseDetailsInfo createCaseDetailsInfo() {
        return new CaseDetailsInfo();
    }

    /**
     * Create an instance of {@link CreateInteractionResponse }
     * 
     */
    public CreateInteractionResponse createCreateInteractionResponse() {
        return new CreateInteractionResponse();
    }

    /**
     * Create an instance of {@link CustomerDetailsRequest }
     * 
     */
    public CustomerDetailsRequest createCustomerDetailsRequest() {
        return new CustomerDetailsRequest();
    }

    /**
     * Create an instance of {@link CreateCaseResponse }
     * 
     */
    public CreateCaseResponse createCreateCaseResponse() {
        return new CreateCaseResponse();
    }

    /**
     * Create an instance of {@link CloseCaseResponse }
     * 
     */
    public CloseCaseResponse createCloseCaseResponse() {
        return new CloseCaseResponse();
    }

    /**
     * Create an instance of {@link CustomerDetailsResponse }
     * 
     */
    public CustomerDetailsResponse createCustomerDetailsResponse() {
        return new CustomerDetailsResponse();
    }

    /**
     * Create an instance of {@link UpdateParentChildResponse }
     * 
     */
    public UpdateParentChildResponse createUpdateParentChildResponse() {
        return new UpdateParentChildResponse();
    }

    /**
     * Create an instance of {@link CustomerCasesResponse }
     * 
     */
    public CustomerCasesResponse createCustomerCasesResponse() {
        return new CustomerCasesResponse();
    }

    /**
     * Create an instance of {@link FailureReason }
     * 
     */
    public FailureReason createFailureReason() {
        return new FailureReason();
    }

    /**
     * Create an instance of {@link BanSubInfo }
     * 
     */
    public BanSubInfo createBanSubInfo() {
        return new BanSubInfo();
    }

    /**
     * Create an instance of {@link DispatchCaseRequest }
     * 
     */
    public DispatchCaseRequest createDispatchCaseRequest() {
        return new DispatchCaseRequest();
    }

    /**
     * Create an instance of {@link CustomerCasesRequest }
     * 
     */
    public CustomerCasesRequest createCustomerCasesRequest() {
        return new CustomerCasesRequest();
    }

    /**
     * Create an instance of {@link CustomerContactsRequest }
     * 
     */
    public CustomerContactsRequest createCustomerContactsRequest() {
        return new CustomerContactsRequest();
    }

    /**
     * Create an instance of {@link TopicInfo }
     * 
     */
    public TopicInfo createTopicInfo() {
        return new TopicInfo();
    }

    /**
     * Create an instance of {@link StatusTrans }
     * 
     */
    public StatusTrans createStatusTrans() {
        return new StatusTrans();
    }

    /**
     * Create an instance of {@link InteractionValuesResponse }
     * 
     */
    public InteractionValuesResponse createInteractionValuesResponse() {
        return new InteractionValuesResponse();
    }

    /**
     * Create an instance of {@link CreateCaseRequest }
     * 
     */
    public CreateCaseRequest createCreateCaseRequest() {
        return new CreateCaseRequest();
    }

    /**
     * Create an instance of {@link UpdateCaseResponse }
     * 
     */
    public UpdateCaseResponse createUpdateCaseResponse() {
        return new UpdateCaseResponse();
    }

    /**
     * Create an instance of {@link UpdateCaseRequest }
     * 
     */
    public UpdateCaseRequest createUpdateCaseRequest() {
        return new UpdateCaseRequest();
    }

    /**
     * Create an instance of {@link ContactoAlternoResponseCollection }
     * 
     */
    public ContactoAlternoResponseCollection createContactoAlternoResponseCollection() {
        return new ContactoAlternoResponseCollection();
    }

    /**
     * Create an instance of {@link AddCaseNotesResponse }
     * 
     */
    public AddCaseNotesResponse createAddCaseNotesResponse() {
        return new AddCaseNotesResponse();
    }

    /**
     * Create an instance of {@link CustomerDetailsByIdResponse }
     * 
     */
    public CustomerDetailsByIdResponse createCustomerDetailsByIdResponse() {
        return new CustomerDetailsByIdResponse();
    }

    /**
     * Create an instance of {@link CustomerDetailsByIdRequest }
     * 
     */
    public CustomerDetailsByIdRequest createCustomerDetailsByIdRequest() {
        return new CustomerDetailsByIdRequest();
    }

    /**
     * Create an instance of {@link ContactoAdicionalResponseCollection }
     * 
     */
    public ContactoAdicionalResponseCollection createContactoAdicionalResponseCollection() {
        return new ContactoAdicionalResponseCollection();
    }

    /**
     * Create an instance of {@link CloseCaseRequest }
     * 
     */
    public CloseCaseRequest createCloseCaseRequest() {
        return new CloseCaseRequest();
    }

    /**
     * Create an instance of {@link CustomerContactsResponse }
     * 
     */
    public CustomerContactsResponse createCustomerContactsResponse() {
        return new CustomerContactsResponse();
    }

    /**
     * Create an instance of {@link ContactoAdicional }
     * 
     */
    public ContactoAdicional createContactoAdicional() {
        return new ContactoAdicional();
    }

    /**
     * Create an instance of {@link AddCaseNotesRequest }
     * 
     */
    public AddCaseNotesRequest createAddCaseNotesRequest() {
        return new AddCaseNotesRequest();
    }

    /**
     * Create an instance of {@link CreateInteractionRequest }
     * 
     */
    public CreateInteractionRequest createCreateInteractionRequest() {
        return new CreateInteractionRequest();
    }

    /**
     * Create an instance of {@link ContactoAlterno }
     * 
     */
    public ContactoAlterno createContactoAlterno() {
        return new ContactoAlterno();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CreateCaseResponse")
    public JAXBElement<CreateCaseResponse> createCreateCaseResponse(CreateCaseResponse value) {
        return new JAXBElement<CreateCaseResponse>(_CreateCaseResponse_QNAME, CreateCaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CloseCaseRequest")
    public JAXBElement<CloseCaseRequest> createCloseCaseRequest(CloseCaseRequest value) {
        return new JAXBElement<CloseCaseRequest>(_CloseCaseRequest_QNAME, CloseCaseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseCaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CloseCaseResponse")
    public JAXBElement<CloseCaseResponse> createCloseCaseResponse(CloseCaseResponse value) {
        return new JAXBElement<CloseCaseResponse>(_CloseCaseResponse_QNAME, CloseCaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "UpdateCaseRequest")
    public JAXBElement<UpdateCaseRequest> createUpdateCaseRequest(UpdateCaseRequest value) {
        return new JAXBElement<UpdateCaseRequest>(_UpdateCaseRequest_QNAME, UpdateCaseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InteractionValuesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "InteractionValuesResponse")
    public JAXBElement<InteractionValuesResponse> createInteractionValuesResponse(InteractionValuesResponse value) {
        return new JAXBElement<InteractionValuesResponse>(_InteractionValuesResponse_QNAME, InteractionValuesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerContactsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CustomerContactsRequest")
    public JAXBElement<CustomerContactsRequest> createCustomerContactsRequest(CustomerContactsRequest value) {
        return new JAXBElement<CustomerContactsRequest>(_CustomerContactsRequest_QNAME, CustomerContactsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerContactsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CustomerContactsResponse")
    public JAXBElement<CustomerContactsResponse> createCustomerContactsResponse(CustomerContactsResponse value) {
        return new JAXBElement<CustomerContactsResponse>(_CustomerContactsResponse_QNAME, CustomerContactsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CustomerDetailsResponse")
    public JAXBElement<CustomerDetailsResponse> createCustomerDetailsResponse(CustomerDetailsResponse value) {
        return new JAXBElement<CustomerDetailsResponse>(_CustomerDetailsResponse_QNAME, CustomerDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BanSubInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "BanSubInfo")
    public JAXBElement<BanSubInfo> createBanSubInfo(BanSubInfo value) {
        return new JAXBElement<BanSubInfo>(_BanSubInfo_QNAME, BanSubInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FailureReason }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "FailureReason")
    public JAXBElement<FailureReason> createFailureReason(FailureReason value) {
        return new JAXBElement<FailureReason>(_FailureReason_QNAME, FailureReason.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactoAlternoResponseCollection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "ContactoAlternoResponseCollection")
    public JAXBElement<ContactoAlternoResponseCollection> createContactoAlternoResponseCollection(ContactoAlternoResponseCollection value) {
        return new JAXBElement<ContactoAlternoResponseCollection>(_ContactoAlternoResponseCollection_QNAME, ContactoAlternoResponseCollection.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InteractionValuesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "InteractionValuesRequest")
    public JAXBElement<InteractionValuesRequest> createInteractionValuesRequest(InteractionValuesRequest value) {
        return new JAXBElement<InteractionValuesRequest>(_InteractionValuesRequest_QNAME, InteractionValuesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DispatchCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "DispatchCaseRequest")
    public JAXBElement<DispatchCaseRequest> createDispatchCaseRequest(DispatchCaseRequest value) {
        return new JAXBElement<DispatchCaseRequest>(_DispatchCaseRequest_QNAME, DispatchCaseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateParentChildResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "UpdateParentChildResponse")
    public JAXBElement<UpdateParentChildResponse> createUpdateParentChildResponse(UpdateParentChildResponse value) {
        return new JAXBElement<UpdateParentChildResponse>(_UpdateParentChildResponse_QNAME, UpdateParentChildResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateInteractionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CreateInteractionResponse")
    public JAXBElement<CreateInteractionResponse> createCreateInteractionResponse(CreateInteractionResponse value) {
        return new JAXBElement<CreateInteractionResponse>(_CreateInteractionResponse_QNAME, CreateInteractionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerCasesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CustomerCasesRequest")
    public JAXBElement<CustomerCasesRequest> createCustomerCasesRequest(CustomerCasesRequest value) {
        return new JAXBElement<CustomerCasesRequest>(_CustomerCasesRequest_QNAME, CustomerCasesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CaseDetailsInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CaseDetailsInfo")
    public JAXBElement<CaseDetailsInfo> createCaseDetailsInfo(CaseDetailsInfo value) {
        return new JAXBElement<CaseDetailsInfo>(_CaseDetailsInfo_QNAME, CaseDetailsInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCaseRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CreateCaseRequest")
    public JAXBElement<CreateCaseRequest> createCreateCaseRequest(CreateCaseRequest value) {
        return new JAXBElement<CreateCaseRequest>(_CreateCaseRequest_QNAME, CreateCaseRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCaseNotesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "AddCaseNotesRequest")
    public JAXBElement<AddCaseNotesRequest> createAddCaseNotesRequest(AddCaseNotesRequest value) {
        return new JAXBElement<AddCaseNotesRequest>(_AddCaseNotesRequest_QNAME, AddCaseNotesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactoAlterno }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "ContactoAlterno")
    public JAXBElement<ContactoAlterno> createContactoAlterno(ContactoAlterno value) {
        return new JAXBElement<ContactoAlterno>(_ContactoAlterno_QNAME, ContactoAlterno.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TopicInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "TopicInfo")
    public JAXBElement<TopicInfo> createTopicInfo(TopicInfo value) {
        return new JAXBElement<TopicInfo>(_TopicInfo_QNAME, TopicInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateInteractionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CreateInteractionRequest")
    public JAXBElement<CreateInteractionRequest> createCreateInteractionRequest(CreateInteractionRequest value) {
        return new JAXBElement<CreateInteractionRequest>(_CreateInteractionRequest_QNAME, CreateInteractionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactoAdicionalResponseCollection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "ContactoAdicionalResponseCollection")
    public JAXBElement<ContactoAdicionalResponseCollection> createContactoAdicionalResponseCollection(ContactoAdicionalResponseCollection value) {
        return new JAXBElement<ContactoAdicionalResponseCollection>(_ContactoAdicionalResponseCollection_QNAME, ContactoAdicionalResponseCollection.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DispatchCaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "DispatchCaseResponse")
    public JAXBElement<DispatchCaseResponse> createDispatchCaseResponse(DispatchCaseResponse value) {
        return new JAXBElement<DispatchCaseResponse>(_DispatchCaseResponse_QNAME, DispatchCaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerDetailsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CustomerDetailsRequest")
    public JAXBElement<CustomerDetailsRequest> createCustomerDetailsRequest(CustomerDetailsRequest value) {
        return new JAXBElement<CustomerDetailsRequest>(_CustomerDetailsRequest_QNAME, CustomerDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCaseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "UpdateCaseResponse")
    public JAXBElement<UpdateCaseResponse> createUpdateCaseResponse(UpdateCaseResponse value) {
        return new JAXBElement<UpdateCaseResponse>(_UpdateCaseResponse_QNAME, UpdateCaseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerCasesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CustomerCasesResponse")
    public JAXBElement<CustomerCasesResponse> createCustomerCasesResponse(CustomerCasesResponse value) {
        return new JAXBElement<CustomerCasesResponse>(_CustomerCasesResponse_QNAME, CustomerCasesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerDetailsByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "CustomerDetailsByIdResponse")
    public JAXBElement<CustomerDetailsByIdResponse> createCustomerDetailsByIdResponse(CustomerDetailsByIdResponse value) {
        return new JAXBElement<CustomerDetailsByIdResponse>(_CustomerDetailsByIdResponse_QNAME, CustomerDetailsByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCaseNotesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "AddCaseNotesResponse")
    public JAXBElement<AddCaseNotesResponse> createAddCaseNotesResponse(AddCaseNotesResponse value) {
        return new JAXBElement<AddCaseNotesResponse>(_AddCaseNotesResponse_QNAME, AddCaseNotesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactoAdicional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "ContactoAdicional")
    public JAXBElement<ContactoAdicional> createContactoAdicional(ContactoAdicional value) {
        return new JAXBElement<ContactoAdicional>(_ContactoAdicional_QNAME, ContactoAdicional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateParentChildRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "UpdateParentChildRequest")
    public JAXBElement<UpdateParentChildRequest> createUpdateParentChildRequest(UpdateParentChildRequest value) {
        return new JAXBElement<UpdateParentChildRequest>(_UpdateParentChildRequest_QNAME, UpdateParentChildRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "StatusTrans")
    public JAXBElement<StatusTrans> createStatusTrans(StatusTrans value) {
        return new JAXBElement<StatusTrans>(_StatusTrans_QNAME, StatusTrans.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "StatusTrans", scope = CloseCaseResponse.class)
    public JAXBElement<StatusTrans> createCloseCaseResponseStatusTrans(StatusTrans value) {
        return new JAXBElement<StatusTrans>(_StatusTrans_QNAME, StatusTrans.class, CloseCaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "StatusTrans", scope = CustomerContactsResponse.class)
    public JAXBElement<StatusTrans> createCustomerContactsResponseStatusTrans(StatusTrans value) {
        return new JAXBElement<StatusTrans>(_StatusTrans_QNAME, StatusTrans.class, CustomerContactsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "StatusTrans", scope = CustomerCasesResponse.class)
    public JAXBElement<StatusTrans> createCustomerCasesResponseStatusTrans(StatusTrans value) {
        return new JAXBElement<StatusTrans>(_StatusTrans_QNAME, StatusTrans.class, CustomerCasesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "Exception", scope = StatusTrans.class)
    public JAXBElement<String> createStatusTransException(String value) {
        return new JAXBElement<String>(_StatusTransException_QNAME, String.class, StatusTrans.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "StatusTrans", scope = UpdateCaseResponse.class)
    public JAXBElement<StatusTrans> createUpdateCaseResponseStatusTrans(StatusTrans value) {
        return new JAXBElement<StatusTrans>(_StatusTrans_QNAME, StatusTrans.class, UpdateCaseResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "reasonDesc", scope = FailureReason.class)
    public JAXBElement<String> createFailureReasonReasonDesc(String value) {
        return new JAXBElement<String>(_FailureReasonReasonDesc_QNAME, String.class, FailureReason.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soa.claro.com.do/crm", name = "errorLog", scope = FailureReason.class)
    public JAXBElement<String> createFailureReasonErrorLog(String value) {
        return new JAXBElement<String>(_FailureReasonErrorLog_QNAME, String.class, FailureReason.class, value);
    }

}
