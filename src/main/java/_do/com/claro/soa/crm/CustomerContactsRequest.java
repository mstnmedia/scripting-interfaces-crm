
package _do.com.claro.soa.crm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerContactsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerContactsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ccu" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerContactsRequest", propOrder = {
    "ccu"
})
public class CustomerContactsRequest {

    @XmlElement(required = true)
    protected String ccu;

    /**
     * Gets the value of the ccu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcu() {
        return ccu;
    }

    /**
     * Sets the value of the ccu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcu(String value) {
        this.ccu = value;
    }

}
