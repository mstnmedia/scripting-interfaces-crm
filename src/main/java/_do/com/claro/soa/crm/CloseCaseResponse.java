
package _do.com.claro.soa.crm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CloseCaseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CloseCaseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusTrans" type="{http://soa.claro.com.do/crm}StatusTrans" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CloseCaseResponse", propOrder = {
    "statusTrans"
})
public class CloseCaseResponse {

    @XmlElementRef(name = "StatusTrans", namespace = "http://soa.claro.com.do/crm", type = JAXBElement.class)
    protected JAXBElement<StatusTrans> statusTrans;

    /**
     * Gets the value of the statusTrans property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}
     *     
     */
    public JAXBElement<StatusTrans> getStatusTrans() {
        return statusTrans;
    }

    /**
     * Sets the value of the statusTrans property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}
     *     
     */
    public void setStatusTrans(JAXBElement<StatusTrans> value) {
        this.statusTrans = ((JAXBElement<StatusTrans> ) value);
    }

}
