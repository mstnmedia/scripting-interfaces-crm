
package _do.com.claro.soa.crm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseDetailsInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseDetailsInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CaseId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CaseStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CaseTypeLevel1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CaseTypeLevel2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CaseTypeLevel3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CreationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CloseDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CaseHistory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseDetailsInfo", propOrder = {
    "caseId",
    "phoneNumber",
    "caseStatus",
    "priority",
    "caseTypeLevel1",
    "caseTypeLevel2",
    "caseTypeLevel3",
    "creationDate",
    "dueDate",
    "closeDate",
    "caseHistory"
})
public class CaseDetailsInfo {

    @XmlElement(name = "CaseId", required = true, nillable = true)
    protected String caseId;
    @XmlElement(name = "PhoneNumber", required = true, nillable = true)
    protected String phoneNumber;
    @XmlElement(name = "CaseStatus", required = true, nillable = true)
    protected String caseStatus;
    @XmlElement(name = "Priority", required = true, nillable = true)
    protected String priority;
    @XmlElement(name = "CaseTypeLevel1", required = true, nillable = true)
    protected String caseTypeLevel1;
    @XmlElement(name = "CaseTypeLevel2", required = true, nillable = true)
    protected String caseTypeLevel2;
    @XmlElement(name = "CaseTypeLevel3", required = true, nillable = true)
    protected String caseTypeLevel3;
    @XmlElement(name = "CreationDate", required = true, nillable = true)
    protected String creationDate;
    @XmlElement(name = "DueDate", required = true, nillable = true)
    protected String dueDate;
    @XmlElement(name = "CloseDate", required = true, nillable = true)
    protected String closeDate;
    @XmlElement(name = "CaseHistory", required = true, nillable = true)
    protected String caseHistory;

    /**
     * Gets the value of the caseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseId() {
        return caseId;
    }

    /**
     * Sets the value of the caseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseId(String value) {
        this.caseId = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the caseStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseStatus() {
        return caseStatus;
    }

    /**
     * Sets the value of the caseStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseStatus(String value) {
        this.caseStatus = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the caseTypeLevel1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel1() {
        return caseTypeLevel1;
    }

    /**
     * Sets the value of the caseTypeLevel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel1(String value) {
        this.caseTypeLevel1 = value;
    }

    /**
     * Gets the value of the caseTypeLevel2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel2() {
        return caseTypeLevel2;
    }

    /**
     * Sets the value of the caseTypeLevel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel2(String value) {
        this.caseTypeLevel2 = value;
    }

    /**
     * Gets the value of the caseTypeLevel3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseTypeLevel3() {
        return caseTypeLevel3;
    }

    /**
     * Sets the value of the caseTypeLevel3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseTypeLevel3(String value) {
        this.caseTypeLevel3 = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationDate(String value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDueDate(String value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the closeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCloseDate() {
        return closeDate;
    }

    /**
     * Sets the value of the closeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseDate(String value) {
        this.closeDate = value;
    }

    /**
     * Gets the value of the caseHistory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseHistory() {
        return caseHistory;
    }

    /**
     * Sets the value of the caseHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseHistory(String value) {
        this.caseHistory = value;
    }

}
