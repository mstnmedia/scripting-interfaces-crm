
package _do.com.claro.soa.crm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerDetailsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerDetailsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductTypeFilter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ListBANs" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ClientSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerDetailsRequest", propOrder = {
    "idType",
    "idValue",
    "productTypeFilter",
    "listBANs",
    "clientSystem"
})
public class CustomerDetailsRequest {

    @XmlElement(name = "IdType", required = true)
    protected String idType;
    @XmlElement(name = "IdValue", required = true)
    protected String idValue;
    @XmlElement(name = "ProductTypeFilter", required = true, nillable = true)
    protected String productTypeFilter;
    @XmlElement(name = "ListBANs")
    protected boolean listBANs;
    @XmlElement(name = "ClientSystem", required = true)
    protected String clientSystem;

    /**
     * Gets the value of the idType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdType() {
        return idType;
    }

    /**
     * Sets the value of the idType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdType(String value) {
        this.idType = value;
    }

    /**
     * Gets the value of the idValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdValue() {
        return idValue;
    }

    /**
     * Sets the value of the idValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdValue(String value) {
        this.idValue = value;
    }

    /**
     * Gets the value of the productTypeFilter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductTypeFilter() {
        return productTypeFilter;
    }

    /**
     * Sets the value of the productTypeFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductTypeFilter(String value) {
        this.productTypeFilter = value;
    }

    /**
     * Gets the value of the listBANs property.
     * 
     */
    public boolean isListBANs() {
        return listBANs;
    }

    /**
     * Sets the value of the listBANs property.
     * 
     */
    public void setListBANs(boolean value) {
        this.listBANs = value;
    }

    /**
     * Gets the value of the clientSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSystem() {
        return clientSystem;
    }

    /**
     * Sets the value of the clientSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSystem(String value) {
        this.clientSystem = value;
    }

}
