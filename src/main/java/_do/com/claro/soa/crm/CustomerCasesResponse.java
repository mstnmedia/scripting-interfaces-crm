
package _do.com.claro.soa.crm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerCasesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerCasesResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CaseDetailsInfo" type="{http://soa.claro.com.do/crm}CaseDetailsInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="StatusTrans" type="{http://soa.claro.com.do/crm}StatusTrans" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerCasesResponse", propOrder = {
    "caseDetailsInfo",
    "statusTrans"
})
public class CustomerCasesResponse {

    @XmlElement(name = "CaseDetailsInfo", nillable = true)
    protected List<CaseDetailsInfo> caseDetailsInfo;
    @XmlElementRef(name = "StatusTrans", namespace = "http://soa.claro.com.do/crm", type = JAXBElement.class)
    protected JAXBElement<StatusTrans> statusTrans;

    /**
     * Gets the value of the caseDetailsInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the caseDetailsInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCaseDetailsInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CaseDetailsInfo }
     * 
     * 
     */
    public List<CaseDetailsInfo> getCaseDetailsInfo() {
        if (caseDetailsInfo == null) {
            caseDetailsInfo = new ArrayList<CaseDetailsInfo>();
        }
        return this.caseDetailsInfo;
    }

    /**
     * Gets the value of the statusTrans property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}
     *     
     */
    public JAXBElement<StatusTrans> getStatusTrans() {
        return statusTrans;
    }

    /**
     * Sets the value of the statusTrans property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StatusTrans }{@code >}
     *     
     */
    public void setStatusTrans(JAXBElement<StatusTrans> value) {
        this.statusTrans = ((JAXBElement<StatusTrans> ) value);
    }

}
