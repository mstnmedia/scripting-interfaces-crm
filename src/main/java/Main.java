/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import _do.com.claro.soa.crm.CrmSoaPort;
import _do.com.claro.soa.crm.CustomerCasesRequest;
import _do.com.claro.soa.crm.CustomerDetailsRequest;
import _do.com.claro.soa.crm.ESBCRMRSService;
import _do.com.claro.soa.model.customer.Interaction;
import _do.com.claro.soa.model.customer.InteractionDirection;
import _do.com.claro.soa.model.customer.InteractionType;
import _do.com.claro.soa.model.customer.Topic;
import _do.com.claro.soa.model.customer.Topics;
import _do.com.claro.soa.model.employee.Employee;
import _do.com.claro.soa.model.product.Comment;
import _do.com.claro.soa.services.customer.createcustomerinteraction.CreateCustomerInteraction;
import _do.com.claro.soa.services.customer.createcustomerinteraction.CreateCustomerInteraction_Service;
import _do.com.claro.soa.services.customer.createcustomerinteraction.CreateInteractionRequest;
import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetCrmTicketByCredentials;
import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetCrmTicketByCredentials_Service;
import _do.com.claro.soa.services.customer.getcrmticketbycredentials.GetRequest;
import com.claro.api.sad.service.core.GetDireccionPorTNResponse;
import com.claro.api.sad.service.core.GetDireccionesPorIDTCalleResponse;
import com.claro.spc.servlet.SPC;
import com.claro.spc.servlet.SPCService;
import com.claro.spc.servlet.STB;
import com.claro.spc.servlet.Transaccion;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.interfaces.clarovideo.ClaroVideo_OTT;
import com.mstn.scripting.interfaces.crm.CRM;
import com.mstn.scripting.interfaces.crm.CRM_CustomerSearch;
import com.mstn.scripting.interfaces.crm.CRM_ESB;
import com.mstn.scripting.interfaces.crm.CRM_GetCrmTicketByCredentials;
import com.mstn.scripting.interfaces.crm.SOAPHeaderHandler;
import com.mstn.scripting.interfaces.dth.DTH_REST;
import com.mstn.scripting.interfaces.dth.DTH_RESTApi;
//import com.mstn.scripting.interfaces.diagnostico.Diagnostico_IDW;
import com.mstn.scripting.interfaces.ensamble.Ensamble;
import com.mstn.scripting.interfaces.ensamble.EnsambleEJB;
import com.mstn.scripting.interfaces.ext.CustomerID_Ext;
import com.mstn.scripting.interfaces.ext.Subscription_Ext;
import com.mstn.scripting.interfaces.ext.TelephoneNumber_Ext;
import com.mstn.scripting.interfaces.fixed.Fixed;
import com.mstn.scripting.interfaces.miclaro.MiClaro_AGP;
import com.mstn.scripting.interfaces.oms.OMSOrderDetails;
import com.mstn.scripting.interfaces.oms.OMS_CustomerOrders;
import com.mstn.scripting.interfaces.sad.DireccionServiceImpl;
import com.mstn.scripting.interfaces.sad.SAD_V2;
import java.util.Arrays;
import java.util.List;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

/**
 * Clase ejecutable con métodos de pruebas para probar las diferentes
 * funcionalidades en este proyecto.
 *
 * @author amatos
 */
public class Main {

	/**
	 * @param args the command line arguments
	 * @throws java.lang.Exception
	 */
	static final String CEDULA_ABEL = "40226304430";
	static final String CUSTOMER_ID_ABEL = "01_12566174";
	static final String CUSTOMER_ID_MSTN_OLD = "01_10649623";
	static final String CUSTOMER_ID_MSTN = "01_7428021";
	static final String RNC_MSTN = "101859202";
	static final String RNC_POPULAR = "130193304";
	static final String PHONE_ABEL = "8294961819";
	static final String PHONE_POPULAR = "8095545555";
	static final String PHONE_MSTN = "8094126786";
	static final String PHONE_HOME = "8096817536";
	static int pageNum = 1;

	static TransactionInterfacesPayload payload = new TransactionInterfacesPayload(
			new V_Transaction(), new Workflow_Step(), "{\"customer_id\":\"" + CUSTOMER_ID_MSTN + "\"}"
	);
	static TransactionInterfacesPayload customerIDSearchPayload = new TransactionInterfacesPayload(
			null, null, "{\"search_type\":\"customer_id\",\"search_value\":\"" + CUSTOMER_ID_ABEL + "\"}"
	);
	static TransactionInterfacesPayload rncSearchPayload = new TransactionInterfacesPayload(
			null, null, "{\"search_type\":\"tax_id\",\"search_value\":\"" + RNC_POPULAR + "\"}"
	);
	static TransactionInterfacesPayload phoneSearchPayload = new TransactionInterfacesPayload(
			null, null, "{\"search_type\":\"phone_number\",\"search_value\":\"" + PHONE_ABEL + "\"}"
	);

	static Object GetCustomerDetails() {
		ESBCRMRSService service = new ESBCRMRSService();
		CrmSoaPort soap = service.getCrmSoaPortPt();
		CustomerDetailsRequest request = new CustomerDetailsRequest();
		request.setIdType("SSN");
		request.setIdValue("40221862499"); //Fidelia
//		payload.setIdValue("40226304430"); //Abel
		return soap.getCustomerDetails(request);
	}

	static Object CRM_CreateCase() throws Exception {
		return CRM.CreateCase(
				"8095813687", //subscriber
				"TEST", //title
				"EDOCS", //clientSystem
				"8094961819", //contactPhone
				"ADSL", //productType
				"Averias", //type1
				"Internet", //type2
				"Intermitencia", //type3
				"AVERIAS ADSL", //queueName
				"14/08/2018", //visitPeriod
				"TEST", //notes
				true //autoDispatch
		);
	}

	static Object CRM_ESB_GetCases() throws Exception {
		CrmSoaPort soap = new ESBCRMRSService().getCrmSoaPortPt();
		CustomerCasesRequest request = new CustomerCasesRequest();
		request.setIdType("PHONE");
		request.setIdValue("8095813687");
		request.setClientSystem("EDOCS");
		return soap.getCustomerCases(request);
	}

	static Object CRM_CreateCustomerInteraction() throws Exception {
		try {
			CreateCustomerInteraction soap = new CreateCustomerInteraction_Service().getCreateCustomerInteractionSOAP();
			CreateInteractionRequest request = new CreateInteractionRequest();
			request.setTicket("TksmauU0kPDS0iGjF9key3QWhCSTbDAMHNlrQZ0");
			Interaction interaction = new Interaction();
			interaction.setCustomerID(new CustomerID_Ext("01_9500047"));
			interaction.setContactID("3435692");
			interaction.setPhone(new TelephoneNumber_Ext(""));
			interaction.setTitle("1010189638");
			interaction.setCreationDate(null);
			interaction.setInteractionDirection(InteractionDirection.INBOUND);
			interaction.setChannel("Help Desk Roaming");
			interaction.setMedium("Phone");
			interaction.setInteractionType(InteractionType.CALL);
			interaction.setNotes("Prueba2");
			Topics topics = new Topics();
			Topic topic = new Topic();
			topic.setClassification("Averías");
			topic.setSubject("Claro tv (DTH)");
			topic.setOutcome("Sin señal");
			topics.topic = Arrays.asList(topic);
			interaction.setTopics(topics);
			Interaction.Creator creator = new Interaction.Creator();
			Employee employee = new Employee();
			employee.setEmployeeId("CT319078");
			creator.setEmployee(employee);
			interaction.setCreator(creator);

			request.setInteraction(interaction);
			Object response = soap.createInteraction(request);
			return response;
		} catch (Exception ex) {
			throw ex;
		}
	}

	static Object CRM_ESB_GetOpenCases() throws Exception {
		CrmSoaPort soap = new ESBCRMRSService().getCrmSoaPortPt();
		CustomerCasesRequest request = new CustomerCasesRequest();
		request.setIdType("CCU");
		request.setIdValue("1010123948");
		request.setClientSystem("EDOCS");
		return soap.getCustomerOpenCases(request);
	}

	static Object GetSubscriptionsBySubscriberNo() throws Exception {
		return Ensamble.GetSubscriptionsBySubscriberNo("8095813687");
	}

	static Object testNewInterfaceBase() throws Exception {
		CRM_ESB service = new CRM_ESB();
		payload.setForm("{}");
		Interface inter = service.getInterfaces(true).get(0);
		return service.callMethod(inter, payload, null);
	}

//	static Object testAsignVoid() throws Exception {
//		IPTV_SPC service = new IPTV_SPC();
//		TransactionInterfacesPayload payload = new TransactionInterfacesPayload();
//		payload.setForm("{}");
//		Interface inter = new Interface(0, "iptv_removerPinComprasPorGuid", "", "", "", false);
//		return service.callMethod(inter, payload);
//	}
//	static Object testDiagnostico() throws Exception {
//		Diagnostico_IDW service = new Diagnostico_IDW();
//		payload.setForm("{\"diagnosticoidw_getDataUsingDataContract_arg0_boolValue\": true, \"diagnosticoidw_getDataUsingDataContract_arg0_stringValue\": \"string\"}");
//
//		Interface inter = service.getInterfaces(true).stream()
//				.filter(item -> Utils.stringAreEquals(item.getName(), "diagnosticoidw_getDataUsingDataContract"))
//				.findFirst().get();
//		return service.callMethod(inter, payload, null);
//	}
	static Object testCRMESB() throws Exception {
		CRM_ESB service = new CRM_ESB();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces.get(0);
		String form = JSON.newObjectNode()
				.put("mail_sender_arg3", "")
				.toString();
		payload.setForm(form);
		return service.callMethod(inter, payload, null);
	}

	static Object testClaroVideo() throws Exception {
		ClaroVideo_OTT service = new ClaroVideo_OTT();
		Interface inter = service.getInterfaces(true).get(0);
		payload.setForm("{\"clarovideo_getCustomerInfo_arg0_subscriber\": \"8096815472\"}");
		TransactionInterfacesResponse result = service.callMethod(inter, payload, null);
		return result;
	}

	static Object testObtenerBanInfo() throws Exception {
		EnsambleEJB service = new EnsambleEJB();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces
				.stream()
				.filter(item -> item.getName().endsWith("getBanInfo"))
				.findFirst().get();
		ObjectNode form = JSON.newObjectNode()
				.put("ensambleejb_getBanInfo_arg0_banId", "718155483")
				.put("ensambleejb_getBanInfo_arg0_clientSystem", "SCRIPTING")
				.put("ensambleejb_getBanInfo_arg0_throwExceptionInd", "1");
		payload.setForm(form.toString());

		return service.callMethod(inter, payload, null);
	}
//	static Object testFacility() throws Exception {
//		Facility_SACS service = new Facility_SACS();
//		String form = JSON.newObjectNode()
//				.put("facilitysacs_facilityGetFacilitiesByPhoneNumber_phoneNumber", "8294961819")
//				.toString();
//		payload.setForm(form);
//
//		Interface inter = service.getInterfaces(true).get(0);
//		return service.callMethod(inter, payload);
//	}

	static Object testOMS() throws Exception {
		OMS_CustomerOrders service = new OMS_CustomerOrders();
		return service.getForm(payload, User.SYSTEM);
	}

	static Object testOMSOrderDetails() throws Exception {
		OMSOrderDetails service = new OMSOrderDetails();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces.stream().filter(i
				-> i.getName().endsWith("omsorderdetails_getDetailsByTypeServiceAndServiceId")
		//		-> i.getName().endsWith("omsorderdetails_getDetailsOrderId")
		).findFirst().get();
		String form = JSON.newObjectNode()
				.put("omsorderdetails_getDetailsByTypeServiceAndServiceId_arg0", "8092211534")
				.put("omsorderdetails_getDetailsByTypeServiceAndServiceId_arg1", "IPTV")
				.put("test", "")
				.toString();
		payload.setForm(form);
		return service.callMethod(inter, payload, User.SYSTEM);
	}

	static Object getTicketByCredentials() throws Exception {
		GetCrmTicketByCredentials INSTANCE = new GetCrmTicketByCredentials_Service()
				.getGetCrmTicketByCredentialsSOAP();
		BindingProvider bp = (BindingProvider) INSTANCE;
		String username = "CT319290";
		String password = "";
		List<Handler> handlerChain = bp.getBinding().getHandlerChain();
		handlerChain.add(new SOAPHeaderHandler(username, password));
		bp.getBinding().setHandlerChain(handlerChain);
		GetRequest params = new GetRequest();
		params.setIn("Lo que sea");
		return INSTANCE.get(params).getTicket();
	}

	static Object testCRMGetTicketByCredentials() throws Exception {
		CRM_GetCrmTicketByCredentials service = new CRM_GetCrmTicketByCredentials();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces.stream().filter(i -> i.getName().endsWith("get")).findFirst().get();
		String form = JSON.newObjectNode()
				.put("crmticketbycredentials_get_in", "Lo que sea")
				.put("crmticketbycredentials_get_user", "CT319343")
				.put("crmticketbycredentials_get_password", "")
				.put("test", "")
				.toString();
		payload.setForm(form);
		TransactionInterfacesResponse response = service.callMethod(inter, payload, User.SYSTEM);
		return response;
	}

	static Object testMiClaro() throws Exception {
		MiClaro_AGP service = new MiClaro_AGP();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces.stream()
				//				.filter(i -> i.getName().endsWith("getCustomerBasicInformation"))
				//				.filter(i -> i.getName().endsWith("getCambiazoInformationForMSISDN"))
				.filter(i -> i.getName().endsWith("miClaroRegisteredMemberVerification"))
				.findFirst().get();
		String document = "40221254754";
		ObjectNode form = JSON.newObjectNode()
				.put(inter.getName() + "_arg0", document)
				.put(inter.getName() + "_documentId", document)
				.put(inter.getName() + "_documentIdentifier", document)
				.put(inter.getName() + "_msisdn", document)
				.put(inter.getName() + "_test", "");
		payload.setForm(form.toString());
		TransactionInterfacesResponse response = service.callMethod(inter, payload, User.SYSTEM);
		return response;
	}

	static Object testFixed() throws Exception {
//		String phone = "8096817536";
//		String productCode = "ADSL";
//		Subscription_Ext subscription = Ensamble.getFirstSubscription(phone, productCode);
//		String customerID = subscription.getCustomerID();
//		return Fixed.GetFixedProduct(customerID, phone, productCode);

//		String phone = "1010151415";
//		String productCode = "ADSL";
//		Subscription_Ext subscription = Ensamble.getFirstSubscription(phone, productCode);
//		String customerID = subscription.getCustomerID();
//		return Fixed.GetFixedProduct(customerID, phone, productCode);
//
		String phone = "8092862136";
		String productCode = "ADSL";
		Subscription_Ext subscription = Ensamble.getFirstSubscription(phone, productCode);
		return Fixed.GetFixedProduct(subscription.getCustomerID(), phone, productCode);
//		return Ensamble.GetSubscriptionsBySubscriberNo(phone);
	}

	static Object testProductComments() throws Exception {
		String phone = "8095813687";
		String productCode = "IPTV";
		int page = 1;
		Subscription_Ext subscription = Ensamble.getFirstSubscription(phone, productCode);
		List<Comment> result = Ensamble.GetProductComments(subscription.getCustomerID(),
				subscription.getProductTypeCode(),
				subscription.getSubscriberNo(),
				page);
		return result;
	}

//	static String dthPhone = "1010061195";
	static Long dthPhone = 1010189638L;
	
	static String dthUser = "56798";

	static Object testDTH() throws Exception {
		DTH_REST service = new DTH_REST();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces
				.stream()
				.filter(item -> item.getName().endsWith("getClientProfile"))
				.findFirst().get();
		ObjectNode form = JSON.newObjectNode();
		String[] value = new String[]{"24364", dthPhone.toString()};
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.put(field.getName(), value[i]);
		}
		payload.setForm(form.toString());
		TransactionInterfacesResponse response = service.callMethod(inter, payload, null);
		return response;
	}

	static Object factoryResetSTB() throws Exception {
		DTH_RESTApi service = new DTH_RESTApi();
		DTH_RESTApi.EquipmentCommandRequest params = new DTH_RESTApi.EquipmentCommandRequest();
		params.user = dthUser;
		DTH_RESTApi.EquipmentCommandParam eParam = new DTH_RESTApi.EquipmentCommandParam();
		eParam.User = params.user;
		eParam.Id = 0;
		eParam.Ficticio = "1010189638";
		eParam.SetTopBox = "1781323163";
		eParam.Smartcard = "2283242400";
		params.equiments = Arrays.asList(eParam);
		DTH_RESTApi.CommandResponse result = service.factoryResetSTB(params);
		return result;
	}

	static Object dthReset() throws Exception {
		DTH_RESTApi service = new DTH_RESTApi();
		DTH_RESTApi.DTHRequest params = new DTH_RESTApi.DTHRequest(dthUser, dthPhone);
		DTH_RESTApi.CommandResponse result = service.refresh(params);
		return result;
	}

	static Object testIPTV() throws Exception {
		SPC soap = new SPCService().getSPC();
		Transaccion tx = new Transaccion();
		tx.setSistema("SCRIPTING");
		tx.setNumeroTransaction("Test");
		List<STB> result = soap.consultarSTBPorCuenta("8092204114", tx);
		return result;
	}

//	static String sadPhone = "8095302179";
//	static String sadPhone = "8096817536";
	static String sadPhone = "8095980560";

	static Object testSAD() throws Exception {
		SAD_V2 service = new SAD_V2();
		List<Interface> interfaces = service.getInterfaces(true);
		Interface inter = interfaces
				.stream()
				.filter(item -> item.getName().endsWith("consultarClientePorCuenta"))
				.findFirst().get();
		ObjectNode form = JSON.newObjectNode();
		String[] value = new String[]{sadPhone, "6789", "SCRIPTING"};
		for (int i = 0; i < inter.getInterface_field().size(); i++) {
			Interface_Field field = inter.getInterface_field().get(i);
			form.put(field.getName(), value[i]);
		}
		payload.setForm(form.toString());
		TransactionInterfacesResponse response = service.callMethod(inter, payload, null);
		return response;
	}

	static Object getDireccionPorTN() throws Exception {
		DireccionServiceImpl soap = new DireccionServiceImpl();
		GetDireccionPorTNResponse.SADResponse result = soap.getDireccionPorTN(sadPhone);
		return result;
	}

	static Object getTextoDireccionPorTN() throws Exception {
		DireccionServiceImpl service = new DireccionServiceImpl();
		String address = service.getTextoDireccionPorTN(sadPhone);
		return address;
	}

	static Object getDireccionesPorIDTCalle() throws Exception {
		DireccionServiceImpl soap = new DireccionServiceImpl();
		GetDireccionesPorIDTCalleResponse.SADResponse result = soap.getDireccionesPorIDTCalle("7707");
		return result;
	}

	public static void main(String[] args) throws Exception {
		try {
			InterfaceConfigs.setConfig();

			long before = DATE.nowAsLong();
			Object result = null;
//		String customerID = Ensamble.GetCustomerIDFromSubscriberNo("8095272621");
//		Product result = Fixed.GetFixedProduct(customerID, "8095272621", "PSTN");
//		MessageInfo result = MiClaro.RegisteredMemberVerification(CEDULA_ABEL);
//		List<Customer_Ext> result = Mixed.GetCustomerListByPhoneNumber(PHONE_ABEL);
//		List<Subscription_Ext> result = Ensamble.GetSubscriptionsBySubscriberNo(PHONE_ABEL);
//		GetCaseAttributesByCaseID_Service service = new GetCaseAttributesByCaseID_Service();
//		GetCaseAttributesByCaseID soap = service.getGetCaseAttributesByCaseIDSOAP();
//		List<String> caseIDs = Arrays.asList(
//		"31924154", "31894678", "30665684", "17854874", "13780229", "8783807", "8629262", "8423808", "7887664", "7861558"		
//		);
//		for (int i = 0; i < caseIDs.size(); i++) {
//			String caseID = caseIDs.get(i);
//			GetRequest request = new GetRequest();
//			request.setCaseID(caseID);
//			GetResponse response = soap.get(request);
//
//			System.out.println(JSON.toString(response));
//		}
//
//		result = CRM.GetAllCustomersByCustomerName("MSTN MEDIA");
//		result = CRM.GetAllCustomersByContactName("Abel_Matos Moquete");
//		result = Mixed.GetCustomerListByPhoneNumber("8096817536");
//		result = CRM.GetCustomerByIdentityCardID(CEDULA_ABEL, 1);
//		result = Ensamble.GetCustomerIDFromBanAccountNo("759205048");
//		List<Subscription_Ext> result = Ensamble.GetSubscriptionsBySubscriberNo("8096817536");
//
//		result = CRM.GetCustomerByIdentityCardID(CEDULA_ABEL, pageNum);;
//		result = CRM_CreateCase();
//		result = CRM_ESB_GetCases();
//			result = CRM_ESB_GetOpenCases();
//			result = CRM_CreateCustomerInteraction();
//		result = GetSubscriptionsBySubscriberNo();
//		result = GetCustomerDetails();
//		result = testNewInterfaceBase();
//		result = testAsignVoid();
//		result = testDiagnostico();
//		result = testFacility();
//		result = testOMS();
//			result = testOMSOrderDetails();
//			result = testCRMESB();
//		result = testObtenerBanInfo();
//		result = testMiClaro();
//		result = testClaroVideo();
//		result = testFixed();
//		result = testProductComments();
//		result = testIPTV();
//			result = testSAD();
//			result = getDireccionPorTN();
//			result = getDireccionesPorIDTCalle();
//			result = getTextoDireccionPorTN();
//			result = testDTH();
//			result = factoryResetSTB();
			result = dthReset();
//			result = new DTH_RESTApi().getClientProfile(new DTH_RESTApi.DTHRequest("24364", 1010061195));
//			result = new DTH_RESTApi().getClientProfile(new DTH_RESTApi.DTHRequest("24364", 1010189638));
//			result = new DTH_RESTApi().getClientProfileBySMC(new DTH_RESTApi.ClientProfileSMCRequest("24364", "1303598271"));
//			result = testCRMGetTicketByCredentials();
//			result = getTicketByCredentials();
//			result = Ensamble.getFirstSubscription("8093456795", "ADSL");
//			result = Ensamble.GetSubscriptionsBySubscriberNo("8093456795");
//			long after = DATE.nowAsLong();
//			System.out.println("Main Total: " + (after - before));
//			payload.setForm("{\"search_type\":\"smart_card\",\"search_value\":\"1303598271\"}");
//			result = new CRM_CustomerSearch().getForm(payload, User.SYSTEM);
			String resultTxt = JSON.toString(result);
			System.out.println(resultTxt);
		} catch (Exception ex) {
			throw ex;
		}
	}

}
