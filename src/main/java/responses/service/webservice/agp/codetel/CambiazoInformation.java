
package responses.service.webservice.agp.codetel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import codetel.agp.webservice.service.CambiazoElegibility;
import codetel.agp.webservice.service.CambiazoProcedence;
import codetel.agp.webservice.service.responses.MessageInfo;


/**
 * <p>Java class for CambiazoInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CambiazoInformation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://responses.service.webservice.agp.codetel}MessageInfo">
 *       &lt;sequence>
 *         &lt;element name="eligibility" type="{http://service.webservice.agp.codetel/}cambiazoElegibility" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cambiazoDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastCambiazoDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="elapsedMonths" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="apply" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="procedence" type="{http://service.webservice.agp.codetel/}cambiazoProcedence" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CambiazoInformation", propOrder = {
    "eligibility",
    "msisdn",
    "cambiazoDate",
    "lastCambiazoDate",
    "elapsedMonths",
    "apply",
    "procedence"
})
public class CambiazoInformation
    extends MessageInfo
{

    protected CambiazoElegibility eligibility;
    protected String msisdn;
    protected String cambiazoDate;
    protected String lastCambiazoDate;
    protected int elapsedMonths;
    protected boolean apply;
    protected CambiazoProcedence procedence;

    /**
     * Gets the value of the eligibility property.
     * 
     * @return
     *     possible object is
     *     {@link CambiazoElegibility }
     *     
     */
    public CambiazoElegibility getEligibility() {
        return eligibility;
    }

    /**
     * Sets the value of the eligibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link CambiazoElegibility }
     *     
     */
    public void setEligibility(CambiazoElegibility value) {
        this.eligibility = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the cambiazoDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCambiazoDate() {
        return cambiazoDate;
    }

    /**
     * Sets the value of the cambiazoDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCambiazoDate(String value) {
        this.cambiazoDate = value;
    }

    /**
     * Gets the value of the lastCambiazoDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastCambiazoDate() {
        return lastCambiazoDate;
    }

    /**
     * Sets the value of the lastCambiazoDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastCambiazoDate(String value) {
        this.lastCambiazoDate = value;
    }

    /**
     * Gets the value of the elapsedMonths property.
     * 
     */
    public int getElapsedMonths() {
        return elapsedMonths;
    }

    /**
     * Sets the value of the elapsedMonths property.
     * 
     */
    public void setElapsedMonths(int value) {
        this.elapsedMonths = value;
    }

    /**
     * Gets the value of the apply property.
     * 
     */
    public boolean isApply() {
        return apply;
    }

    /**
     * Sets the value of the apply property.
     * 
     */
    public void setApply(boolean value) {
        this.apply = value;
    }

    /**
     * Gets the value of the procedence property.
     * 
     * @return
     *     possible object is
     *     {@link CambiazoProcedence }
     *     
     */
    public CambiazoProcedence getProcedence() {
        return procedence;
    }

    /**
     * Sets the value of the procedence property.
     * 
     * @param value
     *     allowed object is
     *     {@link CambiazoProcedence }
     *     
     */
    public void setProcedence(CambiazoProcedence value) {
        this.procedence = value;
    }

}
