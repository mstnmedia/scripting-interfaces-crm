/**
 * Este proyecto contiene las interfaces que conectan con CRM, Claro Video, DTH, 
 * Diagnóstico IWD, Ensamble-Horizonte, Fijo, IPTV, Mi Claro, OMS y PPG. Además,
 * contiene clases propias de Scripting que son requeridas para el el inicio de 
 * transacciones.
 */
