
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceBase;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Interface_Field;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Before;
import org.junit.Test;

import org.reflections.Reflections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author amatos
 */
public class TestFile {

	static final List<InterfaceBase> BASES = new ArrayList();

//	@Before
	public void setup() throws Exception {
		setConfig();

		Reflections reflections = new Reflections("com.mstn.scripting");
		List<Class<? extends InterfaceBase>> baseClasses = reflections.getSubTypesOf(InterfaceBase.class)
				.stream()
				.sorted((a, b) -> {
					return a.getName().compareTo(b.getName());
				})
				.collect(Collectors.toList());
		for (int i = 0; i < baseClasses.size(); i++) {
			Class<? extends InterfaceBase> baseClass = baseClasses.get(i);
			System.out.println("Instancing " + baseClass.getName() + "...");
			InterfaceBase base = baseClass.newInstance();
			base.setShowBaseLogs(true);
			BASES.add(base);
		}
	}

//	@Test
	public void testFieldsAndResults() throws Exception {

		List<Interface> fails = new ArrayList();
		for (int i = 0; i < BASES.size(); i++) {
			InterfaceBase base = BASES.get(i);
			String baseName = base.getNAME();
			System.out.println("BASE: " + baseName);
			if (ignoreBase(baseName)) {
				continue;
			}
			List<Interface> interfaces = base.getInterfaces(true);
			for (int j = 0; j < interfaces.size(); j++) {
				Interface inter = interfaces.get(j);
				ObjectNode form = JSON.newObjectNode();
				for (int k = 0; k < inter.getInterface_field().size(); k++) {
					Interface_Field field = inter.getInterface_field().get(k);
					form.putPOJO(field.getName(), "\"" + String.valueOf(getFormValue(field)) + "\"");
				}
				try {
					base.callMethod(
							inter,
							new TransactionInterfacesPayload(
									new V_Transaction(),
									new Workflow_Step(),
									form.toString()
							),
							null
					);
				} catch (Exception ex) {
					Utils.logException(this.getClass(), inter.getName(), ex);
					fails.add(inter);
				}
			}
		}
		System.out.println("Interfaces: ");
		System.out.println(JSON.toString(fails.stream().map(i -> i.getName()).collect(Collectors.toList())));
	}

	Object getFormValue(Interface_Field field) {
		switch (field.getId_type()) {
			case InterfaceFieldTypes.BOOLEAN:
			case InterfaceFieldTypes.CHECKBOX:
				return true;
			case InterfaceFieldTypes.EMAIL:
				return "amatos@mstn.com";
			case InterfaceFieldTypes.NUMBER:
				return 45;
			case InterfaceFieldTypes.TAGS:
				return "hello,bye";
			case InterfaceFieldTypes.DATE:
				return "2018-05-31";
			case InterfaceFieldTypes.DATETIME:
				return "2018-05-31T12:15:30";
			case InterfaceFieldTypes.TIME:
				return "12:15:30";
			case InterfaceFieldTypes.AUTOCOMPLETE:
			case InterfaceFieldTypes.HIDDEN:
			case InterfaceFieldTypes.LINK:
			case InterfaceFieldTypes.OPTIONS:
			case InterfaceFieldTypes.RADIO:
			case InterfaceFieldTypes.TEXT:
			case InterfaceFieldTypes.TEXTAREA:
			default:
				return "Test";
		}
	}

	boolean ignoreBase(String name) {
		final List<String> IGNORED_BASES = Arrays.asList(
				"clarovideo", "crmesb",
				"diagnosticoidw", "dth", "ensambleejb",
				"iptv",
				"miclaroagp",
				""
		);

		if (IGNORED_BASES.contains(name)) {
			return true;
		}
		if (name.startsWith("crm")) {
			return true;
		}
		if (name.startsWith("ensamble")) {
			return true;
		}
		if (name.startsWith("fixed")) {
			return true;
		}
		if (name.startsWith("oms")) {
			return true;
		}
		if (name.startsWith("ppg")) {
			return true;
		}
		if (name.startsWith("complex")) {
			return true;
		}
		return false;
	}

	void setConfig() {
		InterfaceConfigs.configs.put("crm_esb", "http://soaqaenv3:8010/soa-infra/services/crm/CRM_ESB!1.0/CRM.CRM_RS_ep?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomerbycustomerid", "http://SOADEVENV1:6010/services/customer/GetCustomerByCustomerID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomersandcontactsbycontactidentitycardid", "http://SOADEVENV1:6010/services/customer/GetCustomersAndContactsByContactIdentityCardID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomersandcontactsbycontactpassportid", "http://SOADEVENV1:6010/services/customer/GetCustomersAndContactsByContactPassportID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomersandcontactsbytaxid", "http://SOADEVENV1:6010/services/customer/GetCustomersAndContactsByTaxID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomersandcontactsbycontactnames", "http://SOADEVENV1:6010/services/customer/GetCustomersAndContactsByContactNames?WSDL");
		InterfaceConfigs.configs.put("crm_getbusinesscustomersandcontactsbycustomername", "http://SOADEVENV1:6010/services/customer/GetBusinessCustomersAndContactsByCustomerName?WSDL");
		InterfaceConfigs.configs.put("crm_getcaseandsubcaseattachmentsbyid", "http://SOADEVENV1:6010/services/customer/GetCaseAndSubCaseAttachmentsByID?WSDL");
		InterfaceConfigs.configs.put("crm_getcaseattachmentbyfilepath", "http://SOADEVENV1:6010/services/customer/GetCaseAttachmentByFilePath?WSDL");
		InterfaceConfigs.configs.put("crm_getcaseattributesbycaseid", "http://SOADEVENV1:6010/services/customer/GetCaseAttributesByCaseID?WSDL");
		InterfaceConfigs.configs.put("crm_getcasediagnosesbycaseid", "http://SOAQAENV3:6010/services/customer/GetCaseDiagnosesByCaseID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomercasesbycustomerid", "http://SOADEVENV1:6010/services/customer/GetCustomerCasesByCustomerID?WSDL");
		InterfaceConfigs.configs.put("crm_getsubcasesbycaseid", "http://SOADEVENV1:6010/services/customer/GetSubCasesByCaseID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomeractionitemsbycustomerid", "http://SOADEVENV1:6010/services/customer/GetCustomerActionItemsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomerinteractionsbycustomerid", "http://SOADEVENV1:6010/services/customer/GetCustomerInteractionsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("ensamble_ejb", "http://soaqaenv1:7020/EJB-EnsembleServicesWrapper-context-root/EnWrapperServiceSoapHttpPort?wsdl");
		InterfaceConfigs.configs.put("ensamble_getbilldetailsbyban", "http://SOADEVENV1:6010/services/billing/GetBillDetailsByBAN?WSDL");
		InterfaceConfigs.configs.put("ensamble_getbillingaccountbyban", "http://SOADEVENV1:6010/services/billing/GetBillingAccountByBAN?WSDL");
		InterfaceConfigs.configs.put("ensamble_getbillingaccountsbycustomerid", "http://SOADEVENV1:6010/services/billing/GetBillingAccountsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("ensamble_getbillshistorybybillingaccountno", "http://SOADEVENV1:6010/services/billing/GetBillsHistoryByBillingAccountNo?WSDL");
		InterfaceConfigs.configs.put("ensamble_getpaymentshistorybybillingaccountno", "http://SOADEVENV1:6010/services/billing/GetPaymentsHistoryByBillingAccountNo?WSDL");
		InterfaceConfigs.configs.put("ensamble_getpostpaidsubscriptionsbybillingaccountno", "http://SOADEVENV1:6010/services/billing/GetPostPaidSubscriptionsByBillingAccountNo?WSDL");
		InterfaceConfigs.configs.put("ensamble_getpostpaidsubscriptionsbycustomerid", "http://SOADEVENV1:6010/services/billing/GetPostpaidSubscriptionsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("ensamble_getsubscriptionmemos", "http://SOADEVENV1:6010/services/billing/GetSubscriptionMemos?WSDL");
		InterfaceConfigs.configs.put("ensamble_getsubscriptionsbysubscriberno", "http://SOADEVENV1:6010/services/billing/GetSubscriptionsBySubscriberNo?WSDL");
		InterfaceConfigs.configs.put("ensamble_getsubscriptionsbycustomerid", "http://SOADEVENV1:6010/services/billing/GetSubscriptionsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("claro_video", "http://172.27.17.47:7001/ott-ws/OTTEndpointService?WSDL");
		InterfaceConfigs.configs.put("diagnostico_web", "http://nttappsweb0010.corp.codetel.com.do/SACSWS/DWServices.svc?wsdl");
		InterfaceConfigs.configs.put("diagnostico_sacs", "http://nttappsweb0009/SACSWS");
		InterfaceConfigs.configs.put("dth_satsservices", "http://nttappsweb0009/SATSWS/SATSServices.asmx?wsdl");
		InterfaceConfigs.configs.put("iptv", "http://172.29.80.22:7001/SPC/services/SPCRouter?wsdl");
		InterfaceConfigs.configs.put("fixed", "http://SOADEVENV1:6010/services/product/GetFixedProduct?wsdl");
		InterfaceConfigs.configs.put("miclaro", "https://lxtwlcluts01:7015/agpws/ws/miclaroproviderservices?wsdl				");
		InterfaceConfigs.configs.put("ppg_getprepaidsubscriptionsbycustomerid", "http://SOADEVENV1:6010/services/billing/GetPrepaidSubscriptionsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("ppg_getmixedrentshistorybysubno", "http://SOADEVENV1:6010/services/billing/GetMixedRentsHistoryBySubNo?WSDL");
		InterfaceConfigs.configs.put("ppg_getsubscriptionrefilltransactions", "http://SOADEVENV1:6010/services/billing/GetSubscriptionRefillTransactions?WSDL");
		InterfaceConfigs.configs.put("oms_getorderactionsbycustomerid", "http://SOADEVENV1:6010/services/ordering/GetOrderActionsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("oms_getorderactionsbyproduct", "http://SOADEVENV1:6010/services/ordering/GetOrderActionsByProduct?WSDL");
		InterfaceConfigs.configs.put("oms_getorderbyorderid", "http://SOADEVENV1:6010/services/ordering/GetOrderByOrderID?WSDL");
		InterfaceConfigs.configs.put("complex_hrl", "http://172.27.17.161:7501/hlr-wservice/HLREndPointService?WSDL");
		InterfaceConfigs.configs.put("smscHost", "172.18.247.90");
		InterfaceConfigs.configs.put("smscPort", "5016");
		InterfaceConfigs.configs.put("smscUsername", "Scripting");
		InterfaceConfigs.configs.put("smscPassword", "Scr!p7w");
	}
}
